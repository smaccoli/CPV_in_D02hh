The selection in made in the directory PreSelection:

for D2KSpi:
 - SelectD2KSpi.cxx
 - selectD2KSpi.cxx

for D2Kpipi:
 - SelectD2Kpipi.cxx
 - selectKpipi.cxx

for D2Kpi:
 - selectKKandKpi.cxx
 - SelectKP.cxx

for D2KK:
 - selectKKandKpi.cxx
 - SelectKK.cxx

The kinematic weighting is performed in the directories D2Kpipi_to_D2KSpi, D02Kpi_to_D2KpipiW, D02KK_to_D02KpiW of reweight with the scripts:
 - runIWeight_{...}.cxx (creation of weight istrograms in weights folder)
 - apply_{...}.cxx (application of weight rules)
 - plots/before_after.cxx (plots before and after kin. weighting application)

The fits are made in the directories Dp2KS0pipLL, Dp2Kmpippip, D02Kmpip, D02KmKp:

for Dp decays:
 - fitHist_global_JSU1_G2.C

for D0 decays:
 - fitDeltaMass.C

results are in the sub-folder params with name:
 - params_{year}_{polarity}.txt
- params_{year}_{polarity}_rew.txt if the sample is kinematically weighted
