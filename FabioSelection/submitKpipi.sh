#!/bin/bash

doSelection=$1                                                                          
doFiducial=$2                                                                           
doSecondaries=$3                                                                        
doMultCandRemoval=$4                                                                    
isPGun=$5                                                                    


if [ -z $doSelection ]; then                                                            
    echo "SELECTION FLAG NOT SET! PLEASE CHECK"                                         
    exit 0                                                                              
fi                                                                                      

if [ -z $doFiducial ]; then                                                             
    echo "SELECTION FLAG NOT SET! PLEASE CHECK"                                         
    exit 0                                                                              
fi                                                                                      

if [ -z $doSecondaries ]; then                                                          
    echo "SELECTION FLAG NOT SET! PLEASE CHECK"                                         
    exit 0                                                                              
fi                                                                                      

if [ -z $doMultCandRemoval ]; then                                                      
    echo "SELECTION FLAG NOT SET! PLEASE CHECK"                                         
    exit 0                                                                              
fi                                                                                      
if [ -z $isPGun ]; then                                                      
    echo "SELECTION FLAG NOT SET! PLEASE CHECK"                                         
    exit 0                                                                              
fi                                                                                      

echo "DO SELECTION: "$doSelection                                                       
echo "DO FIDUCIAL CUTS: "$doFiducial                                                    
echo "DO SECONDARIES REMOVAL: "$doSecondaries                                           
echo "DO MULTIPLE CANDIDATES REMOVAL: "$doMultCandRemoval                          
echo "USING PGUN SAMPLE: "$isPGun                          

DIR=$PWD
/bin/rm $DIR/err/*
/bin/rm $DIR/out/*

#make reduceKpipi
g++ -Wall -o reduceKpipi reduceKpipi.C `root-config --cflags --glibs` `gsl-config --cflags --libs` -lstdc++

#for year in $(echo 2015 2016 2017 2018); do
#    for pol in $(echo MagUp MagDown); do
#for year in $(echo 2016); do
#   for pol in $(echo MagDown); do
#	/bin/rm $DIR/ntuples/$year/$pol/files/*Dplus2Kpipi*.root 
for inputFile in $(ls -d /home/LHCB-T3/smaccoli/data/CPV_in_D02hh/pGun/Dp2Kmpippip_2016_Dw/{6000..7000}/pGun_Dp2Kmpippip.root); do
    
    SEED=$(echo $inputFile | tr "/" " " | awk '{print $8}')
    outFile="../data/tmp_Dp2Kmpippip_pGun_16_Dw_"$SEED".root"
    echo $SEED $outFile
    echo "#!/bin/sh" > tmp.sh
    echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> tmp.sh 
    echo "cd $DIR" >> tmp.sh
    #echo 'echo $HOSTNAME > logs/$year.$pol.$SEED' >> tmp.sh
    #echo "./reduceKpipi $inputFile \"Dp2KmPipPip/DecayTree\" ntuples/$year/$pol/files/$outFile \"ntp\" $doSelection $doFiducial $doSecondaries $doMultCandRemoval $isPGun $SEED  >> logs/$year.$pol.$SEED" >> tmp.sh
    echo "./reduceKpipi $inputFile \"Dp2KmPipPip/DecayTree\" $outFile \"ntp\" $doSelection $doFiducial $doSecondaries $doMultCandRemoval $isPGun $SEED  > logs/pGun.$SEED" >> tmp.sh
    
    chmod 777 tmp.sh
    ./tmp.sh
    
    #qsub -N $year.$pol.$SEED -e $DIR"/err/"$year.$pol.$SEED -o $DIR"/out/"$year.$pol.$SEED tmp.sh
done
#   done
#done

