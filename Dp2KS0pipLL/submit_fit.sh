#!/bin/bash

DIR=$PWD
#/bin/rm $DIR/exec/*
#/bin/rm $DIR/err/*
#/bin/rm $DIR/out/*
#/bin/rm $DIR/logs/*
source ~/.bashrc
setROOT_sPlot
which root
#gcc -Wall -o /home/LHCB/smaccoli/RooJohnsonSU.so /home/LHCB/smaccoli/RooJohnsonSU.cpp  -lX11 -lstdc++ `root-config --cflags --glibs` -lRooStats -lRooFit -lRooFitCore

#gcc -Wall -o _fitHist_global_JSU1_G2 fitHist_global_JSU1_G2.C  -lX11 -lstdc++ `root-config --cflags --glibs` -lRooStats -lRooFit -lRooFitCore -L/home/LHCB/smaccoli /home/LHCB/smaccoli/RooJohnsonSU_cpp.so
#g++ -Wall -o _fitHist_global_JSU1_G2 fitHist_global_JSU1_G2.C `root-config --cflags --glibs`
source /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_87/gcc/4.9.3/x86_64-slc6/setup.sh
source /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_87/ROOT/6.08.02/x86_64-slc6-gcc49-opt/bin/thisroot.sh
which root

for year in $(echo 15 16 17 18); do
    for pol in $(echo Up Dw); do
#for year in $(echo 18); do
#    for pol in $(echo Dw); do
	echo "#!/bin/sh" > exec/$year.$pol.sh
	echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> exec/$year.$pol.sh 
	echo "cd $DIR" >> exec/$year.$pol.sh
	echo "hostname" >> exec/$year.$pol.sh
	echo "source ~/.bashrc" >> exec/$year.$pol.sh
	echo "setROOT_sPlot" >> exec/$year.$pol.sh
	#echo "env" >> exec/$year.$pol.sh
	echo "root -l -b -q fitHist_global_JSU1_G2.C'(\"${year}\",\"${pol}\", 1, 1, 0, 1)'" >> exec/$year.$pol.sh
	chmod +x exec/$year.$pol.sh
	python condor_make_template.py condor_template.sub $year.$pol
	condor_submit exec/condor_template.$year.$pol.sub -batch-name $year$pol
	#break
    done
    #break
done
