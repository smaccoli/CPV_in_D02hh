
Processing fitHist_global_JSU1_G2.C+("18","Dw",1,1,0,1)...

[1mRooFit v3.60 -- Developed by Wouter Verkerke and David Kirkby[0m 
                Copyright (C) 2000-2013 NIKHEF, University of California & Stanford University
                All rights reserved, please read http://roofit.sourceforge.net/license.txt

-------------------------
Set LHCb Style - Feb 2012
-------------------------
Requested precision: 1e-08 absolute, 1e-08 relative

1-D integration method: RooAdaptiveGaussKronrodIntegrator1D (RooImproperIntegrator1D if open-ended)
2-D integration method: RooAdaptiveIntegratorND (N/A if open-ended)
N-D integration method: RooAdaptiveIntegratorND (N/A if open-ended)

Available integration methods:

*** RooBinIntegrator ***
Capabilities: [1-D] [2-D] [N-D] 
Configuration: 
  1)  numBins = 100

*** RooIntegrator1D ***
Capabilities: [1-D] 
Configuration: 
  1)        sumRule = Trapezoid(idx = 0)

  2)  extrapolation = Wynn-Epsilon(idx = 1)

  3)       maxSteps = 20
  4)       minSteps = 999
  5)       fixSteps = 0

*** RooIntegrator2D ***
Capabilities: [2-D] 
Configuration: 
(Depends on 'RooIntegrator1D')

*** RooSegmentedIntegrator1D ***
Capabilities: [1-D] 
Configuration: 
  1)  numSeg = 3
(Depends on 'RooIntegrator1D')

*** RooSegmentedIntegrator2D ***
Capabilities: [2-D] 
Configuration: 
(Depends on 'RooSegmentedIntegrator1D')

*** RooImproperIntegrator1D ***
Capabilities: [1-D] [OpenEnded] 
Configuration: 
(Depends on 'RooIntegrator1D')

*** RooMCIntegrator ***
Capabilities: [1-D] [2-D] [N-D] 
Configuration: 
  1)   samplingMode = Importance(idx = 0)

  2)        genType = QuasiRandom(idx = 0)

  3)        verbose = false(idx = 0)

  4)          alpha = 1.5
  5)    nRefineIter = 5
  6)  nRefinePerDim = 1000
  7)     nIntPerDim = 5000

*** RooAdaptiveGaussKronrodIntegrator1D ***
Capabilities: [1-D] [OpenEnded] 
Configuration: 
  1)  maxSeg = 100
  2)  method = 21Points(idx = 2)


*** RooGaussKronrodIntegrator1D ***
Capabilities: [1-D] [OpenEnded] 
Configuration: 

*** RooAdaptiveIntegratorND ***
Capabilities: [2-D] [N-D] 
Configuration: 
  1)  maxEval2D = 100000
  2)  maxEval3D = 1e+06
  3)  maxEvalND = 1e+07
  4)    maxWarn = 5

[#1] INFO:DataHandling -- RooDataHist::adjustBinning(data_0): fit range of variable Dplus_M expanded to nearest bin boundaries: [1800,1930] --> [1800,1930]
[#1] INFO:DataHandling -- RooDataHist::adjustBinning(data): fit range of variable Dplus_M expanded to nearest bin boundaries: [1800,1930] --> [1800,1930]
[#0] ERROR:Eval -- RooAbsReal::logEvalError(sigD_JSU1_plus) evaluation error, 
 origin       : RooJohnsonSU::sigD_JSU1_plus[ x=Dplus_M mean=sigD_mean1_plus width=sigD_sigma1_plus nu=JSU1_nu_plus tau=JSU1_tau_plus ]
 message      : p.d.f value is Not-a-Number (-nan), forcing value to zero
 server values: x=Dplus_M=1865, mean=sigD_mean1_plus=1965, width=sigD_sigma1_plus=10, nu=JSU1_nu_plus=0, tau=JSU1_tau_plus=0
[#0] ERROR:Eval -- RooAbsReal::logEvalError(sigD_JSU1_plus) evaluation error, 
 origin       : RooJohnsonSU::sigD_JSU1_plus[ x=Dplus_M mean=sigD_mean1_plus width=sigD_sigma1_plus nu=JSU1_nu_plus tau=JSU1_tau_plus ]
 message      : p.d.f value is Not-a-Number (-nan), forcing value to zero
 server values: x=Dplus_M=1865, mean=sigD_mean1_plus=1965, width=sigD_sigma1_plus=10, nu=JSU1_nu_plus=0, tau=JSU1_tau_plus=0
[#0] ERROR:Eval -- RooAbsReal::logEvalError(sigD_JSU1_minus) evaluation error, 
 origin       : RooJohnsonSU::sigD_JSU1_minus[ x=Dplus_M mean=sigD_mean1_minus width=sigD_sigma1_minus nu=JSU1_nu_minus tau=JSU1_tau_minus ]
 message      : p.d.f value is Not-a-Number (-nan), forcing value to zero
 server values: x=Dplus_M=1865, mean=sigD_mean1_minus=1965, width=sigD_sigma1_minus=10, nu=JSU1_nu_minus=0, tau=JSU1_tau_minus=0
[#0] ERROR:Eval -- RooAbsReal::logEvalError(sigD_JSU1_minus) evaluation error, 
 origin       : RooJohnsonSU::sigD_JSU1_minus[ x=Dplus_M mean=sigD_mean1_minus width=sigD_sigma1_minus nu=JSU1_nu_minus tau=JSU1_tau_minus ]
 message      : p.d.f value is Not-a-Number (-nan), forcing value to zero
 server values: x=Dplus_M=1865, mean=sigD_mean1_minus=1965, width=sigD_sigma1_minus=10, nu=JSU1_nu_minus=0, tau=JSU1_tau_minus=0
[#0] ERROR:InputArguments -- RooRealVar::readFromStream(N_bkg): parse error, cannot convert '1e' to double precision
[#0] ERROR:InputArguments -- RooRealVar::readFromStream(N_sigD): parse error, cannot convert '1e' to double precision
 **********
 **    9 **SET STR           2
 **********
 NOW USING STRATEGY  2: MAKE SURE MINIMUM TRUE, ERRORS CORRECT  
 **********
 **   10 **SET PRINT           1
 **********
 **********
 **   15 **MIGRAD        4500           1
 **********
 FIRST CALL TO USER FUNCTION AT NEW START POINT, WITH IFLAG=4.
 START MIGRAD MINIMIZATION.  STRATEGY  2.  CONVERGENCE WHEN EDM .LT. 1.00e-03
 COVARIANCE MATRIX CALCULATED SUCCESSFULLY
 FCN=546.372 FROM HESSE     STATUS=OK             89 CALLS          90 TOTAL
                     EDM=8.89553    STRATEGY= 2      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  JSU1_nu     -5.54056e-01   3.72926e-02   9.87035e-04   1.39536e+03
   2  JSU1_tau     5.41860e-01   2.21354e-02   1.01090e-03  -7.28472e+02
   3  N_bkg        7.37750e+04   7.14242e+02   4.85086e-06   1.01959e+05
   4  N_sigD       4.56860e+05   9.45778e+02   3.79002e-06   2.73237e+04
   5  bkg_0_slope  -4.58106e-03   1.31907e-04   1.83719e-04  -1.68158e+03
   6  sigD_0_frac1   4.99200e-01   1.61951e-02   2.20387e-03  -1.73244e+02
   7  sigD_0_mean1   1.86940e+03   3.46235e-02   1.07778e-04  -1.97597e+04
   8  sigD_0_sigma1   1.28710e+01   2.41919e-01   6.74704e-04  -1.77869e+02
   9  sigD_0_sigma2   7.32150e+00   5.83528e-02   4.16237e-04  -2.15662e+02
 MIGRAD MINIMIZATION HAS CONVERGED.
 MIGRAD WILL VERIFY CONVERGENCE AND ERROR MATRIX.
 COVARIANCE MATRIX CALCULATED SUCCESSFULLY
 FCN=537.487 FROM MIGRAD    STATUS=CONVERGED     203 CALLS         204 TOTAL
                     EDM=1.27474e-05    STRATEGY= 2      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  JSU1_nu     -5.54006e-01   4.90058e-02   3.15471e-05  -5.79652e-01
   2  JSU1_tau     5.41904e-01   2.34747e-02   3.12402e-05   1.23341e+00
   3  N_bkg        7.35448e+04   7.01800e+02   7.46428e-07  -7.09290e+01
   4  N_sigD       4.56854e+05   9.35914e+02   7.42568e-07  -1.24933e+02
   5  bkg_0_slope  -4.58100e-03   1.35233e-04   5.70378e-06  -8.97257e-03
   6  sigD_0_frac1   4.99199e-01   1.71927e-02   6.82740e-05  -2.66346e-01
   7  sigD_0_mean1   1.86944e+03   3.15745e-02   3.33600e-06   4.01887e+00
   8  sigD_0_sigma1   1.28714e+01   2.28915e-01   2.09504e-05  -8.37880e-01
   9  sigD_0_sigma2   7.32151e+00   6.01759e-02   1.28956e-05  -2.59766e+00
 EXTERNAL ERROR MATRIX.    NDIM=  25    NPAR=  9    ERR DEF=1
  2.402e-03  7.195e-04 -1.034e+01  1.034e+01 -3.173e-06  4.219e-04  4.805e-04 -2.247e-03  1.148e-03 
  7.195e-04  5.511e-04 -1.193e+01  1.194e+01 -3.110e-07 -3.910e-05 -3.080e-04  2.918e-03  1.060e-03 
 -1.034e+01 -1.193e+01  4.925e+05 -4.190e+05  7.409e-03  2.614e+00  1.058e+01 -1.011e+02 -1.993e+01 
  1.034e+01  1.194e+01 -4.190e+05  8.759e+05 -7.410e-03 -2.614e+00 -1.058e+01  1.011e+02  1.993e+01 
 -3.173e-06 -3.110e-07  7.409e-03 -7.410e-03  1.829e-08 -5.994e-07 -1.816e-06  6.625e-06 -5.249e-07 
  4.219e-04 -3.910e-05  2.614e+00 -2.614e+00 -5.994e-07  2.957e-04  2.154e-04 -3.257e-03 -4.740e-04 
  4.805e-04 -3.080e-04  1.058e+01 -1.058e+01 -1.816e-06  2.154e-04  9.969e-04 -4.728e-03 -4.890e-04 
 -2.247e-03  2.918e-03 -1.011e+02  1.011e+02  6.625e-06 -3.257e-03 -4.728e-03  5.240e-02  7.963e-03 
  1.148e-03  1.060e-03 -1.993e+01  1.993e+01 -5.249e-07 -4.740e-04 -4.890e-04  7.963e-03  3.621e-03 
 PARAMETER  CORRELATION COEFFICIENTS  
       NO.  GLOBAL      1      2      3      4      5      6      7      8      9
        1  0.95936   1.000  0.625 -0.301  0.226 -0.479  0.501  0.311 -0.200  0.389
        2  0.98321   0.625  1.000 -0.724  0.543 -0.098 -0.097 -0.416  0.543  0.750
        3  0.85901  -0.301 -0.724  1.000 -0.638  0.078  0.217  0.477 -0.629 -0.472
        4  0.66304   0.226  0.543 -0.638  1.000 -0.059 -0.162 -0.358  0.472  0.354
        5  0.60446  -0.479 -0.098  0.078 -0.059  1.000 -0.258 -0.425  0.214 -0.065
        6  0.98453   0.501 -0.097  0.217 -0.162 -0.258  1.000  0.397 -0.827 -0.458
        7  0.88277   0.311 -0.416  0.477 -0.358 -0.425  0.397  1.000 -0.654 -0.257
        8  0.98854  -0.200  0.543 -0.629  0.472  0.214 -0.827 -0.654  1.000  0.578
        9  0.94247   0.389  0.750 -0.472  0.354 -0.065 -0.458 -0.257  0.578  1.000
 **********
 **   20 **HESSE        4500
 **********
 COVARIANCE MATRIX CALCULATED SUCCESSFULLY
 FCN=537.487 FROM HESSE     STATUS=OK             73 CALLS         277 TOTAL
                     EDM=1.27394e-05    STRATEGY= 2      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                INTERNAL      INTERNAL  
  NO.   NAME      VALUE            ERROR       STEP SIZE       VALUE   
   1  JSU1_nu     -5.54006e-01   4.90105e-02   6.30942e-06  -1.11029e-01
   2  JSU1_tau     5.41904e-01   2.34510e-02   6.24804e-06  -8.99858e-01
   3  N_bkg        7.35448e+04   7.01456e+02   3.73214e-07  -1.56537e+00
   4  N_sigD       4.56854e+05   9.35619e+02   3.71284e-07  -1.55728e+00
   5  bkg_0_slope  -4.58100e-03   1.35245e-04   2.28151e-07   1.52799e+00
   6  sigD_0_frac1   4.99199e-01   1.71872e-02   1.36548e-05  -1.60247e-03
   7  sigD_0_mean1   1.86944e+03   3.15625e-02   6.67199e-07  -8.34200e-01
   8  sigD_0_sigma1   1.28714e+01   2.28654e-01   4.19008e-06  -3.64443e-01
   9  sigD_0_sigma2   7.32151e+00   6.01242e-02   2.57912e-06  -7.85445e-01
 EXTERNAL ERROR MATRIX.    NDIM=  25    NPAR=  9    ERR DEF=1
  2.402e-03  7.190e-04 -1.034e+01  1.034e+01 -3.175e-06  4.226e-04  4.812e-04 -2.255e-03  1.146e-03 
  7.190e-04  5.500e-04 -1.191e+01  1.191e+01 -3.121e-07 -3.844e-05 -3.070e-04  2.906e-03  1.056e-03 
 -1.034e+01 -1.191e+01  4.920e+05 -4.185e+05  7.439e-03  2.596e+00  1.056e+01 -1.008e+02 -1.985e+01 
  1.034e+01  1.191e+01 -4.185e+05  8.754e+05 -7.439e-03 -2.596e+00 -1.056e+01  1.008e+02  1.986e+01 
 -3.175e-06 -3.121e-07  7.439e-03 -7.439e-03  1.829e-08 -5.996e-07 -1.816e-06  6.621e-06 -5.259e-07 
  4.226e-04 -3.844e-05  2.596e+00 -2.596e+00 -5.996e-07  2.955e-04  2.149e-04 -3.251e-03 -4.730e-04 
  4.812e-04 -3.070e-04  1.056e+01 -1.056e+01 -1.816e-06  2.149e-04  9.962e-04 -4.718e-03 -4.865e-04 
 -2.255e-03  2.906e-03 -1.008e+02  1.008e+02  6.621e-06 -3.251e-03 -4.718e-03  5.229e-02  7.937e-03 
  1.146e-03  1.056e-03 -1.985e+01  1.986e+01 -5.259e-07 -4.730e-04 -4.865e-04  7.937e-03  3.615e-03 
 PARAMETER  CORRELATION COEFFICIENTS  
       NO.  GLOBAL      1      2      3      4      5      6      7      8      9
        1  0.95937   1.000  0.626 -0.301  0.225 -0.479  0.502  0.311 -0.201  0.389
        2  0.98318   0.626  1.000 -0.724  0.543 -0.098 -0.095 -0.415  0.542  0.749
        3  0.85886  -0.301 -0.724  1.000 -0.638  0.078  0.215  0.477 -0.628 -0.471
        4  0.66277   0.225  0.543 -0.638  1.000 -0.059 -0.161 -0.358  0.471  0.353
        5  0.60455  -0.479 -0.098  0.078 -0.059  1.000 -0.258 -0.425  0.214 -0.065
        6  0.98452   0.502 -0.095  0.215 -0.161 -0.258  1.000  0.396 -0.827 -0.458
        7  0.88267   0.311 -0.415  0.477 -0.358 -0.425  0.396  1.000 -0.654 -0.256
        8  0.98851  -0.201  0.542 -0.628  0.471  0.214 -0.827 -0.654  1.000  0.577
        9  0.94237   0.389  0.749 -0.471  0.353 -0.065 -0.458 -0.256  0.577  1.000
 **********
 **    9 **SET STR           2
 **********
 NOW USING STRATEGY  2: MAKE SURE MINIMUM TRUE, ERRORS CORRECT  
 **********
 **   10 **SET PRINT           1
 **********
 **********
 **   15 **MIGRAD        3000           1
 **********
 FIRST CALL TO USER FUNCTION AT NEW START POINT, WITH IFLAG=4.
 START MIGRAD MINIMIZATION.  STRATEGY  2.  CONVERGENCE WHEN EDM .LT. 1.00e-03
 COVARIANCE MATRIX CALCULATED SUCCESSFULLY
 FCN=1009.87 FROM HESSE     STATUS=OK             50 CALLS          51 TOTAL
                     EDM=0.411835    STRATEGY= 2      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  A_bkg        9.39440e-03   5.05763e-03   2.42079e-03  -8.12033e-02
   2  A_sigD_blind   3.73340e-03   1.58167e-03   7.61704e-04  -2.09701e+00
   3  N_bkg        7.35448e+04   3.69160e+02   6.57012e-06  -9.70371e+04
   4  N_sigD       4.56854e+05   7.21027e+02   5.14884e-06  -3.11071e+04
   5  bkg_delta_slope  -4.17835e-05   1.09223e-04   5.33910e-06   4.79172e+02
   6  sigD_delta_mean  -8.34628e-02   1.49394e-02   1.49445e-04  -1.33269e+00
 MIGRAD MINIMIZATION HAS CONVERGED.
 MIGRAD WILL VERIFY CONVERGENCE AND ERROR MATRIX.
 COVARIANCE MATRIX CALCULATED SUCCESSFULLY
 FCN=1009.46 FROM MIGRAD    STATUS=CONVERGED     103 CALLS         104 TOTAL
                     EDM=2.51755e-06    STRATEGY= 2      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  A_bkg        9.39625e-03   5.06190e-03   7.56342e-05   1.43654e-01
   2  A_sigD_blind   3.73319e-03   1.58230e-03   2.37984e-05   1.74753e-01
   3  N_bkg        7.37743e+04   3.70323e+02   7.46424e-07  -1.43214e+02
   4  N_sigD       4.56861e+05   7.21264e+02   7.42568e-07   1.65640e+01
   5  bkg_delta_slope  -4.18236e-05   1.09298e-04   1.66813e-07   6.03558e+01
   6  sigD_delta_mean  -8.34614e-02   1.49440e-02   2.30221e-05   4.05253e-02
 EXTERNAL ERROR MATRIX.    NDIM=  48    NPAR=  6    ERR DEF=1
  2.562e-05 -1.949e-06  1.751e-03 -1.713e-03  7.097e-08  7.462e-08 
 -1.949e-06  2.504e-06 -1.980e-03  1.988e-03 -1.149e-08 -1.125e-08 
  1.751e-03 -1.980e-03  1.371e+05 -6.336e+04  1.475e-04 -1.159e-02 
 -1.713e-03  1.988e-03 -6.336e+04  5.202e+05 -1.467e-04  1.170e-02 
  7.097e-08 -1.149e-08  1.475e-04 -1.467e-04  1.195e-08 -1.935e-07 
  7.462e-08 -1.125e-08 -1.159e-02  1.170e-02 -1.935e-07  2.233e-04 
 PARAMETER  CORRELATION COEFFICIENTS  
       NO.  GLOBAL      1      2      3      4      5      6
        1  0.26843   1.000 -0.243  0.001 -0.000  0.128  0.001
        2  0.24599  -0.243  1.000 -0.003  0.002 -0.066 -0.000
        3  0.23725   0.001 -0.003  1.000 -0.237  0.004 -0.002
        4  0.23721  -0.000  0.002 -0.237  1.000 -0.002  0.001
        5  0.17848   0.128 -0.066  0.004 -0.002  1.000 -0.118
        6  0.11971   0.001 -0.000 -0.002  0.001 -0.118  1.000
 **********
 **   20 **HESSE        3000
 **********
 COVARIANCE MATRIX CALCULATED SUCCESSFULLY
 FCN=1009.46 FROM HESSE     STATUS=OK             40 CALLS         144 TOTAL
                     EDM=2.51761e-06    STRATEGY= 2      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                INTERNAL      INTERNAL  
  NO.   NAME      VALUE            ERROR       STEP SIZE       VALUE   
   1  A_bkg        9.39625e-03   5.06190e-03   1.51268e-05   9.39639e-03
   2  A_sigD_blind   3.73319e-03   1.58230e-03   4.75968e-06   3.73320e-03
   3  N_bkg        7.37743e+04   3.70325e+02   3.73212e-07  -1.56536e+00
   4  N_sigD       4.56861e+05   7.21266e+02   3.71284e-07  -1.55728e+00
   5  bkg_delta_slope  -4.18236e-05   1.09298e-04   3.33625e-08  -4.18236e-06
   6  sigD_delta_mean  -8.34614e-02   1.49440e-02   4.60442e-06  -8.34624e-03
 EXTERNAL ERROR MATRIX.    NDIM=  48    NPAR=  6    ERR DEF=1
  2.562e-05 -1.949e-06  1.725e-03 -1.716e-03  7.097e-08  7.461e-08 
 -1.949e-06  2.504e-06 -1.980e-03  1.981e-03 -1.149e-08 -1.125e-08 
  1.725e-03 -1.980e-03  1.371e+05 -6.336e+04  1.470e-04 -1.161e-02 
 -1.716e-03  1.981e-03 -6.336e+04  5.202e+05 -1.468e-04  1.163e-02 
  7.097e-08 -1.149e-08  1.470e-04 -1.468e-04  1.195e-08 -1.935e-07 
  7.461e-08 -1.125e-08 -1.161e-02  1.163e-02 -1.935e-07  2.233e-04 
 PARAMETER  CORRELATION COEFFICIENTS  
       NO.  GLOBAL      1      2      3      4      5      6
        1  0.26843   1.000 -0.243  0.001 -0.000  0.128  0.001
        2  0.24600  -0.243  1.000 -0.003  0.002 -0.066 -0.000
        3  0.23727   0.001 -0.003  1.000 -0.237  0.004 -0.002
        4  0.23723  -0.000  0.002 -0.237  1.000 -0.002  0.001
        5  0.17848   0.128 -0.066  0.004 -0.002  1.000 -0.118
        6  0.11971   0.001 -0.000 -0.002  0.001 -0.118  1.000
start Plotting-------
[#1] INFO:Plotting -- RooTreeData::plotOn: plotting 264758 events out of 530131 total events
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF) directly selected PDF components: (pdf_bkg)
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF) indirectly selected PDF components: (tag_plus,tag_minus,pdf_bkg_plus,bkg_plus,bkg_tmp_plus,bkg_slope_plus,bkg_frac_plus,bkg_lowmass_plus,bkg_LMslope_plus,N_bkg_plus,pdf_bkg_minus,bkg_minus,bkg_tmp_minus,bkg_slope_minus,bkg_frac_minus,bkg_lowmass_minus,bkg_LMslope_minus,N_bkg_minus)
[#1] INFO:Plotting -- RooAbsReal::plotOn(totPDF) plot on Dplus_M represents a slice in (Dplus_ID)
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF) directly selected PDF components: (pdf_bkg)
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF) indirectly selected PDF components: (tag_plus,tag_minus,pdf_bkg_plus,bkg_plus,bkg_tmp_plus,bkg_slope_plus,bkg_frac_plus,bkg_lowmass_plus,bkg_LMslope_plus,N_bkg_plus,pdf_bkg_minus,bkg_minus,bkg_tmp_minus,bkg_slope_minus,bkg_frac_minus,bkg_lowmass_minus,bkg_LMslope_minus,N_bkg_minus)
[#1] INFO:Plotting -- RooAbsReal::plotOn(totPDF) plot on Dplus_M represents a slice in (Dplus_ID)
[#1] INFO:Plotting -- RooAbsReal::plotOn(totPDF) plot on Dplus_M represents a slice in (Dplus_ID)
[#1] INFO:Plotting -- RooTreeData::plotOn: plotting 264758 events out of 530131 total events
[#1] INFO:Plotting -- RooTreeData::plotOn: plotting 265373 events out of 530131 total events
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF) directly selected PDF components: (pdf_bkg)
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF) indirectly selected PDF components: (tag_plus,tag_minus,pdf_bkg_plus,bkg_plus,bkg_tmp_plus,bkg_slope_plus,bkg_frac_plus,bkg_lowmass_plus,bkg_LMslope_plus,N_bkg_plus,pdf_bkg_minus,bkg_minus,bkg_tmp_minus,bkg_slope_minus,bkg_frac_minus,bkg_lowmass_minus,bkg_LMslope_minus,N_bkg_minus)
[#1] INFO:Plotting -- RooAbsReal::plotOn(totPDF) plot on Dplus_M represents a slice in (Dplus_ID)
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF) directly selected PDF components: (pdf_bkg)
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF) indirectly selected PDF components: (tag_plus,tag_minus,pdf_bkg_plus,bkg_plus,bkg_tmp_plus,bkg_slope_plus,bkg_frac_plus,bkg_lowmass_plus,bkg_LMslope_plus,N_bkg_plus,pdf_bkg_minus,bkg_minus,bkg_tmp_minus,bkg_slope_minus,bkg_frac_minus,bkg_lowmass_minus,bkg_LMslope_minus,N_bkg_minus)
[#1] INFO:Plotting -- RooAbsReal::plotOn(totPDF) plot on Dplus_M represents a slice in (Dplus_ID)
[#1] INFO:Plotting -- RooAbsReal::plotOn(totPDF) plot on Dplus_M represents a slice in (Dplus_ID)
[#1] INFO:Plotting -- RooTreeData::plotOn: plotting 265373 events out of 530131 total events
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF_0) directly selected PDF components: (bkg_0)
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF_0) indirectly selected PDF components: (bkg_0_tmp,bkg_0_lowmass)
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF_0) directly selected PDF components: (bkg_0)
[#1] INFO:Plotting -- RooAbsPdf::plotOn(totPDF_0) indirectly selected PDF components: (bkg_0_tmp,bkg_0_lowmass)
 **********
 **   29 **MIGRAD        3000           1
 **********
 FIRST CALL TO USER FUNCTION AT NEW START POINT, WITH IFLAG=4.
 START MIGRAD MINIMIZATION.  STRATEGY  2.  CONVERGENCE WHEN EDM .LT. 1.00e-03
 COVARIANCE MATRIX CALCULATED SUCCESSFULLY
 FCN=1009.46 FROM HESSE     STATUS=OK             18 CALLS         163 TOTAL
                     EDM=1.00083e-06    STRATEGY= 2      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  A_bkg        9.39625e-03     fixed    
   2  A_sigD_blind   3.73319e-03     fixed    
   3  N_bkg        7.37743e+04   3.70300e+02   6.57853e-06  -1.43125e+02
   4  N_sigD       4.56861e+05   7.21226e+02   5.14890e-06   1.66448e+01
   5  bkg_delta_slope  -4.18236e-05     fixed    
   6  sigD_delta_mean  -8.34614e-02     fixed    
 MIGRAD MINIMIZATION HAS CONVERGED.
 MIGRAD WILL VERIFY CONVERGENCE AND ERROR MATRIX.
 COVARIANCE MATRIX CALCULATED SUCCESSFULLY
 FCN=1009.46 FROM MIGRAD    STATUS=CONVERGED      37 CALLS         182 TOTAL
                     EDM=1.2074e-12    STRATEGY= 2      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  A_bkg        9.39625e-03     fixed    
   2  A_sigD_blind   3.73319e-03     fixed    
   3  N_bkg        7.37747e+04   3.70320e+02   7.46424e-07  -1.00553e-01
   4  N_sigD       4.56861e+05   7.21261e+02   7.42568e-07   4.45566e-02
   5  bkg_delta_slope  -4.18236e-05     fixed    
   6  sigD_delta_mean  -8.34614e-02     fixed    
 EXTERNAL ERROR MATRIX.    NDIM=  48    NPAR=  2    ERR DEF=1
  1.371e+05 -6.336e+04 
 -6.336e+04  5.202e+05 
 PARAMETER  CORRELATION COEFFICIENTS  
       NO.  GLOBAL      3      4
        3  0.23720   1.000 -0.237
        4  0.23720  -0.237  1.000
 **********
 **   34 **HESSE        3000
 **********
 COVARIANCE MATRIX CALCULATED SUCCESSFULLY
 FCN=1009.46 FROM HESSE     STATUS=OK             12 CALLS         194 TOTAL
                     EDM=1.20743e-12    STRATEGY= 2      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                INTERNAL      INTERNAL  
  NO.   NAME      VALUE            ERROR       STEP SIZE       VALUE   
   1  A_bkg        9.39625e-03     fixed    
   2  A_sigD_blind   3.73319e-03     fixed    
   3  N_bkg        7.37747e+04   3.70323e+02   3.73212e-07  -1.56536e+00
   4  N_sigD       4.56861e+05   7.21266e+02   7.42568e-08  -1.55728e+00
   5  bkg_delta_slope  -4.18236e-05     fixed    
   6  sigD_delta_mean  -8.34614e-02     fixed    
 EXTERNAL ERROR MATRIX.    NDIM=  48    NPAR=  2    ERR DEF=1
  1.371e+05 -6.336e+04 
 -6.336e+04  5.202e+05 
 PARAMETER  CORRELATION COEFFICIENTS  
       NO.  GLOBAL      3      4
        3  0.23722   1.000 -0.237
        4  0.23722  -0.237  1.000
[#1] INFO:InputArguments -- Printing Yields
RooArgList:: = (N_sigD,N_bkg)
[#1] INFO:InputArguments -- yield in pdf: N_sigD 456861
[#1] INFO:InputArguments -- yield in pdf: N_bkg 73774.7
RooRealVar: Dplus_M
[#1] INFO:Eval -- Checking Likelihood normalization:  
[#1] INFO:Eval -- Yield of specie  Sum of Row in Matrix   Norm
[#1] INFO:Eval -- 456861 456866 0.999518
[#1] INFO:Eval -- 73774.7 74276 0.996143
[#1] INFO:Eval -- Calculating sWeight
******************************************************************************
*Tree    :ntp       : ntp                                                    *
*Entries :   530131 : Total =        23460008 bytes  File  Size =   19813160 *
*        :          : Tree compression factor =   1.18                       *
******************************************************************************
*Br    0 :Dplus_M   : Dplus_M/D                                              *
*Entries :   530131 : Total  Size=    4285724 bytes  File Size  =    3775396 *
*Baskets :      264 : Basket Size=      32000 bytes  Compression=   1.13     *
*............................................................................*
*Br    1 :Dplus_ID  : Dplus_ID/I                                             *
*Entries :   530131 : Total  Size=    2156162 bytes  File Size  =     247248 *
*Baskets :      264 : Basket Size=      32000 bytes  Compression=   8.66     *
*............................................................................*
*Br    2 :N_sigD_sw : N_sigD_sw/D                                            *
*Entries :   530131 : Total  Size=    4255200 bytes  File Size  =    3880086 *
*Baskets :      132 : Basket Size=      32000 bytes  Compression=   1.09     *
*............................................................................*
*Br    3 :L_N_sigD  : L_N_sigD/D                                             *
*Entries :   530131 : Total  Size=    4255062 bytes  File Size  =    3879954 *
*Baskets :      132 : Basket Size=      32000 bytes  Compression=   1.09     *
*............................................................................*
*Br    4 :N_bkg_sw  : N_bkg_sw/D                                             *
*Entries :   530131 : Total  Size=    4255062 bytes  File Size  =    4015304 *
*Baskets :      132 : Basket Size=      32000 bytes  Compression=   1.05     *
*............................................................................*
*Br    5 :L_N_bkg   : L_N_bkg/D                                              *
*Entries :   530131 : Total  Size=    4254924 bytes  File Size  =    4015172 *
*Baskets :      132 : Basket Size=      32000 bytes  Compression=   1.05     *
*............................................................................*
AT THE END : ../data/Dp2KS0pipLL_18_Dw_SPlot.root:/
N (Ds)   &   A (Ds) BLIND   &   N (D)   &   A (D)   &   N(bkg)   &   A(bkg)
2018 & $0.000 \pm 0.000$ & $0.000 \pm 0.000$ & $0.457 \pm 0.001$ & $0.373 \pm 0.000$ & $7.377 \pm 0.037$ & $0.940 \pm 0.000$
[#1] INFO:Eval -- RooRealVar::setRange(Dplus_M) new range named 'region_D_S' created with bounds [1848.300,1888.300]
0.299
[#1] INFO:Eval -- RooRealVar::setRange(Dplus_M) new range named 'region_D_B1' created with bounds [1808.300,1828.300]
0.188
[#1] INFO:Eval -- RooRealVar::setRange(Dplus_M) new range named 'region_D_B2' created with bounds [1908.300,1928.300]
0.119
[#1] INFO:Eval -- RooRealVar::setRange(Dplus_M) new range named 'region_Ds_S' created with bounds [1948.300,1988.300]
0.189
[#1] INFO:Eval -- RooRealVar::setRange(Dplus_M) new range named 'region_Ds_B1' created with bounds [1908.300,1928.300]
0.119
[#1] INFO:Eval -- RooRealVar::setRange(Dplus_M) new range named 'region_Ds_B2' created with bounds [2008.300,2028.300]
0.075
537.487  1009.456
(Double_t) 216.824
