A_bkg =  0.0023684 +/- 0.0015006 L(-1 - 1) 
A_sigD_blind = -0.00791369 +/- 0.00064800 L(-1 - 1) 
N_B =  1895756 +/- 29649 L(0 - 1e+09) 
N_S =  6829921 +/- 29862 L(0 - 1e+09) 
b =  0.60008 +/- 0.014526 L(0 - 1) 
c =  0.017101 +/- 0.0017792 L(-0.1 - 0.1) 
delta_j =  0.66684 +/- 0.059421 L(0 - 10) 
frac_g1 =  0.44481 +/- 0.017124 L(0 - 1) 
frac_g2 =  0.84451 +/- 0.015278 L(0 - 1) 
frac_j =  0.14205 +/- 0.0066516 L(0 - 1) 
gamma_j = -0.277387 +/- 0.035846 L(-1 - 1) 
mean_g1 =  0.0093242 +/- 0.00082485 L(-1 - 1) 
mean_g1b = -0.00367932 +/- 0.00082688 L(-1 - 1) 
mean_g2 = -0.00383635 +/- 0.0024126 L(-1 - 1) 
mean_g2b = -0.0172969 +/- 0.0023990 L(-1 - 1) 
mean_g3 =  0.021294 +/- 0.049336 L(-1 - 1) 
mean_g3b = -0.00274104 +/- 0.047718 L(-1 - 1) 
mean_j = -0.517086 +/- 0.054926 L(-1 - 1) 
sigma_1 =  0.18424 +/- 0.0017977 L(0 - 1) 
sigma_2 =  0.30446 +/- 0.0057792 L(0 - 1) 
sigma_3 =  0.60630 +/- 0.030300 L(0 - 1) 
sigma_j =  0.66132 +/- 0.053025 L(0 - 1) 
Status: OK 3
