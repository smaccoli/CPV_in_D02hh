#include <iostream>      
#include <stdio.h>       
#include <stdlib.h>      
#include <TString.h>         
#include <TFile.h>            
#include <TChain.h>      
#include <TTree.h>  
#include <TLorentzVector.h>   
#include "/home/LHCB/smaccoli/Tools.h"

void Dp2KS0pipLL_MCefficiency_KS(TString inputFile = "/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/pGun/Dp2KS0pipLL_2016_Dw/1635/pGun_Dp2KS0pipLL.root") {
  
  TFile * f;
  inputFile.ReplaceAll(".root","_ALL.root");
  f = TFile::Open(inputFile);
  if(!f) cout << "FILE " << inputFile << " DOES NOT EXIST!!" << endl;
  TTree * ntp;
  ntp = (TTree*)f->Get("MCDp2KS0pipLL/MCDecayTree");
  ntp->SetBranchStatus("*ID",1);
  ntp->SetBranchStatus("*Reconstructed",1);
  
  int Dplus_ID; ntp->SetBranchAddress("Dplus_ID", &Dplus_ID);
  int piplus_Reconstructed; ntp->SetBranchAddress("piplus_Reconstructed", &piplus_Reconstructed);
  int piminus_Reconstructed; ntp->SetBranchAddress("piminus_Reconstructed", &piminus_Reconstructed);

  double Dplus_TRUEP;// ntp->SetBranchAddress("Dplus_TRUEP", &Dplus_TRUEP);
  double Dplus_TRUEPT; ntp->SetBranchAddress("Dplus_TRUEPT", &Dplus_TRUEPT);
  double Dplus_ETA; ntp->SetBranchAddress("Dplus_ETA", &Dplus_ETA);
  double Dplus_PHI; ntp->SetBranchAddress("Dplus_PHI", &Dplus_PHI);
  double Dplus_TRUEP_X; ntp->SetBranchAddress("Dplus_TRUEP_X", &Dplus_TRUEP_X);
  double Dplus_TRUEP_Y; ntp->SetBranchAddress("Dplus_TRUEP_Y", &Dplus_TRUEP_Y);
  double Dplus_TRUEP_Z; ntp->SetBranchAddress("Dplus_TRUEP_Z", &Dplus_TRUEP_Z);
  double hplus_TRUEP;// ntp->SetBranchAddress("hplus_TRUEP", &hplus_TRUEP);
  double hplus_TRUEPT; ntp->SetBranchAddress("hplus_TRUEPT", &hplus_TRUEPT);
  double hplus_ETA; ntp->SetBranchAddress("hplus_ETA", &hplus_ETA);
  double hplus_PHI; ntp->SetBranchAddress("hplus_PHI", &hplus_PHI);
  double hplus_TRUEP_X; ntp->SetBranchAddress("hplus_TRUEP_X", &hplus_TRUEP_X);
  double hplus_TRUEP_Y; ntp->SetBranchAddress("hplus_TRUEP_Y", &hplus_TRUEP_Y);
  double hplus_TRUEP_Z; ntp->SetBranchAddress("hplus_TRUEP_Z", &hplus_TRUEP_Z);
  double KS0_TRUEP;// ntp->SetBranchAddress("KS0_TRUEP", &KS0_TRUEP);
  double KS0_TRUEPT; ntp->SetBranchAddress("KS0_TRUEPT", &KS0_TRUEPT);
  double KS0_ETA; ntp->SetBranchAddress("KS0_ETA", &KS0_ETA);
  double KS0_PHI; ntp->SetBranchAddress("KS0_PHI", &KS0_PHI);
  double KS0_TRUEP_X; ntp->SetBranchAddress("KS0_TRUEP_X", &KS0_TRUEP_X);
  double KS0_TRUEP_Y; ntp->SetBranchAddress("KS0_TRUEP_Y", &KS0_TRUEP_Y);
  double KS0_TRUEP_Z; ntp->SetBranchAddress("KS0_TRUEP_Z", &KS0_TRUEP_Z);
  double KS0_TRUEORIGINVERTEX_Z; ntp->SetBranchAddress("KS0_TRUEORIGINVERTEX_Z", &KS0_TRUEORIGINVERTEX_Z);
  double KS0_TRUEENDVERTEX_Z; ntp->SetBranchAddress("KS0_TRUEENDVERTEX_Z", &KS0_TRUEENDVERTEX_Z);
  

  bool HarmonizationCut_1D = true;
  bool HarmonizationCut_1D_X = true;
  bool HarmonizationCut_2D_hD = true;
  bool HarmonizationCut = true;

  //Useful variables

  int Nreco_plus = 0;
  int Nreco_minus = 0;
  int Ngen = 0;
 
  for(int i = 0; i < ntp->GetEntries(); i++) {
    ntp->GetEntry(i);
    

    Dplus_TRUEP = sqrt(pow(Dplus_TRUEP_X,2)+pow(Dplus_TRUEP_Y,2)+pow(Dplus_TRUEP_Z,2));
    hplus_TRUEP = sqrt(pow(hplus_TRUEP_X,2)+pow(hplus_TRUEP_Y,2)+pow(hplus_TRUEP_Z,2));
    KS0_TRUEP = sqrt(pow(KS0_TRUEP_X,2)+pow(KS0_TRUEP_Y,2)+pow(KS0_TRUEP_Z,2));

    HarmonizationCut_1D = Dplus_TRUEP>14e3&&Dplus_TRUEP<250e3&&Dplus_TRUEPT>3.5e3&&Dplus_TRUEPT<14e3&&Dplus_ETA>2&&Dplus_ETA<4.5 && hplus_TRUEP>5e3&&hplus_TRUEP<150e3&&hplus_TRUEPT>1.5e3&&hplus_TRUEPT<8.5e3&&hplus_ETA>1.9&&hplus_ETA<4.7;
    HarmonizationCut_1D_X = KS0_TRUEPT > 2.2e3 && KS0_TRUEP > 25e3;
    HarmonizationCut_2D_hD = hplus_TRUEPT < (Dplus_TRUEPT-3.4e3)*6.5/7+1.2e3/*similar to Dplus_TRUEPT - 2.2e3 but better*/ && hplus_TRUEPT > (Dplus_TRUEPT-7.5e3);
    HarmonizationCut_2D_hD = HarmonizationCut_2D_hD && (
							(hplus_PHI > Dplus_PHI-2.96/6.51 && hplus_PHI < Dplus_PHI+2.96/6.51)
							||
							hplus_PHI > Dplus_PHI+2.96/0.51
							||
							hplus_PHI < Dplus_PHI-2.96/0.51
							);
    HarmonizationCut_2D_hD = HarmonizationCut_2D_hD && hplus_ETA < Dplus_ETA + 0.35 && hplus_ETA > Dplus_ETA - 0.45 && hplus_ETA > 2*Dplus_ETA - 4.4;
    HarmonizationCut = HarmonizationCut_1D && HarmonizationCut_1D_X && HarmonizationCut_2D_hD; 
    if(!HarmonizationCut) continue;
    
    Ngen++;

    if (piplus_Reconstructed == 1 && piminus_Reconstructed == 1) {
      if (Dplus_ID > 0)
	Nreco_plus++;
      else
	Nreco_minus++;
    }
    
  }

  cout << "Yields:" << '\t' << Nreco_plus << '\t' << Nreco_minus << '\t' << Ngen << endl;
}


int main(int argc, char * argv[]) {
  TString inputFile = argv[1];
  
  Dp2KS0pipLL_MCefficiency_KS(inputFile);
  
}
