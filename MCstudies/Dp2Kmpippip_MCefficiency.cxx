#include <iostream>      
#include <stdio.h>       
#include <stdlib.h>      
#include <TString.h>         
#include <TFile.h>            
#include <TChain.h>      
#include <TTree.h>  
#include <TLorentzVector.h>   
#include "/home/LHCB/smaccoli/Tools.h"
#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"
#include <TRandom3.h>  

void Dp2Kmpippip_MCefficiency(TString inputFile = "/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/pGun/Dp2Kmpippip_2016_Up/6001/pGun_Dp2Kmpippip.root", Bool_t use_ALL = 0) {

  TRandom3 *r = new TRandom3(inputFile.Hash());
  
  Bool_t save_RECOtuple = 0;//1 or 0 if weights already calculed
  Bool_t save_GENtuple = 0;
  Bool_t save_GENtuple_withRECO_Kpi = 0;
  save_GENtuple = save_GENtuple || save_GENtuple_withRECO_Kpi;
  Bool_t use_Iw_max = 1;

  TString decay = "Dp2Kmpippip";
  TString add_string = "_pGun";
  if(use_ALL)
    add_string += "_ALL";
  TString year = inputFile.Contains("2016") ? "16" : "tobedefined";
  TString polarity = inputFile.Contains("Up") ? "Up" : "Dw";

  myH * h_PT_vs_PT = new myH("../reweight/D2Kpipi_to_D2KSpi/weights/"+year+"_"+polarity+add_string+"_0.myH");
  myH * h_Dp_PTvsETA = new myH("../reweight/D2Kpipi_to_D2KSpi/weights/"+year+"_"+polarity+add_string+"_1.myH");
  myH * h_Dp_PHI = new myH("../reweight/D2Kpipi_to_D2KSpi/weights/"+year+"_"+polarity+add_string+"_2.myH");
  myH * h_hp_PTvsETA = new myH("../reweight/D2Kpipi_to_D2KSpi/weights/"+year+"_"+polarity+add_string+"_3.myH");
  myH * h_hp_PHI = new myH("../reweight/D2Kpipi_to_D2KSpi/weights/"+year+"_"+polarity+add_string+"_4.myH");

  TFile * f;
  if(use_ALL)
    inputFile.ReplaceAll(".root","_ALL.root");
  f = TFile::Open(inputFile);
  if(!f) cout << "FILE " << inputFile << " DOES NOT EXIST!!" << endl;
  TTree * ntp;
  ntp = (TTree*)f->Get("MCDp2Kmpippip/MCDecayTree");
  cout << "tot GEN decays: " << ntp->GetEntries() << endl;
  //ntp->SetBranchStatus("*",0);
  
  int Dplus_ID; ntp->SetBranchAddress("Dplus_ID", &Dplus_ID);
  int Kminus_Reconstructed; ntp->SetBranchAddress("Kminus_Reconstructed", &Kminus_Reconstructed);
  int piplus_Reconstructed; ntp->SetBranchAddress("piplus_Reconstructed", &piplus_Reconstructed);
  int hplus_Reconstructed; ntp->SetBranchAddress("hplus_Reconstructed", &hplus_Reconstructed);

  double Dplus_TRUEP;// ntp->SetBranchAddress("Dplus_TRUEP", &Dplus_TRUEP);
  double Dplus_TRUEPT; ntp->SetBranchAddress("Dplus_TRUEPT", &Dplus_TRUEPT);
  double Dplus_ETA; ntp->SetBranchAddress("Dplus_ETA", &Dplus_ETA);
  double Dplus_PHI; ntp->SetBranchAddress("Dplus_PHI", &Dplus_PHI);
  double Dplus_TRUEP_X; ntp->SetBranchAddress("Dplus_TRUEP_X", &Dplus_TRUEP_X);
  double Dplus_TRUEP_Y; ntp->SetBranchAddress("Dplus_TRUEP_Y", &Dplus_TRUEP_Y);
  double Dplus_TRUEP_Z; ntp->SetBranchAddress("Dplus_TRUEP_Z", &Dplus_TRUEP_Z);
  double hplus_TRUEP;// ntp->SetBranchAddress("hplus_TRUEP", &hplus_TRUEP);
  double hplus_TRUEPT; ntp->SetBranchAddress("hplus_TRUEPT", &hplus_TRUEPT);
  double hplus_ETA; ntp->SetBranchAddress("hplus_ETA", &hplus_ETA);
  double hplus_PHI; ntp->SetBranchAddress("hplus_PHI", &hplus_PHI);
  double hplus_TRUEP_X; ntp->SetBranchAddress("hplus_TRUEP_X", &hplus_TRUEP_X);
  double hplus_TRUEP_Y; ntp->SetBranchAddress("hplus_TRUEP_Y", &hplus_TRUEP_Y);
  double hplus_TRUEP_Z; ntp->SetBranchAddress("hplus_TRUEP_Z", &hplus_TRUEP_Z);
  double Kminus_TRUEP;// ntp->SetBranchAddress("Kminus_TRUEP", &Kminus_TRUEP);
  double Kminus_TRUEPT; ntp->SetBranchAddress("Kminus_TRUEPT", &Kminus_TRUEPT);
  double Kminus_ETA; ntp->SetBranchAddress("Kminus_ETA", &Kminus_ETA);
  double Kminus_PHI; ntp->SetBranchAddress("Kminus_PHI", &Kminus_PHI);
  double Kminus_TRUEP_X; ntp->SetBranchAddress("Kminus_TRUEP_X", &Kminus_TRUEP_X);
  double Kminus_TRUEP_Y; ntp->SetBranchAddress("Kminus_TRUEP_Y", &Kminus_TRUEP_Y);
  double Kminus_TRUEP_Z; ntp->SetBranchAddress("Kminus_TRUEP_Z", &Kminus_TRUEP_Z);
  double piplus_TRUEP;// ntp->SetBranchAddress("piplus_TRUEP", &piplus_TRUEP);
  double piplus_TRUEPT; ntp->SetBranchAddress("piplus_TRUEPT", &piplus_TRUEPT);
  double piplus_ETA; ntp->SetBranchAddress("piplus_ETA", &piplus_ETA);
  double piplus_PHI; ntp->SetBranchAddress("piplus_PHI", &piplus_PHI);
  double piplus_TRUEP_X; ntp->SetBranchAddress("piplus_TRUEP_X", &piplus_TRUEP_X);
  double piplus_TRUEP_Y; ntp->SetBranchAddress("piplus_TRUEP_Y", &piplus_TRUEP_Y);
  double piplus_TRUEP_Z; ntp->SetBranchAddress("piplus_TRUEP_Z", &piplus_TRUEP_Z);

  double X_TRUEPT;// ntpOut->Branch("X_PT",&X_TRUEPT);//
  double X_ETA;// ntpOut->Branch("X_ETA",&X_ETA);//
  double X_PHI;// ntpOut->Branch("X_PHI",&X_PHI);//
  double X_TRUEP;// ntpOut->Branch("X_P",&X_TRUEP);//  
  double X_TRUEM;// ntpOut->Branch("X_M",&X_TRUEM);//  
  double X_TRUEpipi_M;// ntpOut->Branch("X_pipi_M",&X_TRUEpipi_M);//  
  double X_PT;// ntpOut->Branch("X_PT",&X_PT);//
  double X_P;// ntpOut->Branch("X_P",&X_P);//  

  bool HarmonizationCut = true;
  bool HarmonizationCut_trueLabels = true;
  bool HarmonizationCut_falseLabels = true;

  //Useful variables
  double Kminus_E, piplus_E, hplus_E;
  TLorentzVector Kminus, piplus, hplus, X;
  
  int Ngen = 0;
  int Ngen_plus = 0;
  int Ngen_minus = 0;
  int Kpi_Nreco_plus = 0;
  int Kpi_Nreco_minus = 0; 
  int K_Nreco_plus = 0;
  int K_Nreco_minus = 0;
  int pi_Nreco_plus = 0;
  int pi_Nreco_minus = 0;
  int h_Nreco_plus = 0;
  int h_Nreco_minus = 0;
  int Kpih_Nreco_plus = 0;
  int Kpih_Nreco_minus = 0; 
 
  int Ngen_W = 0;
  int Ngen_plus_W = 0;
  int Ngen_minus_W = 0;
  int Kpi_Nreco_plus_W = 0;
  int Kpi_Nreco_minus_W = 0; 
  int K_Nreco_plus_W = 0;
  int K_Nreco_minus_W = 0;
  int pi_Nreco_plus_W = 0;
  int pi_Nreco_minus_W = 0;
  int h_Nreco_plus_W = 0;
  int h_Nreco_minus_W = 0;
  int Kpih_Nreco_plus_W = 0;
  int Kpih_Nreco_minus_W = 0; 
  int Ngen_sumW2 = 0;
  int Ngen_plus_sumW2 = 0;
  int Ngen_minus_sumW2 = 0;
  int Kpi_Nreco_plus_sumW2 = 0;
  int Kpi_Nreco_minus_sumW2 = 0; 
  int K_Nreco_plus_sumW2 = 0;
  int K_Nreco_minus_sumW2 = 0;
  int pi_Nreco_plus_sumW2 = 0;
  int pi_Nreco_minus_sumW2 = 0;
  int h_Nreco_plus_sumW2 = 0;
  int h_Nreco_minus_sumW2 = 0;
  int Kpih_Nreco_plus_sumW2 = 0;
  int Kpih_Nreco_minus_sumW2 = 0; 
 
  vector < double > vec_Dplus_TRUEPT;
  double Iw;
  bool trueLabels;
  
  TFile *f_new;
  TTree* t;
  inputFile.ReplaceAll("/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/pGun/","../data/");
  inputFile.ReplaceAll("Dw/","Dw_");  
  inputFile.ReplaceAll("Up/","Up_");  
  inputFile.ReplaceAll("/pGun","_GEN_pGun");
  if(save_GENtuple_withRECO_Kpi)
    inputFile.ReplaceAll("GEN","GEN_withRECO_Kpi");
  if(save_GENtuple) {
    ntp->SetBranchStatus("*",1);
    f_new = new TFile(inputFile,"RECREATE");
    t = (TTree*) ntp->CloneTree(0);
    t->SetName("ntp");
    t->SetTitle("ntp");
  }

 
  for(int i = 0; i < ntp->GetEntries(); i++) {
    ntp->GetEntry(i);
    
    //begin harmonization cuts
    Dplus_TRUEP = sqrt(pow(Dplus_TRUEP_X,2)+pow(Dplus_TRUEP_Y,2)+pow(Dplus_TRUEP_Z,2));
    hplus_TRUEP = sqrt(pow(hplus_TRUEP_X,2)+pow(hplus_TRUEP_Y,2)+pow(hplus_TRUEP_Z,2));
    Kminus_TRUEP = sqrt(pow(Kminus_TRUEP_X,2)+pow(Kminus_TRUEP_Y,2)+pow(Kminus_TRUEP_Z,2));
    piplus_TRUEP = sqrt(pow(piplus_TRUEP_X,2)+pow(piplus_TRUEP_Y,2)+pow(piplus_TRUEP_Z,2));


    Kminus_E = sqrt(Kminus_TRUEP_X*Kminus_TRUEP_X+Kminus_TRUEP_Y*Kminus_TRUEP_Y+Kminus_TRUEP_Z*Kminus_TRUEP_Z+PdgMass::mKplus_PDG*PdgMass::mKplus_PDG);
    Kminus.SetPxPyPzE(Kminus_TRUEP_X,Kminus_TRUEP_Y,Kminus_TRUEP_Z,Kminus_E);
    piplus_E = sqrt(piplus_TRUEP_X*piplus_TRUEP_X+piplus_TRUEP_Y*piplus_TRUEP_Y+piplus_TRUEP_Z*piplus_TRUEP_Z+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG);
    piplus.SetPxPyPzE(piplus_TRUEP_X,piplus_TRUEP_Y,piplus_TRUEP_Z,piplus_E);
    X = Kminus + piplus;
    
    X_TRUEPT = X.Pt();
    X_ETA = X.Eta();
    X_PHI = X.Phi();
    X_TRUEP = X.P();
    X_TRUEM = X.M();

    //true labels
    HarmonizationCut_trueLabels = Kminus_TRUEP>5e3 && Kminus_TRUEPT>800 && piplus_TRUEP>5e3 && piplus_TRUEPT>800 && X_TRUEP > 30e3 && Dplus_TRUEPT - hplus_TRUEPT > 2280 && piplus_TRUEPT + Kminus_TRUEPT > 2280 && TMath::Abs(Dplus_PHI-hplus_PHI) < 0.4 && TMath::Abs(Dplus_ETA-hplus_ETA) < 0.4 && Dplus_TRUEPT>4.2e3 && Dplus_TRUEPT<11e3 && Dplus_ETA>2.3 && Dplus_ETA<4.2 && hplus_TRUEPT>1.6e3 && hplus_TRUEPT<6e3 && hplus_ETA>2.2 && hplus_ETA<4.2 && piplus_TRUEPT < 5.5e3 && Kminus_TRUEPT < 6e3 && piplus_ETA > 2.2 && piplus_ETA < 4.3 && Kminus_ETA > 2.2 && Kminus_ETA < 4.3;   
 
    HarmonizationCut_trueLabels = HarmonizationCut_trueLabels && (X_TRUEPT > 2.08e3 && X_TRUEPT < 9.08e3 && X_ETA > 2.4 && X_ETA < 4.3) && (Dplus_ETA < 4.1 && hplus_ETA < 4.1);
    

    //false labels
    hplus_E = sqrt(hplus_TRUEP_X*hplus_TRUEP_X+hplus_TRUEP_Y*hplus_TRUEP_Y+hplus_TRUEP_Z*hplus_TRUEP_Z+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG);
    hplus.SetPxPyPzE(hplus_TRUEP_X,hplus_TRUEP_Y,hplus_TRUEP_Z,hplus_E);
    X = Kminus + hplus;
    
    X_TRUEPT = X.Pt();
    X_ETA = X.Eta();
    X_PHI = X.Phi();
    X_TRUEP = X.P();
    X_TRUEM = X.M();

    HarmonizationCut_falseLabels = Kminus_TRUEP>5e3 && Kminus_TRUEPT>800 && hplus_TRUEP>5e3 && hplus_TRUEPT>800 && X_TRUEP > 30e3 && Dplus_TRUEPT - piplus_TRUEPT > 2280 && hplus_TRUEPT + Kminus_TRUEPT > 2280 && TMath::Abs(Dplus_PHI-piplus_PHI) < 0.4 && TMath::Abs(Dplus_ETA-piplus_ETA) < 0.4 && Dplus_TRUEPT>4.2e3 && Dplus_TRUEPT<11e3 && Dplus_ETA>2.3 && Dplus_ETA<4.2 && piplus_TRUEPT>1.6e3 && piplus_TRUEPT<6e3 && piplus_ETA>2.2 && piplus_ETA<4.2 && hplus_TRUEPT < 5.5e3 && Kminus_TRUEPT < 6e3 && hplus_ETA > 2.2 && hplus_ETA < 4.3 && Kminus_ETA > 2.2 && Kminus_ETA < 4.3;   
 
    HarmonizationCut_falseLabels = HarmonizationCut_falseLabels && (X_TRUEPT > 2.08e3 && X_TRUEPT < 9.08e3 && X_ETA > 2.4 && X_ETA < 4.3) && (Dplus_ETA < 4.1 && piplus_ETA < 4.1);
    
    
    HarmonizationCut = HarmonizationCut_trueLabels || HarmonizationCut_falseLabels;
    
    if(!HarmonizationCut) continue;  
    //end harmonization cuts
    

    if(save_GENtuple_withRECO_Kpi) {
      if(!(hplus_Reconstructed==1 || piplus_Reconstructed==1))
	continue;
    }


    if(HarmonizationCut_trueLabels && HarmonizationCut_falseLabels) {
      if (r->Uniform(0,1) <= 0.5)
    	HarmonizationCut_trueLabels = 1;
      else
    	HarmonizationCut_trueLabels = 0;	
    }

    if (HarmonizationCut_trueLabels) {
      if (Dplus_ID > 0) {
    	Ngen_plus++;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_plus++;
    	if (piplus_Reconstructed == 1) 
    	  pi_Nreco_plus++;
    	if (hplus_Reconstructed == 1) 
    	  h_Nreco_plus++;
      
    	if (Kminus_Reconstructed == 1 && piplus_Reconstructed == 1) {
    	  Kpi_Nreco_plus++;
    	  if (hplus_Reconstructed == 1) {
    	    Kpih_Nreco_plus++;
    	    vec_Dplus_TRUEPT.push_back(Dplus_TRUEPT);	
    	  }
    	}

      }
      else {
    	Ngen_minus++;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_minus++;
    	if (piplus_Reconstructed == 1) 
    	  pi_Nreco_minus++;
    	if (hplus_Reconstructed == 1) 
    	  h_Nreco_minus++;
      
    	if (Kminus_Reconstructed == 1 && piplus_Reconstructed == 1) {
    	  Kpi_Nreco_minus++;
    	  if (hplus_Reconstructed == 1) { 
    	    Kpih_Nreco_minus++;
    	    vec_Dplus_TRUEPT.push_back(Dplus_TRUEPT);	
    	  }
    	}
      }
    
      // weighted yields
      Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({Dplus_TRUEPT,hplus_TRUEPT}));
      Iw *= h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({Dplus_TRUEPT,Dplus_ETA}));
      Iw *=  h_Dp_PHI->getBC(h_Dp_PHI->find({Dplus_PHI}));
      Iw *= h_hp_PTvsETA->getBC(h_hp_PTvsETA->find({hplus_TRUEPT,hplus_ETA}));
      Iw *= h_hp_PHI->getBC(h_hp_PHI->find({hplus_PHI}));
      
      if (use_Iw_max)
	if (Iw > 1e3)
	  Iw = 0;

      if (Dplus_ID > 0) {
    	Ngen_plus_W += Iw;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_plus_W += Iw;
    	if (piplus_Reconstructed == 1) 
    	  pi_Nreco_plus_W += Iw;
    	if (hplus_Reconstructed == 1) 
    	  h_Nreco_plus_W += Iw;
      
    	if (Kminus_Reconstructed == 1 && piplus_Reconstructed == 1) {
    	  Kpi_Nreco_plus_W += Iw;
    	  if (hplus_Reconstructed == 1) {
    	    Kpih_Nreco_plus_W += Iw;
    	  }
    	}

      }
      else {
    	Ngen_minus_W += Iw;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_minus_W += Iw;
    	if (piplus_Reconstructed == 1) 
    	  pi_Nreco_minus_W += Iw;
    	if (hplus_Reconstructed == 1) 
    	  h_Nreco_minus_W += Iw;
      
    	if (Kminus_Reconstructed == 1 && piplus_Reconstructed == 1) {
    	  Kpi_Nreco_minus_W += Iw;
    	  if (hplus_Reconstructed == 1) { 
    	    Kpih_Nreco_minus_W += Iw;
    	  }
    	}
      }
   


      if (Dplus_ID > 0) {
    	Ngen_plus_sumW2 += Iw*Iw;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_plus_sumW2 += Iw*Iw;
    	if (piplus_Reconstructed == 1) 
    	  pi_Nreco_plus_sumW2 += Iw*Iw;
    	if (hplus_Reconstructed == 1) 
    	  h_Nreco_plus_sumW2 += Iw*Iw;
      
    	if (Kminus_Reconstructed == 1 && piplus_Reconstructed == 1) {
    	  Kpi_Nreco_plus_sumW2 += Iw*Iw;
    	  if (hplus_Reconstructed == 1) {
    	    Kpih_Nreco_plus_sumW2 += Iw*Iw;
    	  }
    	}

      }
      else {
    	Ngen_minus_sumW2 += Iw*Iw;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_minus_sumW2 += Iw*Iw;
    	if (piplus_Reconstructed == 1) 
    	  pi_Nreco_minus_sumW2 += Iw*Iw;
    	if (hplus_Reconstructed == 1) 
    	  h_Nreco_minus_sumW2 += Iw*Iw;
      
    	if (Kminus_Reconstructed == 1 && piplus_Reconstructed == 1) {
    	  Kpi_Nreco_minus_sumW2 += Iw*Iw;
    	  if (hplus_Reconstructed == 1) { 
    	    Kpih_Nreco_minus_sumW2 += Iw*Iw;
    	  }
    	}
      }
    
    }
    else {

      if (Dplus_ID > 0) {
    	Ngen_plus++;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_plus++;
    	if (hplus_Reconstructed == 1) 
    	  pi_Nreco_plus++;
    	if (piplus_Reconstructed == 1) 
    	  h_Nreco_plus++;
      
    	if (Kminus_Reconstructed == 1 && hplus_Reconstructed == 1) {
    	  Kpi_Nreco_plus++;
    	  if (piplus_Reconstructed == 1) {
    	    Kpih_Nreco_plus++;
    	    vec_Dplus_TRUEPT.push_back(Dplus_TRUEPT);	
    	  }
    	}

      }
      else {
    	Ngen_minus++;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_minus++;
    	if (hplus_Reconstructed == 1) 
    	  pi_Nreco_minus++;
    	if (piplus_Reconstructed == 1) 
    	  h_Nreco_minus++;
      
    	if (Kminus_Reconstructed == 1 && hplus_Reconstructed == 1) {
    	  Kpi_Nreco_minus++;
    	  if (piplus_Reconstructed == 1) { 
    	    Kpih_Nreco_minus++;
    	    vec_Dplus_TRUEPT.push_back(Dplus_TRUEPT);	
    	  }
    	}
      }
    
      // weighted yields
      Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({Dplus_TRUEPT,piplus_TRUEPT}));
      Iw *= h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({Dplus_TRUEPT,Dplus_ETA}));
      Iw *=  h_Dp_PHI->getBC(h_Dp_PHI->find({Dplus_PHI}));
      Iw *= h_hp_PTvsETA->getBC(h_hp_PTvsETA->find({piplus_TRUEPT,piplus_ETA}));
      Iw *= h_hp_PHI->getBC(h_hp_PHI->find({piplus_PHI}));
      
      if (use_Iw_max)
	if (Iw > 1e3) 
	  Iw = 0;

      if (Dplus_ID > 0) {
    	Ngen_plus_W += Iw;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_plus_W += Iw;
    	if (hplus_Reconstructed == 1) 
    	  pi_Nreco_plus_W += Iw;
    	if (piplus_Reconstructed == 1) 
    	  h_Nreco_plus_W += Iw;
      
    	if (Kminus_Reconstructed == 1 && hplus_Reconstructed == 1) {
    	  Kpi_Nreco_plus_W += Iw;
    	  if (piplus_Reconstructed == 1) {
    	    Kpih_Nreco_plus_W += Iw;
    	  }
    	}

      }
      else {
    	Ngen_minus_W += Iw;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_minus_W += Iw;
    	if (hplus_Reconstructed == 1) 
    	  pi_Nreco_minus_W += Iw;
    	if (piplus_Reconstructed == 1) 
    	  h_Nreco_minus_W += Iw;
      
    	if (Kminus_Reconstructed == 1 && hplus_Reconstructed == 1) {
    	  Kpi_Nreco_minus_W += Iw;
    	  if (piplus_Reconstructed == 1) { 
    	    Kpih_Nreco_minus_W += Iw;
    	  }
    	}
      }
   


      if (Dplus_ID > 0) {
    	Ngen_plus_sumW2 += Iw*Iw;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_plus_sumW2 += Iw*Iw;
    	if (hplus_Reconstructed == 1) 
    	  pi_Nreco_plus_sumW2 += Iw*Iw;
    	if (piplus_Reconstructed == 1) 
    	  h_Nreco_plus_sumW2 += Iw*Iw;
      
    	if (Kminus_Reconstructed == 1 && hplus_Reconstructed == 1) {
    	  Kpi_Nreco_plus_sumW2 += Iw*Iw;
    	  if (piplus_Reconstructed == 1) {
    	    Kpih_Nreco_plus_sumW2 += Iw*Iw;
    	  }
    	}

      }
      else {
    	Ngen_minus_sumW2 += Iw*Iw;
    	if (Kminus_Reconstructed == 1) 
    	  K_Nreco_minus_sumW2 += Iw*Iw;
    	if (hplus_Reconstructed == 1) 
    	  pi_Nreco_minus_sumW2 += Iw*Iw;
    	if (piplus_Reconstructed == 1) 
    	  h_Nreco_minus_sumW2 += Iw*Iw;
      
    	if (Kminus_Reconstructed == 1 && hplus_Reconstructed == 1) {
    	  Kpi_Nreco_minus_sumW2 += Iw*Iw;
    	  if (piplus_Reconstructed == 1) { 
    	    Kpih_Nreco_minus_sumW2 += Iw*Iw;
    	  }
    	}
      }
    
 
    }

    

    if(save_GENtuple)
      t->Fill();
  }
  
  if(save_GENtuple) {
    t->Write();
    f_new->Close();
  }
  
  Ngen = Ngen_plus + Ngen_minus;

  cout << ":Kpi:" << '\t' << Kpi_Nreco_plus << '\t' << Kpi_Nreco_minus << endl;
  cout << ":K:" << '\t' << K_Nreco_plus << '\t' << K_Nreco_minus << endl;
  cout << ":pi:" << '\t' << pi_Nreco_plus << '\t' << pi_Nreco_minus << endl;
  cout << ":h:" << '\t' << h_Nreco_plus << '\t' << h_Nreco_minus << endl;
  cout << ":Kpih:" << '\t' << Kpih_Nreco_plus << '\t' << Kpih_Nreco_minus << endl;
  cout << ":Ngen:" << '\t' << Ngen_plus << '\t' << Ngen_minus << endl;

  cout << ":KpiW:" << '\t' << Kpi_Nreco_plus_W << '\t' << Kpi_Nreco_minus_W << '\t' << Kpi_Nreco_plus_sumW2 << '\t' << Kpi_Nreco_minus_sumW2 << endl;
  cout << ":KW:" << '\t' << K_Nreco_plus_W << '\t' << K_Nreco_minus_W << '\t' << K_Nreco_plus_sumW2 << '\t' << K_Nreco_minus_sumW2 << endl;
  cout << ":piW:" << '\t' << pi_Nreco_plus_W << '\t' << pi_Nreco_minus_W << '\t' << pi_Nreco_plus_sumW2 << '\t' << pi_Nreco_minus_sumW2 << endl;
  cout << ":hW:" << '\t' << h_Nreco_plus_W << '\t' << h_Nreco_minus_W << '\t' << h_Nreco_plus_sumW2 << '\t' << h_Nreco_minus_sumW2 << endl;
  cout << ":KpihW:" << '\t' << Kpih_Nreco_plus_W << '\t' << Kpih_Nreco_minus_W << '\t' << Kpih_Nreco_plus_sumW2 << '\t' << Kpih_Nreco_minus_sumW2 << endl;
  cout << ":NgenW:" << '\t' << Ngen_plus_W << '\t' << Ngen_minus_W << '\t' << Ngen_plus_sumW2 << '\t' << Ngen_minus_sumW2 << endl;


  //----------- RECO ------
  
  ntp = (TTree*)f->Get("Dp2Kmpippip/DecayTree");
  cout << "tot RECO decays: " << ntp->GetEntries() << endl;
//  ntp->SetBranchStatus("*",0);
  
  ntp->SetBranchAddress("Dplus_ID", &Dplus_ID);

  ntp->SetBranchAddress("Dplus_PT", &Dplus_TRUEPT);
  ntp->SetBranchAddress("Dplus_ETA", &Dplus_ETA);
  ntp->SetBranchAddress("Dplus_PHI", &Dplus_PHI);
  ntp->SetBranchAddress("Dplus_PX", &Dplus_TRUEP_X);
  ntp->SetBranchAddress("Dplus_PY", &Dplus_TRUEP_Y);
  ntp->SetBranchAddress("Dplus_TRUEP_Z", &Dplus_TRUEP_Z);
  ntp->SetBranchAddress("hplus_PT", &hplus_TRUEPT);
  ntp->SetBranchAddress("hplus_ETA", &hplus_ETA);
  ntp->SetBranchAddress("hplus_PHI", &hplus_PHI);
  ntp->SetBranchAddress("hplus_PX", &hplus_TRUEP_X);
  ntp->SetBranchAddress("hplus_PY", &hplus_TRUEP_Y);
  ntp->SetBranchAddress("hplus_PZ", &hplus_TRUEP_Z);
  ntp->SetBranchAddress("Kminus_PT", &Kminus_TRUEPT);
  ntp->SetBranchAddress("Kminus_ETA", &Kminus_ETA);
  ntp->SetBranchAddress("Kminus_PHI", &Kminus_PHI);
  ntp->SetBranchAddress("Kminus_PX", &Kminus_TRUEP_X);
  ntp->SetBranchAddress("Kminus_PY", &Kminus_TRUEP_Y);
  ntp->SetBranchAddress("Kminus_PZ", &Kminus_TRUEP_Z);
  ntp->SetBranchAddress("piplus_PT", &piplus_TRUEPT);
  ntp->SetBranchAddress("piplus_ETA", &piplus_ETA);
  ntp->SetBranchAddress("piplus_PHI", &piplus_PHI);
  ntp->SetBranchAddress("piplus_PX", &piplus_TRUEP_X);
  ntp->SetBranchAddress("piplus_PY", &piplus_TRUEP_Y);
  ntp->SetBranchAddress("piplus_PZ", &piplus_TRUEP_Z);

  //TruthMatching variables
  Int_t Dplus_TRUEID = 0;
  Int_t hplus_TRUEID = 0;
  Int_t Kminus_TRUEID = 0;
  Int_t piplus_TRUEID = 0;
  Int_t hplus_MC_MOTHER_ID = 0;
  Int_t Kminus_MC_MOTHER_ID = 0;
  Int_t piplus_MC_MOTHER_ID = 0;
  Int_t hplus_MC_MOTHER_KEY = 0;
  Int_t Kminus_MC_MOTHER_KEY = 0;
  Int_t piplus_MC_MOTHER_KEY = 0;
  Int_t Dplus_BKGCAT = 0;
  Bool_t hplus_TRUEISSTABLE;
  Bool_t piplus_TRUEISSTABLE;
  Bool_t Kminus_TRUEISSTABLE;

  ntp->SetBranchAddress("Dplus_TRUEID",&Dplus_TRUEID);
  ntp->SetBranchAddress("Dplus_BKGCAT",&Dplus_BKGCAT);
  ntp->SetBranchAddress("hplus_TRUEID",&hplus_TRUEID);
  ntp->SetBranchAddress("Kminus_TRUEID",&Kminus_TRUEID);
  ntp->SetBranchAddress("piplus_TRUEID",&piplus_TRUEID);
  ntp->SetBranchAddress("hplus_MC_MOTHER_ID",&hplus_MC_MOTHER_ID);
  ntp->SetBranchAddress("Kminus_MC_MOTHER_ID",&Kminus_MC_MOTHER_ID);
  ntp->SetBranchAddress("piplus_MC_MOTHER_ID",&piplus_MC_MOTHER_ID);
  ntp->SetBranchAddress("hplus_MC_MOTHER_KEY",&hplus_MC_MOTHER_KEY);
  ntp->SetBranchAddress("Kminus_MC_MOTHER_KEY",&Kminus_MC_MOTHER_KEY);
  ntp->SetBranchAddress("piplus_MC_MOTHER_KEY",&piplus_MC_MOTHER_KEY);
  ntp->SetBranchAddress("hplus_TRUEISSTABLE", &hplus_TRUEISSTABLE);
  ntp->SetBranchAddress("piplus_TRUEISSTABLE", &piplus_TRUEISSTABLE);
  ntp->SetBranchAddress("Kminus_TRUEISSTABLE", &Kminus_TRUEISSTABLE);
  
  int Nreco = 0;
  int Nreco_plus = 0;
  int Nreco_minus = 0;
  int Nreco_W = 0;
  int Nreco_plus_W = 0;
  int Nreco_minus_W = 0;
  int Nreco_sumW2 = 0;
  int Nreco_plus_sumW2 = 0;
  int Nreco_minus_sumW2 = 0;
  
  ntp->SetBranchStatus("*",1);
  inputFile.ReplaceAll("GEN","RECO");
  if (save_RECOtuple) {
    f_new = new TFile(inputFile,"RECREATE");
    t = (TTree*) ntp->CloneTree(0);
    t->SetName("ntp");
    t->SetTitle("ntp");
    t->Branch("trueLabels",&HarmonizationCut_trueLabels);
  }
  
  size_t k0 = 0;
  for(int i = 0; i < ntp->GetEntries(); i++) {
    ntp->GetEntry(i);
    
    if (Dplus_BKGCAT != 0) continue;   
    if (abs(Dplus_TRUEID)!=PdgCode::D) continue;
    if (abs(hplus_TRUEID)!=PdgCode::Pi) continue;
    if (abs(piplus_TRUEID)!=PdgCode::Pi) continue;
    if (abs(Kminus_TRUEID)!=PdgCode::K) continue;
    if (hplus_MC_MOTHER_ID!=Dplus_TRUEID) continue;
    if (Kminus_MC_MOTHER_ID!=Dplus_TRUEID) continue;
    if (piplus_MC_MOTHER_ID!=Dplus_TRUEID) continue;
    if (!(hplus_MC_MOTHER_KEY==Kminus_MC_MOTHER_KEY && hplus_MC_MOTHER_KEY==piplus_MC_MOTHER_KEY)) continue;
    
    
    //begin harmonization cuts
    Dplus_TRUEP = sqrt(pow(Dplus_TRUEP_X,2)+pow(Dplus_TRUEP_Y,2)+pow(Dplus_TRUEP_Z,2));
    hplus_TRUEP = sqrt(pow(hplus_TRUEP_X,2)+pow(hplus_TRUEP_Y,2)+pow(hplus_TRUEP_Z,2));
    Kminus_TRUEP = sqrt(pow(Kminus_TRUEP_X,2)+pow(Kminus_TRUEP_Y,2)+pow(Kminus_TRUEP_Z,2));
    piplus_TRUEP = sqrt(pow(piplus_TRUEP_X,2)+pow(piplus_TRUEP_Y,2)+pow(piplus_TRUEP_Z,2));


    Kminus_E = sqrt(Kminus_TRUEP_X*Kminus_TRUEP_X+Kminus_TRUEP_Y*Kminus_TRUEP_Y+Kminus_TRUEP_Z*Kminus_TRUEP_Z+PdgMass::mKplus_PDG*PdgMass::mKplus_PDG);
    Kminus.SetPxPyPzE(Kminus_TRUEP_X,Kminus_TRUEP_Y,Kminus_TRUEP_Z,Kminus_E);
    piplus_E = sqrt(piplus_TRUEP_X*piplus_TRUEP_X+piplus_TRUEP_Y*piplus_TRUEP_Y+piplus_TRUEP_Z*piplus_TRUEP_Z+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG);
    piplus.SetPxPyPzE(piplus_TRUEP_X,piplus_TRUEP_Y,piplus_TRUEP_Z,piplus_E);
    X = Kminus + piplus;
    
    X_TRUEPT = X.Pt();
    X_ETA = X.Eta();
    X_PHI = X.Phi();
    X_TRUEP = X.P();
    X_TRUEM = X.M();

    //true labels
    HarmonizationCut_trueLabels = Kminus_TRUEP>5e3 && Kminus_TRUEPT>800 && piplus_TRUEP>5e3 && piplus_TRUEPT>800 && X_TRUEP > 30e3 && Dplus_TRUEPT - hplus_TRUEPT > 2280 && piplus_TRUEPT + Kminus_TRUEPT > 2280 && TMath::Abs(Dplus_PHI-hplus_PHI) < 0.4 && TMath::Abs(Dplus_ETA-hplus_ETA) < 0.4 && Dplus_TRUEPT>4.2e3 && Dplus_TRUEPT<11e3 && Dplus_ETA>2.3 && Dplus_ETA<4.2 && hplus_TRUEPT>1.6e3 && hplus_TRUEPT<6e3 && hplus_ETA>2.2 && hplus_ETA<4.2 && piplus_TRUEPT < 5.5e3 && Kminus_TRUEPT < 6e3 && piplus_ETA > 2.2 && piplus_ETA < 4.3 && Kminus_ETA > 2.2 && Kminus_ETA < 4.3;   
 
    HarmonizationCut_trueLabels = HarmonizationCut_trueLabels && (X_TRUEPT > 2.08e3 && X_TRUEPT < 9.08e3 && X_ETA > 2.4 && X_ETA < 4.3) && (Dplus_ETA < 4.1 && hplus_ETA < 4.1);
    

    //false labels
    hplus_E = sqrt(hplus_TRUEP_X*hplus_TRUEP_X+hplus_TRUEP_Y*hplus_TRUEP_Y+hplus_TRUEP_Z*hplus_TRUEP_Z+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG);
    hplus.SetPxPyPzE(hplus_TRUEP_X,hplus_TRUEP_Y,hplus_TRUEP_Z,hplus_E);
    X = Kminus + hplus;
    
    X_TRUEPT = X.Pt();
    X_ETA = X.Eta();
    X_PHI = X.Phi();
    X_TRUEP = X.P();
    X_TRUEM = X.M();

    HarmonizationCut_falseLabels = Kminus_TRUEP>5e3 && Kminus_TRUEPT>800 && hplus_TRUEP>5e3 && hplus_TRUEPT>800 && X_TRUEP > 30e3 && Dplus_TRUEPT - piplus_TRUEPT > 2280 && hplus_TRUEPT + Kminus_TRUEPT > 2280 && TMath::Abs(Dplus_PHI-piplus_PHI) < 0.4 && TMath::Abs(Dplus_ETA-piplus_ETA) < 0.4 && Dplus_TRUEPT>4.2e3 && Dplus_TRUEPT<11e3 && Dplus_ETA>2.3 && Dplus_ETA<4.2 && piplus_TRUEPT>1.6e3 && piplus_TRUEPT<6e3 && piplus_ETA>2.2 && piplus_ETA<4.2 && hplus_TRUEPT < 5.5e3 && Kminus_TRUEPT < 6e3 && hplus_ETA > 2.2 && hplus_ETA < 4.3 && Kminus_ETA > 2.2 && Kminus_ETA < 4.3;   
 
    HarmonizationCut_falseLabels = HarmonizationCut_falseLabels && (X_TRUEPT > 2.08e3 && X_TRUEPT < 9.08e3 && X_ETA > 2.4 && X_ETA < 4.3) && (Dplus_ETA < 4.1 && piplus_ETA < 4.1);
    
    
    HarmonizationCut = HarmonizationCut_trueLabels || HarmonizationCut_falseLabels;
    
    if(!HarmonizationCut) continue;  
    //end harmonization cuts
  
    if(HarmonizationCut_trueLabels && HarmonizationCut_falseLabels) {
      if (r->Uniform(0,1) <= 0.5)
	HarmonizationCut_trueLabels = 1;
      else
	HarmonizationCut_trueLabels = 0;	
    }

 
    if (HarmonizationCut_trueLabels) {
      if (Dplus_ID > 0) {
	Nreco_plus++;
      }
      else {
	Nreco_minus++;
      }
  
      // weighted yields
      Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({Dplus_TRUEPT,hplus_TRUEPT}));
      Iw *= h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({Dplus_TRUEPT,Dplus_ETA}));
      Iw *=  h_Dp_PHI->getBC(h_Dp_PHI->find({Dplus_PHI}));
      Iw *= h_hp_PTvsETA->getBC(h_hp_PTvsETA->find({hplus_TRUEPT,hplus_ETA}));
      Iw *= h_hp_PHI->getBC(h_hp_PHI->find({hplus_PHI}));
      
      if (use_Iw_max)
	if (Iw > 1e3) 
	  Iw = 0;


      if (Dplus_ID > 0) {
	Nreco_plus_W += Iw;
      }
      else {
	Nreco_minus_W += Iw;
      
      }
  
      if (Dplus_ID > 0) {
	Nreco_plus_sumW2 += Iw*Iw;
      }
      else {
	Nreco_minus_sumW2 += Iw*Iw;
      
      }

    }
    else {
      if (Dplus_ID > 0) {
	Nreco_plus++;
      }
      else {
	Nreco_minus++;
      
      }
  
      // weighted yields
      Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({Dplus_TRUEPT,piplus_TRUEPT}));
      Iw *= h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({Dplus_TRUEPT,Dplus_ETA}));
      Iw *=  h_Dp_PHI->getBC(h_Dp_PHI->find({Dplus_PHI}));
      Iw *= h_hp_PTvsETA->getBC(h_hp_PTvsETA->find({piplus_TRUEPT,piplus_ETA}));
      Iw *= h_hp_PHI->getBC(h_hp_PHI->find({piplus_PHI}));
      
      if (use_Iw_max)
	if (Iw > 1e3) 
	  Iw = 0;


      if (Dplus_ID > 0) {
	Nreco_plus_W += Iw;
      }
      else {
	Nreco_minus_W += Iw;
      
      }
  
      if (Dplus_ID > 0) {
	Nreco_plus_sumW2 += Iw*Iw;
      }
      else {
	Nreco_minus_sumW2 += Iw*Iw;
      
      }
    }

    if(save_RECOtuple)
      t->Fill();
  }

  if(save_RECOtuple) {
    t->Write();
    f_new->Close();
  }
 
  Nreco = Nreco_plus + Nreco_minus;

  cout << ":Nreco:" << '\t' << Nreco_plus << '\t' << Nreco_minus << endl;
  cout << ":NrecoW:" << '\t' << Nreco_plus_W << '\t' << Nreco_minus_W << '\t' << Nreco_plus_sumW2 << '\t' << Nreco_minus_sumW2 << endl;


  
}

int main(int argc, char * argv[]) {
  TString inputFile = argv[1];
  Bool_t use_ALL = atoi(argv[2]);
  
  Dp2Kmpippip_MCefficiency(inputFile,use_ALL);
  
}
