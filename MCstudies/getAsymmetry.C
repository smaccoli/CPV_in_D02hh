#include <iostream>      
#include <stdio.h>       
#include <stdlib.h>      
#include <TString.h>         
#include <TFile.h>            
#include <TChain.h>      
#include <TTree.h>  
#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"


void getAsymmetry(TString year = "16", TString polarity = "Dw") {
  
  TString decay = "Dp2Kmpippip";
  TString add_string = "_pGun_ALL";

  myH * h_PT_vs_PT = new myH("../reweight/D2Kpipi_to_D2KSpi/weights/_25e3_"+year+"_"+polarity+add_string+"_0.myH");
  myH * h_Dp_PTvsETA = new myH("../reweight/D2Kpipi_to_D2KSpi/weights/_25e3_"+year+"_"+polarity+add_string+"_1.myH");
  myH * h_Dp_PHI = new myH("../reweight/D2Kpipi_to_D2KSpi/weights/_25e3_"+year+"_"+polarity+add_string+"_2.myH");
  myH * h_hp_PTvsETA = new myH("../reweight/D2Kpipi_to_D2KSpi/weights/_25e3_"+year+"_"+polarity+add_string+"_3.myH");
  myH * h_hp_PHI = new myH("../reweight/D2Kpipi_to_D2KSpi/weights/_25e3_"+year+"_"+polarity+add_string+"_4.myH");


  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/_25e3/"+decay+"_"+year+"_"+polarity+add_string+".root");
  toweight_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/_25e3/"+decay+"_"+year+"_"+polarity+add_string+"_rew.root");
  toweight_tree->SetBranchStatus("*",0);
  toweight_tree->SetBranchStatus("Iw",1);
  toweight_tree->SetBranchStatus("Dplus_ID",1);
  
  double Iw;
  toweight_tree->SetBranchAddress("Iw",&Iw);
  int Dplus_ID;
  toweight_tree->SetBranchAddress("Dplus_ID",&Dplus_ID);


  double Dplus_PT; toweight_tree->SetBranchAddress("Dplus_PT", &Dplus_PT);
  double Dplus_ETA; toweight_tree->SetBranchAddress("Dplus_ETA", &Dplus_ETA);
  double Dplus_PHI; toweight_tree->SetBranchAddress("Dplus_PHI", &Dplus_PHI);
  double hplus_PT; toweight_tree->SetBranchAddress("hplus_PT", &hplus_PT);
  double hplus_ETA; toweight_tree->SetBranchAddress("hplus_ETA", &hplus_ETA);
  double hplus_PHI; toweight_tree->SetBranchAddress("hplus_PHI", &hplus_PHI);



  double Nplus = 0;
  double Nminus = 0;
  double Nplus_err = 0;
  double Nminus_err = 0;
  
  
  for(Long64_t i = 0; i < 
	toweight_tree->GetEntries();
      i++){
    toweight_tree->GetEntry(i);


    Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({Dplus_PT,hplus_PT}));
    Iw *= h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({Dplus_PT,Dplus_ETA}));
    Iw *=  h_Dp_PHI->getBC(h_Dp_PHI->find({Dplus_PHI}));
    Iw *= h_hp_PTvsETA->getBC(h_hp_PTvsETA->find({hplus_PT,hplus_ETA}));
    Iw *= h_hp_PHI->getBC(h_hp_PHI->find({hplus_PHI}));
    
    if(Iw > 15)
      Iw = 0;
    

    if (Dplus_ID > 0) {
      Nplus += Iw;
      Nplus_err += Iw*Iw;
    }
    else {
      Nminus += Iw;
      Nminus_err += Iw*Iw;
    }

  }

  cout << Nplus << '\t' << Nminus << '\t' << Nplus_err << '\t' << Nminus_err << endl;

  Nplus_err = sqrt(Nplus_err);
  Nminus_err = sqrt(Nminus_err);


  getAsym(Nplus,Nminus,Nplus_err,Nminus_err);

}
