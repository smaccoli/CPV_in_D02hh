#!/bin/bash
isPGun=1

DIR=$PWD
mode=D2Kpipi

g++ -Wall -o _Dp2Kmpippip_MCefficiency_withW Dp2Kmpippip_MCefficiency_withW.cxx `root-config --cflags --glibs` `gsl-config --cflags --libs`

for year in $(echo 2016); do
    for pol in $(echo MagDown); do
#    for pol in $(echo MagUp); do
	inputLists=$(echo "/home/LHCB-T3/smaccoli/CPV_in_D02hh/lists/Dp2Kmpippip/list_${year}_${pol}_*.dat")

	if [ $isPGun -eq 1 ]
	then
	    inputLists=$(echo "/home/LHCB-T3/smaccoli/CPV_in_D02hh/lists/pGun/Dp2Kmpippip/list_${year}_${pol}_*.dat")
	fi

	for inputList in $(ls -I logs $inputLists); do
	    idList=$(echo $inputList | tr "/" " " | awk '{print $7}' | tr "_" " " | awk '{print $4}' | tr "." " " | awk '{print $1}')
	    if [ $isPGun -eq 1 ]
	    then
		idList=$(echo $inputList | tr "/" " " | awk '{print $8}' | tr "_" " " | awk '{print $4}' | tr "." " " | awk '{print $1}')
	    fi
	    echo "#!/bin/sh" > exec/$mode.$year.$pol.$idList.sh
	    echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> exec/$mode.$year.$pol.$idList.sh 
	    echo "cd $DIR" >> exec/$mode.$year.$pol.$idList.sh
	    echo $inputList
	    for inputFile in $(cat $inputList); do
		id=$(echo $inputFile | tr "/" " " | awk '{print $10}')
		if [ $isPGun -eq 1 ]
		then
		    id=$(echo $inputFile | tr "/" " " | awk '{print $8}')
		fi
		echo "./_Dp2Kmpippip_MCefficiency_withW \"$inputFile\" > logs/withW.$mode.$year.$pol.$id.txt" >> exec/$mode.$year.$pol.$idList.sh
	    done
	  
	    chmod +x exec/$mode.$year.$pol.$idList.sh
	    python condor_make_template.py condor_template.sub $mode.$year.$pol.$idList
	    condor_submit exec/condor_template.$mode.$year.$pol.$idList.sub -batch-name $mode$year$pol$idList
  
	    #break
	done
	#break
    done
    #break
done
