#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

int main(int argc, char * argv[]) {
  double Nplus = atof(argv[1]);
  double Nminus = atof(argv[2]);
  double Nplus_err2 = atof(argv[3]);
  double Nminus_err2 = atof(argv[4]);
  
  getAsym(Nplus,Nminus,Nplus_err2,Nminus_err2,1);
}
