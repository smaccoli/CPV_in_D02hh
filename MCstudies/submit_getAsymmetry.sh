#!/bin/bash
decay=$1         
particles=$2     
echo $decay $particles
#yields=$(cat logs_ALL/$decay*.2016.MagDown.* | grep ":$particles:" | awk '{sumP+=$2} {sumM+=$3} {sumPe+=$4} {sumMe+=$5}  END {print sumP, sumM, sumPe, sumMe}')
#yields=$(cat logs_ALL/$decay*.2016.MagUp.* | grep ":$particles:" | awk '{sumP+=$2} {sumM+=$3} {sumPe+=$4} {sumMe+=$5}  END {print sumP, sumM, sumPe, sumMe}')
yields=$(cat logs/$decay*.2016.MagDown.* | grep ":$particles:" | awk '{sumP+=$2} {sumM+=$3} {sumPe+=$4} {sumMe+=$5}  END {print sumP, sumM, sumPe, sumMe}')
#yields=$(cat logs/$decay*.2016.MagUp.* | grep ":$particles:" | awk '{sumP+=$2} {sumM+=$3} {sumPe+=$4} {sumMe+=$5}  END {print sumP, sumM, sumPe, sumMe}')
echo $yields
Nplus=$(echo $yields | awk '{print $1}')
Nminus=$(echo $yields | awk '{print $2}')
NplusE2=$(echo $yields | awk '{print $3}')
NminusE2=$(echo $yields | awk '{print $4}')
./_getAsymmetry $Nplus $Nminus $NplusE2 $NminusE2
