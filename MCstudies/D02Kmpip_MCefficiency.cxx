#include <iostream>      
#include <stdio.h>       
#include <stdlib.h>      
#include <TString.h>         
#include <TFile.h>            
#include <TChain.h>      
#include <TTree.h>  
#include <TLorentzVector.h>   
#include "/home/LHCB/smaccoli/Tools.h"
#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void D02Kmpip_MCefficiency(TString inputFile = "/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/pGun/Dst2D0pi_D02Kpi_2016_Dw/0/pGun_Dst2D0pi_D02Kpi.root", Bool_t use_ALL = 0) {

  Bool_t save_RECOtuple = 0;//1 or 0 if weights already calculed
  Bool_t save_GENtuple = 0;
  
  TString decay = "D02Kmpip";
  TString add_string = "_pGun";
   if(use_ALL)
     add_string += "_ALL";
   TString year = inputFile.Contains("16") ? "16" : "tobedefined";
   TString polarity = inputFile.Contains("Up") ? "Up" : "Dw";

  myH * h_pip_PTvsETA = new myH("../reweight/D02Kpi_to_D2KpipiW/weights/"+year+"_"+polarity+add_string+"_0.myH");
  myH * h_pip_PHI = new myH("../reweight/D02Kpi_to_D2KpipiW/weights/"+year+"_"+polarity+add_string+"_1.myH");
  myH * h_Km_PTvsETA = new myH("../reweight/D02Kpi_to_D2KpipiW/weights/"+year+"_"+polarity+add_string+"_2.myH");
  myH * h_Km_PHI = new myH("../reweight/D02Kpi_to_D2KpipiW/weights/"+year+"_"+polarity+add_string+"_3.myH");

  TFile * f;
  if(use_ALL)
    inputFile.ReplaceAll(".root","_ALL.root");
  f = TFile::Open(inputFile);
  if(!f) cout << "FILE " << inputFile << " DOES NOT EXIST!!" << endl;
  TTree * ntp;
  ntp = (TTree*)f->Get("MCDstp2D0pi/MCDecayTree");
  //ntp->SetBranchStatus("*",0);
  
  int Dst_ID; ntp->SetBranchAddress("Dst_ID", &Dst_ID);
  int P2_Reconstructed; ntp->SetBranchAddress("P2_Reconstructed", &P2_Reconstructed);
  int P1_Reconstructed; ntp->SetBranchAddress("P1_Reconstructed", &P1_Reconstructed);
  int sPi_Reconstructed; ntp->SetBranchAddress("sPi_Reconstructed", &sPi_Reconstructed);
  
  double Dst_TRUEP;// ntp->SetBranchAddress("Dst_TRUEP", &Dst_TRUEP);
  double Dst_TRUEPT; ntp->SetBranchAddress("Dst_TRUEPT", &Dst_TRUEPT);
  double Dst_ETA; ntp->SetBranchAddress("Dst_ETA", &Dst_ETA);
  double Dst_PHI; ntp->SetBranchAddress("Dst_PHI", &Dst_PHI);
  double Dst_TRUEP_X; ntp->SetBranchAddress("Dst_TRUEP_X", &Dst_TRUEP_X);
  double Dst_TRUEP_Y; ntp->SetBranchAddress("Dst_TRUEP_Y", &Dst_TRUEP_Y);
  double Dst_TRUEP_Z; ntp->SetBranchAddress("Dst_TRUEP_Z", &Dst_TRUEP_Z);
  double D0_TRUEP;// ntp->SetBranchAddress("D0_TRUEP", &D0_TRUEP);
  double D0_TRUEPT; //ntp->SetBranchAddress("D0_PT", &D0_PT);
  double D0_ETA; ntp->SetBranchAddress("D0_ETA", &D0_ETA);
  double D0_PHI; ntp->SetBranchAddress("D0_PHI", &D0_PHI);
  double D0_TRUEP_X; ntp->SetBranchAddress("D0_TRUEP_X", &D0_TRUEP_X);
  double D0_TRUEP_Y; ntp->SetBranchAddress("D0_TRUEP_Y", &D0_TRUEP_Y);
  double D0_TRUEP_Z; ntp->SetBranchAddress("D0_TRUEP_Z", &D0_TRUEP_Z);
  double sPi_TRUEP;// ntp->SetBranchAddress("sPi_TRUEP", &sPi_TRUEP);
  double sPi_TRUEPT; ntp->SetBranchAddress("sPi_TRUEPT", &sPi_TRUEPT);
  double sPi_ETA; ntp->SetBranchAddress("sPi_ETA", &sPi_ETA);
  double sPi_PHI; ntp->SetBranchAddress("sPi_PHI", &sPi_PHI);
  double sPi_TRUEP_X; ntp->SetBranchAddress("sPi_TRUEP_X", &sPi_TRUEP_X);
  double sPi_TRUEP_Y; ntp->SetBranchAddress("sPi_TRUEP_Y", &sPi_TRUEP_Y);
  double sPi_TRUEP_Z; ntp->SetBranchAddress("sPi_TRUEP_Z", &sPi_TRUEP_Z);
  double P2_TRUEP;// ntp->SetBranchAddress("P2_TRUEP", &P2_TRUEP);
  double P2_TRUEPT; ntp->SetBranchAddress("P2_TRUEPT", &P2_TRUEPT);
  double P2_ETA; ntp->SetBranchAddress("P2_ETA", &P2_ETA);
  double P2_PHI; ntp->SetBranchAddress("P2_PHI", &P2_PHI);
  double P2_TRUEP_X; ntp->SetBranchAddress("P2_TRUEP_X", &P2_TRUEP_X);
  double P2_TRUEP_Y; ntp->SetBranchAddress("P2_TRUEP_Y", &P2_TRUEP_Y);
  double P2_TRUEP_Z; ntp->SetBranchAddress("P2_TRUEP_Z", &P2_TRUEP_Z);
  double P1_TRUEP;// ntp->SetBranchAddress("P1_TRUEP", &P1_TRUEP);
  double P1_TRUEPT; ntp->SetBranchAddress("P1_TRUEPT", &P1_TRUEPT);
  double P1_ETA; ntp->SetBranchAddress("P1_ETA", &P1_ETA);
  double P1_PHI; ntp->SetBranchAddress("P1_PHI", &P1_PHI);
  double P1_TRUEP_X; ntp->SetBranchAddress("P1_TRUEP_X", &P1_TRUEP_X);
  double P1_TRUEP_Y; ntp->SetBranchAddress("P1_TRUEP_Y", &P1_TRUEP_Y);
  double P1_TRUEP_Z; ntp->SetBranchAddress("P1_TRUEP_Z", &P1_TRUEP_Z);
  
  bool HarmonizationCut = true;

  int Ngen = 0;
  int Ngen_plus = 0;
  int Ngen_minus = 0;
  int Kpi_Nreco_plus = 0;
  int Kpi_Nreco_minus = 0; 
  int K_Nreco_plus = 0;
  int K_Nreco_minus = 0;
  int pi_Nreco_plus = 0;
  int pi_Nreco_minus = 0;
  int h_Nreco_plus = 0;
  int h_Nreco_minus = 0;
  int Kpih_Nreco_plus = 0;
  int Kpih_Nreco_minus = 0; 

  int Ngen_W = 0;
  int Ngen_plus_W = 0;
  int Ngen_minus_W = 0;
  int Kpi_Nreco_plus_W = 0;
  int Kpi_Nreco_minus_W = 0; 
  int K_Nreco_plus_W = 0;
  int K_Nreco_minus_W = 0;
  int pi_Nreco_plus_W = 0;
  int pi_Nreco_minus_W = 0;
  int h_Nreco_plus_W = 0;
  int h_Nreco_minus_W = 0;
  int Kpih_Nreco_plus_W = 0;
  int Kpih_Nreco_minus_W = 0; 
  int Ngen_sumW2 = 0;
  int Ngen_plus_sumW2 = 0;
  int Ngen_minus_sumW2 = 0;
  int Kpi_Nreco_plus_sumW2 = 0;
  int Kpi_Nreco_minus_sumW2 = 0; 
  int K_Nreco_plus_sumW2 = 0;
  int K_Nreco_minus_sumW2 = 0;
  int pi_Nreco_plus_sumW2 = 0;
  int pi_Nreco_minus_sumW2 = 0;
  int h_Nreco_plus_sumW2 = 0;
  int h_Nreco_minus_sumW2 = 0;
  int Kpih_Nreco_plus_sumW2 = 0;
  int Kpih_Nreco_minus_sumW2 = 0; 

  vector < double > vec_Dst_TRUEPT;
  double Iw;

  TFile *f_new;
  TTree* t;
  inputFile.ReplaceAll("/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/pGun/","../data/");
  inputFile.ReplaceAll("Dw/","Dw_");  
  inputFile.ReplaceAll("Up/","Up_");  
  inputFile.ReplaceAll("/pGun","_GEN_pGun");
  if (save_GENtuple) {
    ntp->SetBranchStatus("*",1);
    f_new = new TFile(inputFile,"RECREATE");
    t = (TTree*) ntp->CloneTree(0);
    t->SetName("ntp");
    t->SetTitle("ntp");
  }
  for(int i = 0; i < ntp->GetEntries(); i++) {
    ntp->GetEntry(i);
    

    Dst_TRUEP = sqrt(pow(Dst_TRUEP_X,2)+pow(Dst_TRUEP_Y,2)+pow(Dst_TRUEP_Z,2));
    D0_TRUEP = sqrt(pow(D0_TRUEP_X,2)+pow(D0_TRUEP_Y,2)+pow(D0_TRUEP_Z,2));
    D0_TRUEPT = sqrt(pow(D0_TRUEP_X,2)+pow(D0_TRUEP_Y,2));
    sPi_TRUEP = sqrt(pow(sPi_TRUEP_X,2)+pow(sPi_TRUEP_Y,2)+pow(sPi_TRUEP_Z,2));
    P2_TRUEP = sqrt(pow(P2_TRUEP_X,2)+pow(P2_TRUEP_Y,2)+pow(P2_TRUEP_Z,2));
    P1_TRUEP = sqrt(pow(P1_TRUEP_X,2)+pow(P1_TRUEP_Y,2)+pow(P1_TRUEP_Z,2));

    HarmonizationCut = P2_TRUEP>5e3 && P2_TRUEPT>800 && P1_TRUEP>5e3 && P1_TRUEPT>800 && D0_TRUEP > 30e3;
    HarmonizationCut = HarmonizationCut && Dst_TRUEPT > 2.28e3 && Dst_TRUEPT < 9.5e3 && sPi_TRUEPT > 100 && sPi_TRUEPT < 800 && Dst_ETA > 2.4 && Dst_ETA < 4.3 && sPi_ETA > 2.4 && sPi_ETA < 4.3;
    
    HarmonizationCut = HarmonizationCut && P1_TRUEPT + P2_TRUEPT > 2280 && P1_TRUEPT < 5.5e3 && P2_TRUEPT < 6e3 && P1_ETA > 2.2 && P1_ETA < 4.3 && P2_ETA > 2.2 && P2_ETA < 4.3;

    HarmonizationCut = HarmonizationCut && (D0_TRUEPT > 2.08e3 && D0_TRUEPT < 9.08e3 && D0_ETA > 2.4 && D0_ETA < 4.3);

    if(!HarmonizationCut) continue;

    if (Dst_ID > 0) {
      Ngen_plus++;
      if (P2_Reconstructed == 1) 
	K_Nreco_plus++;
      if (P1_Reconstructed == 1) 
	pi_Nreco_plus++;
      if (sPi_Reconstructed == 1) 
	h_Nreco_plus++;
      
      if (P2_Reconstructed == 1 && P1_Reconstructed == 1) {
	Kpi_Nreco_plus++;
	if (sPi_Reconstructed == 1) {
	  Kpih_Nreco_plus++;
	  vec_Dst_TRUEPT.push_back(Dst_TRUEPT);	
	}
      }

    }
    else {
      Ngen_minus++;
      if (P2_Reconstructed == 1) 
	K_Nreco_minus++;
      if (P1_Reconstructed == 1) 
	pi_Nreco_minus++;
      if (sPi_Reconstructed == 1) 
	h_Nreco_minus++;
      
      if (P2_Reconstructed == 1 && P1_Reconstructed == 1) {
	Kpi_Nreco_minus++;
	if (sPi_Reconstructed == 1) { 
	  Kpih_Nreco_minus++;
	  vec_Dst_TRUEPT.push_back(Dst_TRUEPT);	
	}
      }
    }


    // weighted yields
    Iw = h_pip_PTvsETA->getBC(h_pip_PTvsETA->find({P1_TRUEPT,P1_ETA}));
    Iw *=  h_pip_PHI->getBC(h_pip_PHI->find({P1_PHI}));
    Iw *= h_Km_PTvsETA->getBC(h_Km_PTvsETA->find({P2_TRUEPT,P2_ETA}));
    Iw *= h_Km_PHI->getBC(h_Km_PHI->find({P2_PHI}));
      
    if (Iw > 500)
      Iw = 0;

    if (Dst_ID > 0) {
      Ngen_plus_W += Iw;
      if (P2_Reconstructed == 1) 
	K_Nreco_plus_W += Iw;
      if (P1_Reconstructed == 1) 
	pi_Nreco_plus_W += Iw;
      if (sPi_Reconstructed == 1) 
	h_Nreco_plus_W += Iw;
      
      if (P2_Reconstructed == 1 && P1_Reconstructed == 1) {
	Kpi_Nreco_plus_W += Iw;
	if (sPi_Reconstructed == 1) {
	  Kpih_Nreco_plus_W += Iw;
	}
      }

    }
    else {
      Ngen_minus_W += Iw;
      if (P2_Reconstructed == 1) 
	K_Nreco_minus_W += Iw;
      if (P1_Reconstructed == 1) 
	pi_Nreco_minus_W += Iw;
      if (sPi_Reconstructed == 1) 
	h_Nreco_minus_W += Iw;
      
      if (P2_Reconstructed == 1 && P1_Reconstructed == 1) {
	Kpi_Nreco_minus_W += Iw;
	if (sPi_Reconstructed == 1) { 
	  Kpih_Nreco_minus_W += Iw;
	}
      }
    }
   


    if (Dst_ID > 0) {
      Ngen_plus_sumW2 += Iw*Iw;
      if (P2_Reconstructed == 1) 
	K_Nreco_plus_sumW2 += Iw*Iw;
      if (P1_Reconstructed == 1) 
	pi_Nreco_plus_sumW2 += Iw*Iw;
      if (sPi_Reconstructed == 1) 
	h_Nreco_plus_sumW2 += Iw*Iw;
      
      if (P2_Reconstructed == 1 && P1_Reconstructed == 1) {
	Kpi_Nreco_plus_sumW2 += Iw*Iw;
	if (sPi_Reconstructed == 1) {
	  Kpih_Nreco_plus_sumW2 += Iw*Iw;
	}
      }

    }
    else {
      Ngen_minus_sumW2 += Iw*Iw;
      if (P2_Reconstructed == 1) 
	K_Nreco_minus_sumW2 += Iw*Iw;
      if (P1_Reconstructed == 1) 
	pi_Nreco_minus_sumW2 += Iw*Iw;
      if (sPi_Reconstructed == 1) 
	h_Nreco_minus_sumW2 += Iw*Iw;
      
      if (P2_Reconstructed == 1 && P1_Reconstructed == 1) {
	Kpi_Nreco_minus_sumW2 += Iw*Iw;
	if (sPi_Reconstructed == 1) { 
	  Kpih_Nreco_minus_sumW2 += Iw*Iw;
	}
      }
    }

    if (save_GENtuple)
      t->Fill();
  }

  if (save_GENtuple) {
    t->Write();
    f_new->Close();
  }


  Ngen = Ngen_plus + Ngen_minus;

  cout << ":Kpi:" << '\t' << Kpi_Nreco_plus << '\t' << Kpi_Nreco_minus << endl;
  cout << ":K:" << '\t' << K_Nreco_plus << '\t' << K_Nreco_minus << endl;
  cout << ":pi:" << '\t' << pi_Nreco_plus << '\t' << pi_Nreco_minus << endl;
  cout << ":spi:" << '\t' << h_Nreco_plus << '\t' << h_Nreco_minus << endl;
  cout << ":Kpispi:" << '\t' << Kpih_Nreco_plus << '\t' << Kpih_Nreco_minus << endl;
  cout << ":Ngen:" << '\t' << Ngen_plus << '\t' << Ngen_minus << endl;

  cout << ":KpiW:" << '\t' << Kpi_Nreco_plus_W << '\t' << Kpi_Nreco_minus_W << '\t' << Kpi_Nreco_plus_sumW2 << '\t' << Kpi_Nreco_minus_sumW2 << endl;
  cout << ":KW:" << '\t' << K_Nreco_plus_W << '\t' << K_Nreco_minus_W << '\t' << K_Nreco_plus_sumW2 << '\t' << K_Nreco_minus_sumW2 << endl;
  cout << ":piW:" << '\t' << pi_Nreco_plus_W << '\t' << pi_Nreco_minus_W << '\t' << pi_Nreco_plus_sumW2 << '\t' << pi_Nreco_minus_sumW2 << endl;
  cout << ":spiW:" << '\t' << h_Nreco_plus_W << '\t' << h_Nreco_minus_W << '\t' << h_Nreco_plus_sumW2 << '\t' << h_Nreco_minus_sumW2 << endl;
  cout << ":KpispiW:" << '\t' << Kpih_Nreco_plus_W << '\t' << Kpih_Nreco_minus_W << '\t' << Kpih_Nreco_plus_sumW2 << '\t' << Kpih_Nreco_minus_sumW2 << endl;
  cout << ":NgenW:" << '\t' << Ngen_plus_W << '\t' << Ngen_minus_W << '\t' << Ngen_plus_sumW2 << '\t' << Ngen_minus_sumW2 << endl;

  //----------- RECO ------
  
  ntp = (TTree*)f->Get("Dstp2D0pi/DecayTree");
  //  ntp->SetBranchStatus("*",0);

  ntp->SetBranchAddress("Dst_ID", &Dst_ID);

  ntp->SetBranchAddress("Dst_PT", &Dst_TRUEPT);
  ntp->SetBranchAddress("Dst_ETA", &Dst_ETA);
  ntp->SetBranchAddress("Dst_PHI", &Dst_PHI);
  ntp->SetBranchAddress("Dst_PX", &Dst_TRUEP_X);
  ntp->SetBranchAddress("Dst_PY", &Dst_TRUEP_Y);
  ntp->SetBranchAddress("Dst_PZ", &Dst_TRUEP_Z);
  ntp->SetBranchAddress("D0_ETA", &D0_ETA);
  ntp->SetBranchAddress("D0_PHI", &D0_PHI);
  ntp->SetBranchAddress("D0_PX", &D0_TRUEP_X);
  ntp->SetBranchAddress("D0_PY", &D0_TRUEP_Y);
  ntp->SetBranchAddress("D0_PZ", &D0_TRUEP_Z);
  ntp->SetBranchAddress("sPi_PT", &sPi_TRUEPT);
  ntp->SetBranchAddress("sPi_ETA", &sPi_ETA);
  ntp->SetBranchAddress("sPi_PHI", &sPi_PHI);
  ntp->SetBranchAddress("sPi_PX", &sPi_TRUEP_X);
  ntp->SetBranchAddress("sPi_PY", &sPi_TRUEP_Y);
  ntp->SetBranchAddress("sPi_PZ", &sPi_TRUEP_Z);
  ntp->SetBranchAddress("P2_PT", &P2_TRUEPT);
  ntp->SetBranchAddress("P2_ETA", &P2_ETA);
  ntp->SetBranchAddress("P2_PHI", &P2_PHI);
  ntp->SetBranchAddress("P2_PX", &P2_TRUEP_X);
  ntp->SetBranchAddress("P2_PY", &P2_TRUEP_Y);
  ntp->SetBranchAddress("P2_PZ", &P2_TRUEP_Z);
  ntp->SetBranchAddress("P1_PT", &P1_TRUEPT);
  ntp->SetBranchAddress("P1_ETA", &P1_ETA);
  ntp->SetBranchAddress("P1_PHI", &P1_PHI);
  ntp->SetBranchAddress("P1_PX", &P1_TRUEP_X);
  ntp->SetBranchAddress("P1_PY", &P1_TRUEP_Y);
  ntp->SetBranchAddress("P1_PZ", &P1_TRUEP_Z);

  //TruthMatching variables
  Int_t Dst_BKGCAT = 0;
  Int_t Dst_TRUEID = 0;
  Int_t D0_TRUEID = 0;
  Int_t sPi_TRUEID = 0;
  Int_t P2_TRUEID = 0;
  Int_t P1_TRUEID = 0;
  Int_t D0_MC_MOTHER_ID = 0;
  Int_t sPi_MC_MOTHER_ID = 0;
  Int_t P2_MC_MOTHER_ID = 0;
  Int_t P1_MC_MOTHER_ID = 0;
  Int_t D0_MC_MOTHER_KEY = 0;
  Int_t sPi_MC_MOTHER_KEY = 0;
  Int_t P2_MC_MOTHER_KEY = 0;
  Int_t P1_MC_MOTHER_KEY = 0;
  Bool_t sPi_TRUEISSTABLE;
  Bool_t P1_TRUEISSTABLE;
  Bool_t P2_TRUEISSTABLE;

  ntp->SetBranchAddress("Dst_BKGCAT",&Dst_BKGCAT);
  ntp->SetBranchAddress("Dst_TRUEID",&Dst_TRUEID);
  ntp->SetBranchAddress("D0_TRUEID",&D0_TRUEID);
  ntp->SetBranchAddress("sPi_TRUEID",&sPi_TRUEID);
  ntp->SetBranchAddress("P2_TRUEID",&P2_TRUEID);
  ntp->SetBranchAddress("P1_TRUEID",&P1_TRUEID);
  ntp->SetBranchAddress("D0_MC_MOTHER_ID",&D0_MC_MOTHER_ID);
  ntp->SetBranchAddress("sPi_MC_MOTHER_ID",&sPi_MC_MOTHER_ID);
  ntp->SetBranchAddress("P2_MC_MOTHER_ID",&P2_MC_MOTHER_ID);
  ntp->SetBranchAddress("P1_MC_MOTHER_ID",&P1_MC_MOTHER_ID);
  ntp->SetBranchAddress("D0_MC_MOTHER_KEY",&D0_MC_MOTHER_KEY);
  ntp->SetBranchAddress("sPi_MC_MOTHER_KEY",&sPi_MC_MOTHER_KEY);
  ntp->SetBranchAddress("P2_MC_MOTHER_KEY",&P2_MC_MOTHER_KEY);
  ntp->SetBranchAddress("P1_MC_MOTHER_KEY",&P1_MC_MOTHER_KEY);
  ntp->SetBranchAddress("sPi_TRUEISSTABLE", &sPi_TRUEISSTABLE);
  ntp->SetBranchAddress("P1_TRUEISSTABLE", &P1_TRUEISSTABLE);
  ntp->SetBranchAddress("P2_TRUEISSTABLE", &P2_TRUEISSTABLE);

  int Nreco = 0;
  int Nreco_plus = 0;
  int Nreco_minus = 0;
  int Nreco_W = 0;
  int Nreco_plus_W = 0;
  int Nreco_minus_W = 0;
  int Nreco_sumW2 = 0;
  int Nreco_plus_sumW2 = 0;
  int Nreco_minus_sumW2 = 0;

  ntp->SetBranchStatus("*",1);
  inputFile.ReplaceAll("GEN","RECO");
  if (save_RECOtuple) {
    f_new = new TFile(inputFile,"RECREATE");
    t = (TTree*) ntp->CloneTree(0);
    t->SetName("ntp");
    t->SetTitle("ntp");
  }

  for(int i = 0; i < ntp->GetEntries(); i++) {
    ntp->GetEntry(i);
    
    if (Dst_BKGCAT != 0) continue;   
    if (abs(Dst_TRUEID)!=PdgCode::DS) continue;
    if (abs(D0_TRUEID)!=PdgCode::D0) continue;
    if (abs(sPi_TRUEID)!=PdgCode::Pi) continue;
    if (abs(P1_TRUEID)!=PdgCode::Pi) continue;
    if (abs(P2_TRUEID)!=PdgCode::K) continue;
    if (sPi_MC_MOTHER_ID!=Dst_TRUEID) continue;
    if (D0_MC_MOTHER_ID!=Dst_TRUEID) continue;
    if (P2_MC_MOTHER_ID!=D0_TRUEID) continue;
    if (P1_MC_MOTHER_ID!=D0_TRUEID) continue;
    if (!(sPi_MC_MOTHER_KEY==D0_MC_MOTHER_KEY && P1_MC_MOTHER_KEY==P2_MC_MOTHER_KEY)) continue;
   

    Dst_TRUEP = sqrt(pow(Dst_TRUEP_X,2)+pow(Dst_TRUEP_Y,2)+pow(Dst_TRUEP_Z,2));
    D0_TRUEP = sqrt(pow(D0_TRUEP_X,2)+pow(D0_TRUEP_Y,2)+pow(D0_TRUEP_Z,2));
    D0_TRUEPT = sqrt(pow(D0_TRUEP_X,2)+pow(D0_TRUEP_Y,2));
    sPi_TRUEP = sqrt(pow(sPi_TRUEP_X,2)+pow(sPi_TRUEP_Y,2)+pow(sPi_TRUEP_Z,2));
    P2_TRUEP = sqrt(pow(P2_TRUEP_X,2)+pow(P2_TRUEP_Y,2)+pow(P2_TRUEP_Z,2));
    P1_TRUEP = sqrt(pow(P1_TRUEP_X,2)+pow(P1_TRUEP_Y,2)+pow(P1_TRUEP_Z,2));


    HarmonizationCut = P2_TRUEP>5e3 && P2_TRUEPT>800 && P1_TRUEP>5e3 && P1_TRUEPT>800 && D0_TRUEP > 30e3;
    HarmonizationCut = HarmonizationCut && Dst_TRUEPT > 2.28e3 && Dst_TRUEPT < 9.5e3 && sPi_TRUEPT > 100 && sPi_TRUEPT < 800 && Dst_ETA > 2.4 && Dst_ETA < 4.3 && sPi_ETA > 2.4 && sPi_ETA < 4.3;
    
    HarmonizationCut = HarmonizationCut && P1_TRUEPT + P2_TRUEPT > 2280 && P1_TRUEPT < 5.5e3 && P2_TRUEPT < 6e3 && P1_ETA > 2.2 && P1_ETA < 4.3 && P2_ETA > 2.2 && P2_ETA < 4.3;

    HarmonizationCut = HarmonizationCut && (D0_TRUEPT > 2.08e3 && D0_TRUEPT < 9.08e3 && D0_ETA > 2.4 && D0_ETA < 4.3);

    if(!HarmonizationCut) continue;


    if (Dst_ID > 0) {
      Nreco_plus++;
    }
    else {
      Nreco_minus++;
    }

    // weighted yields
    Iw = h_pip_PTvsETA->getBC(h_pip_PTvsETA->find({P1_TRUEPT,P1_ETA}));
    Iw *=  h_pip_PHI->getBC(h_pip_PHI->find({P1_PHI}));
    Iw *= h_Km_PTvsETA->getBC(h_Km_PTvsETA->find({P2_TRUEPT,P2_ETA}));
    Iw *= h_Km_PHI->getBC(h_Km_PHI->find({P2_PHI}));
      
    if (Iw > 500)
      Iw = 0;


    if (Dst_ID > 0) {
      Nreco_plus_W += Iw;
    }
    else {
      Nreco_minus_W += Iw;
      
    }
  
    if (Dst_ID > 0) {
      Nreco_plus_sumW2 += Iw*Iw;
    }
    else {
      Nreco_minus_sumW2 += Iw*Iw;
      
    }


    if (save_RECOtuple)
      t->Fill();
  }

  if (save_RECOtuple) {
    t->Write();
    f_new->Close();
  }
 
  Nreco = Nreco_plus + Nreco_minus;

  cout << ":Nreco:" << '\t' << Nreco_plus << '\t' << Nreco_minus << endl;
  cout << ":NrecoW:" << '\t' << Nreco_plus_W << '\t' << Nreco_minus_W << '\t' << Nreco_plus_sumW2 << '\t' << Nreco_minus_sumW2 << endl;

}

int main(int argc, char * argv[]) {
  TString inputFile = argv[1];
  Bool_t use_ALL = atoi(argv[2]);
  
  D02Kmpip_MCefficiency(inputFile,use_ALL);
  
}
