#include <iostream>      
#include <stdio.h>       
#include <stdlib.h>      
#include <TString.h>         
#include <TFile.h>            
#include <TChain.h>      
#include <TTree.h>  
#include <TLorentzVector.h>   
#include "/home/LHCB/smaccoli/Tools.h"

void Dp2KS0pipLL_MCefficiency(TString inputFile = "/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/pGun/Dp2KS0pipLL_2016_Up/4001/pGun_Dp2KS0pipLL.root", Bool_t use_ALL = 0) {

  Bool_t save_RECOtuple = 0;
  Bool_t save_GENtuple = 0;
  
  TFile * f;
  if(use_ALL)
    inputFile.ReplaceAll(".root","_ALL.root");
  f = TFile::Open(inputFile);
  if(!f) cout << "FILE " << inputFile << " DOES NOT EXIST!!" << endl;
  TTree * ntp;
  ntp = (TTree*)f->Get("MCDp2KS0pipLL/MCDecayTree");
  cout << "tot GEN decays: " << ntp->GetEntries() << endl;
  ntp->SetBranchStatus("*ID",1);
  //ntp->SetBranchStatus("*",0);
  
  int Dplus_ID; ntp->SetBranchAddress("Dplus_ID", &Dplus_ID);
  int hplus_Reconstructed; ntp->SetBranchAddress("hplus_Reconstructed", &hplus_Reconstructed);
  int piplus_Reconstructed; ntp->SetBranchAddress("piplus_Reconstructed", &piplus_Reconstructed);
  int piminus_Reconstructed; ntp->SetBranchAddress("piminus_Reconstructed", &piminus_Reconstructed);

  double Dplus_TRUEP;// ntp->SetBranchAddress("Dplus_TRUEP", &Dplus_TRUEP);
  double Dplus_TRUEPT; ntp->SetBranchAddress("Dplus_TRUEPT", &Dplus_TRUEPT);
  double Dplus_ETA; ntp->SetBranchAddress("Dplus_ETA", &Dplus_ETA);
  double Dplus_PHI; ntp->SetBranchAddress("Dplus_PHI", &Dplus_PHI);
  double Dplus_TRUEP_X; ntp->SetBranchAddress("Dplus_TRUEP_X", &Dplus_TRUEP_X);
  double Dplus_TRUEP_Y; ntp->SetBranchAddress("Dplus_TRUEP_Y", &Dplus_TRUEP_Y);
  double Dplus_TRUEP_Z; ntp->SetBranchAddress("Dplus_TRUEP_Z", &Dplus_TRUEP_Z);
  double hplus_TRUEP;// ntp->SetBranchAddress("hplus_TRUEP", &hplus_TRUEP);
  double hplus_TRUEPT; ntp->SetBranchAddress("hplus_TRUEPT", &hplus_TRUEPT);
  double hplus_ETA; ntp->SetBranchAddress("hplus_ETA", &hplus_ETA);
  double hplus_PHI; ntp->SetBranchAddress("hplus_PHI", &hplus_PHI);
  double hplus_TRUEP_X; ntp->SetBranchAddress("hplus_TRUEP_X", &hplus_TRUEP_X);
  double hplus_TRUEP_Y; ntp->SetBranchAddress("hplus_TRUEP_Y", &hplus_TRUEP_Y);
  double hplus_TRUEP_Z; ntp->SetBranchAddress("hplus_TRUEP_Z", &hplus_TRUEP_Z);
  double KS0_TRUEP;// ntp->SetBranchAddress("KS0_TRUEP", &KS0_TRUEP);
  double KS0_TRUEPT; ntp->SetBranchAddress("KS0_TRUEPT", &KS0_TRUEPT);
  double KS0_ETA; ntp->SetBranchAddress("KS0_ETA", &KS0_ETA);
  double KS0_PHI; ntp->SetBranchAddress("KS0_PHI", &KS0_PHI);
  double KS0_TRUEP_X; ntp->SetBranchAddress("KS0_TRUEP_X", &KS0_TRUEP_X);
  double KS0_TRUEP_Y; ntp->SetBranchAddress("KS0_TRUEP_Y", &KS0_TRUEP_Y);
  double KS0_TRUEP_Z; ntp->SetBranchAddress("KS0_TRUEP_Z", &KS0_TRUEP_Z);
  

  bool HarmonizationCut = true;

  //Useful variables

  int Ngen = 0;
  int Ngen_plus = 0;
  int Ngen_minus = 0;
  int KSh_Nreco_plus = 0;
  int KSh_Nreco_minus = 0; 
  int KS_Nreco_plus = 0;
  int KS_Nreco_minus = 0;
  int h_Nreco_plus = 0;
  int h_Nreco_minus = 0;
 

  vector < double > vec_Dplus_TRUEPT;
 
  TFile *f_new;
  TTree* t;
  inputFile.ReplaceAll("/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/pGun/","../data/");
  inputFile.ReplaceAll("Dw/","Dw_");  
  inputFile.ReplaceAll("Up/","Up_");  
  inputFile.ReplaceAll("/pGun","_GEN_pGun");
  if(save_GENtuple) {
    ntp->SetBranchStatus("*",1);
    f_new = new TFile(inputFile,"RECREATE");
    t = (TTree*) ntp->CloneTree(0);
    t->SetName("ntp");
    t->SetTitle("ntp");
  }

  for(int i = 0; i < ntp->GetEntries(); i++) {
    ntp->GetEntry(i);
    

    Dplus_TRUEP = sqrt(pow(Dplus_TRUEP_X,2)+pow(Dplus_TRUEP_Y,2)+pow(Dplus_TRUEP_Z,2));
    hplus_TRUEP = sqrt(pow(hplus_TRUEP_X,2)+pow(hplus_TRUEP_Y,2)+pow(hplus_TRUEP_Z,2));
    KS0_TRUEP = sqrt(pow(KS0_TRUEP_X,2)+pow(KS0_TRUEP_Y,2)+pow(KS0_TRUEP_Z,2));

    HarmonizationCut = KS0_TRUEP > 30e3 && Dplus_TRUEPT - hplus_TRUEPT > 2280 && TMath::Abs(Dplus_PHI-hplus_PHI) < 0.4 && TMath::Abs(Dplus_ETA-hplus_ETA) < 0.4 && Dplus_TRUEPT>4.2e3 && Dplus_TRUEPT<11e3 && Dplus_ETA>2.3 && Dplus_ETA<4.2 && hplus_TRUEPT>1.6e3 && hplus_TRUEPT<6e3 && hplus_ETA>2.2 && hplus_ETA<4.2;   
    
    HarmonizationCut = HarmonizationCut && (KS0_TRUEPT > 2.08e3 && KS0_TRUEPT < 9.08e3 && KS0_ETA > 2.4 && KS0_ETA < 4.3) && (Dplus_ETA < 4.1 && hplus_ETA < 4.1);
   
    if(!HarmonizationCut) {
      continue;
    }
   
    if (Dplus_ID > 0) {
      Ngen_plus++;
      if (hplus_Reconstructed == 1)
	h_Nreco_plus++;
      if (piplus_Reconstructed == 1 && piminus_Reconstructed == 1) {
	KS_Nreco_plus++;
	if (hplus_Reconstructed == 1) {
	  KSh_Nreco_plus++;
	  vec_Dplus_TRUEPT.push_back(Dplus_TRUEPT);
	}
      }
    }
    else {
      Ngen_minus++;
      if (hplus_Reconstructed == 1)
	h_Nreco_minus++;
      if (piplus_Reconstructed == 1 && piminus_Reconstructed == 1) {
	KS_Nreco_minus++;
	if (hplus_Reconstructed == 1) {
	  KSh_Nreco_minus++;
	  vec_Dplus_TRUEPT.push_back(Dplus_TRUEPT);
	}
      }
    }
    if(save_GENtuple)
      t->Fill();
  }
  
  if(save_GENtuple) {
    t->Write();
    f_new->Close();
  }
 
  Ngen = Ngen_plus + Ngen_minus;

  cout << ":KSh:" << '\t' << KSh_Nreco_plus << '\t' << KSh_Nreco_minus << endl;
  cout << ":KS:" << '\t' << KS_Nreco_plus << '\t' << KS_Nreco_minus << endl;
  cout << ":h:" << '\t' << h_Nreco_plus << '\t' << h_Nreco_minus << endl;
  cout << ":Ngen:" << '\t' << Ngen_plus << '\t' << Ngen_minus << endl;


  //----------- RECO ------
  
  ntp = (TTree*)f->Get("Dp2KS0pipLL/DecayTree");
  cout << "tot RECO decays: " << ntp->GetEntries() << endl;
  //  ntp->SetBranchStatus("*",0);
  
  ntp->SetBranchAddress("Dplus_ID", &Dplus_ID);
  ntp->SetBranchAddress("Dplus_PT", &Dplus_TRUEPT);
  ntp->SetBranchAddress("Dplus_ETA", &Dplus_ETA);
  ntp->SetBranchAddress("Dplus_PHI", &Dplus_PHI);
  ntp->SetBranchAddress("Dplus_PX", &Dplus_TRUEP_X);
  ntp->SetBranchAddress("Dplus_PY", &Dplus_TRUEP_Y);
  ntp->SetBranchAddress("Dplus_PZ", &Dplus_TRUEP_Z);
  ntp->SetBranchAddress("hplus_PT", &hplus_TRUEPT);
  ntp->SetBranchAddress("hplus_ETA", &hplus_ETA);
  ntp->SetBranchAddress("hplus_PHI", &hplus_PHI);
  ntp->SetBranchAddress("hplus_PX", &hplus_TRUEP_X);
  ntp->SetBranchAddress("hplus_PY", &hplus_TRUEP_Y);
  ntp->SetBranchAddress("hplus_PZ", &hplus_TRUEP_Z);
  ntp->SetBranchAddress("KS0_PT", &KS0_TRUEPT);
  ntp->SetBranchAddress("KS0_ETA", &KS0_ETA);
  ntp->SetBranchAddress("KS0_PHI", &KS0_PHI);
  ntp->SetBranchAddress("KS0_PX", &KS0_TRUEP_X);
  ntp->SetBranchAddress("KS0_PY", &KS0_TRUEP_Y);
  ntp->SetBranchAddress("KS0_PZ", &KS0_TRUEP_Z);

  //TruthMatching variables
  Int_t Dplus_BKGCAT = 0;
  Int_t Dplus_TRUEID = 0;
  Int_t hplus_TRUEID = 0;
  Int_t piminus_TRUEID = 0;
  Int_t piplus_TRUEID = 0;
  Int_t KS0_TRUEID = 0;
  Int_t hplus_MC_MOTHER_ID = 0;
  Int_t piminus_MC_MOTHER_ID = 0;
  Int_t piplus_MC_MOTHER_ID = 0;
  Int_t KS0_MC_MOTHER_ID = 0;
  Int_t hplus_MC_MOTHER_KEY = 0;
  Int_t piminus_MC_MOTHER_KEY = 0;
  Int_t piplus_MC_MOTHER_KEY = 0;
  Int_t KS0_MC_MOTHER_KEY = 0;
  Bool_t hplus_TRUEISSTABLE;
  Bool_t piplus_TRUEISSTABLE;
  Bool_t piminus_TRUEISSTABLE;

  ntp->SetBranchAddress("Dplus_BKGCAT",&Dplus_BKGCAT);
  ntp->SetBranchAddress("Dplus_TRUEID",&Dplus_TRUEID);
  ntp->SetBranchAddress("hplus_TRUEID",&hplus_TRUEID);
  ntp->SetBranchAddress("piminus_TRUEID",&piminus_TRUEID);
  ntp->SetBranchAddress("piplus_TRUEID",&piplus_TRUEID);
  ntp->SetBranchAddress("KS0_TRUEID",&KS0_TRUEID);
  ntp->SetBranchAddress("hplus_MC_MOTHER_ID",&hplus_MC_MOTHER_ID);
  ntp->SetBranchAddress("piminus_MC_MOTHER_ID",&piminus_MC_MOTHER_ID);
  ntp->SetBranchAddress("piplus_MC_MOTHER_ID",&piplus_MC_MOTHER_ID);
  ntp->SetBranchAddress("KS0_MC_MOTHER_ID",&KS0_MC_MOTHER_ID);
  ntp->SetBranchAddress("hplus_MC_MOTHER_KEY",&hplus_MC_MOTHER_KEY);
  ntp->SetBranchAddress("piminus_MC_MOTHER_KEY",&piminus_MC_MOTHER_KEY);
  ntp->SetBranchAddress("piplus_MC_MOTHER_KEY",&piplus_MC_MOTHER_KEY);
  ntp->SetBranchAddress("KS0_MC_MOTHER_KEY",&KS0_MC_MOTHER_KEY);
  ntp->SetBranchAddress("hplus_TRUEISSTABLE", &hplus_TRUEISSTABLE);
  ntp->SetBranchAddress("piplus_TRUEISSTABLE", &piplus_TRUEISSTABLE);
  ntp->SetBranchAddress("piminus_TRUEISSTABLE", &piminus_TRUEISSTABLE);
  
  int Nreco = 0;
  int Nreco_plus = 0;
  int Nreco_minus = 0;
  
  ntp->SetBranchStatus("*",1);
  inputFile.ReplaceAll("GEN","RECO");
  if (save_RECOtuple) {
    f_new = new TFile(inputFile,"RECREATE");
    t = (TTree*) ntp->CloneTree(0);
    t->SetName("ntp");
    t->SetTitle("ntp");
  }
 
  for(int i = 0; i < ntp->GetEntries(); i++) {
    ntp->GetEntry(i);
    
    if (Dplus_BKGCAT != 0) continue;   
    if (abs(Dplus_TRUEID)!=PdgCode::D) continue;
    if (abs(hplus_TRUEID)!=PdgCode::Pi) continue;
    if (abs(piplus_TRUEID)!=PdgCode::Pi) continue;
    if (abs(piminus_TRUEID)!=PdgCode::Pi) continue;
    if (abs(KS0_TRUEID)!=PdgCode::K0s) continue;
    if (hplus_MC_MOTHER_ID!=Dplus_TRUEID) continue;
    if (piplus_MC_MOTHER_ID!=KS0_TRUEID) continue;
    if (piminus_MC_MOTHER_ID!=KS0_TRUEID) continue; 
    if (KS0_MC_MOTHER_ID!=Dplus_TRUEID) continue;
    if (KS0_MC_MOTHER_KEY != hplus_MC_MOTHER_KEY) continue;
    if (piplus_MC_MOTHER_KEY != piminus_MC_MOTHER_KEY) continue;
    
    Dplus_TRUEP = sqrt(pow(Dplus_TRUEP_X,2)+pow(Dplus_TRUEP_Y,2)+pow(Dplus_TRUEP_Z,2));
    hplus_TRUEP = sqrt(pow(hplus_TRUEP_X,2)+pow(hplus_TRUEP_Y,2)+pow(hplus_TRUEP_Z,2));
    KS0_TRUEP = sqrt(pow(KS0_TRUEP_X,2)+pow(KS0_TRUEP_Y,2)+pow(KS0_TRUEP_Z,2));

    HarmonizationCut = KS0_TRUEP > 30e3 && Dplus_TRUEPT - hplus_TRUEPT > 2280 && TMath::Abs(Dplus_PHI-hplus_PHI) < 0.4 && TMath::Abs(Dplus_ETA-hplus_ETA) < 0.4 && Dplus_TRUEPT>4.2e3 && Dplus_TRUEPT<11e3 && Dplus_ETA>2.3 && Dplus_ETA<4.2 && hplus_TRUEPT>1.6e3 && hplus_TRUEPT<6e3 && hplus_ETA>2.2 && hplus_ETA<4.2;   
    
    if(!HarmonizationCut) {
      continue;
    }
    // cout << "survived0" << endl;
    
    HarmonizationCut = HarmonizationCut && (KS0_TRUEPT > 2.08e3 && KS0_TRUEPT < 9.08e3 && KS0_ETA > 2.4 && KS0_ETA < 4.3) && (Dplus_ETA < 4.1 && hplus_ETA < 4.1);
   
    //cout << "survived1" << endl;
    
    if(!HarmonizationCut) {
      continue;
    }
  
    if (Dplus_ID > 0) {
      Nreco_plus++;
    }
    else {
      Nreco_minus++;
      
    }
    
    if(save_RECOtuple)
      t->Fill();
  }

  if(save_RECOtuple) {
    t->Write();
    f_new->Close();
  }

 
  Nreco = Nreco_plus + Nreco_minus;


  cout << ":Nreco:" << '\t' << Nreco_plus << '\t' << Nreco_minus << endl;


}


int main(int argc, char * argv[]) {
  TString inputFile = argv[1];
  Bool_t use_ALL = atoi(argv[2]);
  
  Dp2KS0pipLL_MCefficiency(inputFile,use_ALL);
  
}
