#!/bin/bash

BDTcut="-1"
doSelection=1                                                                          
doFiducial=0                                                                           
doMultCandRemoval=0                                                                    
isPGun=0                                                                    


echo "DO SELECTION: "$doSelection
echo "DO FIDUCIAL CUTS: "$doFiducial
echo "BDT CUT SET TO: "$BDTcut
echo "DO MULTIPLE CANDIDATES REMOVAL: "$doMultCandRemoval

DIR=$PWD
#/bin/rm $DIR/exec/*
#/bin/rm $DIR/err/*
#/bin/rm $DIR/out/*
#/bin/rm $DIR/logs/*

g++ -Wall -o selectKSpi selectKSpi.cxx `root-config --cflags --glibs` `gsl-config --cflags --libs`

mode=D2KSpi
#for year in $(echo 2015 2016 2017 2018); do
#    for pol in $(echo MagUp MagDown); do
for year in $(echo 2018); do
    for pol in $(echo MagDown); do
	#for pol in $(echo MagUp); do
	inputLists=$(echo "/home/LHCB-T3/smaccoli/CPV_in_D02hh/lists/Dp2KS0pipLL/list_${year}_${pol}_*.dat")
	if [ $isPGun -eq 1 ]
	then
	    inputLists=$(echo "/home/LHCB-T3/smaccoli/CPV_in_D02hh/lists/pGun/Dp2KS0pipLL/list_${year}_${pol}_*.dat")
	fi

	for inputList in $(ls -I logs $inputLists ); do
	    idList=$(echo $inputList | tr "/" " " | awk '{print $7}' | tr "_" " " | awk '{print $4}' | tr "." " " | awk '{print $1}')
	    if [ $isPGun -eq 1 ]
	    then
		idList=$(echo $inputList | tr "/" " " | awk '{print $8}' | tr "_" " " | awk '{print $4}' | tr "." " " | awk '{print $1}')
	    fi
	    echo "#!/bin/sh" > exec/$mode.$year.$pol.$idList.sh
	    echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> exec/$mode.$year.$pol.$idList.sh 
	    echo "cd $DIR" >> exec/$mode.$year.$pol.$idList.sh
	    echo "hostname" >> exec/$mode.$year.$pol.$idList.sh
	    echo $inputList
	    outList="/home/LHCB-T3/smaccoli/data/CPV_in_D02hh/tmpL_Dp2KS0pipLL_"$year"_"$pol"_"$idList".root"
	    outFiles=""
	    for inputFile in $(cat $inputList); do
		id=$(echo $inputFile | tr "/" " " | awk '{print $10}')
		if [ $isPGun -eq 1 ]
		then
		    id=$(echo $inputFile | tr "/" " " | awk '{print $8}')
		fi
		outFile="/storage/gpfs_data/local/lhcb/users/smaccoli/data/CPV_in_D02hh/tmp_Dp2KS0pipLL_"$year"_"$pol"_"$id".root"
  		outFiles+=" $outFile"
		
		echo $idList $id $outFile
		echo "./selectKSpi $inputFile \"Dp2KS0pipLL/DecayTree\" $outFile \"ntp\" $doSelection $doFiducial $doMultCandRemoval $isPGun $BDTcut > logs/$mode.$year.$pol.$id.log" >> exec/$mode.$year.$pol.$idList.sh
		#break
	    done

	    echo "hadd -f $outList $outFiles" >> exec/$mode.$year.$pol.$idList.sh
	    echo "rm $outFiles" >> exec/$mode.$year.$pol.$idList.sh
	    
	    chmod +x exec/$mode.$year.$pol.$idList.sh
	    
	    python condor_make_template.py condor_template.sub $mode.$year.$pol.$idList
	    condor_submit exec/condor_template.$mode.$year.$pol.$idList.sub -batch-name $mode$year$pol$idList

	    #break
	done
	#break
    done
    #break
done

#ls /storage/gpfs_data/local/lhcb/users/ferrari/ADKPi_ntuples/2015/MagDown/*/*.root | wc -l = 494 % 
#ls /storage/gpfs_data/local/lhcb/users/ferrari/ADKPi_ntuples/2016/MagDown/*/*.root | wc -l = 427 % 48
#ls /storage/gpfs_data/local/lhcb/users/ferrari/ADKPi_ntuples/2017/MagDown/*/*.root | wc -l = 247 % 
#ls /storage/gpfs_data/local/lhcb/users/ferrari/ADKPi_ntuples/2018/MagDown/*/*.root | wc -l = 264 % 
#ls /storage/gpfs_data/local/lhcb/users/ferrari/ADKPi_ntuples/2015/MagUp/*/*.root | wc -l = 361 % 
#ls /storage/gpfs_data/local/lhcb/users/ferrari/ADKPi_ntuples/2016/MagUp/*/*.root | wc -l = 407 % 
#ls /storage/gpfs_data/local/lhcb/users/ferrari/ADKPi_ntuples/2017/MagUp/*/*.root | wc -l = 243 % 
#ls /storage/gpfs_data/local/lhcb/users/ferrari/ADKPi_ntuples/2018/MagUp/*/*.root | wc -l = 325 % 

# BDTcut="-1"
# #BDTcut="-0.36"
# doSelection=$1                                                                          
# doFiducial=$2                                                                           
# doMultCandRemoval=$3                                                                    
# isPGun=$4                                                                    

# if [ -z $BDTcut ]; then
#     echo "SELECTION FLAG NOT SET! PLEASE CHECK"
#     exit 0
# fi

# if [ -z $doSelection ]; then
#     echo "SELECTION FLAG NOT SET! PLEASE CHECK"
#     exit 0
# fi

# if [ -z $doFiducial ]; then
#     echo "SELECTION FLAG NOT SET! PLEASE CHECK"
#     exit 0
# fi

# if [ -z $doMultCandRemoval ]; then
#     echo "SELECTION FLAG NOT SET! PLEASE CHECK"
#     exit 0
# fi
# if [ -z $isPGun ]; then                                                      
#     echo "SELECTION FLAG NOT SET! PLEASE CHECK"                                         
#     exit 0                                                                              
# fi                                                                           
