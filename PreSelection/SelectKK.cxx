#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TVector.h>
#include <TLorentzVector.h>
#include <iostream>
#include "/home/LHCB/smaccoli/Tools.h"
using namespace std;

void SelectKK(TString year = "18", TString polarity = "Dw") {
  
  TString decay = "D02KmKp";
  TString add_string = "_PreSelected";
  // TString add_string = "";
   
  TString inFileName = "/home/LHCB/smaccoli/CPV_in_D02hh/data/"+decay+"_"+year+"_"+polarity+add_string+".root";
 

  TChain* inputTree = new TChain("ntp","ntp");
  inputTree->Add(inFileName);
  inputTree->SetBranchStatus("*",0);
  inputTree->SetBranchStatus("*M",1);
  inputTree->SetBranchStatus("*Mass",1);
  inputTree->SetBranchStatus("*ID",1);
  inputTree->SetBranchStatus("*PT",1);
  inputTree->SetBranchStatus("*ETA",1);
  inputTree->SetBranchStatus("*PHI",1);
  inputTree->SetBranchStatus("*PX",1);
  inputTree->SetBranchStatus("*PY",1);
  inputTree->SetBranchStatus("*PZ",1);
  inputTree->SetBranchStatus("*P",1);
 
  double Dst_PT;
  inputTree->SetBranchAddress("Dst_PT",&Dst_PT);
  double Dst_ETA;
  inputTree->SetBranchAddress("Dst_ETA",&Dst_ETA);
  double Dst_PHI;
  inputTree->SetBranchAddress("Dst_PHI",&Dst_PHI);
  double Dst_PX;
  inputTree->SetBranchAddress("Dst_PX",&Dst_PX);
  double Dst_PY;
  inputTree->SetBranchAddress("Dst_PY",&Dst_PY);
  double Dst_PZ;
  inputTree->SetBranchAddress("Dst_PZ",&Dst_PZ);
  double sPi_PT;
  inputTree->SetBranchAddress("sPi_PT",&sPi_PT);
  double sPi_ETA;
  inputTree->SetBranchAddress("sPi_ETA",&sPi_ETA);
  double sPi_PHI;
  inputTree->SetBranchAddress("sPi_PHI",&sPi_PHI);
  double sPi_PX;
  inputTree->SetBranchAddress("sPi_PX",&sPi_PX);
  double sPi_PY;
  inputTree->SetBranchAddress("sPi_PY",&sPi_PY);
  double sPi_PZ;
  inputTree->SetBranchAddress("sPi_PZ",&sPi_PZ);

  double P1_P;
  inputTree->SetBranchAddress("P1_P",&P1_P);
  double P1_PT;
  inputTree->SetBranchAddress("P1_PT",&P1_PT);
  double P1_ETA;
  inputTree->SetBranchAddress("P1_ETA",&P1_ETA);
  double P1_PHI;
  inputTree->SetBranchAddress("P1_PHI",&P1_PHI);
  double P1_PX;
  inputTree->SetBranchAddress("P1_PX",&P1_PX);
  double P1_PY;
  inputTree->SetBranchAddress("P1_PY",&P1_PY);
  double P1_PZ;
  inputTree->SetBranchAddress("P1_PZ",&P1_PZ);
  double P2_P;
  inputTree->SetBranchAddress("P2_P",&P2_P);
  double P2_PT;
  inputTree->SetBranchAddress("P2_PT",&P2_PT);
  double P2_ETA;
  inputTree->SetBranchAddress("P2_ETA",&P2_ETA);
  double P2_PHI;
  inputTree->SetBranchAddress("P2_PHI",&P2_PHI);
  double P2_PX;
  inputTree->SetBranchAddress("P2_PX",&P2_PX);
  double P2_PY;
  inputTree->SetBranchAddress("P2_PY",&P2_PY);
  double P2_PZ;
  inputTree->SetBranchAddress("P2_PZ",&P2_PZ);
  double X_P;
  inputTree->SetBranchAddress("X_P",&X_P);
  double X_PT;
  inputTree->SetBranchAddress("X_PT",&X_PT);
  double X_ETA;
  inputTree->SetBranchAddress("X_ETA",&X_ETA);
  double X_PHI;
  inputTree->SetBranchAddress("X_PHI",&X_PHI);
 
  double DTF_Mass;
  inputTree->SetBranchAddress("DTF_Mass",&DTF_Mass);
  int Dst_ID;
  inputTree->SetBranchAddress("Dst_ID",&Dst_ID);

  TString outFileName = inFileName;
  outFileName.ReplaceAll("_PreSelected","");
  //outFileName.ReplaceAll(".root","_HarmCuts.root");

  TFile *f = TFile::Open(outFileName,"RECREATE");
  TH1D * h_X_P = new TH1D("h_X_P","h_X_P",100,0,140);
  TH1D * h_X_PT = new TH1D("h_X_PT","h_X_PT",100,0,12e3);
  TH1D * h_X_ETA = new TH1D("h_X_ETA","h_X_ETA",100,1.5,5.5);
  TH1D * h_dm_plus = new TH1D("h_dm_plus","h_dm_plus",500,2004.5,2020);                               TH1D * h_dm_minus = new TH1D("h_dm_minus","h_dm_minus",500,2004.5,2020);
  TTree * t = inputTree->CloneTree(0);

  double HarmCuts;
  t->Branch("HarmCuts",&HarmCuts);
  double THETA_X_sPi;
  t->Branch("THETA_X_sPi",&THETA_X_sPi);
  double X_PX;
  t->Branch("X_PX",&X_PX);
  double X_PY;
  t->Branch("X_PY",&X_PY);
  double X_PZ;
  t->Branch("X_PZ",&X_PZ);
  
  bool HarmonizationCut;
  for(Long64_t i = 0; i < 
	//5e6;
	inputTree->GetEntries();
      i++){
    inputTree->GetEntry(i);
    
    h_X_P->Fill(X_P/1e3);
    h_X_PT->Fill(X_PT);
    h_X_ETA->Fill(X_ETA);
    
    HarmonizationCut = P2_P>5e3 && P2_PT>800 && P1_P>5e3 && P1_PT>800 && X_P > 30e3;
    HarmonizationCut = HarmonizationCut && Dst_PT > 2.28e3 && Dst_PT < 9.5e3 && sPi_PT > 100 && sPi_PT < 800 && Dst_ETA > 2.4 && Dst_ETA < 4.3 && sPi_ETA > 2.4 && sPi_ETA < 4.3;
   
    HarmCuts = HarmonizationCut && (X_PT > 2.08e3 && X_PT < 9.08e3 && X_ETA > 2.4 && X_ETA < 4.3);
   
    if(!HarmCuts) {
      continue;
    }
   
    X_PX = X_PT*cos(X_PHI);
    X_PY = X_PT*sin(X_PHI);
    X_PZ = X_PT*sinh(X_ETA);
    

    THETA_X_sPi = ( X_PX*sPi_PX + X_PY*sPi_PY + X_PZ*sPi_PZ ) / ( X_PT*cosh(X_ETA) * sPi_PT*cosh(sPi_ETA) );
    
    if(Dst_ID>0) {
      h_dm_plus->Fill(DTF_Mass);
    }
    else {
      h_dm_minus->Fill(DTF_Mass);
    }
    
    t->Fill();
  }
  
  f->Write();
  f->Close();


}

int main(int argc, char * argv[]) { 
  SelectKK(argv[1], argv[2]); 
  return 0;
}
