#include <iostream>      
#include <stdio.h>       
#include <stdlib.h>      
#include <TString.h>         
#include <TFile.h>            
#include <TChain.h>      
#include <TTree.h>  
#include <TVector3.h>   
#include <TFileCollection.h>      
#include <TMath.h>
#include <TH1D.h>
#include <TH2D.h>
#include "TMVA/Tools.h" 
#include "TMVA/Factory.h"
#include "TMVA/Reader.h"
#include "/home/LHCB/smaccoli/Tools.h"

using namespace std;                   

int main(int argc, char * argv[]) { 

  TH1::SetDefaultSumw2(kTRUE);

  //  cout << argv[1] << endl;
  TString inputFile     = argv[1];
  //cout << argv[2] << endl;
  TString inputNtuple   = argv[2];
  //cout << argv[3] << endl;
  TString outputFile    = argv[3];
  //cout << argv[4] << endl;
  TString outputNtuple  = argv[4];
  //cout << argv[5] << endl;
  int doSelection       = atoi(argv[5]);                        
  //cout << argv[6] << endl;
  int doFiducial        = atoi(argv[6]);                        
  //cout << argv[7] << endl;
  int doMultCandRemoval = atoi(argv[7]);                       
  //cout << argv[8] << endl;
  int isPGun = atoi(argv[8]);                       
  //cout << argv[9] << endl;
  double BDTcut         = atof(argv[9]);

  Bool_t use_pGun_all = 1 && isPGun;
  
  TString year;
  if     (outputFile.Contains("2015")) year = "2015";
  else if(outputFile.Contains("2016")) year = "2016";
  else if(outputFile.Contains("2017")) year = "2017";
  else if(outputFile.Contains("2018")) year = "2018";

  
  
  if(isPGun)  {                                                                                       printf("USING SIMULATED SAMPLE: %d\n", isPGun);                                         
  }                                                                                  
  if(doSelection) {
    printf("DOING SELECTION CUTS: %d\n", doSelection);
    //outputFile.ReplaceAll(".root","_withSelection.root");
  }
  if(doFiducial) {
    printf("DOING FIDUCIAL CUTS: %d\n",  doFiducial);
    //outputFile.ReplaceAll(".root","_withFiducialCuts.root");
  }
  if(BDTcut && BDTcut!=-1)  {
    printf("APPLYING BDT WITH CUT %f\n", BDTcut);
    outputFile.ReplaceAll(".root","_withBDT.root"); 
  }
  if(BDTcut && BDTcut==-1)  {
    printf("NO BDT APPLIED\n");
    //outputFile.ReplaceAll(".root","_noBDT.root"); 
  }
  if(doMultCandRemoval)  {
    printf("DOING MULTIPLE CANDIDATES REMOVAL: %d\n", doMultCandRemoval);
    outputFile.ReplaceAll(".root","_noMultCandRemoval.root"); 
  }
  

  TFile * f;
  if (use_pGun_all)
    inputFile.ReplaceAll(".root","_ALL.root");
  f = TFile::Open(inputFile);
  if(!f) cout << "FILE " << inputFile << " DOES NOT EXIST!!" << endl;
  TTree * ntp;
  if (isPGun)
    ntp = (TTree*)f->Get("Dp2KS0pipLL/DecayTree");
  else
    ntp = (TTree*)f->Get(inputNtuple);
  
  if(!ntp) cout << "NTUPLE " << inputNtuple << " DOES NOT EXIST!!" << endl;
  ntp->SetBranchStatus("*",0);
  
  //TruthMatching variables
  Int_t Dplus_TRUEID = 0;
  Int_t hplus_TRUEID = 0;
  Int_t KS0_TRUEID = 0;
  Int_t hplus_MC_MOTHER_ID = 0;
  Int_t hplus_MC_MOTHER_KEY = 0;
  Int_t KS0_MC_MOTHER_ID = 0;
  Int_t KS0_MC_MOTHER_KEY = 0;
  Int_t Dplus_BKGCAT = 0;
  double hplus_TRUEP; 
  double piplus_TRUEP; 
  double piminus_TRUEP; 

  if (isPGun) {
    ntp->SetBranchStatus("Dplus_TRUEID",1);
    ntp->SetBranchStatus("Dplus_BKGCAT",1);
    ntp->SetBranchStatus("hplus_TRUEID",1);
    ntp->SetBranchStatus("hplus_MC_MOTHER_ID",1);
    ntp->SetBranchStatus("hplus_MC_MOTHER_KEY",1);
    ntp->SetBranchStatus("KS0_TRUEID",1);
    ntp->SetBranchStatus("KS0_MC_MOTHER_ID",1);
    ntp->SetBranchStatus("KS0_MC_MOTHER_KEY",1);
  
    ntp->SetBranchAddress("Dplus_TRUEID",&Dplus_TRUEID);
    ntp->SetBranchAddress("Dplus_BKGCAT",&Dplus_BKGCAT);
    ntp->SetBranchAddress("hplus_TRUEID",&hplus_TRUEID);
    ntp->SetBranchAddress("hplus_MC_MOTHER_ID",&hplus_MC_MOTHER_ID);
    ntp->SetBranchAddress("hplus_MC_MOTHER_KEY",&hplus_MC_MOTHER_KEY);
    ntp->SetBranchAddress("KS0_TRUEID",&KS0_TRUEID);
    ntp->SetBranchAddress("KS0_MC_MOTHER_ID",&KS0_MC_MOTHER_ID);
    ntp->SetBranchAddress("KS0_MC_MOTHER_KEY",&KS0_MC_MOTHER_KEY);
    
    ntp->SetBranchStatus("hplus_TRUEP", 1); ntp->SetBranchAddress("hplus_TRUEP", &hplus_TRUEP);
    ntp->SetBranchStatus("piplus_TRUEP", 1); ntp->SetBranchAddress("piplus_TRUEP", &piplus_TRUEP);
    ntp->SetBranchStatus("piminus_TRUEP", 1); ntp->SetBranchAddress("piminus_TRUEP", &piminus_TRUEP);

  }


  ///masses 
  double Dplus_M; ntp->SetBranchStatus("Dplus_M", 1); ntp->SetBranchAddress("Dplus_M", &Dplus_M);
  double KS0_M; ntp->SetBranchStatus("KS0_M", 1); ntp->SetBranchAddress("KS0_M", &KS0_M);

  //lifetimes
  double Dplus_TAU; ntp->SetBranchStatus("Dplus_TAU", 1); ntp->SetBranchAddress("Dplus_TAU", &Dplus_TAU);                                              
  double KS0_TAU; ntp->SetBranchStatus("KS0_TAU", 1); ntp->SetBranchAddress("KS0_TAU", &KS0_TAU);                                              
  //tag
  int Dplus_ID; ntp->SetBranchStatus("Dplus_ID", 1); ntp->SetBranchAddress("Dplus_ID", &Dplus_ID);

  //DTF
  float  Dplus_DTF_M[100]; ntp->SetBranchStatus("Dplus_DTF_M", 1); ntp->SetBranchAddress("Dplus_DTF_M", Dplus_DTF_M);
  float  Dplus_DTF_ctau[100]; ntp->SetBranchStatus("Dplus_DTF_ctau", 1); ntp->SetBranchAddress("Dplus_DTF_ctau", Dplus_DTF_ctau);
  float  Dplus_DTF_nDOF[100]; ntp->SetBranchStatus("Dplus_DTF_nDOF", 1); ntp->SetBranchAddress("Dplus_DTF_nDOF", Dplus_DTF_nDOF);
  float  Dplus_DTF_chi2[100]; ntp->SetBranchStatus("Dplus_DTF_chi2", 1); ntp->SetBranchAddress("Dplus_DTF_chi2", Dplus_DTF_chi2);
  float  Dplus_DTF_status[100]; ntp->SetBranchStatus("Dplus_DTF_status", 1); ntp->SetBranchAddress("Dplus_DTF_status", Dplus_DTF_status);
  float  Dplus_DTF_decayLength[100]; ntp->SetBranchStatus("Dplus_DTF_decayLength", 1); ntp->SetBranchAddress("Dplus_DTF_decayLength", Dplus_DTF_decayLength);
  int    Dplus_DTF_nPV[100]; ntp->SetBranchStatus("Dplus_DTF_nPV", 1); ntp->SetBranchAddress("Dplus_DTF_nPV", Dplus_DTF_nPV);
  float  Dplus_DTF_KS0_piplus_0_ID[100]; ntp->SetBranchStatus("Dplus_DTF_KS0_piplus_0_ID", 1); ntp->SetBranchAddress("Dplus_DTF_KS0_piplus_0_ID", Dplus_DTF_KS0_piplus_0_ID);
  float  Dplus_DTF_KS0_piplus_0_PX[100]; ntp->SetBranchStatus("Dplus_DTF_KS0_piplus_0_PX", 1); ntp->SetBranchAddress("Dplus_DTF_KS0_piplus_0_PX", Dplus_DTF_KS0_piplus_0_PX);
  float  Dplus_DTF_KS0_piplus_0_PY[100]; ntp->SetBranchStatus("Dplus_DTF_KS0_piplus_0_PY", 1); ntp->SetBranchAddress("Dplus_DTF_KS0_piplus_0_PY", Dplus_DTF_KS0_piplus_0_PY);
  float  Dplus_DTF_KS0_piplus_0_PZ[100]; ntp->SetBranchStatus("Dplus_DTF_KS0_piplus_0_PZ", 1); ntp->SetBranchAddress("Dplus_DTF_KS0_piplus_0_PZ", Dplus_DTF_KS0_piplus_0_PZ);
  float  Dplus_DTF_KS0_piplus_ID[100]; ntp->SetBranchStatus("Dplus_DTF_KS0_piplus_ID", 1); ntp->SetBranchAddress("Dplus_DTF_KS0_piplus_ID", Dplus_DTF_KS0_piplus_ID);
  float  Dplus_DTF_KS0_piplus_PX[100]; ntp->SetBranchStatus("Dplus_DTF_KS0_piplus_PX", 1); ntp->SetBranchAddress("Dplus_DTF_KS0_piplus_PX", Dplus_DTF_KS0_piplus_PX);
  float  Dplus_DTF_KS0_piplus_PY[100]; ntp->SetBranchStatus("Dplus_DTF_KS0_piplus_PY", 1); ntp->SetBranchAddress("Dplus_DTF_KS0_piplus_PY", Dplus_DTF_KS0_piplus_PY);
  float  Dplus_DTF_KS0_piplus_PZ[100]; ntp->SetBranchStatus("Dplus_DTF_KS0_piplus_PZ", 1); ntp->SetBranchAddress("Dplus_DTF_KS0_piplus_PZ", Dplus_DTF_KS0_piplus_PZ);
  float  Dplus_DTF_piplus_ID[100]; ntp->SetBranchStatus("Dplus_DTF_piplus_ID", 1); ntp->SetBranchAddress("Dplus_DTF_piplus_ID", Dplus_DTF_piplus_ID);
  float  Dplus_DTF_piplus_PX[100]; ntp->SetBranchStatus("Dplus_DTF_piplus_PX", 1); ntp->SetBranchAddress("Dplus_DTF_piplus_PX", Dplus_DTF_piplus_PX);
  float  Dplus_DTF_piplus_PY[100]; ntp->SetBranchStatus("Dplus_DTF_piplus_PY", 1); ntp->SetBranchAddress("Dplus_DTF_piplus_PY", Dplus_DTF_piplus_PY);
  float  Dplus_DTF_piplus_PZ[100]; ntp->SetBranchStatus("Dplus_DTF_piplus_PZ", 1); ntp->SetBranchAddress("Dplus_DTF_piplus_PZ", Dplus_DTF_piplus_PZ);
  
  //kinematics
  double Dplus_P; ntp->SetBranchStatus("Dplus_P", 1); ntp->SetBranchAddress("Dplus_P", &Dplus_P);
  double Dplus_PT; ntp->SetBranchStatus("Dplus_PT", 1); ntp->SetBranchAddress("Dplus_PT", &Dplus_PT);
  double Dplus_ETA; ntp->SetBranchStatus("Dplus_ETA", 1); ntp->SetBranchAddress("Dplus_ETA", &Dplus_ETA);
  double Dplus_PHI; ntp->SetBranchStatus("Dplus_PHI", 1); ntp->SetBranchAddress("Dplus_PHI", &Dplus_PHI);
  double Dplus_PX; ntp->SetBranchStatus("Dplus_PX", 1); ntp->SetBranchAddress("Dplus_PX", &Dplus_PX);
  double Dplus_PY; ntp->SetBranchStatus("Dplus_PY", 1); ntp->SetBranchAddress("Dplus_PY", &Dplus_PY);
  double Dplus_PZ; ntp->SetBranchStatus("Dplus_PZ", 1); ntp->SetBranchAddress("Dplus_PZ", &Dplus_PZ);

  double KS0_P; ntp->SetBranchStatus("KS0_P", 1); ntp->SetBranchAddress("KS0_P", &KS0_P);
  double KS0_PT; ntp->SetBranchStatus("KS0_PT", 1); ntp->SetBranchAddress("KS0_PT", &KS0_PT);
  double KS0_ETA; ntp->SetBranchStatus("KS0_ETA", 1); ntp->SetBranchAddress("KS0_ETA", &KS0_ETA);
  double KS0_PHI; ntp->SetBranchStatus("KS0_PHI", 1); ntp->SetBranchAddress("KS0_PHI", &KS0_PHI);
  double KS0_PX; ntp->SetBranchStatus("KS0_PX", 1); ntp->SetBranchAddress("KS0_PX", &KS0_PX);
  double KS0_PY; ntp->SetBranchStatus("KS0_PY", 1); ntp->SetBranchAddress("KS0_PY", &KS0_PY);
  double KS0_PZ; ntp->SetBranchStatus("KS0_PZ", 1); ntp->SetBranchAddress("KS0_PZ", &KS0_PZ);

  double piplus_P; ntp->SetBranchStatus("piplus_P", 1); ntp->SetBranchAddress("piplus_P", &piplus_P);
  double piplus_PT; ntp->SetBranchStatus("piplus_PT", 1); ntp->SetBranchAddress("piplus_PT", &piplus_PT);
  double piplus_ETA; ntp->SetBranchStatus("piplus_ETA", 1); ntp->SetBranchAddress("piplus_ETA", &piplus_ETA);
  double piplus_PHI; ntp->SetBranchStatus("piplus_PHI", 1); ntp->SetBranchAddress("piplus_PHI", &piplus_PHI);
  double piplus_PX; ntp->SetBranchStatus("piplus_PX", 1); ntp->SetBranchAddress("piplus_PX", &piplus_PX);
  double piplus_PY; ntp->SetBranchStatus("piplus_PY", 1); ntp->SetBranchAddress("piplus_PY", &piplus_PY);
  double piplus_PZ; ntp->SetBranchStatus("piplus_PZ", 1); ntp->SetBranchAddress("piplus_PZ", &piplus_PZ);

  double piminus_P; ntp->SetBranchStatus("piminus_P", 1); ntp->SetBranchAddress("piminus_P", &piminus_P);
  double piminus_PT; ntp->SetBranchStatus("piminus_PT", 1); ntp->SetBranchAddress("piminus_PT", &piminus_PT);
  double piminus_ETA; ntp->SetBranchStatus("piminus_ETA", 1); ntp->SetBranchAddress("piminus_ETA", &piminus_ETA);
  double piminus_PHI; ntp->SetBranchStatus("piminus_PHI", 1); ntp->SetBranchAddress("piminus_PHI", &piminus_PHI);
  double piminus_PX; ntp->SetBranchStatus("piminus_PX", 1); ntp->SetBranchAddress("piminus_PX", &piminus_PX);
  double piminus_PY; ntp->SetBranchStatus("piminus_PY", 1); ntp->SetBranchAddress("piminus_PY", &piminus_PY);
  double piminus_PZ; ntp->SetBranchStatus("piminus_PZ", 1); ntp->SetBranchAddress("piminus_PZ", &piminus_PZ);

  double hplus_P; ntp->SetBranchStatus("hplus_P", 1); ntp->SetBranchAddress("hplus_P", &hplus_P);
  double hplus_PT; ntp->SetBranchStatus("hplus_PT", 1); ntp->SetBranchAddress("hplus_PT", &hplus_PT);
  double hplus_ETA; ntp->SetBranchStatus("hplus_ETA", 1); ntp->SetBranchAddress("hplus_ETA", &hplus_ETA);
  double hplus_PHI; ntp->SetBranchStatus("hplus_PHI", 1); ntp->SetBranchAddress("hplus_PHI", &hplus_PHI);
  double hplus_PX; ntp->SetBranchStatus("hplus_PX", 1); ntp->SetBranchAddress("hplus_PX", &hplus_PX);
  double hplus_PY; ntp->SetBranchStatus("hplus_PY", 1); ntp->SetBranchAddress("hplus_PY", &hplus_PY);
  double hplus_PZ; ntp->SetBranchStatus("hplus_PZ", 1); ntp->SetBranchAddress("hplus_PZ", &hplus_PZ);

  //PID
  double piplus_PIDK; ntp->SetBranchStatus("piplus_PIDK", 1); ntp->SetBranchAddress("piplus_PIDK", &piplus_PIDK);
  double piminus_PIDK; ntp->SetBranchStatus("piminus_PIDK", 1); ntp->SetBranchAddress("piminus_PIDK", &piminus_PIDK);
  double hplus_PIDK; ntp->SetBranchStatus("hplus_PIDK", 1); ntp->SetBranchAddress("hplus_PIDK", &hplus_PIDK);
  double piplus_PIDp; ntp->SetBranchStatus("piplus_PIDp", 1); ntp->SetBranchAddress("piplus_PIDp", &piplus_PIDp);
  double piminus_PIDp; ntp->SetBranchStatus("piminus_PIDp", 1); ntp->SetBranchAddress("piminus_PIDp", &piminus_PIDp);
  double hplus_PIDp; ntp->SetBranchStatus("hplus_PIDp", 1); ntp->SetBranchAddress("hplus_PIDp", &hplus_PIDp);
  
  //Track
  double Dplus_IP_OWNPV; ntp->SetBranchStatus("Dplus_IP_OWNPV", 1); ntp->SetBranchAddress("Dplus_IP_OWNPV", &Dplus_IP_OWNPV);  
  double KS0_IP_OWNPV; ntp->SetBranchStatus("KS0_IP_OWNPV", 1); ntp->SetBranchAddress("KS0_IP_OWNPV", &KS0_IP_OWNPV);  
  double piplus_IP_OWNPV; ntp->SetBranchStatus("piplus_IP_OWNPV", 1); ntp->SetBranchAddress("piplus_IP_OWNPV", &piplus_IP_OWNPV); 
  double piminus_IP_OWNPV; ntp->SetBranchStatus("piminus_IP_OWNPV", 1); ntp->SetBranchAddress("piminus_IP_OWNPV", &piminus_IP_OWNPV);                                
  double hplus_IP_OWNPV; ntp->SetBranchStatus("hplus_IP_OWNPV", 1); ntp->SetBranchAddress("hplus_IP_OWNPV", &hplus_IP_OWNPV); 

  double Dplus_IPCHI2_OWNPV; ntp->SetBranchStatus("Dplus_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("Dplus_IPCHI2_OWNPV", &Dplus_IPCHI2_OWNPV);  
  double KS0_IPCHI2_OWNPV; ntp->SetBranchStatus("KS0_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("KS0_IPCHI2_OWNPV", &KS0_IPCHI2_OWNPV);  
  double piplus_IPCHI2_OWNPV; ntp->SetBranchStatus("piplus_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("piplus_IPCHI2_OWNPV", &piplus_IPCHI2_OWNPV); 
  double piminus_IPCHI2_OWNPV; ntp->SetBranchStatus("piminus_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("piminus_IPCHI2_OWNPV", &piminus_IPCHI2_OWNPV);                                
  double hplus_IPCHI2_OWNPV; ntp->SetBranchStatus("hplus_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("hplus_IPCHI2_OWNPV", &hplus_IPCHI2_OWNPV); 

  double Dplus_FD_OWNPV; ntp->SetBranchStatus("Dplus_FD_OWNPV", 1); ntp->SetBranchAddress("Dplus_FD_OWNPV", &Dplus_FD_OWNPV);  
  double Dplus_FDCHI2_OWNPV; ntp->SetBranchStatus("Dplus_FDCHI2_OWNPV", 1); ntp->SetBranchAddress("Dplus_FDCHI2_OWNPV", &Dplus_FDCHI2_OWNPV);  
  double KS0_FD_OWNPV; ntp->SetBranchStatus("KS0_FD_OWNPV", 1); ntp->SetBranchAddress("KS0_FD_OWNPV", &KS0_FD_OWNPV);  
  double KS0_FDCHI2_OWNPV; ntp->SetBranchStatus("KS0_FDCHI2_OWNPV", 1); ntp->SetBranchAddress("KS0_FDCHI2_OWNPV", &KS0_FDCHI2_OWNPV);  

  double piplus_TRACK_CHI2NDOF; ntp->SetBranchStatus("piplus_TRACK_CHI2NDOF", 1); ntp->SetBranchAddress("piplus_TRACK_CHI2NDOF", &piplus_TRACK_CHI2NDOF); 
  double piminus_TRACK_CHI2NDOF; ntp->SetBranchStatus("piminus_TRACK_CHI2NDOF", 1); ntp->SetBranchAddress("piminus_TRACK_CHI2NDOF", &piminus_TRACK_CHI2NDOF);                              
  double hplus_TRACK_CHI2NDOF; ntp->SetBranchStatus("hplus_TRACK_CHI2NDOF", 1); ntp->SetBranchAddress("hplus_TRACK_CHI2NDOF", &hplus_TRACK_CHI2NDOF);                              

  double piplus_TRACK_GhostProb; ntp->SetBranchStatus("piplus_TRACK_GhostProb", 1); ntp->SetBranchAddress("piplus_TRACK_GhostProb", &piplus_TRACK_GhostProb); 
  double piminus_TRACK_GhostProb; ntp->SetBranchStatus("piminus_TRACK_GhostProb", 1); ntp->SetBranchAddress("piminus_TRACK_GhostProb", &piminus_TRACK_GhostProb);                              
  double hplus_TRACK_GhostProb; ntp->SetBranchStatus("hplus_TRACK_GhostProb", 1); ntp->SetBranchAddress("hplus_TRACK_GhostProb", &hplus_TRACK_GhostProb);


  //geometrical
  double Dplus_DIRA_OWNPV; ntp->SetBranchStatus("Dplus_DIRA_OWNPV", 1); ntp->SetBranchAddress("Dplus_DIRA_OWNPV", &Dplus_DIRA_OWNPV);       

  double Dplus_DOCA_KS0_hplus; ntp->SetBranchStatus("Dplus_DOCA_KS0_hplus", 1); ntp->SetBranchAddress("Dplus_DOCA_KS0_hplus", &Dplus_DOCA_KS0_hplus);       
  double Dplus_DOCACHI2_KS0_hplus; ntp->SetBranchStatus("Dplus_DOCACHI2_KS0_hplus", 1); ntp->SetBranchAddress("Dplus_DOCACHI2_KS0_hplus", &Dplus_DOCACHI2_KS0_hplus);       

  double KS0_DOCA_piplus_piminus; ntp->SetBranchStatus("KS0_DOCA_piplus_piminus", 1); ntp->SetBranchAddress("KS0_DOCA_piplus_piminus", &KS0_DOCA_piplus_piminus);       
  double KS0_DOCACHI2_piplus_piminus; ntp->SetBranchStatus("KS0_DOCACHI2_piplus_piminus", 1); ntp->SetBranchAddress("KS0_DOCACHI2_piplus_piminus", &KS0_DOCACHI2_piplus_piminus);       
  //sums
  double Dplus_SUMPTChildren; ntp->SetBranchStatus("Dplus_SUMPTChildren", 1); ntp->SetBranchAddress("Dplus_SUMPTChildren", &Dplus_SUMPTChildren);       
  double KS0_SUMPTChildren; ntp->SetBranchStatus("KS0_SUMPTChildren", 1); ntp->SetBranchAddress("KS0_SUMPTChildren", &KS0_SUMPTChildren);       

  //Vertices
  float  PVX[100]; ntp->SetBranchStatus("PVX", 1); ntp->SetBranchAddress("PVX", PVX); 
  float  PVY[100]; ntp->SetBranchStatus("PVY", 1); ntp->SetBranchAddress("PVY", PVY); 
  float  PVZ[100]; ntp->SetBranchStatus("PVZ", 1); ntp->SetBranchAddress("PVZ", PVZ); 

  int nPVs; ntp->SetBranchStatus("nPVs", 1); ntp->SetBranchAddress("nPVs", &nPVs);   

  double Dplus_OWNPV_X; ntp->SetBranchStatus("Dplus_OWNPV_X", 1); ntp->SetBranchAddress("Dplus_OWNPV_X", &Dplus_OWNPV_X);   
  double Dplus_OWNPV_Y; ntp->SetBranchStatus("Dplus_OWNPV_Y", 1); ntp->SetBranchAddress("Dplus_OWNPV_Y", &Dplus_OWNPV_Y);   
  double Dplus_OWNPV_Z; ntp->SetBranchStatus("Dplus_OWNPV_Z", 1); ntp->SetBranchAddress("Dplus_OWNPV_Z", &Dplus_OWNPV_Z);   

  double Dplus_ENDVERTEX_X; ntp->SetBranchStatus("Dplus_ENDVERTEX_X", 1); ntp->SetBranchAddress("Dplus_ENDVERTEX_X", &Dplus_ENDVERTEX_X);   
  double Dplus_ENDVERTEX_Y; ntp->SetBranchStatus("Dplus_ENDVERTEX_Y", 1); ntp->SetBranchAddress("Dplus_ENDVERTEX_Y", &Dplus_ENDVERTEX_Y);   
  double Dplus_ENDVERTEX_Z; ntp->SetBranchStatus("Dplus_ENDVERTEX_Z", 1); ntp->SetBranchAddress("Dplus_ENDVERTEX_Z", &Dplus_ENDVERTEX_Z);   
  double KS0_ENDVERTEX_X; ntp->SetBranchStatus("KS0_ENDVERTEX_X", 1); ntp->SetBranchAddress("KS0_ENDVERTEX_X", &KS0_ENDVERTEX_X);   
  double KS0_ENDVERTEX_Y; ntp->SetBranchStatus("KS0_ENDVERTEX_Y", 1); ntp->SetBranchAddress("KS0_ENDVERTEX_Y", &KS0_ENDVERTEX_Y);   
  double KS0_ENDVERTEX_Z; ntp->SetBranchStatus("KS0_ENDVERTEX_Z", 1); ntp->SetBranchAddress("KS0_ENDVERTEX_Z", &KS0_ENDVERTEX_Z);   

  double Dplus_ENDVERTEX_CHI2; ntp->SetBranchStatus("Dplus_ENDVERTEX_CHI2", 1); ntp->SetBranchAddress("Dplus_ENDVERTEX_CHI2", &Dplus_ENDVERTEX_CHI2);   

  //Triggers
  bool Dplus_L0HadronDecision_TIS; ntp->SetBranchStatus("Dplus_L0HadronDecision_TIS", 1); ntp->SetBranchAddress("Dplus_L0HadronDecision_TIS", &Dplus_L0HadronDecision_TIS);
  bool Dplus_L0HadronDecision_TOS; ntp->SetBranchStatus("Dplus_L0HadronDecision_TOS", 1); ntp->SetBranchAddress("Dplus_L0HadronDecision_TOS", &Dplus_L0HadronDecision_TOS);
  bool Dplus_L0Global_TIS; ntp->SetBranchStatus("Dplus_L0Global_TIS", 1); ntp->SetBranchAddress("Dplus_L0Global_TIS", &Dplus_L0Global_TIS);
  bool Dplus_L0Global_TOS; ntp->SetBranchStatus("Dplus_L0Global_TOS", 1); ntp->SetBranchAddress("Dplus_L0Global_TOS", &Dplus_L0Global_TOS);
  bool Dplus_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("Dplus_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("Dplus_Hlt1TrackMVADecision_TIS", &Dplus_Hlt1TrackMVADecision_TIS);
  bool Dplus_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("Dplus_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("Dplus_Hlt1TrackMVADecision_TOS", &Dplus_Hlt1TrackMVADecision_TOS);
  bool piplus_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("piplus_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("piplus_Hlt1TrackMVADecision_TIS", &piplus_Hlt1TrackMVADecision_TIS);
  bool piplus_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("piplus_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("piplus_Hlt1TrackMVADecision_TOS", &piplus_Hlt1TrackMVADecision_TOS);
  bool piminus_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("piminus_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("piminus_Hlt1TrackMVADecision_TIS", &piminus_Hlt1TrackMVADecision_TIS);
  bool piminus_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("piminus_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("piminus_Hlt1TrackMVADecision_TOS", &piminus_Hlt1TrackMVADecision_TOS);
  bool hplus_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("hplus_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("hplus_Hlt1TrackMVADecision_TIS", &hplus_Hlt1TrackMVADecision_TIS);
  bool hplus_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("hplus_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("hplus_Hlt1TrackMVADecision_TOS", &hplus_Hlt1TrackMVADecision_TOS);

  bool Dplus_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("Dplus_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("Dplus_Hlt1TwoTrackMVADecision_TIS", &Dplus_Hlt1TwoTrackMVADecision_TIS);
  bool Dplus_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("Dplus_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("Dplus_Hlt1TwoTrackMVADecision_TOS", &Dplus_Hlt1TwoTrackMVADecision_TOS);
  bool piplus_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("piplus_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("piplus_Hlt1TwoTrackMVADecision_TIS", &piplus_Hlt1TwoTrackMVADecision_TIS);
  bool piplus_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("piplus_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("piplus_Hlt1TwoTrackMVADecision_TOS", &piplus_Hlt1TwoTrackMVADecision_TOS);
  bool piminus_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("piminus_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("piminus_Hlt1TwoTrackMVADecision_TIS", &piminus_Hlt1TwoTrackMVADecision_TIS);
  bool piminus_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("piminus_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("piminus_Hlt1TwoTrackMVADecision_TOS", &piminus_Hlt1TwoTrackMVADecision_TOS);
  bool hplus_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("hplus_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("hplus_Hlt1TwoTrackMVADecision_TIS", &hplus_Hlt1TwoTrackMVADecision_TIS);
  bool hplus_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("hplus_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("hplus_Hlt1TwoTrackMVADecision_TOS", &hplus_Hlt1TwoTrackMVADecision_TOS);
  

  //Event
  ULong64_t eventNumber; ntp->SetBranchStatus("eventNumber", 1); ntp->SetBranchAddress("eventNumber", &eventNumber); 
  UInt_t runNumber; ntp->SetBranchStatus("runNumber", 1); ntp->SetBranchAddress("runNumber", &runNumber); 
  unsigned int nCandidate; ntp->SetBranchStatus("nCandidate", 1); ntp->SetBranchAddress("nCandidate", &nCandidate); 
  int nVeloClusters; ntp->SetBranchStatus("nVeloClusters", 1); ntp->SetBranchAddress("nVeloClusters",&nVeloClusters);
  int nOTClusters; ntp->SetBranchStatus("nOTClusters", 1); ntp->SetBranchAddress("nOTClusters",&nOTClusters);
  ULong64_t totCandidates; ntp->SetBranchStatus("totCandidates", 1); ntp->SetBranchAddress("totCandidates", &totCandidates); 


  TFile * fOut = new TFile(outputFile, "RECREATE");
  TTree * ntpOut = new TTree(outputNtuple,outputNtuple);
  
  //masses
  double o_Dplus_M; ntpOut->Branch("Dplus_M", &o_Dplus_M); 
  double o_KS0_M; ntpOut->Branch("KS0_M", &o_KS0_M); 

  //tag
  int o_Dplus_ID; ntpOut->Branch("Dplus_ID", &o_Dplus_ID); 

  //lifetimes
  double o_Dplus_TAU; ntpOut->Branch("Dplus_TAU", &o_Dplus_TAU);
  double o_KS0_TAU; ntpOut->Branch("KS0_TAU", &o_KS0_TAU);

  //DTF
  double o_Dplus_DTF_M; ntpOut->Branch("Dplus_DTF_M", &o_Dplus_DTF_M); 
  double o_Dplus_DTF_ctau; ntpOut->Branch("Dplus_DTF_ctau", &o_Dplus_DTF_ctau); 
  double o_Dplus_DTF_nDOF; ntpOut->Branch("Dplus_DTF_nDOF", &o_Dplus_DTF_nDOF); 
  double o_Dplus_DTF_chi2; ntpOut->Branch("Dplus_DTF_chi2", &o_Dplus_DTF_chi2); 
  double o_Dplus_DTF_status; ntpOut->Branch("Dplus_DTF_status", &o_Dplus_DTF_status); 
  double o_Dplus_DTF_decayLength; ntpOut->Branch("Dplus_DTF_decayLength", &o_Dplus_DTF_decayLength); 
  int    o_Dplus_DTF_nPV; ntpOut->Branch("Dplus_DTF_nPV", &o_Dplus_DTF_nPV); 
  double o_Dplus_DTF_KS0_piplus_0_ID; ntpOut->Branch("Dplus_DTF_KS0_piplus_0_ID", &o_Dplus_DTF_KS0_piplus_0_ID); 
  double o_Dplus_DTF_KS0_piplus_0_PX; ntpOut->Branch("Dplus_DTF_KS0_piplus_0_PX", &o_Dplus_DTF_KS0_piplus_0_PX); 
  double o_Dplus_DTF_KS0_piplus_0_PY; ntpOut->Branch("Dplus_DTF_KS0_piplus_0_PY", &o_Dplus_DTF_KS0_piplus_0_PY); 
  double o_Dplus_DTF_KS0_piplus_0_PZ; ntpOut->Branch("Dplus_DTF_KS0_piplus_0_PZ", &o_Dplus_DTF_KS0_piplus_0_PZ); 
  double o_Dplus_DTF_KS0_piplus_ID; ntpOut->Branch("Dplus_DTF_KS0_piplus_ID", &o_Dplus_DTF_KS0_piplus_ID); 
  double o_Dplus_DTF_KS0_piplus_PX; ntpOut->Branch("Dplus_DTF_KS0_piplus_PX", &o_Dplus_DTF_KS0_piplus_PX); 
  double o_Dplus_DTF_KS0_piplus_PY; ntpOut->Branch("Dplus_DTF_KS0_piplus_PY", &o_Dplus_DTF_KS0_piplus_PY); 
  double o_Dplus_DTF_KS0_piplus_PZ; ntpOut->Branch("Dplus_DTF_KS0_piplus_PZ", &o_Dplus_DTF_KS0_piplus_PZ); 
  double o_Dplus_DTF_piplus_ID; ntpOut->Branch("Dplus_DTF_piplus_ID", &o_Dplus_DTF_piplus_ID); 
  double o_Dplus_DTF_piplus_PX; ntpOut->Branch("Dplus_DTF_piplus_PX", &o_Dplus_DTF_piplus_PX); 
  double o_Dplus_DTF_piplus_PY; ntpOut->Branch("Dplus_DTF_piplus_PY", &o_Dplus_DTF_piplus_PY); 
  double o_Dplus_DTF_piplus_PZ; ntpOut->Branch("Dplus_DTF_piplus_PZ", &o_Dplus_DTF_piplus_PZ); 
  
  
  //kinematics
  double o_Dplus_P; ntpOut->Branch("Dplus_P", &o_Dplus_P);   
  double o_Dplus_PT; ntpOut->Branch("Dplus_PT", &o_Dplus_PT);   
  double o_Dplus_ETA; ntpOut->Branch("Dplus_ETA", &o_Dplus_ETA);   
  double o_Dplus_PHI; ntpOut->Branch("Dplus_PHI", &o_Dplus_PHI);   
  double o_Dplus_PX; ntpOut->Branch("Dplus_PX", &o_Dplus_PX);   
  double o_Dplus_PY; ntpOut->Branch("Dplus_PY", &o_Dplus_PY);   
  double o_Dplus_PZ; ntpOut->Branch("Dplus_PZ", &o_Dplus_PZ);   

  double o_KS0_P; ntpOut->Branch("KS0_P", &o_KS0_P);   
  double o_KS0_PT; ntpOut->Branch("KS0_PT", &o_KS0_PT);   
  double o_KS0_ETA; ntpOut->Branch("KS0_ETA", &o_KS0_ETA);   
  double o_KS0_PHI; ntpOut->Branch("KS0_PHI", &o_KS0_PHI);   
  double o_KS0_PX; ntpOut->Branch("KS0_PX", &o_KS0_PX);   
  double o_KS0_PY; ntpOut->Branch("KS0_PY", &o_KS0_PY);   
  double o_KS0_PZ; ntpOut->Branch("KS0_PZ", &o_KS0_PZ);   

  /*double o_KS0_P;*/ ntpOut->Branch("X_P", &o_KS0_P);   
  /*double o_KS0_PT;*/ ntpOut->Branch("X_PT", &o_KS0_PT);   
  /*double o_KS0_ETA;*/ ntpOut->Branch("X_ETA", &o_KS0_ETA);   
  /*double o_KS0_PHI;*/ ntpOut->Branch("X_PHI", &o_KS0_PHI);   
  /*double o_KS0_PX;*/ ntpOut->Branch("X_PX", &o_KS0_PX);   
  /*double o_KS0_PY;*/ ntpOut->Branch("X_PY", &o_KS0_PY);   
  /*double o_KS0_PZ;*/ ntpOut->Branch("X_PZ", &o_KS0_PZ);   

  double o_piplus_P; ntpOut->Branch("piplus_P", &o_piplus_P);   
  double o_piplus_PT; ntpOut->Branch("piplus_PT", &o_piplus_PT);   
  double o_piplus_ETA; ntpOut->Branch("piplus_ETA", &o_piplus_ETA);   
  double o_piplus_PHI; ntpOut->Branch("piplus_PHI", &o_piplus_PHI);   
  double o_piplus_PX; ntpOut->Branch("piplus_PX", &o_piplus_PX);   
  double o_piplus_PY; ntpOut->Branch("piplus_PY", &o_piplus_PY);   
  double o_piplus_PZ; ntpOut->Branch("piplus_PZ", &o_piplus_PZ);   

  double o_piminus_P; ntpOut->Branch("piminus_P", &o_piminus_P);   
  double o_piminus_PT; ntpOut->Branch("piminus_PT", &o_piminus_PT);   
  double o_piminus_ETA; ntpOut->Branch("piminus_ETA", &o_piminus_ETA);   
  double o_piminus_PHI; ntpOut->Branch("piminus_PHI", &o_piminus_PHI);   
  double o_piminus_PX; ntpOut->Branch("piminus_PX", &o_piminus_PX);   
  double o_piminus_PY; ntpOut->Branch("piminus_PY", &o_piminus_PY);   
  double o_piminus_PZ; ntpOut->Branch("piminus_PZ", &o_piminus_PZ);   
  
  double o_hplus_P; ntpOut->Branch("hplus_P", &o_hplus_P);   
  double o_hplus_PT; ntpOut->Branch("hplus_PT", &o_hplus_PT);   
  double o_hplus_ETA; ntpOut->Branch("hplus_ETA", &o_hplus_ETA);   
  double o_hplus_PHI; ntpOut->Branch("hplus_PHI", &o_hplus_PHI);   
  double o_hplus_PX; ntpOut->Branch("hplus_PX", &o_hplus_PX);   
  double o_hplus_PY; ntpOut->Branch("hplus_PY", &o_hplus_PY);   
  double o_hplus_PZ; ntpOut->Branch("hplus_PZ", &o_hplus_PZ);   

  //PID
  double o_piplus_PIDK; ntpOut->Branch("piplus_PIDK", &o_piplus_PIDK);                    
  double o_piminus_PIDK; ntpOut->Branch("piminus_PIDK", &o_piminus_PIDK);                    
  double o_hplus_PIDK; ntpOut->Branch("hplus_PIDK", &o_hplus_PIDK);                    
  double o_piplus_PIDp; ntpOut->Branch("piplus_PIDp", &o_piplus_PIDp);                    
  double o_piminus_PIDp; ntpOut->Branch("piminus_PIDp", &o_piminus_PIDp);                    
  double o_hplus_PIDp; ntpOut->Branch("hplus_PIDp", &o_hplus_PIDp);                    

  //Track
  double o_Dplus_IP_OWNPV; ntpOut->Branch("Dplus_IP_OWNPV", &o_Dplus_IP_OWNPV); 
  double o_KS0_IP_OWNPV; ntpOut->Branch("KS0_IP_OWNPV", &o_KS0_IP_OWNPV); 
  double o_piplus_IP_OWNPV; ntpOut->Branch("piplus_IP_OWNPV", &o_piplus_IP_OWNPV); 
  double o_piminus_IP_OWNPV; ntpOut->Branch("piminus_IP_OWNPV", &o_piminus_IP_OWNPV); 
  double o_hplus_IP_OWNPV; ntpOut->Branch("hplus_IP_OWNPV", &o_hplus_IP_OWNPV); 

  double o_Dplus_IPCHI2_OWNPV; ntpOut->Branch("Dplus_IPCHI2_OWNPV", &o_Dplus_IPCHI2_OWNPV); 
  double o_KS0_IPCHI2_OWNPV; ntpOut->Branch("KS0_IPCHI2_OWNPV", &o_KS0_IPCHI2_OWNPV); 
  double o_piplus_IPCHI2_OWNPV; ntpOut->Branch("piplus_IPCHI2_OWNPV", &o_piplus_IPCHI2_OWNPV); 
  double o_piminus_IPCHI2_OWNPV; ntpOut->Branch("piminus_IPCHI2_OWNPV", &o_piminus_IPCHI2_OWNPV); 
  double o_hplus_IPCHI2_OWNPV; ntpOut->Branch("hplus_IPCHI2_OWNPV", &o_hplus_IPCHI2_OWNPV); 
  
  double o_Dplus_FD_OWNPV; ntpOut->Branch("Dplus_FD_OWNPV", &o_Dplus_FD_OWNPV);                     
  double o_Dplus_FDCHI2_OWNPV; ntpOut->Branch("Dplus_FDCHI2_OWNPV", &o_Dplus_FDCHI2_OWNPV);                     
  double o_KS0_FD_OWNPV; ntpOut->Branch("KS0_FD_OWNPV", &o_KS0_FD_OWNPV);                     
  double o_KS0_FDCHI2_OWNPV; ntpOut->Branch("KS0_FDCHI2_OWNPV", &o_KS0_FDCHI2_OWNPV);                     

  double o_piplus_TRACK_CHI2NDOF; ntpOut->Branch("piplus_TRACK_CHI2NDOF", &o_piplus_TRACK_CHI2NDOF);
  double o_piminus_TRACK_CHI2NDOF; ntpOut->Branch("piminus_TRACK_CHI2NDOF", &o_piminus_TRACK_CHI2NDOF);
  double o_hplus_TRACK_CHI2NDOF; ntpOut->Branch("hplus_TRACK_CHI2NDOF", &o_hplus_TRACK_CHI2NDOF);

  double o_piplus_TRACK_GhostProb; ntpOut->Branch("piplus_TRACK_GhostProb", &o_piplus_TRACK_GhostProb);
  double o_piminus_TRACK_GhostProb; ntpOut->Branch("piminus_TRACK_GhostProb", &o_piminus_TRACK_GhostProb);
  double o_hplus_TRACK_GhostProb; ntpOut->Branch("hplus_TRACK_GhostProb", &o_hplus_TRACK_GhostProb);

  //geometrical
  double o_Dplus_DIRA_OWNPV; ntpOut->Branch("Dplus_DIRA_OWNPV", &o_Dplus_DIRA_OWNPV);

  double o_Dplus_DOCA_KS0_hplus; ntpOut->Branch("Dplus_DOCA_KS0_hplus", &o_Dplus_DOCA_KS0_hplus);      
  double o_Dplus_DOCACHI2_KS0_hplus; ntpOut->Branch("Dplus_DOCACHI2_KS0_hplus", &o_Dplus_DOCACHI2_KS0_hplus);      
  double o_KS0_DOCA_piplus_piminus; ntpOut->Branch("KS0_DOCA_piplus_piminus", &o_KS0_DOCA_piplus_piminus);      
  double o_KS0_DOCACHI2_piplus_piminus; ntpOut->Branch("KS0_DOCACHI2_piplus_piminus", &o_KS0_DOCACHI2_piplus_piminus);      

  //sums
  double o_Dplus_SUMPTChildren; ntpOut->Branch("Dplus_SUMPTChildren", &o_Dplus_SUMPTChildren);                         
  double o_KS0_SUMPTChildren; ntpOut->Branch("KS0_SUMPTChildren", &o_KS0_SUMPTChildren);

  //Vertices
  double o_PVX; ntpOut->Branch("PVX", &o_PVX);                                                                                                                                                               
  double o_PVY; ntpOut->Branch("PVY", &o_PVY);                                                                                                                                                               
  double o_PVZ; ntpOut->Branch("PVZ", &o_PVZ);   

  int o_nPVs; ntpOut->Branch("nPVs", &o_nPVs);      

  double o_Dplus_OWNPV_X; ntpOut->Branch("Dplus_OWNPV_X", &o_Dplus_OWNPV_X);              
  double o_Dplus_OWNPV_Y; ntpOut->Branch("Dplus_OWNPV_Y", &o_Dplus_OWNPV_Y);              
  double o_Dplus_OWNPV_Z; ntpOut->Branch("Dplus_OWNPV_Z", &o_Dplus_OWNPV_Z);              

  double o_Dplus_ENDVERTEX_X; ntpOut->Branch("Dplus_ENDVERTEX_X", &o_Dplus_ENDVERTEX_X);              
  double o_Dplus_ENDVERTEX_Y; ntpOut->Branch("Dplus_ENDVERTEX_Y", &o_Dplus_ENDVERTEX_Y);              
  double o_Dplus_ENDVERTEX_Z; ntpOut->Branch("Dplus_ENDVERTEX_Z", &o_Dplus_ENDVERTEX_Z);              
  double o_KS0_ENDVERTEX_X; ntpOut->Branch("KS0_ENDVERTEX_X", &o_KS0_ENDVERTEX_X);              
  double o_KS0_ENDVERTEX_Y; ntpOut->Branch("KS0_ENDVERTEX_Y", &o_KS0_ENDVERTEX_Y);              
  double o_KS0_ENDVERTEX_Z; ntpOut->Branch("KS0_ENDVERTEX_Z", &o_KS0_ENDVERTEX_Z);              
  double o_Dplus_ENDVERTEX_CHI2; ntpOut->Branch("Dplus_ENDVERTEX_CHI2", &o_Dplus_ENDVERTEX_CHI2);              


  //Triggers
  int o_Dplus_L0HadronDecision_TIS; ntpOut->Branch("Dplus_L0HadronDecision_TIS", &o_Dplus_L0HadronDecision_TIS);   
  int o_Dplus_L0HadronDecision_TOS; ntpOut->Branch("Dplus_L0HadronDecision_TOS", &o_Dplus_L0HadronDecision_TOS);   
  int o_Dplus_L0Global_TIS; ntpOut->Branch("Dplus_L0Global_TIS", &o_Dplus_L0Global_TIS);   
  int o_Dplus_L0Global_TOS; ntpOut->Branch("Dplus_L0Global_TOS", &o_Dplus_L0Global_TOS);   

  int o_Dplus_Hlt1TrackMVADecision_TIS; ntpOut->Branch("Dplus_Hlt1TrackMVADecision_TIS", &o_Dplus_Hlt1TrackMVADecision_TIS);   
  int o_Dplus_Hlt1TrackMVADecision_TOS; ntpOut->Branch("Dplus_Hlt1TrackMVADecision_TOS", &o_Dplus_Hlt1TrackMVADecision_TOS);   
  int o_piplus_Hlt1TrackMVADecision_TIS; ntpOut->Branch("piplus_Hlt1TrackMVADecision_TIS", &o_piplus_Hlt1TrackMVADecision_TIS);   
  int o_piplus_Hlt1TrackMVADecision_TOS; ntpOut->Branch("piplus_Hlt1TrackMVADecision_TOS", &o_piplus_Hlt1TrackMVADecision_TOS);   
  int o_piminus_Hlt1TrackMVADecision_TIS; ntpOut->Branch("piminus_Hlt1TrackMVADecision_TIS", &o_piminus_Hlt1TrackMVADecision_TIS);   
  int o_piminus_Hlt1TrackMVADecision_TOS; ntpOut->Branch("piminus_Hlt1TrackMVADecision_TOS", &o_piminus_Hlt1TrackMVADecision_TOS);   
  int o_hplus_Hlt1TrackMVADecision_TIS; ntpOut->Branch("hplus_Hlt1TrackMVADecision_TIS", &o_hplus_Hlt1TrackMVADecision_TIS);   
  int o_hplus_Hlt1TrackMVADecision_TOS; ntpOut->Branch("hplus_Hlt1TrackMVADecision_TOS", &o_hplus_Hlt1TrackMVADecision_TOS);   
  
  int o_Dplus_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("Dplus_Hlt1TwoTrackMVADecision_TIS", &o_Dplus_Hlt1TwoTrackMVADecision_TIS);   
  int o_Dplus_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("Dplus_Hlt1TwoTrackMVADecision_TOS", &o_Dplus_Hlt1TwoTrackMVADecision_TOS);   
  int o_piplus_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("piplus_Hlt1TwoTrackMVADecision_TIS", &o_piplus_Hlt1TwoTrackMVADecision_TIS);   
  int o_piplus_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("piplus_Hlt1TwoTrackMVADecision_TOS", &o_piplus_Hlt1TwoTrackMVADecision_TOS);   
  int o_piminus_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("piminus_Hlt1TwoTrackMVADecision_TIS", &o_piminus_Hlt1TwoTrackMVADecision_TIS);   
  int o_piminus_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("piminus_Hlt1TwoTrackMVADecision_TOS", &o_piminus_Hlt1TwoTrackMVADecision_TOS);   
  int o_hplus_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("hplus_Hlt1TwoTrackMVADecision_TIS", &o_hplus_Hlt1TwoTrackMVADecision_TIS);   
  int o_hplus_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("hplus_Hlt1TwoTrackMVADecision_TOS", &o_hplus_Hlt1TwoTrackMVADecision_TOS);   
  
  //Event
  ULong64_t o_eventNumber; ntpOut->Branch("eventNumber", &o_eventNumber); 
  UInt_t o_runNumber; ntpOut->Branch("runNumber", &o_runNumber); 
  unsigned int o_nCandidate; ntpOut->Branch("nCandidate",&o_nCandidate);
  int o_nVeloClusters; ntpOut->Branch("nVeloClusters",&o_nVeloClusters);
  int o_nOTClusters; ntpOut->Branch("nOTClusters",&o_nOTClusters);
  ULong64_t o_totCandidates; ntpOut->Branch("totCandidates", &o_totCandidates);

  //Derived quantities
  TVector3 v_Dplus_OWNPV, v_Dplus_ENDVERTEX, p_Dplus; 
  TVector3 p_beam;
  TVector3 vecProduct;
  double o_Dplus_TIP_OWNPV; ntpOut->Branch("Dplus_TIP_OWNPV", &o_Dplus_TIP_OWNPV);                     
  bool o_isPrompt; ntpOut->Branch("isPrompt", &o_isPrompt);
  
  ///DTF calculated quantities
  TVector3 v_Dplus_DTF_KS0_piplus0, v_Dplus_DTF_KS0_piplus, v_Dplus_DTF_piplus, v_Dplus_DTF_KS0, v_Dplus_DTF;
  double o_Dplus_DTF_P; ntpOut->Branch("Dplus_DTF_P", &o_Dplus_DTF_P);
  double o_Dplus_DTF_PT; ntpOut->Branch("Dplus_DTF_PT", &o_Dplus_DTF_PT);
  double o_Dplus_DTF_PHI; ntpOut->Branch("Dplus_DTF_PHI", &o_Dplus_DTF_PHI);
  double o_Dplus_DTF_ETA; ntpOut->Branch("Dplus_DTF_ETA", &o_Dplus_DTF_ETA);
  double o_Dplus_DTF_TAU; ntpOut->Branch("Dplus_DTF_TAU", &o_Dplus_DTF_TAU);

  double o_KS0_DTF_P; ntpOut->Branch("KS0_DTF_P", &o_KS0_DTF_P);
  double o_KS0_DTF_PT; ntpOut->Branch("KS0_DTF_PT", &o_KS0_DTF_PT);
  double o_KS0_DTF_PHI; ntpOut->Branch("KS0_DTF_PHI", &o_KS0_DTF_PHI);
  double o_KS0_DTF_ETA; ntpOut->Branch("KS0_DTF_ETA", &o_KS0_DTF_ETA);

  
  double o_hplus_DTF_P; ntpOut->Branch("hplus_DTF_P", &o_hplus_DTF_P);
  double o_hplus_DTF_PT; ntpOut->Branch("hplus_DTF_PT", &o_hplus_DTF_PT);
  double o_hplus_DTF_PHI; ntpOut->Branch("hplus_DTF_PHI", &o_hplus_DTF_PHI);
  double o_hplus_DTF_ETA; ntpOut->Branch("hplus_DTF_ETA", &o_hplus_DTF_ETA);

  //Armenteros-Podolanski variables for Pi from KS0
  double piplus_Plong(0.), piminus_Plong(0.);
  double o_alfa; ntpOut->Branch("alfa", &o_alfa);
  double o_piplus_Ptrans; ntpOut->Branch("piplus_Ptrans", &o_piplus_Ptrans); 
  double o_piminus_Ptrans; ntpOut->Branch("piminus_Ptrans", &o_piminus_Ptrans); 

  //different mass hypotheses
  double o_m_KS0p; ntpOut->Branch("m_KS0p", &o_m_KS0p);  
  double o_m_KS0K; ntpOut->Branch("m_KS0K", &o_m_KS0K);  
  double o_m_ppi; ntpOut->Branch("m_ppi", &o_m_ppi); 
  double o_BDT; ntpOut->Branch("BDT", &o_BDT); 
  

  
  TH1D * h_KS0_M = new TH1D("h_KS0_M","h_KS0_M",100,478,518);
  TH1D * h_KS0_M_bkg = new TH1D("h_KS0_M_bkg","h_KS0_M_bkg",100,478,518);
  TH1D * h_TIP = new TH1D("h_TIP","h_TIP",100,-0.2,0.2);
  TH1D * h_X_P = new TH1D("h_X_P","h_X_P",100,0,140);
  TH1D * h_X_diffPTs = new TH1D("h_X_diffPTs","h_X_diffPTs",100,0,12e3);
  TH1D * h_X_PT = new TH1D("h_X_PT","h_X_PT",100,0,12e3);
  TH1D * h_X_ETA = new TH1D("h_X_ETA","h_X_ETA",100,1.5,5.5);
  TH2D * h_Dphp_PTPT = new TH2D("h_Dphp_PTPT","h_Dphp_PTPT",200,1e3,15e3,200,1e3,10e3);
  TH2D * h_Dphp_ETAETA = new TH2D("h_Dphp_ETAETA","h_Dphp_ETAETA",100,1.5,5.5,100,1.5,5.5);
  TH2D * h_Dphp_PHIPHI = new TH2D("h_Dphp_PHIPHI","h_Dphp_PHIPHI",100,-3.1416,3.1416,100,-3.1416,3.1416);

  TH1D * h_ETAETA_diff = new TH1D("h_ETAETA_diff","h_ETAETA_diff",100,0,1);
  TH1D * h_PHIPHI_diff = new TH1D("h_PHIPHI_diff","h_PHIPHI_diff",100,0,1);
  
  TH1D * h_DTFmass_plus = new TH1D("h_DTFmass_plus", "h_DTFmass_plus", 500, 1800, 1930);
  TH1D * h_DTFmass_minus = new TH1D("h_DTFmass_minus", "h_DTFmass_minus", 500, 1800, 1930);
  TH1D * h_mass_plus = new TH1D("h_mass_plus", "h_mass_plus", 500, 1800, 1930);
  TH1D * h_mass_minus = new TH1D("h_mass_minus", "h_mass_minus", 500, 1800, 1930);
  TH1D * h_mppi = new TH1D("h_mppi","h_mppi",400,1000,1400);
  TH1D * h_mKS0p = new TH1D("h_mKS0p","h_mKS0p",400,2000,2400);
  TH1D * h_mKS0K = new TH1D("h_mKS0K","h_mKS0K",400,1800,2200);

  TH2D * h_hplus_pzpx_fidCut_plus = new TH2D("h_hplus_pzpx_fidCut_plus","h_hplus_pzpx_fidCut_plus",1000,0,300,1000,-10,10);
  TH2D * h_hplus_pzpx_fidCut_minus = new TH2D("h_hplus_pzpx_fidCut_minus","h_hplus_pzpx_fidCut_minus",1000,0,300,1000,-10,10);
  TH2D * h_hplus_pzpx_nofidCut_plus = new TH2D("h_hplus_pzpx_nofidCut_plus","h_hplus_pzpx_nofidCut_plus",1000,0,300,1000,-10,10);
  TH2D * h_hplus_pzpx_nofidCut_minus = new TH2D("h_hplus_pzpx_nofidCut_minus","h_hplus_pzpx_nofidCut_minus",1000,0,300,1000,-10,10);

  TH2D * h_hplus_pzpx_fidCut_CloseToBeamPipeVertical_plus = new TH2D("h_hplus_pzpx_fidCut_CloseToBeamPipeVertical_plus","h_hplus_pzpx_fidCut_CloseToBeamPipeVertical_plus",1000,0,300,1000,-10,10);
  TH2D * h_hplus_pzpx_fidCut_CloseToBeamPipeVertical_minus = new TH2D("h_hplus_pzpx_fidCut_CloseToBeamPipeVertical_minus","h_hplus_pzpx_fidCut_CloseToBeamPipeVertical_minus",1000,0,300,1000,-10,10);
  TH2D * h_hplus_pzpx_nofidCut_CloseToBeamPipeVertical_plus = new TH2D("h_hplus_pzpx_nofidCut_CloseToBeamPipeVertical_plus","h_hplus_pzpx_nofidCut_CloseToBeamPipeVertical_plus",1000,0,300,1000,-10,10);
  TH2D * h_hplus_pzpx_nofidCut_CloseToBeamPipeVertical_minus = new TH2D("h_hplus_pzpx_nofidCut_CloseToBeamPipeVertical_minus","h_hplus_pzpx_nofidCut_CloseToBeamPipeVertical_minus",1000,0,300,1000,-10,10);
 
  
  
  //  TMVA::Reader * reader =  new TMVA::Reader( MVA::var_name, "!Color:!Silent", true );;
  //  float  var_spectator;
  //  Float_t * var_spectator_pointer;

  //  if(BDTcut) {
  //    ntp->SetBranchStatus("Dplus_M",1); ntp->SetBranchAddress("Dplus_M",&var_spectator);
  //    var_spectator_pointer  = &var_spectator;
  //    reader->AddSpectator("Dplus_M", var_spectator_pointer); 
  //    reader->BookMVA( "BDT","/home/LHCB/fferrari/ACPKK/ADKpi/selection/KS0Pi/weights/TMVA_"+year+"_BDT.weights.xml");
  //    }
 
  // std::vector<float> var_value;
  
  double BDTval(0.);
  
  int nEntries = ntp->GetEntries();

  bool FiducialCut = true;
  bool TriggerCut     = true;
  bool HarmonizationCut = true;
  bool KS0flightdistance = true;
  bool KS0massrange = true;
  bool FitRange = true;
  
  //Booleans
  /*
    ntpOut->Branch("HarmonizationCut_1D_X",&HarmonizationCut_1D_X);
    ntpOut->Branch("HarmonizationCut_1D",&HarmonizationCut_1D);
    ntpOut->Branch("HarmonizationCut_2D_hD",&HarmonizationCut_2D_hD);
  */
  ntpOut->Branch("HarmonizationCut",&HarmonizationCut);

  for(int i = 0; i < nEntries; i++) {
    ntp->GetEntry(i);
    
    TriggerCut = Dplus_L0Global_TIS&&hplus_Hlt1TrackMVADecision_TOS;
    //TriggerCut = (Dplus_L0Global_TIS || Dplus_L0HadronDecision_TOS)&&Dplus_Hlt1TrackMVADecision_TOS;//Serena
    if(!use_pGun_all)
      if(!(TriggerCut)) continue;
    
    //Armenteros plot variables
    piplus_Plong = (piplus_PX*KS0_PX + piplus_PY*KS0_PY + piplus_PZ*KS0_PZ)/sqrt(KS0_PX*KS0_PX+KS0_PY*KS0_PY+KS0_PZ*KS0_PZ);
    piminus_Plong = (piminus_PX*KS0_PX + piminus_PY*KS0_PY + piminus_PZ*KS0_PZ)/sqrt(KS0_PX*KS0_PX+KS0_PY*KS0_PY+KS0_PZ*KS0_PZ);
    o_piplus_Ptrans = sqrt(piplus_P*piplus_P - piplus_Plong*piplus_Plong);
    o_piminus_Ptrans = sqrt(piminus_P*piminus_P - piminus_Plong*piminus_Plong);
    o_alfa = (piplus_Plong-piminus_Plong)/(piplus_Plong+piminus_Plong);

    //different mass hypotheses
    o_m_KS0p = sqrt(PdgMass::mproton_PDG*PdgMass::mproton_PDG+PdgMass::mKS0_PDG*PdgMass::mKS0_PDG+
		    2*(sqrt((PdgMass::mproton_PDG*PdgMass::mproton_PDG+hplus_P*hplus_P)*(PdgMass::mKS0_PDG*PdgMass::mKS0_PDG+KS0_P*KS0_P))-
		       (hplus_PX*KS0_PX+hplus_PY*KS0_PY+hplus_PZ*KS0_PZ)));
    
    o_m_KS0K = sqrt(PdgMass::mKplus_PDG*PdgMass::mKplus_PDG+PdgMass::mKS0_PDG*PdgMass::mKS0_PDG+
		    2*(sqrt((PdgMass::mKplus_PDG*PdgMass::mKplus_PDG+hplus_P*hplus_P)*(PdgMass::mKS0_PDG*PdgMass::mKS0_PDG+KS0_P*KS0_P))-
		       (hplus_PX*KS0_PX+hplus_PY*KS0_PY+hplus_PZ*KS0_PZ)));
    
    if(Dplus_ID>0) {
      o_m_ppi  = sqrt(PdgMass::mproton_PDG*PdgMass::mproton_PDG+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG+
		      2*(sqrt((PdgMass::mproton_PDG*PdgMass::mproton_PDG+piplus_P*piplus_P)*(PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG+piminus_P*piminus_P))-
			 (piplus_PX*piminus_PX+piplus_PY*piminus_PY+piplus_PZ*piminus_PZ)));
    }
    else {
      o_m_ppi  = sqrt(PdgMass::mproton_PDG*PdgMass::mproton_PDG+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG+
		      2*(sqrt((PdgMass::mproton_PDG*PdgMass::mproton_PDG+piminus_P*piminus_P)*(PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG+piplus_P*piplus_P))-
			 (piplus_PX*piminus_PX+piplus_PY*piminus_PY+piplus_PZ*piminus_PZ)));
    }


    h_mppi->Fill(o_m_ppi);
    h_mKS0p->Fill(o_m_KS0p);
    h_mKS0K->Fill(o_m_KS0K);
   
    //isPrompt definition with TIP
    v_Dplus_OWNPV.SetXYZ(Dplus_OWNPV_X, Dplus_OWNPV_Y, Dplus_OWNPV_Z);      
    v_Dplus_ENDVERTEX.SetXYZ(Dplus_ENDVERTEX_X, Dplus_ENDVERTEX_Y, Dplus_ENDVERTEX_Z);        
    p_Dplus.SetXYZ(Dplus_PX, Dplus_PY, Dplus_PZ);      
    p_beam.SetXYZ(0., 0., 1.);       
    vecProduct = p_beam.Cross(p_Dplus);  
    o_Dplus_TIP_OWNPV = vecProduct.Dot(v_Dplus_ENDVERTEX - v_Dplus_OWNPV) / vecProduct.Mag();
    //o_isPrompt = Dplus_IPCHI2_OWNPV<9;
    o_isPrompt = abs(o_Dplus_TIP_OWNPV)<0.040;
    h_TIP->Fill(o_Dplus_TIP_OWNPV);
    if(doSelection)
      if(!use_pGun_all)
	if(!o_isPrompt) continue;
   
  
    if(doSelection) {
      KS0massrange = abs(PdgMass::mKS0_PDG-KS0_M)<15;
      KS0flightdistance = KS0_ENDVERTEX_Z-Dplus_ENDVERTEX_Z>20;
      FitRange = Dplus_M>1800&&Dplus_M<1930;
    }
    if(!use_pGun_all) {
      if(!(FitRange)) continue;
     
      h_KS0_M->Fill(KS0_M);
      if(!(KS0flightdistance)) { 
	h_KS0_M_bkg->Fill(KS0_M);
    	continue;
      }
      if(!KS0massrange) continue;
    }
  
    //TruthMatching
    if(isPGun) {
      //Exclude category 60 --- the consequence is that you remove some badly reconstructed signal which probably has a worse mass resolution, so you will get an overoptimistic lineshape.
      //Include category 60 --- the consequence is that you have to model the real ghosts which will form a background to your signal events.
      if (Dplus_BKGCAT >= 60) continue;
      if (abs(Dplus_TRUEID)!=PdgCode::D) continue;
      if (abs(hplus_TRUEID)!=PdgCode::Pi) continue;
      if (hplus_MC_MOTHER_ID!=Dplus_TRUEID) continue;
      if (abs(KS0_TRUEID)!=PdgCode::K0s) continue;
      if (KS0_MC_MOTHER_ID!=Dplus_TRUEID) continue;
      
      if(!(hplus_TRUEP > 0 && piplus_TRUEP > 0 && piminus_TRUEP > 0)) continue;
    }

    //lines coordinates for fiducial cuts (PZ,PX)
    // P1(2500,0)      P2(35000,10000)
    // P1(2500,0)      P2(35000,-10000)
    // P1(90000,+500)  P2(300000,+3000)
    // P1(90000,-500)  P2(300000,-3000)
    // P1(90000,+3000) P2(300000,+5000)
    // P1(90000,-3000) P2(300000,-5000)
  
    if(Dplus_ID>0) {
      h_hplus_pzpx_nofidCut_plus->Fill(hplus_PZ/1e3,hplus_PX/1e3);
      if (TMath::Abs(hplus_PY/hplus_PZ) < 0.2)
	h_hplus_pzpx_nofidCut_CloseToBeamPipeVertical_plus->Fill(hplus_PZ/1e3,hplus_PX/1e3);	
    }
    else {
      h_hplus_pzpx_nofidCut_minus->Fill(hplus_PZ/1e3,hplus_PX/1e3); 
      if (TMath::Abs(hplus_PY/hplus_PZ) < 0.2)
	h_hplus_pzpx_nofidCut_CloseToBeamPipeVertical_minus->Fill(hplus_PZ/1e3,hplus_PX/1e3);	
    }
    
    h_X_P->Fill(KS0_P/1e3);
    h_X_PT->Fill(KS0_PT);
    h_X_ETA->Fill(KS0_ETA);
    h_Dphp_PTPT->Fill(Dplus_PT,hplus_PT);
    h_Dphp_ETAETA->Fill(Dplus_ETA,hplus_ETA);
    h_Dphp_PHIPHI->Fill(Dplus_PHI,hplus_PHI);
   
    h_X_diffPTs->Fill(Dplus_PT - hplus_PT);
    h_ETAETA_diff->Fill(TMath::Abs(Dplus_ETA-hplus_ETA));
    h_PHIPHI_diff->Fill(TMath::Abs(Dplus_PHI-hplus_PHI));
    if(doSelection) {
      HarmonizationCut = KS0_P>30e3 && Dplus_PT - hplus_PT > 2280 
	&& TMath::Abs(Dplus_PHI-hplus_PHI) < 0.4 && TMath::Abs(Dplus_ETA-hplus_ETA) < 0.4
										      //&& Dplus_PT>4e3 && Dplus_PT<14e3 && Dplus_ETA>2.2 && Dplus_ETA<4.3 && hplus_PT>1.5e3 && hplus_PT<8.5e3 && hplus_ETA>2.2 && hplus_ETA<4.3
&& Dplus_PT>4.2e3 && Dplus_PT<11e3 && Dplus_ETA>2.3 && Dplus_ETA<4.2 && hplus_PT>1.6e3 && hplus_PT<6e3 && hplus_ETA>2.2 && hplus_ETA<4.2;
      //if(!HarmonizationCut) continue;
    }

     
   
    if(BDTcut) {
    }
    
    
    
    if(doFiducial) {
      FiducialCut = (abs(hplus_PY/hplus_PZ)>0.02 && abs(hplus_PX)<(hplus_PZ-2500)/3.25)||      
	(abs(hplus_PY/hplus_PZ)<0.02 && abs(hplus_PX)<(hplus_PZ-2500)/3.25 && !((abs(hplus_PX)>(hplus_PZ-90000)/92.)&&(3*(hplus_PZ-90000)/210+2000)>abs(hplus_PX)&&hplus_PZ>90000));
      if(!use_pGun_all)
	if(!(FiducialCut)) continue;
    
      if(Dplus_ID>0) {
	h_hplus_pzpx_fidCut_plus->Fill(hplus_PZ/1e3,hplus_PX/1e3);
      }
      else {
	h_hplus_pzpx_fidCut_minus->Fill(hplus_PZ/1e3,hplus_PX/1e3); 
      }
      
    }
    
    o_Dplus_M = Dplus_M;
    o_KS0_M = KS0_M;

    o_Dplus_ID = Dplus_ID;

    o_Dplus_TAU = Dplus_TAU*1000; //in ps
    o_KS0_TAU = KS0_TAU*1000; //in ps

    o_Dplus_DTF_M = Dplus_DTF_M[0];
    o_Dplus_DTF_ctau = Dplus_DTF_ctau[0];
    o_Dplus_DTF_nDOF = Dplus_DTF_nDOF[0];
    o_Dplus_DTF_chi2 = Dplus_DTF_chi2[0];
    o_Dplus_DTF_status = Dplus_DTF_status[0];
    o_Dplus_DTF_decayLength = Dplus_DTF_decayLength[0];
    o_Dplus_DTF_nPV = Dplus_DTF_nPV[0];                                                                                        
    o_Dplus_DTF_KS0_piplus_0_ID = Dplus_DTF_KS0_piplus_0_ID[0];  
    o_Dplus_DTF_KS0_piplus_0_PX = Dplus_DTF_KS0_piplus_0_PX[0];  
    o_Dplus_DTF_KS0_piplus_0_PY = Dplus_DTF_KS0_piplus_0_PY[0];  
    o_Dplus_DTF_KS0_piplus_0_PZ = Dplus_DTF_KS0_piplus_0_PZ[0];  
    o_Dplus_DTF_KS0_piplus_ID = Dplus_DTF_KS0_piplus_ID[0];  
    o_Dplus_DTF_KS0_piplus_PX = Dplus_DTF_KS0_piplus_PX[0];  
    o_Dplus_DTF_KS0_piplus_PY = Dplus_DTF_KS0_piplus_PY[0];  
    o_Dplus_DTF_KS0_piplus_PZ = Dplus_DTF_KS0_piplus_PZ[0];  
    o_Dplus_DTF_piplus_ID = Dplus_DTF_piplus_ID[0];  
    o_Dplus_DTF_piplus_PX = Dplus_DTF_piplus_PX[0];  
    o_Dplus_DTF_piplus_PY = Dplus_DTF_piplus_PY[0];  
    o_Dplus_DTF_piplus_PZ = Dplus_DTF_piplus_PZ[0];  


    o_Dplus_P = Dplus_P;
    o_Dplus_PT = Dplus_PT;
    o_Dplus_ETA = Dplus_ETA;
    o_Dplus_PHI = Dplus_PHI;
    o_Dplus_PX = Dplus_PX;
    o_Dplus_PY = Dplus_PY;
    o_Dplus_PZ = Dplus_PZ;

    o_KS0_P = KS0_P;
    o_KS0_PT = KS0_PT;
    o_KS0_ETA = KS0_ETA;
    o_KS0_PHI = KS0_PHI;
    o_KS0_PX = KS0_PX;
    o_KS0_PY = KS0_PY;
    o_KS0_PZ = KS0_PZ;

    o_piplus_P = piplus_P;
    o_piplus_PT = piplus_PT;
    o_piplus_ETA = piplus_ETA;
    o_piplus_PHI = piplus_PHI;
    o_piplus_PX = piplus_PX;
    o_piplus_PY = piplus_PY;
    o_piplus_PZ = piplus_PZ;

    o_piminus_P = piminus_P;
    o_piminus_PT = piminus_PT;
    o_piminus_ETA = piminus_ETA;
    o_piminus_PHI = piminus_PHI;
    o_piminus_PX = piminus_PX;
    o_piminus_PY = piminus_PY;
    o_piminus_PZ = piminus_PZ;

    o_hplus_P = hplus_P;
    o_hplus_PT = hplus_PT;
    o_hplus_ETA = hplus_ETA;
    o_hplus_PHI = hplus_PHI;
    o_hplus_PX = hplus_PX;
    o_hplus_PY = hplus_PY;
    o_hplus_PZ = hplus_PZ;

    o_piplus_PIDK = piplus_PIDK;          
    o_piminus_PIDK = piminus_PIDK;                         
    o_hplus_PIDK = hplus_PIDK;                                                                                                     
    o_piplus_PIDp = piplus_PIDp;          
    o_piminus_PIDp = piminus_PIDp;                         
    o_hplus_PIDp = hplus_PIDp;                                                                                                     

    o_Dplus_IP_OWNPV = Dplus_IP_OWNPV;
    o_KS0_IP_OWNPV = KS0_IP_OWNPV;
    o_piplus_IP_OWNPV = piplus_IP_OWNPV;
    o_piminus_IP_OWNPV = piminus_IP_OWNPV;
    o_hplus_IP_OWNPV = hplus_IP_OWNPV;

    o_Dplus_IPCHI2_OWNPV = Dplus_IPCHI2_OWNPV;
    o_KS0_IPCHI2_OWNPV = KS0_IPCHI2_OWNPV;
    o_piplus_IPCHI2_OWNPV = piplus_IPCHI2_OWNPV;
    o_piminus_IPCHI2_OWNPV = piminus_IPCHI2_OWNPV;
    o_hplus_IPCHI2_OWNPV = hplus_IPCHI2_OWNPV;

    o_Dplus_FD_OWNPV = Dplus_FD_OWNPV;
    o_Dplus_FDCHI2_OWNPV = Dplus_FDCHI2_OWNPV;
    o_KS0_FD_OWNPV = KS0_FD_OWNPV;
    o_KS0_FDCHI2_OWNPV = KS0_FDCHI2_OWNPV;

    o_piplus_TRACK_CHI2NDOF = piplus_TRACK_CHI2NDOF;             
    o_piminus_TRACK_CHI2NDOF = piminus_TRACK_CHI2NDOF;                   
    o_hplus_TRACK_CHI2NDOF = hplus_TRACK_CHI2NDOF;                                                                                                                                                                                       
    o_piplus_TRACK_GhostProb = piplus_TRACK_GhostProb;             
    o_piminus_TRACK_GhostProb = piminus_TRACK_GhostProb;                   
    o_hplus_TRACK_GhostProb = hplus_TRACK_GhostProb;                                                                                                                              

    o_Dplus_DIRA_OWNPV = Dplus_DIRA_OWNPV;
    o_Dplus_DOCA_KS0_hplus = Dplus_DOCA_KS0_hplus;
    o_Dplus_DOCACHI2_KS0_hplus = Dplus_DOCACHI2_KS0_hplus;
    o_KS0_DOCA_piplus_piminus = KS0_DOCA_piplus_piminus;
    o_KS0_DOCACHI2_piplus_piminus = KS0_DOCACHI2_piplus_piminus;
    
    o_Dplus_SUMPTChildren = Dplus_SUMPTChildren;
    o_KS0_SUMPTChildren = KS0_SUMPTChildren;
    
                             
    o_PVX = PVX[0];
    o_PVY = PVY[0];
    o_PVZ = PVZ[0];

    o_nPVs = nPVs;

    o_Dplus_OWNPV_X = Dplus_OWNPV_X;
    o_Dplus_OWNPV_Y = Dplus_OWNPV_Y;
    o_Dplus_OWNPV_Z = Dplus_OWNPV_Z;

    o_Dplus_ENDVERTEX_X = Dplus_ENDVERTEX_X;
    o_Dplus_ENDVERTEX_Y = Dplus_ENDVERTEX_Y;
    o_Dplus_ENDVERTEX_Z = Dplus_ENDVERTEX_Z;

    o_KS0_ENDVERTEX_X = KS0_ENDVERTEX_X;
    o_KS0_ENDVERTEX_Y = KS0_ENDVERTEX_Y;
    o_KS0_ENDVERTEX_Z = KS0_ENDVERTEX_Z;

    o_Dplus_ENDVERTEX_CHI2 = Dplus_ENDVERTEX_CHI2;
    
    o_Dplus_L0HadronDecision_TIS = Dplus_L0HadronDecision_TIS;
    o_Dplus_L0HadronDecision_TOS = Dplus_L0HadronDecision_TOS;
    o_Dplus_Hlt1TrackMVADecision_TIS = Dplus_Hlt1TrackMVADecision_TIS;
    o_Dplus_Hlt1TrackMVADecision_TOS = Dplus_Hlt1TrackMVADecision_TOS;
    o_piplus_Hlt1TrackMVADecision_TIS = piplus_Hlt1TrackMVADecision_TIS;
    o_piplus_Hlt1TrackMVADecision_TOS = piplus_Hlt1TrackMVADecision_TOS;
    o_piminus_Hlt1TrackMVADecision_TIS = piminus_Hlt1TrackMVADecision_TIS;
    o_piminus_Hlt1TrackMVADecision_TOS = piminus_Hlt1TrackMVADecision_TOS;
    o_hplus_Hlt1TrackMVADecision_TIS = hplus_Hlt1TrackMVADecision_TIS;
    o_hplus_Hlt1TrackMVADecision_TOS = hplus_Hlt1TrackMVADecision_TOS;

    o_Dplus_Hlt1TwoTrackMVADecision_TIS = Dplus_Hlt1TwoTrackMVADecision_TIS;
    o_Dplus_Hlt1TwoTrackMVADecision_TOS = Dplus_Hlt1TwoTrackMVADecision_TOS;
    o_piplus_Hlt1TwoTrackMVADecision_TIS = piplus_Hlt1TwoTrackMVADecision_TIS;
    o_piplus_Hlt1TwoTrackMVADecision_TOS = piplus_Hlt1TwoTrackMVADecision_TOS;
    o_piminus_Hlt1TwoTrackMVADecision_TIS = piminus_Hlt1TwoTrackMVADecision_TIS;
    o_piminus_Hlt1TwoTrackMVADecision_TOS = piminus_Hlt1TwoTrackMVADecision_TOS;
    o_hplus_Hlt1TwoTrackMVADecision_TIS = hplus_Hlt1TwoTrackMVADecision_TIS;
    o_hplus_Hlt1TwoTrackMVADecision_TOS = hplus_Hlt1TwoTrackMVADecision_TOS;


    v_Dplus_DTF_KS0_piplus0.SetXYZ(Dplus_DTF_KS0_piplus_0_PX[0],Dplus_DTF_KS0_piplus_0_PY[0],Dplus_DTF_KS0_piplus_0_PZ[0]);
    v_Dplus_DTF_KS0_piplus.SetXYZ(Dplus_DTF_KS0_piplus_PX[0],Dplus_DTF_KS0_piplus_PY[0],Dplus_DTF_KS0_piplus_PZ[0]);
    v_Dplus_DTF_piplus.SetXYZ(Dplus_DTF_piplus_PX[0],Dplus_DTF_piplus_PY[0],Dplus_DTF_piplus_PZ[0]);
    v_Dplus_DTF_KS0 = v_Dplus_DTF_KS0_piplus0+v_Dplus_DTF_KS0_piplus;
    v_Dplus_DTF = v_Dplus_DTF_KS0_piplus0+v_Dplus_DTF_KS0_piplus+v_Dplus_DTF_piplus;

    
    o_Dplus_DTF_P = v_Dplus_DTF.Mag();
    o_Dplus_DTF_PT = sqrt(v_Dplus_DTF.X()*v_Dplus_DTF.X()+v_Dplus_DTF.Y()*v_Dplus_DTF.Y());
    o_Dplus_DTF_PHI = TMath::ATan2(v_Dplus_DTF.Y(),v_Dplus_DTF.X());
    o_Dplus_DTF_ETA = v_Dplus_DTF.PseudoRapidity();
    o_Dplus_DTF_TAU = Dplus_FD_OWNPV*Dplus_DTF_M[0]/v_Dplus_DTF.Mag()/0.2997;

    o_KS0_DTF_P = v_Dplus_DTF_KS0.Mag();
    o_KS0_DTF_PT = sqrt(v_Dplus_DTF_KS0.X()*v_Dplus_DTF_KS0.X()+v_Dplus_DTF_KS0.Y()*v_Dplus_DTF_KS0.Y());
    o_KS0_DTF_PHI = TMath::ATan2(v_Dplus_DTF_KS0.Y(),v_Dplus_DTF_KS0.X());
    o_KS0_DTF_ETA = v_Dplus_DTF_KS0.PseudoRapidity();

    o_hplus_DTF_P = v_Dplus_DTF_piplus.Mag();
    o_hplus_DTF_PT = sqrt(v_Dplus_DTF_piplus.X()*v_Dplus_DTF_piplus.X()+v_Dplus_DTF_piplus.Y()*v_Dplus_DTF_piplus.Y());
    o_hplus_DTF_PHI = TMath::ATan2(v_Dplus_DTF_piplus.Y(),v_Dplus_DTF_piplus.X());
    o_hplus_DTF_ETA = v_Dplus_DTF_piplus.PseudoRapidity();


    o_runNumber = runNumber;
    o_eventNumber = eventNumber;
    o_nCandidate = nCandidate;
    o_nVeloClusters = nVeloClusters;
    o_nOTClusters = nOTClusters;
    o_totCandidates = totCandidates;



    if(Dplus_ID>0) {
      h_DTFmass_plus->Fill(Dplus_DTF_M[0]);
      h_mass_plus->Fill(Dplus_M);
    }
    else {
      h_DTFmass_minus->Fill(Dplus_DTF_M[0]);
      h_mass_minus->Fill(Dplus_M);     
    }
    
    ntpOut->Fill();    
  }
  
  h_KS0_M->Write();
  h_KS0_M_bkg->Write();
  h_TIP->Write();
  h_X_P->Write();
  h_X_PT->Write();
  h_X_ETA->Write();
  h_Dphp_PTPT->Write();
  h_Dphp_ETAETA->Write();
  h_Dphp_PHIPHI->Write();
 
  h_X_diffPTs->Write();
  h_ETAETA_diff->Write();
  h_PHIPHI_diff->Write();
 
  h_DTFmass_plus->Write();
  h_DTFmass_minus->Write();
  h_mass_plus->Write();
  h_mass_minus->Write();
 
  h_hplus_pzpx_nofidCut_plus->Write();
  h_hplus_pzpx_nofidCut_minus->Write();
  h_hplus_pzpx_nofidCut_CloseToBeamPipeVertical_plus->Write();
  h_hplus_pzpx_nofidCut_CloseToBeamPipeVertical_minus->Write();
  h_hplus_pzpx_fidCut_plus->Write();
  h_hplus_pzpx_fidCut_minus->Write();
  
  ntpOut->Write();
  cout << "Before selecting: " << nEntries << endl;                                             
  cout << "After selecting: " << ntpOut->GetEntries() << endl;
  fOut->Close();

                              
  if(doMultCandRemoval) {
    TFile * inputFile = TFile::Open(Form(outputFile));  
    TTree * inputTree = (TTree*)inputFile->Get("ntp");     
    
    ntp->Print();

    UInt_t prev_runNumber = 0, mcrunNumber = 0;    
    ULong64_t prev_eventNumber = 0, mceventNumber = 0;          
    ULong64_t mctotCandidates = 0;                                         
    int mcpdg; double mcDTF_Mass;                          
    
    inputTree->SetBranchAddress( "runNumber", &mcrunNumber );       
    inputTree->SetBranchAddress( "eventNumber", &mceventNumber );      
    inputTree->SetBranchAddress( "totCandidates", &mctotCandidates );     
    inputTree->SetBranchAddress( "Dplus_ID", &mcpdg );         
    inputTree->SetBranchAddress( "Dplus_M", &mcDTF_Mass );         

    outputFile.ReplaceAll("_noMultCandRemoval.root","_withMultCandRemoval.root");
    TFile *outmc = new TFile(outputFile, "RECREATE"); 
                                                                                                                                    
    TTree *outputTree = inputTree->CloneTree(0);                                                                                      
                                                                                                                                    
    TH1F * h_dm_plus_tmp = new TH1F("h_dm_plus_tmp","h_dm_plus_tmp",500,1800,1930);                                                   
    TH1F * h_dm_minus_tmp = new TH1F("h_dm_minus_tmp","h_dm_minus_tmp",500,1800,1930);                                                
                                                                                                                                    
    cout << "Before selecting: " << nEntries << endl;                                              
                                                                                                                              
    cout << "After selecting: " << inputTree->GetEntries() << endl;                                                                                                                                                                   
    std::cout << " --- If there are multiple candidates, I will choose one randomly...\n";                                            
    bool switched_event = false;                          
    std::vector<Long64_t> indices = {};                                                                                               
    TRandom3 random_mult;                                                                                                             
    double nMC = 0;                                                                                                                   
    int nEntries_survived = inputTree->GetEntries();                                                                                  
                                                                                                                                    
    for ( Long64_t iEntry = 0; iEntry < nEntries_survived; iEntry++ ) {                                                               
      if ( iEntry%50000 == 0 )                                                                                                        
	std::cout << "  processing Event " << iEntry << " / " << nEntries_survived << "\n";                                           
      inputTree->GetEntry( iEntry );                                                                                                  
                          
      
      if ( iEntry != 0) {                                                                                                             
	if ( ( prev_runNumber != mcrunNumber ) || ( prev_eventNumber != mceventNumber) )                                              
	  switched_event = true;                                                                                                      
	else                                                                                                                          
	  switched_event = false;                                                                                                     
	if ( switched_event ) {                                                                                                       
	  if ( indices.size() > 1 ) {                                                                                                 
	    random_mult.SetSeed(mcrunNumber*mceventNumber);
	    UInt_t rand_index = random_mult.Integer( indices.size() );                                                                
	    Long64_t rand_entry = indices.at( rand_index );                                                                           
	    inputTree->GetEntry( rand_entry );                                                                                        
	  }                                                                                                                           
	  else                                                                                                                        
	    inputTree->GetEntry( iEntry - 1 );                                                                                        
                                                                                                                                    
	  indices.clear();                                                                                                            
                                                                                                                                    
	  //fill                                                                                                                      
	  if(mcpdg>0) h_dm_plus_tmp->Fill(mcDTF_Mass);                                                                                
	  if(mcpdg<0) h_dm_minus_tmp->Fill(mcDTF_Mass);                          


	  outputTree->Fill();                                                                                                         
	}                                                                                                                  
      }                                                                                                                               
      // check entry to be filled (or not) at the next iteration                                                                      
      inputTree->GetEntry( iEntry );                                                                                                  
      if ( mctotCandidates == 1 )                                                                                                     
	indices.clear();                                                                                                              
      else {                                                                                                                          
	indices.push_back( iEntry );                                                                                                  
      }                                                                                                                               
      prev_runNumber = mcrunNumber;                                                                                                   
      prev_eventNumber = mceventNumber;                                                                                               
    }                                                                                                 
                                                                                                                                      
    // check on very last entry                                                                                                       
    if ( indices.size() > 1 ) {                                                                                                       
      UInt_t rand_index = random_mult.Integer( indices.size() );                                                                      
      Long64_t rand_entry = indices.at( rand_index );                                                                                 
      inputTree->GetEntry( rand_entry );                                                                                              
      nMC++;                                                                                                                          
    }                                                                          
    else                                                                                                                              
      inputTree->GetEntry( nEntries_survived );                                                                                       
    indices.clear();                                                                                                                  
    outputTree->Fill();                                                                                                               
                                                                                                                                    
    outmc->WriteTObject( outputTree, "ntp", "WriteDelete" );                                                                          
                                                                                                                                    
    std::cout << " ===> Random candidates have been picked up!\n";                                                                    
    cout << "After removing all multiple candidates: " << outputTree->GetEntries() << endl;                                           
    std::cout << " >>>> Percentual of multiple candidates: " << 1.0-(double)outputTree->GetEntries()/inputTree->GetEntries() << " %" << endl;     
                                                                                                                                    
    h_dm_plus_tmp->SetName("h_mass_plus");   h_dm_plus_tmp->SetTitle("h_mass_plus");        
    h_dm_minus_tmp->SetName("h_mass_minus");   h_dm_minus_tmp->SetTitle("h_mass_minus");    
    h_dm_plus_tmp->Write();                                                                                                           
    h_dm_minus_tmp->Write();                                                                                                          
    outmc->Close();                        

    outputFile.ReplaceAll("_withMultCandRemoval.root","_noMultCandRemoval.root");
    //system("rm "+outputFile);
      
				    
  }
  
  
}
