#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TVector.h>
#include <TLorentzVector.h>
#include <iostream>
#include "/home/LHCB/smaccoli/Tools.h"
using namespace std;

void SelectD2Kpipi(TString year = "18", TString polarity = "Dw") {
  
  TString decay = "Dp2Kmpippip";
  TString add_string = "_PreSelected";
  // TString add_string = "";
   
  TString inFileName = "/home/LHCB/smaccoli/CPV_in_D02hh/data/"+decay+"_"+year+"_"+polarity+add_string+".root";
 

  TChain* inputTree = new TChain("ntp","ntp");
  inputTree->Add(inFileName);
  inputTree->SetBranchStatus("*",0);
  inputTree->SetBranchStatus("*M",1);
  inputTree->SetBranchStatus("*ID",1);
  inputTree->SetBranchStatus("*PT",1);
  inputTree->SetBranchStatus("*ETA",1);
  inputTree->SetBranchStatus("*PHI",1);
  inputTree->SetBranchStatus("*PX",1);
  inputTree->SetBranchStatus("*PY",1);
  inputTree->SetBranchStatus("*PZ",1);
  inputTree->SetBranchStatus("*P",1);
 
  double Dplus_PT;
  inputTree->SetBranchAddress("Dplus_PT",&Dplus_PT);
  double Dplus_ETA;
  inputTree->SetBranchAddress("Dplus_ETA",&Dplus_ETA);
  double Dplus_PHI;
  inputTree->SetBranchAddress("Dplus_PHI",&Dplus_PHI);
  double Dplus_PX;
  inputTree->SetBranchAddress("Dplus_PX",&Dplus_PX);
  double Dplus_PY;
  inputTree->SetBranchAddress("Dplus_PY",&Dplus_PY);
  double Dplus_PZ;
  inputTree->SetBranchAddress("Dplus_PZ",&Dplus_PZ);
  double hplus_PT;
  inputTree->SetBranchAddress("hplus_PT",&hplus_PT);
  double hplus_ETA;
  inputTree->SetBranchAddress("hplus_ETA",&hplus_ETA);
  double hplus_PHI;
  inputTree->SetBranchAddress("hplus_PHI",&hplus_PHI);
  double hplus_PX;
  inputTree->SetBranchAddress("hplus_PX",&hplus_PX);
  double hplus_PY;
  inputTree->SetBranchAddress("hplus_PY",&hplus_PY);
  double hplus_PZ;
  inputTree->SetBranchAddress("hplus_PZ",&hplus_PZ);

  double piplus_P;
  inputTree->SetBranchAddress("piplus_P",&piplus_P);
  double piplus_PT;
  inputTree->SetBranchAddress("piplus_PT",&piplus_PT);
  double piplus_ETA;
  inputTree->SetBranchAddress("piplus_ETA",&piplus_ETA);
  double piplus_PHI;
  inputTree->SetBranchAddress("piplus_PHI",&piplus_PHI);
  double piplus_PX;
  inputTree->SetBranchAddress("piplus_PX",&piplus_PX);
  double piplus_PY;
  inputTree->SetBranchAddress("piplus_PY",&piplus_PY);
  double piplus_PZ;
  inputTree->SetBranchAddress("piplus_PZ",&piplus_PZ);
  double Kminus_P;
  inputTree->SetBranchAddress("Kminus_P",&Kminus_P);
  double Kminus_PT;
  inputTree->SetBranchAddress("Kminus_PT",&Kminus_PT);
  double Kminus_ETA;
  inputTree->SetBranchAddress("Kminus_ETA",&Kminus_ETA);
  double Kminus_PHI;
  inputTree->SetBranchAddress("Kminus_PHI",&Kminus_PHI);
  double Kminus_PX;
  inputTree->SetBranchAddress("Kminus_PX",&Kminus_PX);
  double Kminus_PY;
  inputTree->SetBranchAddress("Kminus_PY",&Kminus_PY);
  double Kminus_PZ;
  inputTree->SetBranchAddress("Kminus_PZ",&Kminus_PZ);
  double X_P;
  inputTree->SetBranchAddress("X_P",&X_P);
  double X_PT;
  inputTree->SetBranchAddress("X_PT",&X_PT);
  double X_ETA;
  inputTree->SetBranchAddress("X_ETA",&X_ETA);
  double X_PHI;
  inputTree->SetBranchAddress("X_PHI",&X_PHI);
 
  double Dplus_M;
  inputTree->SetBranchAddress("Dplus_M",&Dplus_M);
  int Dplus_ID;
  inputTree->SetBranchAddress("Dplus_ID",&Dplus_ID);

  TString outFileName = inFileName;
  outFileName.ReplaceAll("_PreSelected","");
 
  TFile *f = TFile::Open(outFileName,"RECREATE");
  TH1D * h_X_P = new TH1D("h_X_P","h_X_P",100,0,140);
  TH1D * h_X_PT = new TH1D("h_X_PT","h_X_PT",100,0,12e3);
  TH1D * h_X_ETA = new TH1D("h_X_ETA","h_X_ETA",100,1.5,5.5);
  TH1D * h_X_diffPTs = new TH1D("h_X_diffPTs","h_X_diffPTs",100,0,12e3);
  TH1D * h_X_sumPTs = new TH1D("h_X_sumPTs","h_X_sumPTs",100,0,12e3);
  TH2D * h_Dphp_PTPT = new TH2D("h_Dphp_PTPT","h_Dphp_PTPT",200,1e3,15e3,200,1e3,10e3);
  TH2D * h_Dphp_ETAETA = new TH2D("h_Dphp_ETAETA","h_Dphp_ETAETA",100,1.5,5.5,100,1.5,5.5);
  TH2D * h_Dphp_PHIPHI = new TH2D("h_Dphp_PHIPHI","h_Dphp_PHIPHI",100,-3.1416,3.1416,100,-3.1416,3.1416);
  TH2D * h_Kmpip_PTPT = new TH2D("h_Kmpip_PTPT","h_Kmpip_PTPT",200,800,8e3,200,800,8e3);
  TH2D * h_Kmpip_ETAETA = new TH2D("h_Kmpip_ETAETA","h_Kmpip_ETAETA",100,1.5,5.5,100,1.5,5.5);
  TH2D * h_Kmpip_PHIPHI = new TH2D("h_Kmpip_PHIPHI","h_Kmpip_PHIPHI",100,-3.1416,3.1416,100,-3.1416,3.1416);

  TH1D * h_mass_plus = new TH1D("h_mass_plus", "h_mass_plus", 500, 1800, 1930);
  TH1D * h_mass_minus = new TH1D("h_mass_minus", "h_mass_minus", 500, 1800, 1930);
  TTree * t = inputTree->CloneTree(0);
 
  double THETA_X_hplus;
  t->Branch("THETA_X_hplus",&THETA_X_hplus);
  double X_PX;
  t->Branch("X_PX",&X_PX);
  double X_PY;
  t->Branch("X_PY",&X_PY);
  double X_PZ;
  t->Branch("X_PZ",&X_PZ);
  
  bool HarmonizationCut;
  for(Long64_t i = 0; i < 
	//5e6;
	inputTree->GetEntries();
      i++){
    inputTree->GetEntry(i);
    
    h_X_P->Fill(X_P/1e3);
    h_X_PT->Fill(X_PT);
    h_X_ETA->Fill(X_ETA);
  
    HarmonizationCut = X_P > 30e3 && X_PT > 2.08e3 && X_PT < 9.08e3 && X_ETA > 2.4 && X_ETA < 4.3;
 
    h_X_diffPTs->Fill(Dplus_PT - hplus_PT);
    h_X_sumPTs->Fill(piplus_PT + Kminus_PT);
    h_Dphp_PTPT->Fill(Dplus_PT,hplus_PT);
    h_Dphp_ETAETA->Fill(Dplus_ETA,hplus_ETA);
    h_Dphp_PHIPHI->Fill(Dplus_PHI,hplus_PHI);
    h_Kmpip_PTPT->Fill(Kminus_PT,piplus_PT);
    h_Kmpip_ETAETA->Fill(Kminus_ETA,piplus_ETA);
    h_Kmpip_PHIPHI->Fill(Kminus_PHI,piplus_PHI);
  
    HarmonizationCut = HarmonizationCut && Kminus_P>5e3 && Kminus_PT>800 && piplus_P>5e3 && piplus_PT>800 && piplus_PT + Kminus_PT > 2280 && piplus_PT < 5.5e3 && Kminus_PT < 6e3 && piplus_ETA > 2.2 && piplus_ETA < 4.3 && Kminus_ETA > 2.2 && Kminus_ETA < 4.3;
    
  
    HarmonizationCut = HarmonizationCut && Dplus_PT - hplus_PT > 2280 &&  TMath::Abs(Dplus_PHI-hplus_PHI) < 0.4 && TMath::Abs(Dplus_ETA-hplus_ETA) < 0.4 && Dplus_PT>4.2e3 && Dplus_PT<11e3 && Dplus_ETA>2.3 && Dplus_ETA<4.2 && hplus_PT>1.6e3 && hplus_PT<6e3 && hplus_ETA>2.2 && hplus_ETA<4.2;   
 
    HarmonizationCut = HarmonizationCut && (Dplus_ETA < 4.1 && hplus_ETA < 4.1);
    
    if(!HarmonizationCut) {
      continue;
    }
   
    X_PX = X_PT*cos(X_PHI);
    X_PY = X_PT*sin(X_PHI);
    X_PZ = X_PT*sinh(X_ETA);
    

    THETA_X_hplus = ( X_PX*hplus_PX + X_PY*hplus_PY + X_PZ*hplus_PZ ) / ( X_PT*cosh(X_ETA) * hplus_PT*cosh(hplus_ETA) );
    
    // Dplus_E = sqrt(Dplus_PX*Dplus_PX+Dplus_PY*Dplus_PY+Dplus_PZ*Dplus_PZ+PdgMass::mDp*PdgMass::mDp);
    // Dplus.SetPxPyPzE(Dplus_PX,Dplus_PY,Dplus_PZ,Dplus_E);
    // Dplus_boost = -Dplus.BoostVector(); 
    // Dplus.Boost(Dplus_boost);    
   
    // hplus_E = sqrt(hplus_PX*hplus_PX+hplus_PY*hplus_PY+hplus_PZ*hplus_PZ+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG);
    // hplus.SetPxPyPzE(hplus_PX,hplus_PY,hplus_PZ,hplus_E);
    // hplus.Boost(Dplus_boost);    
    
    // piplus_E = sqrt(piplus_PX*piplus_PX+piplus_PY*piplus_PY+piplus_PZ*piplus_PZ+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG);
    // piplus.SetPxPyPzE(piplus_PX,piplus_PY,piplus_PZ,piplus_E);
    
    // Kminus_E = sqrt(Kminus_PX*Kminus_PX+Kminus_PY*Kminus_PY+Kminus_PZ*Kminus_PZ+PdgMass::mKplus_PDG*PdgMass::mKplus_PDG);
    // Kminus.SetPxPyPzE(Kminus_PX,Kminus_PY,Kminus_PZ,Kminus_E);
    
    // X = Kminus+piplus;
    // X.Boost(Dplus_boost);

    // DplusRef_X_PX = X.Px();
    // DplusRef_X_PY = X.Py();
    // DplusRef_X_PZ = X.Pz();

   
    if(Dplus_ID>0) {
      h_mass_plus->Fill(Dplus_M);
    }
    else {
      h_mass_minus->Fill(Dplus_M);
    }
    
    t->Fill();
  }
  
  f->Write();
  f->Close();


}

int main(int argc, char * argv[]) { 
  SelectD2Kpipi(argv[1], argv[2]); 
  return 0;
}
