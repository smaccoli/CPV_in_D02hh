void test(TString year = "15", TString polarity = "Dw") {

  TString decay = "Dp2Kmpippip";
  TChain* target = new TChain("ntp","ntp");
  target->Add("../data/"+decay+"_"+year+"_"+polarity+"_MVA_P1.root");
  target->AddFriend("ntp","../data/"+decay+"_"+year+"_"+polarity+"_MVA_P2.root");
  target->Draw("MVA_P1:MVA_P2>>h_target(5000,-1,1,5000,-1,1)","","");
  TH2D* h_target = (TH2D*) gDirectory->Get("h_target");
  //target->Draw("MVA_P2>>h_target(5000,-1,1)","","");
  //TH1D* h_target = (TH1D*) gDirectory->Get("h_target");
  
  decay = "D02Kmpip";
  TChain* toweight = new TChain("ntp","ntp");
  toweight->Add("../data/"+decay+"_"+year+"_"+polarity+"_MVA_P1.root");
  toweight->AddFriend("ntp","../data/"+decay+"_"+year+"_"+polarity+"_MVA_P2.root");
  toweight->Draw("MVA_P1:MVA_P2>>h_toweight(5000,-1,1,5000,-1,1)","","");
  TH2D* h_toweight = (TH2D*) gDirectory->Get("h_toweight");
  //toweight->Draw("MVA_P2>>h_toweight(5000,-1,1)","","");
  //TH1D* h_toweight = (TH1D*) gDirectory->Get("h_toweight");

  bool ok_weights = false;
  double i_toweight,i_target;
  TH2D *tmp_toweight,*tmp_target;
  Int_t Nbins = 5000*5000;
  // TH1D *tmp_toweight,*tmp_target;
  // Int_t Nbins = 5000;

  for (int i = 1; i <= Nbins; i++) {
    if (h_toweight->GetBinContent(i) == 0)
      h_target->SetBinContent(i,0);
  }

  while(!ok_weights) {
    i_toweight = 1./h_toweight->Integral();
    i_target = 1./h_target->Integral();
    tmp_toweight = (TH2D*) h_toweight->Clone();
    tmp_target = (TH2D*) h_target->Clone();
    //  tmp_toweight = (TH1D*) h_toweight->Clone();
    //tmp_target = (TH1D*) h_target->Clone();
    tmp_toweight->Scale(i_toweight);
    tmp_target->Scale(i_target);
    tmp_target->Divide(tmp_toweight);
    ok_weights = true;
    for (int i = 1; i <= Nbins; i++) {
      if (tmp_target->GetBinContent(i) > 2/* || (tmp_target->GetBinError(i) != 0 ? tmp_target->GetBinContent(i)/tmp_target->GetBinError(i) < 2 : 0 )*/ ) {
	h_toweight->SetBinContent(i,0);
	h_target->SetBinContent(i,0);
	ok_weights = false;
      }
    }
  }
  
  
  tmp_target->Draw("colz");

  TFile *outFile;
  TTree *TreeNew;
  Float_t MVA_P2;
  Float_t MVA_P1;
  Double_t w;
  Double_t inv_w;
  Double_t o;
  Long64_t nentries;

  outFile = new TFile("../data/"+decay+"_"+year+"_"+polarity+"_MVA_rew.root","RECREATE");
  TreeNew = toweight->CloneTree(0);
  toweight->SetBranchAddress("MVA_P2",&MVA_P2);
  toweight->SetBranchAddress("MVA_P1",&MVA_P1);
 
  TreeNew->Branch("w",&w);
  TreeNew->Branch("inv_w",&inv_w);
  TreeNew->Branch("o",&o);
  TreeNew->Branch("MVA_P2",&MVA_P2);

  nentries = toweight->GetEntries();
  for (Long64_t k=0; k < nentries; ++k) {
    toweight->GetEntry(k);
    
    w = tmp_target->GetBinContent(tmp_target->FindBin(MVA_P2,MVA_P1));
    o = w == 0 ? 0 : 1;
    inv_w = w == 0 ? 0 : 1./w;

    TreeNew->Fill();
  }
  
  outFile = TreeNew->GetCurrentFile();
  outFile->cd();
  
  TreeNew->Print();
  TreeNew->Write();
  outFile->Close();
 
  delete outFile;




  decay = "Dp2Kmpippip";
 outFile = new TFile("../data/"+decay+"_"+year+"_"+polarity+"_MVA_rew.root","RECREATE");
  TreeNew = target->CloneTree(0);
  target->SetBranchAddress("MVA_P2",&MVA_P2);
  target->SetBranchAddress("MVA_P1",&MVA_P1);
 
  TreeNew->Branch("w",&w);
  TreeNew->Branch("inv_w",&inv_w);
  TreeNew->Branch("o",&o);
  TreeNew->Branch("MVA_P2",&MVA_P2);

  nentries = target->GetEntries();
  for (Long64_t k=0; k < nentries; ++k) {
    target->GetEntry(k);
    
    w = tmp_target->GetBinContent(tmp_target->FindBin(MVA_P2,MVA_P1));
    o = w == 0 ? 0 : 1;
    inv_w = w == 0 ? 0 : 1./w;

    TreeNew->Fill();
  }
  
  outFile = TreeNew->GetCurrentFile();
  outFile->cd();
  
  TreeNew->Print();
  TreeNew->Write();
  outFile->Close();
 
  delete outFile;
 
}
