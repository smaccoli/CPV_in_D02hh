#include "/home/LHCB-T3/smaccoli/dcastyle.C"
#include "/home/LHCB-T3/smaccoli/Tools.h"

void show_X_sumPTs(TString year = "18", TString pol = "Dw") {

  dcastyle();
  TH1::SetDefaultSumw2();
  
  TString path = "/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/";
  TCanvas* c = new TCanvas("X_sumPTs","X_sumPTs",50,50, 1000,700);
  TLegend* leg = new TLegend(0.6,0.70,0.90,0.90);
  leg->SetFillColor(kWhite);
  leg->SetTextSize(0.05);
  leg->SetBorderSize(0);
  leg->SetTextFont(132);

  {
    TString decay = "Dp2KS0pipLL";
    EColor color = kRed;
    Int_t style = 20;
    TFile *f = new TFile(path+decay+"_"+year+"_"+pol+".root");
    TH1D* h_0 = (TH1D*)f->Get("h_X_diffPTs");
    h_0->Scale(1./h_0->Integral());
    h_0->SetXTitle(findAxisTitle("p_{T} (D^{+}) - p_{T} (h^{+}) or p_{T} (#pi^{+}) + p_{T} (K^{#minus}) [MeV/c]"));
    h_0->SetYTitle(Form("Normalized events / %.0f MeV",h_0->GetBinWidth(1)));
    h_0->GetXaxis()->SetRangeUser(h_0->GetXaxis()->GetXmin(),10e3);
    h_0->Scale(1./h_0->Integral());
    h_0->SetTitleOffset(1.2,"yx");
    h_0->SetMarkerSize(1.5);
    h_0->SetMarkerColor(color);
    h_0->SetMarkerStyle(style);
    h_0->Draw("pe");
    leg->AddEntry(h_0,findDecayTitle(decay)+" (diff)", "pe");
    TLine * l = new TLine(2.28e3,0,2.28e3,h_0->GetMaximum());
    l->SetLineStyle(kDashed);
    l->Draw("same");
  }
  
  {
    TString decay = "Dp2Kmpippip";
    EColor color = kBlue;
    Int_t style = 20;
    TFile *f = new TFile(path+decay+"_"+year+"_"+pol+".root");
    TH1D* h_0 = (TH1D*)f->Get("h_X_diffPTs");
    h_0->Scale(1./h_0->Integral());
    h_0->SetMarkerSize(1.5);
    h_0->SetMarkerColor(color);
    h_0->SetMarkerStyle(style);
    h_0->Draw("pesame");    
    leg->AddEntry(h_0,findDecayTitle(decay)+" (diff)", "pe");
  }

  {
    TString decay = "Dp2Kmpippip";
    EColor color = kBlue;
    Int_t style = 22;
    TFile *f = new TFile(path+decay+"_"+year+"_"+pol+".root");
    TH1D* h_0 = (TH1D*)f->Get("h_X_sumPTs");
    h_0->Scale(1./h_0->Integral());
    h_0->SetMarkerSize(1.5);
    h_0->SetMarkerColor(color);
    h_0->SetMarkerStyle(style);
    h_0->Draw("pesame");    
    leg->AddEntry(h_0,findDecayTitle(decay)+" (sum)", "pe");
  }

  {
    TString decay = "D02Kmpip";//D2Kpi
    EColor color = kGreen;
    Int_t style = 22;
    TFile *f = new TFile(path+decay+"_"+year+"_"+pol+".root");
    TH1D* h_0 = (TH1D*)f->Get("h_X_sumPTs");
    h_0->Scale(1./h_0->Integral());
    h_0->SetMarkerSize(1.5);
    h_0->SetMarkerColor(color);
    h_0->SetMarkerStyle(style);
    h_0->Draw("pesame");    
    leg->AddEntry(h_0,findDecayTitle(decay)+" (sum)", "pe");
  }
 
  leg->Draw("same");
  c->SaveAs("plots/X_sumPTs.C");
  c->SaveAs("plots/X_sumPTs.pdf");
 
}
