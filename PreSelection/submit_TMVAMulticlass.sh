#!/bin/bash

DIR=$PWD
#/bin/rm $DIR/exec/*
#/bin/rm $DIR/err/*
#/bin/rm $DIR/out/*
#/bin/rm $DIR/logs/*

#for year in $(echo 15 16 17 18); do
#    for pol in $(echo Up Dw); do
for year in $(echo 15); do
    for pol in $(echo Dw); do
	echo "#!/bin/sh" > exec/TMVA.$year.$pol.sh
	echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> exec/TMVA.$year.$pol.sh 
	echo "cd $DIR" >> exec/TMVA.$year.$pol.sh
	echo "hostname" >> exec/TMVA.$year.$pol.sh
	echo "lb-run ROOT/6.14.04 bash" >> exec/TMVA.$year.$pol.sh
	echo "root -l -b -q TMVAMulticlass.cxx+" >> exec/TMVA.$year.$pol.sh
	chmod +x exec/TMVA.$year.$pol.sh
	python condor_make_template.py condor_template.sub TMVA.$year.$pol
	condor_submit exec/condor_template.TMVA.$year.$pol.sub -batch-name TMVA$year$pol
	#break
    done
    #break
done
