#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TMVA/Tools.h"
#include "TMVA/Factory.h"
#include "TMVA/DataLoader.h"
#include "TMVA/TMVAMultiClassGui.h"
using namespace TMVA;

void TMVAMulticlass( TString myMethodList = "" )
{
   // This loads the library
   TMVA::Tools::Instance();
   // to get access to the GUI and all tmva macros
   //
   //     TString tmva_dir(TString(gRootDir) + "/tmva");
   //     if(gSystem->Getenv("TMVASYS"))
   //        tmva_dir = TString(gSystem->Getenv("TMVASYS"));
   //     gROOT->SetMacroPath(tmva_dir + "/test/:" + gROOT->GetMacroPath() );
   //     gROOT->ProcessLine(".L TMVAMultiClassGui.C");
   //---------------------------------------------------------------
   // Default MVA methods to be trained + tested
   std::map<std::string,int> Use;
   Use["MLP"]             = 0;
   Use["BDTG"]            = 1;
   //Use["Cuts"]            = 1;
   
   Use["DNN"]             = 0;
   Use["FDA_GA"]          = 0;
   Use["PDEFoam"]         = 0;
   //---------------------------------------------------------------
   std::cout << std::endl;
   std::cout << "==> Start TMVAMulticlass" << std::endl;
   if (myMethodList != "") {
      for (std::map<std::string,int>::iterator it = Use.begin(); it != Use.end(); it++) it->second = 0;
      std::vector<TString> mlist = TMVA::gTools().SplitString( myMethodList, ',' );
      for (UInt_t i=0; i<mlist.size(); i++) {
         std::string regMethod(mlist[i]);
         if (Use.find(regMethod) == Use.end()) {
            std::cout << "Method \"" << regMethod << "\" not known in TMVA under this name. Choose among the following:" << std::endl;
            for (std::map<std::string,int>::iterator it = Use.begin(); it != Use.end(); it++) std::cout << it->first << " ";
            std::cout << std::endl;
            return;
         }
         Use[regMethod] = 1;
      }
   }
   // Create a new root output file.
   TString outfileName = "TMVAMulticlass.root";
   TFile* outputFile = TFile::Open( outfileName, "RECREATE" );
   TMVA::Factory *factory = new TMVA::Factory( "TMVAMulticlass", outputFile,
                                               "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=multiclass" );
   TMVA::DataLoader *dataloader=new TMVA::DataLoader("dataset");
   dataloader->AddVariable( "Dplus_PT", "Dplus_PT","MeV/c^{2}",'F' );
   dataloader->AddVariable( "Dplus_ETA", "Dplus_ETA","",'F' );
   dataloader->AddVariable( "Dplus_PHI", "Dplus_PHI","",'F' );
   dataloader->AddVariable( "hplus_PT", "hplus_PT","MeV/c^{2}",'F' );
   dataloader->AddVariable( "hplus_ETA", "hplus_ETA","",'F' );
   dataloader->AddVariable( "hplus_PHI", "hplus_PHI","",'F' );
   dataloader->AddVariable( "P1_PT", "P1_PT","MeV/c^{2}",'F' );
   dataloader->AddVariable( "P1_ETA", "P1_ETA","",'F' );
   dataloader->AddVariable( "P1_PHI", "P1_PHI","",'F' );
   dataloader->AddVariable( "P2_PT", "P2_PT","MeV/c^{2}",'F' );
   dataloader->AddVariable( "P2_ETA", "P2_ETA","",'F' );
   dataloader->AddVariable( "P2_PHI", "P2_PHI","",'F' );

   dataloader->AddSpectator( "X_P", "X_P","MeV/c^{2}",'F' );
   dataloader->AddSpectator( "X_PT", "X_PT","MeV/c^{2}",'F' );
   dataloader->AddSpectator( "X_ETA", "X_ETA","",'F' );
   dataloader->AddSpectator( "X_PHI", "X_PHI","",'F' );
  
   
   TFile *input_Dp2Kmpippip(0);
   TString fname_Dp2Kmpippip = "../data/Dp2Kmpippip_15_Dw_PreSelected.root";
   std::cout << "--- TMVAMulticlass   : Accessing " << fname_Dp2Kmpippip << std::endl;
   input_Dp2Kmpippip = TFile::Open( fname_Dp2Kmpippip );
   if (!input_Dp2Kmpippip) {
      std::cout << "ERROR: could not open data file" << std::endl;
      exit(1);
   }
   TTree *tree_Dp2Kmpippip  = (TTree*)input_Dp2Kmpippip->Get("ntp");
   dataloader->AddTree    (tree_Dp2Kmpippip,"Dp2Kmpippip");
   
   TFile *input_Dp2KS0pipLL(0);
   TString fname_Dp2KS0pipLL = "../data/Dp2KS0pipLL_15_Dw_PreSelected.root";
   std::cout << "--- TMVAMulticlass   : Accessing " << fname_Dp2KS0pipLL << std::endl;
   input_Dp2KS0pipLL = TFile::Open( fname_Dp2KS0pipLL );
   if (!input_Dp2KS0pipLL) {
      std::cout << "ERROR: could not open data file" << std::endl;
      exit(1);
   }
   TTree *tree_Dp2KS0pipLL  = (TTree*)input_Dp2KS0pipLL->Get("ntp");
   tree_Dp2KS0pipLL->AddFriend("ntp",fname_Dp2Kmpippip);
   dataloader->AddTree    (tree_Dp2KS0pipLL,"Dp2KS0pipLL");
 
   TFile *input_D02Kmpip(0);
   TString fname_D02Kmpip = "../data/D02Kmpip_15_Dw_PreSelected.root";
   std::cout << "--- TMVAMulticlass   : Accessing " << fname_D02Kmpip << std::endl;
   input_D02Kmpip = TFile::Open( fname_D02Kmpip );
   if (!input_D02Kmpip) {
      std::cout << "ERROR: could not open data file" << std::endl;
      exit(1);
   }
   TTree *tree_D02Kmpip  = (TTree*)input_D02Kmpip->Get("ntp");
   tree_D02Kmpip->AddFriend("ntp",fname_Dp2Kmpippip);
   dataloader->AddTree    (tree_D02Kmpip,"D02Kmpip");
 
   gROOT->cd( outfileName+TString(":/") );
   dataloader->PrepareTrainingAndTestTree( "", "SplitMode=Random:NormMode=NumEvents:nTest_Dp2Kmpippip=1000:nTrain_Dp2Kmpippip=1000::nTest_Dp2KS0pipLL=1000:nTrain_Dp2KS0pipLL=1000:nTest_D02Kmpip=1000:nTrain_D02Kmpip=1000!V" );
   //dataloader->PrepareTrainingAndTestTree( "", "SplitMode=Random:NormMode=NumEvents:nTest_Dp2Kmpippip=500000:nTrain_Dp2Kmpippip=500000::nTest_Dp2KS0pipLL=500000:nTrain_Dp2KS0pipLL=500000:nTest_D02Kmpip=500000:nTrain_D02Kmpip=500000!V" );
   
if (Use["BDTG"]) // gradient boosted decision trees
  factory->BookMethod( dataloader,  TMVA::Types::kBDT, "BDTG", "!H:!V:NTrees=1000:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.50:nCuts=20:MaxDepth=2");
/*
  if (Use["Cuts"]) //Rectangular cuts
  factory->BookMethod( dataloader,  TMVA::Types::kBDT, "Cuts", "!H:!V:NTrees=1000:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.50:nCuts=20:MaxDepth=2:VarTransform=D");
*/ 
if (Use["MLP"]) // neural network
      factory->BookMethod( dataloader,  TMVA::Types::kMLP, "MLP", "!H:!V:NeuronType=tanh:NCycles=1000:HiddenLayers=N+5,5:TestRate=5:EstimatorType=MSE");
   if (Use["FDA_GA"]) // functional discriminant with GA minimizer
      factory->BookMethod( dataloader,  TMVA::Types::kFDA, "FDA_GA", "H:!V:Formula=(0)+(1)*x0+(2)*x1+(3)*x2+(4)*x3:ParRanges=(-1,1);(-10,10);(-10,10);(-10,10);(-10,10):FitMethod=GA:PopSize=300:Cycles=3:Steps=20:Trim=True:SaveBestGen=1" );
   if (Use["PDEFoam"]) // PDE-Foam approach
      factory->BookMethod( dataloader,  TMVA::Types::kPDEFoam, "PDEFoam", "!H:!V:TailCut=0.001:VolFrac=0.0666:nActiveCells=500:nSampl=2000:nBin=5:Nmin=100:Kernel=None:Compress=T" );
   if (Use["DNN"]) {
       TString layoutString ("Layout=TANH|100,TANH|50,TANH|10,LINEAR");
       TString training0 ("LearningRate=1e-1, Momentum=0.5, Repetitions=1, ConvergenceSteps=10,"
                          " BatchSize=256, TestRepetitions=10, Multithreading=True");
       TString training1 ("LearningRate=1e-2, Momentum=0.0, Repetitions=1, ConvergenceSteps=10,"
                          " BatchSize=256, TestRepetitions=7, Multithreading=True");
       TString trainingStrategyString ("TrainingStrategy=");
       trainingStrategyString += training0 + "|" + training1;
       TString nnOptions ("!H:V:ErrorStrategy=CROSSENTROPY:VarTransform=N:"
                          "WeightInitialization=XAVIERUNIFORM:Architecture=STANDARD");
       nnOptions.Append (":"); nnOptions.Append (layoutString);
       nnOptions.Append (":"); nnOptions.Append (trainingStrategyString);
       factory->BookMethod(dataloader, TMVA::Types::kDNN, "DNN", nnOptions );
   }
   // Train MVAs using the set of training events
   factory->TrainAllMethods();
   // Evaluate all MVAs using the set of test events
   factory->TestAllMethods();
   // Evaluate and compare performance of all configured MVAs
   factory->EvaluateAllMethods();
   // --------------------------------------------------------------
   // Save the output
   outputFile->Close();
   std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;
   std::cout << "==> TMVAClassification is done!" << std::endl;
   delete factory;
   delete dataloader;
   // Launch the GUI for the root macros
   if (!gROOT->IsBatch()) TMVAMultiClassGui( outfileName );
}
int main( int argc, char** argv )
{
   // Select methods (don't look at this code - not of interest)
   TString methodList;
   for (int i=1; i<argc; i++) {
      TString regMethod(argv[i]);
      if(regMethod=="-b" || regMethod=="--batch") continue;
      if (!methodList.IsNull()) methodList += TString(",");
      methodList += regMethod;
   }
   TMVAMulticlass(methodList);
   return 0;
}
