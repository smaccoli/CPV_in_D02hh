#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

string convertToString(const char* a, int size) 
{ 
  int i; 
  string s = ""; 
  for (i = 0; i <= size; i++) { 
    s = s + a[i]; 
  } 
  return s; 
} 

void before_after(TString year = "15", TString polarity = "Dw", string variable = "") {

  TString tmp = variable.c_str();
  tmp.ReplaceAll("__",",");
  variable = convertToString((const char*)tmp, sizeof(tmp));

  dcastyle();

   TString toweight_tree_fname = "/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+"_MVA_rew.root";
  // TString toweight_tree_fname = "/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_"+year+"_"+polarity+"_MVA_rew.root";
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add(toweight_tree_fname);
  
  TString target_tree_fname = "/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_"+year+"_"+polarity+"_MVA_rew.root";
  // TString target_tree_fname = "/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+"_MVA_rew.root";
  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add(target_tree_fname);

  
  IDecay* toweight_decay = new IDecay(toweight_tree,"D02Kmpip","D^{0} #rightarrow K^{#minus} #pi^{+}",{},toweight_tree->GetEntries(),0);
  IDecay* target_decay = new IDecay(target_tree,"Dp2Kmpippip","D^{+} #rightarrow K^{#minus} #pi^{+} h^{+}",{"o"},target_tree->GetEntries(),0);

  toweight_decay->addTargetDistribution(target_decay,{"P1_PT(100,8e2,5e3)"},5);//15
  toweight_decay->addTargetDistribution(target_decay,{"P1_ETA(100,2.2,4.3)"},5);//15
  toweight_decay->addTargetDistribution(target_decay,{"P1_PHI(100,-3.1416,3.1416)"},5);
  toweight_decay->addTargetDistribution(target_decay,{"P2_PT(100,8e2,6e3)"},5);//15
  toweight_decay->addTargetDistribution(target_decay,{"P2_ETA(100,2.2,4.3)"},5);//15
  toweight_decay->addTargetDistribution(target_decay,{"P2_PHI(100,-3.1416,3.1416)"},5);
  /*
    toweight_decay->addTargetDistribution(target_decay,{"P1_PX(100,-5e3,5e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P2_PX(100,-6e3,6e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P1_PY(100,-5e3,5e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P2_PY(100,-6e3,6e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P1_PZ(100,0,100e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P2_PZ(100,0,100e3)"},5);//15
  */

  /*
  toweight_decay->setAsymVariable("D0_ID");
  target_decay->setAsymVariable("Dplus_ID");
  toweight_decay->calculateResidualAsymmetry = 1;
  */
  toweight_decay->fillTargets();
  
  toweight_decay->use_Iw_max = 0;
  toweight_decay->Iw_max = 15;
  toweight_decay->Iw_max_value = 0;
 
   toweight_decay->setIWeight("w");
  // toweight_decay->setIWeight("inv_w");


  toweight_decay->plotDir = year+"_"+polarity+"/";
  toweight_decay->showVariables();
}

int main(int argc, char * argv[]) {
  before_after(argv[1],argv[2],argv[3]);
  return 0;
}

