#!/bin/bash


doSelection=1
doFiducial=1
doMultCandRemoval=1 


DIR=$PWD
#/bin/rm $DIR/exec/*
#/bin/rm $DIR/err/*
#/bin/rm $DIR/out/*
#/bin/rm $DIR/logs/*

g++ -Wall -o selectKKandKpi selectKKandKpi.cxx `root-config --cflags --glibs` `gsl-config --cflags --libs`

#for year in $(echo 2015 2016 2017 2018); do
for year in $(echo 2015); do
    #for pol in $(echo MagUp MagDown); do
    #for mode in $(echo KK KP_RS); do
    #for year in $(echo 2018); do
    for mode in $(echo "KP_RS"); do
	#for mode in $(echo "KK" ); do
	for pol in $(echo MagDown); do
	    #for pol in $(echo MagUp); do
	    inputLists=$(echo "/home/LHCB-T3/smaccoli/CPV_in_D02hh/lists/D02hmhp/list_${year}_${pol}_*.dat")
	    for inputList in $(ls -I logs $inputLists); do
		idList=$(echo $inputList | tr "/" " " | awk '{print $7}' | tr "_" " " | awk '{print $4}' | tr "." " " | awk '{print $1}')
		echo "#!/bin/sh" > exec/$mode.$year.$pol.$idList.sh
		echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> exec/$mode.$year.$pol.$idList.sh 
		echo "cd $DIR" >> exec/$mode.$year.$pol.$idList.sh
		echo "hostname" >> exec/$mode.$year.$pol.$idList.sh
		echo $inputList
		outList="/home/LHCB-T3/smaccoli/data/CPV_in_D02hh/tmpL_"$mode"_"$year"_"$pol"_"$idList".root"
		outFiles=""
		for inputFile in $(cat $inputList); do
		    #echo $inputFile
		    if [ $year = "2015" ] || [ $year = "2016" ]; then 
			id=$(echo $inputFile | tr "/" " " | awk '{print $10}' | tr ".rootuple" " " | awk '{print $1}') 
		    elif [ $year = "2017" ] || [ $year = "2018" ]; then
			id=$(echo $inputFile | tr "/" " " | awk '{print $10}') 
		    fi
		    outFile="/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/tmp_"$mode"_"$year"_"$pol"_"$id".root"
		    outFiles+=" $outFile"
		    echo $idList $id $outFile
		    echo "./selectKKandKpi $inputFile \"DStar"$mode"_Tuple/DStar"$mode"\" $outFile \"ntp\" $doSelection $doFiducial $doMultCandRemoval >> logs/$mode.$year.$pol.$id" >> exec/$mode.$year.$pol.$idList.sh
		    #break
		done

		echo "hadd -f $outList $outFiles" >> exec/$mode.$year.$pol.$idList.sh
		echo "rm $outFiles" >> exec/$mode.$year.$pol.$idList.sh
		
		chmod +x exec/$mode.$year.$pol.$idList.sh
		python condor_make_template.py condor_template.sub $mode.$year.$pol.$idList
		condor_submit exec/condor_template.$mode.$year.$pol.$idList.sub -batch-name $mode$year$pol$idList
		
		#break
	    done
	    #break
	done
	#break
    done
    #break
done

#ls /storage/gpfs_data/local/lhcb/users/ferrari/D2hh_ntuples/2016/MagDown/*.root | wc -l -> 469

# doSelection=$1
# doFiducial=$2
# doMultCandRemoval=$3 

# if [ -z $doSelection ]; then
#     echo "SELECTION FLAG NOT SET! PLEASE CHECK"
#     exit 0
# fi
# if [ -z $doFiducial ]; then                                                               
#     echo "SELECTION FLAG NOT SET! PLEASE CHECK"                                           
#     exit 0                                                                                
# fi          
# if [ -z $doMultCandRemoval ]; then                                                        
#     echo "SELECTION FLAG NOT SET! PLEASE CHECK"                                           
#     exit 0                                                                                
# fi            

# echo "DO SELECTION: "$doSelection                                                         
# echo "DO FIDUCIAL CUTS: "$doFiducial                                                      
# echo "DO MULTIPLE CANDIDATES REMOVAL: "$doMultCandRemoval 
