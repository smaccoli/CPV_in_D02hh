#include "/home/LHCB-T3/smaccoli/dcastyle.C"
#include "/home/LHCB-T3/smaccoli/Tools.h"
#include <iostream>
using namespace std;

void show_Kminus_pzpx(TString year = "16", TString pol = "Dw", Bool_t useKSpi = 0) {

  dcastyle();
  TString decay = useKSpi ? "Dp2KS0pipLL" : "Dp2Kmpippip";

  TString path = "/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/";
  TFile *f = new TFile(path+decay+"_"+year+"_"+pol+"_simplecuts.root");
  {
    TCanvas* c = new TCanvas("Kminus_pzpx","Kminus_pzpx",50,50, 1000,700);
    c->SetRightMargin(0.17);
    c->SetLeftMargin(0.15);
    gStyle->SetPalette(kBlackBody);
    TH2D* h_plus = (TH2D*)f->Get("h_Kminus_pzpx_fidCut_plus");
    TH2D* h_minus = (TH2D*)f->Get("h_Kminus_pzpx_fidCut_minus");
    TH2D* h_asym = (TH2D*)h_plus->GetAsymmetry(h_minus);
    h_asym->SetTitleOffset(1.2,"X");  
    h_asym->SetTitleOffset(1.2,"Y");  
    h_asym->SetXTitle(findAxisTitle("Kminus_PZ").ReplaceAll("MeV","GeV"));
    h_asym->SetYTitle(findAxisTitle("Kminus_PX").ReplaceAll("MeV","GeV"));
    h_asym->SetName("h_asym");
    h_asym->SetTitle("h_asym");
    h_asym->SetTitleOffset(1.0,"Z");  
    h_asym->SetZTitle("Raw asymmetry");
    h_asym->GetXaxis()->SetRangeUser(0,15);
    h_asym->GetYaxis()->SetRangeUser(-2,2);

    h_asym->Draw("colz");
    c->SaveAs("plots/"+decay+"_Kminus_pzpx.C");
    c->SaveAs("plots/"+decay+"_Kminus_pzpx.pdf");
    cout << h_plus->GetEntries() << endl;
  }

  {
    TCanvas* c = new TCanvas("Kminus_pzpx_nofidCut","Kminus_pzpx_nofidCut",50,50, 1000,700);
    c->SetRightMargin(0.17);
    c->SetLeftMargin(0.15);
    gStyle->SetPalette(kBlackBody);
    TH2D* h_plus = (TH2D*)f->Get("h_Kminus_pzpx_nofidCut_plus");
    TH2D* h_minus = (TH2D*)f->Get("h_Kminus_pzpx_nofidCut_minus");
    TH2D* h_asym = (TH2D*)h_plus->GetAsymmetry(h_minus);
    h_asym->SetTitleOffset(1.2,"X");  
    h_asym->SetTitleOffset(1.2,"Y");  
    h_asym->SetXTitle(findAxisTitle("Kminus_PZ").ReplaceAll("MeV","GeV"));
    h_asym->SetYTitle(findAxisTitle("Kminus_PX").ReplaceAll("MeV","GeV"));
    h_asym->SetName("h_asym");
    h_asym->SetTitle("h_asym");
    h_asym->SetTitleOffset(1.0,"Z");  
    h_asym->SetZTitle("Raw asymmetry");
    h_asym->GetXaxis()->SetRangeUser(0,15);
    h_asym->GetYaxis()->SetRangeUser(-2,2);

    h_asym->Draw("colz");
    c->SaveAs("plots/"+decay+"_Kminus_pzpx_noFidCut.C");
    c->SaveAs("plots/"+decay+"_Kminus_pzpx_noFidCut.pdf");
    cout << h_plus->GetEntries() << endl;

  }

  {
    TCanvas* c = new TCanvas("PID_Kminus_pzpx_fidCut","PID_Kminus_pzpx_fidCut",50,50, 1000,700);
    c->SetRightMargin(0.17);
    c->SetLeftMargin(0.15);
    gStyle->SetPalette(kBlackBody);
    TH2D* h_plus = (TH2D*)f->Get("h_PID_Kminus_pzpx_fidCut_plus");
    TH2D* h_minus = (TH2D*)f->Get("h_PID_Kminus_pzpx_fidCut_minus");
    TH2D* h_asym = (TH2D*)h_plus->GetAsymmetry(h_minus);
    h_asym->SetTitleOffset(1.2,"X");  
    h_asym->SetTitleOffset(1.2,"Y");  
    h_asym->SetXTitle(findAxisTitle("Kminus_PZ").ReplaceAll("MeV","GeV"));
    h_asym->SetYTitle(findAxisTitle("Kminus_PX").ReplaceAll("MeV","GeV"));
    h_asym->SetName("h_asym");
    h_asym->SetTitle("h_asym");
    h_asym->SetTitleOffset(1.0,"Z");  
    h_asym->SetZTitle("Raw asymmetry");
    h_asym->GetXaxis()->SetRangeUser(0,15);
    h_asym->GetYaxis()->SetRangeUser(-2,2);

    h_asym->Draw("colz");
    c->SaveAs("plots/"+decay+"_Kminus_pzpx_PID.C");
    c->SaveAs("plots/"+decay+"_Kminus_pzpx_PID.pdf");
    cout << h_plus->GetEntries() << endl;

  }

}
