#!/bin/bash

DIR=$PWD
DATADIR="/home/LHCB-T3/smaccoli/CPV_in_D02hh/data"
#/bin/rm $DIR/exec/*
#/bin/rm $DIR/err/*
#/bin/rm $DIR/out/*
#/bin/rm $DIR/logs/*

#for year in $(echo 15 16 17 18); do
#for year in $(echo 16 17 18); do
for year in $(echo 16); do
    for pol in $(echo MagUp MagDown); do
    #for pol in $(echo MagDown); do
    #for pol in $(echo MagUp); do
	if [ $pol = "MagUp" ]; then 
	    short_pol=$(echo Up)
	elif [ $pol = "MagDown" ]; then 
	    short_pol=$(echo Dw) 
	fi
	#for mode in $(echo Dst2D0pi_D02Kpi Dp2KS0pipLL Dp2Kmpippip); do
	#for mode in $(echo Dp2KS0pipLL Dp2Kmpippip); do
	for mode in $(echo Dp2Kmpippip); do
	#for mode in $(echo Dp2KS0pipLL); do
	#for mode in $(echo DstToD0pi_KK); do
	#for mode in $(echo Dst2D0pi_D02Kpi); do
	    long_mode=$mode
	    if [ $mode = "DstToD0pi_KK" ]; then 
		long_mode=$(echo D02KmKp)
	    elif [ $mode = "Dst2D0pi_D02Kpi" ]; then 
		long_mode=$(echo D02Kmpip) 
	    fi 
	    echo "#!/bin/sh" > exec/pGun.hadd.$mode.$year.$pol.sh
	    echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> exec/pGun.hadd.$mode.$year.$pol.sh 
	    echo "cd $DATADIR" >> exec/pGun.hadd.$mode.$year.$pol.sh
	    echo "hostname" >> exec/pGun.hadd.$mode.$year.$pol.sh
	    #echo "hadd -f ${long_mode}_${year}_${short_pol}_pGun_ALL.root ${mode}_20${year}_${short_pol}_*RECO*.root" >> exec/pGun.hadd.$mode.$year.$pol.sh
	    #echo "rm ${mode}_20${year}_${short_pol}_*RECO*.root" >> exec/pGun.hadd.$mode.$year.$pol.sh
	    echo "hadd -f ${long_mode}_${year}_${short_pol}_pGun.root ${mode}_20${year}_${short_pol}_*RECO*.root" >> exec/pGun.hadd.$mode.$year.$pol.sh
	    echo "rm ${mode}_20${year}_${short_pol}_*RECO*.root" >> exec/pGun.hadd.$mode.$year.$pol.sh
	    chmod +x exec/pGun.hadd.$mode.$year.$pol.sh
	    python condor_make_template.py condor_template.sub pGun.hadd.$mode.$year.$pol
	    condor_submit exec/condor_template.pGun.hadd.$mode.$year.$pol.sub -batch-name hadd$mode$year$pol
	    #break
	done
	#break
    done
    #break
done
