#include <iostream>      
#include <stdio.h>       
#include <stdlib.h>      
#include <TString.h>         
#include <TFile.h>            
#include <TChain.h>      
#include <TTree.h>  
#include <TLorentzVector.h>   
#include <TVector3.h>   
#include <TFileCollection.h>      
#include <TMath.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TRandom3.h>
#include <TColor.h>
#include "/home/LHCB/smaccoli/Tools.h"


using namespace std;                   

std::pair<double,double> switcher(int dec, double a, double b, double r) {
  
  std::pair<double,double> p;
  if(dec==1)
    p = std::make_pair(a,b);
  else if(dec==-1)
    p = std::make_pair(b,a);
  else if(dec==0) 
    r > 0.5 ? p = std::make_pair(a,b) : p = std::make_pair(b,a);

  return p;
}

std::pair<int,int> switcher(int dec, int a, int b, double r) {
  
  std::pair<int,int> p;
  if(dec==1)
    p = std::make_pair(a,b);
  else if(dec==-1)
    p = std::make_pair(b,a);
  else if(dec==0) 
    r > 0.5 ? p = std::make_pair(a,b) : p = std::make_pair(b,a); 

  return p;
}

int main(int argc, char * argv[]) { 

  TString inputFile     = argv[1];
  TString inputNtuple   = argv[2];
  TString outputFile    = argv[3];
  TString outputNtuple  = argv[4];
  int doSelection       = atoi(argv[5]);                        
  int doFiducial        = atoi(argv[6]);                        
  int doMultCandRemoval = atoi(argv[7]);                       
  int isPGun = atoi(argv[8]);                       
  TString seed          = argv[9];

  Bool_t use_pGun_all = 1 && isPGun;

  if(isPGun)  {                                                                                        
    printf("USING SIMULATED SAMPLE: %d\n", isPGun);                                         
  }                                                                                  
  if(doSelection) {                                                                                               
    printf("DOING SELECTION CUTS: %d\n", doSelection);                                                          
    //outputFile.ReplaceAll(".root","_withSelection.root");                                                       
  }                                                                                                             
  if(doFiducial) {                                                                                                
    printf("DOING FIDUCIAL CUTS: %d\n",  doFiducial);                                                             
    //outputFile.ReplaceAll(".root","_withFiducialCuts.root");                                                      
  }                                                                                                               
  if(doMultCandRemoval)  {                                                                                        
    printf("DOING MULTIPLE CANDIDATES REMOVAL: %d\n", doMultCandRemoval);                                         
    outputFile.ReplaceAll(".root","_noMultCandRemoval.root");                                                     
  }                                                                                  

  TRandom3 * rnd = new TRandom3();
  rnd->SetSeed(atoi(seed));
  
  if (use_pGun_all)
    inputFile.ReplaceAll(".root","_ALL.root");
 
  TFile * f;
  f = TFile::Open(inputFile);
  if(!f) cout << "FILE " << inputFile << " DOES NOT EXIST!!" << endl;
  TTree * ntp;
  if(isPGun) {
    ntp = (TTree*)f->Get("Dp2Kmpippip/DecayTree");
    inputFile.ReplaceAll(".root","_Hlt1TrackMVA.root");
    ntp->AddFriend("DecayTree",inputFile);
    inputFile.ReplaceAll("_Hlt1TrackMVA.root",".root");
  }
  else
    ntp = (TTree*)f->Get(inputNtuple);
  if(!ntp) cout << "NTUPLE " << inputNtuple << " DOES NOT EXIST!!" << endl;

  /*
    ntp->SetBranchStatus("*",0);
    ntp->SetBranchStatus("*PT",1);
    ntp->SetBranchStatus("*ETA",1);
    ntp->SetBranchStatus("*PHI",1);
    ntp->SetBranchStatus("*Hlt1*",1);
    ntp->SetBranchStatus("*L0*",1);
    ntp->SetBranchStatus("*PID*",1);
    ntp->SetBranchStatus("*TRUE*",1);
    ntp->SetBranchStatus("*TRACK*",1);
    ntp->SetBranchStatus("*M",1);
    ntp->SetBranchStatus("*ID",1);
  */

  //TruthMatching variables
  Int_t Dplus_TRUEID = 0;
  Int_t hplus_TRUEID = 0;
  Int_t Kminus_TRUEID = 0;
  Int_t piplus_TRUEID = 0;
  Int_t hplus_MC_MOTHER_ID = 0;
  Int_t Kminus_MC_MOTHER_ID = 0;
  Int_t piplus_MC_MOTHER_ID = 0;
  Int_t hplus_MC_MOTHER_KEY = 0;
  Int_t Kminus_MC_MOTHER_KEY = 0;
  Int_t piplus_MC_MOTHER_KEY = 0;
  Int_t Dplus_BKGCAT = 0;
  
  // Int_t Dplus_ENDVERTEX_NDOF = 0;  
  //  double Dplus_ENDVERTEX_CHI2;
  bool hplus_TRUEISSTABLE;
  bool piplus_TRUEISSTABLE;
  bool Kminus_TRUEISSTABLE;
  
  if (isPGun) {
    ntp->SetBranchStatus("Dplus_TRUEID",1);
    ntp->SetBranchStatus("Dplus_BKGCAT",1);
    ntp->SetBranchStatus("hplus_TRUEID",1);
    ntp->SetBranchStatus("Kminus_TRUEID",1);
    ntp->SetBranchStatus("piplus_TRUEID",1);
    ntp->SetBranchStatus("hplus_MC_MOTHER_ID",1);
    ntp->SetBranchStatus("Kminus_MC_MOTHER_ID",1);
    ntp->SetBranchStatus("piplus_MC_MOTHER_ID",1);
    ntp->SetBranchStatus("hplus_MC_MOTHER_KEY",1);
    ntp->SetBranchStatus("Kminus_MC_MOTHER_KEY",1);
    ntp->SetBranchStatus("piplus_MC_MOTHER_KEY",1);
  
    ntp->SetBranchAddress("Dplus_TRUEID",&Dplus_TRUEID);
    ntp->SetBranchAddress("Dplus_BKGCAT",&Dplus_BKGCAT);
    ntp->SetBranchAddress("hplus_TRUEID",&hplus_TRUEID);
    ntp->SetBranchAddress("Kminus_TRUEID",&Kminus_TRUEID);
    ntp->SetBranchAddress("piplus_TRUEID",&piplus_TRUEID);
    ntp->SetBranchAddress("hplus_MC_MOTHER_ID",&hplus_MC_MOTHER_ID);
    ntp->SetBranchAddress("Kminus_MC_MOTHER_ID",&Kminus_MC_MOTHER_ID);
    ntp->SetBranchAddress("piplus_MC_MOTHER_ID",&piplus_MC_MOTHER_ID);
    ntp->SetBranchAddress("hplus_MC_MOTHER_KEY",&hplus_MC_MOTHER_KEY);
    ntp->SetBranchAddress("Kminus_MC_MOTHER_KEY",&Kminus_MC_MOTHER_KEY);
    ntp->SetBranchAddress("piplus_MC_MOTHER_KEY",&piplus_MC_MOTHER_KEY);  
 
    //    ntp->SetBranchAddress("Dplus_ENDVERTEX_NDOF",&Dplus_ENDVERTEX_NDOF);
    //  ntp->SetBranchAddress("Dplus_ENDVERTEX_CHI2",&Dplus_ENDVERTEX_CHI2);

    ntp->SetBranchStatus("hplus_TRUEISSTABLE", 1); ntp->SetBranchAddress("hplus_TRUEISSTABLE", &hplus_TRUEISSTABLE);
    ntp->SetBranchStatus("piplus_TRUEISSTABLE", 1); ntp->SetBranchAddress("piplus_TRUEISSTABLE", &piplus_TRUEISSTABLE);
    ntp->SetBranchStatus("Kminus_TRUEISSTABLE", 1); ntp->SetBranchAddress("Kminus_TRUEISSTABLE", &Kminus_TRUEISSTABLE);

  }
  
  ///masses 
  double Dplus_M; ntp->SetBranchStatus("Dplus_M", 1); ntp->SetBranchAddress("Dplus_M", &Dplus_M);

  //lifetimes
  double Dplus_TAU; ntp->SetBranchStatus("Dplus_TAU", 1); ntp->SetBranchAddress("Dplus_TAU", &Dplus_TAU);                                              
  //tag
  int Dplus_ID; ntp->SetBranchStatus("Dplus_ID", 1); ntp->SetBranchAddress("Dplus_ID", &Dplus_ID);

  //kinematics
  double Dplus_P; ntp->SetBranchStatus("Dplus_P", 1); ntp->SetBranchAddress("Dplus_P", &Dplus_P);
  double Dplus_PT; ntp->SetBranchStatus("Dplus_PT", 1); ntp->SetBranchAddress("Dplus_PT", &Dplus_PT);
  double Dplus_ETA; ntp->SetBranchStatus("Dplus_ETA", 1); ntp->SetBranchAddress("Dplus_ETA", &Dplus_ETA);
  double Dplus_PHI; ntp->SetBranchStatus("Dplus_PHI", 1); ntp->SetBranchAddress("Dplus_PHI", &Dplus_PHI);
  double Dplus_PX; ntp->SetBranchStatus("Dplus_PX", 1); ntp->SetBranchAddress("Dplus_PX", &Dplus_PX);
  double Dplus_PY; ntp->SetBranchStatus("Dplus_PY", 1); ntp->SetBranchAddress("Dplus_PY", &Dplus_PY);
  double Dplus_PZ; ntp->SetBranchStatus("Dplus_PZ", 1); ntp->SetBranchAddress("Dplus_PZ", &Dplus_PZ);

  double Kminus_P; ntp->SetBranchStatus("Kminus_P", 1); ntp->SetBranchAddress("Kminus_P", &Kminus_P);
  double Kminus_PT; ntp->SetBranchStatus("Kminus_PT", 1); ntp->SetBranchAddress("Kminus_PT", &Kminus_PT);
  double Kminus_ETA; ntp->SetBranchStatus("Kminus_ETA", 1); ntp->SetBranchAddress("Kminus_ETA", &Kminus_ETA);
  double Kminus_PHI; ntp->SetBranchStatus("Kminus_PHI", 1); ntp->SetBranchAddress("Kminus_PHI", &Kminus_PHI);
  double Kminus_PX; ntp->SetBranchStatus("Kminus_PX", 1); ntp->SetBranchAddress("Kminus_PX", &Kminus_PX);
  double Kminus_PY; ntp->SetBranchStatus("Kminus_PY", 1); ntp->SetBranchAddress("Kminus_PY", &Kminus_PY);
  double Kminus_PZ; ntp->SetBranchStatus("Kminus_PZ", 1); ntp->SetBranchAddress("Kminus_PZ", &Kminus_PZ);

  double piplus1_P; ntp->SetBranchStatus("piplus1_P", 1); ntp->SetBranchAddress("piplus1_P", &piplus1_P);
  double piplus1_PT; ntp->SetBranchStatus("piplus1_PT", 1); ntp->SetBranchAddress("piplus1_PT", &piplus1_PT);
  double piplus1_ETA; ntp->SetBranchStatus("piplus1_ETA", 1); ntp->SetBranchAddress("piplus1_ETA", &piplus1_ETA);
  double piplus1_PHI; ntp->SetBranchStatus("piplus1_PHI", 1); ntp->SetBranchAddress("piplus1_PHI", &piplus1_PHI);
  double piplus1_PX; ntp->SetBranchStatus("piplus1_PX", 1); ntp->SetBranchAddress("piplus1_PX", &piplus1_PX);
  double piplus1_PY; ntp->SetBranchStatus("piplus1_PY", 1); ntp->SetBranchAddress("piplus1_PY", &piplus1_PY);
  double piplus1_PZ; ntp->SetBranchStatus("piplus1_PZ", 1); ntp->SetBranchAddress("piplus1_PZ", &piplus1_PZ);

  double piplus2_P; ntp->SetBranchStatus("piplus2_P", 1); ntp->SetBranchAddress("piplus2_P", &piplus2_P);
  double piplus2_PT; ntp->SetBranchStatus("piplus2_PT", 1); ntp->SetBranchAddress("piplus2_PT", &piplus2_PT);
  double piplus2_ETA; ntp->SetBranchStatus("piplus2_ETA", 1); ntp->SetBranchAddress("piplus2_ETA", &piplus2_ETA);
  double piplus2_PHI; ntp->SetBranchStatus("piplus2_PHI", 1); ntp->SetBranchAddress("piplus2_PHI", &piplus2_PHI);
  double piplus2_PX; ntp->SetBranchStatus("piplus2_PX", 1); ntp->SetBranchAddress("piplus2_PX", &piplus2_PX);
  double piplus2_PY; ntp->SetBranchStatus("piplus2_PY", 1); ntp->SetBranchAddress("piplus2_PY", &piplus2_PY);
  double piplus2_PZ; ntp->SetBranchStatus("piplus2_PZ", 1); ntp->SetBranchAddress("piplus2_PZ", &piplus2_PZ);

  //PID
  double Kminus_PIDK; ntp->SetBranchStatus("Kminus_PIDK", 1); ntp->SetBranchAddress("Kminus_PIDK", &Kminus_PIDK);
  double piplus1_PIDK; ntp->SetBranchStatus("piplus1_PIDK", 1); ntp->SetBranchAddress("piplus1_PIDK", &piplus1_PIDK);
  double piplus2_PIDK; ntp->SetBranchStatus("piplus2_PIDK", 1); ntp->SetBranchAddress("piplus2_PIDK", &piplus2_PIDK);
  double Kminus_PIDp; ntp->SetBranchStatus("Kminus_PIDp", 1); ntp->SetBranchAddress("Kminus_PIDp", &Kminus_PIDp);
  double piplus1_PIDp; ntp->SetBranchStatus("piplus1_PIDp", 1); ntp->SetBranchAddress("piplus1_PIDp", &piplus1_PIDp);
  double piplus2_PIDp; ntp->SetBranchStatus("piplus2_PIDp", 1); ntp->SetBranchAddress("piplus2_PIDp", &piplus2_PIDp);
  
  //Track
  double Dplus_IP_OWNPV; ntp->SetBranchStatus("Dplus_IP_OWNPV", 1); ntp->SetBranchAddress("Dplus_IP_OWNPV", &Dplus_IP_OWNPV);  
  double Kminus_IP_OWNPV; ntp->SetBranchStatus("Kminus_IP_OWNPV", 1); ntp->SetBranchAddress("Kminus_IP_OWNPV", &Kminus_IP_OWNPV);  
  double piplus1_IP_OWNPV; ntp->SetBranchStatus("piplus1_IP_OWNPV", 1); ntp->SetBranchAddress("piplus1_IP_OWNPV", &piplus1_IP_OWNPV); 
  double piplus2_IP_OWNPV; ntp->SetBranchStatus("piplus2_IP_OWNPV", 1); ntp->SetBranchAddress("piplus2_IP_OWNPV", &piplus2_IP_OWNPV);                                

  double Dplus_IPCHI2_OWNPV; ntp->SetBranchStatus("Dplus_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("Dplus_IPCHI2_OWNPV", &Dplus_IPCHI2_OWNPV);  
  double Kminus_IPCHI2_OWNPV; ntp->SetBranchStatus("Kminus_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("Kminus_IPCHI2_OWNPV", &Kminus_IPCHI2_OWNPV);  
  double piplus1_IPCHI2_OWNPV; ntp->SetBranchStatus("piplus1_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("piplus1_IPCHI2_OWNPV", &piplus1_IPCHI2_OWNPV); 
  double piplus2_IPCHI2_OWNPV; ntp->SetBranchStatus("piplus2_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("piplus2_IPCHI2_OWNPV", &piplus2_IPCHI2_OWNPV);                                

  double Dplus_FD_OWNPV; ntp->SetBranchStatus("Dplus_FD_OWNPV", 1); ntp->SetBranchAddress("Dplus_FD_OWNPV", &Dplus_FD_OWNPV);  
  double Dplus_FDCHI2_OWNPV; ntp->SetBranchStatus("Dplus_FDCHI2_OWNPV", 1); ntp->SetBranchAddress("Dplus_FDCHI2_OWNPV", &Dplus_FDCHI2_OWNPV);  

  double Kminus_TRACK_CHI2NDOF; ntp->SetBranchStatus("Kminus_TRACK_CHI2NDOF", 1); ntp->SetBranchAddress("Kminus_TRACK_CHI2NDOF", &Kminus_TRACK_CHI2NDOF);                              
  double piplus1_TRACK_CHI2NDOF; ntp->SetBranchStatus("piplus1_TRACK_CHI2NDOF", 1); ntp->SetBranchAddress("piplus1_TRACK_CHI2NDOF", &piplus1_TRACK_CHI2NDOF); 
  double piplus2_TRACK_CHI2NDOF; ntp->SetBranchStatus("piplus2_TRACK_CHI2NDOF", 1); ntp->SetBranchAddress("piplus2_TRACK_CHI2NDOF", &piplus2_TRACK_CHI2NDOF);                              

  double Kminus_TRACK_GhostProb; ntp->SetBranchStatus("Kminus_TRACK_GhostProb", 1); ntp->SetBranchAddress("Kminus_TRACK_GhostProb", &Kminus_TRACK_GhostProb); 
  double piplus1_TRACK_GhostProb; ntp->SetBranchStatus("piplus1_TRACK_GhostProb", 1); ntp->SetBranchAddress("piplus1_TRACK_GhostProb", &piplus1_TRACK_GhostProb); 
  double piplus2_TRACK_GhostProb; ntp->SetBranchStatus("piplus2_TRACK_GhostProb", 1); ntp->SetBranchAddress("piplus2_TRACK_GhostProb", &piplus2_TRACK_GhostProb);                              

  //geometrical
  double Dplus_DIRA_OWNPV; ntp->SetBranchStatus("Dplus_DIRA_OWNPV", 1); ntp->SetBranchAddress("Dplus_DIRA_OWNPV", &Dplus_DIRA_OWNPV);  
  double Dplus_DOCA_Kminus_piplus1; ntp->SetBranchStatus("Dplus_DOCA_Kminus_piplus1", 1); ntp->SetBranchAddress("Dplus_DOCA_Kminus_piplus1", &Dplus_DOCA_Kminus_piplus1);  
  double Dplus_DOCA_Kminus_piplus2; ntp->SetBranchStatus("Dplus_DOCA_Kminus_piplus2", 1); ntp->SetBranchAddress("Dplus_DOCA_Kminus_piplus2", &Dplus_DOCA_Kminus_piplus2);  
  double Dplus_DOCA_piplus1_piplus2; ntp->SetBranchStatus("Dplus_DOCA_piplus1_piplus2", 1); ntp->SetBranchAddress("Dplus_DOCA_piplus1_piplus2", &Dplus_DOCA_piplus1_piplus2);  
  double Dplus_DOCACHI2_Kminus_piplus1; ntp->SetBranchStatus("Dplus_DOCACHI2_Kminus_piplus1", 1); ntp->SetBranchAddress("Dplus_DOCACHI2_Kminus_piplus1", &Dplus_DOCACHI2_Kminus_piplus1);  
  double Dplus_DOCACHI2_Kminus_piplus2; ntp->SetBranchStatus("Dplus_DOCACHI2_Kminus_piplus2", 1); ntp->SetBranchAddress("Dplus_DOCACHI2_Kminus_piplus2", &Dplus_DOCACHI2_Kminus_piplus2);  
  double Dplus_DOCACHI2_piplus1_piplus2; ntp->SetBranchStatus("Dplus_DOCACHI2_piplus1_piplus2", 1); ntp->SetBranchAddress("Dplus_DOCACHI2_piplus1_piplus2", &Dplus_DOCACHI2_piplus1_piplus2);  


  //sums
  double Dplus_SUMPTChildren; ntp->SetBranchStatus("Dplus_SUMPTChildren", 1); ntp->SetBranchAddress("Dplus_SUMPTChildren", &Dplus_SUMPTChildren);       

  //Vertices
  float  PVX[100]; ntp->SetBranchStatus("PVX", 1); ntp->SetBranchAddress("PVX", PVX); 
  float  PVY[100]; ntp->SetBranchStatus("PVY", 1); ntp->SetBranchAddress("PVY", PVY); 
  float  PVZ[100]; ntp->SetBranchStatus("PVZ", 1); ntp->SetBranchAddress("PVZ", PVZ); 

  int nPVs; ntp->SetBranchStatus("nPVs", 1); ntp->SetBranchAddress("nPVs", &nPVs);   

  double Dplus_OWNPV_X; ntp->SetBranchStatus("Dplus_OWNPV_X", 1); ntp->SetBranchAddress("Dplus_OWNPV_X", &Dplus_OWNPV_X);   
  double Dplus_OWNPV_Y; ntp->SetBranchStatus("Dplus_OWNPV_Y", 1); ntp->SetBranchAddress("Dplus_OWNPV_Y", &Dplus_OWNPV_Y);   
  double Dplus_OWNPV_Z; ntp->SetBranchStatus("Dplus_OWNPV_Z", 1); ntp->SetBranchAddress("Dplus_OWNPV_Z", &Dplus_OWNPV_Z);   

  double Dplus_ENDVERTEX_X; ntp->SetBranchStatus("Dplus_ENDVERTEX_X", 1); ntp->SetBranchAddress("Dplus_ENDVERTEX_X", &Dplus_ENDVERTEX_X);   
  double Dplus_ENDVERTEX_Y; ntp->SetBranchStatus("Dplus_ENDVERTEX_Y", 1); ntp->SetBranchAddress("Dplus_ENDVERTEX_Y", &Dplus_ENDVERTEX_Y);   
  double Dplus_ENDVERTEX_Z; ntp->SetBranchStatus("Dplus_ENDVERTEX_Z", 1); ntp->SetBranchAddress("Dplus_ENDVERTEX_Z", &Dplus_ENDVERTEX_Z);   

  double Dplus_ENDVERTEX_CHI2; ntp->SetBranchStatus("Dplus_ENDVERTEX_CHI2", 1); ntp->SetBranchAddress("Dplus_ENDVERTEX_CHI2", &Dplus_ENDVERTEX_CHI2);   

  //Triggers
  bool Dplus_L0HadronDecision_TIS; ntp->SetBranchStatus("Dplus_L0HadronDecision_TIS", 1); ntp->SetBranchAddress("Dplus_L0HadronDecision_TIS", &Dplus_L0HadronDecision_TIS);
  bool Dplus_L0HadronDecision_TOS; ntp->SetBranchStatus("Dplus_L0HadronDecision_TOS", 1); ntp->SetBranchAddress("Dplus_L0HadronDecision_TOS", &Dplus_L0HadronDecision_TOS);
  bool piplus1_L0HadronDecision_TOS; ntp->SetBranchStatus("piplus1_L0HadronDecision_TOS", 1); ntp->SetBranchAddress("piplus1_L0HadronDecision_TOS", &piplus1_L0HadronDecision_TOS);
  bool piplus2_L0HadronDecision_TOS; ntp->SetBranchStatus("piplus2_L0HadronDecision_TOS", 1); ntp->SetBranchAddress("piplus2_L0HadronDecision_TOS", &piplus2_L0HadronDecision_TOS);
  bool Kminus_L0HadronDecision_TOS; ntp->SetBranchStatus("Kminus_L0HadronDecision_TOS", 1); ntp->SetBranchAddress("Kminus_L0HadronDecision_TOS", &Kminus_L0HadronDecision_TOS);

  bool Dplus_L0Global_TIS; ntp->SetBranchStatus("Dplus_L0Global_TIS", 1); ntp->SetBranchAddress("Dplus_L0Global_TIS", &Dplus_L0Global_TIS);
  bool Dplus_L0Global_TOS; ntp->SetBranchStatus("Dplus_L0Global_TOS", 1); ntp->SetBranchAddress("Dplus_L0Global_TOS", &Dplus_L0Global_TOS);
  bool Dplus_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("Dplus_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("Dplus_Hlt1TrackMVADecision_TIS", &Dplus_Hlt1TrackMVADecision_TIS);
  bool Dplus_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("Dplus_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("Dplus_Hlt1TrackMVADecision_TOS", &Dplus_Hlt1TrackMVADecision_TOS);
  bool piplus1_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("piplus1_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("piplus1_Hlt1TrackMVADecision_TIS", &piplus1_Hlt1TrackMVADecision_TIS);
  bool piplus1_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("piplus1_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("piplus1_Hlt1TrackMVADecision_TOS", &piplus1_Hlt1TrackMVADecision_TOS);
  bool piplus2_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("piplus2_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("piplus2_Hlt1TrackMVADecision_TIS", &piplus2_Hlt1TrackMVADecision_TIS);
  bool piplus2_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("piplus2_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("piplus2_Hlt1TrackMVADecision_TOS", &piplus2_Hlt1TrackMVADecision_TOS);
  bool Kminus_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("Kminus_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("Kminus_Hlt1TrackMVADecision_TIS", &Kminus_Hlt1TrackMVADecision_TIS);
  bool Kminus_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("Kminus_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("Kminus_Hlt1TrackMVADecision_TOS", &Kminus_Hlt1TrackMVADecision_TOS);

  bool Dplus_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("Dplus_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("Dplus_Hlt1TwoTrackMVADecision_TIS", &Dplus_Hlt1TwoTrackMVADecision_TIS);
  bool Dplus_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("Dplus_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("Dplus_Hlt1TwoTrackMVADecision_TOS", &Dplus_Hlt1TwoTrackMVADecision_TOS);
  bool piplus1_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("piplus1_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("piplus1_Hlt1TwoTrackMVADecision_TIS", &piplus1_Hlt1TwoTrackMVADecision_TIS);
  bool piplus1_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("piplus1_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("piplus1_Hlt1TwoTrackMVADecision_TOS", &piplus1_Hlt1TwoTrackMVADecision_TOS);
  bool piplus2_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("piplus2_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("piplus2_Hlt1TwoTrackMVADecision_TIS", &piplus2_Hlt1TwoTrackMVADecision_TIS);
  bool piplus2_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("piplus2_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("piplus2_Hlt1TwoTrackMVADecision_TOS", &piplus2_Hlt1TwoTrackMVADecision_TOS);
  bool Kminus_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("Kminus_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("Kminus_Hlt1TwoTrackMVADecision_TIS", &Kminus_Hlt1TwoTrackMVADecision_TIS);
  bool Kminus_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("Kminus_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("Kminus_Hlt1TwoTrackMVADecision_TOS", &Kminus_Hlt1TwoTrackMVADecision_TOS);

  double hplus_Hlt1TrackMVAResponse; 
  double piplus_Hlt1TrackMVAResponse;
  double Kminus_Hlt1TrackMVAResponse;

  if(isPGun) {
    ntp->SetBranchStatus("piplus_P", 1); ntp->SetBranchAddress("piplus_P", &piplus1_P);
    ntp->SetBranchStatus("piplus_PT", 1); ntp->SetBranchAddress("piplus_PT", &piplus1_PT);
    ntp->SetBranchStatus("piplus_ETA", 1); ntp->SetBranchAddress("piplus_ETA", &piplus1_ETA);
    ntp->SetBranchStatus("piplus_PHI", 1); ntp->SetBranchAddress("piplus_PHI", &piplus1_PHI);
    ntp->SetBranchStatus("piplus_PX", 1); ntp->SetBranchAddress("piplus_PX", &piplus1_PX);
    ntp->SetBranchStatus("piplus_PY", 1); ntp->SetBranchAddress("piplus_PY", &piplus1_PY);
    ntp->SetBranchStatus("piplus_PZ", 1); ntp->SetBranchAddress("piplus_PZ", &piplus1_PZ);
    ntp->SetBranchStatus("hplus_P", 1); ntp->SetBranchAddress("hplus_P", &piplus2_P);
    ntp->SetBranchStatus("hplus_PT", 1); ntp->SetBranchAddress("hplus_PT", &piplus2_PT);
    ntp->SetBranchStatus("hplus_ETA", 1); ntp->SetBranchAddress("hplus_ETA", &piplus2_ETA);
    ntp->SetBranchStatus("hplus_PHI", 1); ntp->SetBranchAddress("hplus_PHI", &piplus2_PHI);
    ntp->SetBranchStatus("hplus_PX", 1); ntp->SetBranchAddress("hplus_PX", &piplus2_PX);
    ntp->SetBranchStatus("hplus_PY", 1); ntp->SetBranchAddress("hplus_PY", &piplus2_PY);
    ntp->SetBranchStatus("hplus_PZ", 1); ntp->SetBranchAddress("hplus_PZ", &piplus2_PZ);

    ntp->SetBranchStatus("piplus_PIDK", 1); ntp->SetBranchAddress("piplus_PIDK", &piplus1_PIDK);
    ntp->SetBranchStatus("hplus_PIDK", 1); ntp->SetBranchAddress("hplus_PIDK", &piplus2_PIDK);
    ntp->SetBranchStatus("piplus_PIDp", 1); ntp->SetBranchAddress("piplus_PIDp", &piplus1_PIDp);
    ntp->SetBranchStatus("hplus_PIDp", 1); ntp->SetBranchAddress("hplus_PIDp", &piplus2_PIDp);
 
    ntp->SetBranchStatus("piplus_IP_OWNPV", 1); ntp->SetBranchAddress("piplus_IP_OWNPV", &piplus1_IP_OWNPV);
    ntp->SetBranchStatus("hplus_IP_OWNPV", 1); ntp->SetBranchAddress("hplus_IP_OWNPV", &piplus2_IP_OWNPV);

    ntp->SetBranchStatus("piplus_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("piplus_IPCHI2_OWNPV", &piplus1_IPCHI2_OWNPV);
    ntp->SetBranchStatus("hplus_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("hplus_IPCHI2_OWNPV", &piplus2_IPCHI2_OWNPV);

    ntp->SetBranchStatus("piplus_TRACK_CHI2NDOF", 1); ntp->SetBranchAddress("piplus_TRACK_CHI2NDOF", &piplus1_TRACK_CHI2NDOF);
    ntp->SetBranchStatus("hplus_TRACK_CHI2NDOF", 1); ntp->SetBranchAddress("hplus_TRACK_CHI2NDOF", &piplus2_TRACK_CHI2NDOF);

    ntp->SetBranchStatus("piplus_TRACK_GhostProb", 1); ntp->SetBranchAddress("piplus_TRACK_GhostProb", &piplus1_TRACK_GhostProb);
    ntp->SetBranchStatus("hplus_TRACK_GhostProb", 1); ntp->SetBranchAddress("hplus_TRACK_GhostProb", &piplus2_TRACK_GhostProb);  
    
    ntp->SetBranchStatus("hplus_Hlt1TrackMVAResponse", 1); ntp->SetBranchAddress("hplus_Hlt1TrackMVAResponse", &hplus_Hlt1TrackMVAResponse);
    ntp->SetBranchStatus("piplus_Hlt1TrackMVAResponse", 1); ntp->SetBranchAddress("piplus_Hlt1TrackMVAResponse", &piplus_Hlt1TrackMVAResponse);
    ntp->SetBranchStatus("Kminus_Hlt1TrackMVAResponse", 1); ntp->SetBranchAddress("Kminus_Hlt1TrackMVAResponse", &Kminus_Hlt1TrackMVAResponse);
  }

  //Event
  ULong64_t eventNumber; ntp->SetBranchStatus("eventNumber", 1); ntp->SetBranchAddress("eventNumber", &eventNumber); 
  UInt_t runNumber; ntp->SetBranchStatus("runNumber", 1); ntp->SetBranchAddress("runNumber", &runNumber); 
  unsigned int nCandidate; ntp->SetBranchStatus("nCandidate", 1); ntp->SetBranchAddress("nCandidate", &nCandidate); 
  int nVeloClusters; ntp->SetBranchStatus("nVeloClusters", 1); ntp->SetBranchAddress("nVeloClusters",&nVeloClusters);
  int nOTClusters; ntp->SetBranchStatus("nOTClusters", 1); ntp->SetBranchAddress("nOTClusters",&nOTClusters);
  ULong64_t totCandidates; ntp->SetBranchStatus("totCandidates", 1); ntp->SetBranchAddress("totCandidates", &totCandidates);

  TFile * fOut = new TFile(outputFile, "RECREATE");
  TTree * ntpOut = new TTree(outputNtuple,outputNtuple);
  /*
    ntp->SetBranchStatus("*",1);
    TTree * ntpOut = ntp->CloneTree(0);
    ntpOut->SetName(outputNtuple);
    ntpOut->SetTitle(outputNtuple);
  */

  //masses
  double o_Dplus_M; ntpOut->Branch("Dplus_M", &o_Dplus_M); 

  //tag
  int o_Dplus_ID; ntpOut->Branch("Dplus_ID", &o_Dplus_ID); 

  //lifetimes
  double o_Dplus_TAU; ntpOut->Branch("Dplus_TAU", &o_Dplus_TAU);

  
  
  //kinematics
  double o_Dplus_P; ntpOut->Branch("Dplus_P", &o_Dplus_P);   
  double o_Dplus_PT; ntpOut->Branch("Dplus_PT", &o_Dplus_PT);   
  double o_Dplus_ETA; ntpOut->Branch("Dplus_ETA", &o_Dplus_ETA);   
  double o_Dplus_PHI; ntpOut->Branch("Dplus_PHI", &o_Dplus_PHI);   
  double o_Dplus_PX; ntpOut->Branch("Dplus_PX", &o_Dplus_PX);   
  double o_Dplus_PY; ntpOut->Branch("Dplus_PY", &o_Dplus_PY);   
  double o_Dplus_PZ; ntpOut->Branch("Dplus_PZ", &o_Dplus_PZ);   

  double o_Kminus_P; ntpOut->Branch("Kminus_P", &o_Kminus_P);   
  double o_Kminus_PT; ntpOut->Branch("Kminus_PT", &o_Kminus_PT);   
  double o_Kminus_ETA; ntpOut->Branch("Kminus_ETA", &o_Kminus_ETA);   
  double o_Kminus_PHI; ntpOut->Branch("Kminus_PHI", &o_Kminus_PHI);   
  double o_Kminus_PX; ntpOut->Branch("Kminus_PX", &o_Kminus_PX);   
  double o_Kminus_PY; ntpOut->Branch("Kminus_PY", &o_Kminus_PY);   
  double o_Kminus_PZ; ntpOut->Branch("Kminus_PZ", &o_Kminus_PZ);   

  double o_piplus_P; ntpOut->Branch("piplus_P", &o_piplus_P);   
  double o_piplus_PT; ntpOut->Branch("piplus_PT", &o_piplus_PT);   
  double o_piplus_ETA; ntpOut->Branch("piplus_ETA", &o_piplus_ETA);   
  double o_piplus_PHI; ntpOut->Branch("piplus_PHI", &o_piplus_PHI);   
  double o_piplus_PX; ntpOut->Branch("piplus_PX", &o_piplus_PX);   
  double o_piplus_PY; ntpOut->Branch("piplus_PY", &o_piplus_PY);   
  double o_piplus_PZ; ntpOut->Branch("piplus_PZ", &o_piplus_PZ);   

  /*double o_Kminus_P;*/ ntpOut->Branch("P2_P", &o_Kminus_P);   
  /*double o_Kminus_PT;*/ ntpOut->Branch("P2_PT", &o_Kminus_PT);   
  /*double o_Kminus_ETA;*/ ntpOut->Branch("P2_ETA", &o_Kminus_ETA);   
  /*double o_Kminus_PHI;*/ ntpOut->Branch("P2_PHI", &o_Kminus_PHI);   
  /*double o_Kminus_PX;*/ ntpOut->Branch("P2_PX", &o_Kminus_PX);   
  /*double o_Kminus_PY;*/ ntpOut->Branch("P2_PY", &o_Kminus_PY);   
  /*double o_Kminus_PZ;*/ ntpOut->Branch("P2_PZ", &o_Kminus_PZ);   

  /*double o_piplus_P;*/ ntpOut->Branch("P1_P", &o_piplus_P);   
  /*double o_piplus_PT;*/ ntpOut->Branch("P1_PT", &o_piplus_PT);   
  /*double o_piplus_ETA;*/ ntpOut->Branch("P1_ETA", &o_piplus_ETA);   
  /*double o_piplus_PHI;*/ ntpOut->Branch("P1_PHI", &o_piplus_PHI);   
  /*double o_piplus_PX;*/ ntpOut->Branch("P1_PX", &o_piplus_PX);   
  /*double o_piplus_PY;*/ ntpOut->Branch("P1_PY", &o_piplus_PY);   
  /*double o_piplus_PZ;*/ ntpOut->Branch("P1_PZ", &o_piplus_PZ);   

  double o_hplus_P; ntpOut->Branch("hplus_P", &o_hplus_P);   
  double o_hplus_PT; ntpOut->Branch("hplus_PT", &o_hplus_PT);   
  double o_hplus_ETA; ntpOut->Branch("hplus_ETA", &o_hplus_ETA);   
  double o_hplus_PHI; ntpOut->Branch("hplus_PHI", &o_hplus_PHI);   
  double o_hplus_PX; ntpOut->Branch("hplus_PX", &o_hplus_PX);   
  double o_hplus_PY; ntpOut->Branch("hplus_PY", &o_hplus_PY);   
  double o_hplus_PZ; ntpOut->Branch("hplus_PZ", &o_hplus_PZ);   
  

  //PID
  double o_piplus_PIDK; ntpOut->Branch("piplus_PIDK", &o_piplus_PIDK);                    
  double o_hplus_PIDK; ntpOut->Branch("hplus_PIDK", &o_hplus_PIDK);                    
  double o_Kminus_PIDK; ntpOut->Branch("Kminus_PIDK", &o_Kminus_PIDK);                    
  double o_piplus_PIDp; ntpOut->Branch("piplus_PIDp", &o_piplus_PIDp);                    
  double o_hplus_PIDp; ntpOut->Branch("hplus_PIDp", &o_hplus_PIDp);                    
  double o_Kminus_PIDp; ntpOut->Branch("Kminus_PIDp", &o_Kminus_PIDp);                    

  //Track
  double o_Dplus_IP_OWNPV; ntpOut->Branch("Dplus_IP_OWNPV", &o_Dplus_IP_OWNPV); 
  double o_Kminus_IP_OWNPV; ntpOut->Branch("Kminus_IP_OWNPV", &o_Kminus_IP_OWNPV); 
  double o_piplus_IP_OWNPV; ntpOut->Branch("piplus_IP_OWNPV", &o_piplus_IP_OWNPV); 
  double o_hplus_IP_OWNPV; ntpOut->Branch("hplus_IP_OWNPV", &o_hplus_IP_OWNPV); 

  double o_Dplus_IPCHI2_OWNPV; ntpOut->Branch("Dplus_IPCHI2_OWNPV", &o_Dplus_IPCHI2_OWNPV); 
  double o_Kminus_IPCHI2_OWNPV; ntpOut->Branch("Kminus_IPCHI2_OWNPV", &o_Kminus_IPCHI2_OWNPV); 
  double o_piplus_IPCHI2_OWNPV; ntpOut->Branch("piplus_IPCHI2_OWNPV", &o_piplus_IPCHI2_OWNPV); 
  double o_hplus_IPCHI2_OWNPV; ntpOut->Branch("hplus_IPCHI2_OWNPV", &o_hplus_IPCHI2_OWNPV); 
  
  double o_Dplus_FD_OWNPV; ntpOut->Branch("Dplus_FD_OWNPV", &o_Dplus_FD_OWNPV);                     
  double o_Dplus_FDCHI2_OWNPV; ntpOut->Branch("Dplus_FDCHI2_OWNPV", &o_Dplus_FDCHI2_OWNPV);                     

  double o_Kminus_TRACK_CHI2NDOF; ntpOut->Branch("Kminus_TRACK_CHI2NDOF", &o_Kminus_TRACK_CHI2NDOF);
  double o_piplus_TRACK_CHI2NDOF; ntpOut->Branch("piplus_TRACK_CHI2NDOF", &o_piplus_TRACK_CHI2NDOF);
  double o_hplus_TRACK_CHI2NDOF; ntpOut->Branch("hplus_TRACK_CHI2NDOF", &o_hplus_TRACK_CHI2NDOF);

  double o_Kminus_TRACK_GhostProb; ntpOut->Branch("Kminus_TRACK_GhostProb", &o_Kminus_TRACK_GhostProb);
  double o_piplus_TRACK_GhostProb; ntpOut->Branch("piplus_TRACK_GhostProb", &o_piplus_TRACK_GhostProb);
  double o_hplus_TRACK_GhostProb; ntpOut->Branch("hplus_TRACK_GhostProb", &o_hplus_TRACK_GhostProb);

  //geometrical
  double o_Dplus_DIRA_OWNPV; ntpOut->Branch("Dplus_DIRA_OWNPV", &o_Dplus_DIRA_OWNPV);

  double o_Dplus_DOCA_Kminus_piplus1; ntpOut->Branch("Dplus_DOCA_Kminus_piplus1", &o_Dplus_DOCA_Kminus_piplus1);      
  double o_Dplus_DOCA_Kminus_piplus2; ntpOut->Branch("Dplus_DOCA_Kminus_piplus2", &o_Dplus_DOCA_Kminus_piplus2);      
  double o_Dplus_DOCA_piplus1_piplus2; ntpOut->Branch("Dplus_DOCA_piplus1_piplus2", &o_Dplus_DOCA_piplus1_piplus2);      
  double o_Dplus_DOCACHI2_Kminus_piplus1; ntpOut->Branch("Dplus_DOCACHI2_Kminus_piplus1", &o_Dplus_DOCACHI2_Kminus_piplus1);      
  double o_Dplus_DOCACHI2_Kminus_piplus2; ntpOut->Branch("Dplus_DOCACHI2_Kminus_piplus2", &o_Dplus_DOCACHI2_Kminus_piplus2);      
  double o_Dplus_DOCACHI2_piplus1_piplus2; ntpOut->Branch("Dplus_DOCACHI2_piplus1_piplus2", &o_Dplus_DOCACHI2_piplus1_piplus2);      

  double o_Dplus_DOCA_Kminus_hplus; ntpOut->Branch("Dplus_DOCA_Kminus_hplus", &o_Dplus_DOCA_Kminus_hplus); 
  double o_Dplus_DOCACHI2_Kminus_hplus; ntpOut->Branch("Dplus_DOCACHI2_Kminus_hplus", &o_Dplus_DOCACHI2_Kminus_hplus);      

  //sums
  double o_Dplus_SUMPTChildren; ntpOut->Branch("Dplus_SUMPTChildren", &o_Dplus_SUMPTChildren);                         

  //Vertices
  double o_PVX; ntpOut->Branch("PVX", &o_PVX);                                                                                                                                                               
  double o_PVY; ntpOut->Branch("PVY", &o_PVY);                                                                                                                                                               
  double o_PVZ; ntpOut->Branch("PVZ", &o_PVZ);   

  int o_nPVs; ntpOut->Branch("nPVs", &o_nPVs);      

  double o_Dplus_OWNPV_X; ntpOut->Branch("Dplus_OWNPV_X", &o_Dplus_OWNPV_X);              
  double o_Dplus_OWNPV_Y; ntpOut->Branch("Dplus_OWNPV_Y", &o_Dplus_OWNPV_Y);              
  double o_Dplus_OWNPV_Z; ntpOut->Branch("Dplus_OWNPV_Z", &o_Dplus_OWNPV_Z);              

  double o_Dplus_ENDVERTEX_X; ntpOut->Branch("Dplus_ENDVERTEX_X", &o_Dplus_ENDVERTEX_X);              
  double o_Dplus_ENDVERTEX_Y; ntpOut->Branch("Dplus_ENDVERTEX_Y", &o_Dplus_ENDVERTEX_Y);              
  double o_Dplus_ENDVERTEX_Z; ntpOut->Branch("Dplus_ENDVERTEX_Z", &o_Dplus_ENDVERTEX_Z);              
  double o_Dplus_ENDVERTEX_CHI2; ntpOut->Branch("Dplus_ENDVERTEX_CHI2", &o_Dplus_ENDVERTEX_CHI2);              


  //Triggers
  int o_Dplus_L0HadronDecision_TIS; ntpOut->Branch("Dplus_L0HadronDecision_TIS", &o_Dplus_L0HadronDecision_TIS);   
  int o_Dplus_L0HadronDecision_TOS; ntpOut->Branch("Dplus_L0HadronDecision_TOS", &o_Dplus_L0HadronDecision_TOS);   
  int o_Kminus_L0HadronDecision_TOS; ntpOut->Branch("Kminus_L0HadronDecision_TOS", &o_Kminus_L0HadronDecision_TOS);  int o_piplus_L0HadronDecision_TOS; ntpOut->Branch("piplus_L0HadronDecision_TOS", &o_piplus_L0HadronDecision_TOS);  int o_hplus_L0HadronDecision_TOS; ntpOut->Branch("hplus_L0HadronDecision_TOS", &o_hplus_L0HadronDecision_TOS);
   
  int o_Dplus_L0Global_TIS; ntpOut->Branch("Dplus_L0Global_TIS", &o_Dplus_L0Global_TIS);   
  int o_Dplus_L0Global_TOS; ntpOut->Branch("Dplus_L0Global_TOS", &o_Dplus_L0Global_TOS);   

  int o_Dplus_Hlt1TrackMVADecision_TIS; ntpOut->Branch("Dplus_Hlt1TrackMVADecision_TIS", &o_Dplus_Hlt1TrackMVADecision_TIS);   
  int o_Dplus_Hlt1TrackMVADecision_TOS; ntpOut->Branch("Dplus_Hlt1TrackMVADecision_TOS", &o_Dplus_Hlt1TrackMVADecision_TOS);   
  int o_piplus_Hlt1TrackMVADecision_TIS; ntpOut->Branch("piplus_Hlt1TrackMVADecision_TIS", &o_piplus_Hlt1TrackMVADecision_TIS);   
  int o_piplus_Hlt1TrackMVADecision_TOS; ntpOut->Branch("piplus_Hlt1TrackMVADecision_TOS", &o_piplus_Hlt1TrackMVADecision_TOS);   
  int o_hplus_Hlt1TrackMVADecision_TIS; ntpOut->Branch("hplus_Hlt1TrackMVADecision_TIS", &o_hplus_Hlt1TrackMVADecision_TIS);   
  int o_hplus_Hlt1TrackMVADecision_TOS; ntpOut->Branch("hplus_Hlt1TrackMVADecision_TOS", &o_hplus_Hlt1TrackMVADecision_TOS);   
  int o_Kminus_Hlt1TrackMVADecision_TIS; ntpOut->Branch("Kminus_Hlt1TrackMVADecision_TIS", &o_Kminus_Hlt1TrackMVADecision_TIS);   
  int o_Kminus_Hlt1TrackMVADecision_TOS; ntpOut->Branch("Kminus_Hlt1TrackMVADecision_TOS", &o_Kminus_Hlt1TrackMVADecision_TOS);   
  
  int o_Dplus_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("Dplus_Hlt1TwoTrackMVADecision_TIS", &o_Dplus_Hlt1TwoTrackMVADecision_TIS);   
  int o_Dplus_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("Dplus_Hlt1TwoTrackMVADecision_TOS", &o_Dplus_Hlt1TwoTrackMVADecision_TOS);   
  int o_piplus_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("piplus_Hlt1TwoTrackMVADecision_TIS", &o_piplus_Hlt1TwoTrackMVADecision_TIS);   
  int o_piplus_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("piplus_Hlt1TwoTrackMVADecision_TOS", &o_piplus_Hlt1TwoTrackMVADecision_TOS);   
  int o_hplus_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("hplus_Hlt1TwoTrackMVADecision_TIS", &o_hplus_Hlt1TwoTrackMVADecision_TIS);   
  int o_hplus_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("hplus_Hlt1TwoTrackMVADecision_TOS", &o_hplus_Hlt1TwoTrackMVADecision_TOS);   
  int o_Kminus_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("Kminus_Hlt1TwoTrackMVADecision_TIS", &o_Kminus_Hlt1TwoTrackMVADecision_TIS);   
  int o_Kminus_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("Kminus_Hlt1TwoTrackMVADecision_TOS", &o_Kminus_Hlt1TwoTrackMVADecision_TOS);   
  
  //Event
  ULong64_t o_eventNumber; ntpOut->Branch("eventNumber", &o_eventNumber); 
  UInt_t  o_runNumber; ntpOut->Branch("runNumber", &o_runNumber); 
  unsigned int o_nCandidate; ntpOut->Branch("nCandidate",&o_nCandidate);
  int o_nVeloClusters; ntpOut->Branch("nVeloClusters",&o_nVeloClusters);
  int o_nOTClusters; ntpOut->Branch("nOTClusters",&o_nOTClusters);
  ULong64_t o_totCandidates; ntpOut->Branch("totCandidates", &o_totCandidates);
  
  //Derived quantities
  double o_Dplus_TIP_OWNPV; ntpOut->Branch("Dplus_TIP_OWNPV", &o_Dplus_TIP_OWNPV);                                          int o_decision; ntpOut->Branch("decision", &o_decision); 
  bool o_isPrompt; ntpOut->Branch("isPrompt", &o_isPrompt);
  
  double o_Kmpip_PT; ntpOut->Branch("X_PT",&o_Kmpip_PT);
  double o_Kmpip_ETA; ntpOut->Branch("X_ETA",&o_Kmpip_ETA);
  double o_Kmpip_PHI; ntpOut->Branch("X_PHI",&o_Kmpip_PHI);
  double o_Kmpip_P; ntpOut->Branch("X_P",&o_Kmpip_P);  
  double o_Kmpip_M; ntpOut->Branch("X_M",&o_Kmpip_M);  
  double o_Kmpip_pipi_M; ntpOut->Branch("X_pipi_M",&o_Kmpip_pipi_M);  
  
  //Useful variables
  TVector3 v_Dplus_OWNPV, v_Dplus_ENDVERTEX, p_Dplus; 
  TVector3 p_beam;
  TVector3 vecProduct;
  double Kminus_E,piplus_E;
  TLorentzVector Kminus, piplus, Kmpip;
  
  
  //Histograms
  TH1D * h_TIP = new TH1D("h_TIP","h_TIP",100,-0.2,0.2);
  TH1D * h_X_P = new TH1D("h_X_P","h_X_P",100,0,140);
  TH1D * h_X_PT = new TH1D("h_X_PT","h_X_PT",100,0,12e3);
  TH1D * h_X_ETA = new TH1D("h_X_ETA","h_X_ETA",100,1.5,5.5);
  TH2D * h_Dphp_PTPT = new TH2D("h_Dphp_PTPT","h_Dphp_PTPT",200,1e3,15e3,200,1e3,10e3);
  TH2D * h_Dphp_ETAETA = new TH2D("h_Dphp_ETAETA","h_Dphp_ETAETA",100,1.5,5.5,100,1.5,5.5);
  TH2D * h_Dphp_PHIPHI = new TH2D("h_Dphp_PHIPHI","h_Dphp_PHIPHI",100,-3.1416,3.1416,100,-3.1416,3.1416);
  TH2D * h_Kmpip_PTPT = new TH2D("h_Kmpip_PTPT","h_Kmpip_PTPT",200,800,8e3,200,800,8e3);
  TH2D * h_Kmpip_ETAETA = new TH2D("h_Kmpip_ETAETA","h_Kmpip_ETAETA",100,1.5,5.5,100,1.5,5.5);
  TH2D * h_Kmpip_PHIPHI = new TH2D("h_Kmpip_PHIPHI","h_Kmpip_PHIPHI",100,-3.1416,3.1416,100,-3.1416,3.1416);

  TH1D * h_X_diffPTs = new TH1D("h_X_diffPTs","h_X_diffPTs",100,0,12e3);
  TH1D * h_X_sumPTs = new TH1D("h_X_sumPTs","h_X_sumPTs",100,0,12e3);
  TH1D * h_ETAETA_diff = new TH1D("h_ETAETA_diff","h_ETAETA_diff",100,0,1);
  TH1D * h_PHIPHI_diff = new TH1D("h_PHIPHI_diff","h_PHIPHI_diff",100,0,1);
 
  TH1D * h_mass_plus = new TH1D("h_mass_plus", "h_mass_plus", 500, 1800, 1930);
  TH1D * h_mass_minus = new TH1D("h_mass_minus", "h_mass_minus", 500, 1800, 1930);

  TH2D * h_Kminus_pzpx_fidCut_plus = new TH2D("h_Kminus_pzpx_fidCut_plus","h_Kminus_pzpx_fidCut_plus",1000,0,250,1000,-10,10);
  TH2D * h_Kminus_pzpx_fidCut_minus = new TH2D("h_Kminus_pzpx_fidCut_minus","h_Kminus_pzpx_fidCut_minus",1000,0,250,1000,-10,10);
  TH2D * h_Kminus_pzpx_nofidCut_plus = new TH2D("h_Kminus_pzpx_nofidCut_plus","h_Kminus_pzpx_nofidCut_plus",1000,0,250,1000,-10,10);
  TH2D * h_Kminus_pzpx_nofidCut_minus = new TH2D("h_Kminus_pzpx_nofidCut_minus","h_Kminus_pzpx_nofidCut_minus",1000,0,250,1000,-10,10);
  TH2D * h_PID_Kminus_pzpx_fidCut_plus = new TH2D("h_PID_Kminus_pzpx_fidCut_plus","h_PID_Kminus_pzpx_fidCut_plus",1000,0,250,1000,-10,10);
  TH2D * h_PID_Kminus_pzpx_fidCut_minus = new TH2D("h_PID_Kminus_pzpx_fidCut_minus","h_PID_Kminus_pzpx_fidCut_minus",1000,0,250,1000,-10,10);
 
  TH2D * h_hplus_pzpx_fidCut_plus = new TH2D("h_hplus_pzpx_fidCut_plus","h_hplus_pzpx_fidCut_plus",1000,0,250,1000,-10,10);
  TH2D * h_hplus_pzpx_fidCut_minus = new TH2D("h_hplus_pzpx_fidCut_minus","h_hplus_pzpx_fidCut_minus",1000,0,250,1000,-10,10);
  TH2D * h_hplus_pzpx_nofidCut_plus = new TH2D("h_hplus_pzpx_nofidCut_plus","h_hplus_pzpx_nofidCut_plus",1000,0,250,1000,-10,10);
  TH2D * h_hplus_pzpx_nofidCut_minus = new TH2D("h_hplus_pzpx_nofidCut_minus","h_hplus_pzpx_nofidCut_minus",1000,0,250,1000,-10,10);
  TH2D * h_PID_hplus_pzpx_fidCut_plus = new TH2D("h_PID_hplus_pzpx_fidCut_plus","h_PID_hplus_pzpx_fidCut_plus",1000,0,250,1000,-10,10);
  TH2D * h_PID_hplus_pzpx_fidCut_minus = new TH2D("h_PID_hplus_pzpx_fidCut_minus","h_PID_hplus_pzpx_fidCut_minus",1000,0,250,1000,-10,10);
  
  TH2D * h_piplus_pzpx_fidCut_plus = new TH2D("h_piplus_pzpx_fidCut_plus","h_piplus_pzpx_fidCut_plus",1000,0,250,1000,-10,10);
  TH2D * h_piplus_pzpx_fidCut_minus = new TH2D("h_piplus_pzpx_fidCut_minus","h_piplus_pzpx_fidCut_minus",1000,0,250,1000,-10,10);
  TH2D * h_piplus_pzpx_nofidCut_plus = new TH2D("h_piplus_pzpx_nofidCut_plus","h_piplus_pzpx_nofidCut_plus",1000,0,250,1000,-10,10);
  TH2D * h_piplus_pzpx_nofidCut_minus = new TH2D("h_piplus_pzpx_nofidCut_minus","h_piplus_pzpx_nofidCut_minus",1000,0,250,1000,-10,10);
  TH2D * h_PID_piplus_pzpx_fidCut_plus = new TH2D("h_PID_piplus_pzpx_fidCut_plus","h_PID_piplus_pzpx_fidCut_plus",1000,0,250,1000,-10,10);
  TH2D * h_PID_piplus_pzpx_fidCut_minus = new TH2D("h_PID_piplus_pzpx_fidCut_minus","h_PID_piplus_pzpx_fidCut_minus",1000,0,250,1000,-10,10);
 
  
  h_mass_plus->Sumw2(); h_mass_minus->Sumw2();
  
  int nEntries = ntp->GetEntries();
  
  bool PIDCut      = true;
  bool KminusFiducialCut = true;
  bool piplusFiducialCut = true;
  bool hplusFiducialCut = true;
  bool TriggerCut     = true;
  bool HarmonizationCut = true;
  bool FitRange = true;
 
  //Booleans
  ntpOut->Branch("HarmonizationCut",&HarmonizationCut);

  int decision = 0;
  double r = 0;
  std::pair <double,double> pD;
  std::pair <int,int> pI;

  for(int i = 0; i < nEntries; i++) {
    ntp->GetEntry(i);
    
    if(isPGun)
      TriggerCut = (piplus_Hlt1TrackMVAResponse>0||hplus_Hlt1TrackMVAResponse>0);
    else 
      TriggerCut = Dplus_L0Global_TIS&&(piplus1_Hlt1TrackMVADecision_TOS||piplus2_Hlt1TrackMVADecision_TOS)/*hplus_Hlt1TrackMVADecision_TOS*/; //Baseline
    //TriggerCut = Dplus_L0Global_TIS&&(piplus1_Hlt1TrackMVADecision_TOS||piplus2_Hlt1TrackMVADecision_TOS)&&Kminus_Hlt1TrackMVADecision_TOS; // Run1
    // TriggerCut = (Dplus_L0Global_TIS || piplus1_L0HadronDecision_TOS || piplus2_L0HadronDecision_TOS)&&(piplus1_Hlt1TrackMVADecision_TOS||piplus2_Hlt1TrackMVADecision_TOS)/*hplus_Hlt1TrackMVADecision_TOS*/;
    
    if(!use_pGun_all)
      if(!(TriggerCut)) continue;//other requirements later...
    
    
    if(doSelection)
      FitRange = Dplus_M>1800&&Dplus_M<1930;
    if(!use_pGun_all)
      if(!(FitRange)) continue;
    
    //isPrompt definition with TIP
    v_Dplus_OWNPV.SetXYZ(Dplus_OWNPV_X, Dplus_OWNPV_Y, Dplus_OWNPV_Z);      
    v_Dplus_ENDVERTEX.SetXYZ(Dplus_ENDVERTEX_X, Dplus_ENDVERTEX_Y, Dplus_ENDVERTEX_Z);        
    p_Dplus.SetXYZ(Dplus_PX, Dplus_PY, Dplus_PZ);      
    p_beam.SetXYZ(0., 0., 1.);       
    vecProduct = p_beam.Cross(p_Dplus);  
    o_Dplus_TIP_OWNPV = vecProduct.Dot(v_Dplus_ENDVERTEX - v_Dplus_OWNPV) / vecProduct.Mag();
    //o_isPrompt = Dplus_IPCHI2_OWNPV<9;
    o_isPrompt = abs(o_Dplus_TIP_OWNPV)<0.040;
    h_TIP->Fill(o_Dplus_TIP_OWNPV);
    if(doSelection)
      if(!use_pGun_all)
	if(!o_isPrompt) continue;

    //TruthMatching
    if(isPGun) {
      //Exclude category 60 --- the consequence is that you remove some badly reconstructed signal which probably has a worse mass resolution, so you will get an overoptimistic lineshape.
      //Include category 60 --- the consequence is that you have to model the real ghosts which will form a background to your signal events.
      //Exclude category 40 -- the consequence is that you remove the partially reconstructed tracks
      if (Dplus_BKGCAT != 0) continue;
      if (abs(Dplus_TRUEID)!=PdgCode::D) continue;
      if (abs(hplus_TRUEID)!=PdgCode::Pi) continue;
      if (abs(piplus_TRUEID)!=PdgCode::Pi) continue;
      if (abs(Kminus_TRUEID)!=PdgCode::K) continue;
      if (hplus_MC_MOTHER_ID!=Dplus_TRUEID) continue;
      if (Kminus_MC_MOTHER_ID!=Dplus_TRUEID) continue;
      if (piplus_MC_MOTHER_ID!=Dplus_TRUEID) continue;
      if (!(hplus_MC_MOTHER_KEY==Kminus_MC_MOTHER_KEY && hplus_MC_MOTHER_KEY==piplus_MC_MOTHER_KEY)) continue;
      if(!(hplus_TRUEISSTABLE && Kminus_TRUEISSTABLE && piplus_TRUEISSTABLE)) continue;
      //if(!(hplus_TRACK_CHI2NDOF < 3 && Kminus_TRACK_CHI2NDOF < 3 && piplus_TRACK_CHI2NDOF < 3)) continue;
      //if(!(Dplus_ENDVERTEX_CHI2/Dplus_ENDVERTEX_NDOF < 6)) continue;
      
    }
    
    o_Dplus_M = Dplus_M;
    o_Dplus_ID = Dplus_ID;
    o_Dplus_TAU = Dplus_TAU*1000; //in ps


    o_Dplus_P = Dplus_P;
    o_Dplus_PT = Dplus_PT;
    o_Dplus_ETA = Dplus_ETA;
    o_Dplus_PHI = Dplus_PHI;
    o_Dplus_PX = Dplus_PX;
    o_Dplus_PY = Dplus_PY;
    o_Dplus_PZ = Dplus_PZ;

    o_Kminus_P = Kminus_P;
    o_Kminus_PT = Kminus_PT;
    o_Kminus_ETA = Kminus_ETA;
    o_Kminus_PHI = Kminus_PHI;
    o_Kminus_PX = Kminus_PX;
    o_Kminus_PY = Kminus_PY;
    o_Kminus_PZ = Kminus_PZ;
    
    o_Kminus_PIDK = Kminus_PIDK;

    o_Dplus_IP_OWNPV = Dplus_IP_OWNPV;
    o_Kminus_IP_OWNPV = Kminus_IP_OWNPV;
    o_Dplus_IPCHI2_OWNPV = Dplus_IPCHI2_OWNPV;
    o_Kminus_IPCHI2_OWNPV = Kminus_IPCHI2_OWNPV;

    o_Dplus_FD_OWNPV = Dplus_FD_OWNPV;
    o_Dplus_FDCHI2_OWNPV = Dplus_FDCHI2_OWNPV;


    o_Dplus_L0Global_TIS = Dplus_L0Global_TIS;
    o_Dplus_L0Global_TOS = Dplus_L0Global_TOS;
    o_Dplus_L0HadronDecision_TIS = Dplus_L0HadronDecision_TIS;
    o_Dplus_L0HadronDecision_TOS = Dplus_L0HadronDecision_TOS;

    o_Dplus_Hlt1TrackMVADecision_TIS = Dplus_Hlt1TrackMVADecision_TIS;
    o_Dplus_Hlt1TrackMVADecision_TOS = Dplus_Hlt1TrackMVADecision_TOS;
    o_Dplus_Hlt1TwoTrackMVADecision_TIS = Dplus_Hlt1TwoTrackMVADecision_TIS;
    o_Dplus_Hlt1TwoTrackMVADecision_TOS = Dplus_Hlt1TwoTrackMVADecision_TOS;


    o_Dplus_SUMPTChildren = Dplus_SUMPTChildren;
                             
    o_PVX = PVX[0];
    o_PVY = PVY[0];
    o_PVZ = PVZ[0];

    o_nPVs = nPVs;

    o_Dplus_OWNPV_X = Dplus_OWNPV_X;
    o_Dplus_OWNPV_Y = Dplus_OWNPV_Y;
    o_Dplus_OWNPV_Z = Dplus_OWNPV_Z;

    o_Dplus_ENDVERTEX_X = Dplus_ENDVERTEX_X;
    o_Dplus_ENDVERTEX_Y = Dplus_ENDVERTEX_Y;
    o_Dplus_ENDVERTEX_Z = Dplus_ENDVERTEX_Z;
    o_Dplus_ENDVERTEX_CHI2 = Dplus_ENDVERTEX_CHI2;

    o_Dplus_DOCA_Kminus_piplus1 = Dplus_DOCA_Kminus_piplus1;
    o_Dplus_DOCA_Kminus_piplus2 = Dplus_DOCA_Kminus_piplus2;
    o_Dplus_DOCA_piplus1_piplus2 = Dplus_DOCA_piplus1_piplus2;
    o_Dplus_DOCACHI2_Kminus_piplus1 = Dplus_DOCACHI2_Kminus_piplus1;
    o_Dplus_DOCACHI2_Kminus_piplus2 = Dplus_DOCACHI2_Kminus_piplus2;
    o_Dplus_DOCACHI2_piplus1_piplus2 = Dplus_DOCACHI2_piplus1_piplus2;
    
    o_Kminus_TRACK_CHI2NDOF = Kminus_TRACK_CHI2NDOF;
    o_Kminus_TRACK_GhostProb = Kminus_TRACK_GhostProb;




    if(!isPGun) {
      if(piplus1_Hlt1TrackMVADecision_TOS&&!piplus2_Hlt1TrackMVADecision_TOS)
	{decision = 1; r = 0;}
      else if(!piplus1_Hlt1TrackMVADecision_TOS&&piplus2_Hlt1TrackMVADecision_TOS)
	{decision = -1; r = 0;}
      else if(piplus1_Hlt1TrackMVADecision_TOS&&piplus2_Hlt1TrackMVADecision_TOS)
	{decision = 0; r = rnd->Uniform();}
    }
    else {
      if(piplus_Hlt1TrackMVAResponse>0&&!hplus_Hlt1TrackMVAResponse>0)
	{decision = 1; r = 0;}
      else if(!piplus_Hlt1TrackMVAResponse>0&&hplus_Hlt1TrackMVAResponse>0)
	{decision = -1; r = 0;}
      else if(piplus_Hlt1TrackMVAResponse>0&&hplus_Hlt1TrackMVAResponse>0)
	{decision = 0; r = rnd->Uniform();}
    }
    
    o_decision = decision;
    
    pD = switcher(decision,piplus1_P,piplus2_P,r);
    o_hplus_P  = pD.first;
    o_piplus_P = pD.second;

    pD = switcher(decision,piplus1_PT,piplus2_PT,r);
    o_hplus_PT  = pD.first;
    o_piplus_PT = pD.second;
      
    pD = switcher(decision,piplus1_ETA,piplus2_ETA,r);
    o_hplus_ETA  = pD.first;
    o_piplus_ETA = pD.second;

    pD = switcher(decision,piplus1_PHI,piplus2_PHI,r);
    o_hplus_PHI  = pD.first;
    o_piplus_PHI = pD.second;

    pD = switcher(decision,piplus1_PX,piplus2_PX,r);
    o_hplus_PX  = pD.first;
    o_piplus_PX = pD.second;

    pD = switcher(decision,piplus1_PY,piplus2_PY,r);
    o_hplus_PY  = pD.first;
    o_piplus_PY = pD.second;

    pD = switcher(decision,piplus1_PZ,piplus2_PZ,r);
    o_hplus_PZ  = pD.first;
    o_piplus_PZ = pD.second;

    pD = switcher(decision,piplus1_PIDK,piplus2_PIDK,r);
    o_hplus_PIDK  = pD.first;
    o_piplus_PIDK = pD.second;

    pD = switcher(decision,piplus1_PIDp,piplus2_PIDp,r);
    o_hplus_PIDp  = pD.first;
    o_piplus_PIDp = pD.second;

    pD = switcher(decision,piplus1_IP_OWNPV,piplus2_IP_OWNPV,r);
    o_hplus_IP_OWNPV  = pD.first;
    o_piplus_IP_OWNPV = pD.second;

    pD = switcher(decision,piplus1_IPCHI2_OWNPV,piplus2_IPCHI2_OWNPV,r);
    o_hplus_IPCHI2_OWNPV  = pD.first;
    o_piplus_IPCHI2_OWNPV = pD.second;
    
    pD = switcher(decision,piplus1_TRACK_CHI2NDOF,piplus2_TRACK_CHI2NDOF,r);
    o_hplus_TRACK_CHI2NDOF  = pD.first;
    o_piplus_TRACK_CHI2NDOF = pD.second;

    pD = switcher(decision,piplus1_TRACK_GhostProb,piplus2_TRACK_GhostProb,r);
    o_hplus_TRACK_GhostProb  = pD.first;
    o_piplus_TRACK_GhostProb = pD.second;

    pD = switcher(decision,Dplus_DOCA_Kminus_piplus1,Dplus_DOCA_Kminus_piplus2,r);
    o_Dplus_DOCA_Kminus_hplus  = pD.first;
  
    pD = switcher(decision,Dplus_DOCACHI2_Kminus_piplus1,Dplus_DOCACHI2_Kminus_piplus2,r);
    o_Dplus_DOCACHI2_Kminus_hplus  = pD.first;
   
    pI = switcher(decision,piplus1_Hlt1TrackMVADecision_TIS,piplus2_Hlt1TrackMVADecision_TIS,r);
    o_hplus_Hlt1TrackMVADecision_TIS  = pI.first;
    o_piplus_Hlt1TrackMVADecision_TIS = pI.second;

    pI = switcher(decision,piplus1_Hlt1TrackMVADecision_TOS,piplus2_Hlt1TrackMVADecision_TOS,r);
    o_hplus_Hlt1TrackMVADecision_TOS  = pI.first;
    o_piplus_Hlt1TrackMVADecision_TOS = pI.second;

    pI = switcher(decision,piplus1_Hlt1TwoTrackMVADecision_TIS,piplus2_Hlt1TwoTrackMVADecision_TIS,r);
    o_hplus_Hlt1TwoTrackMVADecision_TIS  = pI.first;
    o_piplus_Hlt1TwoTrackMVADecision_TIS = pI.second;

    pI = switcher(decision,piplus1_Hlt1TwoTrackMVADecision_TOS,piplus2_Hlt1TwoTrackMVADecision_TOS,r);
    o_hplus_Hlt1TwoTrackMVADecision_TOS  = pI.first;
    o_piplus_Hlt1TwoTrackMVADecision_TOS = pI.second;

    pI = switcher(decision,piplus1_L0HadronDecision_TOS,piplus2_L0HadronDecision_TOS,r);
    o_hplus_L0HadronDecision_TOS  = pI.first;
    o_piplus_L0HadronDecision_TOS = pI.second;
  
  
    o_Kminus_Hlt1TrackMVADecision_TIS = Kminus_Hlt1TrackMVADecision_TIS;
    o_Kminus_Hlt1TwoTrackMVADecision_TIS = Kminus_Hlt1TwoTrackMVADecision_TIS;
    o_Kminus_Hlt1TrackMVADecision_TOS = Kminus_Hlt1TrackMVADecision_TOS;
    o_Kminus_Hlt1TwoTrackMVADecision_TOS = Kminus_Hlt1TwoTrackMVADecision_TOS;
    o_Kminus_L0HadronDecision_TOS = Kminus_L0HadronDecision_TOS;
      
  
    o_eventNumber = eventNumber;
    o_runNumber = runNumber;
    o_nCandidate = nCandidate;
    o_nVeloClusters = nVeloClusters;
    o_nOTClusters = nOTClusters;
    o_totCandidates = totCandidates;


    Kminus_E = sqrt(Kminus_PX*Kminus_PX+Kminus_PY*Kminus_PY+Kminus_PZ*Kminus_PZ+PdgMass::mKplus_PDG*PdgMass::mKplus_PDG);
    Kminus.SetPxPyPzE(Kminus_PX,Kminus_PY,Kminus_PZ,Kminus_E);
    piplus_E = sqrt(o_piplus_PX*o_piplus_PX+o_piplus_PY*o_piplus_PY+o_piplus_PZ*o_piplus_PZ+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG);
    piplus.SetPxPyPzE(o_piplus_PX,o_piplus_PY,o_piplus_PZ,piplus_E);
    Kmpip = Kminus + piplus;
    
    o_Kmpip_PT = Kmpip.Pt();
    o_Kmpip_ETA = Kmpip.Eta();
    o_Kmpip_PHI = Kmpip.Phi();
    o_Kmpip_P = Kmpip.P();
    o_Kmpip_M = Kmpip.M();

    Kminus_E = sqrt(Kminus_PX*Kminus_PX+Kminus_PY*Kminus_PY+Kminus_PZ*Kminus_PZ+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG);
    Kminus.SetPxPyPzE(Kminus_PX,Kminus_PY,Kminus_PZ,Kminus_E);
    Kmpip = Kminus + piplus;
    o_Kmpip_pipi_M = Kmpip.M();
  
    //cuts on daughters are made after the identities have been assigned
    h_X_P->Fill(o_Kmpip_P/1e3);
    h_X_PT->Fill(o_Kmpip_PT);
    h_X_ETA->Fill(o_Kmpip_ETA);
    h_Dphp_PTPT->Fill(Dplus_PT,o_hplus_PT);
    h_Dphp_ETAETA->Fill(Dplus_ETA,o_hplus_ETA);
    h_Dphp_PHIPHI->Fill(Dplus_PHI,o_hplus_PHI);
    h_Kmpip_PTPT->Fill(Kminus_PT,o_piplus_PT);
    h_Kmpip_ETAETA->Fill(Kminus_ETA,o_piplus_ETA);
    h_Kmpip_PHIPHI->Fill(Kminus_PHI,o_piplus_PHI);
    
    h_X_diffPTs->Fill(Dplus_PT - o_hplus_PT);
    h_X_sumPTs->Fill(o_piplus_PT + Kminus_PT);  
    h_ETAETA_diff->Fill(TMath::Abs(Dplus_ETA-o_hplus_ETA));
    h_PHIPHI_diff->Fill(TMath::Abs(Dplus_PHI-o_hplus_PHI));
    if(doSelection) {
      HarmonizationCut = Kminus_P>5e3 && Kminus_PT>800 && o_piplus_P>5e3 && o_piplus_PT>800 && o_Kmpip_P > 30e3 && Dplus_PT - o_hplus_PT > 2280 && o_piplus_PT + Kminus_PT > 2280 
    	&& TMath::Abs(Dplus_PHI-o_hplus_PHI) < 0.4 && TMath::Abs(Dplus_ETA-o_hplus_ETA) < 0.4 
											  //&& Dplus_PT>4e3 && Dplus_PT<14e3 && Dplus_ETA>2.2 && Dplus_ETA<4.3									   && o_hplus_PT>1.5e3 && o_hplus_PT<8.5e3 && o_hplus_ETA>2.2 && o_hplus_ETA<4.3
											  && Dplus_PT>4.2e3 && Dplus_PT<11e3 && Dplus_ETA>2.3 && Dplus_ETA<4.2									   && o_hplus_PT>1.6e3 && o_hplus_PT<6e3 && o_hplus_ETA>2.2 && o_hplus_ETA<4.2
&& o_piplus_PT < 5.5e3 && Kminus_PT < 6e3 && o_piplus_ETA > 2.2 && o_piplus_ETA < 4.3 && Kminus_ETA > 2.2 && Kminus_ETA < 4.3;   
      //if(!HarmonizationCut) continue;
      //if (!(o_Kmpip_P > 30e3)) continue;
      if (!(Kminus_P>5e3 && Kminus_PT>800 && o_piplus_P>5e3 && o_piplus_PT>800 && Kminus_PT<8e3 && o_piplus_PT<8e3)) continue;
    }
    /*
      if(!o_Dplus_L0Global_TIS)//aka (only o_hplus_L0HadronDecision_TOS fired)
      TriggerCut = (o_piplus_L0HadronDecision_TOS || o_Kminus_L0HadronDecision_TOS);
      if(!(TriggerCut)) continue;
      if(isPGun)
      TriggerCut = (piplus_Hlt1TrackMVAResponse>0||Kminus_Hlt1TrackMVAResponse>0);
      else 
      TriggerCut = (o_piplus_Hlt1TrackMVADecision_TOS || o_Kminus_Hlt1TrackMVADecision_TOS);
      if(!(TriggerCut)) continue;
    */
    
    
    

    if(Dplus_ID>0) {
      h_Kminus_pzpx_nofidCut_plus->Fill(1e-3*o_Kminus_PZ,1e-3*o_Kminus_PX);
      h_hplus_pzpx_nofidCut_plus->Fill(1e-3*o_hplus_PZ,1e-3*o_hplus_PX);
      h_piplus_pzpx_nofidCut_plus->Fill(1e-3*o_piplus_PZ,1e-3*o_piplus_PX);
    }
    else {
      h_Kminus_pzpx_nofidCut_minus->Fill(1e-3*o_Kminus_PZ,1e-3*o_Kminus_PX);
      h_hplus_pzpx_nofidCut_minus->Fill(1e-3*o_hplus_PZ,1e-3*o_hplus_PX);
      h_piplus_pzpx_nofidCut_minus->Fill(1e-3*o_piplus_PZ,1e-3*o_piplus_PX);
    }


    // PX, PZ fiducial cuts
    // P1(2500,0)      P2(35000,10000)                     
    // P1(2500,0)      P2(35000,-10000)
    // P1(750,0)    P2(4500,300e3)
    // P1(-750,0)   P2(-4500,300e3)
    // P1(-750, 0)     P2(3000,300e3)
    // P1(750, 0)      P2(-3000,300e3)
    
    piplusFiducialCut = (abs(o_piplus_PY/o_piplus_PZ)>0.02 && abs(o_piplus_PX)<(o_piplus_PZ-2500)/3.25) || 
      (abs(o_piplus_PY/o_piplus_PZ)<0.02 && abs(o_piplus_PX)<(o_piplus_PZ-2500)/3.2 && 
       !((3*(abs(o_piplus_PX)-750)/0.045<o_piplus_PZ)&&3*(abs(o_piplus_PX)+750)/0.0375>o_piplus_PZ));

    KminusFiducialCut = (abs(o_Kminus_PY/o_Kminus_PZ)>0.02 && abs(o_Kminus_PX)<(o_Kminus_PZ-2500)/3.25) || 
				    (abs(o_Kminus_PY/o_Kminus_PZ)<0.02 && abs(o_Kminus_PX)<(o_Kminus_PZ-2500)/3.2 && 
				     !((3*(abs(o_Kminus_PX)-750)/0.045<o_Kminus_PZ)&&3*(abs(o_Kminus_PX)+750)/0.0375>o_Kminus_PZ));
    
    hplusFiducialCut = (abs(o_hplus_PY/o_hplus_PZ)>0.02 && abs(o_hplus_PX)<(o_hplus_PZ-2500)/3.25) ||                                                                        (abs(o_hplus_PY/o_hplus_PZ)<0.02 && abs(o_hplus_PX)<(o_hplus_PZ-2500)/3.2) ;
    
    if(doFiducial) {
      if(!use_pGun_all) {
	if(!piplusFiducialCut) continue;
	if(!KminusFiducialCut) continue;
	if(!hplusFiducialCut) continue;
	
	if(Dplus_ID>0) {
	  h_Kminus_pzpx_fidCut_plus->Fill(1e-3*o_Kminus_PZ,1e-3*o_Kminus_PX);
	  h_hplus_pzpx_fidCut_plus->Fill(1e-3*o_hplus_PZ,1e-3*o_hplus_PX);
	  h_piplus_pzpx_fidCut_plus->Fill(1e-3*o_piplus_PZ,1e-3*o_piplus_PX);
	}
	else {
	  h_Kminus_pzpx_fidCut_minus->Fill(1e-3*o_Kminus_PZ,1e-3*o_Kminus_PX);
	  h_hplus_pzpx_fidCut_minus->Fill(1e-3*o_hplus_PZ,1e-3*o_hplus_PX);
	  h_piplus_pzpx_fidCut_minus->Fill(1e-3*o_piplus_PZ,1e-3*o_piplus_PX);
	}
      }
    }
    
 
    ///necessary to do it here, after we identified which pion is piplus and which hplus
    if(!use_pGun_all)
      if(doSelection)
	PIDCut = o_piplus_PIDK<-5&&Kminus_PIDK>5;
    if(!PIDCut) continue;

    ////remember to put it also on the real ntuple
    
    if(Dplus_ID>0) {
      h_mass_plus->Fill(Dplus_M);
      h_PID_Kminus_pzpx_fidCut_plus->Fill(1e-3*o_Kminus_PZ,1e-3*o_Kminus_PX);
      h_PID_hplus_pzpx_fidCut_plus->Fill(1e-3*o_hplus_PZ,1e-3*o_hplus_PX);
      h_PID_piplus_pzpx_fidCut_plus->Fill(1e-3*o_piplus_PZ,1e-3*o_piplus_PX);
    }
    else {
      h_mass_minus->Fill(Dplus_M);     
      h_PID_Kminus_pzpx_fidCut_minus->Fill(1e-3*o_Kminus_PZ,1e-3*o_Kminus_PX);
      h_PID_hplus_pzpx_fidCut_minus->Fill(1e-3*o_hplus_PZ,1e-3*o_hplus_PX);
      h_PID_piplus_pzpx_fidCut_minus->Fill(1e-3*o_piplus_PZ,1e-3*o_piplus_PX);
    }

    
    ntpOut->Fill();
    
  }

  h_TIP->Write();
  h_X_P->Write();
  h_X_PT->Write();
  h_X_ETA->Write();
  h_Dphp_PTPT->Write();
  h_Dphp_ETAETA->Write();
  h_Dphp_PHIPHI->Write();
  h_Kmpip_PTPT->Write();
  h_Kmpip_ETAETA->Write();
  h_Kmpip_PHIPHI->Write();
 
  h_X_sumPTs->Write();
  h_X_diffPTs->Write();
  h_ETAETA_diff->Write();
  h_PHIPHI_diff->Write();
 
  h_Kminus_pzpx_nofidCut_plus->Write();
  h_Kminus_pzpx_fidCut_plus->Write();
  h_PID_Kminus_pzpx_fidCut_plus->Write();
  h_piplus_pzpx_nofidCut_plus->Write();
  h_piplus_pzpx_fidCut_plus->Write();
  h_PID_piplus_pzpx_fidCut_plus->Write();
  h_hplus_pzpx_nofidCut_plus->Write();
  h_hplus_pzpx_fidCut_plus->Write();
  h_PID_hplus_pzpx_fidCut_plus->Write();

  h_Kminus_pzpx_nofidCut_minus->Write();
  h_Kminus_pzpx_fidCut_minus->Write();
  h_PID_Kminus_pzpx_fidCut_minus->Write();
  h_piplus_pzpx_nofidCut_minus->Write();
  h_piplus_pzpx_fidCut_minus->Write();
  h_PID_piplus_pzpx_fidCut_minus->Write();
  h_hplus_pzpx_nofidCut_minus->Write();
  h_hplus_pzpx_fidCut_minus->Write();
  h_PID_hplus_pzpx_fidCut_minus->Write();

  h_mass_plus->Write();
  h_mass_minus->Write();
  ntpOut->Write();
  fOut->Close();

  if(doMultCandRemoval) {                                                                                                                
    TFile * inputFile = TFile::Open(Form(outputFile));                                                                                   
    TTree * inputTree = (TTree*)inputFile->Get("ntp");                                                                                   
                                                                                                                                         
    ntp->Print();                                                                                                                        
                                                                                                                                         
    UInt_t prev_runNumber = 0, mcrunNumber = 0;                                                                                          
    ULong64_t prev_eventNumber = 0, mceventNumber = 0;                                                                                   
    ULong64_t mctotCandidates = 0;                                                                                                       
    int mcpdg; double mcDTF_Mass;                                                                                                        
                                                                                                                                         
    inputTree->SetBranchAddress( "runNumber", &mcrunNumber );                                                                            
    inputTree->SetBranchAddress( "eventNumber", &mceventNumber );                                                                        
    inputTree->SetBranchAddress( "totCandidates", &mctotCandidates );                                                                    
    inputTree->SetBranchAddress( "Dplus_ID", &mcpdg );                                                                                   
    inputTree->SetBranchAddress( "Dplus_M", &mcDTF_Mass );                                                                               
                                                                                                                                         
    outputFile.ReplaceAll("_noMultCandRemoval.root","_withMultCandRemoval.root");                                                        
    TFile *outmc = new TFile(outputFile, "RECREATE");                                                                                    
                                                                                                                                         
    TTree *outputTree = inputTree->CloneTree(0);                                                                                         
                                                                                                                                         
    TH1F * h_dm_plus_tmp = new TH1F("h_dm_plus_tmp","h_dm_plus_tmp",500,1800,1930);                                                      
    TH1F * h_dm_minus_tmp = new TH1F("h_dm_minus_tmp","h_dm_minus_tmp",500,1800,1930);                                                   
                                                                                                                                         
    cout << "Before selecting: " << nEntries << endl;                                              
                                                                                                                              
    cout << "After selecting: " << inputTree->GetEntries() << endl;                                                                                        
    std::cout << " --- If there are multiple candidates, I will choose one randomly...\n";                                               
    bool switched_event = false;                                                                                                         
    std::vector<Long64_t> indices = {};                                                                                                  
    TRandom3 random_mult;                                                                                                                
    double nMC = 0;                                                                                                                      
    int nEntries_survived = inputTree->GetEntries();                 

    for ( Long64_t iEntry = 0; iEntry < nEntries_survived; iEntry++ ) {                                                                  
      if ( iEntry%50000 == 0 )                                                                                                           
	std::cout << "  processing Event " << iEntry << " / " << nEntries_survived << "\n";                                              
      inputTree->GetEntry( iEntry );               

      if ( iEntry != 0) {                                                                                                                
        if ( ( prev_runNumber != mcrunNumber ) || ( prev_eventNumber != mceventNumber) )                                                 
          switched_event = true;                                                                                                         
        else                                                                                                                             
          switched_event = false;                                                                                                        
        if ( switched_event ) {                                                                                                          
          if ( indices.size() > 1 ) {                                                                                                    
            random_mult.SetSeed(mcrunNumber*mceventNumber);                                                                              
            UInt_t rand_index = random_mult.Integer( indices.size() );                                                                   
            Long64_t rand_entry = indices.at( rand_index );                                                                              
            inputTree->GetEntry( rand_entry );                                                                                           
          }                                                                                                                              
          else                                                                                                                           
            inputTree->GetEntry( iEntry - 1 );                                                                                           
                                                                                                                                         
          indices.clear();                           
	  
	  //fill                                                                                                                         
          if(mcpdg>0) h_dm_plus_tmp->Fill(mcDTF_Mass);                                                                                   
          if(mcpdg<0) h_dm_minus_tmp->Fill(mcDTF_Mass);                                                                                  
                                                                                                                                         
                                                                                                                                         
          outputTree->Fill();                                                                                                            
        }                                                                                                                                
                                                                                                                                         
                                                                                                                                         
      }                                         

      // check entry to be filled (or not) at the next iteration                                                                         
      inputTree->GetEntry( iEntry );                                                                                                     
      if ( mctotCandidates == 1 )                                                                                                        
        indices.clear();                                                                                                                 
      else {                                                                                                                             
        indices.push_back( iEntry );                                                                                                     
      }                                                                                                                                  
      prev_runNumber = mcrunNumber;                                                                                                      
      prev_eventNumber = mceventNumber;                                                                                                  
    }                                                                                                                                    
                                                                                                                                         
    // check on very last entry                                                                                                          
    if ( indices.size() > 1 ) {                                                                                                          
      UInt_t rand_index = random_mult.Integer( indices.size() );                                                                         
      Long64_t rand_entry = indices.at( rand_index );                                                                                    
      inputTree->GetEntry( rand_entry );                                                                                                 
      nMC++;                                                                                                                             
    }                                                                                                                                    
    else                                                                                                                                 
      inputTree->GetEntry( nEntries_survived );                                                                                          
    indices.clear();                                                                                                                     
    outputTree->Fill();                                                                                                                  
                                                                                                                                         
    outmc->WriteTObject( outputTree, "ntp", "WriteDelete" );                                                                             
                                                                                                                                         
    std::cout << " ===> Random candidates have been picked up!\n";                                                                       
    cout << "After removing multiple candidates: " << outputTree->GetEntries() << endl;                                              
    std::cout << " >>>> Percentual of multiple candidates: " << 1.0-(double)outputTree->GetEntries()/inputTree->GetEntries() << " %" << endl;                                                                                                                                  
                                                                                                                                         
    h_dm_plus_tmp->SetName("h_mass_plus");   h_dm_plus_tmp->SetTitle("h_mass_plus");                
    h_dm_minus_tmp->SetName("h_mass_minus");   h_dm_minus_tmp->SetTitle("h_mass_minus");      
    h_dm_plus_tmp->Write();                                                                                                              
    h_dm_minus_tmp->Write();                                                                                                             
    outmc->Close();                                                                                                                      
                                                                                                                                         
    outputFile.ReplaceAll("_withMultCandRemoval.root","_noMultCandRemoval.root");                                                        
    //system("rm "+outputFile);                                                     
  }

    
  
}
