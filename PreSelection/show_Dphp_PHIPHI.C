#include "/home/LHCB-T3/smaccoli/dcastyle.C"
#include "/home/LHCB-T3/smaccoli/Tools.h"

void show_Dphp_PHIPHI(TString year = "18", TString pol = "Dw") {
  
  dcastyle();

  TString path = "/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/";
  TCanvas* c = new TCanvas("Dphp_PHIPHI","Dphp_PHIPHI",50,50, 1000,700);
  c->SetLeftMargin(0.15);
  c->SetRightMargin(0.18);

  TLegend* leg = new TLegend(0.7,0.70,0.90,0.90);
  leg->SetFillColor(kWhite);
  leg->SetTextSize(0.05);
  leg->SetBorderSize(0);
  leg->SetTextFont(132);

  
  TF1 *l_up = new TF1("l_up","x - 0.4",-15e3,15e3);
  l_up->SetLineColor(kRed); 
  TF1 *l_dw = new TF1("l_up","x + 0.4",-15e3,15e3);
  l_dw->SetLineColor(kRed); 
  
  
  TH2D *h_0, *h_1;

  
  TString decay_0 = "Dp2KS0pipLL";
  TFile *f_0 = new TFile(path+decay_0+"_"+year+"_"+pol+".root");
  h_0 = (TH2D*)f_0->Get("h_Dphp_PHIPHI");
  /*
    h_0->GetXaxis()->SetRangeUser(800,6e3);
    h_0->GetYaxis()->SetRangeUser(800,5.5e3);
  */
  h_0->Scale(1./h_0->Integral());
  h_0->SetXTitle(findAxisTitle("Dplus_PHI"));
  h_0->SetYTitle(findAxisTitle("hplus_PHI"));
  h_0->SetZTitle(Form("Normalized events / %.0f (MeV/c)^{2}",h_0->GetXaxis()->GetBinWidth(1)*h_0->GetYaxis()->GetBinWidth(1)));
  h_0->SetTitleOffset(1,"z");
  h_0->SetTitleOffset(1.2,"xy");
  h_0->SetMarkerSize(1.5);
  h_0->Draw("colz");
  l_up->Draw("lsame");
  l_dw->Draw("lsame");
  c->SaveAs("plots/"+decay_0+"_Dphp_PHIPHI.C");
  c->SaveAs("plots/"+decay_0+"_Dphp_PHIPHI.pdf");
  cout << h_0->GetEntries() << endl;
  
  TString decay_1 = "Dp2Kmpippip";
  TFile *f_1 = new TFile(path+decay_1+"_"+year+"_"+pol+".root");
  h_1 = (TH2D*)f_1->Get("h_Dphp_PHIPHI");
  /*
    h_1->GetXaxis()->SetRangeUser(800,6e3);
    h_1->GetYaxis()->SetRangeUser(800,5.5e3);
  */
  h_1->Scale(1./h_1->Integral());
  h_1->SetXTitle(findAxisTitle("Dplus_PHI"));
  h_1->SetYTitle(findAxisTitle("hplus_PHI"));
  h_1->SetZTitle(Form("Normalized events / %.0f (MeV/c)^{2}",h_1->GetXaxis()->GetBinWidth(1)*h_1->GetYaxis()->GetBinWidth(1)));
  h_1->SetTitleOffset(1,"z");
  h_1->SetTitleOffset(1.2,"xy");
  h_1->SetMarkerSize(1.5);
  //  h_1->Draw("psame");
  h_1->Draw("colz");
  l_up->Draw("lsame");
  l_dw->Draw("lsame");
  c->SaveAs("plots/"+decay_1+"_Dphp_PHIPHI.C");
  c->SaveAs("plots/"+decay_1+"_Dphp_PHIPHI.pdf");
 

  h_0->Divide(h_1);
  h_0->GetZaxis()->SetRangeUser(0,20);
  h_0->Draw("colz");
  l_up->Draw("lsame");
  l_dw->Draw("lsame");
  c->SaveAs("plots/"+decay_0+"_Dphp_PHIPHI_w.C");
  c->SaveAs("plots/"+decay_0+"_Dphp_PHIPHI_w.pdf");





}
