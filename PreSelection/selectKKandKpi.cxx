#include <iostream>   
#include <stdio.h>         
#include <stdlib.h>         
#include <TString.h>      
#include <TFile.h>      
#include <TChain.h>        
#include <TTree.h>      
#include <TVector3.h>    
#include <TFileCollection.h>          
#include <TMath.h>         
#include <TH1D.h>        
#include <TH2D.h>          
#include <TRandom3.h>         
#include <TColor.h>
#include "/home/LHCB/smaccoli/Tools.h"

using namespace std;


int main(int argc, char * argv[]) { 

  TString inputFile    = argv[1];
  TString inputNtuple  = argv[2];
  TString outputFile   = argv[3];
  TString outputNtuple = argv[4];
  int doSelection      = atoi(argv[5]); 
  int doFiducial        = atoi(argv[6]); 
  int doMultCandRemoval = atoi(argv[7]);

  TString year;
  if     (outputFile.Contains("2015")) year = "2015";
  if     (outputFile.Contains("2016")) year = "2016";
  if     (outputFile.Contains("2017")) year = "2017";
  if     (outputFile.Contains("2018")) year = "2018";

  if(doSelection) { 
    printf("DOING SELECTION CUTS: %d\n", doSelection);
    //outputFile.ReplaceAll(".root","_withSelection.root");  
  }
  if(doFiducial) {  
    printf("DOING FIDUCIAL CUTS: %d\n",  doFiducial);
    //outputFile.ReplaceAll(".root","_withFiducialCuts.root");
  }
  if(doMultCandRemoval)  { 
    printf("DOING MULTIPLE CANDIDATES REMOVAL: %d\n", doMultCandRemoval); 
    outputFile.ReplaceAll(".root","_noMultCandRemoval.root"); 
  }

    

  TFile * f = TFile::Open(inputFile); 
  if(!f) cout << "FILE " << inputFile << " DOES NOT EXIST!!" << endl;  
  TTree * ntp = (TTree*)f->Get(inputNtuple); 
  if(!ntp) cout << "NTUPLE " << inputNtuple << " DOES NOT EXIST!!" << endl; 
  ntp->SetBranchStatus("*",0);
  cout << "Before selection: " << ntp->GetEntries() << endl;        


  /// masses
  double D0_M; ntp->SetBranchStatus("D0_M", 1); ntp->SetBranchAddress("D0_M", &D0_M);
  double D0_DTF_M; ntp->SetBranchStatus("Dst_DTF_D0_M", 1); ntp->SetBranchAddress("Dst_DTF_D0_M", &D0_DTF_M); 
  
  /// lifetimes
  double D0_TAU; ntp->SetBranchStatus("D0_TAU", 1); ntp->SetBranchAddress("D0_TAU", &D0_TAU);
  double D0_DTF_CTAU; ntp->SetBranchStatus("Dst_DTF_D0_CTAU", 1); ntp->SetBranchAddress("Dst_DTF_D0_CTAU", &D0_DTF_CTAU);

  /// IDs
  int Dst_ID; ntp->SetBranchStatus("Dst_ID", 1); ntp->SetBranchAddress("Dst_ID", &Dst_ID);  
  int D0_ID; ntp->SetBranchStatus("D0_ID", 1); ntp->SetBranchAddress("D0_ID", &D0_ID);  
  int P1_ID; ntp->SetBranchStatus("P1_ID", 1); ntp->SetBranchAddress("P1_ID", &P1_ID);  
  int P2_ID; ntp->SetBranchStatus("P2_ID", 1); ntp->SetBranchAddress("P2_ID", &P2_ID);  

  /// kinematics
  
  double Dst_P; ntp->SetBranchStatus("Dst_P", 1); ntp->SetBranchAddress("Dst_P", &Dst_P);
  double Dst_PT; ntp->SetBranchStatus("Dst_PT", 1); ntp->SetBranchAddress("Dst_PT", &Dst_PT);
  double Dst_PX; ntp->SetBranchStatus("Dst_PX", 1); ntp->SetBranchAddress("Dst_PX", &Dst_PX);
  double Dst_PY; ntp->SetBranchStatus("Dst_PY", 1); ntp->SetBranchAddress("Dst_PY", &Dst_PY);
  double Dst_PZ; ntp->SetBranchStatus("Dst_PZ", 1); ntp->SetBranchAddress("Dst_PZ", &Dst_PZ);
  

  double D0_P; ntp->SetBranchStatus("D0_P", 1); ntp->SetBranchAddress("D0_P", &D0_P);
  double D0_PT; ntp->SetBranchStatus("D0_PT", 1); ntp->SetBranchAddress("D0_PT", &D0_PT);
  double D0_PX; ntp->SetBranchStatus("D0_PX", 1); ntp->SetBranchAddress("D0_PX", &D0_PX);
  double D0_PY; ntp->SetBranchStatus("D0_PY", 1); ntp->SetBranchAddress("D0_PY", &D0_PY);
  double D0_PZ; ntp->SetBranchStatus("D0_PZ", 1); ntp->SetBranchAddress("D0_PZ", &D0_PZ);

  double P1_P; ntp->SetBranchStatus("P1_P", 1); ntp->SetBranchAddress("P1_P", &P1_P);
  double P1_PT; ntp->SetBranchStatus("P1_PT", 1); ntp->SetBranchAddress("P1_PT", &P1_PT);
  double P1_PX; ntp->SetBranchStatus("P1_PX", 1); ntp->SetBranchAddress("P1_PX", &P1_PX);
  double P1_PY; ntp->SetBranchStatus("P1_PY", 1); ntp->SetBranchAddress("P1_PY", &P1_PY);
  double P1_PZ; ntp->SetBranchStatus("P1_PZ", 1); ntp->SetBranchAddress("P1_PZ", &P1_PZ);

  double P1_DTF_PX; ntp->SetBranchStatus("Dst_DTF_P1_PX", 1); ntp->SetBranchAddress("Dst_DTF_P1_PX", &P1_DTF_PX);
  double P1_DTF_PY; ntp->SetBranchStatus("Dst_DTF_P1_PY", 1); ntp->SetBranchAddress("Dst_DTF_P1_PY", &P1_DTF_PY);
  double P1_DTF_PZ; ntp->SetBranchStatus("Dst_DTF_P1_PZ", 1); ntp->SetBranchAddress("Dst_DTF_P1_PZ", &P1_DTF_PZ);

  double P2_P; ntp->SetBranchStatus("P2_P", 1); ntp->SetBranchAddress("P2_P", &P2_P);
  double P2_PT; ntp->SetBranchStatus("P2_PT", 1); ntp->SetBranchAddress("P2_PT", &P2_PT);
  double P2_PX; ntp->SetBranchStatus("P2_PX", 1); ntp->SetBranchAddress("P2_PX", &P2_PX);
  double P2_PY; ntp->SetBranchStatus("P2_PY", 1); ntp->SetBranchAddress("P2_PY", &P2_PY);
  double P2_PZ; ntp->SetBranchStatus("P2_PZ", 1); ntp->SetBranchAddress("P2_PZ", &P2_PZ);
  
  double P2_DTF_PX; ntp->SetBranchStatus("Dst_DTF_P2_PX", 1); ntp->SetBranchAddress("Dst_DTF_P2_PX", &P2_DTF_PX);
  double P2_DTF_PY; ntp->SetBranchStatus("Dst_DTF_P2_PY", 1); ntp->SetBranchAddress("Dst_DTF_P2_PY", &P2_DTF_PY);
  double P2_DTF_PZ; ntp->SetBranchStatus("Dst_DTF_P2_PZ", 1); ntp->SetBranchAddress("Dst_DTF_P2_PZ", &P2_DTF_PZ);

  double sPi_P; ntp->SetBranchStatus("sPi_P", 1); ntp->SetBranchAddress("sPi_P", &sPi_P);
  double sPi_PT; ntp->SetBranchStatus("sPi_PT", 1); ntp->SetBranchAddress("sPi_PT", &sPi_PT);
  double sPi_PX; ntp->SetBranchStatus("sPi_PX", 1); ntp->SetBranchAddress("sPi_PX", &sPi_PX);
  double sPi_PY; ntp->SetBranchStatus("sPi_PY", 1); ntp->SetBranchAddress("sPi_PY", &sPi_PY);
  double sPi_PZ; ntp->SetBranchStatus("sPi_PZ", 1); ntp->SetBranchAddress("sPi_PZ", &sPi_PZ);
  
  double sPi_DTF_PX; ntp->SetBranchStatus("Dst_DTF_sPi_PX", 1); ntp->SetBranchAddress("Dst_DTF_sPi_PX", &sPi_DTF_PX);
  double sPi_DTF_PY; ntp->SetBranchStatus("Dst_DTF_sPi_PY", 1); ntp->SetBranchAddress("Dst_DTF_sPi_PY", &sPi_DTF_PY);
  double sPi_DTF_PZ; ntp->SetBranchStatus("Dst_DTF_sPi_PZ", 1); ntp->SetBranchAddress("Dst_DTF_sPi_PZ", &sPi_DTF_PZ);
  
  ///PID
  double P1_PIDK; ntp->SetBranchStatus("P1_PIDK", 1); ntp->SetBranchAddress("P1_PIDK", &P1_PIDK);  
  double P1_PIDe; ntp->SetBranchStatus("P1_PIDe", 1); ntp->SetBranchAddress("P1_PIDe", &P1_PIDe);  
  double P1_PIDmu; ntp->SetBranchStatus("P1_PIDmu", 1); ntp->SetBranchAddress("P1_PIDmu", &P1_PIDmu);  
  double P1_PIDp; ntp->SetBranchStatus("P1_PIDp", 1); ntp->SetBranchAddress("P1_PIDp", &P1_PIDp);  

  double P2_PIDK; ntp->SetBranchStatus("P2_PIDK", 1); ntp->SetBranchAddress("P2_PIDK", &P2_PIDK);  
  double P2_PIDe; ntp->SetBranchStatus("P2_PIDe", 1); ntp->SetBranchAddress("P2_PIDe", &P2_PIDe);  
  double P2_PIDmu; ntp->SetBranchStatus("P2_PIDmu", 1); ntp->SetBranchAddress("P2_PIDmu", &P2_PIDmu);  
  double P2_PIDp; ntp->SetBranchStatus("P2_PIDp", 1); ntp->SetBranchAddress("P2_PIDp", &P2_PIDp);  

  double sPi_PIDK; ntp->SetBranchStatus("sPi_PIDK", 1); ntp->SetBranchAddress("sPi_PIDK", &sPi_PIDK);  
  double sPi_PIDe; ntp->SetBranchStatus("sPi_PIDe", 1); ntp->SetBranchAddress("sPi_PIDe", &sPi_PIDe);  
  double sPi_PIDmu; ntp->SetBranchStatus("sPi_PIDmu", 1); ntp->SetBranchAddress("sPi_PIDmu", &sPi_PIDmu);  
  double sPi_PIDp; ntp->SetBranchStatus("sPi_PIDp", 1); ntp->SetBranchAddress("sPi_PIDp", &sPi_PIDp);  

  ///geometric
  double Dst_FD_OWNPV; ntp->SetBranchStatus("Dst_FD_OWNPV", 1); ntp->SetBranchAddress("Dst_FD_OWNPV", &Dst_FD_OWNPV);  
  double Dst_FDCHI2_OWNPV; ntp->SetBranchStatus("Dst_FDCHI2_OWNPV", 1); ntp->SetBranchAddress("Dst_FDCHI2_OWNPV", &Dst_FDCHI2_OWNPV);  
  double Dst_IP_OWNPV; ntp->SetBranchStatus("Dst_IP_OWNPV", 1); ntp->SetBranchAddress("Dst_IP_OWNPV", &Dst_IP_OWNPV);  
  double Dst_IPCHI2_OWNPV; ntp->SetBranchStatus("Dst_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("Dst_IPCHI2_OWNPV", &Dst_IPCHI2_OWNPV);  

  double D0_FD_OWNPV; ntp->SetBranchStatus("D0_FD_OWNPV", 1); ntp->SetBranchAddress("D0_FD_OWNPV", &D0_FD_OWNPV);  
  double D0_FDCHI2_OWNPV; ntp->SetBranchStatus("D0_FDCHI2_OWNPV", 1); ntp->SetBranchAddress("D0_FDCHI2_OWNPV", &D0_FDCHI2_OWNPV);  
  double D0_IP_OWNPV; ntp->SetBranchStatus("D0_IP_OWNPV", 1); ntp->SetBranchAddress("D0_IP_OWNPV", &D0_IP_OWNPV);  
  double D0_IPCHI2_OWNPV; ntp->SetBranchStatus("D0_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("D0_IPCHI2_OWNPV", &D0_IPCHI2_OWNPV);  

  double P1_IP_OWNPV; ntp->SetBranchStatus("P1_IP_OWNPV", 1); ntp->SetBranchAddress("P1_IP_OWNPV", &P1_IP_OWNPV);  
  double P1_IPCHI2_OWNPV; ntp->SetBranchStatus("P1_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("P1_IPCHI2_OWNPV", &P1_IPCHI2_OWNPV);  

  double P2_IP_OWNPV; ntp->SetBranchStatus("P2_IP_OWNPV", 1); ntp->SetBranchAddress("P2_IP_OWNPV", &P2_IP_OWNPV);  
  double P2_IPCHI2_OWNPV; ntp->SetBranchStatus("P2_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("P2_IPCHI2_OWNPV", &P2_IPCHI2_OWNPV);  

  double sPi_IP_OWNPV; ntp->SetBranchStatus("sPi_IP_OWNPV", 1); ntp->SetBranchAddress("sPi_IP_OWNPV", &sPi_IP_OWNPV);  
  double sPi_IPCHI2_OWNPV; ntp->SetBranchStatus("sPi_IPCHI2_OWNPV", 1); ntp->SetBranchAddress("sPi_IPCHI2_OWNPV", &sPi_IPCHI2_OWNPV);  



  /// vertices
  double Dst_ENDVERTEX_CHI2; ntp->SetBranchStatus("Dst_ENDVERTEX_CHI2", 1); ntp->SetBranchAddress("Dst_ENDVERTEX_CHI2", &Dst_ENDVERTEX_CHI2);  
  int Dst_ENDVERTEX_NDOF; ntp->SetBranchStatus("Dst_ENDVERTEX_NDOF", 1); ntp->SetBranchAddress("Dst_ENDVERTEX_NDOF", &Dst_ENDVERTEX_NDOF);  
  double Dst_ENDVERTEX_X; ntp->SetBranchStatus("Dst_ENDVERTEX_X", 1); ntp->SetBranchAddress("Dst_ENDVERTEX_X", &Dst_ENDVERTEX_X);  
  double Dst_ENDVERTEX_Y; ntp->SetBranchStatus("Dst_ENDVERTEX_Y", 1); ntp->SetBranchAddress("Dst_ENDVERTEX_Y", &Dst_ENDVERTEX_Y);  
  double Dst_ENDVERTEX_Z; ntp->SetBranchStatus("Dst_ENDVERTEX_Z", 1); ntp->SetBranchAddress("Dst_ENDVERTEX_Z", &Dst_ENDVERTEX_Z);  
  double Dst_ENDVERTEX_XERR; ntp->SetBranchStatus("Dst_ENDVERTEX_XERR", 1); ntp->SetBranchAddress("Dst_ENDVERTEX_XERR", &Dst_ENDVERTEX_XERR);  
  double Dst_ENDVERTEX_YERR; ntp->SetBranchStatus("Dst_ENDVERTEX_YERR", 1); ntp->SetBranchAddress("Dst_ENDVERTEX_YERR", &Dst_ENDVERTEX_YERR);  
  double Dst_ENDVERTEX_ZERR; ntp->SetBranchStatus("Dst_ENDVERTEX_ZERR", 1); ntp->SetBranchAddress("Dst_ENDVERTEX_ZERR", &Dst_ENDVERTEX_ZERR);  

  double D0_ENDVERTEX_CHI2; ntp->SetBranchStatus("D0_ENDVERTEX_CHI2", 1); ntp->SetBranchAddress("D0_ENDVERTEX_CHI2", &D0_ENDVERTEX_CHI2);  
  int D0_ENDVERTEX_NDOF; ntp->SetBranchStatus("D0_ENDVERTEX_NDOF", 1); ntp->SetBranchAddress("D0_ENDVERTEX_NDOF", &D0_ENDVERTEX_NDOF);  
  double D0_ENDVERTEX_X; ntp->SetBranchStatus("D0_ENDVERTEX_X", 1); ntp->SetBranchAddress("D0_ENDVERTEX_X", &D0_ENDVERTEX_X);  
  double D0_ENDVERTEX_Y; ntp->SetBranchStatus("D0_ENDVERTEX_Y", 1); ntp->SetBranchAddress("D0_ENDVERTEX_Y", &D0_ENDVERTEX_Y);  
  double D0_ENDVERTEX_Z; ntp->SetBranchStatus("D0_ENDVERTEX_Z", 1); ntp->SetBranchAddress("D0_ENDVERTEX_Z", &D0_ENDVERTEX_Z);  
  double D0_ENDVERTEX_XERR; ntp->SetBranchStatus("D0_ENDVERTEX_XERR", 1); ntp->SetBranchAddress("D0_ENDVERTEX_XERR", &D0_ENDVERTEX_XERR);  
  double D0_ENDVERTEX_YERR; ntp->SetBranchStatus("D0_ENDVERTEX_YERR", 1); ntp->SetBranchAddress("D0_ENDVERTEX_YERR", &D0_ENDVERTEX_YERR);  
  double D0_ENDVERTEX_ZERR; ntp->SetBranchStatus("D0_ENDVERTEX_ZERR", 1); ntp->SetBranchAddress("D0_ENDVERTEX_ZERR", &D0_ENDVERTEX_ZERR);  

  double Dst_OWNPV_X; ntp->SetBranchStatus("Dst_OWNPV_X", 1); ntp->SetBranchAddress("Dst_OWNPV_X", &Dst_OWNPV_X);  
  double Dst_OWNPV_Y; ntp->SetBranchStatus("Dst_OWNPV_Y", 1); ntp->SetBranchAddress("Dst_OWNPV_Y", &Dst_OWNPV_Y);  
  double Dst_OWNPV_Z; ntp->SetBranchStatus("Dst_OWNPV_Z", 1); ntp->SetBranchAddress("Dst_OWNPV_Z", &Dst_OWNPV_Z);  

  double D0_OWNPV_X; ntp->SetBranchStatus("D0_OWNPV_X", 1); ntp->SetBranchAddress("D0_OWNPV_X", &D0_OWNPV_X);  
  double D0_OWNPV_Y; ntp->SetBranchStatus("D0_OWNPV_Y", 1); ntp->SetBranchAddress("D0_OWNPV_Y", &D0_OWNPV_Y);  
  double D0_OWNPV_Z; ntp->SetBranchStatus("D0_OWNPV_Z", 1); ntp->SetBranchAddress("D0_OWNPV_Z", &D0_OWNPV_Z);  

  double P1_OWNPV_X; ntp->SetBranchStatus("P1_OWNPV_X", 1); ntp->SetBranchAddress("P1_OWNPV_X", &P1_OWNPV_X);  
  double P1_OWNPV_Y; ntp->SetBranchStatus("P1_OWNPV_Y", 1); ntp->SetBranchAddress("P1_OWNPV_Y", &P1_OWNPV_Y);  
  double P1_OWNPV_Z; ntp->SetBranchStatus("P1_OWNPV_Z", 1); ntp->SetBranchAddress("P1_OWNPV_Z", &P1_OWNPV_Z);  

  double P2_OWNPV_X; ntp->SetBranchStatus("P2_OWNPV_X", 1); ntp->SetBranchAddress("P2_OWNPV_X", &P2_OWNPV_X);  
  double P2_OWNPV_Y; ntp->SetBranchStatus("P2_OWNPV_Y", 1); ntp->SetBranchAddress("P2_OWNPV_Y", &P2_OWNPV_Y);  
  double P2_OWNPV_Z; ntp->SetBranchStatus("P2_OWNPV_Z", 1); ntp->SetBranchAddress("P2_OWNPV_Z", &P2_OWNPV_Z);  

  double sPi_OWNPV_X; ntp->SetBranchStatus("sPi_OWNPV_X", 1); ntp->SetBranchAddress("sPi_OWNPV_X", &sPi_OWNPV_X);  
  double sPi_OWNPV_Y; ntp->SetBranchStatus("sPi_OWNPV_Y", 1); ntp->SetBranchAddress("sPi_OWNPV_Y", &sPi_OWNPV_Y);  
  double sPi_OWNPV_Z; ntp->SetBranchStatus("sPi_OWNPV_Z", 1); ntp->SetBranchAddress("sPi_OWNPV_Z", &sPi_OWNPV_Z);  

  float PVX[100]; ntp->SetBranchStatus("PVX", 1); ntp->SetBranchAddress("PVX", &PVX);  
  float PVY[100]; ntp->SetBranchStatus("PVY", 1); ntp->SetBranchAddress("PVY", &PVY);  
  float PVZ[100]; ntp->SetBranchStatus("PVZ", 1); ntp->SetBranchAddress("PVZ", &PVZ);  

  ///track quality
  double P1_TRACK_CHI2NDOF; ntp->SetBranchStatus("P1_TRACK_CHI2NDOF", 1); ntp->SetBranchAddress("P1_TRACK_CHI2NDOF", &P1_TRACK_CHI2NDOF); 
  double P2_TRACK_CHI2NDOF; ntp->SetBranchStatus("P2_TRACK_CHI2NDOF", 1); ntp->SetBranchAddress("P2_TRACK_CHI2NDOF", &P2_TRACK_CHI2NDOF); 

  double sPi_TRACK_MatchCHI2; ntp->SetBranchStatus("sPi_TRACK_MatchCHI2", 1); ntp->SetBranchAddress("sPi_TRACK_MatchCHI2", &sPi_TRACK_MatchCHI2); 
  double sPi_TRACK_GhostProb; ntp->SetBranchStatus("sPi_TRACK_GhostProb", 1); ntp->SetBranchAddress("sPi_TRACK_GhostProb", &sPi_TRACK_GhostProb); 
  double sPi_TRACK_FirstMeasurementX; ntp->SetBranchStatus("sPi_TRACK_FirstMeasurementX", 1); ntp->SetBranchAddress("sPi_TRACK_FirstMeasurementX", &sPi_TRACK_FirstMeasurementX); 
  double sPi_TRACK_FirstMeasurementY; ntp->SetBranchStatus("sPi_TRACK_FirstMeasurementY", 1); ntp->SetBranchAddress("sPi_TRACK_FirstMeasurementY", &sPi_TRACK_FirstMeasurementY); 
  double sPi_TRACK_FirstMeasurementZ; ntp->SetBranchStatus("sPi_TRACK_FirstMeasurementZ", 1); ntp->SetBranchAddress("sPi_TRACK_FirstMeasurementZ", &sPi_TRACK_FirstMeasurementZ); 
  double sPi_ProbNNghost; ntp->SetBranchStatus("sPi_ProbNNghost", 1); ntp->SetBranchAddress("sPi_ProbNNghost", &sPi_ProbNNghost); 

  double D0_DOCA; ntp->SetBranchStatus("D0_DOCA", 1); ntp->SetBranchAddress("D0_DOCA", &D0_DOCA); 
  double D0_DOCACHI2; ntp->SetBranchStatus("D0_DOCACHI2", 1); ntp->SetBranchAddress("D0_DOCACHI2", &D0_DOCACHI2); 

  /// triggers
  bool Dst_L0HadronDecision_TOS; ntp->SetBranchStatus("Dst_L0HadronDecision_TOS", 1); ntp->SetBranchAddress("Dst_L0HadronDecision_TOS", &Dst_L0HadronDecision_TOS);
  bool Dst_L0HadronDecision_TIS; ntp->SetBranchStatus("Dst_L0HadronDecision_TIS", 1); ntp->SetBranchAddress("Dst_L0HadronDecision_TIS", &Dst_L0HadronDecision_TIS);
  bool Dst_L0MuonDecision_TOS; ntp->SetBranchStatus("Dst_L0MuonDecision_TOS", 1); ntp->SetBranchAddress("Dst_L0MuonDecision_TOS", &Dst_L0MuonDecision_TOS);
  bool Dst_L0MuonDecision_TIS; ntp->SetBranchStatus("Dst_L0MuonDecision_TIS", 1); ntp->SetBranchAddress("Dst_L0MuonDecision_TIS", &Dst_L0MuonDecision_TIS);
  bool Dst_L0DiMuonDecision_TOS; ntp->SetBranchStatus("Dst_L0DiMuonDecision_TOS", 1); ntp->SetBranchAddress("Dst_L0DiMuonDecision_TOS", &Dst_L0DiMuonDecision_TOS);
  bool Dst_L0DiMuonDecision_TIS; ntp->SetBranchStatus("Dst_L0DiMuonDecision_TIS", 1); ntp->SetBranchAddress("Dst_L0DiMuonDecision_TIS", &Dst_L0DiMuonDecision_TIS);
  bool Dst_L0Global_TOS; ntp->SetBranchStatus("Dst_L0Global_TOS", 1); ntp->SetBranchAddress("Dst_L0Global_TOS", &Dst_L0Global_TOS);
  bool Dst_L0Global_TIS; ntp->SetBranchStatus("Dst_L0Global_TIS", 1); ntp->SetBranchAddress("Dst_L0Global_TIS", &Dst_L0Global_TIS);

  bool D0_L0HadronDecision_TOS; ntp->SetBranchStatus("D0_L0HadronDecision_TOS", 1); ntp->SetBranchAddress("D0_L0HadronDecision_TOS", &D0_L0HadronDecision_TOS);
  bool D0_L0HadronDecision_TIS; ntp->SetBranchStatus("D0_L0HadronDecision_TIS", 1); ntp->SetBranchAddress("D0_L0HadronDecision_TIS", &D0_L0HadronDecision_TIS);
  bool D0_L0Global_TOS; ntp->SetBranchStatus("D0_L0Global_TOS", 1); ntp->SetBranchAddress("D0_L0Global_TOS", &D0_L0Global_TOS);
  bool D0_L0Global_TIS; ntp->SetBranchStatus("D0_L0Global_TIS", 1); ntp->SetBranchAddress("D0_L0Global_TIS", &D0_L0Global_TIS);


  bool Dst_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("Dst_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("Dst_Hlt1TrackMVADecision_TOS", &Dst_Hlt1TrackMVADecision_TOS);
  bool Dst_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("Dst_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("Dst_Hlt1TrackMVADecision_TIS", &Dst_Hlt1TrackMVADecision_TIS);
  bool Dst_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("Dst_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("Dst_Hlt1TwoTrackMVADecision_TOS", &Dst_Hlt1TwoTrackMVADecision_TOS);
  bool Dst_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("Dst_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("Dst_Hlt1TwoTrackMVADecision_TIS", &Dst_Hlt1TwoTrackMVADecision_TIS);
  bool D0_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("D0_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("D0_Hlt1TrackMVADecision_TOS", &D0_Hlt1TrackMVADecision_TOS);
  bool D0_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("D0_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("D0_Hlt1TrackMVADecision_TIS", &D0_Hlt1TrackMVADecision_TIS);
  bool D0_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("D0_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("D0_Hlt1TwoTrackMVADecision_TOS", &D0_Hlt1TwoTrackMVADecision_TOS);
  bool D0_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("D0_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("D0_Hlt1TwoTrackMVADecision_TIS", &D0_Hlt1TwoTrackMVADecision_TIS);

  bool P1_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("P1_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("P1_Hlt1TrackMVADecision_TOS", &P1_Hlt1TrackMVADecision_TOS);
  bool P1_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("P1_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("P1_Hlt1TrackMVADecision_TIS", &P1_Hlt1TrackMVADecision_TIS);
  bool P1_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("P1_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("P1_Hlt1TwoTrackMVADecision_TOS", &P1_Hlt1TwoTrackMVADecision_TOS);
  bool P1_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("P1_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("P1_Hlt1TwoTrackMVADecision_TIS", &P1_Hlt1TwoTrackMVADecision_TIS);

  bool P2_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("P2_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("P2_Hlt1TrackMVADecision_TOS", &P2_Hlt1TrackMVADecision_TOS);
  bool P2_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("P2_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("P2_Hlt1TrackMVADecision_TIS", &P2_Hlt1TrackMVADecision_TIS);
  bool P2_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("P2_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("P2_Hlt1TwoTrackMVADecision_TOS", &P2_Hlt1TwoTrackMVADecision_TOS);
  bool P2_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("P2_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("P2_Hlt1TwoTrackMVADecision_TIS", &P2_Hlt1TwoTrackMVADecision_TIS);

  bool sPi_Hlt1TrackMVADecision_TOS; ntp->SetBranchStatus("sPi_Hlt1TrackMVADecision_TOS", 1); ntp->SetBranchAddress("sPi_Hlt1TrackMVADecision_TOS", &sPi_Hlt1TrackMVADecision_TOS);
  bool sPi_Hlt1TrackMVADecision_TIS; ntp->SetBranchStatus("sPi_Hlt1TrackMVADecision_TIS", 1); ntp->SetBranchAddress("sPi_Hlt1TrackMVADecision_TIS", &sPi_Hlt1TrackMVADecision_TIS);
  bool sPi_Hlt1TwoTrackMVADecision_TOS; ntp->SetBranchStatus("sPi_Hlt1TwoTrackMVADecision_TOS", 1); ntp->SetBranchAddress("sPi_Hlt1TwoTrackMVADecision_TOS", &sPi_Hlt1TwoTrackMVADecision_TOS);
  bool sPi_Hlt1TwoTrackMVADecision_TIS; ntp->SetBranchStatus("sPi_Hlt1TwoTrackMVADecision_TIS", 1); ntp->SetBranchAddress("sPi_Hlt1TwoTrackMVADecision_TIS", &sPi_Hlt1TwoTrackMVADecision_TIS);

  ///event variables
  unsigned long long int eventNumber; ntp->SetBranchStatus("eventNumber", 1); ntp->SetBranchAddress("eventNumber", &eventNumber);        
  unsigned int runNumber; ntp->SetBranchStatus("runNumber", 1); ntp->SetBranchAddress("runNumber", &runNumber);                          
  unsigned int nCandidate; ntp->SetBranchStatus("nCandidate", 1); ntp->SetBranchAddress("nCandidate", &nCandidate);                      
  int nVeloClusters; ntp->SetBranchStatus("nVeloClusters", 1); ntp->SetBranchAddress("nVeloClusters",&nVeloClusters);                    
  int nOTClusters; ntp->SetBranchStatus("nOTClusters", 1); ntp->SetBranchAddress("nOTClusters",&nOTClusters);  
  int nPVs; ntp->SetBranchStatus("nPVs", 1); ntp->SetBranchAddress("nPVs",&nPVs);
  int nTracks; ntp->SetBranchStatus("nTracks", 1); ntp->SetBranchAddress("nTracks",&nTracks);
  unsigned long long int totCandidates;  ntp->SetBranchStatus("totCandidates", 1); ntp->SetBranchAddress("totCandidates",&totCandidates);
  

  TFile * fOut = new TFile(outputFile, "RECREATE");                                                                                      
  TTree * ntpOut = new TTree(outputNtuple,outputNtuple); 

  /// masses 
  double o_D0_M; ntpOut->Branch("D0_M", &o_D0_M); 
  double o_D0_DTF_M; ntpOut->Branch("Dst_DTF_D0_M", &o_D0_DTF_M); 

  /// lifetimes 
  double o_D0_TAU; ntpOut->Branch("D0_TAU", &o_D0_TAU); 
  double o_D0_DTF_CTAU; ntpOut->Branch("Dst_DTF_D0_CTAU", &o_D0_DTF_CTAU); 

  /// IDs
  int o_Dst_ID; ntpOut->Branch("Dst_ID", &o_Dst_ID); 
  int o_D0_ID; ntpOut->Branch("D0_ID", &o_D0_ID); 
  int o_P1_ID; ntpOut->Branch("P1_ID", &o_P1_ID); 
  int o_P2_ID; ntpOut->Branch("P2_ID", &o_P2_ID); 

  /// kinematics 
  
  double o_Dst_P; ntpOut->Branch("Dst_P", &o_Dst_P); 
  double o_Dst_PT; ntpOut->Branch("Dst_PT", &o_Dst_PT);
  double o_Dst_PX; ntpOut->Branch("Dst_PX", &o_Dst_PX);
  double o_Dst_PY; ntpOut->Branch("Dst_PY", &o_Dst_PY);
  double o_Dst_PZ; ntpOut->Branch("Dst_PZ", &o_Dst_PZ);
  double o_Dst_ETA; ntpOut->Branch("Dst_ETA", &o_Dst_ETA);
  double o_Dst_PHI; ntpOut->Branch("Dst_PHI", &o_Dst_PHI);

  double o_Dst_DTF_P; ntpOut->Branch("Dst_DTF_P", &o_Dst_DTF_P); 
  double o_Dst_DTF_PT; ntpOut->Branch("Dst_DTF_PT", &o_Dst_DTF_PT);
  double o_Dst_DTF_PX; ntpOut->Branch("Dst_DTF_PX", &o_Dst_DTF_PX);
  double o_Dst_DTF_PY; ntpOut->Branch("Dst_DTF_PY", &o_Dst_DTF_PY);
  double o_Dst_DTF_PZ; ntpOut->Branch("Dst_DTF_PZ", &o_Dst_DTF_PZ);
  double o_Dst_DTF_ETA; ntpOut->Branch("Dst_DTF_ETA", &o_Dst_DTF_ETA);
  double o_Dst_DTF_PHI; ntpOut->Branch("Dst_DTF_PHI", &o_Dst_DTF_PHI);
  

  double o_D0_P; ntpOut->Branch("D0_P", &o_D0_P); 
  double o_D0_PT; ntpOut->Branch("D0_PT", &o_D0_PT);
  double o_D0_PX; ntpOut->Branch("D0_PX", &o_D0_PX);
  double o_D0_PY; ntpOut->Branch("D0_PY", &o_D0_PY);
  double o_D0_PZ; ntpOut->Branch("D0_PZ", &o_D0_PZ);
  double o_D0_ETA; ntpOut->Branch("D0_ETA", &o_D0_ETA);
  double o_D0_PHI; ntpOut->Branch("D0_PHI", &o_D0_PHI);
 
  /*double o_D0_P;*/ ntpOut->Branch("X_P", &o_D0_P); 
  /*double o_D0_PT;*/ ntpOut->Branch("X_PT", &o_D0_PT);
  /*double o_D0_PX;*/ ntpOut->Branch("X_PX", &o_D0_PX);
  /*double o_D0_PY;*/ ntpOut->Branch("X_PY", &o_D0_PY);
  /*double o_D0_PZ;*/ ntpOut->Branch("X_PZ", &o_D0_PZ);
  /*double o_D0_ETA;*/ ntpOut->Branch("X_ETA", &o_D0_ETA);
  /*double o_D0_PHI;*/ ntpOut->Branch("X_PHI", &o_D0_PHI);
  
  double o_D0_DTF_P; ntpOut->Branch("Dst_DTF_D0_P", &o_D0_DTF_P); 
  double o_D0_DTF_PT; ntpOut->Branch("Dst_DTF_D0_PT", &o_D0_DTF_PT); 
  double o_D0_DTF_PX; ntpOut->Branch("Dst_DTF_D0_PX", &o_D0_DTF_PX); 
  double o_D0_DTF_PY; ntpOut->Branch("Dst_DTF_D0_PY", &o_D0_DTF_PY); 
  double o_D0_DTF_PZ; ntpOut->Branch("Dst_DTF_D0_PZ", &o_D0_DTF_PZ); 
  double o_D0_DTF_ETA; ntpOut->Branch("Dst_DTF_D0_ETA", &o_D0_DTF_ETA); 
  double o_D0_DTF_PHI; ntpOut->Branch("Dst_DTF_D0_PHI", &o_D0_DTF_PHI); 

  double o_P1_P; ntpOut->Branch("P1_P", &o_P1_P); 
  double o_P1_PT; ntpOut->Branch("P1_PT", &o_P1_PT);
  double o_P1_PX; ntpOut->Branch("P1_PX", &o_P1_PX);
  double o_P1_PY; ntpOut->Branch("P1_PY", &o_P1_PY);
  double o_P1_PZ; ntpOut->Branch("P1_PZ", &o_P1_PZ);
  double o_P1_ETA; ntpOut->Branch("P1_ETA", &o_P1_ETA);
  double o_P1_PHI; ntpOut->Branch("P1_PHI", &o_P1_PHI);
  
                                                                                                                                         
  double o_P1_DTF_P; ntpOut->Branch("Dst_DTF_P1_P", &o_P1_DTF_P); 
  double o_P1_DTF_PT; ntpOut->Branch("Dst_DTF_P1_PT", &o_P1_DTF_PT); 
  double o_P1_DTF_PX; ntpOut->Branch("Dst_DTF_P1_PX", &o_P1_DTF_PX); 
  double o_P1_DTF_PY; ntpOut->Branch("Dst_DTF_P1_PY", &o_P1_DTF_PY); 
  double o_P1_DTF_PZ; ntpOut->Branch("Dst_DTF_P1_PZ", &o_P1_DTF_PZ); 
  double o_P1_DTF_ETA; ntpOut->Branch("Dst_DTF_P1_ETA", &o_P1_DTF_ETA); 
  double o_P1_DTF_PHI; ntpOut->Branch("Dst_DTF_P1_PHI", &o_P1_DTF_PHI); 


  double o_P2_P; ntpOut->Branch("P2_P", &o_P2_P); 
  double o_P2_PT; ntpOut->Branch("P2_PT", &o_P2_PT);
  double o_P2_PX; ntpOut->Branch("P2_PX", &o_P2_PX);
  double o_P2_PY; ntpOut->Branch("P2_PY", &o_P2_PY);
  double o_P2_PZ; ntpOut->Branch("P2_PZ", &o_P2_PZ);
  double o_P2_ETA; ntpOut->Branch("P2_ETA", &o_P2_ETA);
  double o_P2_PHI; ntpOut->Branch("P2_PHI", &o_P2_PHI);
                                                                                                                                         
  double o_P2_DTF_P; ntpOut->Branch("Dst_DTF_P2_P", &o_P2_DTF_P);
  double o_P2_DTF_PT; ntpOut->Branch("Dst_DTF_P2_PT", &o_P2_DTF_PT); 
  double o_P2_DTF_PX; ntpOut->Branch("Dst_DTF_P2_PX", &o_P2_DTF_PX); 
  double o_P2_DTF_PY; ntpOut->Branch("Dst_DTF_P2_PY", &o_P2_DTF_PY); 
  double o_P2_DTF_PZ; ntpOut->Branch("Dst_DTF_P2_PZ", &o_P2_DTF_PZ); 
  double o_P2_DTF_ETA; ntpOut->Branch("Dst_DTF_P2_ETA", &o_P2_DTF_ETA); 
  double o_P2_DTF_PHI; ntpOut->Branch("Dst_DTF_P2_PHI", &o_P2_DTF_PHI); 
                                                                                                                                         
  double o_sPi_P; ntpOut->Branch("sPi_P", &o_sPi_P); 
  double o_sPi_PT; ntpOut->Branch("sPi_PT", &o_sPi_PT);
  double o_sPi_PX; ntpOut->Branch("sPi_PX", &o_sPi_PX); 
  double o_sPi_PY; ntpOut->Branch("sPi_PY", &o_sPi_PY); 
  double o_sPi_PZ; ntpOut->Branch("sPi_PZ", &o_sPi_PZ); 
  double o_sPi_ETA; ntpOut->Branch("sPi_ETA", &o_sPi_ETA); 
  double o_sPi_PHI; ntpOut->Branch("sPi_PHI", &o_sPi_PHI); 
                                                                                                                                         
  double o_sPi_DTF_P; ntpOut->Branch("Dst_DTF_sPi_P", &o_sPi_DTF_P);
  double o_sPi_DTF_PT; ntpOut->Branch("Dst_DTF_sPi_PT", &o_sPi_DTF_PT);
  double o_sPi_DTF_PX; ntpOut->Branch("Dst_DTF_sPi_PX", &o_sPi_DTF_PX);
  double o_sPi_DTF_PY; ntpOut->Branch("Dst_DTF_sPi_PY", &o_sPi_DTF_PY);
  double o_sPi_DTF_PZ; ntpOut->Branch("Dst_DTF_sPi_PZ", &o_sPi_DTF_PZ);
  double o_sPi_DTF_ETA; ntpOut->Branch("Dst_DTF_sPi_ETA", &o_sPi_DTF_ETA);
  double o_sPi_DTF_PHI; ntpOut->Branch("Dst_DTF_sPi_PHI", &o_sPi_DTF_PHI);

  /// PID
  double o_P1_PIDK; ntpOut->Branch("P1_PIDK", &o_P1_PIDK); 
  double o_P1_PIDe; ntpOut->Branch("P1_PIDe", &o_P1_PIDe); 
  double o_P1_PIDmu; ntpOut->Branch("P1_PIDmu", &o_P1_PIDmu);
  double o_P1_PIDp; ntpOut->Branch("P1_PIDp", &o_P1_PIDp); 
                                                                                                                                         
  double o_P2_PIDK; ntpOut->Branch("P2_PIDK", &o_P2_PIDK); 
  double o_P2_PIDe; ntpOut->Branch("P2_PIDe", &o_P2_PIDe); 
  double o_P2_PIDmu; ntpOut->Branch("P2_PIDmu", &o_P2_PIDmu);
  double o_P2_PIDp; ntpOut->Branch("P2_PIDp", &o_P2_PIDp); 
                                                                                                                                         
  double o_sPi_PIDK; ntpOut->Branch("sPi_PIDK", &o_sPi_PIDK);
  double o_sPi_PIDe; ntpOut->Branch("sPi_PIDe", &o_sPi_PIDe);
  double o_sPi_PIDmu; ntpOut->Branch("sPi_PIDmu", &o_sPi_PIDmu);
  double o_sPi_PIDp; ntpOut->Branch("sPi_PIDp", &o_sPi_PIDp); 

  /// geometric
  double o_Dst_FD_OWNPV; ntpOut->Branch("Dst_FD_OWNPV", &o_Dst_FD_OWNPV); 
  double o_Dst_FDCHI2_OWNPV; ntpOut->Branch("Dst_FDCHI2_OWNPV", &o_Dst_FDCHI2_OWNPV); 
  double o_Dst_IP_OWNPV; ntpOut->Branch("Dst_IP_OWNPV", &o_Dst_IP_OWNPV); 
  double o_Dst_IPCHI2_OWNPV; ntpOut->Branch("Dst_IPCHI2_OWNPV", &o_Dst_IPCHI2_OWNPV); 
                                                                                                                                         
  double o_D0_FD_OWNPV; ntpOut->Branch("D0_FD_OWNPV", &o_D0_FD_OWNPV); 
  double o_D0_FDCHI2_OWNPV; ntpOut->Branch("D0_FDCHI2_OWNPV", &o_D0_FDCHI2_OWNPV);
  double o_D0_IP_OWNPV; ntpOut->Branch("D0_IP_OWNPV", &o_D0_IP_OWNPV); 
  double o_D0_IPCHI2_OWNPV; ntpOut->Branch("D0_IPCHI2_OWNPV", &o_D0_IPCHI2_OWNPV);
                                                                                                                                         
  double o_P1_IP_OWNPV; ntpOut->Branch("P1_IP_OWNPV", &o_P1_IP_OWNPV); 
  double o_P1_IPCHI2_OWNPV; ntpOut->Branch("P1_IPCHI2_OWNPV", &o_P1_IPCHI2_OWNPV); 
                                                                                                                                         
  double o_P2_IP_OWNPV; ntpOut->Branch("P2_IP_OWNPV", &o_P2_IP_OWNPV); 
  double o_P2_IPCHI2_OWNPV; ntpOut->Branch("P2_IPCHI2_OWNPV", &o_P2_IPCHI2_OWNPV); 
                                                                                                                                         
  double o_sPi_IP_OWNPV; ntpOut->Branch("sPi_IP_OWNPV", &o_sPi_IP_OWNPV); 
  double o_sPi_IPCHI2_OWNPV; ntpOut->Branch("sPi_IPCHI2_OWNPV", &o_sPi_IPCHI2_OWNPV); 

  /// vertices
  double o_Dst_ENDVERTEX_CHI2; ntpOut->Branch("Dst_ENDVERTEX_CHI2", &o_Dst_ENDVERTEX_CHI2); 
  int o_Dst_ENDVERTEX_NDOF; ntpOut->Branch("Dst_ENDVERTEX_NDOF", &o_Dst_ENDVERTEX_NDOF); 
  double o_Dst_ENDVERTEX_X; ntpOut->Branch("Dst_ENDVERTEX_X", &o_Dst_ENDVERTEX_X); 
  double o_Dst_ENDVERTEX_Y; ntpOut->Branch("Dst_ENDVERTEX_Y", &o_Dst_ENDVERTEX_Y); 
  double o_Dst_ENDVERTEX_Z; ntpOut->Branch("Dst_ENDVERTEX_Z", &o_Dst_ENDVERTEX_Z); 
  double o_Dst_ENDVERTEX_XERR; ntpOut->Branch("Dst_ENDVERTEX_XERR", &o_Dst_ENDVERTEX_XERR); 
  double o_Dst_ENDVERTEX_YERR; ntpOut->Branch("Dst_ENDVERTEX_YERR", &o_Dst_ENDVERTEX_YERR); 
  double o_Dst_ENDVERTEX_ZERR; ntpOut->Branch("Dst_ENDVERTEX_ZERR", &o_Dst_ENDVERTEX_ZERR); 
                                                                                                                                                                    
  double o_D0_ENDVERTEX_CHI2; ntpOut->Branch("D0_ENDVERTEX_CHI2", &o_D0_ENDVERTEX_CHI2); 
  int o_D0_ENDVERTEX_NDOF; ntpOut->Branch("D0_ENDVERTEX_NDOF", &o_D0_ENDVERTEX_NDOF); 
  double o_D0_ENDVERTEX_X; ntpOut->Branch("D0_ENDVERTEX_X", &o_D0_ENDVERTEX_X); 
  double o_D0_ENDVERTEX_Y; ntpOut->Branch("D0_ENDVERTEX_Y", &o_D0_ENDVERTEX_Y); 
  double o_D0_ENDVERTEX_Z; ntpOut->Branch("D0_ENDVERTEX_Z", &o_D0_ENDVERTEX_Z); 
  double o_D0_ENDVERTEX_XERR; ntpOut->Branch("D0_ENDVERTEX_XERR", &o_D0_ENDVERTEX_XERR); 
  double o_D0_ENDVERTEX_YERR; ntpOut->Branch("D0_ENDVERTEX_YERR", &o_D0_ENDVERTEX_YERR); 
  double o_D0_ENDVERTEX_ZERR; ntpOut->Branch("D0_ENDVERTEX_ZERR", &o_D0_ENDVERTEX_ZERR); 
                                                                                                                                                                    
  double o_Dst_OWNPV_X; ntpOut->Branch("Dst_OWNPV_X", &o_Dst_OWNPV_X); 
  double o_Dst_OWNPV_Y; ntpOut->Branch("Dst_OWNPV_Y", &o_Dst_OWNPV_Y); 
  double o_Dst_OWNPV_Z; ntpOut->Branch("Dst_OWNPV_Z", &o_Dst_OWNPV_Z); 
                                                                                                                                                                    
  double o_D0_OWNPV_X; ntpOut->Branch("D0_OWNPV_X", &o_D0_OWNPV_X); 
  double o_D0_OWNPV_Y; ntpOut->Branch("D0_OWNPV_Y", &o_D0_OWNPV_Y); 
  double o_D0_OWNPV_Z; ntpOut->Branch("D0_OWNPV_Z", &o_D0_OWNPV_Z); 
                                                                                                                 
  double o_P1_OWNPV_X; ntpOut->Branch("P1_OWNPV_X", &o_P1_OWNPV_X); 
  double o_P1_OWNPV_Y; ntpOut->Branch("P1_OWNPV_Y", &o_P1_OWNPV_Y); 
  double o_P1_OWNPV_Z; ntpOut->Branch("P1_OWNPV_Z", &o_P1_OWNPV_Z); 
                                                                                                                                                                    
  double o_P2_OWNPV_X; ntpOut->Branch("P2_OWNPV_X", &o_P2_OWNPV_X); 
  double o_P2_OWNPV_Y; ntpOut->Branch("P2_OWNPV_Y", &o_P2_OWNPV_Y); 
  double o_P2_OWNPV_Z; ntpOut->Branch("P2_OWNPV_Z", &o_P2_OWNPV_Z); 
                                                                                                                                                                    
  double o_sPi_OWNPV_X; ntpOut->Branch("sPi_OWNPV_X", &o_sPi_OWNPV_X); 
  double o_sPi_OWNPV_Y; ntpOut->Branch("sPi_OWNPV_Y", &o_sPi_OWNPV_Y); 
  double o_sPi_OWNPV_Z; ntpOut->Branch("sPi_OWNPV_Z", &o_sPi_OWNPV_Z); 
                                                                                                                                                                    
  float o_PVX; ntpOut->Branch("PVX", &o_PVX); 
  float o_PVY; ntpOut->Branch("PVY", &o_PVY); 
  float o_PVZ; ntpOut->Branch("PVZ", &o_PVZ); 

  ///track quality
  double o_P1_TRACK_CHI2NDOF; ntpOut->Branch("P1_TRACK_CHI2NDOF", &o_P1_TRACK_CHI2NDOF); 
  double o_P2_TRACK_CHI2NDOF; ntpOut->Branch("P2_TRACK_CHI2NDOF", &o_P2_TRACK_CHI2NDOF); 
                                                                                                                                                                                       
  double o_sPi_TRACK_MatchCHI2; ntpOut->Branch("sPi_TRACK_MatchCHI2", &o_sPi_TRACK_MatchCHI2); 
  double o_sPi_TRACK_GhostProb; ntpOut->Branch("sPi_TRACK_GhostProb", &o_sPi_TRACK_GhostProb); 
  double o_sPi_TRACK_FirstMeasurementX; ntpOut->Branch("sPi_TRACK_FirstMeasurementX", &o_sPi_TRACK_FirstMeasurementX); 
  double o_sPi_TRACK_FirstMeasurementY; ntpOut->Branch("sPi_TRACK_FirstMeasurementY", &o_sPi_TRACK_FirstMeasurementY); 
  double o_sPi_TRACK_FirstMeasurementZ; ntpOut->Branch("sPi_TRACK_FirstMeasurementZ", &o_sPi_TRACK_FirstMeasurementZ); 
  double o_sPi_ProbNNghost; ntpOut->Branch("sPi_ProbNNghost", &o_sPi_ProbNNghost); 

  double o_D0_DOCA; ntpOut->Branch("D0_DOCA", &o_D0_DOCA);
  double o_D0_DOCACHI2; ntpOut->Branch("D0_DOCACHI2", &o_D0_DOCACHI2);

  ///triggers
  bool o_Dst_L0HadronDecision_TOS; ntpOut->Branch("Dst_L0HadronDecision_TOS", &o_Dst_L0HadronDecision_TOS);
  bool o_Dst_L0HadronDecision_TIS; ntpOut->Branch("Dst_L0HadronDecision_TIS", &o_Dst_L0HadronDecision_TIS);
  bool o_Dst_L0MuonDecision_TOS; ntpOut->Branch("Dst_L0MuonDecision_TOS", &o_Dst_L0MuonDecision_TOS);
  bool o_Dst_L0MuonDecision_TIS; ntpOut->Branch("Dst_L0MuonDecision_TIS", &o_Dst_L0MuonDecision_TIS);
  bool o_Dst_L0DiMuonDecision_TOS; ntpOut->Branch("Dst_L0DiMuonDecision_TOS", &o_Dst_L0DiMuonDecision_TOS);
  bool o_Dst_L0DiMuonDecision_TIS; ntpOut->Branch("Dst_L0DiMuonDecision_TIS", &o_Dst_L0DiMuonDecision_TIS);
  bool o_Dst_L0Global_TOS; ntpOut->Branch("Dst_L0Global_TOS", &o_Dst_L0Global_TOS);
  bool o_Dst_L0Global_TIS; ntpOut->Branch("Dst_L0Global_TIS", &o_Dst_L0Global_TIS);
                             
  bool o_D0_L0HadronDecision_TOS; ntpOut->Branch("D0_L0HadronDecision_TOS", &o_D0_L0HadronDecision_TOS);
  bool o_D0_L0HadronDecision_TIS; ntpOut->Branch("D0_L0HadronDecision_TIS", &o_D0_L0HadronDecision_TIS);
  bool o_D0_L0Global_TOS; ntpOut->Branch("D0_L0Global_TOS", &o_D0_L0Global_TOS); 
  bool o_D0_L0Global_TIS; ntpOut->Branch("D0_L0Global_TIS", &o_D0_L0Global_TIS); 
                                                                                                                                                                   
  bool o_Dst_Hlt1TrackMVADecision_TOS; ntpOut->Branch("Dst_Hlt1TrackMVADecision_TOS", &o_Dst_Hlt1TrackMVADecision_TOS);
  bool o_Dst_Hlt1TrackMVADecision_TIS; ntpOut->Branch("Dst_Hlt1TrackMVADecision_TIS", &o_Dst_Hlt1TrackMVADecision_TIS);
  bool o_Dst_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("Dst_Hlt1TwoTrackMVADecision_TOS", &o_Dst_Hlt1TwoTrackMVADecision_TOS); 
  bool o_Dst_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("Dst_Hlt1TwoTrackMVADecision_TIS", &o_Dst_Hlt1TwoTrackMVADecision_TIS); 
 
  bool o_D0_Hlt1TrackMVADecision_TOS; ntpOut->Branch("D0_Hlt1TrackMVADecision_TOS", &o_D0_Hlt1TrackMVADecision_TOS);
  bool o_D0_Hlt1TrackMVADecision_TIS; ntpOut->Branch("D0_Hlt1TrackMVADecision_TIS", &o_D0_Hlt1TrackMVADecision_TIS);
  bool o_D0_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("D0_Hlt1TwoTrackMVADecision_TOS", &o_D0_Hlt1TwoTrackMVADecision_TOS); 
  bool o_D0_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("D0_Hlt1TwoTrackMVADecision_TIS", &o_D0_Hlt1TwoTrackMVADecision_TIS); 

  bool o_P1_Hlt1TrackMVADecision_TOS; ntpOut->Branch("P1_Hlt1TrackMVADecision_TOS", &o_P1_Hlt1TrackMVADecision_TOS);
  bool o_P1_Hlt1TrackMVADecision_TIS; ntpOut->Branch("P1_Hlt1TrackMVADecision_TIS", &o_P1_Hlt1TrackMVADecision_TIS);
  bool o_P1_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("P1_Hlt1TwoTrackMVADecision_TOS", &o_P1_Hlt1TwoTrackMVADecision_TOS); 
  bool o_P1_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("P1_Hlt1TwoTrackMVADecision_TIS", &o_P1_Hlt1TwoTrackMVADecision_TIS); 
                                                 
  bool o_P2_Hlt1TrackMVADecision_TOS; ntpOut->Branch("P2_Hlt1TrackMVADecision_TOS", &o_P2_Hlt1TrackMVADecision_TOS); 
  bool o_P2_Hlt1TrackMVADecision_TIS; ntpOut->Branch("P2_Hlt1TrackMVADecision_TIS", &o_P2_Hlt1TrackMVADecision_TIS); 
  bool o_P2_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("P2_Hlt1TwoTrackMVADecision_TOS", &o_P2_Hlt1TwoTrackMVADecision_TOS);
  bool o_P2_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("P2_Hlt1TwoTrackMVADecision_TIS", &o_P2_Hlt1TwoTrackMVADecision_TIS);
  
  bool o_sPi_Hlt1TrackMVADecision_TOS; ntpOut->Branch("sPi_Hlt1TrackMVADecision_TOS", &o_sPi_Hlt1TrackMVADecision_TOS); 
  bool o_sPi_Hlt1TrackMVADecision_TIS; ntpOut->Branch("sPi_Hlt1TrackMVADecision_TIS", &o_sPi_Hlt1TrackMVADecision_TIS); 
  bool o_sPi_Hlt1TwoTrackMVADecision_TOS; ntpOut->Branch("sPi_Hlt1TwoTrackMVADecision_TOS", &o_sPi_Hlt1TwoTrackMVADecision_TOS);
  bool o_sPi_Hlt1TwoTrackMVADecision_TIS; ntpOut->Branch("sPi_Hlt1TwoTrackMVADecision_TIS", &o_sPi_Hlt1TwoTrackMVADecision_TIS);

  ///event variables
  unsigned long long int o_eventNumber; ntpOut->Branch("eventNumber", &o_eventNumber); 
  unsigned int o_runNumber; ntpOut->Branch("runNumber", &o_runNumber); 
  unsigned int o_nCandidate; ntpOut->Branch("nCandidate", &o_nCandidate); 
  int o_nVeloClusters; ntpOut->Branch("nVeloClusters", &o_nVeloClusters); 
  int o_nOTClusters; ntpOut->Branch("nOTClusters", &o_nOTClusters); 
  int o_nPVs; ntpOut->Branch("nPVs", &o_nPVs); 
  int o_nTracks; ntpOut->Branch("nTracks", &o_nTracks); 
  unsigned long long int o_totCandidates;  ntpOut->Branch("totCandidates", &o_totCandidates); 


  //Useful variables
  TVector3 v_D0_OWNPV, v_D0_ENDVERTEX, p_D0;                                                                                       
  TVector3 p_beam;                                                                                                               
  TVector3 vecProduct;                                                                                                           
  TVector3 v_D0, v_DTF_D0, v_P1, v_DTF_P1, v_P2, v_DTF_P2, v_sPi, v_DTF_sPi, v_Dst, v_DTF_Dst;
 
  //Derived quantities                                                            
  double o_D0_TIP_OWNPV; ntpOut->Branch("D0_TIP_OWNPV", &o_D0_TIP_OWNPV); 
  double o_DTF_Mass; ntpOut->Branch("DTF_Mass", &o_DTF_Mass);
  bool o_isPrompt; ntpOut->Branch("isPrompt", &o_isPrompt);
  
  TH1F * h_dm_plus = new TH1F("h_dm_plus","h_dm_plus",500,2004.5,2020);                                         
  TH1F * h_dm_minus = new TH1F("h_dm_minus","h_dm_minus",500,2004.5,2020);            


  int nEntries = ntp->GetEntries();

  bool PIDCut      = true;
  bool FiducialCut = true;
  bool TriggerCut     = true;
  bool HarmonizationCut = true;
  bool D0massCut   = true;
  bool FitRange      = true;

  //Booleans
  ntpOut->Branch("HarmonizationCut",&HarmonizationCut);

  TH2D * h_sPi_pzpx_fidCut_plus = new TH2D("h_sPi_pzpx_fidCut_plus","h_sPi_pzpx_fidCut_plus",1000,0,50,1000,-5,5);
  TH2D * h_sPi_pzpx_fidCut_minus = new TH2D("h_sPi_pzpx_fidCut_minus","h_sPi_pzpx_fidCut_minus",1000,0,50,1000,-5,5);
  TH2D * h_sPi_pzpx_nofidCut_plus = new TH2D("h_sPi_pzpx_nofidCut_plus","h_sPi_pzpx_nofidCut_plus",1000,0,50,1000,-5,5);
  TH2D * h_sPi_pzpx_nofidCut_minus = new TH2D("h_sPi_pzpx_nofidCut_minus","h_sPi_pzpx_nofidCut_minus",1000,0,50,1000,-5,5);
  TH2D * h_sPi_pzpx_Removed_plus = new TH2D("h_sPi_pzpx_Removed_plus","h_sPi_pzpx_Removed_plus",1000,0,50,1000,-5,5);
  TH2D * h_sPi_pzpx_Removed_minus = new TH2D("h_sPi_pzpx_Removed_minus","h_sPi_pzpx_Removed_minus",1000,0,50,1000,-5,5);
 
  for(int i = 0; i < nEntries; i++) {                                                                                                                                                           
    
    ntp->GetEntry(i);

    if(inputNtuple.Contains("KK"))
      {
	TriggerCut = (Dst_L0Global_TIS || D0_L0Global_TOS) && (D0_Hlt1TrackMVADecision_TOS==1 || D0_Hlt1TwoTrackMVADecision_TOS==1);
	PIDCut  = P1_PIDK>5&&P2_PIDK>5;
      }
    else if(inputNtuple.Contains("KP"))
      {
	TriggerCut = (Dst_L0Global_TIS) && (Dst_Hlt1TrackMVADecision_TIS==1);
	PIDCut  = P1_PIDK<-5&&P2_PIDK>5;
      }

    D0massCut = D0_M>1844&&D0_M<1887;
   
  
    if(doSelection) {
      if(!TriggerCut)   continue;
      if(!D0massCut) continue;
      if(!PIDCut)    continue;
    }



    //isPrompt definition with TIP
    v_D0_OWNPV.SetXYZ(D0_OWNPV_X, D0_OWNPV_Y, D0_OWNPV_Z);      
    v_D0_ENDVERTEX.SetXYZ(D0_ENDVERTEX_X, D0_ENDVERTEX_Y, D0_ENDVERTEX_Z);        
    p_D0.SetXYZ(D0_PX, D0_PY, D0_PZ);           
    p_beam.SetXYZ(0., 0., 1.);              
    vecProduct = p_beam.Cross(p_D0);                  
    o_D0_TIP_OWNPV = vecProduct.Dot(v_D0_ENDVERTEX - v_D0_OWNPV) / vecProduct.Mag();                                     
    //o_isPrompt = D0_IPCHI2_OWNPV<9;
    o_isPrompt = abs(o_D0_TIP_OWNPV)<0.040;
    if(doSelection)
      if(!o_isPrompt) continue;

    v_D0.SetXYZ(D0_PX,D0_PY,D0_PZ);

    v_P1.SetXYZ(P1_PX,P1_PY,P1_PZ);
    v_DTF_P1.SetXYZ(P1_DTF_PX,P1_DTF_PY,P1_DTF_PZ);
    v_P2.SetXYZ(P2_PX,P2_PY,P2_PZ);
    v_DTF_P2.SetXYZ(P2_DTF_PX,P2_DTF_PY,P2_DTF_PZ);
    v_sPi.SetXYZ(sPi_PX,sPi_PY,sPi_PZ);
    v_DTF_sPi.SetXYZ(sPi_DTF_PX,sPi_DTF_PY,sPi_DTF_PZ);

    v_DTF_D0 = v_DTF_P1+v_DTF_P2;
    v_Dst = v_P1+v_P2+v_sPi;
    v_DTF_Dst = v_DTF_P1+v_DTF_P2+v_DTF_sPi;

    o_D0_M = D0_M;
    o_D0_DTF_M = D0_DTF_M;

    o_D0_TAU = D0_TAU;
    o_D0_DTF_CTAU = D0_DTF_CTAU;
    
    o_Dst_ID = Dst_ID;
    o_D0_ID = D0_ID;
    o_P1_ID = P1_ID;
    o_P2_ID = P2_ID;
    
    o_Dst_P = Dst_P;
    o_Dst_PT = Dst_PT;
    o_Dst_PX = Dst_PX;
    o_Dst_PY = Dst_PY;
    o_Dst_PZ = Dst_PZ;
    o_Dst_ETA = v_Dst.PseudoRapidity();
    o_Dst_PHI = TMath::ATan2(Dst_PY,D0_PX);

    o_Dst_DTF_P = v_DTF_Dst.Mag();
    o_Dst_DTF_PT = v_DTF_Dst.Pt();
    o_Dst_DTF_PX = v_DTF_Dst.Px();
    o_Dst_DTF_PY = v_DTF_Dst.Py();
    o_Dst_DTF_PZ = v_DTF_Dst.Pz();
    o_Dst_DTF_ETA = v_DTF_Dst.PseudoRapidity();
    o_Dst_DTF_PHI = TMath::ATan2(v_DTF_Dst.Py(),v_DTF_Dst.Pz()); 
    
    o_D0_P = D0_P;
    o_D0_PT = D0_PT;
    o_D0_PX = D0_PX;
    o_D0_PY = D0_PY;
    o_D0_PZ = D0_PZ;
    o_D0_ETA = v_D0.PseudoRapidity();
    o_D0_PHI = TMath::ATan2(D0_PY,D0_PX);

    
    o_D0_DTF_P = v_DTF_D0.Mag();
    o_D0_DTF_PT = v_DTF_D0.Pt();
    o_D0_DTF_PX = v_DTF_D0.Px();
    o_D0_DTF_PY = v_DTF_D0.Py();
    o_D0_DTF_PZ = v_DTF_D0.Pz();
    o_D0_DTF_ETA = v_DTF_D0.PseudoRapidity();
    o_D0_DTF_PHI = TMath::ATan2(v_DTF_D0.Py(),v_DTF_D0.Pz()); 

    o_P1_P = P1_P;
    o_P1_PT = P1_PT;
    o_P1_PX = P1_PX;
    o_P1_PY = P1_PY;
    o_P1_PZ = P1_PZ;
    o_P1_ETA = v_P1.PseudoRapidity();
    o_P1_PHI = TMath::ATan2(P1_PY,P1_PX);

    o_P1_DTF_P = sqrt(P1_DTF_PX*P1_DTF_PX+P1_DTF_PY*P1_DTF_PY+P1_DTF_PZ*P1_DTF_PZ);;
    o_P1_DTF_PT = sqrt(P1_DTF_PX*P1_DTF_PX+P1_DTF_PY*P1_DTF_PY);
    o_P1_DTF_PX = P1_DTF_PX;
    o_P1_DTF_PY = P1_DTF_PY;
    o_P1_DTF_PZ = P1_DTF_PZ;
    o_P1_DTF_ETA = v_DTF_P1.PseudoRapidity();
    o_P1_DTF_PHI = TMath::ATan2(P1_DTF_PY,P1_DTF_PX); 

    o_P2_P = P2_P;
    o_P2_PT = P2_PT;
    o_P2_PX = P2_PX;
    o_P2_PY = P2_PY;
    o_P2_PZ = P2_PZ;
    o_P2_ETA = v_P2.PseudoRapidity();
    o_P2_PHI = TMath::ATan2(P2_PY,P2_PX);

    o_P2_DTF_P = sqrt(P2_DTF_PX*P2_DTF_PX+P2_DTF_PY*P2_DTF_PY+P2_DTF_PZ*P2_DTF_PZ);;
    o_P2_DTF_PT = sqrt(P2_DTF_PX*P2_DTF_PX+P2_DTF_PY*P2_DTF_PY);
    o_P2_DTF_PX = P2_DTF_PX;
    o_P2_DTF_PY = P2_DTF_PY;
    o_P2_DTF_PZ = P2_DTF_PZ;
    o_P2_DTF_ETA = v_DTF_P2.PseudoRapidity();
    o_P2_DTF_PHI = TMath::ATan2(P2_DTF_PY,P2_DTF_PX); 

    o_sPi_P = sPi_P;
    o_sPi_PT = sPi_PT;
    o_sPi_PX = sPi_PX;
    o_sPi_PY = sPi_PY;
    o_sPi_PZ = sPi_PZ;
    o_sPi_ETA = v_sPi.PseudoRapidity();
    o_sPi_PHI = TMath::ATan2(sPi_PY,sPi_PX);

    o_sPi_DTF_P = sqrt(sPi_DTF_PX*sPi_DTF_PX+sPi_DTF_PY*sPi_DTF_PY+sPi_DTF_PZ*sPi_DTF_PZ);
    o_sPi_DTF_PT = sqrt(sPi_DTF_PX*sPi_DTF_PX+sPi_DTF_PY*sPi_DTF_PY);
    o_sPi_DTF_PX = sPi_DTF_PX;
    o_sPi_DTF_PY = sPi_DTF_PY;
    o_sPi_DTF_PZ = sPi_DTF_PZ;
    o_sPi_DTF_ETA = v_DTF_sPi.PseudoRapidity();
    o_sPi_DTF_PHI = TMath::ATan2(sPi_DTF_PY,sPi_DTF_PX); 

    o_P1_PIDK = P1_PIDK;
    o_P1_PIDe = P1_PIDe;
    o_P1_PIDmu = P1_PIDmu;
    o_P1_PIDp = P1_PIDp;

    o_P2_PIDK = P2_PIDK;
    o_P2_PIDe = P2_PIDe;
    o_P2_PIDmu = P2_PIDmu;
    o_P2_PIDp = P2_PIDp;

    o_sPi_PIDK = sPi_PIDK;
    o_sPi_PIDe = sPi_PIDe;
    o_sPi_PIDmu = sPi_PIDmu;
    o_sPi_PIDp = sPi_PIDp;

    o_Dst_FD_OWNPV = Dst_FD_OWNPV;
    o_Dst_FDCHI2_OWNPV = Dst_FDCHI2_OWNPV;
    o_Dst_IP_OWNPV = Dst_IP_OWNPV;
    o_Dst_IPCHI2_OWNPV = Dst_IPCHI2_OWNPV;

    o_D0_FD_OWNPV = D0_FD_OWNPV;
    o_D0_FDCHI2_OWNPV = D0_FDCHI2_OWNPV;
    o_D0_IP_OWNPV = D0_IP_OWNPV;
    o_D0_IPCHI2_OWNPV = D0_IPCHI2_OWNPV;

    o_P1_IP_OWNPV = P1_IP_OWNPV;
    o_P1_IPCHI2_OWNPV = P1_IPCHI2_OWNPV;
    
    o_P2_IP_OWNPV = P2_IP_OWNPV;
    o_P2_IPCHI2_OWNPV = P2_IPCHI2_OWNPV;
    o_sPi_IP_OWNPV = sPi_IP_OWNPV;
    o_sPi_IPCHI2_OWNPV = sPi_IPCHI2_OWNPV;
    
    o_Dst_ENDVERTEX_CHI2 = Dst_ENDVERTEX_CHI2;
    o_Dst_ENDVERTEX_NDOF = Dst_ENDVERTEX_NDOF;
    o_Dst_ENDVERTEX_X = Dst_ENDVERTEX_X;
    o_Dst_ENDVERTEX_Y = Dst_ENDVERTEX_Y;
    o_Dst_ENDVERTEX_Z = Dst_ENDVERTEX_Z;
    o_Dst_ENDVERTEX_XERR = Dst_ENDVERTEX_XERR;
    o_Dst_ENDVERTEX_YERR = Dst_ENDVERTEX_YERR;
    o_Dst_ENDVERTEX_ZERR = Dst_ENDVERTEX_ZERR;
    
    o_D0_ENDVERTEX_CHI2 = D0_ENDVERTEX_CHI2;
    o_D0_ENDVERTEX_NDOF = D0_ENDVERTEX_NDOF;
    o_D0_ENDVERTEX_X = D0_ENDVERTEX_X;
    o_D0_ENDVERTEX_Y = D0_ENDVERTEX_Y;
    o_D0_ENDVERTEX_Z = D0_ENDVERTEX_Z;
    o_D0_ENDVERTEX_XERR = D0_ENDVERTEX_XERR;
    o_D0_ENDVERTEX_YERR = D0_ENDVERTEX_YERR;
    o_D0_ENDVERTEX_ZERR = D0_ENDVERTEX_ZERR;
    
    o_Dst_OWNPV_X = Dst_OWNPV_X;
    o_Dst_OWNPV_Y = Dst_OWNPV_Y;
    o_Dst_OWNPV_Z = Dst_OWNPV_Z;
    
    o_D0_OWNPV_X = D0_OWNPV_X;
    o_D0_OWNPV_Y = D0_OWNPV_Y;
    o_D0_OWNPV_Z = D0_OWNPV_Z;
    
    o_P1_OWNPV_X = P1_OWNPV_X;
    o_P1_OWNPV_Y = P1_OWNPV_Y;
    o_P1_OWNPV_Z = P1_OWNPV_Z;

    o_P2_OWNPV_X = P2_OWNPV_X;
    o_P2_OWNPV_Y = P2_OWNPV_Y;
    o_P2_OWNPV_Z = P2_OWNPV_Z;
    
    o_sPi_OWNPV_X = sPi_OWNPV_X;
    o_sPi_OWNPV_Y = sPi_OWNPV_Y;
    o_sPi_OWNPV_Z = sPi_OWNPV_Z;

    o_PVX = PVX[0];
    o_PVY = PVY[0];
    o_PVZ = PVZ[0];
    
    o_P1_TRACK_CHI2NDOF = P1_TRACK_CHI2NDOF;
    o_P2_TRACK_CHI2NDOF = P2_TRACK_CHI2NDOF;
    
    o_sPi_TRACK_MatchCHI2 = sPi_TRACK_MatchCHI2;
    o_sPi_TRACK_GhostProb = sPi_TRACK_GhostProb;
    o_sPi_TRACK_FirstMeasurementX = sPi_TRACK_FirstMeasurementX;
    o_sPi_TRACK_FirstMeasurementY = sPi_TRACK_FirstMeasurementY;
    o_sPi_TRACK_FirstMeasurementZ = sPi_TRACK_FirstMeasurementZ;
    o_sPi_ProbNNghost = sPi_ProbNNghost;

    o_D0_DOCA = D0_DOCA;
    o_D0_DOCACHI2 = D0_DOCACHI2;
  
    o_Dst_L0HadronDecision_TOS = Dst_L0HadronDecision_TOS;
    o_Dst_L0HadronDecision_TIS = Dst_L0HadronDecision_TIS;
    o_Dst_L0MuonDecision_TOS = Dst_L0MuonDecision_TOS;
    o_Dst_L0MuonDecision_TIS = Dst_L0MuonDecision_TIS;
    o_Dst_L0DiMuonDecision_TOS = Dst_L0DiMuonDecision_TOS;
    o_Dst_L0DiMuonDecision_TIS = Dst_L0DiMuonDecision_TIS;
    o_Dst_L0Global_TOS = Dst_L0Global_TOS;
    o_Dst_L0Global_TIS = Dst_L0Global_TIS;

    o_D0_L0HadronDecision_TOS = D0_L0HadronDecision_TOS;
    o_D0_L0HadronDecision_TIS = D0_L0HadronDecision_TIS;
    o_D0_L0Global_TOS = D0_L0Global_TOS;
    o_D0_L0Global_TIS = D0_L0Global_TIS;
    
    o_Dst_Hlt1TrackMVADecision_TOS = Dst_Hlt1TrackMVADecision_TOS;
    o_Dst_Hlt1TrackMVADecision_TIS = Dst_Hlt1TrackMVADecision_TIS;
    o_Dst_Hlt1TwoTrackMVADecision_TOS = Dst_Hlt1TwoTrackMVADecision_TOS;
    o_Dst_Hlt1TwoTrackMVADecision_TIS = Dst_Hlt1TwoTrackMVADecision_TIS;

    o_D0_Hlt1TrackMVADecision_TOS = D0_Hlt1TrackMVADecision_TOS;
    o_D0_Hlt1TrackMVADecision_TIS = D0_Hlt1TrackMVADecision_TIS;
    o_D0_Hlt1TwoTrackMVADecision_TOS = D0_Hlt1TwoTrackMVADecision_TOS;
    o_D0_Hlt1TwoTrackMVADecision_TIS = D0_Hlt1TwoTrackMVADecision_TIS;

    o_P1_Hlt1TrackMVADecision_TOS = P1_Hlt1TrackMVADecision_TOS;
    o_P1_Hlt1TrackMVADecision_TIS = P1_Hlt1TrackMVADecision_TIS;
    o_P1_Hlt1TwoTrackMVADecision_TOS = P1_Hlt1TwoTrackMVADecision_TOS;
    o_P1_Hlt1TwoTrackMVADecision_TIS = P1_Hlt1TwoTrackMVADecision_TIS;

    o_P2_Hlt1TrackMVADecision_TOS = P2_Hlt1TrackMVADecision_TOS;
    o_P2_Hlt1TrackMVADecision_TIS = P2_Hlt1TrackMVADecision_TIS;
    o_P2_Hlt1TwoTrackMVADecision_TOS = P2_Hlt1TwoTrackMVADecision_TOS;
    o_P2_Hlt1TwoTrackMVADecision_TIS = P2_Hlt1TwoTrackMVADecision_TIS;

    o_sPi_Hlt1TrackMVADecision_TOS = sPi_Hlt1TrackMVADecision_TOS;
    o_sPi_Hlt1TrackMVADecision_TIS = sPi_Hlt1TrackMVADecision_TIS;
    o_sPi_Hlt1TwoTrackMVADecision_TOS = sPi_Hlt1TwoTrackMVADecision_TOS;
    o_sPi_Hlt1TwoTrackMVADecision_TIS = sPi_Hlt1TwoTrackMVADecision_TIS;
  
    o_eventNumber = eventNumber;
    o_runNumber = runNumber;
    o_nCandidate = nCandidate;
    o_nVeloClusters = nVeloClusters;
    o_nOTClusters = nOTClusters;
    o_nPVs = nPVs;
    o_nTracks = nTracks;
    o_totCandidates = totCandidates;

    o_DTF_Mass = sqrt(PdgMass::mD0_PDG*PdgMass::mD0_PDG+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG+2*sqrt(PdgMass::mD0_PDG*PdgMass::mD0_PDG+o_D0_DTF_P*o_D0_DTF_P)*sqrt(PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG+o_sPi_DTF_P*o_sPi_DTF_P)-2*(o_D0_DTF_PX*o_sPi_DTF_PX+o_D0_DTF_PY*o_sPi_DTF_PY+o_D0_DTF_PZ*o_sPi_DTF_PZ));

    if(doSelection) {
      FitRange = o_DTF_Mass > 2004.5 && o_DTF_Mass < 2020;
      if(!FitRange) continue;
    }
 

 if(doSelection) {
     HarmonizationCut = P2_P>5e3 && P2_PT>800 && o_P1_P>5e3 && o_P1_PT>800 && D0_P > 30e3;
     HarmonizationCut = HarmonizationCut && Dst_PT > 2.28e3 && Dst_PT < 9.5e3 && sPi_PT > 100 && sPi_PT < 800 && o_Dst_ETA > 2.4 && o_Dst_ETA < 4.3 && o_sPi_ETA > 2.4 && o_sPi_ETA < 4.3;
	
     if(inputNtuple.Contains("KP")) {
       HarmonizationCut = HarmonizationCut && P1_PT + P2_PT > 2280 && P1_PT < 5.5e3 && P2_PT < 6e3 && o_P1_ETA > 2.2 && o_P1_ETA < 4.3 && o_P2_ETA > 2.2 && o_P2_ETA < 4.3;
     }
      
     //if(!HarmonizationCut) continue;
     if (!(P2_P>5e3 && P2_PT>800 && P1_P>5e3 && P1_PT>800 && P2_PT<8e3 && P1_PT<8e3)) continue;
    
    }
    
   
   if(Dst_ID>0) {
     h_sPi_pzpx_nofidCut_plus->Fill(1e-3*sPi_PZ,1e-3*sPi_PX);
   }
   else {
     h_sPi_pzpx_nofidCut_minus->Fill(1e-3*sPi_PZ,1e-3*sPi_PX);
   }
    
   //FiducialCut = fabs(sPi_PX)<0.317*(sPi_P-2400) && ( (fabs(sPi_PY/sPi_PZ)>0.02) ||  (((fabs(sPi_PY/sPi_PZ)<0.02)&&(fabs(sPi_PX)<418-0.01397*sPi_PZ)) || (fabs(sPi_PX)>497+0.01605*sPi_PZ)));
   //  FiducialCut = ( fabs(sPi_PX)<0.317*(sPi_PZ-1910) && ( (fabs(sPi_PY/sPi_PZ)>0.014) || (((fabs(sPi_PY/sPi_PZ)<0.014)&&(fabs(sPi_PX)<418-0.01397*sPi_PZ))||(fabs(sPi_PX)>497+0.01605*sPi_PZ))) );
   FiducialCut = ( fabs(sPi_DTF_PX)<0.317*(sPi_DTF_PZ-1910) && ( (fabs(sPi_DTF_PY/sPi_DTF_PZ)>0.014) || (((fabs(sPi_DTF_PY/sPi_DTF_PZ)<0.014)&&(fabs(sPi_DTF_PX)<418-0.01397*sPi_DTF_PZ))||(fabs(sPi_DTF_PX)>497+0.01605*sPi_DTF_PZ))) );
    //FiducialCut = ( fabs(Dst_DTF_PV_sPi_PX)<0.317*(Dst_DTF_PV_sPi_PZ-1910) && ( (fabs(Dst_DTF_PV_sPi_PY/Dst_DTF_PV_sPi_PZ)>0.014) || (((fabs(Dst_DTF_PV_sPi_PY/Dst_DTF_PV_sPi_PZ)<0.014)&&(fabs(Dst_DTF_PV_sPi_PX)<418-0.01397*Dst_DTF_PV_sPi_PZ))||(fabs(Dst_DTF_PV_sPi_PX)>497+0.01605*Dst_DTF_PV_sPi_PZ))) );

    if(doFiducial) 
      if(!FiducialCut) {
	if(Dst_ID>0) {
	  h_sPi_pzpx_Removed_plus->Fill(1e-3*sPi_PZ,1e-3*sPi_PX);
	}
	else {
	  h_sPi_pzpx_Removed_minus->Fill(1e-3*sPi_PZ,1e-3*sPi_PX);
	}
 	continue;
      }

    if(Dst_ID>0) {
      h_sPi_pzpx_fidCut_plus->Fill(1e-3*sPi_PZ,1e-3*sPi_PX);
    }
    else {
      h_sPi_pzpx_fidCut_minus->Fill(1e-3*sPi_PZ,1e-3*sPi_PX);
    }
 
 
    if(Dst_ID>0) h_dm_plus->Fill(o_DTF_Mass);
    else         h_dm_minus->Fill(o_DTF_Mass);   
    ntpOut->Fill();
    
  }
  

  h_sPi_pzpx_nofidCut_plus->Write();
  h_sPi_pzpx_fidCut_plus->Write();
  h_sPi_pzpx_nofidCut_minus->Write();
  h_sPi_pzpx_fidCut_minus->Write();
  h_sPi_pzpx_Removed_plus->Write();
  h_sPi_pzpx_Removed_minus->Write();

  h_dm_plus->Write();
  h_dm_minus->Write();
  ntpOut->Write();
  
  // ntp->Print();                                                                                                   
  cout << "Before removing multiple candidates: " << ntpOut->GetEntries() << endl;
  
  
  fOut->Close();

  if(doMultCandRemoval) {                                                                                                              
    TFile * inputFile = TFile::Open(Form(outputFile));                                                                                 
    TTree * inputTree = (TTree*)inputFile->Get("ntp");                                                                                 
                                                                                                                                       
                  
                                                                                                                                       
    UInt_t prev_runNumber = 0, mcrunNumber = 0;                                                                                        
    ULong64_t prev_eventNumber = 0, mceventNumber = 0;                                                                                 
    ULong64_t mctotCandidates = 0;                                                                                                     
    int mcpdg; double mcDTF_Mass;                                                                                                      
                                                                                                                                       
    inputTree->SetBranchAddress( "runNumber", &mcrunNumber );                                                                          
    inputTree->SetBranchAddress( "eventNumber", &mceventNumber );                                                                      
    inputTree->SetBranchAddress( "totCandidates", &mctotCandidates );                                                                  
    inputTree->SetBranchAddress( "Dst_ID", &mcpdg );                                                                                 
    inputTree->SetBranchAddress( "DTF_Mass", &mcDTF_Mass );                                                                             
    double D0_TIP_OWNPV; 
    inputTree->SetBranchAddress("D0_TIP_OWNPV", &D0_TIP_OWNPV); 
    double X_P; 
    inputTree->SetBranchAddress("X_P", &X_P);
    double P1_PT; 
    inputTree->SetBranchAddress("P1_PT", &P1_PT);
    double P2_PT; 
    inputTree->SetBranchAddress("P2_PT", &P2_PT);
    
                                                                                           
    // outputFile.ReplaceAll("_noMultCandRemoval.root","_withMultCandRemoval.root");                                         
    outputFile.ReplaceAll("_noMultCandRemoval.root",".root");                                                      
    TFile *outmc = new TFile(outputFile, "RECREATE");                                                                                  
                                                                                                                                       
    TTree *outputTree = inputTree->CloneTree(0);                                                                                       
                                                                                                                                       
    TH1F * h_dm_plus_tmp = new TH1F("h_dm_plus_tmp","h_dm_plus_tmp",500,2004.5,2020);                                                    
    TH1F * h_dm_minus_tmp = new TH1F("h_dm_minus_tmp","h_dm_minus_tmp",500,2004.5,2020);                              
    TH1D * h_X_P = new TH1D("h_X_P","h_X_P",100,0,140);
    TH1D * h_X_sumPTs = new TH1D("h_X_sumPTs","h_X_sumPTs",100,0,12e3);
 
                 
                                                                                                                                       
    std::cout << " --- If there are multiple candidates, I will choose one randomly...\n";                                             
    bool switched_event = false;                                                                                                       
    std::vector<Long64_t> indices = {};                                                                                                
    TRandom3 random_mult;                                                                                                              
    double nMC = 0;                                                                                                                    
    int nEntries_survived = inputTree->GetEntries();                                                                                   
                                                                                                                                       
    for ( Long64_t iEntry = 0; iEntry < nEntries_survived; iEntry++ ) {                                                                
      if ( iEntry%50000 == 0 )                                                                                                         
	std::cout << "  processing Event " << iEntry << " / " << nEntries_survived << "\n";                                            
      inputTree->GetEntry( iEntry );                                            

      if ( iEntry != 0) {                                                                                                              
        if ( ( prev_runNumber != mcrunNumber ) || ( prev_eventNumber != mceventNumber) )                                               
          switched_event = true;                                                                                                       
        else                                                                                                                           
          switched_event = false;                                                                                                      
        if ( switched_event ) {                                                                                                        
          if ( indices.size() > 1 ) {                                                                                                  
            random_mult.SetSeed(mcrunNumber*mceventNumber);                                                                            
            UInt_t rand_index = random_mult.Integer( indices.size() );                                                                 
            Long64_t rand_entry = indices.at( rand_index );                                                                            
            inputTree->GetEntry( rand_entry );                                                                                         
          }                                                                                                                            
          else                                                                                                                         
            inputTree->GetEntry( iEntry - 1 );                                                                            
                                                                                                                                       
          indices.clear();                    

	  //fill                                                                                             
	  h_X_P->Fill(X_P/1e3);
	  h_X_sumPTs->Fill(P1_PT + P2_PT);  
	  
          if(mcpdg>0) h_dm_plus_tmp->Fill(mcDTF_Mass);                                                                                 
          if(mcpdg<0) h_dm_minus_tmp->Fill(mcDTF_Mass);                                                                                
	  
                                                                                                                                       
          outputTree->Fill();                                                                                                          
        }                                                                                                                              
      }                                                         

      // check entry to be filled (or not) at the next iteration                                                                       
      inputTree->GetEntry( iEntry );                                                                                                   
      if ( mctotCandidates == 1 )                                                                                                      
        indices.clear();                                                                                                               
      else {                                                                                                                           
        indices.push_back( iEntry );                                                                                                   
      }                                                                                                                                
      prev_runNumber = mcrunNumber;                                                                                                    
      prev_eventNumber = mceventNumber;                                                                                                
    }                                                                                                                                  
                                                                                                                                       
    // check on very last entry                                                                                                        
    if ( indices.size() > 1 ) {                                                                                                        
      UInt_t rand_index = random_mult.Integer( indices.size() );                                                                       
      Long64_t rand_entry = indices.at( rand_index );                                                                                  
      inputTree->GetEntry( rand_entry );                                                                                               
      nMC++;                                                                                                                           
    }                                                                                                                                  
    else                                                                                                                               
      inputTree->GetEntry( nEntries_survived );                                                                                        
    indices.clear();                                                                                                                   
    outputTree->Fill();                                                                                                                
    //fill                                                                                                  
    h_X_P->Fill(X_P/1e3);
    h_X_sumPTs->Fill(P1_PT + P2_PT);  
    
    if(mcpdg>0) h_dm_plus_tmp->Fill(mcDTF_Mass);                                                                                 
    if(mcpdg<0) h_dm_minus_tmp->Fill(mcDTF_Mass);                                                                                                                                                                                                  
    outmc->WriteTObject( outputTree, "ntp", "WriteDelete" );                                                                           
                                                                                                                                       
    std::cout << " ===> Random candidates have been picked up!\n";                                                                     
    cout << "After removing all multiple candidates: " << outputTree->GetEntries() << endl;          
    
    h_X_P->Write();
    h_X_sumPTs->Write();

    h_dm_plus_tmp->SetName("h_dm_plus");   h_dm_plus_tmp->SetTitle("h_dm_plus");                                                   
    h_dm_minus_tmp->SetName("h_dm_minus");   h_dm_minus_tmp->SetTitle("h_dm_minus");                                               
    h_dm_plus_tmp->Write();                                                                                                            
    h_dm_minus_tmp->Write();                                                                          
                         
    TH2D * h_sPi_pzpx_fidCut_plus = (TH2D*)inputTree->GetCurrentFile()->Get("h_sPi_pzpx_fidCut_plus");
    h_sPi_pzpx_fidCut_plus->Write();
    TH2D * h_sPi_pzpx_nofidCut_plus = (TH2D*)inputTree->GetCurrentFile()->Get("h_sPi_pzpx_nofidCut_plus");
    h_sPi_pzpx_nofidCut_plus->Write();
   TH2D * h_sPi_pzpx_fidCut_minus = (TH2D*)inputTree->GetCurrentFile()->Get("h_sPi_pzpx_fidCut_minus");
    h_sPi_pzpx_fidCut_minus->Write();
    TH2D * h_sPi_pzpx_nofidCut_minus = (TH2D*)inputTree->GetCurrentFile()->Get("h_sPi_pzpx_nofidCut_minus");
    h_sPi_pzpx_nofidCut_minus->Write();
    TH2D * h_sPi_pzpx_Removed_plus = (TH2D*)inputTree->GetCurrentFile()->Get("h_sPi_pzpx_Removed_plus");
    h_sPi_pzpx_Removed_plus->Write();
    TH2D * h_sPi_pzpx_Removed_minus = (TH2D*)inputTree->GetCurrentFile()->Get("h_sPi_pzpx_Removed_minus");
    h_sPi_pzpx_Removed_minus->Write();
  
    outmc->Close();                                                                                                                    
                                                                                                                                       
    // outputFile.ReplaceAll("_withMultCandRemoval.root","_noMultCandRemoval.root");                                             
    outputFile.ReplaceAll(".root","_noMultCandRemoval.root");                                                      
    system("rm "+outputFile);                                                                            
  }

  
}
