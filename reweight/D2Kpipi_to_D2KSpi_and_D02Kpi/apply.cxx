#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void apply() {

  myH * h_Dp_PTvsETA = new myH("weights/test_16_Dw_0.myH");
  myH * h_Dp_PHI = new myH("weights/test_16_Dw_1.myH");
  myH * h_hp_PTvsETA = new myH("weights/test_16_Dw_2.myH");
  myH * h_hp_PHI = new myH("weights/test_16_Dw_3.myH");
  
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_16_Dw_tmp.root");
  
  double Dplus_PT;
  toweight_tree->SetBranchAddress("Dplus_PT",&Dplus_PT);
  double Dplus_ETA;
  toweight_tree->SetBranchAddress("Dplus_ETA",&Dplus_ETA);
  double Dplus_PHI;
  toweight_tree->SetBranchAddress("Dplus_PHI",&Dplus_PHI);
  double hplus_PT;
  toweight_tree->SetBranchAddress("hplus_PT",&hplus_PT);
  double hplus_ETA;
  toweight_tree->SetBranchAddress("hplus_ETA",&hplus_ETA);
  double hplus_PHI;
  toweight_tree->SetBranchAddress("hplus_PHI",&hplus_PHI);

  double piplus_PT;
  toweight_tree->SetBranchAddress("piplus_PT",&piplus_PT);
  double piplus_ETA;
  toweight_tree->SetBranchAddress("piplus_ETA",&piplus_ETA);
  double piplus_PHI;
  toweight_tree->SetBranchAddress("piplus_PHI",&piplus_PHI);
  double Kminus_PT;
  toweight_tree->SetBranchAddress("Kminus_PT",&Kminus_PT);
  double Kminus_ETA;
  toweight_tree->SetBranchAddress("Kminus_ETA",&Kminus_ETA);
  double Kminus_PHI;
  toweight_tree->SetBranchAddress("Kminus_PHI",&Kminus_PHI);
 
  double Dplus_M;
  toweight_tree->SetBranchAddress("Dplus_M",&Dplus_M);
  int Dplus_ID;
  toweight_tree->SetBranchAddress("Dplus_ID",&Dplus_ID);
  
  

  TFile *f = TFile::Open("rew.root","RECREATE");
  TTree * t = new TTree("ntp","ntp");
  double Iw;
  t->Branch("Iw",&Iw);
  t->Branch("Dplus_PT",&Dplus_PT);
  t->Branch("Dplus_ETA",&Dplus_ETA);
  t->Branch("Dplus_PHI",&Dplus_PHI);
  t->Branch("hplus_PT",&hplus_PT);
  t->Branch("hplus_ETA",&hplus_ETA);
  t->Branch("hplus_PHI",&hplus_PHI);
  t->Branch("Kminus_PT",&Kminus_PT);
  t->Branch("Kminus_ETA",&Kminus_ETA);
  t->Branch("Kminus_PHI",&Kminus_PHI);
  t->Branch("piplus_PT",&piplus_PT);
  t->Branch("piplus_ETA",&piplus_ETA);
  t->Branch("piplus_PHI",&piplus_PHI);
  t->Branch("Dplus_M",&Dplus_M);
  t->Branch("Dplus_ID",&Dplus_ID);

  double HarmCut_post = 0;
  t->Branch("HarmCut_post",&HarmCut_post);

  TH1D * h_mass_plus = new TH1D("h_mass_plus", "h_mass_plus", 500, 1800, 1930);
  TH1D * h_mass_minus = new TH1D("h_mass_minus", "h_mass_minus", 500, 1800, 1930);
  vector <double> values;
  for(Long64_t i = 0; i < 
	30e6;
      //toweight_tree->GetEntries();
      i++){
    toweight_tree->GetEntry(i);

    Iw = h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({Dplus_PT,Dplus_ETA,hplus_PT}));
    Iw *=  h_Dp_PHI->getBC(h_Dp_PHI->find({hplus_PT,hplus_ETA,Dplus_PT}));
    Iw *= h_hp_PTvsETA->getBC(h_hp_PTvsETA->find({Dplus_PT,Dplus_PHI,hplus_PT}));
    Iw *= h_hp_PHI->getBC(h_hp_PHI->find({hplus_PT,hplus_PHI,Dplus_PT}));
    if (Iw > 100)
      Iw = 100;
  
    
    HarmCut_post = 1;

    if (Kminus_PT<-piplus_PT+2.5e3 || Kminus_PT>-piplus_PT+7.7e3)
      HarmCut_post = 0;
    
    if(Dplus_ID>0) {
      h_mass_plus->Fill(Dplus_M,Iw);
    }
    else {
      h_mass_minus->Fill(Dplus_M,Iw);
    }
    
    t->Fill();
  }
  
  f->Write();
  f->Close();
  
}
