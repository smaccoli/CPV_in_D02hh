#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

int main() {

  TString year = "16";
  TString polarity = "Dw";
  int i_set = 0;
  int N_set = 5;

  dcastyle();
  
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_16_Dw_tmp.root");
  toweight_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_16_Dw_tmp_SPlot.root");
 
  toweight_tree->SetAlias("P2_PT","Kminus_PT");
  toweight_tree->GetLeaf("Kminus_PT")->SetName("P2_PT");
  toweight_tree->SetAlias("P2_ETA","Kminus_ETA");
  toweight_tree->GetLeaf("Kminus_ETA")->SetName("P2_ETA");
  toweight_tree->SetAlias("P2_PHI","Kminus_PHI");
  toweight_tree->GetLeaf("Kminus_PHI")->SetName("P2_PHI");
  toweight_tree->SetAlias("P1_PT","piplus_PT");
  toweight_tree->GetLeaf("piplus_PT")->SetName("P1_PT");
  toweight_tree->SetAlias("P1_ETA","piplus_ETA");
  toweight_tree->GetLeaf("piplus_ETA")->SetName("P1_ETA");
  toweight_tree->SetAlias("P1_PHI","piplus_PHI");
  toweight_tree->GetLeaf("piplus_PHI")->SetName("P1_PHI");

  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2KS0pipLL_16_Dw_tmp.root");
  target_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2KS0pipLL_16_Dw_tmp_SPlot.root");

  TChain* target2_tree = new TChain("ntp","ntp");
  target2_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+"_TIS.root"); 
  target2_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+"_TIS_SPlot.root"); 
  target2_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+"_TIS_addHarmCut.root"); 
 
  IDecay* toweight_decay = new IDecay(toweight_tree,"Dp2Kmpippip","D^{+} #rightarrow K^{#minus} #pi^{+} h^{+}",{"N_sigD_sw"/*,"HarmCut"*/},20e6/*toweight_tree->GetEntries()*/,0);//20*target
  IDecay* target_decay = new IDecay(target_tree,"Dp2KS0pip","D^{+} #rightarrow K_{S}^{0} h^{+}",{"N_sigD_sw"},target_tree->GetEntries(),0);
  IDecay* target2_decay = new IDecay(target2_tree,"D02Kmpip","D^{0} #rightarrow K^{#minus} #pi^{+}",{"N_S_sw","HarmCut"},((size_t)(N_set/N_set*target2_tree->GetEntries())),((size_t)(((double) i_set)/N_set*target2_tree->GetEntries())));

  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(100,3.2e3,14e3)","Dplus_ETA(100,2,4.4)","hplus_PT(100,1.5e3,8.5e3)"},2);//5
  toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(100,1.5e3,8.5e3)","hplus_ETA(100,1.9,4.5)","Dplus_PT(100,3.2e3,14e3)"},2);//5
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(100,3.2e3,14e3)","Dplus_PHI(100,-3.1416,3.1416)","hplus_PT(100,1.5e3,8.5e3)"},2);//5
  toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(100,1.5e3,8.5e3)","hplus_PHI(100,-3.1416,3.1416)","Dplus_PT(100,3.2e3,14e3)"},2);//5
 /*
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PHI(100,-3.1416,3.1416)"},15);
  toweight_decay->addTargetDistribution(target_decay,{"hplus_PHI(100,-3.1416,3.1416)"},15);
 */
  /*
  toweight_decay->addTargetDistribution(target2_decay,{"P1_PT(100,8e2,5e3)","P1_ETA(100,2,4.7)"},15);//15
  toweight_decay->addTargetDistribution(target2_decay,{"P1_PHI(100,-3.1416,3.1416)"},5);
  toweight_decay->addTargetDistribution(target2_decay,{"P2_PT(100,8e2,6e3)","P2_ETA(100,2,4.7)"},15);//15
  toweight_decay->addTargetDistribution(target2_decay,{"P2_PHI(100,-3.1416,3.1416)"},5);
  */
 
  toweight_decay->Iw_max = 100;
  toweight_decay->Iw_max_value = 100;
  toweight_decay->chi2_Ndof_max = 0.1;
  toweight_decay->outFileName = "weights/test_16_Dw";
  toweight_decay->IWeight();
  
  return 0;
}
