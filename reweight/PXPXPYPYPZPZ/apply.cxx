#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void apply() {

  myH * h_PTvsPT = new myH("29_0.myH");
  myH * h_PXvsPX = new myH("29_1.myH");
  myH * h_PYvsPY = new myH("29_2.myH");
  myH * h_PZvsPZ = new myH("29_3.myH");
  
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/forAngelo/tmp_Dp2Kmpippip.root");
  
  double Dplus_PT;
  toweight_tree->SetBranchAddress("Dplus_PT",&Dplus_PT);
  double Dplus_PX;
  toweight_tree->SetBranchAddress("Dplus_PX",&Dplus_PX);
  double Dplus_PY;
  toweight_tree->SetBranchAddress("Dplus_PY",&Dplus_PY);
  double Dplus_PZ;
  toweight_tree->SetBranchAddress("Dplus_PZ",&Dplus_PZ);
  double hplus_PT;
  toweight_tree->SetBranchAddress("hplus_PT",&hplus_PT);
  double hplus_PX;
  toweight_tree->SetBranchAddress("hplus_PX",&hplus_PX);
  double hplus_PY;
  toweight_tree->SetBranchAddress("hplus_PY",&hplus_PY);
  double hplus_PZ;
  toweight_tree->SetBranchAddress("hplus_PZ",&hplus_PZ);
 
  
  TFile *f = TFile::Open("rew.root","RECREATE");
  TTree * t = new TTree("ntp","ntp");
  double Iw;
  t->Branch("Iw",&Iw);

  vector <double> values;
  for(Long64_t i = 0; i < toweight_tree->GetEntries(); i++){
    toweight_tree->GetEntry(i);
    
    // cout << Dplus_PX << " " << Dplus_PY << " " << Dplus_PZ << " ";
    // cout << hplus_PX << " " << hplus_PY << " " << hplus_PZ << endl;
    
    Iw = h_PTvsPT->getBC(h_PTvsPT->find({Dplus_PT,hplus_PT}));
    Iw *= h_PXvsPX->getBC(h_PXvsPX->find({Dplus_PX,hplus_PX}));
    Iw *=  h_PYvsPY->getBC(h_PYvsPY->find({Dplus_PY,hplus_PY}));
    Iw *= h_PZvsPZ->getBC(h_PZvsPZ->find({Dplus_PZ,hplus_PZ}));
    if (Iw > 50)
      Iw = 0;
    
    t->Fill();
  }
  
  f->Write();
  f->Close();
  
}
