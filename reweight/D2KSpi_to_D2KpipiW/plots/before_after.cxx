#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void before_after() {
  dcastyle();

  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_16_Dw.root");
  target_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_16_Dw_SPlot.root");
   target_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_16_Dw_addHarmCut.root");
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2KS0pipLL_16_Dw.root");
  toweight_tree->AddFriend("ntp","../rew.root");
  toweight_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2KS0pipLL_16_Dw_SPlot.root");
  toweight_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2KS0pipLL_16_Dw_addHarmCut.root");
 
  IDecay* target_decay = new IDecay(target_tree,"Dp2Kmpippip","D^{+} #rightarrow K^{-} #pi^{+} h^{+}",{"N_sigD_sw","HarmCut"},20e6/*target_tree->GetEntries()*/,0);
  IDecay* toweight_decay = new IDecay(toweight_tree,"Dp2KS0pip","D^{+} #rightarrow K_{S}^{0} h^{+}",{"N_sigD_sw","HarmCut"},toweight_tree->GetEntries(),0);

  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(100,4e3,14e3)","Dplus_ETA(100,2,4.3)"},4);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PHI(100,-3.1416,3.1416)"},15);
  toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(100,1.5e3,8.5e3)","hplus_ETA(100,1.9,4.5)"},4);
  toweight_decay->addTargetDistribution(target_decay,{"hplus_PHI(100,-3.1416,3.1416)"},15);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PY(100,-14e3,14e3)","hplus_PY(100,-8.5e3,8.5e3)"},5);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PX(100,-14e3,14e3)","hplus_PX(100,-8.5e3,8.5e3)"},5);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PZ(100,15e3,250e3)","hplus_PZ(100,5e3,150e3)"},5);
  
  toweight_decay->fillTargets();

  toweight_decay->setIWeight("Iw"); //from rew.root

  toweight_decay->showVariables();
}
