#include "/home/LHCB/smaccoli/IWeight/IDecay.cxx"

int main(int argc, char * argv[]) {

  bool isPGun = 1;
  bool use_ALL = 0;
  TString add_string = "";
  
  if (isPGun) {
    add_string += "_pGun";
    if (use_ALL)
      add_string += "_ALL";
  } 

  dcastyle();
  TString year = argv[1];
  TString polarity = argv[2];
  int i_set = atoi(argv[3]);
  int N_set = atoi(argv[4]);
  
  TString toweight_fname = "/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+add_string+".root";
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add(toweight_fname); 
  toweight_tree->AddFriend("ntp",toweight_fname.ReplaceAll(".root","_SPlot.root"));
 
  TString target_fname = "/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_"+year+"_"+polarity+add_string+".root";
  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add(target_fname);
  target_tree->AddFriend("ntp",target_fname.ReplaceAll(".root","_SPlot.root"));
  target_tree->AddFriend("ntp",target_fname.ReplaceAll("SPlot","rew"));
  

  vector <string> toweight_input_weights = {"N_S_sw"};
  if(isPGun) toweight_input_weights.clear();
  vector <string> target_input_weights = {"N_sigD_sw","Iw"};
  if(isPGun) target_input_weights = {"trueLabels","Iw"};
    
  int N_set_toweight = 1;
  
  if(isPGun)
    N_set_toweight = N_set/2;
  
  IDecay* toweight_decay = new IDecay(toweight_tree,"D02Kmpip","D^{0} #rightarrow K^{#minus} #pi^{+}",toweight_input_weights,((size_t)(1./N_set_toweight*toweight_tree->GetEntries())),((size_t)(((double) i_set)/N_set*toweight_tree->GetEntries())));
  IDecay* target_decay = new IDecay(target_tree,"Dp2Kmpippip","D^{+} #rightarrow K^{#minus} #pi^{+} h^{+}",target_input_weights,((size_t)(1./N_set*target_tree->GetEntries())),((size_t)(((double) i_set)/N_set*target_tree->GetEntries())));
 
  //toweight_decay->addTargetDistribution(target_decay,{"P1_PT(50,8e2,5.5e3)","P1_ETA(50,2.2,4.3)","P1_PHI(80,-3.1416,3.1416)"},15.0);
  //toweight_decay->addTargetDistribution(target_decay,{"P2_PT(50,8e2,6e3)","P2_ETA(50,2.2,4.3)","P2_PHI(80,-3.1416,3.1416)"},15.0);
 
  if (year != "15") {
    toweight_decay->addTargetDistribution(target_decay,{"P1_PT(50,8e2,5.5e3)","P1_ETA(50,2.2,4.3)"},15.0);
    toweight_decay->addTargetDistribution(target_decay,{"P1_PHI(80,-3.1416,3.1416)"},15.0);
    toweight_decay->addTargetDistribution(target_decay,{"P2_PT(50,8e2,6e3)","P2_ETA(50,2.2,4.3)"},15.0);
    toweight_decay->addTargetDistribution(target_decay,{"P2_PHI(80,-3.1416,3.1416)"},15.0);
  }
  else {
    toweight_decay->addTargetDistribution(target_decay,{"P1_PT(25,8e2,5.5e3)","P1_ETA(25,2.2,4.3)"},15.0);
    toweight_decay->addTargetDistribution(target_decay,{"P1_PHI(80,-3.1416,3.1416)"},15.0);
    toweight_decay->addTargetDistribution(target_decay,{"P2_PT(25,8e2,6e3)","P2_ETA(25,2.2,4.3)"},15.0);
    toweight_decay->addTargetDistribution(target_decay,{"P2_PHI(80,-3.1416,3.1416)"},15.0);    
  }
  toweight_decay->use_Iw_max = 0;
  toweight_decay->Iw_max = 20;
  toweight_decay->Iw_max_value = 20;
  toweight_decay->chi2_Ndof_max = 0.4;
  if (year == "15")
    toweight_decay->chi2_Ndof_max = 0.5;    
  toweight_decay->outFileName = "/home/LHCB-T3/smaccoli/CPV_in_D02hh/reweight/D02Kpi_to_D2KpipiW/weights/"+year+"_"+polarity+add_string;
  toweight_decay->ApplySmoothing = 0;
  toweight_decay->IWeight();
  
  return 0;
}
