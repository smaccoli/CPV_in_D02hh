#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void apply(TString year, TString polarity, TString decay = "D02Kmpip") {

  bool isPGun = 1;
  bool use_ALL = 0;
  TString add_string = "";
  
  if (isPGun) {
    add_string += "_pGun";
    if (use_ALL)
      add_string += "_ALL";
  } 

  myH * h_pip_PTvsETA = new myH("weights/"+year+"_"+polarity+add_string+"_0.myH");
  myH * h_pip_PHI = new myH("weights/"+year+"_"+polarity+add_string+"_1.myH");
  myH * h_Km_PTvsETA = new myH("weights/"+year+"_"+polarity+add_string+"_2.myH");
  myH * h_Km_PHI = new myH("weights/"+year+"_"+polarity+add_string+"_3.myH");
  
  TString fname = "/home/LHCB/smaccoli/CPV_in_D02hh/data/"+decay+"_"+year+"_"+polarity+add_string+".root";

  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add(fname);
  
  double P1_PT;
  toweight_tree->SetBranchAddress("P1_PT",&P1_PT);
  double P1_ETA;
  toweight_tree->SetBranchAddress("P1_ETA",&P1_ETA);
  double P1_PHI;
  toweight_tree->SetBranchAddress("P1_PHI",&P1_PHI);
  double P1_PX;
  toweight_tree->SetBranchAddress("P1_PX",&P1_PX);
  double P1_PY;
  toweight_tree->SetBranchAddress("P1_PY",&P1_PY);
  double P1_PZ;
  toweight_tree->SetBranchAddress("P1_PZ",&P1_PZ);
  double P2_PT;
  toweight_tree->SetBranchAddress("P2_PT",&P2_PT);
  double P2_ETA;
  toweight_tree->SetBranchAddress("P2_ETA",&P2_ETA);
  double P2_PHI;
  toweight_tree->SetBranchAddress("P2_PHI",&P2_PHI);
  double P2_PX;
  toweight_tree->SetBranchAddress("P2_PX",&P2_PX);
  double P2_PY;
  toweight_tree->SetBranchAddress("P2_PY",&P2_PY);
  double P2_PZ;
  toweight_tree->SetBranchAddress("P2_PZ",&P2_PZ);
  double D0_PT;
  toweight_tree->SetBranchAddress("D0_PT",&D0_PT);
  double D0_ETA;
  toweight_tree->SetBranchAddress("D0_ETA",&D0_ETA);
  double D0_PHI;
  toweight_tree->SetBranchAddress("D0_PHI",&D0_PHI);

  double DTF_Mass;
  toweight_tree->SetBranchAddress("DTF_Mass",&DTF_Mass);
  int Dst_ID;
  toweight_tree->SetBranchAddress("Dst_ID",&Dst_ID);

 
  double Iw;
  vector <double> values;
  double sumw = 0;
  double Nentries_over_sumw;
  for(Long64_t i = 0; i < 
	toweight_tree->GetEntries()
	; i++){
    toweight_tree->GetEntry(i);
    
    Iw = h_pip_PTvsETA->getBC(h_pip_PTvsETA->find({P1_PT,P1_ETA}));
    Iw *=  h_pip_PHI->getBC(h_pip_PHI->find({P1_PHI}));
    Iw *= h_Km_PTvsETA->getBC(h_Km_PTvsETA->find({P2_PT,P2_ETA}));
    Iw *= h_Km_PHI->getBC(h_Km_PHI->find({P2_PHI}));

    /*
      if (Iw > 15)
      Iw = 0;
    */
    
    sumw += Iw;
    values.push_back(Iw);
  }

  Nentries_over_sumw = ((double)toweight_tree->GetEntries())/sumw;
  cout << Nentries_over_sumw << endl;

  Nentries_over_sumw = 1;
  cout << Nentries_over_sumw << endl;

  
  fname.ReplaceAll(".root","_rew.root");
  
  TFile *f = TFile::Open(fname,"RECREATE");
  TTree * t = new TTree("ntp","ntp");
  t->Branch(decay == "D02Kmpip" ? "Iw": "Iw2",&Iw);
  t->Branch("P1_PT",&P1_PT);
  t->Branch("P1_ETA",&P1_ETA);
  t->Branch("P1_PHI",&P1_PHI);
  t->Branch("P2_PT",&P2_PT);
  t->Branch("P2_ETA",&P2_ETA);
  t->Branch("P2_PHI",&P2_PHI);
  t->Branch("KS0_PT",&D0_PT);
  t->Branch("KS0_ETA",&D0_ETA);
  t->Branch("KS0_PHI",&D0_PHI);
  t->Branch("DTF_Mass",&DTF_Mass);
  t->Branch("Dst_ID",&Dst_ID);

  TH1F * h_dm_plus = new TH1F("h_dm_plus","h_dm_plus",500,2004.5,2020);
  TH1F * h_dm_minus = new TH1F("h_dm_minus","h_dm_minus",500,2004.5,2020);

  for(Long64_t i = 0; i < 
	toweight_tree->GetEntries()
	; i++){
    toweight_tree->GetEntry(i);

    Iw = values.at(i)*Nentries_over_sumw;

    if(Dst_ID>0) {
      h_dm_plus->Fill(DTF_Mass,Iw);
    }
    else {
      h_dm_minus->Fill(DTF_Mass,Iw);
    }
    
    t->Fill();
  }

  f->Write();
  f->Close();

  
}

int main(int argc, char * argv[]) { 
  apply(argv[1], argv[2]); 
  return 0;
}
