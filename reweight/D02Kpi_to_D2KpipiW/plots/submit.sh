#!/bin/bash
g++ -Wall -o _before_after before_after.cxx `root-config --cflags --glibs` `gsl-config --cflags --libs`

DIR=$PWD

for year in $(echo 15 16 17 18); do
    for pol in $(echo Dw Up); do
	# for year in $(echo 18); do
	#     for pol in $(echo Dw); do
	for variable in $(echo X_PT__100__2080__9080__ X_P__100__27000__140000__ P1_PT__100__800__5500__ P1_ETA__100__2.2__4.3__ P1_PHI__100__-3.1416__3.1416__ P2_PT__100__800__6000__ P2_ETA__100__2.2__4.3__ P2_PHI__100__-3.1416__3.1416__ P1_PX__100__-5500__5500__ P2_PX__100__-6000__6000__ P1_PY__100__-5500__5500__ P2_PY__100__-6000__6000__ P1_PZ__100__0__100000__ P2_PZ__100__0__100000__  ); do
	    echo "#!/bin/sh" > exec/$year.$pol.$variable.sh
	    echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> exec/$year.$pol.$variable.sh 
	    echo "cd $DIR" >> exec/$year.$pol.$variable.sh
	    echo "hostname" >> exec/$year.$pol.$variable.sh
	    echo "time ./_before_after $year $pol $variable > logs/log_${year}_${pol}_${variable}.txt" >> exec/$year.$pol.$variable.sh
	    chmod +x exec/$year.$pol.$variable.sh
	    python condor_make_template.py condor_template.sub $year.$pol.$variable
	    
	    condor_submit exec/condor_template.$year.$pol.$variable.sub -batch-name $year$pol$variable

	done		  	
    done
done



#X_PT__100__2080__9080__ X_P__100__27000__140000__ P1_PT__100__800__5500__ P1_ETA__100__2.2__4.3__ P1_PHI__100__-3.1416__3.1416__ P2_PT__100__800__6000__ P2_ETA__100__2.2__4.3__ P2_PHI__100__-3.1416__3.1416__ P1_PX__100__-5500__5500__ P2_PX__100__-6000__6000__ P1_PY__100__-5500__5500__ P2_PY__100__-6000__6000__ P1_PZ__100__0__100000__ P2_PZ__100__0__100000__ 
