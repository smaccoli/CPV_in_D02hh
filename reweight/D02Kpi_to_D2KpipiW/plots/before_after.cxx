#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

string convertToString(const char* a, int size) 
{ 
  int i; 
  string s = ""; 
  for (i = 0; i <= size; i++) { 
    s = s + a[i]; 
  } 
  return s; 
} 

void before_after(TString year = "16", TString polarity = "Dw", string variable = "") {

  TString tmp = variable.c_str();
  tmp.ReplaceAll("__",",");
  variable = convertToString((const char*)tmp, sizeof(tmp));

  dcastyle();

  TString toweight_tree_fname = "/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+".root";
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add(toweight_tree_fname);
  toweight_tree->AddFriend("ntp",toweight_tree_fname.ReplaceAll(".root","_SPlot.root"));
  toweight_tree->AddFriend("ntp",toweight_tree_fname.ReplaceAll("SPlot","rew"));
  
  TString target_tree_fname = "/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_"+year+"_"+polarity+".root";
  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add(target_tree_fname);
  target_tree->AddFriend("ntp",target_tree_fname.ReplaceAll(".root","_SPlot.root"));
  target_tree->AddFriend("ntp",target_tree_fname.ReplaceAll("SPlot","rew"));

  
  IDecay* toweight_decay = new IDecay(toweight_tree,"D02Kmpip","D^{0} #rightarrow K^{#minus} #pi^{+}",{"N_S_sw"},toweight_tree->GetEntries(),0);
  IDecay* target_decay = new IDecay(target_tree,"Dp2Kmpippip","D^{+} #rightarrow K^{#minus} #pi^{+} h^{+}",{"N_sigD_sw","Iw"},target_tree->GetEntries(),0);
 
  if (variable == "") {
    toweight_decay->addTargetDistribution(target_decay,{"X_PT(100,2.2e3,8e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"X_P(100,27e3,140e3)"},5);//15

    toweight_decay->addTargetDistribution(target_decay,{"P1_PT(100,8e2,5e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P1_ETA(100,2.2,4.3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P1_PHI(100,-3.1416,3.1416)"},5);
    toweight_decay->addTargetDistribution(target_decay,{"P2_PT(100,8e2,6e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P2_ETA(100,2.2,4.3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P2_PHI(100,-3.1416,3.1416)"},5);

    toweight_decay->addTargetDistribution(target_decay,{"P1_PX(100,-5e3,5e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P2_PX(100,-6e3,6e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P1_PY(100,-5e3,5e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P2_PY(100,-6e3,6e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P1_PZ(100,0,100e3)"},5);//15
    toweight_decay->addTargetDistribution(target_decay,{"P2_PZ(100,0,100e3)"},5);//15
  }
  else {
    toweight_decay->addTargetDistribution(target_decay,{variable},5);
  }

  toweight_decay->setAsymVariable("D0_ID");
  target_decay->setAsymVariable("Dplus_ID");
  toweight_decay->calculateResidualAsymmetry = 1;

  toweight_decay->fillTargets();

  toweight_decay->use_Iw_max = 0;
  toweight_decay->Iw_max = 15;
  toweight_decay->Iw_max_value = 0;

  toweight_decay->setIWeight("Iw"); //from rew.root

  toweight_decay->plotDir = year+"_"+polarity+"/";
  toweight_decay->showVariables();
}

int main(int argc, char * argv[]) {
  before_after(argv[1],argv[2],argv[3]);
  return 0;
}

