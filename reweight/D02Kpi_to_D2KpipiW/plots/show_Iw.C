#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TString.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include"/home/LHCB/smaccoli/dcastyle.C"
#include "TROOT.h"
#include "TInterpreter.h"
#include "Riostream.h"

using namespace std;

void show_Iw(TString year = "18", TString polarity = "Dw") {
 
  dcastyle();
  
  TH1::SetDefaultSumw2();
  Int_t N = 100;
  TH1D * h = new TH1D("h","h",N,0,15.1);
  TString inFileName = "/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+".root";
  inFileName.ReplaceAll(".root","_rew.root");

  TChain *tree = new TChain("ntp");
  tree->Add(inFileName);
  tree->SetBranchStatus("*",0);
  tree->SetBranchStatus("Iw",1);
  tree->SetBranchStatus("*sw*",1);

  double Iw;
  tree->SetBranchAddress("Iw",&Iw);

  double Nsig_sw;
  tree->SetBranchAddress("Nsig_sw",&Nsig_sw);

  Long64_t nentries = tree->GetEntries();
  for (Long64_t i = 0; i < 
	 //1e6
	 nentries
	 ; i++) {
    tree->GetEntry(i);

    h->Fill(Iw/*,Nsig_sw*/);
  }
  
  h->GetXaxis()->SetTitle("#it{w}");
  h->GetYaxis()->SetTitle("Candidates");
  h->GetXaxis()->SetTitleOffset(1.2);
  h->GetYaxis()->SetTitleOffset(1.2);
  h->SetLineColor(kBlue);


  TCanvas* Canvas = new TCanvas("Iw","Iw",50,50, 1000,700);
  //Canvas->SetRightMargin(0.15);
  gPad->SetLogy(1);
  gPad->SetLogy(0);

  h->Draw("histo");
 
  Canvas->SaveAs(year+"_"+polarity+"/Iw.C");
  Canvas->SaveAs(year+"_"+polarity+"/Iw.pdf");

}

