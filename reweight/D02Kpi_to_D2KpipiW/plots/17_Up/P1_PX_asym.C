void P1_PX_asym()
{
//=========Macro generated from canvas: P1_PX_asym/P1_PX_asym
//=========  (Mon Nov 18 18:16:29 2019) by ROOT version6.08/02
   TCanvas *P1_PX_asym = new TCanvas("P1_PX_asym", "P1_PX_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   P1_PX_asym->SetHighLightColor(2);
   P1_PX_asym->Range(0,0,1,1);
   P1_PX_asym->SetFillColor(0);
   P1_PX_asym->SetBorderMode(0);
   P1_PX_asym->SetBorderSize(10);
   P1_PX_asym->SetTickx(1);
   P1_PX_asym->SetTicky(1);
   P1_PX_asym->SetLeftMargin(0.18);
   P1_PX_asym->SetRightMargin(0.05);
   P1_PX_asym->SetTopMargin(0.07);
   P1_PX_asym->SetBottomMargin(0.16);
   P1_PX_asym->SetFrameFillStyle(0);
   P1_PX_asym->SetFrameLineStyle(0);
   P1_PX_asym->SetFrameLineColor(0);
   P1_PX_asym->SetFrameLineWidth(0);
   P1_PX_asym->SetFrameBorderMode(0);
   P1_PX_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(-7562.5,-0.001561146,6187.5,0.001188353);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,-5500,5500);
   h1D_target_minus_toweight__8->SetBinContent(1,6.619409e-06);
   h1D_target_minus_toweight__8->SetBinContent(2,8.435871e-06);
   h1D_target_minus_toweight__8->SetBinContent(3,1.192919e-05);
   h1D_target_minus_toweight__8->SetBinContent(4,1.78656e-05);
   h1D_target_minus_toweight__8->SetBinContent(5,2.339873e-05);
   h1D_target_minus_toweight__8->SetBinContent(6,1.826863e-05);
   h1D_target_minus_toweight__8->SetBinContent(7,2.855538e-05);
   h1D_target_minus_toweight__8->SetBinContent(8,3.373857e-05);
   h1D_target_minus_toweight__8->SetBinContent(9,4.155035e-05);
   h1D_target_minus_toweight__8->SetBinContent(10,4.722309e-05);
   h1D_target_minus_toweight__8->SetBinContent(11,3.855399e-05);
   h1D_target_minus_toweight__8->SetBinContent(12,5.216597e-05);
   h1D_target_minus_toweight__8->SetBinContent(13,6.728011e-05);
   h1D_target_minus_toweight__8->SetBinContent(14,6.703759e-05);
   h1D_target_minus_toweight__8->SetBinContent(15,6.729725e-05);
   h1D_target_minus_toweight__8->SetBinContent(16,8.03505e-05);
   h1D_target_minus_toweight__8->SetBinContent(17,5.836401e-05);
   h1D_target_minus_toweight__8->SetBinContent(18,6.505451e-05);
   h1D_target_minus_toweight__8->SetBinContent(19,4.684576e-05);
   h1D_target_minus_toweight__8->SetBinContent(20,5.140074e-05);
   h1D_target_minus_toweight__8->SetBinContent(21,5.993608e-05);
   h1D_target_minus_toweight__8->SetBinContent(22,5.764863e-05);
   h1D_target_minus_toweight__8->SetBinContent(23,2.794014e-05);
   h1D_target_minus_toweight__8->SetBinContent(24,7.28108e-06);
   h1D_target_minus_toweight__8->SetBinContent(25,4.409556e-05);
   h1D_target_minus_toweight__8->SetBinContent(26,-6.829621e-05);
   h1D_target_minus_toweight__8->SetBinContent(27,-7.616263e-05);
   h1D_target_minus_toweight__8->SetBinContent(28,-0.0001370525);
   h1D_target_minus_toweight__8->SetBinContent(29,-0.0001458549);
   h1D_target_minus_toweight__8->SetBinContent(30,-0.0001842533);
   h1D_target_minus_toweight__8->SetBinContent(31,-0.0002295724);
   h1D_target_minus_toweight__8->SetBinContent(32,-0.0002504578);
   h1D_target_minus_toweight__8->SetBinContent(33,-0.0002577109);
   h1D_target_minus_toweight__8->SetBinContent(34,-0.0002682107);
   h1D_target_minus_toweight__8->SetBinContent(35,-0.0004614573);
   h1D_target_minus_toweight__8->SetBinContent(36,-0.00051839);
   h1D_target_minus_toweight__8->SetBinContent(37,-0.0005163811);
   h1D_target_minus_toweight__8->SetBinContent(38,-0.0003628638);
   h1D_target_minus_toweight__8->SetBinContent(39,-0.0004089922);
   h1D_target_minus_toweight__8->SetBinContent(40,-4.867837e-05);
   h1D_target_minus_toweight__8->SetBinContent(41,0.0001063868);
   h1D_target_minus_toweight__8->SetBinContent(42,0.0005644523);
   h1D_target_minus_toweight__8->SetBinContent(43,0.0008485541);
   h1D_target_minus_toweight__8->SetBinContent(44,0.0003603213);
   h1D_target_minus_toweight__8->SetBinContent(45,0.0005425308);
   h1D_target_minus_toweight__8->SetBinContent(46,0.0005827844);
   h1D_target_minus_toweight__8->SetBinContent(47,0.0002292916);
   h1D_target_minus_toweight__8->SetBinContent(48,0.0001369305);
   h1D_target_minus_toweight__8->SetBinContent(49,-2.645701e-05);
   h1D_target_minus_toweight__8->SetBinContent(50,6.447919e-05);
   h1D_target_minus_toweight__8->SetBinContent(51,-7.770956e-06);
   h1D_target_minus_toweight__8->SetBinContent(52,-0.0001761634);
   h1D_target_minus_toweight__8->SetBinContent(53,-2.976879e-05);
   h1D_target_minus_toweight__8->SetBinContent(54,0.000180725);
   h1D_target_minus_toweight__8->SetBinContent(55,0.0001851879);
   h1D_target_minus_toweight__8->SetBinContent(56,0.0005638953);
   h1D_target_minus_toweight__8->SetBinContent(57,0.001096703);
   h1D_target_minus_toweight__8->SetBinContent(58,0.0004986264);
   h1D_target_minus_toweight__8->SetBinContent(59,6.891415e-05);
   h1D_target_minus_toweight__8->SetBinContent(60,-0.0001552925);
   h1D_target_minus_toweight__8->SetBinContent(61,8.898228e-05);
   h1D_target_minus_toweight__8->SetBinContent(62,-0.0003668256);
   h1D_target_minus_toweight__8->SetBinContent(63,-0.0004717745);
   h1D_target_minus_toweight__8->SetBinContent(64,-0.0006490108);
   h1D_target_minus_toweight__8->SetBinContent(65,-0.0005579442);
   h1D_target_minus_toweight__8->SetBinContent(66,-0.0004714848);
   h1D_target_minus_toweight__8->SetBinContent(67,-0.0002982561);
   h1D_target_minus_toweight__8->SetBinContent(68,-0.0003224676);
   h1D_target_minus_toweight__8->SetBinContent(69,-0.0001936015);
   h1D_target_minus_toweight__8->SetBinContent(70,-0.000159028);
   h1D_target_minus_toweight__8->SetBinContent(71,-0.0001814417);
   h1D_target_minus_toweight__8->SetBinContent(72,-0.0001739324);
   h1D_target_minus_toweight__8->SetBinContent(73,-7.694541e-05);
   h1D_target_minus_toweight__8->SetBinContent(74,-6.192224e-05);
   h1D_target_minus_toweight__8->SetBinContent(75,-9.944197e-06);
   h1D_target_minus_toweight__8->SetBinContent(76,-3.811275e-05);
   h1D_target_minus_toweight__8->SetBinContent(77,8.777087e-05);
   h1D_target_minus_toweight__8->SetBinContent(78,6.097229e-05);
   h1D_target_minus_toweight__8->SetBinContent(79,0.0001014059);
   h1D_target_minus_toweight__8->SetBinContent(80,5.904352e-05);
   h1D_target_minus_toweight__8->SetBinContent(81,7.09123e-05);
   h1D_target_minus_toweight__8->SetBinContent(82,6.059324e-05);
   h1D_target_minus_toweight__8->SetBinContent(83,7.100485e-05);
   h1D_target_minus_toweight__8->SetBinContent(84,6.92436e-05);
   h1D_target_minus_toweight__8->SetBinContent(85,7.160369e-05);
   h1D_target_minus_toweight__8->SetBinContent(86,5.241414e-05);
   h1D_target_minus_toweight__8->SetBinContent(87,7.520209e-05);
   h1D_target_minus_toweight__8->SetBinContent(88,4.839012e-05);
   h1D_target_minus_toweight__8->SetBinContent(89,4.3493e-05);
   h1D_target_minus_toweight__8->SetBinContent(90,4.1641e-05);
   h1D_target_minus_toweight__8->SetBinContent(91,4.703877e-05);
   h1D_target_minus_toweight__8->SetBinContent(92,5.711298e-05);
   h1D_target_minus_toweight__8->SetBinContent(93,3.50535e-05);
   h1D_target_minus_toweight__8->SetBinContent(94,3.130759e-05);
   h1D_target_minus_toweight__8->SetBinContent(95,3.05271e-05);
   h1D_target_minus_toweight__8->SetBinContent(96,2.713375e-05);
   h1D_target_minus_toweight__8->SetBinContent(97,2.28578e-05);
   h1D_target_minus_toweight__8->SetBinContent(98,2.362463e-05);
   h1D_target_minus_toweight__8->SetBinContent(99,1.558487e-05);
   h1D_target_minus_toweight__8->SetBinContent(100,1.093813e-05);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("P1_PX");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{+} #rightarrow K^{#minus} #pi^{+} h^{+})#minusN(D^{0} #rightarrow K^{#minus} #pi^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",-5500,5500);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   P1_PX_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(-7562.5,-0.171574,6187.5,0.1840686);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,-5500,5500);
   h1D_asym__9->SetBinContent(1,-0.06746514);
   h1D_asym__9->SetBinContent(2,-0.0995665);
   h1D_asym__9->SetBinContent(3,-0.04964117);
   h1D_asym__9->SetBinContent(4,-0.07011501);
   h1D_asym__9->SetBinContent(5,-0.05270581);
   h1D_asym__9->SetBinContent(6,-0.07055514);
   h1D_asym__9->SetBinContent(7,-0.06463921);
   h1D_asym__9->SetBinContent(8,-0.05569161);
   h1D_asym__9->SetBinContent(9,-0.05049539);
   h1D_asym__9->SetBinContent(10,-0.05528155);
   h1D_asym__9->SetBinContent(11,-0.04518814);
   h1D_asym__9->SetBinContent(12,-0.06029075);
   h1D_asym__9->SetBinContent(13,-0.05314198);
   h1D_asym__9->SetBinContent(14,-0.03351542);
   h1D_asym__9->SetBinContent(15,-0.04576977);
   h1D_asym__9->SetBinContent(16,-0.04222507);
   h1D_asym__9->SetBinContent(17,-0.04158899);
   h1D_asym__9->SetBinContent(18,-0.04232835);
   h1D_asym__9->SetBinContent(19,-0.03848426);
   h1D_asym__9->SetBinContent(20,-0.02924912);
   h1D_asym__9->SetBinContent(21,-0.02013917);
   h1D_asym__9->SetBinContent(22,-0.02628872);
   h1D_asym__9->SetBinContent(23,-0.02031657);
   h1D_asym__9->SetBinContent(24,-0.01700512);
   h1D_asym__9->SetBinContent(25,-0.01883038);
   h1D_asym__9->SetBinContent(26,-0.00919032);
   h1D_asym__9->SetBinContent(27,-0.007873149);
   h1D_asym__9->SetBinContent(28,-0.007869842);
   h1D_asym__9->SetBinContent(29,-0.006958921);
   h1D_asym__9->SetBinContent(30,-0.002707743);
   h1D_asym__9->SetBinContent(31,-0.003872602);
   h1D_asym__9->SetBinContent(32,-0.008523244);
   h1D_asym__9->SetBinContent(33,-0.01268179);
   h1D_asym__9->SetBinContent(34,-0.01047198);
   h1D_asym__9->SetBinContent(35,-0.01618579);
   h1D_asym__9->SetBinContent(36,-0.01324515);
   h1D_asym__9->SetBinContent(37,-0.01638095);
   h1D_asym__9->SetBinContent(38,-0.01516211);
   h1D_asym__9->SetBinContent(39,-0.01031332);
   h1D_asym__9->SetBinContent(40,-0.008943511);
   h1D_asym__9->SetBinContent(41,-0.01112007);
   h1D_asym__9->SetBinContent(42,-0.005706913);
   h1D_asym__9->SetBinContent(43,-0.002050661);
   h1D_asym__9->SetBinContent(44,0.001638759);
   h1D_asym__9->SetBinContent(45,-0.008424671);
   h1D_asym__9->SetBinContent(46,-0.02195736);
   h1D_asym__9->SetBinContent(47,-0.01905909);
   h1D_asym__9->SetBinContent(48,-0.01836072);
   h1D_asym__9->SetBinContent(49,-0.01750348);
   h1D_asym__9->SetBinContent(50,-0.01837722);
   h1D_asym__9->SetBinContent(51,-0.02776868);
   h1D_asym__9->SetBinContent(52,-0.02202271);
   h1D_asym__9->SetBinContent(53,-0.02494144);
   h1D_asym__9->SetBinContent(54,-0.02122451);
   h1D_asym__9->SetBinContent(55,-0.01801788);
   h1D_asym__9->SetBinContent(56,-0.02799901);
   h1D_asym__9->SetBinContent(57,-0.03375036);
   h1D_asym__9->SetBinContent(58,-0.03018018);
   h1D_asym__9->SetBinContent(59,-0.02265527);
   h1D_asym__9->SetBinContent(60,-0.020377);
   h1D_asym__9->SetBinContent(61,-0.02557739);
   h1D_asym__9->SetBinContent(62,-0.02616582);
   h1D_asym__9->SetBinContent(63,-0.02542472);
   h1D_asym__9->SetBinContent(64,-0.02298407);
   h1D_asym__9->SetBinContent(65,-0.02041079);
   h1D_asym__9->SetBinContent(66,-0.02763213);
   h1D_asym__9->SetBinContent(67,-0.02408575);
   h1D_asym__9->SetBinContent(68,-0.03006309);
   h1D_asym__9->SetBinContent(69,-0.02685226);
   h1D_asym__9->SetBinContent(70,-0.02599132);
   h1D_asym__9->SetBinContent(71,-0.02203718);
   h1D_asym__9->SetBinContent(72,-0.02296222);
   h1D_asym__9->SetBinContent(73,-0.02185804);
   h1D_asym__9->SetBinContent(74,-0.02387538);
   h1D_asym__9->SetBinContent(75,-0.02493251);
   h1D_asym__9->SetBinContent(76,-0.02148508);
   h1D_asym__9->SetBinContent(77,-0.01837924);
   h1D_asym__9->SetBinContent(78,-0.01296117);
   h1D_asym__9->SetBinContent(79,-0.01291253);
   h1D_asym__9->SetBinContent(80,-0.009116057);
   h1D_asym__9->SetBinContent(81,-0.01149945);
   h1D_asym__9->SetBinContent(82,-0.002953788);
   h1D_asym__9->SetBinContent(83,-0.007681589);
   h1D_asym__9->SetBinContent(84,0.002891815);
   h1D_asym__9->SetBinContent(85,0.01412749);
   h1D_asym__9->SetBinContent(86,0.007661311);
   h1D_asym__9->SetBinContent(87,0.007077156);
   h1D_asym__9->SetBinContent(88,0.007611353);
   h1D_asym__9->SetBinContent(89,0.01050497);
   h1D_asym__9->SetBinContent(90,0.01906333);
   h1D_asym__9->SetBinContent(91,0.02068824);
   h1D_asym__9->SetBinContent(92,0.04247846);
   h1D_asym__9->SetBinContent(93,0.03131957);
   h1D_asym__9->SetBinContent(94,0.01734499);
   h1D_asym__9->SetBinContent(95,0.0413993);
   h1D_asym__9->SetBinContent(96,0.06795598);
   h1D_asym__9->SetBinContent(97,0.04721808);
   h1D_asym__9->SetBinContent(98,0.05743379);
   h1D_asym__9->SetBinContent(99,0.01336828);
   h1D_asym__9->SetBinContent(100,0.05460924);
   h1D_asym__9->SetBinError(1,0.08959283);
   h1D_asym__9->SetBinError(2,0.04795542);
   h1D_asym__9->SetBinError(3,0.06367553);
   h1D_asym__9->SetBinError(4,0.03442361);
   h1D_asym__9->SetBinError(5,0.03194752);
   h1D_asym__9->SetBinError(6,0.02824602);
   h1D_asym__9->SetBinError(7,0.02507439);
   h1D_asym__9->SetBinError(8,0.02307991);
   h1D_asym__9->SetBinError(9,0.02119621);
   h1D_asym__9->SetBinError(10,0.01859489);
   h1D_asym__9->SetBinError(11,0.01836174);
   h1D_asym__9->SetBinError(12,0.01566443);
   h1D_asym__9->SetBinError(13,0.0151982);
   h1D_asym__9->SetBinError(14,0.0144891);
   h1D_asym__9->SetBinError(15,0.01271457);
   h1D_asym__9->SetBinError(16,0.01457576);
   h1D_asym__9->SetBinError(17,0.01199682);
   h1D_asym__9->SetBinError(18,0.01059487);
   h1D_asym__9->SetBinError(19,0.01123972);
   h1D_asym__9->SetBinError(20,0.01042315);
   h1D_asym__9->SetBinError(21,0.01441603);
   h1D_asym__9->SetBinError(22,0.008548835);
   h1D_asym__9->SetBinError(23,0.00898066);
   h1D_asym__9->SetBinError(24,0.00808519);
   h1D_asym__9->SetBinError(25,0.008457762);
   h1D_asym__9->SetBinError(26,0.007454909);
   h1D_asym__9->SetBinError(27,0.006527732);
   h1D_asym__9->SetBinError(28,0.007288394);
   h1D_asym__9->SetBinError(29,0.005726184);
   h1D_asym__9->SetBinError(30,0.005809444);
   h1D_asym__9->SetBinError(31,0.005330788);
   h1D_asym__9->SetBinError(32,0.00485755);
   h1D_asym__9->SetBinError(33,0.004574549);
   h1D_asym__9->SetBinError(34,0.004478162);
   h1D_asym__9->SetBinError(35,0.004218072);
   h1D_asym__9->SetBinError(36,0.003964454);
   h1D_asym__9->SetBinError(37,0.003578784);
   h1D_asym__9->SetBinError(38,0.003403293);
   h1D_asym__9->SetBinError(39,0.003288357);
   h1D_asym__9->SetBinError(40,0.00314372);
   h1D_asym__9->SetBinError(41,0.003000131);
   h1D_asym__9->SetBinError(42,0.002955497);
   h1D_asym__9->SetBinError(43,0.002964936);
   h1D_asym__9->SetBinError(44,0.003262695);
   h1D_asym__9->SetBinError(45,0.003434446);
   h1D_asym__9->SetBinError(46,0.00348392);
   h1D_asym__9->SetBinError(47,0.003656122);
   h1D_asym__9->SetBinError(48,0.003832172);
   h1D_asym__9->SetBinError(49,0.004560254);
   h1D_asym__9->SetBinError(50,0.004219175);
   h1D_asym__9->SetBinError(51,0.004223661);
   h1D_asym__9->SetBinError(52,0.003947881);
   h1D_asym__9->SetBinError(53,0.003788226);
   h1D_asym__9->SetBinError(54,0.003931215);
   h1D_asym__9->SetBinError(55,0.003529249);
   h1D_asym__9->SetBinError(56,0.003289358);
   h1D_asym__9->SetBinError(57,0.003090415);
   h1D_asym__9->SetBinError(58,0.002775223);
   h1D_asym__9->SetBinError(59,0.002828788);
   h1D_asym__9->SetBinError(60,0.002913024);
   h1D_asym__9->SetBinError(61,0.002968853);
   h1D_asym__9->SetBinError(62,0.003120725);
   h1D_asym__9->SetBinError(63,0.003253167);
   h1D_asym__9->SetBinError(64,0.003521256);
   h1D_asym__9->SetBinError(65,0.003875701);
   h1D_asym__9->SetBinError(66,0.003949605);
   h1D_asym__9->SetBinError(67,0.004158915);
   h1D_asym__9->SetBinError(68,0.004906149);
   h1D_asym__9->SetBinError(69,0.004510408);
   h1D_asym__9->SetBinError(70,0.004719876);
   h1D_asym__9->SetBinError(71,0.00517632);
   h1D_asym__9->SetBinError(72,0.005206808);
   h1D_asym__9->SetBinError(73,0.005571954);
   h1D_asym__9->SetBinError(74,0.005821178);
   h1D_asym__9->SetBinError(75,0.006053243);
   h1D_asym__9->SetBinError(76,0.006690145);
   h1D_asym__9->SetBinError(77,0.006897472);
   h1D_asym__9->SetBinError(78,0.008663243);
   h1D_asym__9->SetBinError(79,0.008155745);
   h1D_asym__9->SetBinError(80,0.008421427);
   h1D_asym__9->SetBinError(81,0.008940587);
   h1D_asym__9->SetBinError(82,0.01094834);
   h1D_asym__9->SetBinError(83,0.01033081);
   h1D_asym__9->SetBinError(84,0.01110125);
   h1D_asym__9->SetBinError(85,0.01219071);
   h1D_asym__9->SetBinError(86,0.0129439);
   h1D_asym__9->SetBinError(87,0.01388566);
   h1D_asym__9->SetBinError(88,0.01846979);
   h1D_asym__9->SetBinError(89,0.01672154);
   h1D_asym__9->SetBinError(90,0.01830474);
   h1D_asym__9->SetBinError(91,0.0213296);
   h1D_asym__9->SetBinError(92,0.02234916);
   h1D_asym__9->SetBinError(93,0.03522148);
   h1D_asym__9->SetBinError(94,0.02483844);
   h1D_asym__9->SetBinError(95,0.03065645);
   h1D_asym__9->SetBinError(96,0.03518804);
   h1D_asym__9->SetBinError(97,0.03733981);
   h1D_asym__9->SetBinError(98,0.04406887);
   h1D_asym__9->SetBinError(99,0.05503851);
   h1D_asym__9->SetBinError(100,0.07865329);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("P1_PX");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",-5500,5500);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","-0.018142607100",-5500,5500);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   P1_PX_asym->cd();
   P1_PX_asym->Modified();
   P1_PX_asym->cd();
   P1_PX_asym->SetSelected(P1_PX_asym);
}
