#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void apply(TString year, TString polarity) {

  myH * h_PT_vs_PT = new myH("weights/"+year+"_"+polarity+"_0.myH");
  myH * h_Dst_PTvsETA = new myH("weights/"+year+"_"+polarity+"_1.myH");
  myH * h_Dst_PHI = new myH("weights/"+year+"_"+polarity+"_2.myH");
  myH * h_sPi_PTvsETA = new myH("weights/"+year+"_"+polarity+"_3.myH");
  myH * h_sPi_PHI = new myH("weights/"+year+"_"+polarity+"_4.myH");
  
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/_25e3/D02Kmpip_"+year+"_"+polarity+".root"); 
  
  double Dst_PT;
  toweight_tree->SetBranchAddress("Dst_PT",&Dst_PT);
  double Dst_ETA;
  toweight_tree->SetBranchAddress("Dst_ETA",&Dst_ETA);
  double Dst_PHI;
  toweight_tree->SetBranchAddress("Dst_PHI",&Dst_PHI);
  double sPi_PT;
  toweight_tree->SetBranchAddress("sPi_PT",&sPi_PT);
  double sPi_ETA;
  toweight_tree->SetBranchAddress("sPi_ETA",&sPi_ETA);
  double sPi_PHI;
  toweight_tree->SetBranchAddress("sPi_PHI",&sPi_PHI);

  double DTF_Mass;
  toweight_tree->SetBranchAddress("DTF_Mass",&DTF_Mass);
  int Dst_ID;
  toweight_tree->SetBranchAddress("Dst_ID",&Dst_ID);

 
  double Iw;
  vector <double> values;
  double sumw = 0;
  double Nentries_over_sumw;
  for(Long64_t i = 0; i < toweight_tree->GetEntries()
	; i++){
    toweight_tree->GetEntry(i);
    
    Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({Dst_PT,sPi_PT}));
    Iw *= h_Dst_PTvsETA->getBC(h_Dst_PTvsETA->find({Dst_PT,Dst_ETA}));
    Iw *=  h_Dst_PHI->getBC(h_Dst_PHI->find({Dst_PHI}));
    Iw *= h_sPi_PTvsETA->getBC(h_sPi_PTvsETA->find({sPi_PT,sPi_ETA}));
    Iw *= h_sPi_PHI->getBC(h_sPi_PHI->find({sPi_PHI}));
    
    if (Iw > 20)
      Iw = 20;
    
    sumw += Iw;
    values.push_back(Iw);
  }
  
  Nentries_over_sumw = ((double)toweight_tree->GetEntries())/sumw; 
  cout << Nentries_over_sumw << endl;
  Nentries_over_sumw = 1;//((double)toweight_tree->GetEntries())/sumw; 
  cout << Nentries_over_sumw << endl;


  TFile *f = TFile::Open("rew.root","RECREATE");
  TTree * t = new TTree("ntp","ntp");
  t->Branch("Iw",&Iw);
  t->Branch("Dst_PT",&Dst_PT);
  t->Branch("Dst_ETA",&Dst_ETA);
  t->Branch("Dst_PHI",&Dst_PHI);
  t->Branch("sPi_PT",&sPi_PT);
  t->Branch("sPi_ETA",&sPi_ETA);
  t->Branch("sPi_PHI",&sPi_PHI);
  t->Branch("DTF_Mass",&DTF_Mass);
  t->Branch("Dst_ID",&Dst_ID);

  TH1F * h_dm_plus = new TH1F("h_dm_plus","h_dm_plus",500,2004.5,2020);
  TH1F * h_dm_minus = new TH1F("h_dm_minus","h_dm_minus",500,2004.5,2020);

  for(Long64_t i = 0; i < 
	//5e3
	toweight_tree->GetEntries()
	; i++){
    toweight_tree->GetEntry(i);

    Iw = values.at(i)*Nentries_over_sumw;

    if(Dst_ID>0) {
      h_dm_plus->Fill(DTF_Mass,Iw);
    }
    else {
      h_dm_minus->Fill(DTF_Mass,Iw);
    }
    
    t->Fill();
  }

  f->Write();
  f->Close();
  
}
