#include "/home/LHCB/smaccoli/IWeight/IDecay.cxx"

int main(int argc, char * argv[]) {

  dcastyle();
  TString year = argv[1];
  TString polarity = argv[2];
  int i_set = atoi(argv[3]);
  int N_set = atoi(argv[4]);
  
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/_25e3/D02Kmpip_"+year+"_"+polarity+".root"); 
  toweight_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/_25e3/D02Kmpip_"+year+"_"+polarity+"_SPlot.root"); 
  
  TChain* target_tree_D2KK = new TChain("ntp","ntp");
  target_tree_D2KK->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/_25e3/D02KmKp_"+year+"_"+polarity+".root"); 
  target_tree_D2KK->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/_25e3/D02KmKp_"+year+"_"+polarity+"_SPlot.root");
 
  IDecay* toweight_decay = new IDecay(toweight_tree,"D02Kmpip","D^{0} #rightarrow K^{#minus} #pi^{+}",{"N_S_sw"},
				      5e6,
				      // toweight_tree->GetEntries(),
				      0);
  IDecay* target_decay_D2KK = new IDecay(target_tree_D2KK,"D02KmKp","D^{0} #rightarrow K^{#minus} K^{+}",{"N_S_sw"},
					 5e6,
					 // target_tree_D2KK->GetEntries(),
					 0);


  toweight_decay->addTargetDistribution(target_decay_D2KK,{"Dst_PT(100,2.2e3,8e3)","sPi_PT(100,100,8e2)"},5);
  
  toweight_decay->addTargetDistribution(target_decay_D2KK,{"Dst_PT(100,2.2e3,8e3)","Dst_ETA(100,2.0,4.7)"},5);
  toweight_decay->addTargetDistribution(target_decay_D2KK,{"Dst_PHI(100,-3.1416,3.1416)"},5);
  toweight_decay->addTargetDistribution(target_decay_D2KK,{"sPi_PT(100,100,8e2)","sPi_ETA(100,2.0,4.7)"},5);
  toweight_decay->addTargetDistribution(target_decay_D2KK,{"sPi_PHI(100,-3.1416,3.1416)"},5);
 
 
  toweight_decay->Iw_max = 20;
  toweight_decay->Iw_max_value = 20;
  toweight_decay->chi2_Ndof_max = 0.15;
  toweight_decay->outFileName = "weights/"+year+"_"+polarity+"";
  toweight_decay->ApplySmoothing = 0;
  toweight_decay->IWeight();

  
  return 0;
}
