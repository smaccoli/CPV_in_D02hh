#!/bin/bash
g++ -Wall -o _runIWeight runIWeight_D02Kpi_to_D02KK.cxx `root-config --cflags --glibs` `gsl-config --cflags --libs`


for year in $(echo 16); do
    for pol in $(echo Dw); do
#    for pol in $(echo Up); do
	    
	echo "time ./_runIWeight $year $pol 0 10 > log_${year}_${pol}.txt" > tmp.sh
	chmod +x tmp.sh
    done
done
