#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

int main() {

  dcastyle();
  
  TChain* toweight_tree = new TChain("ntp","ntp");
  // toweight_tree->Add("/home/LHCB/fferrari/ACPKK/ADKpi/data/ntuples/2016/MagDown/tuple_Kpipi_2016_MagDown_withSelection_withFiducialCuts_withSecondariesCut_withMultCandRemoval.root");
  //toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/forAngelo/Dp2Kmpippip_forAngelo.root");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip.root");
  //toweight_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/MVAWeight/Dp2Kmpippip_MVA.root");

  //toweight_tree->Add("~/data/Dp2KmKppip_selectedANDsweighted_16_Dw.root");
  // toweight_tree->GetLeaf("piplus_PT")->SetName("hplus_PT");
 
  TChain* target_tree = new TChain("ntp","ntp");
  //target_tree->Add("/home/LHCB/fferrari/ACPKK/ADKpi/data/ntuples/2016/MagDown/tuple_KS0Pip_2016_MagDown_withSelection_withFiducialCuts_withBDT_withSecondariesCut_withMultCandRemoval.root");
  //target_tree->Add("/home/LHCB/fferrari/ACPKK/ADKpi/data/ntuples/2016/MagDown/tuple_KS0Pip_2016_MagDown_withSelection_withFiducialCuts_noBDT.root");
  //  target_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/forAngelo/Dp2KS0pipLL_forAngelo.root");
  target_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2KS0pipLL.root");
  //target_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/MVAWeight/Dp2KS0pipLL_MVA.root");
  //target_tree->Add("~/data/Dp2KS0pipLL_selectedANDsweighted_16_Dw.root");

  IDecay* toweight_decay = new IDecay(toweight_tree,"Dp2Kmpippip","D^{+} #rightarrow K^{-} #pi^{+} h^{+}",{"Nsig_sw"},9e6/*toweight_tree->GetEntries()*/,0);
  IDecay* target_decay = new IDecay(target_tree,"Dp2KS0pip","D^{+} #rightarrow K_{S}^{0} h^{+}",{"Nsig_sw"},target_tree->GetEntries(),0);

  
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(100,3.5e3,14e3)","Dplus_ETA(100,2,4.5)"},15);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PHI(100,-3.1416,3.1416)"},15);
  toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(100,1.5e3,8.5e3)","hplus_ETA(100,1.9,4.7)"},10);
  toweight_decay->addTargetDistribution(target_decay,{"hplus_PHI(100,-3.1416,3.1416)"},15);
  


  toweight_decay->Iw_max = 20;
  toweight_decay->Iw_max_value = 0;
  toweight_decay->chi2_Ndof_max = 0.15;
  toweight_decay->IWeight();
  
  return 0;
}
