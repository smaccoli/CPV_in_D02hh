#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void before_after() {
  dcastyle();

  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip.root");
  toweight_tree->AddFriend("ntp","../rew.root");
  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2KS0pipLL.root");
 
  IDecay* toweight_decay = new IDecay(toweight_tree,"Dp2Kmpippip","D^{+} #rightarrow K^{-} #pi^{+} h^{+}",{"Nsig_sw"},toweight_tree->GetEntries()
				      ,0);
  IDecay* target_decay = new IDecay(target_tree,"Dp2KS0pip","D^{+} #rightarrow K_{S}^{0} h^{+}",{"Nsig_sw"},target_tree->GetEntries()
				    ,0);

  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(100,3.5e3,14e3)","Dplus_ETA(100,2,4.5)"},10);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PHI(100,-3.1416,3.1416)"},10);
  toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(100,1.5e3,8.5e3)","hplus_ETA(100,1.9,4.7)"},10);
  toweight_decay->addTargetDistribution(target_decay,{"hplus_PHI(100,-3.1416,3.1416)"},10);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PY(100,-14e3,14e3)","hplus_PY(100,-8.5e3,8.5e3)"},5);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PX(100,-14e3,14e3)","hplus_PX(100,-8.5e3,8.5e3)"},5);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PZ(100,15e3,250e3)","hplus_PZ(100,5e3,150e3)"},5);
  
  toweight_decay->fillTargets();

  toweight_decay->setIWeight("Iw"); //from rew.root

  toweight_decay->showVariables();
}
