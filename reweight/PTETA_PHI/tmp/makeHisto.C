#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TCanvas.h"
#include "TChain.h"
#include "TLegend.h"
#include "TH1D.h"
#include "TH2F.h"
#include <iostream>
#include "TCut.h"
#include "TROOT.h"

using namespace std;


void makeHisto( ) {

  TH1::SetDefaultSumw2();

  gROOT->SetBatch(kTRUE); //no window for canvas

  TChain* tree = new TChain("ntp","ntp");
  tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/tmp_Dp2Kmpippip.root");
  tree->AddFriend("ntp","../rew.root");


  TCut DplusID = "Dplus_ID == 431 || Dplus_ID == 411";
  TCut DminusID = "Dplus_ID == -431 || Dplus_ID == -411";
  TString weight = "Iw";
  
  TFile *f(0);
  //f = new TFile("Dplus_M_Dp2Kmpippip.root","RECREATE");
  f = new TFile("Dplus_M_Dp2Kmpippip_rew.root","RECREATE");
  //  f = new TFile("Dplus_M_Dp2KS0pipLL.root","RECREATE");
  
  tree->Draw("Dplus_M>>h_Dplus(500,1800,1930)",DplusID*weight,"");
  tree->Draw("Dplus_M>>h_Dminus(500,1800,1930)",DminusID*weight,"");


  TH1D *h_Dplus = (TH1D*) gDirectory->Get("h_Dplus");  
  TH1D *h_Dminus = (TH1D*) gDirectory->Get("h_Dminus");

  cout << f->GetName() << " written." << endl;  
  
  f->Write();
  f->Close();

}

