#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

int main(int argc, char * argv[]) {
 
  bool test = 1;
  bool isPGun = 0;
  bool use_ALL = 0;
  bool rew_X = 0;
  TString add_string = "";
  
  
  if (isPGun) {
    add_string += "_pGun";
    if (use_ALL)
      add_string += "_ALL";
  }

  if (test)
    add_string += "_MVAsel";

  dcastyle();
  TString year = argv[1];
  TString polarity = argv[2];
  int i_set = atoi(argv[3]);
  int N_set = atoi(argv[4]);
  
  TString toweight_tree_inputFileName = "/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_"+year+"_"+polarity+add_string+".root";
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add(toweight_tree_inputFileName);
  toweight_tree->AddFriend("ntp",toweight_tree_inputFileName.ReplaceAll(".root","_SPlot.root"));
  
  TString target_tree_inputFileName = "/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2KS0pipLL_"+year+"_"+polarity+add_string+".root";
  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add(target_tree_inputFileName);
  target_tree->AddFriend("ntp",target_tree_inputFileName.ReplaceAll(".root","_SPlot.root"));

  vector <string> toweight_input_weights = {"N_sigD_sw"};
  if(isPGun || test) toweight_input_weights.clear();
  vector <string> target_input_weights = toweight_input_weights;
  if(isPGun) toweight_input_weights = {"trueLabels"};
  if(test) toweight_input_weights.clear();

  size_t toweight_evts = min(toweight_tree->GetEntries(),12*target_tree->GetEntries());
  if (test)
    toweight_evts = min(toweight_tree->GetEntries(),5*target_tree->GetEntries());
  
  IDecay* toweight_decay = new IDecay(toweight_tree,"Dp2Kmpippip","D^{+} #rightarrow K^{#minus} #pi^{+} h^{+}",toweight_input_weights,toweight_evts,0);//20*target
  IDecay* target_decay = new IDecay(target_tree,"Dp2KS0pip","D^{+} #rightarrow K_{S}^{0} h^{+}",target_input_weights,target_tree->GetEntries(),0);

  if (!rew_X) { 
    if (year != "15" && !isPGun && !test) {
      toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(70,4.2e3,11e3)","hplus_PT(70,1.6e3,6e3)"},5.0);//5
      toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(70,4.2e3,11e3)","Dplus_ETA(60,2.3,4.1)"},5.0);//5
      toweight_decay->addTargetDistribution(target_decay,{"Dplus_PHI(100,-3.1416,3.1416)"},5.0);
      toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(70,1.6e3,6e3)","hplus_ETA(70,2.2,4.1)"},5.0);//5
      toweight_decay->addTargetDistribution(target_decay,{"hplus_PHI(100,-3.1416,3.1416)"},5.0);
    }
    else if (year == "15" && !test) {
      toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(35,4.2e3,11e3)","hplus_PT(35,1.6e3,6e3)"},5.0);//5
      //toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(35,4.2e3,11e3)","Dplus_ETA(30,2.3,4.1)"},5.0);//5
      toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(35,4.2e3,11e3)","Dplus_ETA(35,2.3,4.1)"},5.0);//5
      toweight_decay->addTargetDistribution(target_decay,{"Dplus_PHI(100,-3.1416,3.1416)"},5.0);
      toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(35,1.6e3,6e3)","hplus_ETA(35,2.2,4.1)"},5.0);//5
      toweight_decay->addTargetDistribution(target_decay,{"hplus_PHI(100,-3.1416,3.1416)"},5.0);    
    } 
    else {
      toweight_decay->addTargetDistribution(target_decay,{"BDTG(100,0.4,0.9)"},5.0);    
    }
  }
  else {
    //toweight_decay->addTargetDistribution(target_decay,{"X_PT(40,2.2e3,14e3)","X_ETA(40,2,4.4)","X_PHI(40,-3.1416,3.1416)"},5);
    //toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(35,4.2e3,11e3)","Dplus_ETA(35,2.3,4.1)","Dplus_PHI(75,-3.1416,3.1416)"},5.0);//5
    //toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(35,1.6e3,6e3)","hplus_ETA(35,2.2,4.1)","hplus_PHI(75,-3.1416,3.1416)"},5.0);//5
    //    toweight_decay->addTargetDistribution(target_decay,{"Dplus_ETA(25,2.3,4.1)","hplus_ETA(25,2.2,4.1)","Dplus_PT(25,4.2e3,11e3)"},5.0);//5
    toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(25,4.2e3,11e3)","Dplus_ETA(25,2.3,4.1)","Dplus_PHI(25,-3.1416,3.1416)","hplus_PT(25,1.6e3,6e3)","hplus_ETA(25,2.2,4.1)","hplus_PHI(25,-3.1416,3.1416)"},5.0);//5
    /*    
	  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(2,4.2e3,11e3)","Dplus_ETA(2,2.3,4.1)","Dplus_PHI(2,-3.1416,3.1416)","hplus_PT(2,1.6e3,6e3)","hplus_ETA(2,2.2,4.1)","hplus_PHI(2,-3.1416,3.1416)"},5);
	  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(3,4.2e3,11e3)","Dplus_ETA(3,2.3,4.1)","Dplus_PHI(3,-3.1416,3.1416)","hplus_PT(3,1.6e3,6e3)","hplus_ETA(3,2.2,4.1)","hplus_PHI(3,-3.1416,3.1416)"},5);
	  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(4,4.2e3,11e3)","Dplus_ETA(4,2.3,4.1)","Dplus_PHI(4,-3.1416,3.1416)","hplus_PT(4,1.6e3,6e3)","hplus_ETA(4,2.2,4.1)","hplus_PHI(4,-3.1416,3.1416)"},5);
	  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(5,4.2e3,11e3)","Dplus_ETA(5,2.3,4.1)","Dplus_PHI(5,-3.1416,3.1416)","hplus_PT(5,1.6e3,6e3)","hplus_ETA(5,2.2,4.1)","hplus_PHI(5,-3.1416,3.1416)"},5);
	  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(6,4.2e3,11e3)","Dplus_ETA(6,2.3,4.1)","Dplus_PHI(6,-3.1416,3.1416)","hplus_PT(6,1.6e3,6e3)","hplus_ETA(6,2.2,4.1)","hplus_PHI(6,-3.1416,3.1416)"},5);
	  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(7,4.2e3,11e3)","Dplus_ETA(7,2.3,4.1)","Dplus_PHI(7,-3.1416,3.1416)","hplus_PT(7,1.6e3,6e3)","hplus_ETA(7,2.2,4.1)","hplus_PHI(7,-3.1416,3.1416)"},5);
	  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(8,4.2e3,11e3)","Dplus_ETA(8,2.3,4.1)","Dplus_PHI(8,-3.1416,3.1416)","hplus_PT(8,1.6e3,6e3)","hplus_ETA(8,2.2,4.1)","hplus_PHI(8,-3.1416,3.1416)"},5);
    */
    //toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(23,4.2e3,11e3)","Dplus_ETA(23,2.3,4.1)","Dplus_PHI(23,-3.1416,3.1416)"},5.0);//5
    //toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(23,1.6e3,6e3)","hplus_ETA(23,2.2,4.1)","hplus_PHI(23,-3.1416,3.1416)"},5.0);//5
    //    toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(5,4.2e3,11e3)","Dplus_ETA(5,2.3,4.1)","Dplus_PHI(5,-3.1416,3.1416)","hplus_PT(5,1.6e3,6e3)","hplus_ETA(5,2.2,4.1)","hplus_PHI(5,-3.1416,3.1416)","X_PT(5,2.2e3,14e3)","X_ETA(5,2,4.4)","X_PHI(5,-3.1416,3.1416)"},5);
    
    /*
      toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(100,4.2e3,11e3)"},5.0);
      toweight_decay->addTargetDistribution(target_decay,{"Dplus_ETA(100,2.3,4.1)"},5.0);
      toweight_decay->addTargetDistribution(target_decay,{"Dplus_PHI(100,-3.1416,3.1416)"},5.0);
      toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(100,1.6e3,6e3)"},5.0);
      toweight_decay->addTargetDistribution(target_decay,{"hplus_ETA(100,2.2,4.1)"},5.0);    
      toweight_decay->addTargetDistribution(target_decay,{"hplus_PHI(100,-3.1416,3.1416)"},5.0);
    */
  }
  if (rew_X)
    add_string += "_X";
 
  toweight_decay->use_Iw_max = 0;
  toweight_decay->Iw_max = 15;
  toweight_decay->Iw_max_value = 15;
  toweight_decay->chi2_Ndof_max = 0.15;
  if(rew_X)
    toweight_decay->chi2_Ndof_max = 0.4;   
  toweight_decay->ApplySmoothing = 0;
  toweight_decay->outFileName = "/home/LHCB-T3/smaccoli/CPV_in_D02hh/reweight/D2Kpipi_to_D2KSpi/weights/"+year+"_"+polarity+add_string;
  toweight_decay->IWeight();
  
  return 0;
}
