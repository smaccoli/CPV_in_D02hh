#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void apply(TString year = "16", TString polarity = "Dw",TString decay = "Dp2Kmpippip") {
  
  bool test = 1;
  bool isPGun = 0;
  bool use_ALL = 0;
  bool rew_X = 0;
  TString add_string = "";
  bool cut_Iw = 0;
  
  if (isPGun) {
    add_string += "_pGun";
    if (use_ALL)
      add_string += "_ALL";
  }
  
  if (test)
    add_string += "_MVAsel";

  TString inFileName = "/home/LHCB/smaccoli/CPV_in_D02hh/data/"+decay+"_"+year+"_"+polarity+add_string+".root";
  
  if (rew_X)
    add_string += "_X";
  
  myH * h_PT_vs_PT = new myH("weights/"+year+"_"+polarity+add_string+"_0.myH");
  /*
  dcastyle();
  TCanvas*c = new TCanvas("c","c",500,500,500,500);
  TH2D h = h_PT_vs_PT->convert2TH2D();
  h.SetName("h");
  h.SetTitle("h");
  cout << h.GetEntries() << endl;
  cout << h.GetBinContent(25) << endl;
  h.Draw("colz");
  return;
  */
  myH * h_Dp_PTvsETA = new myH("weights/"+year+"_"+polarity+add_string+"_1.myH");
  myH * h_Dp_PHI = new myH("weights/"+year+"_"+polarity+add_string+"_2.myH");
  myH * h_hp_PTvsETA = new myH("weights/"+year+"_"+polarity+add_string+"_3.myH");
  myH * h_hp_PHI = new myH("weights/"+year+"_"+polarity+add_string+"_4.myH");
  myH * h_5 = new myH("weights/"+year+"_"+polarity+add_string+"_5.myH");
  myH * h_6 = new myH("weights/"+year+"_"+polarity+add_string+"_6.myH");
  
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add(inFileName);
  
  double Dplus_PT;
  toweight_tree->SetBranchAddress("Dplus_PT",&Dplus_PT);
  double Dplus_ETA;
  toweight_tree->SetBranchAddress("Dplus_ETA",&Dplus_ETA);
  double Dplus_PHI;
  toweight_tree->SetBranchAddress("Dplus_PHI",&Dplus_PHI);
  double hplus_PT;
  toweight_tree->SetBranchAddress("hplus_PT",&hplus_PT);
  double hplus_ETA;
  toweight_tree->SetBranchAddress("hplus_ETA",&hplus_ETA);
  double hplus_PHI;
  toweight_tree->SetBranchAddress("hplus_PHI",&hplus_PHI);

  double piplus_PT;
  toweight_tree->SetBranchAddress("piplus_PT",&piplus_PT);
  double piplus_ETA;
  toweight_tree->SetBranchAddress("piplus_ETA",&piplus_ETA);
  double piplus_PHI;
  toweight_tree->SetBranchAddress("piplus_PHI",&piplus_PHI);
  double Kminus_PT;
  toweight_tree->SetBranchAddress("Kminus_PT",&Kminus_PT);
  double Kminus_ETA;
  toweight_tree->SetBranchAddress("Kminus_ETA",&Kminus_ETA);
  double Kminus_PHI;
  toweight_tree->SetBranchAddress("Kminus_PHI",&Kminus_PHI);
  double X_PT;
  toweight_tree->SetBranchAddress("X_PT",&X_PT);
  double X_ETA;
  toweight_tree->SetBranchAddress("X_ETA",&X_ETA);
  double X_PHI;
  toweight_tree->SetBranchAddress("X_PHI",&X_PHI);
 
  double Dplus_M;
  toweight_tree->SetBranchAddress("Dplus_M",&Dplus_M);
  int Dplus_ID;
  toweight_tree->SetBranchAddress("Dplus_ID",&Dplus_ID);
 
  float BDTG;
  toweight_tree->SetBranchAddress("BDTG",&BDTG);
 
  //pGun only
  bool trueLabels;
  if (isPGun)
    toweight_tree->SetBranchAddress("trueLabels",&trueLabels);
  
  TString outFileName = inFileName;
  outFileName.ReplaceAll(".root","_rew.root");
  if (rew_X)
    outFileName.ReplaceAll(".root","_X.root");
  TFile *f = TFile::Open(outFileName,"RECREATE");

  TTree * t = new TTree("ntp","ntp");
  double Iw;
  t->Branch("Iw",&Iw);
  double Iw_err;
  t->Branch("Iw_err",&Iw_err);
  t->Branch("Dplus_PT",&Dplus_PT);
  t->Branch("Dplus_ETA",&Dplus_ETA);
  t->Branch("Dplus_PHI",&Dplus_PHI);
  t->Branch("hplus_PT",&hplus_PT);
  t->Branch("hplus_ETA",&hplus_ETA);
  t->Branch("hplus_PHI",&hplus_PHI);
  t->Branch("P2_PT",&Kminus_PT);
  t->Branch("P2_ETA",&Kminus_ETA);
  t->Branch("P2_PHI",&Kminus_PHI);
  t->Branch("P1_PT",&piplus_PT);
  t->Branch("P1_ETA",&piplus_ETA);
  t->Branch("P1_PHI",&piplus_PHI);
  t->Branch("X_PT",&X_PT);
  t->Branch("X_ETA",&X_ETA);
  t->Branch("X_PHI",&X_PHI);
  t->Branch("Dplus_M",&Dplus_M);
  t->Branch("Dplus_ID",&Dplus_ID);

  TH1D * h_mass_plus = new TH1D("h_mass_plus", "h_mass_plus", 500, 1800, 1930);
  TH1D * h_mass_minus = new TH1D("h_mass_minus", "h_mass_minus", 500, 1800, 1930);

  vector <double> values;
  for(Long64_t i = 0; i < 
	//3e6;
	toweight_tree->GetEntries();
      i++){
    toweight_tree->GetEntry(i);
    
    if(!rew_X && !test) {
      Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({Dplus_PT,hplus_PT}));
      Iw *= h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({Dplus_PT,Dplus_ETA}));
      Iw *=  h_Dp_PHI->getBC(h_Dp_PHI->find({Dplus_PHI}));
      Iw *= h_hp_PTvsETA->getBC(h_hp_PTvsETA->find({hplus_PT,hplus_ETA}));
      Iw *= h_hp_PHI->getBC(h_hp_PHI->find({hplus_PHI}));
    }
    else {
      //Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({Dplus_PT,Dplus_ETA,Dplus_PHI}));
      //      Iw *= h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({hplus_PT,hplus_ETA,hplus_PHI}));
      //Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({Dplus_PT,Dplus_ETA,Dplus_PHI,hplus_PT,hplus_ETA,hplus_PHI}));
      //Iw_err = h_PT_vs_PT->getBE(h_PT_vs_PT->find({Dplus_PT,Dplus_ETA,Dplus_PHI,hplus_PT,hplus_ETA,hplus_PHI}));
      Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({BDTG}));
      /*
	Iw *=  h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({Dplus_PT,Dplus_ETA,Dplus_PHI,hplus_PT,hplus_ETA,hplus_PHI}));
	Iw *= h_Dp_PHI->getBC(h_Dp_PHI->find({Dplus_PT,Dplus_ETA,Dplus_PHI,hplus_PT,hplus_ETA,hplus_PHI}));
	Iw *= h_hp_PTvsETA->getBC(h_hp_PTvsETA->find({Dplus_PT,Dplus_ETA,Dplus_PHI,hplus_PT,hplus_ETA,hplus_PHI}));
	Iw *= h_hp_PHI->getBC(h_hp_PHI->find({Dplus_PT,Dplus_ETA,Dplus_PHI,hplus_PT,hplus_ETA,hplus_PHI}));
	Iw *= h_5->getBC(h_5->find({Dplus_PT,Dplus_ETA,Dplus_PHI,hplus_PT,hplus_ETA,hplus_PHI}));
	Iw *= h_6->getBC(h_6->find({Dplus_PT,Dplus_ETA,Dplus_PHI,hplus_PT,hplus_ETA,hplus_PHI}));
      */
    }

    if (isPGun) {
      if (trueLabels) {
	Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({Dplus_PT,hplus_PT}));
	Iw *= h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({Dplus_PT,Dplus_ETA}));
	Iw *=  h_Dp_PHI->getBC(h_Dp_PHI->find({Dplus_PHI}));
	Iw *= h_hp_PTvsETA->getBC(h_hp_PTvsETA->find({hplus_PT,hplus_ETA}));
	Iw *= h_hp_PHI->getBC(h_hp_PHI->find({hplus_PHI}));
      }
      else {
	Iw = h_PT_vs_PT->getBC(h_PT_vs_PT->find({Dplus_PT,piplus_PT}));
	Iw *= h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({Dplus_PT,Dplus_ETA}));
	Iw *=  h_Dp_PHI->getBC(h_Dp_PHI->find({Dplus_PHI}));
	Iw *= h_hp_PTvsETA->getBC(h_hp_PTvsETA->find({piplus_PT,piplus_ETA}));
	Iw *= h_hp_PHI->getBC(h_hp_PHI->find({piplus_PHI}));
      }

    }

    if(cut_Iw)
      if (Iw > 15)
	Iw = 0;
  
    if(Dplus_ID>0) {
      h_mass_plus->Fill(Dplus_M,Iw);
    }
    else {
      h_mass_minus->Fill(Dplus_M,Iw);
    }
    
    t->Fill();
  }
  
  f->Write();
  f->Close();
  
}

int main(int argc, char * argv[]) { 
  apply(argv[1], argv[2]); 
  return 0;
}
