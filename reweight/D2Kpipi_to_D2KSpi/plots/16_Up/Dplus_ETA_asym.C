void Dplus_ETA_asym()
{
//=========Macro generated from canvas: Dplus_ETA_asym/Dplus_ETA_asym
//=========  (Mon Nov 18 16:23:04 2019) by ROOT version6.08/02
   TCanvas *Dplus_ETA_asym = new TCanvas("Dplus_ETA_asym", "Dplus_ETA_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   Dplus_ETA_asym->SetHighLightColor(2);
   Dplus_ETA_asym->Range(0,0,1,1);
   Dplus_ETA_asym->SetFillColor(0);
   Dplus_ETA_asym->SetBorderMode(0);
   Dplus_ETA_asym->SetBorderSize(10);
   Dplus_ETA_asym->SetTickx(1);
   Dplus_ETA_asym->SetTicky(1);
   Dplus_ETA_asym->SetLeftMargin(0.18);
   Dplus_ETA_asym->SetRightMargin(0.05);
   Dplus_ETA_asym->SetTopMargin(0.07);
   Dplus_ETA_asym->SetBottomMargin(0.16);
   Dplus_ETA_asym->SetFrameFillStyle(0);
   Dplus_ETA_asym->SetFrameLineStyle(0);
   Dplus_ETA_asym->SetFrameLineColor(0);
   Dplus_ETA_asym->SetFrameLineWidth(0);
   Dplus_ETA_asym->SetFrameBorderMode(0);
   Dplus_ETA_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(1.9625,-0.001948316,4.2125,0.0008130503);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,2.3,4.1);
   h1D_target_minus_toweight__8->SetBinContent(2,-1.255363e-05);
   h1D_target_minus_toweight__8->SetBinContent(3,-2.282274e-05);
   h1D_target_minus_toweight__8->SetBinContent(4,-0.0001976592);
   h1D_target_minus_toweight__8->SetBinContent(5,-9.678872e-05);
   h1D_target_minus_toweight__8->SetBinContent(6,-0.0003365428);
   h1D_target_minus_toweight__8->SetBinContent(7,-0.000220086);
   h1D_target_minus_toweight__8->SetBinContent(8,-0.0002177499);
   h1D_target_minus_toweight__8->SetBinContent(9,-0.0004633636);
   h1D_target_minus_toweight__8->SetBinContent(10,-0.0002550058);
   h1D_target_minus_toweight__8->SetBinContent(11,-0.0006994966);
   h1D_target_minus_toweight__8->SetBinContent(12,-0.0004966445);
   h1D_target_minus_toweight__8->SetBinContent(13,-0.0005080898);
   h1D_target_minus_toweight__8->SetBinContent(14,-0.0006581065);
   h1D_target_minus_toweight__8->SetBinContent(15,-0.0001862366);
   h1D_target_minus_toweight__8->SetBinContent(16,-0.001032243);
   h1D_target_minus_toweight__8->SetBinContent(17,-0.0003271173);
   h1D_target_minus_toweight__8->SetBinContent(18,-0.0007218802);
   h1D_target_minus_toweight__8->SetBinContent(19,-0.0004790402);
   h1D_target_minus_toweight__8->SetBinContent(20,-0.0001239651);
   h1D_target_minus_toweight__8->SetBinContent(21,-0.0006033326);
   h1D_target_minus_toweight__8->SetBinContent(22,-0.0003723465);
   h1D_target_minus_toweight__8->SetBinContent(23,-0.0004392769);
   h1D_target_minus_toweight__8->SetBinContent(24,-0.0005094819);
   h1D_target_minus_toweight__8->SetBinContent(25,-0.0002569221);
   h1D_target_minus_toweight__8->SetBinContent(26,-0.0002642442);
   h1D_target_minus_toweight__8->SetBinContent(27,-0.0003849678);
   h1D_target_minus_toweight__8->SetBinContent(28,-0.0003125779);
   h1D_target_minus_toweight__8->SetBinContent(29,1.193956e-06);
   h1D_target_minus_toweight__8->SetBinContent(30,-0.0004773084);
   h1D_target_minus_toweight__8->SetBinContent(31,-0.0001614643);
   h1D_target_minus_toweight__8->SetBinContent(32,-0.0002896935);
   h1D_target_minus_toweight__8->SetBinContent(33,-7.356331e-05);
   h1D_target_minus_toweight__8->SetBinContent(34,8.315779e-06);
   h1D_target_minus_toweight__8->SetBinContent(35,-5.708914e-05);
   h1D_target_minus_toweight__8->SetBinContent(36,-0.0001986213);
   h1D_target_minus_toweight__8->SetBinContent(37,0.0001640776);
   h1D_target_minus_toweight__8->SetBinContent(38,-0.0001057507);
   h1D_target_minus_toweight__8->SetBinContent(39,0.0001212554);
   h1D_target_minus_toweight__8->SetBinContent(40,-5.769916e-05);
   h1D_target_minus_toweight__8->SetBinContent(41,0.0002018334);
   h1D_target_minus_toweight__8->SetBinContent(42,-0.0001480915);
   h1D_target_minus_toweight__8->SetBinContent(43,0.0001085661);
   h1D_target_minus_toweight__8->SetBinContent(44,0.0003667437);
   h1D_target_minus_toweight__8->SetBinContent(45,8.422229e-05);
   h1D_target_minus_toweight__8->SetBinContent(46,0.0001959745);
   h1D_target_minus_toweight__8->SetBinContent(47,0.0001886059);
   h1D_target_minus_toweight__8->SetBinContent(48,5.878601e-05);
   h1D_target_minus_toweight__8->SetBinContent(49,6.921869e-05);
   h1D_target_minus_toweight__8->SetBinContent(50,0.0004293863);
   h1D_target_minus_toweight__8->SetBinContent(51,0.0004734043);
   h1D_target_minus_toweight__8->SetBinContent(52,4.033558e-05);
   h1D_target_minus_toweight__8->SetBinContent(53,3.56026e-05);
   h1D_target_minus_toweight__8->SetBinContent(54,0.0005032346);
   h1D_target_minus_toweight__8->SetBinContent(55,0.0001828149);
   h1D_target_minus_toweight__8->SetBinContent(56,0.0001467075);
   h1D_target_minus_toweight__8->SetBinContent(57,0.0006150668);
   h1D_target_minus_toweight__8->SetBinContent(58,0.0002551489);
   h1D_target_minus_toweight__8->SetBinContent(59,0.0002332944);
   h1D_target_minus_toweight__8->SetBinContent(60,0.0002820622);
   h1D_target_minus_toweight__8->SetBinContent(61,0.0003891373);
   h1D_target_minus_toweight__8->SetBinContent(62,0.0001827544);
   h1D_target_minus_toweight__8->SetBinContent(63,0.0007210048);
   h1D_target_minus_toweight__8->SetBinContent(64,0.0002021501);
   h1D_target_minus_toweight__8->SetBinContent(65,0.0005229488);
   h1D_target_minus_toweight__8->SetBinContent(66,0.0001748754);
   h1D_target_minus_toweight__8->SetBinContent(67,0.0005645407);
   h1D_target_minus_toweight__8->SetBinContent(68,0.000263948);
   h1D_target_minus_toweight__8->SetBinContent(69,0.0001252508);
   h1D_target_minus_toweight__8->SetBinContent(70,0.0005492019);
   h1D_target_minus_toweight__8->SetBinContent(71,0.000241857);
   h1D_target_minus_toweight__8->SetBinContent(72,-3.785919e-05);
   h1D_target_minus_toweight__8->SetBinContent(73,0.0005218247);
   h1D_target_minus_toweight__8->SetBinContent(74,4.260615e-05);
   h1D_target_minus_toweight__8->SetBinContent(75,0.0004259953);
   h1D_target_minus_toweight__8->SetBinContent(76,0.0001338068);
   h1D_target_minus_toweight__8->SetBinContent(77,0.0003429446);
   h1D_target_minus_toweight__8->SetBinContent(78,0.0002595065);
   h1D_target_minus_toweight__8->SetBinContent(79,-6.81635e-06);
   h1D_target_minus_toweight__8->SetBinContent(80,0.0002939655);
   h1D_target_minus_toweight__8->SetBinContent(81,0.000259622);
   h1D_target_minus_toweight__8->SetBinContent(82,0.0002306597);
   h1D_target_minus_toweight__8->SetBinContent(83,0.0003463347);
   h1D_target_minus_toweight__8->SetBinContent(84,-4.973076e-05);
   h1D_target_minus_toweight__8->SetBinContent(85,-4.769303e-06);
   h1D_target_minus_toweight__8->SetBinContent(86,0.0002243053);
   h1D_target_minus_toweight__8->SetBinContent(87,0.0001963545);
   h1D_target_minus_toweight__8->SetBinContent(88,0.0001718998);
   h1D_target_minus_toweight__8->SetBinContent(89,6.887596e-05);
   h1D_target_minus_toweight__8->SetBinContent(90,-8.764956e-05);
   h1D_target_minus_toweight__8->SetBinContent(91,0.0003199764);
   h1D_target_minus_toweight__8->SetBinContent(92,-0.0001387936);
   h1D_target_minus_toweight__8->SetBinContent(93,-0.0002504219);
   h1D_target_minus_toweight__8->SetBinContent(94,0.0001659994);
   h1D_target_minus_toweight__8->SetBinContent(95,-3.042957e-05);
   h1D_target_minus_toweight__8->SetBinContent(96,-2.715737e-06);
   h1D_target_minus_toweight__8->SetBinContent(97,-0.0002932649);
   h1D_target_minus_toweight__8->SetBinContent(98,1.373934e-06);
   h1D_target_minus_toweight__8->SetBinContent(99,-4.795403e-05);
   h1D_target_minus_toweight__8->SetBinContent(100,8.657807e-06);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("#eta (D^{+})");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{+} #rightarrow K_{S}^{0} h^{+})#minusN(D^{+} #rightarrow K^{#minus} #pi^{+} h^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",2.3,4.1);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   Dplus_ETA_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(1.9625,-0.1993389,4.2125,0.2979746);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,2.3,4.1);
   h1D_asym__9->SetBinContent(2,0.02394472);
   h1D_asym__9->SetBinContent(3,-0.02427356);
   h1D_asym__9->SetBinContent(4,-0.03339289);
   h1D_asym__9->SetBinContent(5,-0.0337187);
   h1D_asym__9->SetBinContent(6,-0.01614751);
   h1D_asym__9->SetBinContent(7,-0.02117555);
   h1D_asym__9->SetBinContent(8,-0.02193347);
   h1D_asym__9->SetBinContent(9,-0.02814485);
   h1D_asym__9->SetBinContent(10,-0.01934586);
   h1D_asym__9->SetBinContent(11,-0.01441769);
   h1D_asym__9->SetBinContent(12,-0.01294309);
   h1D_asym__9->SetBinContent(13,-0.01205964);
   h1D_asym__9->SetBinContent(14,-0.02157665);
   h1D_asym__9->SetBinContent(15,-0.01823989);
   h1D_asym__9->SetBinContent(16,-0.01564039);
   h1D_asym__9->SetBinContent(17,-0.01815025);
   h1D_asym__9->SetBinContent(18,-0.02055737);
   h1D_asym__9->SetBinContent(19,-0.01102613);
   h1D_asym__9->SetBinContent(20,-0.01548843);
   h1D_asym__9->SetBinContent(21,-0.01824438);
   h1D_asym__9->SetBinContent(22,-0.01489921);
   h1D_asym__9->SetBinContent(23,-0.01658858);
   h1D_asym__9->SetBinContent(24,-0.01185033);
   h1D_asym__9->SetBinContent(25,-0.01420197);
   h1D_asym__9->SetBinContent(26,-0.01252557);
   h1D_asym__9->SetBinContent(27,-0.01833389);
   h1D_asym__9->SetBinContent(28,-0.01488274);
   h1D_asym__9->SetBinContent(29,-0.01803385);
   h1D_asym__9->SetBinContent(30,-0.01720207);
   h1D_asym__9->SetBinContent(31,-0.01846773);
   h1D_asym__9->SetBinContent(32,-0.01659046);
   h1D_asym__9->SetBinContent(33,-0.01675987);
   h1D_asym__9->SetBinContent(34,-0.01447567);
   h1D_asym__9->SetBinContent(35,-0.01841328);
   h1D_asym__9->SetBinContent(36,-0.01245438);
   h1D_asym__9->SetBinContent(37,-0.01602346);
   h1D_asym__9->SetBinContent(38,-0.01567882);
   h1D_asym__9->SetBinContent(39,-0.01539949);
   h1D_asym__9->SetBinContent(40,-0.0198159);
   h1D_asym__9->SetBinContent(41,-0.0158211);
   h1D_asym__9->SetBinContent(42,-0.01461866);
   h1D_asym__9->SetBinContent(43,-0.01215509);
   h1D_asym__9->SetBinContent(44,-0.01795523);
   h1D_asym__9->SetBinContent(45,-0.01287906);
   h1D_asym__9->SetBinContent(46,-0.0159967);
   h1D_asym__9->SetBinContent(47,-0.01337069);
   h1D_asym__9->SetBinContent(48,-0.01714137);
   h1D_asym__9->SetBinContent(49,-0.0163538);
   h1D_asym__9->SetBinContent(50,-0.01691147);
   h1D_asym__9->SetBinContent(51,-0.01891977);
   h1D_asym__9->SetBinContent(52,-0.01562264);
   h1D_asym__9->SetBinContent(53,-0.01990473);
   h1D_asym__9->SetBinContent(54,-0.01292378);
   h1D_asym__9->SetBinContent(55,-0.01330829);
   h1D_asym__9->SetBinContent(56,-0.01624397);
   h1D_asym__9->SetBinContent(57,-0.01644635);
   h1D_asym__9->SetBinContent(58,-0.01778324);
   h1D_asym__9->SetBinContent(59,-0.01648576);
   h1D_asym__9->SetBinContent(60,-0.01773229);
   h1D_asym__9->SetBinContent(61,-0.01249535);
   h1D_asym__9->SetBinContent(62,-0.01506344);
   h1D_asym__9->SetBinContent(63,-0.01782185);
   h1D_asym__9->SetBinContent(64,-0.01911206);
   h1D_asym__9->SetBinContent(65,-0.01420051);
   h1D_asym__9->SetBinContent(66,-0.02051214);
   h1D_asym__9->SetBinContent(67,-0.01881287);
   h1D_asym__9->SetBinContent(68,-0.01764844);
   h1D_asym__9->SetBinContent(69,-0.01895674);
   h1D_asym__9->SetBinContent(70,-0.01727761);
   h1D_asym__9->SetBinContent(71,-0.01762281);
   h1D_asym__9->SetBinContent(72,-0.01639892);
   h1D_asym__9->SetBinContent(73,-0.02222755);
   h1D_asym__9->SetBinContent(74,-0.02353575);
   h1D_asym__9->SetBinContent(75,-0.02158782);
   h1D_asym__9->SetBinContent(76,-0.02169649);
   h1D_asym__9->SetBinContent(77,-0.02305014);
   h1D_asym__9->SetBinContent(78,-0.02497694);
   h1D_asym__9->SetBinContent(79,-0.02580312);
   h1D_asym__9->SetBinContent(80,-0.02843492);
   h1D_asym__9->SetBinContent(81,-0.02756167);
   h1D_asym__9->SetBinContent(82,-0.02717877);
   h1D_asym__9->SetBinContent(83,-0.03005048);
   h1D_asym__9->SetBinContent(84,-0.03167278);
   h1D_asym__9->SetBinContent(85,-0.03718784);
   h1D_asym__9->SetBinContent(86,-0.02674415);
   h1D_asym__9->SetBinContent(87,-0.02831054);
   h1D_asym__9->SetBinContent(88,-0.03109506);
   h1D_asym__9->SetBinContent(89,-0.03114493);
   h1D_asym__9->SetBinContent(90,-0.03376803);
   h1D_asym__9->SetBinContent(91,-0.03695731);
   h1D_asym__9->SetBinContent(92,-0.04064937);
   h1D_asym__9->SetBinContent(93,-0.03170628);
   h1D_asym__9->SetBinContent(94,-0.04414494);
   h1D_asym__9->SetBinContent(95,-0.04470846);
   h1D_asym__9->SetBinContent(96,-0.04059422);
   h1D_asym__9->SetBinContent(97,-0.03105488);
   h1D_asym__9->SetBinContent(98,-0.02501967);
   h1D_asym__9->SetBinContent(99,-0.03053543);
   h1D_asym__9->SetBinContent(100,-0.04233139);
   h1D_asym__9->SetBinError(2,0.2029851);
   h1D_asym__9->SetBinError(3,0.09372444);
   h1D_asym__9->SetBinError(4,0.0806724);
   h1D_asym__9->SetBinError(5,0.05710623);
   h1D_asym__9->SetBinError(6,0.0321701);
   h1D_asym__9->SetBinError(7,0.01926811);
   h1D_asym__9->SetBinError(8,0.01553488);
   h1D_asym__9->SetBinError(9,0.01355855);
   h1D_asym__9->SetBinError(10,0.01190067);
   h1D_asym__9->SetBinError(11,0.01060638);
   h1D_asym__9->SetBinError(12,0.009748382);
   h1D_asym__9->SetBinError(13,0.008633528);
   h1D_asym__9->SetBinError(14,0.007823829);
   h1D_asym__9->SetBinError(15,0.007308581);
   h1D_asym__9->SetBinError(16,0.006970805);
   h1D_asym__9->SetBinError(17,0.006559132);
   h1D_asym__9->SetBinError(18,0.006233119);
   h1D_asym__9->SetBinError(19,0.006100279);
   h1D_asym__9->SetBinError(20,0.005861104);
   h1D_asym__9->SetBinError(21,0.005551663);
   h1D_asym__9->SetBinError(22,0.005419274);
   h1D_asym__9->SetBinError(23,0.005242816);
   h1D_asym__9->SetBinError(24,0.005183334);
   h1D_asym__9->SetBinError(25,0.005079228);
   h1D_asym__9->SetBinError(26,0.005023693);
   h1D_asym__9->SetBinError(27,0.004884606);
   h1D_asym__9->SetBinError(28,0.004815965);
   h1D_asym__9->SetBinError(29,0.004738926);
   h1D_asym__9->SetBinError(30,0.004633047);
   h1D_asym__9->SetBinError(31,0.00457632);
   h1D_asym__9->SetBinError(32,0.004532662);
   h1D_asym__9->SetBinError(33,0.004483866);
   h1D_asym__9->SetBinError(34,0.004465952);
   h1D_asym__9->SetBinError(35,0.004407359);
   h1D_asym__9->SetBinError(36,0.004399006);
   h1D_asym__9->SetBinError(37,0.004359636);
   h1D_asym__9->SetBinError(38,0.004345404);
   h1D_asym__9->SetBinError(39,0.004330239);
   h1D_asym__9->SetBinError(40,0.004289207);
   h1D_asym__9->SetBinError(41,0.004288828);
   h1D_asym__9->SetBinError(42,0.004286619);
   h1D_asym__9->SetBinError(43,0.00429619);
   h1D_asym__9->SetBinError(44,0.004277295);
   h1D_asym__9->SetBinError(45,0.004322902);
   h1D_asym__9->SetBinError(46,0.004299174);
   h1D_asym__9->SetBinError(47,0.004333606);
   h1D_asym__9->SetBinError(48,0.00430668);
   h1D_asym__9->SetBinError(49,0.004349678);
   h1D_asym__9->SetBinError(50,0.004384454);
   h1D_asym__9->SetBinError(51,0.004370057);
   h1D_asym__9->SetBinError(52,0.004431759);
   h1D_asym__9->SetBinError(53,0.00443285);
   h1D_asym__9->SetBinError(54,0.004531613);
   h1D_asym__9->SetBinError(55,0.004528646);
   h1D_asym__9->SetBinError(56,0.004497198);
   h1D_asym__9->SetBinError(57,0.004534404);
   h1D_asym__9->SetBinError(58,0.004576511);
   h1D_asym__9->SetBinError(59,0.004606469);
   h1D_asym__9->SetBinError(60,0.004597246);
   h1D_asym__9->SetBinError(61,0.004647843);
   h1D_asym__9->SetBinError(62,0.004687566);
   h1D_asym__9->SetBinError(63,0.004633312);
   h1D_asym__9->SetBinError(64,0.004639606);
   h1D_asym__9->SetBinError(65,0.004630102);
   h1D_asym__9->SetBinError(66,0.004701338);
   h1D_asym__9->SetBinError(67,0.004709146);
   h1D_asym__9->SetBinError(68,0.004757458);
   h1D_asym__9->SetBinError(69,0.004773804);
   h1D_asym__9->SetBinError(70,0.004823634);
   h1D_asym__9->SetBinError(71,0.004867689);
   h1D_asym__9->SetBinError(72,0.004903027);
   h1D_asym__9->SetBinError(73,0.004889703);
   h1D_asym__9->SetBinError(74,0.004992879);
   h1D_asym__9->SetBinError(75,0.005073351);
   h1D_asym__9->SetBinError(76,0.005120153);
   h1D_asym__9->SetBinError(77,0.005204071);
   h1D_asym__9->SetBinError(78,0.005278768);
   h1D_asym__9->SetBinError(79,0.005398429);
   h1D_asym__9->SetBinError(80,0.005459073);
   h1D_asym__9->SetBinError(81,0.005605192);
   h1D_asym__9->SetBinError(82,0.005720997);
   h1D_asym__9->SetBinError(83,0.005923701);
   h1D_asym__9->SetBinError(84,0.006005418);
   h1D_asym__9->SetBinError(85,0.006114811);
   h1D_asym__9->SetBinError(86,0.00651807);
   h1D_asym__9->SetBinError(87,0.006793423);
   h1D_asym__9->SetBinError(88,0.007039966);
   h1D_asym__9->SetBinError(89,0.007323255);
   h1D_asym__9->SetBinError(90,0.007609394);
   h1D_asym__9->SetBinError(91,0.008077924);
   h1D_asym__9->SetBinError(92,0.008515399);
   h1D_asym__9->SetBinError(93,0.009395543);
   h1D_asym__9->SetBinError(94,0.01008752);
   h1D_asym__9->SetBinError(95,0.01057246);
   h1D_asym__9->SetBinError(96,0.01457202);
   h1D_asym__9->SetBinError(97,0.01576842);
   h1D_asym__9->SetBinError(98,0.01877107);
   h1D_asym__9->SetBinError(99,0.01744171);
   h1D_asym__9->SetBinError(100,0.01983898);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("#eta (D^{+})");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",2.3,4.1);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","-0.019653727082",2.3,4.1);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   Dplus_ETA_asym->cd();
   Dplus_ETA_asym->Modified();
   Dplus_ETA_asym->cd();
   Dplus_ETA_asym->SetSelected(Dplus_ETA_asym);
}
