void hplus_PY_asym()
{
//=========Macro generated from canvas: hplus_PY_asym/hplus_PY_asym
//=========  (Tue Nov 26 15:18:13 2019) by ROOT version6.08/02
   TCanvas *hplus_PY_asym = new TCanvas("hplus_PY_asym", "hplus_PY_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   hplus_PY_asym->SetHighLightColor(2);
   hplus_PY_asym->Range(0,0,1,1);
   hplus_PY_asym->SetFillColor(0);
   hplus_PY_asym->SetBorderMode(0);
   hplus_PY_asym->SetBorderSize(10);
   hplus_PY_asym->SetTickx(1);
   hplus_PY_asym->SetTicky(1);
   hplus_PY_asym->SetLeftMargin(0.18);
   hplus_PY_asym->SetRightMargin(0.05);
   hplus_PY_asym->SetTopMargin(0.07);
   hplus_PY_asym->SetBottomMargin(0.16);
   hplus_PY_asym->SetFrameFillStyle(0);
   hplus_PY_asym->SetFrameLineStyle(0);
   hplus_PY_asym->SetFrameLineColor(0);
   hplus_PY_asym->SetFrameLineWidth(0);
   hplus_PY_asym->SetFrameBorderMode(0);
   hplus_PY_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(-8250,-0.4547816,6750,0.04092051);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,-6000,6000);
   h1D_target_minus_toweight__8->SetBinContent(1,6.314337e-05);
   h1D_target_minus_toweight__8->SetBinContent(2,0.0001833578);
   h1D_target_minus_toweight__8->SetBinContent(3,0.0002638215);
   h1D_target_minus_toweight__8->SetBinContent(4,0.0003259143);
   h1D_target_minus_toweight__8->SetBinContent(5,0.0004746866);
   h1D_target_minus_toweight__8->SetBinContent(6,0.0005510397);
   h1D_target_minus_toweight__8->SetBinContent(7,-0.2819414);
   h1D_target_minus_toweight__8->SetBinContent(8,0.0008066993);
   h1D_target_minus_toweight__8->SetBinContent(9,-0.2903344);
   h1D_target_minus_toweight__8->SetBinContent(10,0.001170394);
   h1D_target_minus_toweight__8->SetBinContent(11,0.001341496);
   h1D_target_minus_toweight__8->SetBinContent(12,0.00161146);
   h1D_target_minus_toweight__8->SetBinContent(13,0.001866116);
   h1D_target_minus_toweight__8->SetBinContent(14,0.002111571);
   h1D_target_minus_toweight__8->SetBinContent(15,0.002332551);
   h1D_target_minus_toweight__8->SetBinContent(16,0.002736985);
   h1D_target_minus_toweight__8->SetBinContent(17,0.003179121);
   h1D_target_minus_toweight__8->SetBinContent(18,0.003517197);
   h1D_target_minus_toweight__8->SetBinContent(19,0.004032684);
   h1D_target_minus_toweight__8->SetBinContent(20,0.004622453);
   h1D_target_minus_toweight__8->SetBinContent(21,0.005344709);
   h1D_target_minus_toweight__8->SetBinContent(22,0.005936433);
   h1D_target_minus_toweight__8->SetBinContent(23,0.006680038);
   h1D_target_minus_toweight__8->SetBinContent(24,0.007646724);
   h1D_target_minus_toweight__8->SetBinContent(25,0.0086817);
   h1D_target_minus_toweight__8->SetBinContent(26,0.009866306);
   h1D_target_minus_toweight__8->SetBinContent(27,0.01113659);
   h1D_target_minus_toweight__8->SetBinContent(28,0.01252904);
   h1D_target_minus_toweight__8->SetBinContent(29,0.01409662);
   h1D_target_minus_toweight__8->SetBinContent(30,0.0161942);
   h1D_target_minus_toweight__8->SetBinContent(31,0.01786376);
   h1D_target_minus_toweight__8->SetBinContent(32,0.01979113);
   h1D_target_minus_toweight__8->SetBinContent(33,-0.2045044);
   h1D_target_minus_toweight__8->SetBinContent(34,0.02155613);
   h1D_target_minus_toweight__8->SetBinContent(35,0.02217863);
   h1D_target_minus_toweight__8->SetBinContent(36,0.02187846);
   h1D_target_minus_toweight__8->SetBinContent(37,0.02179567);
   h1D_target_minus_toweight__8->SetBinContent(38,0.01978462);
   h1D_target_minus_toweight__8->SetBinContent(39,0.01874767);
   h1D_target_minus_toweight__8->SetBinContent(40,0.01826604);
   h1D_target_minus_toweight__8->SetBinContent(41,0.0177936);
   h1D_target_minus_toweight__8->SetBinContent(42,0.01717733);
   h1D_target_minus_toweight__8->SetBinContent(43,0.01675785);
   h1D_target_minus_toweight__8->SetBinContent(44,0.01584593);
   h1D_target_minus_toweight__8->SetBinContent(45,0.01481777);
   h1D_target_minus_toweight__8->SetBinContent(46,0.01351243);
   h1D_target_minus_toweight__8->SetBinContent(47,0.01307579);
   h1D_target_minus_toweight__8->SetBinContent(48,0.01268888);
   h1D_target_minus_toweight__8->SetBinContent(49,0.01253735);
   h1D_target_minus_toweight__8->SetBinContent(50,0.01073929);
   h1D_target_minus_toweight__8->SetBinContent(51,0.01095339);
   h1D_target_minus_toweight__8->SetBinContent(52,0.01259217);
   h1D_target_minus_toweight__8->SetBinContent(53,0.01326441);
   h1D_target_minus_toweight__8->SetBinContent(54,0.01384497);
   h1D_target_minus_toweight__8->SetBinContent(55,0.01442845);
   h1D_target_minus_toweight__8->SetBinContent(56,0.01567748);
   h1D_target_minus_toweight__8->SetBinContent(57,0.01679106);
   h1D_target_minus_toweight__8->SetBinContent(58,0.01791293);
   h1D_target_minus_toweight__8->SetBinContent(59,0.01843725);
   h1D_target_minus_toweight__8->SetBinContent(60,0.01920024);
   h1D_target_minus_toweight__8->SetBinContent(61,0.01990396);
   h1D_target_minus_toweight__8->SetBinContent(62,0.02085135);
   h1D_target_minus_toweight__8->SetBinContent(63,0.02205632);
   h1D_target_minus_toweight__8->SetBinContent(64,0.02364521);
   h1D_target_minus_toweight__8->SetBinContent(65,0.02420541);
   h1D_target_minus_toweight__8->SetBinContent(66,0.02439711);
   h1D_target_minus_toweight__8->SetBinContent(67,0.02402957);
   h1D_target_minus_toweight__8->SetBinContent(68,0.0233442);
   h1D_target_minus_toweight__8->SetBinContent(69,0.0217367);
   h1D_target_minus_toweight__8->SetBinContent(70,0.01971567);
   h1D_target_minus_toweight__8->SetBinContent(71,0.01769661);
   h1D_target_minus_toweight__8->SetBinContent(72,0.01565928);
   h1D_target_minus_toweight__8->SetBinContent(73,0.01378499);
   h1D_target_minus_toweight__8->SetBinContent(74,-0.1870583);
   h1D_target_minus_toweight__8->SetBinContent(75,0.01080958);
   h1D_target_minus_toweight__8->SetBinContent(76,0.009436402);
   h1D_target_minus_toweight__8->SetBinContent(77,0.008335262);
   h1D_target_minus_toweight__8->SetBinContent(78,0.007364528);
   h1D_target_minus_toweight__8->SetBinContent(79,0.006360138);
   h1D_target_minus_toweight__8->SetBinContent(80,0.005732752);
   h1D_target_minus_toweight__8->SetBinContent(81,0.005092099);
   h1D_target_minus_toweight__8->SetBinContent(82,0.004394221);
   h1D_target_minus_toweight__8->SetBinContent(83,0.003944125);
   h1D_target_minus_toweight__8->SetBinContent(84,0.003489843);
   h1D_target_minus_toweight__8->SetBinContent(85,0.002936631);
   h1D_target_minus_toweight__8->SetBinContent(86,0.002601534);
   h1D_target_minus_toweight__8->SetBinContent(87,0.002215432);
   h1D_target_minus_toweight__8->SetBinContent(88,0.001957769);
   h1D_target_minus_toweight__8->SetBinContent(89,0.001664399);
   h1D_target_minus_toweight__8->SetBinContent(90,0.001500407);
   h1D_target_minus_toweight__8->SetBinContent(91,0.001212104);
   h1D_target_minus_toweight__8->SetBinContent(92,0.001094019);
   h1D_target_minus_toweight__8->SetBinContent(93,0.0008525609);
   h1D_target_minus_toweight__8->SetBinContent(94,0.0007221836);
   h1D_target_minus_toweight__8->SetBinContent(95,0.0005744752);
   h1D_target_minus_toweight__8->SetBinContent(96,0.000449163);
   h1D_target_minus_toweight__8->SetBinContent(97,0.000327956);
   h1D_target_minus_toweight__8->SetBinContent(98,0.0002709499);
   h1D_target_minus_toweight__8->SetBinContent(99,0.0001721388);
   h1D_target_minus_toweight__8->SetBinContent(100,8.760191e-05);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("p_{Y} (h^{+}) [MeV/c]");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{+} #rightarrow K_{S}^{0} h^{+})#minusN(D^{+} #rightarrow K^{#minus} #pi^{+} h^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",-6000,6000);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   hplus_PY_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(-8250,-2166722,6750,2659160);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,-6000,6000);
   h1D_asym__9->SetBinContent(1,0.0186296);
   h1D_asym__9->SetBinContent(2,-0.05001612);
   h1D_asym__9->SetBinContent(3,-0.02980375);
   h1D_asym__9->SetBinContent(4,-0.01345844);
   h1D_asym__9->SetBinContent(5,-0.02006562);
   h1D_asym__9->SetBinContent(6,-0.007897691);
   h1D_asym__9->SetBinContent(7,0.999998);
   h1D_asym__9->SetBinContent(8,-0.01819377);
   h1D_asym__9->SetBinContent(9,-0.9999974);
   h1D_asym__9->SetBinContent(10,-0.005116132);
   h1D_asym__9->SetBinContent(11,-0.02198437);
   h1D_asym__9->SetBinContent(12,-0.02057978);
   h1D_asym__9->SetBinContent(13,-0.01255493);
   h1D_asym__9->SetBinContent(14,-0.01524086);
   h1D_asym__9->SetBinContent(15,0.000803948);
   h1D_asym__9->SetBinContent(16,0.002639108);
   h1D_asym__9->SetBinContent(17,-0.002474557);
   h1D_asym__9->SetBinContent(18,-0.02082668);
   h1D_asym__9->SetBinContent(19,-0.01577835);
   h1D_asym__9->SetBinContent(20,-0.003599182);
   h1D_asym__9->SetBinContent(21,-0.01456415);
   h1D_asym__9->SetBinContent(22,0.0005309544);
   h1D_asym__9->SetBinContent(23,-0.01037454);
   h1D_asym__9->SetBinContent(24,-0.006942592);
   h1D_asym__9->SetBinContent(25,-0.007466254);
   h1D_asym__9->SetBinContent(26,-0.006383154);
   h1D_asym__9->SetBinContent(27,-0.006811826);
   h1D_asym__9->SetBinContent(28,-0.003456732);
   h1D_asym__9->SetBinContent(29,-0.01161403);
   h1D_asym__9->SetBinContent(30,-0.007472787);
   h1D_asym__9->SetBinContent(31,0.0007553395);
   h1D_asym__9->SetBinContent(32,-0.006166359);
   h1D_asym__9->SetBinContent(33,-0.9999309);
   h1D_asym__9->SetBinContent(34,-0.00171788);
   h1D_asym__9->SetBinContent(35,-0.000756282);
   h1D_asym__9->SetBinContent(36,0.9619254);
   h1D_asym__9->SetBinContent(37,-0.005539823);
   h1D_asym__9->SetBinContent(38,0.001607548);
   h1D_asym__9->SetBinContent(39,-0.004007442);
   h1D_asym__9->SetBinContent(40,-0.005496201);
   h1D_asym__9->SetBinContent(41,-0.004625134);
   h1D_asym__9->SetBinContent(42,-0.007226272);
   h1D_asym__9->SetBinContent(43,-0.002607287);
   h1D_asym__9->SetBinContent(44,-0.002008466);
   h1D_asym__9->SetBinContent(45,-0.005366829);
   h1D_asym__9->SetBinContent(46,-0.01322274);
   h1D_asym__9->SetBinContent(47,0.001081125);
   h1D_asym__9->SetBinContent(48,-0.004467395);
   h1D_asym__9->SetBinContent(49,-0.002740259);
   h1D_asym__9->SetBinContent(50,-0.00961091);
   h1D_asym__9->SetBinContent(51,-0.0009825671);
   h1D_asym__9->SetBinContent(52,0.0006143057);
   h1D_asym__9->SetBinContent(53,0.003558115);
   h1D_asym__9->SetBinContent(54,0.000848399);
   h1D_asym__9->SetBinContent(55,-0.002684167);
   h1D_asym__9->SetBinContent(56,-0.004359534);
   h1D_asym__9->SetBinContent(57,-0.001255976);
   h1D_asym__9->SetBinContent(58,0.00640124);
   h1D_asym__9->SetBinContent(59,-0.003117318);
   h1D_asym__9->SetBinContent(60,0.001771991);
   h1D_asym__9->SetBinContent(61,-0.001077007);
   h1D_asym__9->SetBinContent(62,-0.00521328);
   h1D_asym__9->SetBinContent(63,-0.003051971);
   h1D_asym__9->SetBinContent(64,-0.004636915);
   h1D_asym__9->SetBinContent(65,-0.0001899706);
   h1D_asym__9->SetBinContent(66,-0.003424627);
   h1D_asym__9->SetBinContent(67,-0.005278432);
   h1D_asym__9->SetBinContent(68,-0.001763149);
   h1D_asym__9->SetBinContent(69,-0.005962013);
   h1D_asym__9->SetBinContent(70,-0.005914555);
   h1D_asym__9->SetBinContent(71,-0.004460569);
   h1D_asym__9->SetBinContent(72,0.001891572);
   h1D_asym__9->SetBinContent(73,0.0002605343);
   h1D_asym__9->SetBinContent(74,0.999953);
   h1D_asym__9->SetBinContent(75,0.0001874816);
   h1D_asym__9->SetBinContent(76,-0.003031462);
   h1D_asym__9->SetBinContent(77,0.00884711);
   h1D_asym__9->SetBinContent(78,-0.00741902);
   h1D_asym__9->SetBinContent(79,-0.004511894);
   h1D_asym__9->SetBinContent(80,-0.007050999);
   h1D_asym__9->SetBinContent(81,-0.00464469);
   h1D_asym__9->SetBinContent(82,-0.002656222);
   h1D_asym__9->SetBinContent(83,-0.007629869);
   h1D_asym__9->SetBinContent(84,-0.01963437);
   h1D_asym__9->SetBinContent(85,-0.01478357);
   h1D_asym__9->SetBinContent(86,-0.004549265);
   h1D_asym__9->SetBinContent(87,-0.002461648);
   h1D_asym__9->SetBinContent(88,-0.005622391);
   h1D_asym__9->SetBinContent(89,0.01869449);
   h1D_asym__9->SetBinContent(90,-0.003854531);
   h1D_asym__9->SetBinContent(91,0.001859532);
   h1D_asym__9->SetBinContent(92,0.01687746);
   h1D_asym__9->SetBinContent(93,-0.007046901);
   h1D_asym__9->SetBinContent(94,0.03367017);
   h1D_asym__9->SetBinContent(95,-0.01026916);
   h1D_asym__9->SetBinContent(96,-0.01381199);
   h1D_asym__9->SetBinContent(97,-0.01962702);
   h1D_asym__9->SetBinContent(98,0.03819565);
   h1D_asym__9->SetBinContent(99,-0.03279815);
   h1D_asym__9->SetBinContent(100,-0.02280695);
   h1D_asym__9->SetBinError(1,0.1475467);
   h1D_asym__9->SetBinError(2,0.08824738);
   h1D_asym__9->SetBinError(3,0.07848375);
   h1D_asym__9->SetBinError(4,0.07889939);
   h1D_asym__9->SetBinError(5,0.06029183);
   h1D_asym__9->SetBinError(6,0.05444984);
   h1D_asym__9->SetBinError(7,1969748);
   h1D_asym__9->SetBinError(8,0.04228081);
   h1D_asym__9->SetBinError(9,5.524576e-08);
   h1D_asym__9->SetBinError(10,0.0374264);
   h1D_asym__9->SetBinError(11,0.03392595);
   h1D_asym__9->SetBinError(12,0.03463515);
   h1D_asym__9->SetBinError(13,0.02889478);
   h1D_asym__9->SetBinError(14,0.02807669);
   h1D_asym__9->SetBinError(15,0.02690493);
   h1D_asym__9->SetBinError(16,0.02730397);
   h1D_asym__9->SetBinError(17,0.02282242);
   h1D_asym__9->SetBinError(18,0.02021162);
   h1D_asym__9->SetBinError(19,0.01923483);
   h1D_asym__9->SetBinError(20,0.01899404);
   h1D_asym__9->SetBinError(21,0.01730377);
   h1D_asym__9->SetBinError(22,0.01700597);
   h1D_asym__9->SetBinError(23,0.01532935);
   h1D_asym__9->SetBinError(24,0.01447618);
   h1D_asym__9->SetBinError(25,0.01380853);
   h1D_asym__9->SetBinError(26,0.01294808);
   h1D_asym__9->SetBinError(27,0.01208832);
   h1D_asym__9->SetBinError(28,0.01187702);
   h1D_asym__9->SetBinError(29,0.01083018);
   h1D_asym__9->SetBinError(30,0.01015175);
   h1D_asym__9->SetBinError(31,0.00963972);
   h1D_asym__9->SetBinError(32,0.008976194);
   h1D_asym__9->SetBinError(33,2.97669e-07);
   h1D_asym__9->SetBinError(34,0.008032209);
   h1D_asym__9->SetBinError(35,0.007899987);
   h1D_asym__9->SetBinError(36,101.0894);
   h1D_asym__9->SetBinError(37,0.00749894);
   h1D_asym__9->SetBinError(38,0.008028178);
   h1D_asym__9->SetBinError(39,0.00805262);
   h1D_asym__9->SetBinError(40,0.008252132);
   h1D_asym__9->SetBinError(41,0.00837192);
   h1D_asym__9->SetBinError(42,0.008462311);
   h1D_asym__9->SetBinError(43,0.008664909);
   h1D_asym__9->SetBinError(44,0.008761126);
   h1D_asym__9->SetBinError(45,0.00889419);
   h1D_asym__9->SetBinError(46,0.008899587);
   h1D_asym__9->SetBinError(47,0.009601653);
   h1D_asym__9->SetBinError(48,0.009799708);
   h1D_asym__9->SetBinError(49,0.01017636);
   h1D_asym__9->SetBinError(50,0.01076495);
   h1D_asym__9->SetBinError(51,0.01115625);
   h1D_asym__9->SetBinError(52,0.01037213);
   h1D_asym__9->SetBinError(53,0.00988013);
   h1D_asym__9->SetBinError(54,0.009500708);
   h1D_asym__9->SetBinError(55,0.009069112);
   h1D_asym__9->SetBinError(56,0.008841645);
   h1D_asym__9->SetBinError(57,0.008597707);
   h1D_asym__9->SetBinError(58,0.008658342);
   h1D_asym__9->SetBinError(59,0.008339486);
   h1D_asym__9->SetBinError(60,0.008645254);
   h1D_asym__9->SetBinError(61,0.008049157);
   h1D_asym__9->SetBinError(62,0.007815275);
   h1D_asym__9->SetBinError(63,0.007661154);
   h1D_asym__9->SetBinError(64,0.007240568);
   h1D_asym__9->SetBinError(65,0.007307753);
   h1D_asym__9->SetBinError(66,0.007441422);
   h1D_asym__9->SetBinError(67,0.007726491);
   h1D_asym__9->SetBinError(68,0.008238438);
   h1D_asym__9->SetBinError(69,0.008679107);
   h1D_asym__9->SetBinError(70,0.009297024);
   h1D_asym__9->SetBinError(71,0.009899119);
   h1D_asym__9->SetBinError(72,0.01053039);
   h1D_asym__9->SetBinError(73,0.01139044);
   h1D_asym__9->SetBinError(74,85108.33);
   h1D_asym__9->SetBinError(75,0.01254604);
   h1D_asym__9->SetBinError(76,0.01347322);
   h1D_asym__9->SetBinError(77,0.01731489);
   h1D_asym__9->SetBinError(78,0.01554812);
   h1D_asym__9->SetBinError(79,0.01616006);
   h1D_asym__9->SetBinError(80,0.01726573);
   h1D_asym__9->SetBinError(81,0.01817732);
   h1D_asym__9->SetBinError(82,0.0197899);
   h1D_asym__9->SetBinError(83,0.02050734);
   h1D_asym__9->SetBinError(84,0.02092309);
   h1D_asym__9->SetBinError(85,0.02312879);
   h1D_asym__9->SetBinError(86,0.02575094);
   h1D_asym__9->SetBinError(87,0.02860425);
   h1D_asym__9->SetBinError(88,0.02900059);
   h1D_asym__9->SetBinError(89,0.03645745);
   h1D_asym__9->SetBinError(90,0.03726345);
   h1D_asym__9->SetBinError(91,0.03879805);
   h1D_asym__9->SetBinError(92,0.04137495);
   h1D_asym__9->SetBinError(93,0.04733385);
   h1D_asym__9->SetBinError(94,0.05527717);
   h1D_asym__9->SetBinError(95,0.05320184);
   h1D_asym__9->SetBinError(96,0.06008989);
   h1D_asym__9->SetBinError(97,0.06567713);
   h1D_asym__9->SetBinError(98,0.08558348);
   h1D_asym__9->SetBinError(99,0.09736697);
   h1D_asym__9->SetBinError(100,0.1499086);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("p_{Y} (h^{+}) [MeV/c]");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",-6000,6000);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","-0.012611200712",-6000,6000);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   hplus_PY_asym->cd();
   hplus_PY_asym->Modified();
   hplus_PY_asym->cd();
   hplus_PY_asym->SetSelected(hplus_PY_asym);
}
