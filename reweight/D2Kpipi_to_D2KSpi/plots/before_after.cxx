#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"
//#include <thread>
//#include <string>

string convertToString(const char* a, int size) 
{ 
  int i; 
  string s = ""; 
  for (i = 0; i <= size; i++) { 
    s = s + a[i]; 
  } 
  return s; 
} 
void before_after(TString year = "16", TString polarity = "Dw", string variable = "") {

  TString tmp = variable.c_str();
  tmp.ReplaceAll("__",",");
  variable = convertToString((const char*)tmp, sizeof(tmp));
  dcastyle();

  //bool test = 1;
  bool isPGun = 1;
  bool use_ALL = 0;
  bool rew_X = 0;
  TString add_string = "";
    if (isPGun) {
    add_string += "_pGun";
    if (use_ALL)
      add_string += "_ALL";
  }
    if (rew_X)
      add_string += "_X";

  TString toweight_tree_inFileName = "/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip_"+year+"_"+polarity+add_string+".root";
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add(toweight_tree_inFileName);
  toweight_tree->AddFriend("ntp",toweight_tree_inFileName.ReplaceAll(".root","_SPlot.root"));
  //  toweight_tree->AddFriend("ntp",toweight_tree_inFileName.ReplaceAll("SPlot","rew"+add_string));
  toweight_tree->AddFriend("ntp",toweight_tree_inFileName.ReplaceAll("SPlot","rew"));
  
  TString target_tree_inFileName = "/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2KS0pipLL_"+year+"_"+polarity+add_string+".root";
  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add(target_tree_inFileName);
  target_tree->AddFriend("ntp",target_tree_inFileName.ReplaceAll(".root","_SPlot.root"));
  
  IDecay* toweight_decay;
  if(isPGun)
    toweight_decay = new IDecay(toweight_tree,"Dp2Kmpippip","D^{+} #rightarrow K^{#minus} #pi^{+} h^{+}",{"trueLabels"/**/},toweight_tree->GetEntries(),0);
  else
    toweight_decay = new IDecay(toweight_tree,"Dp2Kmpippip","D^{+} #rightarrow K^{#minus} #pi^{+} h^{+}",{"N_sigD_sw"},toweight_tree->GetEntries(),0);
  
  IDecay* target_decay;
  if(isPGun)
    target_decay  = new IDecay(target_tree,"Dp2KS0pip","D^{+} #rightarrow K_{S}^{0} h^{+}",{/**/},target_tree->GetEntries(),0);
  else
    target_decay  = new IDecay(target_tree,"Dp2KS0pip","D^{+} #rightarrow K_{S}^{0} h^{+}",{"N_sigD_sw"},target_tree->GetEntries(),0);
  
  if (variable == "") {
    toweight_decay->addTargetDistribution(target_decay,{"X_PT(100,2.2e3,14e3)"},5);//5
    toweight_decay->addTargetDistribution(target_decay,{"X_ETA(100,2,4.4)"},5);//5
    toweight_decay->addTargetDistribution(target_decay,{"X_PHI(100,-3.1416,3.1416)"},15);
    toweight_decay->addTargetDistribution(target_decay,{"X_P(100,0,250e3)"},5);

    toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(100,4e3,14e3)"},5);
    toweight_decay->addTargetDistribution(target_decay,{"Dplus_ETA(100,2.2,4.3)"},5);
    toweight_decay->addTargetDistribution(target_decay,{"Dplus_PHI(100,-3.1416,3.1416)"},15);
    toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(100,1.5e3,8.5e3)"},5);//5
    toweight_decay->addTargetDistribution(target_decay,{"hplus_ETA(100,2.2,4.3)"},5);//5
    toweight_decay->addTargetDistribution(target_decay,{"hplus_PHI(100,-3.1416,3.1416)"},15);

    toweight_decay->addTargetDistribution(target_decay,{"Dplus_PY(100,-14e3,14e3)"},5);
    toweight_decay->addTargetDistribution(target_decay,{"hplus_PY(100,-8.5e3,8.5e3)"},5);
    toweight_decay->addTargetDistribution(target_decay,{"Dplus_PX(100,-14e3,14e3)"},5);
    toweight_decay->addTargetDistribution(target_decay,{"hplus_PX(100,-8.5e3,8.5e3)"},5);
    toweight_decay->addTargetDistribution(target_decay,{"Dplus_PZ(100,15e3,250e3)"},5);
    toweight_decay->addTargetDistribution(target_decay,{"hplus_PZ(100,5e3,150e3)"},5);
  }
  else {
    toweight_decay->addTargetDistribution(target_decay,{variable},5);
  }

  toweight_decay->setAsymVariable("Dplus_ID");
  target_decay->setAsymVariable("Dplus_ID");

  toweight_decay->calculateResidualAsymmetry = 1;
  
  toweight_decay->fillTargets();
 
  if(!rew_X)
    toweight_decay->use_Iw_max = 0;
  toweight_decay->Iw_max = 15;
  toweight_decay->Iw_max_value = 0;

  toweight_decay->setIWeight("Iw"); //from rew.root

  toweight_decay->plotDir = year+"_"+polarity+add_string+"/";
  toweight_decay->showVariables();
}


int main(int argc, char * argv[]) {
  before_after(argv[1],argv[2],argv[3]);
  return 0;
}
