#!/bin/bash
g++ -Wall -o _before_after before_after.cxx `root-config --cflags --glibs` `gsl-config --cflags --libs`

DIR=$PWD

#for year in $(echo 15 16 17 18); do
#    for pol in $(echo Dw Up); do
for year in $(echo 16); do
    for pol in $(echo Dw); do
    #for pol in $(echo Up); do
	for variable in $(echo X_PT__100__2080__9080__ X_ETA__100__2__4.4__ X_PHI__100__-3.1416__3.1416__ X_P__100__0__250000__  Dplus_PT__100__4200__11000__ Dplus_ETA__100__2.3__4.1__ Dplus_PHI__100__-3.1416__3.1416__ hplus_PT__100__1600__6000__ hplus_ETA__100__2.2__4.1__ hplus_PHI__100__-3.1416__3.1416__  Dplus_PY__100__-11000__11000__ hplus_PY__100__-6000__6000__ Dplus_PX__100__-11000__11000__ hplus_PX__100__-6000__6000__ Dplus_PZ__100__30000__250000__ hplus_PZ__100__5000__150000__); do
	    echo "#!/bin/sh" > exec/$year.$pol.$variable.sh
	    echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> exec/$year.$pol.$variable.sh 
	    echo "cd $DIR" >> exec/$year.$pol.$variable.sh
	    echo "hostname" >> exec/$year.$pol.$variable.sh
	    #echo "time ./_before_after $year $pol $variable > logs/X_log_${year}_${pol}_${variable}.txt" >> exec/$year.$pol.$variable.sh
	    echo "time ./_before_after $year $pol $variable > logs/log_${year}_${pol}_${variable}.txt" >> exec/$year.$pol.$variable.sh
	    chmod +x exec/$year.$pol.$variable.sh
	    python condor_make_template.py condor_template.sub $year.$pol.$variable
	    
	    condor_submit exec/condor_template.$year.$pol.$variable.sub -batch-name $year$pol$variable

	done		  	
    done
done


#X_PT__100__2080__9080__ X_ETA__100__2__4.4__ X_PHI__100__-3.1416__3.1416__ X_P__100__0__250000__  Dplus_PT__100__4200__11000__ Dplus_ETA__100__2.3__4.1__ Dplus_PHI__100__-3.1416__3.1416__ hplus_PT__100__1600__6000__ hplus_ETA__100__2.2__4.1__ hplus_PHI__100__-3.1416__3.1416__  Dplus_PY__100__-11000__11000__ hplus_PY__100__-6000__6000__ Dplus_PX__100__-11000__11000__ hplus_PX__100__-6000__6000__ Dplus_PZ__100__30000__250000__ hplus_PZ__100__5000__150000__
