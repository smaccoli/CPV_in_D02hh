void Dplus_PY_asym()
{
//=========Macro generated from canvas: Dplus_PY_asym/Dplus_PY_asym
//=========  (Thu Nov  7 16:07:48 2019) by ROOT version6.08/02
   TCanvas *Dplus_PY_asym = new TCanvas("Dplus_PY_asym", "Dplus_PY_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   Dplus_PY_asym->SetHighLightColor(2);
   Dplus_PY_asym->Range(0,0,1,1);
   Dplus_PY_asym->SetFillColor(0);
   Dplus_PY_asym->SetBorderMode(0);
   Dplus_PY_asym->SetBorderSize(10);
   Dplus_PY_asym->SetTickx(1);
   Dplus_PY_asym->SetTicky(1);
   Dplus_PY_asym->SetLeftMargin(0.18);
   Dplus_PY_asym->SetRightMargin(0.05);
   Dplus_PY_asym->SetTopMargin(0.07);
   Dplus_PY_asym->SetBottomMargin(0.16);
   Dplus_PY_asym->SetFrameFillStyle(0);
   Dplus_PY_asym->SetFrameLineStyle(0);
   Dplus_PY_asym->SetFrameLineColor(0);
   Dplus_PY_asym->SetFrameLineWidth(0);
   Dplus_PY_asym->SetFrameBorderMode(0);
   Dplus_PY_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(-19250,-0.006587432,15750,0.002844495);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,-14000,14000);
   h1D_target_minus_toweight__8->SetBinContent(1,1.402534e-05);
   h1D_target_minus_toweight__8->SetBinContent(2,4.850857e-05);
   h1D_target_minus_toweight__8->SetBinContent(3,8.738876e-05);
   h1D_target_minus_toweight__8->SetBinContent(4,0.0001016297);
   h1D_target_minus_toweight__8->SetBinContent(5,0.0001662585);
   h1D_target_minus_toweight__8->SetBinContent(6,0.0002326914);
   h1D_target_minus_toweight__8->SetBinContent(7,0.0002439566);
   h1D_target_minus_toweight__8->SetBinContent(8,0.0002840437);
   h1D_target_minus_toweight__8->SetBinContent(9,0.0003451281);
   h1D_target_minus_toweight__8->SetBinContent(10,0.0004060199);
   h1D_target_minus_toweight__8->SetBinContent(11,0.0005262252);
   h1D_target_minus_toweight__8->SetBinContent(12,0.000613568);
   h1D_target_minus_toweight__8->SetBinContent(13,0.0006081951);
   h1D_target_minus_toweight__8->SetBinContent(14,0.0007424158);
   h1D_target_minus_toweight__8->SetBinContent(15,0.0007917075);
   h1D_target_minus_toweight__8->SetBinContent(16,0.0008600848);
   h1D_target_minus_toweight__8->SetBinContent(17,0.001120025);
   h1D_target_minus_toweight__8->SetBinContent(18,0.00103255);
   h1D_target_minus_toweight__8->SetBinContent(19,0.001340485);
   h1D_target_minus_toweight__8->SetBinContent(20,0.001542659);
   h1D_target_minus_toweight__8->SetBinContent(21,0.001518504);
   h1D_target_minus_toweight__8->SetBinContent(22,0.001692997);
   h1D_target_minus_toweight__8->SetBinContent(23,0.001826486);
   h1D_target_minus_toweight__8->SetBinContent(24,0.001962641);
   h1D_target_minus_toweight__8->SetBinContent(25,0.002092207);
   h1D_target_minus_toweight__8->SetBinContent(26,0.002179411);
   h1D_target_minus_toweight__8->SetBinContent(27,0.002081588);
   h1D_target_minus_toweight__8->SetBinContent(28,0.002054471);
   h1D_target_minus_toweight__8->SetBinContent(29,0.001992647);
   h1D_target_minus_toweight__8->SetBinContent(30,0.00137756);
   h1D_target_minus_toweight__8->SetBinContent(31,0.0009296276);
   h1D_target_minus_toweight__8->SetBinContent(32,2.552196e-05);
   h1D_target_minus_toweight__8->SetBinContent(33,-0.001029419);
   h1D_target_minus_toweight__8->SetBinContent(34,-0.0018616);
   h1D_target_minus_toweight__8->SetBinContent(35,-0.003103854);
   h1D_target_minus_toweight__8->SetBinContent(36,-0.002924209);
   h1D_target_minus_toweight__8->SetBinContent(37,-0.00159568);
   h1D_target_minus_toweight__8->SetBinContent(38,-0.001274206);
   h1D_target_minus_toweight__8->SetBinContent(39,-0.001268998);
   h1D_target_minus_toweight__8->SetBinContent(40,-0.0007204469);
   h1D_target_minus_toweight__8->SetBinContent(41,-0.001627224);
   h1D_target_minus_toweight__8->SetBinContent(42,-0.00112265);
   h1D_target_minus_toweight__8->SetBinContent(43,-0.001478806);
   h1D_target_minus_toweight__8->SetBinContent(44,-0.002561293);
   h1D_target_minus_toweight__8->SetBinContent(45,-0.003006627);
   h1D_target_minus_toweight__8->SetBinContent(46,-0.002582621);
   h1D_target_minus_toweight__8->SetBinContent(47,-0.00223565);
   h1D_target_minus_toweight__8->SetBinContent(48,-0.001203591);
   h1D_target_minus_toweight__8->SetBinContent(49,-0.0004861578);
   h1D_target_minus_toweight__8->SetBinContent(50,-0.0005786363);
   h1D_target_minus_toweight__8->SetBinContent(51,-0.0004214067);
   h1D_target_minus_toweight__8->SetBinContent(52,-0.0004743319);
   h1D_target_minus_toweight__8->SetBinContent(53,-0.001131726);
   h1D_target_minus_toweight__8->SetBinContent(54,-0.002228879);
   h1D_target_minus_toweight__8->SetBinContent(55,-0.002579441);
   h1D_target_minus_toweight__8->SetBinContent(56,-0.002946259);
   h1D_target_minus_toweight__8->SetBinContent(57,-0.002393107);
   h1D_target_minus_toweight__8->SetBinContent(58,-0.00205554);
   h1D_target_minus_toweight__8->SetBinContent(59,-0.001721475);
   h1D_target_minus_toweight__8->SetBinContent(60,-0.001286831);
   h1D_target_minus_toweight__8->SetBinContent(61,-0.001219539);
   h1D_target_minus_toweight__8->SetBinContent(62,-0.001452817);
   h1D_target_minus_toweight__8->SetBinContent(63,-0.001787819);
   h1D_target_minus_toweight__8->SetBinContent(64,-0.001852702);
   h1D_target_minus_toweight__8->SetBinContent(65,-0.002419384);
   h1D_target_minus_toweight__8->SetBinContent(66,-0.003240213);
   h1D_target_minus_toweight__8->SetBinContent(67,-0.002138779);
   h1D_target_minus_toweight__8->SetBinContent(68,-0.0007938035);
   h1D_target_minus_toweight__8->SetBinContent(69,0.0002705026);
   h1D_target_minus_toweight__8->SetBinContent(70,0.001129266);
   h1D_target_minus_toweight__8->SetBinContent(71,0.001628328);
   h1D_target_minus_toweight__8->SetBinContent(72,0.002050456);
   h1D_target_minus_toweight__8->SetBinContent(73,0.001968564);
   h1D_target_minus_toweight__8->SetBinContent(74,0.002350319);
   h1D_target_minus_toweight__8->SetBinContent(75,0.002278869);
   h1D_target_minus_toweight__8->SetBinContent(76,0.002133931);
   h1D_target_minus_toweight__8->SetBinContent(77,0.002100103);
   h1D_target_minus_toweight__8->SetBinContent(78,0.002000477);
   h1D_target_minus_toweight__8->SetBinContent(79,0.001606052);
   h1D_target_minus_toweight__8->SetBinContent(80,0.001717858);
   h1D_target_minus_toweight__8->SetBinContent(81,0.001327652);
   h1D_target_minus_toweight__8->SetBinContent(82,0.001213647);
   h1D_target_minus_toweight__8->SetBinContent(83,0.001141724);
   h1D_target_minus_toweight__8->SetBinContent(84,0.001150525);
   h1D_target_minus_toweight__8->SetBinContent(85,0.0009964188);
   h1D_target_minus_toweight__8->SetBinContent(86,0.0008286315);
   h1D_target_minus_toweight__8->SetBinContent(87,0.0007595258);
   h1D_target_minus_toweight__8->SetBinContent(88,0.0005713871);
   h1D_target_minus_toweight__8->SetBinContent(89,0.0004935785);
   h1D_target_minus_toweight__8->SetBinContent(90,0.0004341661);
   h1D_target_minus_toweight__8->SetBinContent(91,0.000409761);
   h1D_target_minus_toweight__8->SetBinContent(92,0.000297427);
   h1D_target_minus_toweight__8->SetBinContent(93,0.0002117404);
   h1D_target_minus_toweight__8->SetBinContent(94,0.0002840158);
   h1D_target_minus_toweight__8->SetBinContent(95,0.0002214884);
   h1D_target_minus_toweight__8->SetBinContent(96,0.0001139876);
   h1D_target_minus_toweight__8->SetBinContent(97,9.106145e-05);
   h1D_target_minus_toweight__8->SetBinContent(98,6.26197e-05);
   h1D_target_minus_toweight__8->SetBinContent(99,8.0941e-05);
   h1D_target_minus_toweight__8->SetBinContent(100,3.946574e-05);
   h1D_target_minus_toweight__8->SetBinError(1,9.702571e-06);
   h1D_target_minus_toweight__8->SetBinError(2,1.676824e-05);
   h1D_target_minus_toweight__8->SetBinError(3,2.163082e-05);
   h1D_target_minus_toweight__8->SetBinError(4,2.486317e-05);
   h1D_target_minus_toweight__8->SetBinError(5,3.045537e-05);
   h1D_target_minus_toweight__8->SetBinError(6,3.405749e-05);
   h1D_target_minus_toweight__8->SetBinError(7,3.779604e-05);
   h1D_target_minus_toweight__8->SetBinError(8,4.183652e-05);
   h1D_target_minus_toweight__8->SetBinError(9,4.616207e-05);
   h1D_target_minus_toweight__8->SetBinError(10,5.048649e-05);
   h1D_target_minus_toweight__8->SetBinError(11,5.667433e-05);
   h1D_target_minus_toweight__8->SetBinError(12,6.1295e-05);
   h1D_target_minus_toweight__8->SetBinError(13,6.626031e-05);
   h1D_target_minus_toweight__8->SetBinError(14,7.207929e-05);
   h1D_target_minus_toweight__8->SetBinError(15,7.757611e-05);
   h1D_target_minus_toweight__8->SetBinError(16,8.387332e-05);
   h1D_target_minus_toweight__8->SetBinError(17,9.083522e-05);
   h1D_target_minus_toweight__8->SetBinError(18,9.615522e-05);
   h1D_target_minus_toweight__8->SetBinError(19,0.0001048411);
   h1D_target_minus_toweight__8->SetBinError(20,0.0001134801);
   h1D_target_minus_toweight__8->SetBinError(21,0.0001205646);
   h1D_target_minus_toweight__8->SetBinError(22,0.0001282003);
   h1D_target_minus_toweight__8->SetBinError(23,0.0001371033);
   h1D_target_minus_toweight__8->SetBinError(24,0.000146607);
   h1D_target_minus_toweight__8->SetBinError(25,0.0001556936);
   h1D_target_minus_toweight__8->SetBinError(26,0.0001655594);
   h1D_target_minus_toweight__8->SetBinError(27,0.0001752233);
   h1D_target_minus_toweight__8->SetBinError(28,0.0001856732);
   h1D_target_minus_toweight__8->SetBinError(29,0.0001956686);
   h1D_target_minus_toweight__8->SetBinError(30,0.0002042186);
   h1D_target_minus_toweight__8->SetBinError(31,0.0002124521);
   h1D_target_minus_toweight__8->SetBinError(32,0.0002170086);
   h1D_target_minus_toweight__8->SetBinError(33,0.0002202388);
   h1D_target_minus_toweight__8->SetBinError(34,0.0002195074);
   h1D_target_minus_toweight__8->SetBinError(35,0.0002131078);
   h1D_target_minus_toweight__8->SetBinError(36,0.0002062089);
   h1D_target_minus_toweight__8->SetBinError(37,0.0002038602);
   h1D_target_minus_toweight__8->SetBinError(38,0.00019973);
   h1D_target_minus_toweight__8->SetBinError(39,0.0001952382);
   h1D_target_minus_toweight__8->SetBinError(40,0.0001933715);
   h1D_target_minus_toweight__8->SetBinError(41,0.000186582);
   h1D_target_minus_toweight__8->SetBinError(42,0.0001864552);
   h1D_target_minus_toweight__8->SetBinError(43,0.00018239);
   h1D_target_minus_toweight__8->SetBinError(44,0.0001734471);
   h1D_target_minus_toweight__8->SetBinError(45,0.0001663404);
   h1D_target_minus_toweight__8->SetBinError(46,0.000161874);
   h1D_target_minus_toweight__8->SetBinError(47,0.0001567003);
   h1D_target_minus_toweight__8->SetBinError(48,0.0001562375);
   h1D_target_minus_toweight__8->SetBinError(49,0.0001548873);
   h1D_target_minus_toweight__8->SetBinError(50,0.0001515281);
   h1D_target_minus_toweight__8->SetBinError(51,0.000155549);
   h1D_target_minus_toweight__8->SetBinError(52,0.0001560811);
   h1D_target_minus_toweight__8->SetBinError(53,0.000156844);
   h1D_target_minus_toweight__8->SetBinError(54,0.0001568182);
   h1D_target_minus_toweight__8->SetBinError(55,0.0001622519);
   h1D_target_minus_toweight__8->SetBinError(56,0.0001671513);
   h1D_target_minus_toweight__8->SetBinError(57,0.0001762419);
   h1D_target_minus_toweight__8->SetBinError(58,0.0001821105);
   h1D_target_minus_toweight__8->SetBinError(59,0.0001876496);
   h1D_target_minus_toweight__8->SetBinError(60,0.0001926432);
   h1D_target_minus_toweight__8->SetBinError(61,0.0001965581);
   h1D_target_minus_toweight__8->SetBinError(62,0.0001998362);
   h1D_target_minus_toweight__8->SetBinError(63,0.0002038012);
   h1D_target_minus_toweight__8->SetBinError(64,0.0002099209);
   h1D_target_minus_toweight__8->SetBinError(65,0.0002156833);
   h1D_target_minus_toweight__8->SetBinError(66,0.0002182145);
   h1D_target_minus_toweight__8->SetBinError(67,0.0002245922);
   h1D_target_minus_toweight__8->SetBinError(68,0.0002266263);
   h1D_target_minus_toweight__8->SetBinError(69,0.0002230233);
   h1D_target_minus_toweight__8->SetBinError(70,0.00021751);
   h1D_target_minus_toweight__8->SetBinError(71,0.0002083272);
   h1D_target_minus_toweight__8->SetBinError(72,0.0001994436);
   h1D_target_minus_toweight__8->SetBinError(73,0.0001882005);
   h1D_target_minus_toweight__8->SetBinError(74,0.0001797781);
   h1D_target_minus_toweight__8->SetBinError(75,0.0001691721);
   h1D_target_minus_toweight__8->SetBinError(76,0.0001587084);
   h1D_target_minus_toweight__8->SetBinError(77,0.0001494686);
   h1D_target_minus_toweight__8->SetBinError(78,0.0001400969);
   h1D_target_minus_toweight__8->SetBinError(79,0.0001288495);
   h1D_target_minus_toweight__8->SetBinError(80,0.0001232675);
   h1D_target_minus_toweight__8->SetBinError(81,0.0001135731);
   h1D_target_minus_toweight__8->SetBinError(82,0.0001047323);
   h1D_target_minus_toweight__8->SetBinError(83,9.802777e-05);
   h1D_target_minus_toweight__8->SetBinError(84,9.245567e-05);
   h1D_target_minus_toweight__8->SetBinError(85,8.547815e-05);
   h1D_target_minus_toweight__8->SetBinError(86,7.915517e-05);
   h1D_target_minus_toweight__8->SetBinError(87,7.32476e-05);
   h1D_target_minus_toweight__8->SetBinError(88,6.669603e-05);
   h1D_target_minus_toweight__8->SetBinError(89,6.014876e-05);
   h1D_target_minus_toweight__8->SetBinError(90,5.62689e-05);
   h1D_target_minus_toweight__8->SetBinError(91,5.220834e-05);
   h1D_target_minus_toweight__8->SetBinError(92,4.589638e-05);
   h1D_target_minus_toweight__8->SetBinError(93,4.130079e-05);
   h1D_target_minus_toweight__8->SetBinError(94,3.930064e-05);
   h1D_target_minus_toweight__8->SetBinError(95,3.560431e-05);
   h1D_target_minus_toweight__8->SetBinError(96,2.909839e-05);
   h1D_target_minus_toweight__8->SetBinError(97,2.480158e-05);
   h1D_target_minus_toweight__8->SetBinError(98,2.160721e-05);
   h1D_target_minus_toweight__8->SetBinError(99,1.896877e-05);
   h1D_target_minus_toweight__8->SetBinError(100,1.264708e-05);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("p_{Y} (D^{+}) [MeV/c]");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{+} #rightarrow K_{S}^{0} h^{+})#minusN(D^{+} #rightarrow K^{#minus} #pi^{+} h^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",-14000,14000);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   Dplus_PY_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(-19250,-0.3562255,15750,0.4259805);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,-14000,14000);
   h1D_asym__9->SetBinContent(1,0.01274087);
   h1D_asym__9->SetBinContent(2,0.05893438);
   h1D_asym__9->SetBinContent(3,0.03470121);
   h1D_asym__9->SetBinContent(4,0.006845166);
   h1D_asym__9->SetBinContent(5,0.06215634);
   h1D_asym__9->SetBinContent(6,-0.02312938);
   h1D_asym__9->SetBinContent(7,-0.01208345);
   h1D_asym__9->SetBinContent(8,0.02699302);
   h1D_asym__9->SetBinContent(9,0.02111455);
   h1D_asym__9->SetBinContent(10,-0.002263121);
   h1D_asym__9->SetBinContent(11,0.002047963);
   h1D_asym__9->SetBinContent(12,-0.01101297);
   h1D_asym__9->SetBinContent(13,0.02444979);
   h1D_asym__9->SetBinContent(14,-0.01795343);
   h1D_asym__9->SetBinContent(15,0.009020345);
   h1D_asym__9->SetBinContent(16,0.004859435);
   h1D_asym__9->SetBinContent(17,-0.02310503);
   h1D_asym__9->SetBinContent(18,-0.00168169);
   h1D_asym__9->SetBinContent(19,0.0003035541);
   h1D_asym__9->SetBinContent(20,-0.00427207);
   h1D_asym__9->SetBinContent(21,0.01452203);
   h1D_asym__9->SetBinContent(22,-0.004791195);
   h1D_asym__9->SetBinContent(23,-0.0124168);
   h1D_asym__9->SetBinContent(24,-0.01457253);
   h1D_asym__9->SetBinContent(25,-0.009275751);
   h1D_asym__9->SetBinContent(26,0.008256155);
   h1D_asym__9->SetBinContent(27,-0.005400476);
   h1D_asym__9->SetBinContent(28,-0.005584465);
   h1D_asym__9->SetBinContent(29,-0.01163164);
   h1D_asym__9->SetBinContent(30,0.0004053572);
   h1D_asym__9->SetBinContent(31,-0.008571791);
   h1D_asym__9->SetBinContent(32,-0.0007899364);
   h1D_asym__9->SetBinContent(33,-0.001650834);
   h1D_asym__9->SetBinContent(34,-0.009986408);
   h1D_asym__9->SetBinContent(35,-0.004477944);
   h1D_asym__9->SetBinContent(36,-0.01130385);
   h1D_asym__9->SetBinContent(37,-0.01206125);
   h1D_asym__9->SetBinContent(38,-0.01075265);
   h1D_asym__9->SetBinContent(39,0.004465435);
   h1D_asym__9->SetBinContent(40,-0.001617014);
   h1D_asym__9->SetBinContent(41,-0.01372937);
   h1D_asym__9->SetBinContent(42,-0.00121404);
   h1D_asym__9->SetBinContent(43,0.0001018844);
   h1D_asym__9->SetBinContent(44,-0.002719062);
   h1D_asym__9->SetBinContent(45,0.001124574);
   h1D_asym__9->SetBinContent(46,-0.007223898);
   h1D_asym__9->SetBinContent(47,-0.004961491);
   h1D_asym__9->SetBinContent(48,-0.007278657);
   h1D_asym__9->SetBinContent(49,-0.01244756);
   h1D_asym__9->SetBinContent(50,-0.01328732);
   h1D_asym__9->SetBinContent(51,-0.004557531);
   h1D_asym__9->SetBinContent(52,-0.009808121);
   h1D_asym__9->SetBinContent(53,-0.0126003);
   h1D_asym__9->SetBinContent(54,-0.01367429);
   h1D_asym__9->SetBinContent(55,-0.02111769);
   h1D_asym__9->SetBinContent(56,-0.0004946343);
   h1D_asym__9->SetBinContent(57,-0.006162606);
   h1D_asym__9->SetBinContent(58,-0.002568468);
   h1D_asym__9->SetBinContent(59,-0.00253061);
   h1D_asym__9->SetBinContent(60,-0.009072891);
   h1D_asym__9->SetBinContent(61,-0.0149222);
   h1D_asym__9->SetBinContent(62,-0.01710941);
   h1D_asym__9->SetBinContent(63,-0.008582981);
   h1D_asym__9->SetBinContent(64,-0.004749233);
   h1D_asym__9->SetBinContent(65,-0.008831958);
   h1D_asym__9->SetBinContent(66,-2.555922e-05);
   h1D_asym__9->SetBinContent(67,-0.00946921);
   h1D_asym__9->SetBinContent(68,-0.003036368);
   h1D_asym__9->SetBinContent(69,0.001301919);
   h1D_asym__9->SetBinContent(70,0.0007427642);
   h1D_asym__9->SetBinContent(71,-0.005936197);
   h1D_asym__9->SetBinContent(72,0.001531485);
   h1D_asym__9->SetBinContent(73,-0.0130166);
   h1D_asym__9->SetBinContent(74,-0.01389506);
   h1D_asym__9->SetBinContent(75,-0.01207577);
   h1D_asym__9->SetBinContent(76,-0.004434797);
   h1D_asym__9->SetBinContent(77,-0.003998753);
   h1D_asym__9->SetBinContent(78,-0.01587032);
   h1D_asym__9->SetBinContent(79,-0.009113225);
   h1D_asym__9->SetBinContent(80,-0.006564864);
   h1D_asym__9->SetBinContent(81,-0.009008842);
   h1D_asym__9->SetBinContent(82,-0.009922888);
   h1D_asym__9->SetBinContent(83,-0.0272679);
   h1D_asym__9->SetBinContent(84,0.01048013);
   h1D_asym__9->SetBinContent(85,-0.003973215);
   h1D_asym__9->SetBinContent(86,-0.003808559);
   h1D_asym__9->SetBinContent(87,0.002164796);
   h1D_asym__9->SetBinContent(88,0.01612636);
   h1D_asym__9->SetBinContent(89,0.009833228);
   h1D_asym__9->SetBinContent(90,-0.02978101);
   h1D_asym__9->SetBinContent(91,-0.003355658);
   h1D_asym__9->SetBinContent(92,0.00723089);
   h1D_asym__9->SetBinContent(93,-0.01128107);
   h1D_asym__9->SetBinContent(94,-0.05674878);
   h1D_asym__9->SetBinContent(95,-0.0121678);
   h1D_asym__9->SetBinContent(96,0.03152587);
   h1D_asym__9->SetBinContent(97,0.01419187);
   h1D_asym__9->SetBinContent(98,0.00218918);
   h1D_asym__9->SetBinContent(99,-0.0623123);
   h1D_asym__9->SetBinContent(100,-0.1488862);
   h1D_asym__9->SetBinError(1,0.3014959);
   h1D_asym__9->SetBinError(2,0.2270233);
   h1D_asym__9->SetBinError(3,0.1471791);
   h1D_asym__9->SetBinError(4,0.125506);
   h1D_asym__9->SetBinError(5,0.1233522);
   h1D_asym__9->SetBinError(6,0.08381835);
   h1D_asym__9->SetBinError(7,0.07808961);
   h1D_asym__9->SetBinError(8,0.07975438);
   h1D_asym__9->SetBinError(9,0.06806828);
   h1D_asym__9->SetBinError(10,0.05918891);
   h1D_asym__9->SetBinError(11,0.05470152);
   h1D_asym__9->SetBinError(12,0.04755205);
   h1D_asym__9->SetBinError(13,0.04959441);
   h1D_asym__9->SetBinError(14,0.04039478);
   h1D_asym__9->SetBinError(15,0.04142912);
   h1D_asym__9->SetBinError(16,0.03752246);
   h1D_asym__9->SetBinError(17,0.03154785);
   h1D_asym__9->SetBinError(18,0.0317803);
   h1D_asym__9->SetBinError(19,0.02904553);
   h1D_asym__9->SetBinError(20,0.02687225);
   h1D_asym__9->SetBinError(21,0.02688804);
   h1D_asym__9->SetBinError(22,0.02342906);
   h1D_asym__9->SetBinError(23,0.02154776);
   h1D_asym__9->SetBinError(24,0.02018671);
   h1D_asym__9->SetBinError(25,0.01922625);
   h1D_asym__9->SetBinError(26,0.01889174);
   h1D_asym__9->SetBinError(27,0.01734241);
   h1D_asym__9->SetBinError(28,0.01642556);
   h1D_asym__9->SetBinError(29,0.01524006);
   h1D_asym__9->SetBinError(30,0.01533838);
   h1D_asym__9->SetBinError(31,0.0144241);
   h1D_asym__9->SetBinError(32,0.01433506);
   h1D_asym__9->SetBinError(33,0.01416113);
   h1D_asym__9->SetBinError(34,0.01377845);
   h1D_asym__9->SetBinError(35,0.01448681);
   h1D_asym__9->SetBinError(36,0.01468372);
   h1D_asym__9->SetBinError(37,0.0146794);
   h1D_asym__9->SetBinError(38,0.01507351);
   h1D_asym__9->SetBinError(39,0.01623556);
   h1D_asym__9->SetBinError(40,0.01593599);
   h1D_asym__9->SetBinError(41,0.01612821);
   h1D_asym__9->SetBinError(42,0.01656423);
   h1D_asym__9->SetBinError(43,0.01705161);
   h1D_asym__9->SetBinError(44,0.01778426);
   h1D_asym__9->SetBinError(45,0.01875658);
   h1D_asym__9->SetBinError(46,0.01882078);
   h1D_asym__9->SetBinError(47,0.01947557);
   h1D_asym__9->SetBinError(48,0.01944798);
   h1D_asym__9->SetBinError(49,0.01940248);
   h1D_asym__9->SetBinError(50,0.01979175);
   h1D_asym__9->SetBinError(51,0.01992428);
   h1D_asym__9->SetBinError(52,0.01938528);
   h1D_asym__9->SetBinError(53,0.01929444);
   h1D_asym__9->SetBinError(54,0.01930795);
   h1D_asym__9->SetBinError(55,0.01826301);
   h1D_asym__9->SetBinError(56,0.01852589);
   h1D_asym__9->SetBinError(57,0.01728502);
   h1D_asym__9->SetBinError(58,0.01698995);
   h1D_asym__9->SetBinError(59,0.01652121);
   h1D_asym__9->SetBinError(60,0.0156924);
   h1D_asym__9->SetBinError(61,0.01504857);
   h1D_asym__9->SetBinError(62,0.01484931);
   h1D_asym__9->SetBinError(63,0.01486353);
   h1D_asym__9->SetBinError(64,0.0146821);
   h1D_asym__9->SetBinError(65,0.01411212);
   h1D_asym__9->SetBinError(66,0.01418482);
   h1D_asym__9->SetBinError(67,0.01350056);
   h1D_asym__9->SetBinError(68,0.01372267);
   h1D_asym__9->SetBinError(69,0.01395265);
   h1D_asym__9->SetBinError(70,0.0143908);
   h1D_asym__9->SetBinError(71,0.01466335);
   h1D_asym__9->SetBinError(72,0.01570539);
   h1D_asym__9->SetBinError(73,0.01600885);
   h1D_asym__9->SetBinError(74,0.0166676);
   h1D_asym__9->SetBinError(75,0.0177328);
   h1D_asym__9->SetBinError(76,0.01938722);
   h1D_asym__9->SetBinError(77,0.02055956);
   h1D_asym__9->SetBinError(78,0.02122957);
   h1D_asym__9->SetBinError(79,0.02339574);
   h1D_asym__9->SetBinError(80,0.02491766);
   h1D_asym__9->SetBinError(81,0.02749297);
   h1D_asym__9->SetBinError(82,0.02868172);
   h1D_asym__9->SetBinError(83,0.02938163);
   h1D_asym__9->SetBinError(84,0.03438755);
   h1D_asym__9->SetBinError(85,0.03560732);
   h1D_asym__9->SetBinError(86,0.03883946);
   h1D_asym__9->SetBinError(87,0.04298167);
   h1D_asym__9->SetBinError(88,0.04942857);
   h1D_asym__9->SetBinError(89,0.05144836);
   h1D_asym__9->SetBinError(90,0.05079813);
   h1D_asym__9->SetBinError(91,0.05895809);
   h1D_asym__9->SetBinError(92,0.0687031);
   h1D_asym__9->SetBinError(93,0.07613178);
   h1D_asym__9->SetBinError(94,0.06545053);
   h1D_asym__9->SetBinError(95,0.08695377);
   h1D_asym__9->SetBinError(96,0.1152517);
   h1D_asym__9->SetBinError(97,0.1294296);
   h1D_asym__9->SetBinError(98,0.1487885);
   h1D_asym__9->SetBinError(99,0.139369);
   h1D_asym__9->SetBinError(100,0.1754126);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("p_{Y} (D^{+}) [MeV/c]");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",-14000,14000);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","-0.006120517069",-14000,14000);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   Dplus_PY_asym->cd();
   Dplus_PY_asym->Modified();
   Dplus_PY_asym->cd();
   Dplus_PY_asym->SetSelected(Dplus_PY_asym);
}
