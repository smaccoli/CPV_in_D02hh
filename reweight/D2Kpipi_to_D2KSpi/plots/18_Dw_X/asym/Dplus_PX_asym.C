void Dplus_PX_asym()
{
//=========Macro generated from canvas: Dplus_PX_asym/Dplus_PX_asym
//=========  (Thu Nov  7 16:35:14 2019) by ROOT version6.08/02
   TCanvas *Dplus_PX_asym = new TCanvas("Dplus_PX_asym", "Dplus_PX_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   Dplus_PX_asym->SetHighLightColor(2);
   Dplus_PX_asym->Range(0,0,1,1);
   Dplus_PX_asym->SetFillColor(0);
   Dplus_PX_asym->SetBorderMode(0);
   Dplus_PX_asym->SetBorderSize(10);
   Dplus_PX_asym->SetTickx(1);
   Dplus_PX_asym->SetTicky(1);
   Dplus_PX_asym->SetLeftMargin(0.18);
   Dplus_PX_asym->SetRightMargin(0.05);
   Dplus_PX_asym->SetTopMargin(0.07);
   Dplus_PX_asym->SetBottomMargin(0.16);
   Dplus_PX_asym->SetFrameFillStyle(0);
   Dplus_PX_asym->SetFrameLineStyle(0);
   Dplus_PX_asym->SetFrameLineColor(0);
   Dplus_PX_asym->SetFrameLineWidth(0);
   Dplus_PX_asym->SetFrameBorderMode(0);
   Dplus_PX_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(-19250,-0.008532289,15750,0.003378683);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,-14000,14000);
   h1D_target_minus_toweight__8->SetBinContent(1,2.541706e-05);
   h1D_target_minus_toweight__8->SetBinContent(2,5.201417e-05);
   h1D_target_minus_toweight__8->SetBinContent(3,6.290755e-05);
   h1D_target_minus_toweight__8->SetBinContent(4,9.019197e-05);
   h1D_target_minus_toweight__8->SetBinContent(5,0.0001429259);
   h1D_target_minus_toweight__8->SetBinContent(6,0.0001703702);
   h1D_target_minus_toweight__8->SetBinContent(7,0.0003134652);
   h1D_target_minus_toweight__8->SetBinContent(8,0.0003620925);
   h1D_target_minus_toweight__8->SetBinContent(9,0.0003797895);
   h1D_target_minus_toweight__8->SetBinContent(10,0.0004416119);
   h1D_target_minus_toweight__8->SetBinContent(11,0.0006016187);
   h1D_target_minus_toweight__8->SetBinContent(12,0.0007606496);
   h1D_target_minus_toweight__8->SetBinContent(13,0.0007572897);
   h1D_target_minus_toweight__8->SetBinContent(14,0.0008246655);
   h1D_target_minus_toweight__8->SetBinContent(15,0.001036812);
   h1D_target_minus_toweight__8->SetBinContent(16,0.001253677);
   h1D_target_minus_toweight__8->SetBinContent(17,0.001252386);
   h1D_target_minus_toweight__8->SetBinContent(18,0.001571639);
   h1D_target_minus_toweight__8->SetBinContent(19,0.001554933);
   h1D_target_minus_toweight__8->SetBinContent(20,0.001752751);
   h1D_target_minus_toweight__8->SetBinContent(21,0.00166457);
   h1D_target_minus_toweight__8->SetBinContent(22,0.00190352);
   h1D_target_minus_toweight__8->SetBinContent(23,0.001959704);
   h1D_target_minus_toweight__8->SetBinContent(24,0.00215208);
   h1D_target_minus_toweight__8->SetBinContent(25,0.00216599);
   h1D_target_minus_toweight__8->SetBinContent(26,0.002167735);
   h1D_target_minus_toweight__8->SetBinContent(27,0.002310012);
   h1D_target_minus_toweight__8->SetBinContent(28,0.002051638);
   h1D_target_minus_toweight__8->SetBinContent(29,0.001562502);
   h1D_target_minus_toweight__8->SetBinContent(30,0.001196671);
   h1D_target_minus_toweight__8->SetBinContent(31,0.0005107485);
   h1D_target_minus_toweight__8->SetBinContent(32,-0.0003729537);
   h1D_target_minus_toweight__8->SetBinContent(33,-0.001144584);
   h1D_target_minus_toweight__8->SetBinContent(34,-0.002769317);
   h1D_target_minus_toweight__8->SetBinContent(35,-0.003523262);
   h1D_target_minus_toweight__8->SetBinContent(36,-0.003306046);
   h1D_target_minus_toweight__8->SetBinContent(37,-0.002624214);
   h1D_target_minus_toweight__8->SetBinContent(38,-0.002282813);
   h1D_target_minus_toweight__8->SetBinContent(39,-0.001994504);
   h1D_target_minus_toweight__8->SetBinContent(40,-0.001869915);
   h1D_target_minus_toweight__8->SetBinContent(41,-0.00222473);
   h1D_target_minus_toweight__8->SetBinContent(42,-0.002380189);
   h1D_target_minus_toweight__8->SetBinContent(43,-0.002314458);
   h1D_target_minus_toweight__8->SetBinContent(44,-0.002478157);
   h1D_target_minus_toweight__8->SetBinContent(45,-0.002118403);
   h1D_target_minus_toweight__8->SetBinContent(46,-0.001525503);
   h1D_target_minus_toweight__8->SetBinContent(47,-0.0008311579);
   h1D_target_minus_toweight__8->SetBinContent(48,9.443052e-05);
   h1D_target_minus_toweight__8->SetBinContent(49,0.0003078757);
   h1D_target_minus_toweight__8->SetBinContent(50,-1.776498e-05);
   h1D_target_minus_toweight__8->SetBinContent(51,7.210206e-05);
   h1D_target_minus_toweight__8->SetBinContent(52,0.000487932);
   h1D_target_minus_toweight__8->SetBinContent(53,0.0002068114);
   h1D_target_minus_toweight__8->SetBinContent(54,-0.000593612);
   h1D_target_minus_toweight__8->SetBinContent(55,-0.001210998);
   h1D_target_minus_toweight__8->SetBinContent(56,-0.001937091);
   h1D_target_minus_toweight__8->SetBinContent(57,-0.002228252);
   h1D_target_minus_toweight__8->SetBinContent(58,-0.002618786);
   h1D_target_minus_toweight__8->SetBinContent(59,-0.002677831);
   h1D_target_minus_toweight__8->SetBinContent(60,-0.002581188);
   h1D_target_minus_toweight__8->SetBinContent(61,-0.002050917);
   h1D_target_minus_toweight__8->SetBinContent(62,-0.002191378);
   h1D_target_minus_toweight__8->SetBinContent(63,-0.00233439);
   h1D_target_minus_toweight__8->SetBinContent(64,-0.002646552);
   h1D_target_minus_toweight__8->SetBinContent(65,-0.003960572);
   h1D_target_minus_toweight__8->SetBinContent(66,-0.004357921);
   h1D_target_minus_toweight__8->SetBinContent(67,-0.003554072);
   h1D_target_minus_toweight__8->SetBinContent(68,-0.00160961);
   h1D_target_minus_toweight__8->SetBinContent(69,-0.0003129728);
   h1D_target_minus_toweight__8->SetBinContent(70,0.0004723687);
   h1D_target_minus_toweight__8->SetBinContent(71,0.001078729);
   h1D_target_minus_toweight__8->SetBinContent(72,0.002331549);
   h1D_target_minus_toweight__8->SetBinContent(73,0.002315743);
   h1D_target_minus_toweight__8->SetBinContent(74,0.002787887);
   h1D_target_minus_toweight__8->SetBinContent(75,0.002532719);
   h1D_target_minus_toweight__8->SetBinContent(76,0.002317469);
   h1D_target_minus_toweight__8->SetBinContent(77,0.002480024);
   h1D_target_minus_toweight__8->SetBinContent(78,0.002267955);
   h1D_target_minus_toweight__8->SetBinContent(79,0.002045616);
   h1D_target_minus_toweight__8->SetBinContent(80,0.001953355);
   h1D_target_minus_toweight__8->SetBinContent(81,0.001669175);
   h1D_target_minus_toweight__8->SetBinContent(82,0.001668475);
   h1D_target_minus_toweight__8->SetBinContent(83,0.001679315);
   h1D_target_minus_toweight__8->SetBinContent(84,0.001382063);
   h1D_target_minus_toweight__8->SetBinContent(85,0.001230115);
   h1D_target_minus_toweight__8->SetBinContent(86,0.001061163);
   h1D_target_minus_toweight__8->SetBinContent(87,0.0009119997);
   h1D_target_minus_toweight__8->SetBinContent(88,0.0008308549);
   h1D_target_minus_toweight__8->SetBinContent(89,0.0006142042);
   h1D_target_minus_toweight__8->SetBinContent(90,0.0004710847);
   h1D_target_minus_toweight__8->SetBinContent(91,0.0005389489);
   h1D_target_minus_toweight__8->SetBinContent(92,0.0004276636);
   h1D_target_minus_toweight__8->SetBinContent(93,0.0003413514);
   h1D_target_minus_toweight__8->SetBinContent(94,0.0002672173);
   h1D_target_minus_toweight__8->SetBinContent(95,0.0002371667);
   h1D_target_minus_toweight__8->SetBinContent(96,0.0002064997);
   h1D_target_minus_toweight__8->SetBinContent(97,0.0001320668);
   h1D_target_minus_toweight__8->SetBinContent(98,8.699461e-05);
   h1D_target_minus_toweight__8->SetBinContent(99,5.309671e-05);
   h1D_target_minus_toweight__8->SetBinContent(100,2.972272e-05);
   h1D_target_minus_toweight__8->SetBinError(1,1.165059e-05);
   h1D_target_minus_toweight__8->SetBinError(2,1.864123e-05);
   h1D_target_minus_toweight__8->SetBinError(3,2.186449e-05);
   h1D_target_minus_toweight__8->SetBinError(4,2.688578e-05);
   h1D_target_minus_toweight__8->SetBinError(5,3.083151e-05);
   h1D_target_minus_toweight__8->SetBinError(6,3.387764e-05);
   h1D_target_minus_toweight__8->SetBinError(7,4.054229e-05);
   h1D_target_minus_toweight__8->SetBinError(8,4.548017e-05);
   h1D_target_minus_toweight__8->SetBinError(9,4.938722e-05);
   h1D_target_minus_toweight__8->SetBinError(10,5.360382e-05);
   h1D_target_minus_toweight__8->SetBinError(11,6.023e-05);
   h1D_target_minus_toweight__8->SetBinError(12,6.554106e-05);
   h1D_target_minus_toweight__8->SetBinError(13,7.04338e-05);
   h1D_target_minus_toweight__8->SetBinError(14,7.540975e-05);
   h1D_target_minus_toweight__8->SetBinError(15,8.215888e-05);
   h1D_target_minus_toweight__8->SetBinError(16,9.020371e-05);
   h1D_target_minus_toweight__8->SetBinError(17,9.516356e-05);
   h1D_target_minus_toweight__8->SetBinError(18,0.000103561);
   h1D_target_minus_toweight__8->SetBinError(19,0.0001100665);
   h1D_target_minus_toweight__8->SetBinError(20,0.0001176752);
   h1D_target_minus_toweight__8->SetBinError(21,0.0001248893);
   h1D_target_minus_toweight__8->SetBinError(22,0.0001336739);
   h1D_target_minus_toweight__8->SetBinError(23,0.0001424284);
   h1D_target_minus_toweight__8->SetBinError(24,0.000152322);
   h1D_target_minus_toweight__8->SetBinError(25,0.000161337);
   h1D_target_minus_toweight__8->SetBinError(26,0.0001711015);
   h1D_target_minus_toweight__8->SetBinError(27,0.0001824432);
   h1D_target_minus_toweight__8->SetBinError(28,0.0001921934);
   h1D_target_minus_toweight__8->SetBinError(29,0.0002010403);
   h1D_target_minus_toweight__8->SetBinError(30,0.0002101884);
   h1D_target_minus_toweight__8->SetBinError(31,0.0002177253);
   h1D_target_minus_toweight__8->SetBinError(32,0.0002229893);
   h1D_target_minus_toweight__8->SetBinError(33,0.0002259453);
   h1D_target_minus_toweight__8->SetBinError(34,0.0002228517);
   h1D_target_minus_toweight__8->SetBinError(35,0.0002163552);
   h1D_target_minus_toweight__8->SetBinError(36,0.0002079653);
   h1D_target_minus_toweight__8->SetBinError(37,0.000199291);
   h1D_target_minus_toweight__8->SetBinError(38,0.0001915756);
   h1D_target_minus_toweight__8->SetBinError(39,0.0001864726);
   h1D_target_minus_toweight__8->SetBinError(40,0.0001813175);
   h1D_target_minus_toweight__8->SetBinError(41,0.0001740263);
   h1D_target_minus_toweight__8->SetBinError(42,0.0001669268);
   h1D_target_minus_toweight__8->SetBinError(43,0.0001612563);
   h1D_target_minus_toweight__8->SetBinError(44,0.000153885);
   h1D_target_minus_toweight__8->SetBinError(45,0.000149933);
   h1D_target_minus_toweight__8->SetBinError(46,0.0001477232);
   h1D_target_minus_toweight__8->SetBinError(47,0.0001449752);
   h1D_target_minus_toweight__8->SetBinError(48,0.000148296);
   h1D_target_minus_toweight__8->SetBinError(49,0.000146363);
   h1D_target_minus_toweight__8->SetBinError(50,0.0001449019);
   h1D_target_minus_toweight__8->SetBinError(51,0.000145411);
   h1D_target_minus_toweight__8->SetBinError(52,0.0001472385);
   h1D_target_minus_toweight__8->SetBinError(53,0.0001474955);
   h1D_target_minus_toweight__8->SetBinError(54,0.000145827);
   h1D_target_minus_toweight__8->SetBinError(55,0.0001476343);
   h1D_target_minus_toweight__8->SetBinError(56,0.0001498163);
   h1D_target_minus_toweight__8->SetBinError(57,0.0001563815);
   h1D_target_minus_toweight__8->SetBinError(58,0.0001610943);
   h1D_target_minus_toweight__8->SetBinError(59,0.0001676574);
   h1D_target_minus_toweight__8->SetBinError(60,0.0001746045);
   h1D_target_minus_toweight__8->SetBinError(61,0.0001836008);
   h1D_target_minus_toweight__8->SetBinError(62,0.0001906362);
   h1D_target_minus_toweight__8->SetBinError(63,0.0001963202);
   h1D_target_minus_toweight__8->SetBinError(64,0.000203712);
   h1D_target_minus_toweight__8->SetBinError(65,0.0002118561);
   h1D_target_minus_toweight__8->SetBinError(66,0.0002229506);
   h1D_target_minus_toweight__8->SetBinError(67,0.0002322012);
   h1D_target_minus_toweight__8->SetBinError(68,0.0002377256);
   h1D_target_minus_toweight__8->SetBinError(69,0.0002358651);
   h1D_target_minus_toweight__8->SetBinError(70,0.0002290841);
   h1D_target_minus_toweight__8->SetBinError(71,0.0002205712);
   h1D_target_minus_toweight__8->SetBinError(72,0.0002141597);
   h1D_target_minus_toweight__8->SetBinError(73,0.0002027707);
   h1D_target_minus_toweight__8->SetBinError(74,0.0001937632);
   h1D_target_minus_toweight__8->SetBinError(75,0.0001821587);
   h1D_target_minus_toweight__8->SetBinError(76,0.0001708089);
   h1D_target_minus_toweight__8->SetBinError(77,0.0001622783);
   h1D_target_minus_toweight__8->SetBinError(78,0.0001510521);
   h1D_target_minus_toweight__8->SetBinError(79,0.0001411123);
   h1D_target_minus_toweight__8->SetBinError(80,0.000132327);
   h1D_target_minus_toweight__8->SetBinError(81,0.0001224194);
   h1D_target_minus_toweight__8->SetBinError(82,0.0001161623);
   h1D_target_minus_toweight__8->SetBinError(83,0.0001095031);
   h1D_target_minus_toweight__8->SetBinError(84,0.0001010929);
   h1D_target_minus_toweight__8->SetBinError(85,9.412222e-05);
   h1D_target_minus_toweight__8->SetBinError(86,8.729285e-05);
   h1D_target_minus_toweight__8->SetBinError(87,8.02306e-05);
   h1D_target_minus_toweight__8->SetBinError(88,7.466226e-05);
   h1D_target_minus_toweight__8->SetBinError(89,6.778166e-05);
   h1D_target_minus_toweight__8->SetBinError(90,6.115051e-05);
   h1D_target_minus_toweight__8->SetBinError(91,5.843805e-05);
   h1D_target_minus_toweight__8->SetBinError(92,5.209451e-05);
   h1D_target_minus_toweight__8->SetBinError(93,4.72237e-05);
   h1D_target_minus_toweight__8->SetBinError(94,4.152239e-05);
   h1D_target_minus_toweight__8->SetBinError(95,3.779919e-05);
   h1D_target_minus_toweight__8->SetBinError(96,3.36809e-05);
   h1D_target_minus_toweight__8->SetBinError(97,2.872421e-05);
   h1D_target_minus_toweight__8->SetBinError(98,2.4811e-05);
   h1D_target_minus_toweight__8->SetBinError(99,1.923713e-05);
   h1D_target_minus_toweight__8->SetBinError(100,1.388651e-05);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("p_{X} (D^{+}) [MeV/c]");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{+} #rightarrow K_{S}^{0} h^{+})#minusN(D^{+} #rightarrow K^{#minus} #pi^{+} h^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",-14000,14000);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   Dplus_PX_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(-19250,-0.3490477,15750,0.318979);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,-14000,14000);
   h1D_asym__9->SetBinContent(1,-0.02346143);
   h1D_asym__9->SetBinContent(2,-0.05721925);
   h1D_asym__9->SetBinContent(3,0.0180185);
   h1D_asym__9->SetBinContent(4,-0.04409091);
   h1D_asym__9->SetBinContent(5,-0.01089592);
   h1D_asym__9->SetBinContent(6,0.0224046);
   h1D_asym__9->SetBinContent(7,-0.04347534);
   h1D_asym__9->SetBinContent(8,-0.01753722);
   h1D_asym__9->SetBinContent(9,-0.01550796);
   h1D_asym__9->SetBinContent(10,-0.02840811);
   h1D_asym__9->SetBinContent(11,0.01372788);
   h1D_asym__9->SetBinContent(12,0.01239411);
   h1D_asym__9->SetBinContent(13,-0.008879101);
   h1D_asym__9->SetBinContent(14,0.0008737046);
   h1D_asym__9->SetBinContent(15,-0.005944156);
   h1D_asym__9->SetBinContent(16,-0.03032279);
   h1D_asym__9->SetBinContent(17,0.004881732);
   h1D_asym__9->SetBinContent(18,0.006147085);
   h1D_asym__9->SetBinContent(19,-0.006188109);
   h1D_asym__9->SetBinContent(20,-0.01091747);
   h1D_asym__9->SetBinContent(21,0.002097379);
   h1D_asym__9->SetBinContent(22,0.0007721893);
   h1D_asym__9->SetBinContent(23,-0.008789989);
   h1D_asym__9->SetBinContent(24,-0.01501847);
   h1D_asym__9->SetBinContent(25,-0.008944753);
   h1D_asym__9->SetBinContent(26,-0.009954527);
   h1D_asym__9->SetBinContent(27,-0.01101125);
   h1D_asym__9->SetBinContent(28,-0.01984818);
   h1D_asym__9->SetBinContent(29,-0.01089113);
   h1D_asym__9->SetBinContent(30,-0.01571893);
   h1D_asym__9->SetBinContent(31,-0.006361801);
   h1D_asym__9->SetBinContent(32,-0.0147854);
   h1D_asym__9->SetBinContent(33,-0.03104207);
   h1D_asym__9->SetBinContent(34,-0.01579025);
   h1D_asym__9->SetBinContent(35,-0.006235272);
   h1D_asym__9->SetBinContent(36,-0.02288101);
   h1D_asym__9->SetBinContent(37,-0.0214887);
   h1D_asym__9->SetBinContent(38,-0.01398434);
   h1D_asym__9->SetBinContent(39,-0.01350893);
   h1D_asym__9->SetBinContent(40,-0.02125728);
   h1D_asym__9->SetBinContent(41,-0.02690916);
   h1D_asym__9->SetBinContent(42,-0.01641983);
   h1D_asym__9->SetBinContent(43,-0.02101116);
   h1D_asym__9->SetBinContent(44,-0.009582077);
   h1D_asym__9->SetBinContent(45,0.003838705);
   h1D_asym__9->SetBinContent(46,0.001717554);
   h1D_asym__9->SetBinContent(47,-0.004511284);
   h1D_asym__9->SetBinContent(48,-0.005843303);
   h1D_asym__9->SetBinContent(49,-0.01444422);
   h1D_asym__9->SetBinContent(50,-0.002906931);
   h1D_asym__9->SetBinContent(51,0.0022161);
   h1D_asym__9->SetBinContent(52,-0.002828069);
   h1D_asym__9->SetBinContent(53,-0.002718742);
   h1D_asym__9->SetBinContent(54,-0.007768378);
   h1D_asym__9->SetBinContent(55,0.0004600333);
   h1D_asym__9->SetBinContent(56,-0.008476158);
   h1D_asym__9->SetBinContent(57,-0.007156433);
   h1D_asym__9->SetBinContent(58,0.006189558);
   h1D_asym__9->SetBinContent(59,-0.006605846);
   h1D_asym__9->SetBinContent(60,0.003834646);
   h1D_asym__9->SetBinContent(61,-0.001390677);
   h1D_asym__9->SetBinContent(62,0.00107121);
   h1D_asym__9->SetBinContent(63,0.006067053);
   h1D_asym__9->SetBinContent(64,-0.001469642);
   h1D_asym__9->SetBinContent(65,0.01056151);
   h1D_asym__9->SetBinContent(66,0.008961884);
   h1D_asym__9->SetBinContent(67,0.004908783);
   h1D_asym__9->SetBinContent(68,0.007632141);
   h1D_asym__9->SetBinContent(69,0.000670328);
   h1D_asym__9->SetBinContent(70,0.005228221);
   h1D_asym__9->SetBinContent(71,0.005404161);
   h1D_asym__9->SetBinContent(72,0.006800978);
   h1D_asym__9->SetBinContent(73,-0.000814901);
   h1D_asym__9->SetBinContent(74,-0.008851741);
   h1D_asym__9->SetBinContent(75,-0.01046433);
   h1D_asym__9->SetBinContent(76,0.009148982);
   h1D_asym__9->SetBinContent(77,-0.002518885);
   h1D_asym__9->SetBinContent(78,-0.0002859593);
   h1D_asym__9->SetBinContent(79,-0.01357838);
   h1D_asym__9->SetBinContent(80,-0.01790614);
   h1D_asym__9->SetBinContent(81,-0.003570369);
   h1D_asym__9->SetBinContent(82,-0.01026037);
   h1D_asym__9->SetBinContent(83,0.005695203);
   h1D_asym__9->SetBinContent(84,-0.01524472);
   h1D_asym__9->SetBinContent(85,-0.01074335);
   h1D_asym__9->SetBinContent(86,-0.0274064);
   h1D_asym__9->SetBinContent(87,-0.007660783);
   h1D_asym__9->SetBinContent(88,0.02728217);
   h1D_asym__9->SetBinContent(89,-0.01686146);
   h1D_asym__9->SetBinContent(90,-0.003283254);
   h1D_asym__9->SetBinContent(91,-0.01176006);
   h1D_asym__9->SetBinContent(92,-0.0141964);
   h1D_asym__9->SetBinContent(93,0.001345484);
   h1D_asym__9->SetBinContent(94,0.01811219);
   h1D_asym__9->SetBinContent(95,-0.01091514);
   h1D_asym__9->SetBinContent(96,0.01624895);
   h1D_asym__9->SetBinContent(97,-0.006552104);
   h1D_asym__9->SetBinContent(98,0.01826015);
   h1D_asym__9->SetBinContent(99,-0.03593851);
   h1D_asym__9->SetBinContent(100,-0.1533261);
   h1D_asym__9->SetBinError(1,0.247008);
   h1D_asym__9->SetBinError(2,0.1569019);
   h1D_asym__9->SetBinError(3,0.1661966);
   h1D_asym__9->SetBinError(4,0.1134962);
   h1D_asym__9->SetBinError(5,0.1071105);
   h1D_asym__9->SetBinError(6,0.09872178);
   h1D_asym__9->SetBinError(7,0.06920414);
   h1D_asym__9->SetBinError(8,0.06795517);
   h1D_asym__9->SetBinError(9,0.06138249);
   h1D_asym__9->SetBinError(10,0.05220943);
   h1D_asym__9->SetBinError(11,0.05336164);
   h1D_asym__9->SetBinError(12,0.04729155);
   h1D_asym__9->SetBinError(13,0.04228456);
   h1D_asym__9->SetBinError(14,0.0406977);
   h1D_asym__9->SetBinError(15,0.03681654);
   h1D_asym__9->SetBinError(16,0.0316545);
   h1D_asym__9->SetBinError(17,0.03299844);
   h1D_asym__9->SetBinError(18,0.03018088);
   h1D_asym__9->SetBinError(19,0.02783092);
   h1D_asym__9->SetBinError(20,0.0254019);
   h1D_asym__9->SetBinError(21,0.02509454);
   h1D_asym__9->SetBinError(22,0.02346013);
   h1D_asym__9->SetBinError(23,0.02139721);
   h1D_asym__9->SetBinError(24,0.01968499);
   h1D_asym__9->SetBinError(25,0.01882777);
   h1D_asym__9->SetBinError(26,0.01777337);
   h1D_asym__9->SetBinError(27,0.01664093);
   h1D_asym__9->SetBinError(28,0.01552758);
   h1D_asym__9->SetBinError(29,0.01525534);
   h1D_asym__9->SetBinError(30,0.01446569);
   h1D_asym__9->SetBinError(31,0.01431049);
   h1D_asym__9->SetBinError(32,0.01370269);
   h1D_asym__9->SetBinError(33,0.01284337);
   h1D_asym__9->SetBinError(34,0.01379011);
   h1D_asym__9->SetBinError(35,0.0144547);
   h1D_asym__9->SetBinError(36,0.01444003);
   h1D_asym__9->SetBinError(37,0.01515141);
   h1D_asym__9->SetBinError(38,0.01602636);
   h1D_asym__9->SetBinError(39,0.01641678);
   h1D_asym__9->SetBinError(40,0.01651651);
   h1D_asym__9->SetBinError(41,0.01690315);
   h1D_asym__9->SetBinError(42,0.01800389);
   h1D_asym__9->SetBinError(43,0.01834659);
   h1D_asym__9->SetBinError(44,0.01977416);
   h1D_asym__9->SetBinError(45,0.02092806);
   h1D_asym__9->SetBinError(46,0.02140295);
   h1D_asym__9->SetBinError(47,0.02111598);
   h1D_asym__9->SetBinError(48,0.02084285);
   h1D_asym__9->SetBinError(49,0.02038888);
   h1D_asym__9->SetBinError(50,0.02144977);
   h1D_asym__9->SetBinError(51,0.02161325);
   h1D_asym__9->SetBinError(52,0.02079829);
   h1D_asym__9->SetBinError(53,0.02094567);
   h1D_asym__9->SetBinError(54,0.02114222);
   h1D_asym__9->SetBinError(55,0.02115288);
   h1D_asym__9->SetBinError(56,0.02018187);
   h1D_asym__9->SetBinError(57,0.01958923);
   h1D_asym__9->SetBinError(58,0.01960998);
   h1D_asym__9->SetBinError(59,0.01814224);
   h1D_asym__9->SetBinError(60,0.01771953);
   h1D_asym__9->SetBinError(61,0.01657224);
   h1D_asym__9->SetBinError(62,0.01628567);
   h1D_asym__9->SetBinError(63,0.0157969);
   h1D_asym__9->SetBinError(64,0.01489869);
   h1D_asym__9->SetBinError(65,0.01472161);
   h1D_asym__9->SetBinError(66,0.01398526);
   h1D_asym__9->SetBinError(67,0.01338718);
   h1D_asym__9->SetBinError(68,0.01315405);
   h1D_asym__9->SetBinError(69,0.01303305);
   h1D_asym__9->SetBinError(70,0.01353059);
   h1D_asym__9->SetBinError(71,0.0140648);
   h1D_asym__9->SetBinError(72,0.01440432);
   h1D_asym__9->SetBinError(73,0.01493721);
   h1D_asym__9->SetBinError(74,0.01528218);
   h1D_asym__9->SetBinError(75,0.0162451);
   h1D_asym__9->SetBinError(76,0.01840479);
   h1D_asym__9->SetBinError(77,0.01885788);
   h1D_asym__9->SetBinError(78,0.02020868);
   h1D_asym__9->SetBinError(79,0.02100652);
   h1D_asym__9->SetBinError(80,0.02204451);
   h1D_asym__9->SetBinError(81,0.02469818);
   h1D_asym__9->SetBinError(82,0.02554028);
   h1D_asym__9->SetBinError(83,0.02838021);
   h1D_asym__9->SetBinError(84,0.02932897);
   h1D_asym__9->SetBinError(85,0.03182996);
   h1D_asym__9->SetBinError(86,0.03335573);
   h1D_asym__9->SetBinError(87,0.03720946);
   h1D_asym__9->SetBinError(88,0.04419125);
   h1D_asym__9->SetBinError(89,0.044354);
   h1D_asym__9->SetBinError(90,0.05046704);
   h1D_asym__9->SetBinError(91,0.05127813);
   h1D_asym__9->SetBinError(92,0.05693356);
   h1D_asym__9->SetBinError(93,0.06543725);
   h1D_asym__9->SetBinError(94,0.07724187);
   h1D_asym__9->SetBinError(95,0.07855494);
   h1D_asym__9->SetBinError(96,0.09169196);
   h1D_asym__9->SetBinError(97,0.1047487);
   h1D_asym__9->SetBinError(98,0.1337);
   h1D_asym__9->SetBinError(99,0.1335033);
   h1D_asym__9->SetBinError(100,0.1684552);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("p_{X} (D^{+}) [MeV/c]");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",-14000,14000);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","-0.006033722535",-14000,14000);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   Dplus_PX_asym->cd();
   Dplus_PX_asym->Modified();
   Dplus_PX_asym->cd();
   Dplus_PX_asym->SetSelected(Dplus_PX_asym);
}
