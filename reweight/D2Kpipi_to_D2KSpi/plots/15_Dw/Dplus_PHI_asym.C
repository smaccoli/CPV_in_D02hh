void Dplus_PHI_asym()
{
//=========Macro generated from canvas: Dplus_PHI_asym/Dplus_PHI_asym
//=========  (Mon Nov 18 16:16:56 2019) by ROOT version6.08/02
   TCanvas *Dplus_PHI_asym = new TCanvas("Dplus_PHI_asym", "Dplus_PHI_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   Dplus_PHI_asym->SetHighLightColor(2);
   Dplus_PHI_asym->Range(0,0,1,1);
   Dplus_PHI_asym->SetFillColor(0);
   Dplus_PHI_asym->SetBorderMode(0);
   Dplus_PHI_asym->SetBorderSize(10);
   Dplus_PHI_asym->SetTickx(1);
   Dplus_PHI_asym->SetTicky(1);
   Dplus_PHI_asym->SetLeftMargin(0.18);
   Dplus_PHI_asym->SetRightMargin(0.05);
   Dplus_PHI_asym->SetTopMargin(0.07);
   Dplus_PHI_asym->SetBottomMargin(0.16);
   Dplus_PHI_asym->SetFrameFillStyle(0);
   Dplus_PHI_asym->SetFrameLineStyle(0);
   Dplus_PHI_asym->SetFrameLineColor(0);
   Dplus_PHI_asym->SetFrameLineWidth(0);
   Dplus_PHI_asym->SetFrameBorderMode(0);
   Dplus_PHI_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(-4.3119,-0.00179512,3.4901,0.0007612077);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,-3.1416,3.1);
   h1D_target_minus_toweight__8->SetBinContent(1,3.296416e-05);
   h1D_target_minus_toweight__8->SetBinContent(2,7.651513e-05);
   h1D_target_minus_toweight__8->SetBinContent(3,0.0001837029);
   h1D_target_minus_toweight__8->SetBinContent(4,2.808962e-05);
   h1D_target_minus_toweight__8->SetBinContent(5,1.697429e-05);
   h1D_target_minus_toweight__8->SetBinContent(6,-0.0002286518);
   h1D_target_minus_toweight__8->SetBinContent(7,-0.0001358315);
   h1D_target_minus_toweight__8->SetBinContent(8,0.0004057391);
   h1D_target_minus_toweight__8->SetBinContent(9,-5.913805e-05);
   h1D_target_minus_toweight__8->SetBinContent(10,0.0003901077);
   h1D_target_minus_toweight__8->SetBinContent(11,-0.0002411753);
   h1D_target_minus_toweight__8->SetBinContent(12,0.0003519785);
   h1D_target_minus_toweight__8->SetBinContent(13,8.199364e-05);
   h1D_target_minus_toweight__8->SetBinContent(14,6.902311e-05);
   h1D_target_minus_toweight__8->SetBinContent(15,0.0002121832);
   h1D_target_minus_toweight__8->SetBinContent(16,-0.0002494575);
   h1D_target_minus_toweight__8->SetBinContent(17,-0.0002242802);
   h1D_target_minus_toweight__8->SetBinContent(18,0.0002199803);
   h1D_target_minus_toweight__8->SetBinContent(19,-0.0002637599);
   h1D_target_minus_toweight__8->SetBinContent(20,0.0001115724);
   h1D_target_minus_toweight__8->SetBinContent(21,-0.000161319);
   h1D_target_minus_toweight__8->SetBinContent(22,0.0002550497);
   h1D_target_minus_toweight__8->SetBinContent(23,-0.0004057274);
   h1D_target_minus_toweight__8->SetBinContent(24,0.0001658527);
   h1D_target_minus_toweight__8->SetBinContent(25,-0.0004809229);
   h1D_target_minus_toweight__8->SetBinContent(26,0.0001193676);
   h1D_target_minus_toweight__8->SetBinContent(27,-0.0001483932);
   h1D_target_minus_toweight__8->SetBinContent(28,0.0001407689);
   h1D_target_minus_toweight__8->SetBinContent(29,-0.000171368);
   h1D_target_minus_toweight__8->SetBinContent(30,-5.004648e-05);
   h1D_target_minus_toweight__8->SetBinContent(31,-1.82474e-05);
   h1D_target_minus_toweight__8->SetBinContent(32,-0.0003301091);
   h1D_target_minus_toweight__8->SetBinContent(33,0.0005076677);
   h1D_target_minus_toweight__8->SetBinContent(34,-7.562898e-05);
   h1D_target_minus_toweight__8->SetBinContent(35,8.108839e-05);
   h1D_target_minus_toweight__8->SetBinContent(36,0.0002082977);
   h1D_target_minus_toweight__8->SetBinContent(37,-1.427624e-05);
   h1D_target_minus_toweight__8->SetBinContent(38,-4.312582e-05);
   h1D_target_minus_toweight__8->SetBinContent(39,0.0002250485);
   h1D_target_minus_toweight__8->SetBinContent(40,-0.0004532821);
   h1D_target_minus_toweight__8->SetBinContent(41,0.0001447825);
   h1D_target_minus_toweight__8->SetBinContent(42,-7.869676e-06);
   h1D_target_minus_toweight__8->SetBinContent(43,0.0003351849);
   h1D_target_minus_toweight__8->SetBinContent(44,-2.934877e-05);
   h1D_target_minus_toweight__8->SetBinContent(45,-0.0004126746);
   h1D_target_minus_toweight__8->SetBinContent(46,0.0002595177);
   h1D_target_minus_toweight__8->SetBinContent(47,-0.0001618862);
   h1D_target_minus_toweight__8->SetBinContent(48,6.161351e-05);
   h1D_target_minus_toweight__8->SetBinContent(49,0.0002517048);
   h1D_target_minus_toweight__8->SetBinContent(50,0.0001985021);
   h1D_target_minus_toweight__8->SetBinContent(51,-9.966083e-05);
   h1D_target_minus_toweight__8->SetBinContent(52,0.0002129572);
   h1D_target_minus_toweight__8->SetBinContent(53,-0.0002209498);
   h1D_target_minus_toweight__8->SetBinContent(54,0.0006759968);
   h1D_target_minus_toweight__8->SetBinContent(55,-0.0004931875);
   h1D_target_minus_toweight__8->SetBinContent(56,0.0003038216);
   h1D_target_minus_toweight__8->SetBinContent(57,-0.0003373912);
   h1D_target_minus_toweight__8->SetBinContent(58,0.0003401432);
   h1D_target_minus_toweight__8->SetBinContent(59,-0.0004820274);
   h1D_target_minus_toweight__8->SetBinContent(60,0.00051561);
   h1D_target_minus_toweight__8->SetBinContent(61,-0.0006247442);
   h1D_target_minus_toweight__8->SetBinContent(62,0.0003500134);
   h1D_target_minus_toweight__8->SetBinContent(63,0.0004118597);
   h1D_target_minus_toweight__8->SetBinContent(64,0.0002339799);
   h1D_target_minus_toweight__8->SetBinContent(65,-0.0002573477);
   h1D_target_minus_toweight__8->SetBinContent(66,-3.095623e-05);
   h1D_target_minus_toweight__8->SetBinContent(67,-1.863111e-05);
   h1D_target_minus_toweight__8->SetBinContent(68,-1.915172e-05);
   h1D_target_minus_toweight__8->SetBinContent(69,-0.0004669437);
   h1D_target_minus_toweight__8->SetBinContent(70,-2.864935e-05);
   h1D_target_minus_toweight__8->SetBinContent(71,-0.0001390604);
   h1D_target_minus_toweight__8->SetBinContent(72,-2.821861e-05);
   h1D_target_minus_toweight__8->SetBinContent(73,-8.197268e-05);
   h1D_target_minus_toweight__8->SetBinContent(74,3.584707e-05);
   h1D_target_minus_toweight__8->SetBinContent(75,-0.0009470684);
   h1D_target_minus_toweight__8->SetBinContent(76,-0.0001783427);
   h1D_target_minus_toweight__8->SetBinContent(77,0.0002193358);
   h1D_target_minus_toweight__8->SetBinContent(78,-0.0006415308);
   h1D_target_minus_toweight__8->SetBinContent(79,6.228779e-05);
   h1D_target_minus_toweight__8->SetBinContent(80,-0.0001764889);
   h1D_target_minus_toweight__8->SetBinContent(81,0.0002945866);
   h1D_target_minus_toweight__8->SetBinContent(82,-3.878307e-05);
   h1D_target_minus_toweight__8->SetBinContent(83,-0.0001136567);
   h1D_target_minus_toweight__8->SetBinContent(84,9.964779e-05);
   h1D_target_minus_toweight__8->SetBinContent(85,0.0001930883);
   h1D_target_minus_toweight__8->SetBinContent(86,-0.0002778601);
   h1D_target_minus_toweight__8->SetBinContent(87,7.183664e-05);
   h1D_target_minus_toweight__8->SetBinContent(88,-6.190222e-05);
   h1D_target_minus_toweight__8->SetBinContent(89,0.0004284987);
   h1D_target_minus_toweight__8->SetBinContent(90,-2.402812e-05);
   h1D_target_minus_toweight__8->SetBinContent(91,8.69371e-05);
   h1D_target_minus_toweight__8->SetBinContent(92,-0.0001869835);
   h1D_target_minus_toweight__8->SetBinContent(93,0.0006039394);
   h1D_target_minus_toweight__8->SetBinContent(94,0.0002249517);
   h1D_target_minus_toweight__8->SetBinContent(95,-0.00017343);
   h1D_target_minus_toweight__8->SetBinContent(96,8.438248e-05);
   h1D_target_minus_toweight__8->SetBinContent(97,0.0002564415);
   h1D_target_minus_toweight__8->SetBinContent(98,-0.0001383983);
   h1D_target_minus_toweight__8->SetBinContent(99,0.0001629055);
   h1D_target_minus_toweight__8->SetBinContent(100,-0.0003504567);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("#phi (D^{+})");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{+} #rightarrow K_{S}^{0} h^{+})#minusN(D^{+} #rightarrow K^{#minus} #pi^{+} h^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",-3.1416,3.1);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   Dplus_PHI_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(-4.3119,-0.05475177,3.4901,0.03966907);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,-3.1416,3.1);
   h1D_asym__9->SetBinContent(1,-0.01294279);
   h1D_asym__9->SetBinContent(2,-0.00645499);
   h1D_asym__9->SetBinContent(3,0.004554867);
   h1D_asym__9->SetBinContent(4,-0.01077631);
   h1D_asym__9->SetBinContent(5,-0.01264517);
   h1D_asym__9->SetBinContent(6,-0.0156827);
   h1D_asym__9->SetBinContent(7,-0.01451094);
   h1D_asym__9->SetBinContent(8,-0.02661497);
   h1D_asym__9->SetBinContent(9,-0.02080284);
   h1D_asym__9->SetBinContent(10,-0.01156594);
   h1D_asym__9->SetBinContent(11,-0.02486799);
   h1D_asym__9->SetBinContent(12,-0.01185427);
   h1D_asym__9->SetBinContent(13,-0.02314032);
   h1D_asym__9->SetBinContent(14,-0.01675122);
   h1D_asym__9->SetBinContent(15,-0.02013386);
   h1D_asym__9->SetBinContent(16,-0.01431856);
   h1D_asym__9->SetBinContent(17,-0.02070114);
   h1D_asym__9->SetBinContent(18,-0.01826272);
   h1D_asym__9->SetBinContent(19,-0.01516049);
   h1D_asym__9->SetBinContent(20,-0.01376812);
   h1D_asym__9->SetBinContent(21,-0.008766856);
   h1D_asym__9->SetBinContent(22,-0.009134972);
   h1D_asym__9->SetBinContent(23,-0.004588375);
   h1D_asym__9->SetBinContent(24,-0.0009085842);
   h1D_asym__9->SetBinContent(25,-0.003630895);
   h1D_asym__9->SetBinContent(26,-0.003946756);
   h1D_asym__9->SetBinContent(27,-0.01939872);
   h1D_asym__9->SetBinContent(28,-0.02723278);
   h1D_asym__9->SetBinContent(29,-0.02612203);
   h1D_asym__9->SetBinContent(30,-0.02728938);
   h1D_asym__9->SetBinContent(31,-0.02350638);
   h1D_asym__9->SetBinContent(32,-0.01176205);
   h1D_asym__9->SetBinContent(33,0.00614542);
   h1D_asym__9->SetBinContent(34,-0.005371962);
   h1D_asym__9->SetBinContent(35,-0.002408687);
   h1D_asym__9->SetBinContent(36,-0.002022323);
   h1D_asym__9->SetBinContent(37,0.01109631);
   h1D_asym__9->SetBinContent(38,0.01141319);
   h1D_asym__9->SetBinContent(39,0.003679551);
   h1D_asym__9->SetBinContent(40,5.305826e-05);
   h1D_asym__9->SetBinContent(41,-0.003265164);
   h1D_asym__9->SetBinContent(42,-0.003025972);
   h1D_asym__9->SetBinContent(43,-0.0003562101);
   h1D_asym__9->SetBinContent(44,-0.002355942);
   h1D_asym__9->SetBinContent(45,0.002206293);
   h1D_asym__9->SetBinContent(46,-0.002367117);
   h1D_asym__9->SetBinContent(47,-0.001461162);
   h1D_asym__9->SetBinContent(48,-0.01317739);
   h1D_asym__9->SetBinContent(49,-0.01226752);
   h1D_asym__9->SetBinContent(50,-0.003639988);
   h1D_asym__9->SetBinContent(51,-0.0126972);
   h1D_asym__9->SetBinContent(52,-0.01732114);
   h1D_asym__9->SetBinContent(53,-0.01516482);
   h1D_asym__9->SetBinContent(54,-0.01410377);
   h1D_asym__9->SetBinContent(55,-0.01485349);
   h1D_asym__9->SetBinContent(56,-0.01307656);
   h1D_asym__9->SetBinContent(57,-0.006684402);
   h1D_asym__9->SetBinContent(58,-0.003116988);
   h1D_asym__9->SetBinContent(59,-0.006428027);
   h1D_asym__9->SetBinContent(60,-0.00175269);
   h1D_asym__9->SetBinContent(61,-0.000293351);
   h1D_asym__9->SetBinContent(62,0.0004939273);
   h1D_asym__9->SetBinContent(63,0.00925513);
   h1D_asym__9->SetBinContent(64,0.00765821);
   h1D_asym__9->SetBinContent(65,0.0140172);
   h1D_asym__9->SetBinContent(66,0.005495134);
   h1D_asym__9->SetBinContent(67,0.006001298);
   h1D_asym__9->SetBinContent(68,-0.0004488856);
   h1D_asym__9->SetBinContent(69,-0.0002424911);
   h1D_asym__9->SetBinContent(70,-0.002042904);
   h1D_asym__9->SetBinContent(71,-0.01136965);
   h1D_asym__9->SetBinContent(72,-0.02264055);
   h1D_asym__9->SetBinContent(73,-0.01741928);
   h1D_asym__9->SetBinContent(74,-0.0174384);
   h1D_asym__9->SetBinContent(75,-0.007257187);
   h1D_asym__9->SetBinContent(76,-0.009453534);
   h1D_asym__9->SetBinContent(77,-7.597512e-05);
   h1D_asym__9->SetBinContent(78,-0.001315617);
   h1D_asym__9->SetBinContent(79,-0.005161142);
   h1D_asym__9->SetBinContent(80,-0.01298822);
   h1D_asym__9->SetBinContent(81,-0.02007982);
   h1D_asym__9->SetBinContent(82,-0.02896467);
   h1D_asym__9->SetBinContent(83,-0.02512927);
   h1D_asym__9->SetBinContent(84,-0.03356502);
   h1D_asym__9->SetBinContent(85,-0.03691706);
   h1D_asym__9->SetBinContent(86,-0.04006426);
   h1D_asym__9->SetBinContent(87,-0.02487728);
   h1D_asym__9->SetBinContent(88,-0.0279938);
   h1D_asym__9->SetBinContent(89,-0.02326429);
   h1D_asym__9->SetBinContent(90,-0.02119236);
   h1D_asym__9->SetBinContent(91,-0.0163555);
   h1D_asym__9->SetBinContent(92,-0.01072322);
   h1D_asym__9->SetBinContent(93,-0.01212311);
   h1D_asym__9->SetBinContent(94,-0.01131972);
   h1D_asym__9->SetBinContent(95,-0.02093756);
   h1D_asym__9->SetBinContent(96,-0.01544287);
   h1D_asym__9->SetBinContent(97,-0.009206506);
   h1D_asym__9->SetBinContent(98,-0.01978378);
   h1D_asym__9->SetBinContent(99,-0.02308151);
   h1D_asym__9->SetBinContent(100,-0.01397611);
   h1D_asym__9->SetBinError(1,0.0158561);
   h1D_asym__9->SetBinError(2,0.01437189);
   h1D_asym__9->SetBinError(3,0.01334119);
   h1D_asym__9->SetBinError(4,0.01170796);
   h1D_asym__9->SetBinError(5,0.011551);
   h1D_asym__9->SetBinError(6,0.01119727);
   h1D_asym__9->SetBinError(7,0.01105348);
   h1D_asym__9->SetBinError(8,0.01076196);
   h1D_asym__9->SetBinError(9,0.01087452);
   h1D_asym__9->SetBinError(10,0.01103826);
   h1D_asym__9->SetBinError(11,0.01082117);
   h1D_asym__9->SetBinError(12,0.0112603);
   h1D_asym__9->SetBinError(13,0.01104186);
   h1D_asym__9->SetBinError(14,0.01128975);
   h1D_asym__9->SetBinError(15,0.011465);
   h1D_asym__9->SetBinError(16,0.0117837);
   h1D_asym__9->SetBinError(17,0.0119651);
   h1D_asym__9->SetBinError(18,0.01207449);
   h1D_asym__9->SetBinError(19,0.01237695);
   h1D_asym__9->SetBinError(20,0.01264586);
   h1D_asym__9->SetBinError(21,0.01326227);
   h1D_asym__9->SetBinError(22,0.01375454);
   h1D_asym__9->SetBinError(23,0.01466725);
   h1D_asym__9->SetBinError(24,0.0151743);
   h1D_asym__9->SetBinError(25,0.01553846);
   h1D_asym__9->SetBinError(26,0.01570838);
   h1D_asym__9->SetBinError(27,0.01523263);
   h1D_asym__9->SetBinError(28,0.01459977);
   h1D_asym__9->SetBinError(29,0.0139743);
   h1D_asym__9->SetBinError(30,0.01322795);
   h1D_asym__9->SetBinError(31,0.01265456);
   h1D_asym__9->SetBinError(32,0.01248896);
   h1D_asym__9->SetBinError(33,0.012436);
   h1D_asym__9->SetBinError(34,0.01229911);
   h1D_asym__9->SetBinError(35,0.01202678);
   h1D_asym__9->SetBinError(36,0.01216575);
   h1D_asym__9->SetBinError(37,0.01293878);
   h1D_asym__9->SetBinError(38,0.01224819);
   h1D_asym__9->SetBinError(39,0.01215707);
   h1D_asym__9->SetBinError(40,0.01209353);
   h1D_asym__9->SetBinError(41,0.01311039);
   h1D_asym__9->SetBinError(42,0.01160553);
   h1D_asym__9->SetBinError(43,0.01159472);
   h1D_asym__9->SetBinError(44,0.01169418);
   h1D_asym__9->SetBinError(45,0.01191314);
   h1D_asym__9->SetBinError(46,0.01200622);
   h1D_asym__9->SetBinError(47,0.01220729);
   h1D_asym__9->SetBinError(48,0.01181114);
   h1D_asym__9->SetBinError(49,0.011927);
   h1D_asym__9->SetBinError(50,0.01211627);
   h1D_asym__9->SetBinError(51,0.01175701);
   h1D_asym__9->SetBinError(52,0.01182343);
   h1D_asym__9->SetBinError(53,0.01182828);
   h1D_asym__9->SetBinError(54,0.01168533);
   h1D_asym__9->SetBinError(55,0.0115541);
   h1D_asym__9->SetBinError(56,0.0114576);
   h1D_asym__9->SetBinError(57,0.01145851);
   h1D_asym__9->SetBinError(58,0.01143671);
   h1D_asym__9->SetBinError(59,0.01136685);
   h1D_asym__9->SetBinError(60,0.01153386);
   h1D_asym__9->SetBinError(61,0.01164041);
   h1D_asym__9->SetBinError(62,0.01162563);
   h1D_asym__9->SetBinError(63,0.01182518);
   h1D_asym__9->SetBinError(64,0.01197794);
   h1D_asym__9->SetBinError(65,0.01216318);
   h1D_asym__9->SetBinError(66,0.01198491);
   h1D_asym__9->SetBinError(67,0.01219005);
   h1D_asym__9->SetBinError(68,0.0120959);
   h1D_asym__9->SetBinError(69,0.01251681);
   h1D_asym__9->SetBinError(70,0.0129573);
   h1D_asym__9->SetBinError(71,0.01317363);
   h1D_asym__9->SetBinError(72,0.01375984);
   h1D_asym__9->SetBinError(73,0.01473187);
   h1D_asym__9->SetBinError(74,0.01584772);
   h1D_asym__9->SetBinError(75,0.01615411);
   h1D_asym__9->SetBinError(76,0.01644438);
   h1D_asym__9->SetBinError(77,0.01597821);
   h1D_asym__9->SetBinError(78,0.01560122);
   h1D_asym__9->SetBinError(79,0.01468439);
   h1D_asym__9->SetBinError(80,0.01372064);
   h1D_asym__9->SetBinError(81,0.01297917);
   h1D_asym__9->SetBinError(82,0.01218517);
   h1D_asym__9->SetBinError(83,0.01176616);
   h1D_asym__9->SetBinError(84,0.01145501);
   h1D_asym__9->SetBinError(85,0.01135301);
   h1D_asym__9->SetBinError(86,0.0108336);
   h1D_asym__9->SetBinError(87,0.01117807);
   h1D_asym__9->SetBinError(88,0.010892);
   h1D_asym__9->SetBinError(89,0.0108307);
   h1D_asym__9->SetBinError(90,0.0108173);
   h1D_asym__9->SetBinError(91,0.01092033);
   h1D_asym__9->SetBinError(92,0.01129117);
   h1D_asym__9->SetBinError(93,0.01122762);
   h1D_asym__9->SetBinError(94,0.01124028);
   h1D_asym__9->SetBinError(95,0.01097704);
   h1D_asym__9->SetBinError(96,0.01134604);
   h1D_asym__9->SetBinError(97,0.01170221);
   h1D_asym__9->SetBinError(98,0.01201782);
   h1D_asym__9->SetBinError(99,0.01267369);
   h1D_asym__9->SetBinError(100,0.01438491);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("#phi (D^{+})");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",-3.1416,3.1);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","-0.010866059667",-3.1416,3.1);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   Dplus_PHI_asym->cd();
   Dplus_PHI_asym->Modified();
   Dplus_PHI_asym->cd();
   Dplus_PHI_asym->SetSelected(Dplus_PHI_asym);
}
