void hplus_PY_asym()
{
//=========Macro generated from canvas: hplus_PY_asym/hplus_PY_asym
//=========  (Mon Nov 18 16:31:02 2019) by ROOT version6.08/02
   TCanvas *hplus_PY_asym = new TCanvas("hplus_PY_asym", "hplus_PY_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   hplus_PY_asym->SetHighLightColor(2);
   hplus_PY_asym->Range(0,0,1,1);
   hplus_PY_asym->SetFillColor(0);
   hplus_PY_asym->SetBorderMode(0);
   hplus_PY_asym->SetBorderSize(10);
   hplus_PY_asym->SetTickx(1);
   hplus_PY_asym->SetTicky(1);
   hplus_PY_asym->SetLeftMargin(0.18);
   hplus_PY_asym->SetRightMargin(0.05);
   hplus_PY_asym->SetTopMargin(0.07);
   hplus_PY_asym->SetBottomMargin(0.16);
   hplus_PY_asym->SetFrameFillStyle(0);
   hplus_PY_asym->SetFrameLineStyle(0);
   hplus_PY_asym->SetFrameLineColor(0);
   hplus_PY_asym->SetFrameLineWidth(0);
   hplus_PY_asym->SetFrameBorderMode(0);
   hplus_PY_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(-8250,-0.001435909,6750,0.0009532278);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,-6000,6000);
   h1D_target_minus_toweight__8->SetBinContent(1,-7.678973e-06);
   h1D_target_minus_toweight__8->SetBinContent(2,-2.959633e-05);
   h1D_target_minus_toweight__8->SetBinContent(3,-5.722401e-05);
   h1D_target_minus_toweight__8->SetBinContent(4,-5.081767e-05);
   h1D_target_minus_toweight__8->SetBinContent(5,-6.798096e-05);
   h1D_target_minus_toweight__8->SetBinContent(6,-1.337752e-05);
   h1D_target_minus_toweight__8->SetBinContent(7,7.127482e-05);
   h1D_target_minus_toweight__8->SetBinContent(8,-1.494668e-05);
   h1D_target_minus_toweight__8->SetBinContent(9,-0.0001362569);
   h1D_target_minus_toweight__8->SetBinContent(10,-0.0001315204);
   h1D_target_minus_toweight__8->SetBinContent(11,5.090237e-05);
   h1D_target_minus_toweight__8->SetBinContent(12,-2.817623e-05);
   h1D_target_minus_toweight__8->SetBinContent(13,-5.932269e-05);
   h1D_target_minus_toweight__8->SetBinContent(14,-4.727347e-05);
   h1D_target_minus_toweight__8->SetBinContent(15,-3.135647e-05);
   h1D_target_minus_toweight__8->SetBinContent(16,-6.06575e-05);
   h1D_target_minus_toweight__8->SetBinContent(17,4.017306e-05);
   h1D_target_minus_toweight__8->SetBinContent(18,5.82072e-05);
   h1D_target_minus_toweight__8->SetBinContent(19,-4.464202e-05);
   h1D_target_minus_toweight__8->SetBinContent(20,1.822319e-05);
   h1D_target_minus_toweight__8->SetBinContent(21,-1.528952e-05);
   h1D_target_minus_toweight__8->SetBinContent(22,0.0001343354);
   h1D_target_minus_toweight__8->SetBinContent(23,-4.161708e-05);
   h1D_target_minus_toweight__8->SetBinContent(24,5.971268e-05);
   h1D_target_minus_toweight__8->SetBinContent(25,0.0001479806);
   h1D_target_minus_toweight__8->SetBinContent(26,-0.0004017437);
   h1D_target_minus_toweight__8->SetBinContent(27,-0.0001613535);
   h1D_target_minus_toweight__8->SetBinContent(28,-0.0002233144);
   h1D_target_minus_toweight__8->SetBinContent(29,-0.0002408316);
   h1D_target_minus_toweight__8->SetBinContent(30,9.52743e-05);
   h1D_target_minus_toweight__8->SetBinContent(31,-0.000171816);
   h1D_target_minus_toweight__8->SetBinContent(32,-2.851337e-05);
   h1D_target_minus_toweight__8->SetBinContent(33,0.0002561994);
   h1D_target_minus_toweight__8->SetBinContent(34,0.0003565419);
   h1D_target_minus_toweight__8->SetBinContent(35,-4.189461e-05);
   h1D_target_minus_toweight__8->SetBinContent(36,-0.0002013203);
   h1D_target_minus_toweight__8->SetBinContent(37,-0.0002221074);
   h1D_target_minus_toweight__8->SetBinContent(38,0.0002115574);
   h1D_target_minus_toweight__8->SetBinContent(39,0.0001612995);
   h1D_target_minus_toweight__8->SetBinContent(40,0.0003650654);
   h1D_target_minus_toweight__8->SetBinContent(41,0.000547668);
   h1D_target_minus_toweight__8->SetBinContent(42,0.0005922094);
   h1D_target_minus_toweight__8->SetBinContent(43,0.0004200665);
   h1D_target_minus_toweight__8->SetBinContent(44,0.000280275);
   h1D_target_minus_toweight__8->SetBinContent(45,-0.0003955588);
   h1D_target_minus_toweight__8->SetBinContent(46,-0.0006433222);
   h1D_target_minus_toweight__8->SetBinContent(47,-0.0006041387);
   h1D_target_minus_toweight__8->SetBinContent(48,-0.0001681279);
   h1D_target_minus_toweight__8->SetBinContent(49,8.020084e-05);
   h1D_target_minus_toweight__8->SetBinContent(50,0.0003626519);
   h1D_target_minus_toweight__8->SetBinContent(51,0.0003976873);
   h1D_target_minus_toweight__8->SetBinContent(52,-0.0002372153);
   h1D_target_minus_toweight__8->SetBinContent(53,-2.641231e-06);
   h1D_target_minus_toweight__8->SetBinContent(54,-0.0005245451);
   h1D_target_minus_toweight__8->SetBinContent(55,-0.0006276807);
   h1D_target_minus_toweight__8->SetBinContent(56,-0.0005107448);
   h1D_target_minus_toweight__8->SetBinContent(57,-6.391294e-05);
   h1D_target_minus_toweight__8->SetBinContent(58,0.0005717631);
   h1D_target_minus_toweight__8->SetBinContent(59,0.0008735899);
   h1D_target_minus_toweight__8->SetBinContent(60,0.0002221167);
   h1D_target_minus_toweight__8->SetBinContent(61,0.0002488568);
   h1D_target_minus_toweight__8->SetBinContent(62,0.000260381);
   h1D_target_minus_toweight__8->SetBinContent(63,-0.0002256874);
   h1D_target_minus_toweight__8->SetBinContent(64,-6.103702e-05);
   h1D_target_minus_toweight__8->SetBinContent(65,-0.0001945198);
   h1D_target_minus_toweight__8->SetBinContent(66,0.0001898669);
   h1D_target_minus_toweight__8->SetBinContent(67,0.000286784);
   h1D_target_minus_toweight__8->SetBinContent(68,0.0003440827);
   h1D_target_minus_toweight__8->SetBinContent(69,6.416813e-05);
   h1D_target_minus_toweight__8->SetBinContent(70,2.301671e-05);
   h1D_target_minus_toweight__8->SetBinContent(71,7.070228e-05);
   h1D_target_minus_toweight__8->SetBinContent(72,8.784421e-05);
   h1D_target_minus_toweight__8->SetBinContent(73,0.0001951456);
   h1D_target_minus_toweight__8->SetBinContent(74,4.488416e-05);
   h1D_target_minus_toweight__8->SetBinContent(75,-7.312745e-05);
   h1D_target_minus_toweight__8->SetBinContent(76,-0.000110059);
   h1D_target_minus_toweight__8->SetBinContent(77,5.884562e-05);
   h1D_target_minus_toweight__8->SetBinContent(78,-9.630434e-05);
   h1D_target_minus_toweight__8->SetBinContent(79,0.0001355852);
   h1D_target_minus_toweight__8->SetBinContent(80,6.603543e-06);
   h1D_target_minus_toweight__8->SetBinContent(81,-0.0001603873);
   h1D_target_minus_toweight__8->SetBinContent(82,-3.952067e-06);
   h1D_target_minus_toweight__8->SetBinContent(83,-6.474182e-05);
   h1D_target_minus_toweight__8->SetBinContent(84,6.573461e-05);
   h1D_target_minus_toweight__8->SetBinContent(85,-0.000240956);
   h1D_target_minus_toweight__8->SetBinContent(86,-4.996546e-06);
   h1D_target_minus_toweight__8->SetBinContent(87,-0.0003295178);
   h1D_target_minus_toweight__8->SetBinContent(88,-0.0001655242);
   h1D_target_minus_toweight__8->SetBinContent(89,-0.000102151);
   h1D_target_minus_toweight__8->SetBinContent(90,8.898089e-06);
   h1D_target_minus_toweight__8->SetBinContent(91,-6.073993e-05);
   h1D_target_minus_toweight__8->SetBinContent(92,-0.0001288458);
   h1D_target_minus_toweight__8->SetBinContent(93,-6.267545e-05);
   h1D_target_minus_toweight__8->SetBinContent(94,-6.130349e-05);
   h1D_target_minus_toweight__8->SetBinContent(95,-3.703637e-06);
   h1D_target_minus_toweight__8->SetBinContent(96,-3.319746e-05);
   h1D_target_minus_toweight__8->SetBinContent(97,1.276424e-05);
   h1D_target_minus_toweight__8->SetBinContent(98,4.666002e-05);
   h1D_target_minus_toweight__8->SetBinContent(99,-4.361285e-05);
   h1D_target_minus_toweight__8->SetBinContent(100,9.753043e-06);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("p_{Y} (h^{+}) [MeV/c]");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{+} #rightarrow K_{S}^{0} h^{+})#minusN(D^{+} #rightarrow K^{#minus} #pi^{+} h^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",-6000,6000);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   hplus_PY_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(-8250,-0.07337195,6750,0.1031992);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,-6000,6000);
   h1D_asym__9->SetBinContent(1,-0.003433079);
   h1D_asym__9->SetBinContent(2,0.007411104);
   h1D_asym__9->SetBinContent(3,0.01689797);
   h1D_asym__9->SetBinContent(4,-0.0252731);
   h1D_asym__9->SetBinContent(5,0.000618762);
   h1D_asym__9->SetBinContent(6,0.003786243);
   h1D_asym__9->SetBinContent(7,-0.005169077);
   h1D_asym__9->SetBinContent(8,-0.007911967);
   h1D_asym__9->SetBinContent(9,-0.009145972);
   h1D_asym__9->SetBinContent(10,-0.01505042);
   h1D_asym__9->SetBinContent(11,-0.007565718);
   h1D_asym__9->SetBinContent(12,-0.00409181);
   h1D_asym__9->SetBinContent(13,-0.004946816);
   h1D_asym__9->SetBinContent(14,-0.002099303);
   h1D_asym__9->SetBinContent(15,-0.006393835);
   h1D_asym__9->SetBinContent(16,-0.008709218);
   h1D_asym__9->SetBinContent(17,-0.008611614);
   h1D_asym__9->SetBinContent(18,-0.001460559);
   h1D_asym__9->SetBinContent(19,-0.006705621);
   h1D_asym__9->SetBinContent(20,-0.00758699);
   h1D_asym__9->SetBinContent(21,-0.007488011);
   h1D_asym__9->SetBinContent(22,-0.008352525);
   h1D_asym__9->SetBinContent(23,-0.008561605);
   h1D_asym__9->SetBinContent(24,-0.008732171);
   h1D_asym__9->SetBinContent(25,-0.005485296);
   h1D_asym__9->SetBinContent(26,-0.007855311);
   h1D_asym__9->SetBinContent(27,-0.00737214);
   h1D_asym__9->SetBinContent(28,-0.009935972);
   h1D_asym__9->SetBinContent(29,-0.006116895);
   h1D_asym__9->SetBinContent(30,-0.006013721);
   h1D_asym__9->SetBinContent(31,-0.00816137);
   h1D_asym__9->SetBinContent(32,-0.008124441);
   h1D_asym__9->SetBinContent(33,-0.006809897);
   h1D_asym__9->SetBinContent(34,-0.006586928);
   h1D_asym__9->SetBinContent(35,-0.008465962);
   h1D_asym__9->SetBinContent(36,-0.0055312);
   h1D_asym__9->SetBinContent(37,-0.003262746);
   h1D_asym__9->SetBinContent(38,-0.003741134);
   h1D_asym__9->SetBinContent(39,-0.005550707);
   h1D_asym__9->SetBinContent(40,-0.003977543);
   h1D_asym__9->SetBinContent(41,-0.00569529);
   h1D_asym__9->SetBinContent(42,-0.004069901);
   h1D_asym__9->SetBinContent(43,-0.005347889);
   h1D_asym__9->SetBinContent(44,-0.007703953);
   h1D_asym__9->SetBinContent(45,-0.007707736);
   h1D_asym__9->SetBinContent(46,-0.007143165);
   h1D_asym__9->SetBinContent(47,-0.002452357);
   h1D_asym__9->SetBinContent(48,-0.00288416);
   h1D_asym__9->SetBinContent(49,-0.007691768);
   h1D_asym__9->SetBinContent(50,-0.01959731);
   h1D_asym__9->SetBinContent(51,-0.02425358);
   h1D_asym__9->SetBinContent(52,-0.006998918);
   h1D_asym__9->SetBinContent(53,-0.005377882);
   h1D_asym__9->SetBinContent(54,-0.003141448);
   h1D_asym__9->SetBinContent(55,-0.00707561);
   h1D_asym__9->SetBinContent(56,-0.005973225);
   h1D_asym__9->SetBinContent(57,-0.007568657);
   h1D_asym__9->SetBinContent(58,-0.006494905);
   h1D_asym__9->SetBinContent(59,-0.008602893);
   h1D_asym__9->SetBinContent(60,-0.005320812);
   h1D_asym__9->SetBinContent(61,-0.006030655);
   h1D_asym__9->SetBinContent(62,-0.005556023);
   h1D_asym__9->SetBinContent(63,-0.00778225);
   h1D_asym__9->SetBinContent(64,-0.007685862);
   h1D_asym__9->SetBinContent(65,-0.006373477);
   h1D_asym__9->SetBinContent(66,-0.01035237);
   h1D_asym__9->SetBinContent(67,-0.006506679);
   h1D_asym__9->SetBinContent(68,-0.007624719);
   h1D_asym__9->SetBinContent(69,-0.006440551);
   h1D_asym__9->SetBinContent(70,-0.004361356);
   h1D_asym__9->SetBinContent(71,-0.004892738);
   h1D_asym__9->SetBinContent(72,-0.00565551);
   h1D_asym__9->SetBinContent(73,-0.008957298);
   h1D_asym__9->SetBinContent(74,-0.00620633);
   h1D_asym__9->SetBinContent(75,-0.006323474);
   h1D_asym__9->SetBinContent(76,-0.00910265);
   h1D_asym__9->SetBinContent(77,-0.009069063);
   h1D_asym__9->SetBinContent(78,-0.008977124);
   h1D_asym__9->SetBinContent(79,-0.004239972);
   h1D_asym__9->SetBinContent(80,-0.01122967);
   h1D_asym__9->SetBinContent(81,-0.005351076);
   h1D_asym__9->SetBinContent(82,-0.006585427);
   h1D_asym__9->SetBinContent(83,-0.00773826);
   h1D_asym__9->SetBinContent(84,-0.005706395);
   h1D_asym__9->SetBinContent(85,-0.006178181);
   h1D_asym__9->SetBinContent(86,0.001396132);
   h1D_asym__9->SetBinContent(87,-0.004348877);
   h1D_asym__9->SetBinContent(88,-0.001479082);
   h1D_asym__9->SetBinContent(89,0.0009203967);
   h1D_asym__9->SetBinContent(90,-0.01562821);
   h1D_asym__9->SetBinContent(91,-0.00374165);
   h1D_asym__9->SetBinContent(92,-0.001713361);
   h1D_asym__9->SetBinContent(93,-0.004084747);
   h1D_asym__9->SetBinContent(94,-1.941627e-05);
   h1D_asym__9->SetBinContent(95,0.001681758);
   h1D_asym__9->SetBinContent(96,-0.005048719);
   h1D_asym__9->SetBinContent(97,0.003512433);
   h1D_asym__9->SetBinContent(98,0.003796034);
   h1D_asym__9->SetBinContent(99,0.02708661);
   h1D_asym__9->SetBinContent(100,0.01287706);
   h1D_asym__9->SetBinError(1,0.06273188);
   h1D_asym__9->SetBinError(2,0.04279358);
   h1D_asym__9->SetBinError(3,0.03566475);
   h1D_asym__9->SetBinError(4,0.02863448);
   h1D_asym__9->SetBinError(5,0.02650156);
   h1D_asym__9->SetBinError(6,0.02436916);
   h1D_asym__9->SetBinError(7,0.0216828);
   h1D_asym__9->SetBinError(8,0.01945397);
   h1D_asym__9->SetBinError(9,0.01764563);
   h1D_asym__9->SetBinError(10,0.01644373);
   h1D_asym__9->SetBinError(11,0.01517304);
   h1D_asym__9->SetBinError(12,0.01404877);
   h1D_asym__9->SetBinError(13,0.01307191);
   h1D_asym__9->SetBinError(14,0.01240778);
   h1D_asym__9->SetBinError(15,0.01141256);
   h1D_asym__9->SetBinError(16,0.01051993);
   h1D_asym__9->SetBinError(17,0.009772821);
   h1D_asym__9->SetBinError(18,0.009223972);
   h1D_asym__9->SetBinError(19,0.008570649);
   h1D_asym__9->SetBinError(20,0.007975186);
   h1D_asym__9->SetBinError(21,0.007457814);
   h1D_asym__9->SetBinError(22,0.00690964);
   h1D_asym__9->SetBinError(23,0.006503839);
   h1D_asym__9->SetBinError(24,0.006043102);
   h1D_asym__9->SetBinError(25,0.00570409);
   h1D_asym__9->SetBinError(26,0.005298182);
   h1D_asym__9->SetBinError(27,0.004946154);
   h1D_asym__9->SetBinError(28,0.004603337);
   h1D_asym__9->SetBinError(29,0.004331755);
   h1D_asym__9->SetBinError(30,0.004057029);
   h1D_asym__9->SetBinError(31,0.00377752);
   h1D_asym__9->SetBinError(32,0.003543136);
   h1D_asym__9->SetBinError(33,0.003378377);
   h1D_asym__9->SetBinError(34,0.003276928);
   h1D_asym__9->SetBinError(35,0.003218392);
   h1D_asym__9->SetBinError(36,0.003249793);
   h1D_asym__9->SetBinError(37,0.003368853);
   h1D_asym__9->SetBinError(38,0.003501842);
   h1D_asym__9->SetBinError(39,0.003580972);
   h1D_asym__9->SetBinError(40,0.00366352);
   h1D_asym__9->SetBinError(41,0.003697859);
   h1D_asym__9->SetBinError(42,0.003768289);
   h1D_asym__9->SetBinError(43,0.003804932);
   h1D_asym__9->SetBinError(44,0.003855245);
   h1D_asym__9->SetBinError(45,0.003921453);
   h1D_asym__9->SetBinError(46,0.004049081);
   h1D_asym__9->SetBinError(47,0.00422457);
   h1D_asym__9->SetBinError(48,0.004329355);
   h1D_asym__9->SetBinError(49,0.004537281);
   h1D_asym__9->SetBinError(50,0.005866403);
   h1D_asym__9->SetBinError(51,0.005608169);
   h1D_asym__9->SetBinError(52,0.004468882);
   h1D_asym__9->SetBinError(53,0.004308085);
   h1D_asym__9->SetBinError(54,0.004171397);
   h1D_asym__9->SetBinError(55,0.003997925);
   h1D_asym__9->SetBinError(56,0.003860055);
   h1D_asym__9->SetBinError(57,0.003768652);
   h1D_asym__9->SetBinError(58,0.00375054);
   h1D_asym__9->SetBinError(59,0.003696589);
   h1D_asym__9->SetBinError(60,0.003660634);
   h1D_asym__9->SetBinError(61,0.003604699);
   h1D_asym__9->SetBinError(62,0.003523804);
   h1D_asym__9->SetBinError(63,0.003425825);
   h1D_asym__9->SetBinError(64,0.003269787);
   h1D_asym__9->SetBinError(65,0.003204209);
   h1D_asym__9->SetBinError(66,0.003145504);
   h1D_asym__9->SetBinError(67,0.003212329);
   h1D_asym__9->SetBinError(68,0.003329473);
   h1D_asym__9->SetBinError(69,0.003499909);
   h1D_asym__9->SetBinError(70,0.003736249);
   h1D_asym__9->SetBinError(71,0.004002136);
   h1D_asym__9->SetBinError(72,0.00429002);
   h1D_asym__9->SetBinError(73,0.00453459);
   h1D_asym__9->SetBinError(74,0.004873802);
   h1D_asym__9->SetBinError(75,0.005249063);
   h1D_asym__9->SetBinError(76,0.005553266);
   h1D_asym__9->SetBinError(77,0.005959392);
   h1D_asym__9->SetBinError(78,0.006378917);
   h1D_asym__9->SetBinError(79,0.006900831);
   h1D_asym__9->SetBinError(80,0.007291561);
   h1D_asym__9->SetBinError(81,0.007890909);
   h1D_asym__9->SetBinError(82,0.008628484);
   h1D_asym__9->SetBinError(83,0.009153054);
   h1D_asym__9->SetBinError(84,0.009784327);
   h1D_asym__9->SetBinError(85,0.01041588);
   h1D_asym__9->SetBinError(86,0.01136227);
   h1D_asym__9->SetBinError(87,0.01203995);
   h1D_asym__9->SetBinError(88,0.01331508);
   h1D_asym__9->SetBinError(89,0.01410798);
   h1D_asym__9->SetBinError(90,0.01493012);
   h1D_asym__9->SetBinError(91,0.01639792);
   h1D_asym__9->SetBinError(92,0.01792622);
   h1D_asym__9->SetBinError(93,0.01926507);
   h1D_asym__9->SetBinError(94,0.02164147);
   h1D_asym__9->SetBinError(95,0.02416003);
   h1D_asym__9->SetBinError(96,0.02650652);
   h1D_asym__9->SetBinError(97,0.03053584);
   h1D_asym__9->SetBinError(98,0.03569182);
   h1D_asym__9->SetBinError(99,0.04834794);
   h1D_asym__9->SetBinError(100,0.0650977);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("p_{Y} (h^{+}) [MeV/c]");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",-6000,6000);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","-0.006620257712",-6000,6000);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   hplus_PY_asym->cd();
   hplus_PY_asym->Modified();
   hplus_PY_asym->cd();
   hplus_PY_asym->SetSelected(hplus_PY_asym);
}
