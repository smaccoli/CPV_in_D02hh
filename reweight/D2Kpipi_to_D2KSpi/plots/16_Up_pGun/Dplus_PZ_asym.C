void Dplus_PZ_asym()
{
//=========Macro generated from canvas: Dplus_PZ_asym/Dplus_PZ_asym
//=========  (Tue Nov 26 12:37:25 2019) by ROOT version6.08/02
   TCanvas *Dplus_PZ_asym = new TCanvas("Dplus_PZ_asym", "Dplus_PZ_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   Dplus_PZ_asym->SetHighLightColor(2);
   Dplus_PZ_asym->Range(0,0,1,1);
   Dplus_PZ_asym->SetFillColor(0);
   Dplus_PZ_asym->SetBorderMode(0);
   Dplus_PZ_asym->SetBorderSize(10);
   Dplus_PZ_asym->SetTickx(1);
   Dplus_PZ_asym->SetTicky(1);
   Dplus_PZ_asym->SetLeftMargin(0.18);
   Dplus_PZ_asym->SetRightMargin(0.05);
   Dplus_PZ_asym->SetTopMargin(0.07);
   Dplus_PZ_asym->SetBottomMargin(0.16);
   Dplus_PZ_asym->SetFrameFillStyle(0);
   Dplus_PZ_asym->SetFrameLineStyle(0);
   Dplus_PZ_asym->SetFrameLineColor(0);
   Dplus_PZ_asym->SetFrameLineWidth(0);
   Dplus_PZ_asym->SetFrameBorderMode(0);
   Dplus_PZ_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(-11250,-0.002427418,263750,0.0007464332);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,30000,250000);
   h1D_target_minus_toweight__8->SetBinContent(4,-8.63352e-06);
   h1D_target_minus_toweight__8->SetBinContent(5,-0.0001721246);
   h1D_target_minus_toweight__8->SetBinContent(6,-0.0004929174);
   h1D_target_minus_toweight__8->SetBinContent(7,-0.0007749433);
   h1D_target_minus_toweight__8->SetBinContent(8,-0.0009198347);
   h1D_target_minus_toweight__8->SetBinContent(9,-0.001187835);
   h1D_target_minus_toweight__8->SetBinContent(10,-0.001250366);
   h1D_target_minus_toweight__8->SetBinContent(11,-0.001374505);
   h1D_target_minus_toweight__8->SetBinContent(12,-0.0007431619);
   h1D_target_minus_toweight__8->SetBinContent(13,-0.0008665882);
   h1D_target_minus_toweight__8->SetBinContent(14,-0.0005118996);
   h1D_target_minus_toweight__8->SetBinContent(15,-0.000618156);
   h1D_target_minus_toweight__8->SetBinContent(16,-0.0003805421);
   h1D_target_minus_toweight__8->SetBinContent(17,-0.0005413219);
   h1D_target_minus_toweight__8->SetBinContent(18,-0.0003154054);
   h1D_target_minus_toweight__8->SetBinContent(19,0.0004211441);
   h1D_target_minus_toweight__8->SetBinContent(20,-0.0002337731);
   h1D_target_minus_toweight__8->SetBinContent(21,0.000392342);
   h1D_target_minus_toweight__8->SetBinContent(22,3.359281e-05);
   h1D_target_minus_toweight__8->SetBinContent(23,0.0001413282);
   h1D_target_minus_toweight__8->SetBinContent(24,0.0003530029);
   h1D_target_minus_toweight__8->SetBinContent(25,0.0004013143);
   h1D_target_minus_toweight__8->SetBinContent(26,0.0006277002);
   h1D_target_minus_toweight__8->SetBinContent(27,0.0001357775);
   h1D_target_minus_toweight__8->SetBinContent(28,-4.95296e-05);
   h1D_target_minus_toweight__8->SetBinContent(29,0.0006406382);
   h1D_target_minus_toweight__8->SetBinContent(30,0.0004660543);
   h1D_target_minus_toweight__8->SetBinContent(31,0.0005548038);
   h1D_target_minus_toweight__8->SetBinContent(32,0.0005248543);
   h1D_target_minus_toweight__8->SetBinContent(33,0.0004541576);
   h1D_target_minus_toweight__8->SetBinContent(34,0.0005784146);
   h1D_target_minus_toweight__8->SetBinContent(35,0.0004050303);
   h1D_target_minus_toweight__8->SetBinContent(36,0.0004298128);
   h1D_target_minus_toweight__8->SetBinContent(37,3.797188e-05);
   h1D_target_minus_toweight__8->SetBinContent(38,0.0004725214);
   h1D_target_minus_toweight__8->SetBinContent(39,0.0002909824);
   h1D_target_minus_toweight__8->SetBinContent(40,0.0002806475);
   h1D_target_minus_toweight__8->SetBinContent(41,0.000432821);
   h1D_target_minus_toweight__8->SetBinContent(42,6.908551e-05);
   h1D_target_minus_toweight__8->SetBinContent(43,-5.357433e-05);
   h1D_target_minus_toweight__8->SetBinContent(44,0.0003726948);
   h1D_target_minus_toweight__8->SetBinContent(45,0.000129506);
   h1D_target_minus_toweight__8->SetBinContent(46,0.0002282029);
   h1D_target_minus_toweight__8->SetBinContent(47,0.000511053);
   h1D_target_minus_toweight__8->SetBinContent(48,6.195437e-05);
   h1D_target_minus_toweight__8->SetBinContent(49,0.0003482131);
   h1D_target_minus_toweight__8->SetBinContent(50,0.0001060395);
   h1D_target_minus_toweight__8->SetBinContent(51,0.0001846366);
   h1D_target_minus_toweight__8->SetBinContent(52,0.0002852427);
   h1D_target_minus_toweight__8->SetBinContent(53,0.0002007028);
   h1D_target_minus_toweight__8->SetBinContent(54,0.0002374221);
   h1D_target_minus_toweight__8->SetBinContent(55,-0.0002777227);
   h1D_target_minus_toweight__8->SetBinContent(56,-7.428789e-06);
   h1D_target_minus_toweight__8->SetBinContent(57,-1.920799e-05);
   h1D_target_minus_toweight__8->SetBinContent(58,-1.921331e-05);
   h1D_target_minus_toweight__8->SetBinContent(59,-2.443938e-06);
   h1D_target_minus_toweight__8->SetBinContent(60,7.790208e-06);
   h1D_target_minus_toweight__8->SetBinContent(61,-3.111927e-08);
   h1D_target_minus_toweight__8->SetBinContent(62,3.265995e-09);
   h1D_target_minus_toweight__8->SetBinContent(63,3.069647e-07);
   h1D_target_minus_toweight__8->SetBinContent(64,1.699735e-06);
   h1D_target_minus_toweight__8->SetBinContent(65,1.699735e-06);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("p_{Z} (D^{+}) [MeV/c]");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{+} #rightarrow K_{S}^{0} h^{+})#minusN(D^{+} #rightarrow K^{#minus} #pi^{+} h^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",30000,250000);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   Dplus_PZ_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(-11250,-1.891813,263750,1.795689);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,30000,250000);
   h1D_asym__9->SetBinContent(4,-0.07223018);
   h1D_asym__9->SetBinContent(5,0.01614931);
   h1D_asym__9->SetBinContent(6,-0.01589122);
   h1D_asym__9->SetBinContent(7,0.001817387);
   h1D_asym__9->SetBinContent(8,0.003631375);
   h1D_asym__9->SetBinContent(9,-0.004046291);
   h1D_asym__9->SetBinContent(10,0.006479214);
   h1D_asym__9->SetBinContent(11,0.00670794);
   h1D_asym__9->SetBinContent(12,-0.009643862);
   h1D_asym__9->SetBinContent(13,-0.002025715);
   h1D_asym__9->SetBinContent(14,0.0003553714);
   h1D_asym__9->SetBinContent(15,-0.002989008);
   h1D_asym__9->SetBinContent(16,-0.000375116);
   h1D_asym__9->SetBinContent(17,-0.007356941);
   h1D_asym__9->SetBinContent(18,-0.01044474);
   h1D_asym__9->SetBinContent(19,-0.01414966);
   h1D_asym__9->SetBinContent(20,-0.007668776);
   h1D_asym__9->SetBinContent(21,-0.01090057);
   h1D_asym__9->SetBinContent(22,0.01144436);
   h1D_asym__9->SetBinContent(23,-0.01098063);
   h1D_asym__9->SetBinContent(24,0.002163358);
   h1D_asym__9->SetBinContent(25,-0.00805308);
   h1D_asym__9->SetBinContent(26,-0.003446258);
   h1D_asym__9->SetBinContent(27,-0.002222142);
   h1D_asym__9->SetBinContent(28,-0.00568502);
   h1D_asym__9->SetBinContent(29,0.0009321348);
   h1D_asym__9->SetBinContent(30,-0.01485428);
   h1D_asym__9->SetBinContent(31,-0.002296516);
   h1D_asym__9->SetBinContent(32,-0.01094338);
   h1D_asym__9->SetBinContent(33,0.01703646);
   h1D_asym__9->SetBinContent(34,-0.00862559);
   h1D_asym__9->SetBinContent(35,-0.01043667);
   h1D_asym__9->SetBinContent(36,-0.02439536);
   h1D_asym__9->SetBinContent(37,0.002826558);
   h1D_asym__9->SetBinContent(38,-0.01121711);
   h1D_asym__9->SetBinContent(39,-0.01648054);
   h1D_asym__9->SetBinContent(40,0.009085597);
   h1D_asym__9->SetBinContent(41,-0.003157033);
   h1D_asym__9->SetBinContent(42,-0.006500477);
   h1D_asym__9->SetBinContent(43,0.002763562);
   h1D_asym__9->SetBinContent(44,-0.02708439);
   h1D_asym__9->SetBinContent(45,-0.0152748);
   h1D_asym__9->SetBinContent(46,0.003105623);
   h1D_asym__9->SetBinContent(47,0.007832767);
   h1D_asym__9->SetBinContent(48,0.00198461);
   h1D_asym__9->SetBinContent(49,-0.004543163);
   h1D_asym__9->SetBinContent(50,-0.01058595);
   h1D_asym__9->SetBinContent(51,-0.02284621);
   h1D_asym__9->SetBinContent(52,0.01744331);
   h1D_asym__9->SetBinContent(53,0.007908754);
   h1D_asym__9->SetBinContent(54,-0.008266615);
   h1D_asym__9->SetBinContent(55,-0.01700829);
   h1D_asym__9->SetBinContent(56,0.2058752);
   h1D_asym__9->SetBinContent(57,-0.4132626);
   h1D_asym__9->SetBinContent(58,-0.3763924);
   h1D_asym__9->SetBinContent(59,-0.2003944);
   h1D_asym__9->SetBinContent(60,1);
   h1D_asym__9->SetBinContent(61,-0.04784333);
   h1D_asym__9->SetBinContent(62,-1);
   h1D_asym__9->SetBinContent(63,1);
   h1D_asym__9->SetBinError(4,0.1505804);
   h1D_asym__9->SetBinError(5,0.06151076);
   h1D_asym__9->SetBinError(6,0.03755185);
   h1D_asym__9->SetBinError(7,0.0296677);
   h1D_asym__9->SetBinError(8,0.02482297);
   h1D_asym__9->SetBinError(9,0.02160849);
   h1D_asym__9->SetBinError(10,0.01981813);
   h1D_asym__9->SetBinError(11,0.01828785);
   h1D_asym__9->SetBinError(12,0.01692214);
   h1D_asym__9->SetBinError(13,0.01658924);
   h1D_asym__9->SetBinError(14,0.01618152);
   h1D_asym__9->SetBinError(15,0.01579971);
   h1D_asym__9->SetBinError(16,0.01579098);
   h1D_asym__9->SetBinError(17,0.01560548);
   h1D_asym__9->SetBinError(18,0.01584924);
   h1D_asym__9->SetBinError(19,0.01596724);
   h1D_asym__9->SetBinError(20,0.01646429);
   h1D_asym__9->SetBinError(21,0.0167411);
   h1D_asym__9->SetBinError(22,0.01753716);
   h1D_asym__9->SetBinError(23,0.01735371);
   h1D_asym__9->SetBinError(24,0.01840833);
   h1D_asym__9->SetBinError(25,0.01844919);
   h1D_asym__9->SetBinError(26,0.01891162);
   h1D_asym__9->SetBinError(27,0.01948079);
   h1D_asym__9->SetBinError(28,0.01993875);
   h1D_asym__9->SetBinError(29,0.02072693);
   h1D_asym__9->SetBinError(30,0.02061566);
   h1D_asym__9->SetBinError(31,0.02156722);
   h1D_asym__9->SetBinError(32,0.021523);
   h1D_asym__9->SetBinError(33,0.02378211);
   h1D_asym__9->SetBinError(34,0.02299601);
   h1D_asym__9->SetBinError(35,0.02323969);
   h1D_asym__9->SetBinError(36,0.02370424);
   h1D_asym__9->SetBinError(37,0.02535286);
   h1D_asym__9->SetBinError(38,0.02519616);
   h1D_asym__9->SetBinError(39,0.02552591);
   h1D_asym__9->SetBinError(40,0.02731488);
   h1D_asym__9->SetBinError(41,0.02766451);
   h1D_asym__9->SetBinError(42,0.02792165);
   h1D_asym__9->SetBinError(43,0.03001835);
   h1D_asym__9->SetBinError(44,0.02966891);
   h1D_asym__9->SetBinError(45,0.03019703);
   h1D_asym__9->SetBinError(46,0.03264439);
   h1D_asym__9->SetBinError(47,0.03406628);
   h1D_asym__9->SetBinError(48,0.03595021);
   h1D_asym__9->SetBinError(49,0.03689316);
   h1D_asym__9->SetBinError(50,0.03661017);
   h1D_asym__9->SetBinError(51,0.04280858);
   h1D_asym__9->SetBinError(52,0.04494688);
   h1D_asym__9->SetBinError(53,0.05289752);
   h1D_asym__9->SetBinError(54,0.04989362);
   h1D_asym__9->SetBinError(55,0.07315087);
   h1D_asym__9->SetBinError(56,0.6861358);
   h1D_asym__9->SetBinError(57,0.2240728);
   h1D_asym__9->SetBinError(58,0.5202727);
   h1D_asym__9->SetBinError(59,0.9602365);
   h1D_asym__9->SetBinError(61,1.316746);
   h1D_asym__9->SetBinError(62,0.7413026);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("p_{Z} (D^{+}) [MeV/c]");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",30000,250000);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","-0.004337408118",30000,250000);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   Dplus_PZ_asym->cd();
   Dplus_PZ_asym->Modified();
   Dplus_PZ_asym->cd();
   Dplus_PZ_asym->SetSelected(Dplus_PZ_asym);
}
