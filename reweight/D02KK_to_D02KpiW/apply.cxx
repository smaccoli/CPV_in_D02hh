#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void apply(TString year, TString polarity) {

  bool isPGun = 1;
  bool rew_X = 1;
  bool use_ALL = 0;
  TString add_string = "";
  if (isPGun)
    add_string += "_pGun";

  TString inFileName = "/home/LHCB/smaccoli/CPV_in_D02hh/data/D02KmKp_"+year+"_"+polarity+add_string+".root";

  if (rew_X)
    add_string += "_X";

  if (isPGun) {
    if (use_ALL)
      add_string += "_ALL";
  } 

  myH * h_PTvsPT = new myH("/home/LHCB-T3/smaccoli/CPV_in_D02hh/reweight/D02KK_to_D02KpiW/weights/"+year+"_"+polarity+add_string+"_0.myH");
  myH * h_Dst_PTvsETA = new myH("/home/LHCB-T3/smaccoli/CPV_in_D02hh/reweight/D02KK_to_D02KpiW/weights/"+year+"_"+polarity+add_string+"_1.myH");
  myH * h_Dst_PHI = new myH("/home/LHCB-T3/smaccoli/CPV_in_D02hh/reweight/D02KK_to_D02KpiW/weights/"+year+"_"+polarity+add_string+"_2.myH");
  myH * h_sPi_PTvsETA = new myH("/home/LHCB-T3/smaccoli/CPV_in_D02hh/reweight/D02KK_to_D02KpiW/weights/"+year+"_"+polarity+add_string+"_3.myH");
  myH * h_sPi_PHI = new myH("/home/LHCB-T3/smaccoli/CPV_in_D02hh/reweight/D02KK_to_D02KpiW/weights/"+year+"_"+polarity+add_string+"_4.myH");
  

  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add(inFileName);
  
  double Dst_PT;
  toweight_tree->SetBranchAddress("Dst_PT",&Dst_PT);
  double Dst_ETA;
  toweight_tree->SetBranchAddress("Dst_ETA",&Dst_ETA);
  double Dst_PHI;
  toweight_tree->SetBranchAddress("Dst_PHI",&Dst_PHI);
  double D0_PT;
  toweight_tree->SetBranchAddress("D0_PT",&D0_PT);
  double D0_ETA;
  toweight_tree->SetBranchAddress("D0_ETA",&D0_ETA);
  double D0_PHI;
  toweight_tree->SetBranchAddress("D0_PHI",&D0_PHI);
  double sPi_PT;
  toweight_tree->SetBranchAddress("sPi_PT",&sPi_PT);
  double sPi_ETA;
  toweight_tree->SetBranchAddress("sPi_ETA",&sPi_ETA);
  double sPi_PHI;
  toweight_tree->SetBranchAddress("sPi_PHI",&sPi_PHI);

  double DTF_Mass;
  toweight_tree->SetBranchAddress("DTF_Mass",&DTF_Mass);
  int Dst_ID;
  toweight_tree->SetBranchAddress("Dst_ID",&Dst_ID);

 
  double Iw;
  vector <double> values;
  double Iw_err;
  vector <double> values_err;
  double sumw = 0;
  double Nentries_over_sumw;
  for(Long64_t i = 0; i < 
	toweight_tree->GetEntries()
	; i++){
    toweight_tree->GetEntry(i);
    
    if(!rew_X) {
      Iw = h_PTvsPT->getBC(h_PTvsPT->find({Dst_PT,sPi_PT}));
      Iw *= h_Dst_PTvsETA->getBC(h_Dst_PTvsETA->find({Dst_PT,Dst_ETA}));
      Iw *=  h_Dst_PHI->getBC(h_Dst_PHI->find({Dst_PHI}));
      Iw *= h_sPi_PTvsETA->getBC(h_sPi_PTvsETA->find({sPi_PT,sPi_ETA}));
      Iw *= h_sPi_PHI->getBC(h_sPi_PHI->find({sPi_PHI}));
   
      Iw_err = sqrt(
		    pow( Iw / h_PTvsPT->getBC(h_PTvsPT->find({Dst_PT,sPi_PT})),2)*pow(h_PTvsPT->getBE(h_PTvsPT->find({Dst_PT,sPi_PT})),2) //+....
		    
		    );
    }
    else {
      Iw = h_PTvsPT->getBC(h_PTvsPT->find({D0_PT,D0_ETA,D0_PHI}));

      Iw_err = h_PTvsPT->getBE(h_PTvsPT->find({D0_PT,D0_ETA,D0_PHI}));
    }
    
    /*
      if (Iw > 15)
      Iw = 0;
    */

    sumw += Iw;
    values.push_back(Iw);
    values_err.push_back(Iw_err);
  }
  
  Nentries_over_sumw = ((double)toweight_tree->GetEntries())/sumw;
  cout << Nentries_over_sumw << endl;

  Nentries_over_sumw = 1;
  cout << Nentries_over_sumw << endl;

  TString outFileName = inFileName;
  if (isPGun) {
    if (use_ALL)
      outFileName.ReplaceAll(".root","_ALL.root");
  } 
  outFileName.ReplaceAll(".root","_rew.root");
  if (rew_X)
    outFileName.ReplaceAll(".root","_X.root");
  
  TFile *f = TFile::Open(outFileName,"RECREATE");
  TTree * t = new TTree("ntp","ntp");
  t->Branch("Iw",&Iw);
  t->Branch("Iw_err",&Iw_err);
  t->Branch("Dst_PT",&Dst_PT);
  t->Branch("Dst_ETA",&Dst_ETA);
  t->Branch("Dst_PHI",&Dst_PHI);
  t->Branch("sPi_PT",&sPi_PT);
  t->Branch("sPi_ETA",&sPi_ETA);
  t->Branch("sPi_PHI",&sPi_PHI);
  t->Branch("DTF_Mass",&DTF_Mass);
  t->Branch("Dst_ID",&Dst_ID);

  TH1F * h_dm_plus = new TH1F("h_dm_plus","h_dm_plus",500,2004.5,2020);
  TH1F * h_dm_minus = new TH1F("h_dm_minus","h_dm_minus",500,2004.5,2020);

  for(Long64_t i = 0; i < 
	toweight_tree->GetEntries()
	; i++){
    toweight_tree->GetEntry(i);

    Iw = values.at(i)*Nentries_over_sumw;
    Iw_err = values_err.at(i)*Nentries_over_sumw;

    if(Dst_ID>0) {
      h_dm_plus->Fill(DTF_Mass,Iw);
    }
    else {
      h_dm_minus->Fill(DTF_Mass,Iw);
    }
    
    t->Fill();
  }

  f->Write();
  f->Close();
  
}

int main(int argc, char * argv[]) {

  TString year = argv[1];
  TString polarity = argv[2];

  apply(year, polarity);

  return 0;
}
