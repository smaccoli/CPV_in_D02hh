#include "/home/LHCB/smaccoli/IWeight/IDecay.cxx"

int main(int argc, char * argv[]) {

  bool rew_X = 1;
  bool isPGun = 1;
  bool use_ALL = 0;
  TString add_string = "";
  if (isPGun)
    add_string += "_pGun";

  dcastyle();
  TString year = argv[1];
  TString polarity = argv[2];
  int i_set = atoi(argv[3]);
  int N_set = atoi(argv[4]);

  dcastyle();
    
  TString toweight_inputFileName = "/home/LHCB/smaccoli/CPV_in_D02hh/data/D02KmKp_"+year+"_"+polarity+add_string+".root";
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add(toweight_inputFileName); 
  toweight_tree->AddFriend("ntp",toweight_inputFileName.ReplaceAll(".root","_SPlot.root"));

  if (isPGun) {
    if (use_ALL)
      add_string += "_ALL";
  } 

  TString target_inputFileName = "/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+add_string+".root";
  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add(target_inputFileName); 
  target_tree->AddFriend("ntp",target_inputFileName.ReplaceAll(".root","_SPlot.root"));
  target_tree->AddFriend("ntp",target_inputFileName.ReplaceAll("_SPlot.root","_rew.root"));

  
  vector <string> toweight_input_weights = {"N_S_sw"};
  if(isPGun) toweight_input_weights.clear();
  vector <string> target_input_weights = {"N_S_sw","Iw"};
  if(isPGun) target_input_weights = {"Iw"};

  size_t toweight_evts = min((size_t)toweight_tree->GetEntries(),(size_t)600000000);
  size_t target_evts = min((size_t)target_tree->GetEntries(),(size_t)400000000);
   
  IDecay* toweight_decay = new IDecay(toweight_tree,"D02KmKp","D^{0} #rightarrow K^{#minus} K^{+}",toweight_input_weights,toweight_evts,0);
  IDecay* target_decay = new IDecay(target_tree,"D02Kmpip","D^{0} #rightarrow K^{#minus} #pi^{+}",target_input_weights,target_evts,0);

  if(!rew_X) {
    toweight_decay->addTargetDistribution(target_decay,{"Dst_PT(100,2.28e3,9.5e3)","sPi_PT(100,100,8e2)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"Dst_PT(100,2.28e3,9.5e3)","Dst_ETA(100,2.4,4.3)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"Dst_PHI(100,-3.1416,3.1416)"},5);
    toweight_decay->addTargetDistribution(target_decay,{"sPi_PT(100,100,8e2)","sPi_ETA(100,2.4,4.3)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"sPi_PHI(100,-3.1416,3.1416)"},5);
  }
  else {
    if (year != "15")
      toweight_decay->addTargetDistribution(target_decay,{"D0_PT(40,2.08e3,9.08e3)","D0_ETA(40,2.4,4.3)","D0_PHI(40,-3.1416,3.1416)"},5);
    else
      toweight_decay->addTargetDistribution(target_decay,{"D0_PT(20,2.08e3,9.08e3)","D0_ETA(20,2.4,4.3)","D0_PHI(20,-3.1416,3.1416)"},5);
  }

  if (rew_X)
    add_string += "_X";

  toweight_decay->use_Iw_max = 0;
  toweight_decay->Iw_max = 35;
  toweight_decay->Iw_max_value = 35;
  toweight_decay->chi2_Ndof_max = 0.15;
  toweight_decay->outFileName = "/home/LHCB-T3/smaccoli/CPV_in_D02hh/reweight/D02KK_to_D02KpiW/weights/"+year+"_"+polarity+add_string;
  toweight_decay->ApplySmoothing = 0;
  toweight_decay->IWeight();

  
  return 0;
}
