#!/bin/bash
g++ -Wall -o _before_after before_after.cxx `root-config --cflags --glibs` `gsl-config --cflags --libs`

DIR=$PWD

for year in $(echo 15 16 17 18); do
    for pol in $(echo Dw Up); do
	#for year in $(echo 18); do
	#for pol in $(echo Dw); do
	for variable in $(echo Dst_PT__100__2280__9500__ Dst_ETA__100__2.4__4.3__ Dst_PHI__100__-3.1416__3.1416__ sPi_PT__100__100__800__ sPi_ETA__100__2.4__4.3__ sPi_PHI__100__-3.1416__3.1416__ Dst_PX__100__-9500__9500__ sPi_PX__100__-800__800__ Dst_PY__100__-9500__9500__ sPi_PY__100__-800__800__ Dst_PZ__100__25000__195000__ sPi_PZ__100__2000__19500__ X_PT__100__2080__9080__ X_ETA__100__2.4__4.3__ X_PHI__100__-3.1416__3.1416__ X_P__100__0__250000__ ); do
	    echo "#!/bin/sh" > exec/$year.$pol.$variable.sh
	    echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> exec/$year.$pol.$variable.sh 
	    echo "cd $DIR" >> exec/$year.$pol.$variable.sh
	    echo "hostname" >> exec/$year.$pol.$variable.sh
	    echo "time ./_before_after $year $pol $variable > logs/log_${year}_${pol}_${variable}.txt" >> exec/$year.$pol.$variable.sh
	    chmod +x exec/$year.$pol.$variable.sh
	    python condor_make_template.py condor_template.sub $year.$pol.$variable
	    condor_submit exec/condor_template.$year.$pol.$variable.sub -batch-name $year$pol$variable

	done		  	
    done
done



#Dst_PT__100__2280__9500__ Dst_ETA__100__2.4__4.3__ Dst_PHI__100__-3.1416__3.1416__ sPi_PT__100__100__800__ sPi_ETA__100__2.4__4.3__ sPi_PHI__100__-3.1416__3.1416__ Dst_PX__100__-9500__9500__ sPi_PX__100__-800__800__ Dst_PY__100__-9500__9500__ sPi_PY__100__-800__800__ Dst_PZ__100__25000__195000__ sPi_PZ__100__2000__19500__ X_PT__100__2080__9080__ X_ETA__100__2.4__4.3__ X_PHI__100__-3.1416__3.1416__ X_P__100__0__250000__
