void sPi_ETA_asym()
{
//=========Macro generated from canvas: sPi_ETA_asym/sPi_ETA_asym
//=========  (Mon Nov 18 21:05:00 2019) by ROOT version6.08/02
   TCanvas *sPi_ETA_asym = new TCanvas("sPi_ETA_asym", "sPi_ETA_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   sPi_ETA_asym->SetHighLightColor(2);
   sPi_ETA_asym->Range(0,0,1,1);
   sPi_ETA_asym->SetFillColor(0);
   sPi_ETA_asym->SetBorderMode(0);
   sPi_ETA_asym->SetBorderSize(10);
   sPi_ETA_asym->SetTickx(1);
   sPi_ETA_asym->SetTicky(1);
   sPi_ETA_asym->SetLeftMargin(0.18);
   sPi_ETA_asym->SetRightMargin(0.05);
   sPi_ETA_asym->SetTopMargin(0.07);
   sPi_ETA_asym->SetBottomMargin(0.16);
   sPi_ETA_asym->SetFrameFillStyle(0);
   sPi_ETA_asym->SetFrameLineStyle(0);
   sPi_ETA_asym->SetFrameLineColor(0);
   sPi_ETA_asym->SetFrameLineWidth(0);
   sPi_ETA_asym->SetFrameBorderMode(0);
   sPi_ETA_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(2.04375,-0.0005882971,4.41875,0.0002961798);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,2.4,4.3);
   h1D_target_minus_toweight__8->SetBinContent(1,-4.163303e-06);
   h1D_target_minus_toweight__8->SetBinContent(2,-4.823953e-06);
   h1D_target_minus_toweight__8->SetBinContent(3,-7.231763e-06);
   h1D_target_minus_toweight__8->SetBinContent(4,-7.037524e-06);
   h1D_target_minus_toweight__8->SetBinContent(5,-6.765738e-06);
   h1D_target_minus_toweight__8->SetBinContent(6,-8.165545e-06);
   h1D_target_minus_toweight__8->SetBinContent(7,-1.482014e-05);
   h1D_target_minus_toweight__8->SetBinContent(8,-1.508964e-05);
   h1D_target_minus_toweight__8->SetBinContent(9,-2.051162e-05);
   h1D_target_minus_toweight__8->SetBinContent(10,-2.521602e-05);
   h1D_target_minus_toweight__8->SetBinContent(11,4.754402e-06);
   h1D_target_minus_toweight__8->SetBinContent(12,1.672353e-05);
   h1D_target_minus_toweight__8->SetBinContent(13,-3.307511e-05);
   h1D_target_minus_toweight__8->SetBinContent(14,-1.967419e-07);
   h1D_target_minus_toweight__8->SetBinContent(15,1.172069e-05);
   h1D_target_minus_toweight__8->SetBinContent(16,-1.28292e-05);
   h1D_target_minus_toweight__8->SetBinContent(17,9.824755e-06);
   h1D_target_minus_toweight__8->SetBinContent(18,5.205628e-06);
   h1D_target_minus_toweight__8->SetBinContent(19,-1.969747e-05);
   h1D_target_minus_toweight__8->SetBinContent(20,-4.9375e-05);
   h1D_target_minus_toweight__8->SetBinContent(21,7.690489e-05);
   h1D_target_minus_toweight__8->SetBinContent(22,-4.195282e-05);
   h1D_target_minus_toweight__8->SetBinContent(23,-1.235632e-05);
   h1D_target_minus_toweight__8->SetBinContent(24,1.568859e-05);
   h1D_target_minus_toweight__8->SetBinContent(25,9.606592e-07);
   h1D_target_minus_toweight__8->SetBinContent(26,8.786097e-06);
   h1D_target_minus_toweight__8->SetBinContent(27,-4.381873e-05);
   h1D_target_minus_toweight__8->SetBinContent(28,-7.954426e-06);
   h1D_target_minus_toweight__8->SetBinContent(29,2.994202e-06);
   h1D_target_minus_toweight__8->SetBinContent(30,-9.423774e-05);
   h1D_target_minus_toweight__8->SetBinContent(31,0.0001377761);
   h1D_target_minus_toweight__8->SetBinContent(32,-0.0001214426);
   h1D_target_minus_toweight__8->SetBinContent(33,2.25082e-05);
   h1D_target_minus_toweight__8->SetBinContent(34,-1.197588e-05);
   h1D_target_minus_toweight__8->SetBinContent(35,-8.422229e-05);
   h1D_target_minus_toweight__8->SetBinContent(36,-4.207715e-06);
   h1D_target_minus_toweight__8->SetBinContent(37,9.902753e-05);
   h1D_target_minus_toweight__8->SetBinContent(38,0.000100187);
   h1D_target_minus_toweight__8->SetBinContent(39,-0.000161157);
   h1D_target_minus_toweight__8->SetBinContent(40,-0.0002096519);
   h1D_target_minus_toweight__8->SetBinContent(41,-2.936646e-05);
   h1D_target_minus_toweight__8->SetBinContent(42,-0.0001631789);
   h1D_target_minus_toweight__8->SetBinContent(43,5.305931e-05);
   h1D_target_minus_toweight__8->SetBinContent(44,9.13851e-05);
   h1D_target_minus_toweight__8->SetBinContent(45,4.615635e-05);
   h1D_target_minus_toweight__8->SetBinContent(46,6.732903e-05);
   h1D_target_minus_toweight__8->SetBinContent(47,-5.521625e-05);
   h1D_target_minus_toweight__8->SetBinContent(48,-0.0001025479);
   h1D_target_minus_toweight__8->SetBinContent(49,-3.053807e-05);
   h1D_target_minus_toweight__8->SetBinContent(50,-7.610954e-05);
   h1D_target_minus_toweight__8->SetBinContent(51,7.261336e-05);
   h1D_target_minus_toweight__8->SetBinContent(52,0.0001514386);
   h1D_target_minus_toweight__8->SetBinContent(53,-8.563884e-05);
   h1D_target_minus_toweight__8->SetBinContent(54,2.959929e-05);
   h1D_target_minus_toweight__8->SetBinContent(55,0.0002061576);
   h1D_target_minus_toweight__8->SetBinContent(56,-0.0002147611);
   h1D_target_minus_toweight__8->SetBinContent(57,-0.0001524165);
   h1D_target_minus_toweight__8->SetBinContent(58,-4.566647e-05);
   h1D_target_minus_toweight__8->SetBinContent(59,-0.0002948754);
   h1D_target_minus_toweight__8->SetBinContent(60,-0.0001434796);
   h1D_target_minus_toweight__8->SetBinContent(61,7.308833e-05);
   h1D_target_minus_toweight__8->SetBinContent(62,1.026131e-05);
   h1D_target_minus_toweight__8->SetBinContent(63,2.058223e-06);
   h1D_target_minus_toweight__8->SetBinContent(64,0.0001167078);
   h1D_target_minus_toweight__8->SetBinContent(65,-0.0001668092);
   h1D_target_minus_toweight__8->SetBinContent(66,0.0001953412);
   h1D_target_minus_toweight__8->SetBinContent(67,3.891438e-05);
   h1D_target_minus_toweight__8->SetBinContent(68,-0.0001877714);
   h1D_target_minus_toweight__8->SetBinContent(69,-0.0002753846);
   h1D_target_minus_toweight__8->SetBinContent(70,0.0001371894);
   h1D_target_minus_toweight__8->SetBinContent(71,4.257821e-05);
   h1D_target_minus_toweight__8->SetBinContent(72,0.0001734076);
   h1D_target_minus_toweight__8->SetBinContent(73,4.356354e-05);
   h1D_target_minus_toweight__8->SetBinContent(74,0.0002666973);
   h1D_target_minus_toweight__8->SetBinContent(75,0.000112053);
   h1D_target_minus_toweight__8->SetBinContent(76,-0.0001819981);
   h1D_target_minus_toweight__8->SetBinContent(77,4.710723e-05);
   h1D_target_minus_toweight__8->SetBinContent(78,5.750265e-05);
   h1D_target_minus_toweight__8->SetBinContent(79,7.283688e-05);
   h1D_target_minus_toweight__8->SetBinContent(80,0.0001305677);
   h1D_target_minus_toweight__8->SetBinContent(81,-0.0001468193);
   h1D_target_minus_toweight__8->SetBinContent(82,1.65184e-05);
   h1D_target_minus_toweight__8->SetBinContent(83,9.242631e-05);
   h1D_target_minus_toweight__8->SetBinContent(84,-3.675977e-05);
   h1D_target_minus_toweight__8->SetBinContent(85,-8.534733e-05);
   h1D_target_minus_toweight__8->SetBinContent(86,5.463324e-05);
   h1D_target_minus_toweight__8->SetBinContent(87,-1.342827e-05);
   h1D_target_minus_toweight__8->SetBinContent(88,0.0001078122);
   h1D_target_minus_toweight__8->SetBinContent(89,5.667866e-05);
   h1D_target_minus_toweight__8->SetBinContent(90,0.0001163997);
   h1D_target_minus_toweight__8->SetBinContent(91,-6.524846e-06);
   h1D_target_minus_toweight__8->SetBinContent(92,-9.192387e-06);
   h1D_target_minus_toweight__8->SetBinContent(93,2.142228e-05);
   h1D_target_minus_toweight__8->SetBinContent(94,2.641918e-05);
   h1D_target_minus_toweight__8->SetBinContent(95,3.593985e-05);
   h1D_target_minus_toweight__8->SetBinContent(96,8.84505e-05);
   h1D_target_minus_toweight__8->SetBinContent(97,3.526313e-05);
   h1D_target_minus_toweight__8->SetBinContent(98,9.460241e-05);
   h1D_target_minus_toweight__8->SetBinContent(99,3.935746e-05);
   h1D_target_minus_toweight__8->SetBinContent(100,-1.079339e-05);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("#eta (#pi_{soft}^{+})");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{0} #rightarrow K^{#minus} #pi^{+})#minusN(D^{0} #rightarrow K^{#minus} K^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",2.4,4.3);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   sPi_ETA_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(2.04375,-0.1566011,4.41875,0.1010812);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,2.4,4.3);
   h1D_asym__9->SetBinContent(1,-0.02011571);
   h1D_asym__9->SetBinContent(2,0.01387468);
   h1D_asym__9->SetBinContent(3,-0.006653785);
   h1D_asym__9->SetBinContent(4,-0.007764782);
   h1D_asym__9->SetBinContent(5,0.001994319);
   h1D_asym__9->SetBinContent(6,0.00208838);
   h1D_asym__9->SetBinContent(7,-0.001413256);
   h1D_asym__9->SetBinContent(8,-0.006180879);
   h1D_asym__9->SetBinContent(9,-0.005830099);
   h1D_asym__9->SetBinContent(10,0.009330004);
   h1D_asym__9->SetBinContent(11,0.002014645);
   h1D_asym__9->SetBinContent(12,0.005725485);
   h1D_asym__9->SetBinContent(13,0.003338944);
   h1D_asym__9->SetBinContent(14,0.000888808);
   h1D_asym__9->SetBinContent(15,-0.005112202);
   h1D_asym__9->SetBinContent(16,0.003912399);
   h1D_asym__9->SetBinContent(17,0.001452943);
   h1D_asym__9->SetBinContent(18,-9.120425e-05);
   h1D_asym__9->SetBinContent(19,0.007242953);
   h1D_asym__9->SetBinContent(20,0.003635569);
   h1D_asym__9->SetBinContent(21,0.007102098);
   h1D_asym__9->SetBinContent(22,0.001211701);
   h1D_asym__9->SetBinContent(23,0.003665658);
   h1D_asym__9->SetBinContent(24,0.01097194);
   h1D_asym__9->SetBinContent(25,-0.0007450036);
   h1D_asym__9->SetBinContent(26,0.003169401);
   h1D_asym__9->SetBinContent(27,0.01199024);
   h1D_asym__9->SetBinContent(28,0.0004865895);
   h1D_asym__9->SetBinContent(29,0.007447832);
   h1D_asym__9->SetBinContent(30,0.009958936);
   h1D_asym__9->SetBinContent(31,0.008578425);
   h1D_asym__9->SetBinContent(32,0.004538241);
   h1D_asym__9->SetBinContent(33,0.006210926);
   h1D_asym__9->SetBinContent(34,0.003320279);
   h1D_asym__9->SetBinContent(35,0.009118527);
   h1D_asym__9->SetBinContent(36,0.007522357);
   h1D_asym__9->SetBinContent(37,0.007102085);
   h1D_asym__9->SetBinContent(38,0.004117392);
   h1D_asym__9->SetBinContent(39,0.007443164);
   h1D_asym__9->SetBinContent(40,0.006094371);
   h1D_asym__9->SetBinContent(41,0.00526709);
   h1D_asym__9->SetBinContent(42,0.008511757);
   h1D_asym__9->SetBinContent(43,0.004989952);
   h1D_asym__9->SetBinContent(44,0.003749341);
   h1D_asym__9->SetBinContent(45,0.01075503);
   h1D_asym__9->SetBinContent(46,0.005373402);
   h1D_asym__9->SetBinContent(47,0.005966714);
   h1D_asym__9->SetBinContent(48,0.005384029);
   h1D_asym__9->SetBinContent(49,-0.001711866);
   h1D_asym__9->SetBinContent(50,0.005793412);
   h1D_asym__9->SetBinContent(51,0.007487926);
   h1D_asym__9->SetBinContent(52,0.005275641);
   h1D_asym__9->SetBinContent(53,0.01268263);
   h1D_asym__9->SetBinContent(54,0.005269466);
   h1D_asym__9->SetBinContent(55,-0.00268843);
   h1D_asym__9->SetBinContent(56,0.004117662);
   h1D_asym__9->SetBinContent(57,0.001069644);
   h1D_asym__9->SetBinContent(58,0.005120639);
   h1D_asym__9->SetBinContent(59,-0.0055397);
   h1D_asym__9->SetBinContent(60,0.003919091);
   h1D_asym__9->SetBinContent(61,0.004554786);
   h1D_asym__9->SetBinContent(62,-0.0005125734);
   h1D_asym__9->SetBinContent(63,-0.001446454);
   h1D_asym__9->SetBinContent(64,0.0005214994);
   h1D_asym__9->SetBinContent(65,-0.006827463);
   h1D_asym__9->SetBinContent(66,0.006194308);
   h1D_asym__9->SetBinContent(67,0.002659317);
   h1D_asym__9->SetBinContent(68,-0.004464848);
   h1D_asym__9->SetBinContent(69,-0.005833037);
   h1D_asym__9->SetBinContent(70,0.0003681183);
   h1D_asym__9->SetBinContent(71,-0.007309027);
   h1D_asym__9->SetBinContent(72,0.0001386125);
   h1D_asym__9->SetBinContent(73,-0.02456971);
   h1D_asym__9->SetBinContent(74,0.0140533);
   h1D_asym__9->SetBinContent(75,0.006783426);
   h1D_asym__9->SetBinContent(76,-0.00909269);
   h1D_asym__9->SetBinContent(77,-0.001825287);
   h1D_asym__9->SetBinContent(78,-0.01182149);
   h1D_asym__9->SetBinContent(79,-0.01313757);
   h1D_asym__9->SetBinContent(80,-0.0156673);
   h1D_asym__9->SetBinContent(81,0.002688103);
   h1D_asym__9->SetBinContent(82,0.003288332);
   h1D_asym__9->SetBinContent(83,-0.01285814);
   h1D_asym__9->SetBinContent(84,0.008592692);
   h1D_asym__9->SetBinContent(85,-0.008058784);
   h1D_asym__9->SetBinContent(86,0.007353677);
   h1D_asym__9->SetBinContent(87,0.001517479);
   h1D_asym__9->SetBinContent(88,-0.02307256);
   h1D_asym__9->SetBinContent(89,-0.01622405);
   h1D_asym__9->SetBinContent(90,-0.00751276);
   h1D_asym__9->SetBinContent(91,-0.008387819);
   h1D_asym__9->SetBinContent(92,-0.01110465);
   h1D_asym__9->SetBinContent(93,-0.006712959);
   h1D_asym__9->SetBinContent(94,-0.02685584);
   h1D_asym__9->SetBinContent(95,0.004301431);
   h1D_asym__9->SetBinContent(96,-0.07991047);
   h1D_asym__9->SetBinContent(97,-0.008171448);
   h1D_asym__9->SetBinContent(98,0.0001592287);
   h1D_asym__9->SetBinContent(99,-0.04584112);
   h1D_asym__9->SetBinContent(100,-0.07744726);
   h1D_asym__9->SetBinError(1,0.0377611);
   h1D_asym__9->SetBinError(2,0.03241186);
   h1D_asym__9->SetBinError(3,0.02744148);
   h1D_asym__9->SetBinError(4,0.02518317);
   h1D_asym__9->SetBinError(5,0.02359619);
   h1D_asym__9->SetBinError(6,0.02493106);
   h1D_asym__9->SetBinError(7,0.02122363);
   h1D_asym__9->SetBinError(8,0.01900478);
   h1D_asym__9->SetBinError(9,0.01862227);
   h1D_asym__9->SetBinError(10,0.0211179);
   h1D_asym__9->SetBinError(11,0.01700109);
   h1D_asym__9->SetBinError(12,0.01770356);
   h1D_asym__9->SetBinError(13,0.01561466);
   h1D_asym__9->SetBinError(14,0.01517501);
   h1D_asym__9->SetBinError(15,0.01471309);
   h1D_asym__9->SetBinError(16,0.01453352);
   h1D_asym__9->SetBinError(17,0.01402113);
   h1D_asym__9->SetBinError(18,0.01364866);
   h1D_asym__9->SetBinError(19,0.01353183);
   h1D_asym__9->SetBinError(20,0.01324475);
   h1D_asym__9->SetBinError(21,0.01311193);
   h1D_asym__9->SetBinError(22,0.01277525);
   h1D_asym__9->SetBinError(23,0.01271293);
   h1D_asym__9->SetBinError(24,0.01268029);
   h1D_asym__9->SetBinError(25,0.01226668);
   h1D_asym__9->SetBinError(26,0.01221739);
   h1D_asym__9->SetBinError(27,0.01224885);
   h1D_asym__9->SetBinError(28,0.01196468);
   h1D_asym__9->SetBinError(29,0.01201626);
   h1D_asym__9->SetBinError(30,0.01194752);
   h1D_asym__9->SetBinError(31,0.01205513);
   h1D_asym__9->SetBinError(32,0.01176419);
   h1D_asym__9->SetBinError(33,0.0118728);
   h1D_asym__9->SetBinError(34,0.0117945);
   h1D_asym__9->SetBinError(35,0.01190888);
   h1D_asym__9->SetBinError(36,0.01193624);
   h1D_asym__9->SetBinError(37,0.01197881);
   h1D_asym__9->SetBinError(38,0.01196089);
   h1D_asym__9->SetBinError(39,0.01206893);
   h1D_asym__9->SetBinError(40,0.01210542);
   h1D_asym__9->SetBinError(41,0.01215488);
   h1D_asym__9->SetBinError(42,0.01229754);
   h1D_asym__9->SetBinError(43,0.01229499);
   h1D_asym__9->SetBinError(44,0.01238187);
   h1D_asym__9->SetBinError(45,0.01262007);
   h1D_asym__9->SetBinError(46,0.01255236);
   h1D_asym__9->SetBinError(47,0.0126547);
   h1D_asym__9->SetBinError(48,0.01280406);
   h1D_asym__9->SetBinError(49,0.01275915);
   h1D_asym__9->SetBinError(50,0.01296581);
   h1D_asym__9->SetBinError(51,0.01308928);
   h1D_asym__9->SetBinError(52,0.01316095);
   h1D_asym__9->SetBinError(53,0.01338107);
   h1D_asym__9->SetBinError(54,0.01340979);
   h1D_asym__9->SetBinError(55,0.01333087);
   h1D_asym__9->SetBinError(56,0.01353243);
   h1D_asym__9->SetBinError(57,0.01356536);
   h1D_asym__9->SetBinError(58,0.01388116);
   h1D_asym__9->SetBinError(59,0.0136909);
   h1D_asym__9->SetBinError(60,0.01408069);
   h1D_asym__9->SetBinError(61,0.01429719);
   h1D_asym__9->SetBinError(62,0.01434131);
   h1D_asym__9->SetBinError(63,0.01450759);
   h1D_asym__9->SetBinError(64,0.01480919);
   h1D_asym__9->SetBinError(65,0.01480015);
   h1D_asym__9->SetBinError(66,0.0154629);
   h1D_asym__9->SetBinError(67,0.01559605);
   h1D_asym__9->SetBinError(68,0.01580628);
   h1D_asym__9->SetBinError(69,0.01608434);
   h1D_asym__9->SetBinError(70,0.01670244);
   h1D_asym__9->SetBinError(71,0.01680736);
   h1D_asym__9->SetBinError(72,0.01745483);
   h1D_asym__9->SetBinError(73,0.01726301);
   h1D_asym__9->SetBinError(74,0.01886366);
   h1D_asym__9->SetBinError(75,0.01925918);
   h1D_asym__9->SetBinError(76,0.01903251);
   h1D_asym__9->SetBinError(77,0.02003706);
   h1D_asym__9->SetBinError(78,0.02039737);
   h1D_asym__9->SetBinError(79,0.02101636);
   h1D_asym__9->SetBinError(80,0.02200324);
   h1D_asym__9->SetBinError(81,0.02348535);
   h1D_asym__9->SetBinError(82,0.02485146);
   h1D_asym__9->SetBinError(83,0.02540704);
   h1D_asym__9->SetBinError(84,0.0273476);
   h1D_asym__9->SetBinError(85,0.02726162);
   h1D_asym__9->SetBinError(86,0.02975898);
   h1D_asym__9->SetBinError(87,0.03020268);
   h1D_asym__9->SetBinError(88,0.03050328);
   h1D_asym__9->SetBinError(89,0.03206608);
   h1D_asym__9->SetBinError(90,0.03413877);
   h1D_asym__9->SetBinError(91,0.03599653);
   h1D_asym__9->SetBinError(92,0.03792774);
   h1D_asym__9->SetBinError(93,0.04102559);
   h1D_asym__9->SetBinError(94,0.04241871);
   h1D_asym__9->SetBinError(95,0.05020681);
   h1D_asym__9->SetBinError(96,0.0480312);
   h1D_asym__9->SetBinError(97,0.0564605);
   h1D_asym__9->SetBinError(98,0.06411021);
   h1D_asym__9->SetBinError(99,0.06523725);
   h1D_asym__9->SetBinError(100,0.06863624);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("#eta (#pi_{soft}^{+})");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",2.4,4.3);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","0.001807806530",2.4,4.3);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   sPi_ETA_asym->cd();
   sPi_ETA_asym->Modified();
   sPi_ETA_asym->cd();
   sPi_ETA_asym->SetSelected(sPi_ETA_asym);
}
