void X_ETA_asym()
{
//=========Macro generated from canvas: X_ETA_asym/X_ETA_asym
//=========  (Mon Nov 18 21:11:19 2019) by ROOT version6.08/02
   TCanvas *X_ETA_asym = new TCanvas("X_ETA_asym", "X_ETA_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   X_ETA_asym->SetHighLightColor(2);
   X_ETA_asym->Range(0,0,1,1);
   X_ETA_asym->SetFillColor(0);
   X_ETA_asym->SetBorderMode(0);
   X_ETA_asym->SetBorderSize(10);
   X_ETA_asym->SetTickx(1);
   X_ETA_asym->SetTicky(1);
   X_ETA_asym->SetLeftMargin(0.18);
   X_ETA_asym->SetRightMargin(0.05);
   X_ETA_asym->SetTopMargin(0.07);
   X_ETA_asym->SetBottomMargin(0.16);
   X_ETA_asym->SetFrameFillStyle(0);
   X_ETA_asym->SetFrameLineStyle(0);
   X_ETA_asym->SetFrameLineColor(0);
   X_ETA_asym->SetFrameLineWidth(0);
   X_ETA_asym->SetFrameBorderMode(0);
   X_ETA_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(2.04375,-0.0007457314,4.41875,0.0004234453);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,2.4,4.3);
   h1D_target_minus_toweight__8->SetBinContent(1,-7.151961e-06);
   h1D_target_minus_toweight__8->SetBinContent(2,9.538126e-07);
   h1D_target_minus_toweight__8->SetBinContent(3,-5.113245e-06);
   h1D_target_minus_toweight__8->SetBinContent(4,1.636855e-05);
   h1D_target_minus_toweight__8->SetBinContent(5,-6.112678e-06);
   h1D_target_minus_toweight__8->SetBinContent(6,-6.085463e-05);
   h1D_target_minus_toweight__8->SetBinContent(7,1.165611e-05);
   h1D_target_minus_toweight__8->SetBinContent(8,-1.47149e-05);
   h1D_target_minus_toweight__8->SetBinContent(9,-3.53097e-05);
   h1D_target_minus_toweight__8->SetBinContent(10,9.228999e-05);
   h1D_target_minus_toweight__8->SetBinContent(11,-0.000126429);
   h1D_target_minus_toweight__8->SetBinContent(12,3.240875e-05);
   h1D_target_minus_toweight__8->SetBinContent(13,-1.027843e-05);
   h1D_target_minus_toweight__8->SetBinContent(14,-4.456192e-05);
   h1D_target_minus_toweight__8->SetBinContent(15,0.0001260182);
   h1D_target_minus_toweight__8->SetBinContent(16,-0.0001358287);
   h1D_target_minus_toweight__8->SetBinContent(17,-3.981637e-06);
   h1D_target_minus_toweight__8->SetBinContent(18,-8.645235e-06);
   h1D_target_minus_toweight__8->SetBinContent(19,-2.631452e-06);
   h1D_target_minus_toweight__8->SetBinContent(20,0.0001060078);
   h1D_target_minus_toweight__8->SetBinContent(21,-0.0001287409);
   h1D_target_minus_toweight__8->SetBinContent(22,3.126031e-05);
   h1D_target_minus_toweight__8->SetBinContent(23,8.539762e-06);
   h1D_target_minus_toweight__8->SetBinContent(24,-1.294771e-05);
   h1D_target_minus_toweight__8->SetBinContent(25,2.976833e-05);
   h1D_target_minus_toweight__8->SetBinContent(26,7.325783e-06);
   h1D_target_minus_toweight__8->SetBinContent(27,-4.49745e-05);
   h1D_target_minus_toweight__8->SetBinContent(28,1.608953e-05);
   h1D_target_minus_toweight__8->SetBinContent(29,-0.0001348564);
   h1D_target_minus_toweight__8->SetBinContent(30,5.350076e-05);
   h1D_target_minus_toweight__8->SetBinContent(31,-2.535153e-05);
   h1D_target_minus_toweight__8->SetBinContent(32,-6.776676e-05);
   h1D_target_minus_toweight__8->SetBinContent(33,1.847558e-05);
   h1D_target_minus_toweight__8->SetBinContent(34,-9.339675e-05);
   h1D_target_minus_toweight__8->SetBinContent(35,3.476348e-05);
   h1D_target_minus_toweight__8->SetBinContent(36,1.201406e-06);
   h1D_target_minus_toweight__8->SetBinContent(37,-2.644584e-05);
   h1D_target_minus_toweight__8->SetBinContent(38,-1.061056e-05);
   h1D_target_minus_toweight__8->SetBinContent(39,-0.0002122242);
   h1D_target_minus_toweight__8->SetBinContent(40,8.751638e-05);
   h1D_target_minus_toweight__8->SetBinContent(41,-2.728403e-05);
   h1D_target_minus_toweight__8->SetBinContent(42,-0.0001371335);
   h1D_target_minus_toweight__8->SetBinContent(43,3.339536e-05);
   h1D_target_minus_toweight__8->SetBinContent(44,-7.73631e-05);
   h1D_target_minus_toweight__8->SetBinContent(45,1.400709e-05);
   h1D_target_minus_toweight__8->SetBinContent(46,-0.0001668185);
   h1D_target_minus_toweight__8->SetBinContent(47,-2.833083e-05);
   h1D_target_minus_toweight__8->SetBinContent(48,-3.480166e-05);
   h1D_target_minus_toweight__8->SetBinContent(49,-8.132495e-05);
   h1D_target_minus_toweight__8->SetBinContent(50,8.852594e-05);
   h1D_target_minus_toweight__8->SetBinContent(51,-0.0002340712);
   h1D_target_minus_toweight__8->SetBinContent(52,-4.998408e-05);
   h1D_target_minus_toweight__8->SetBinContent(53,0.0001390986);
   h1D_target_minus_toweight__8->SetBinContent(54,-2.644956e-05);
   h1D_target_minus_toweight__8->SetBinContent(55,0.0001657996);
   h1D_target_minus_toweight__8->SetBinContent(56,-0.000120813);
   h1D_target_minus_toweight__8->SetBinContent(57,0.0001325682);
   h1D_target_minus_toweight__8->SetBinContent(58,-0.000124611);
   h1D_target_minus_toweight__8->SetBinContent(59,3.003143e-05);
   h1D_target_minus_toweight__8->SetBinContent(60,0.0001502782);
   h1D_target_minus_toweight__8->SetBinContent(61,-0.0003381576);
   h1D_target_minus_toweight__8->SetBinContent(62,0.000178948);
   h1D_target_minus_toweight__8->SetBinContent(63,4.449859e-05);
   h1D_target_minus_toweight__8->SetBinContent(64,0.0002516676);
   h1D_target_minus_toweight__8->SetBinContent(65,-2.242811e-05);
   h1D_target_minus_toweight__8->SetBinContent(66,-1.760386e-05);
   h1D_target_minus_toweight__8->SetBinContent(67,5.801208e-05);
   h1D_target_minus_toweight__8->SetBinContent(68,0.000179423);
   h1D_target_minus_toweight__8->SetBinContent(69,5.524233e-05);
   h1D_target_minus_toweight__8->SetBinContent(70,-0.0001197662);
   h1D_target_minus_toweight__8->SetBinContent(71,0.0002205512);
   h1D_target_minus_toweight__8->SetBinContent(72,-0.0001753503);
   h1D_target_minus_toweight__8->SetBinContent(73,0.000146755);
   h1D_target_minus_toweight__8->SetBinContent(74,0.0001513064);
   h1D_target_minus_toweight__8->SetBinContent(75,-0.0001575025);
   h1D_target_minus_toweight__8->SetBinContent(76,0.0003518835);
   h1D_target_minus_toweight__8->SetBinContent(77,-2.303533e-05);
   h1D_target_minus_toweight__8->SetBinContent(78,-1.358334e-05);
   h1D_target_minus_toweight__8->SetBinContent(79,0.000193255);
   h1D_target_minus_toweight__8->SetBinContent(80,-0.0003578616);
   h1D_target_minus_toweight__8->SetBinContent(81,0.0003844728);
   h1D_target_minus_toweight__8->SetBinContent(82,-7.783528e-05);
   h1D_target_minus_toweight__8->SetBinContent(83,-3.116671e-05);
   h1D_target_minus_toweight__8->SetBinContent(84,3.213389e-05);
   h1D_target_minus_toweight__8->SetBinContent(85,-0.0001967293);
   h1D_target_minus_toweight__8->SetBinContent(86,0.0002259575);
   h1D_target_minus_toweight__8->SetBinContent(87,7.398892e-06);
   h1D_target_minus_toweight__8->SetBinContent(88,-5.639833e-05);
   h1D_target_minus_toweight__8->SetBinContent(89,8.84363e-05);
   h1D_target_minus_toweight__8->SetBinContent(90,-0.0001772447);
   h1D_target_minus_toweight__8->SetBinContent(91,0.0002059416);
   h1D_target_minus_toweight__8->SetBinContent(92,-6.968912e-05);
   h1D_target_minus_toweight__8->SetBinContent(93,-3.669411e-07);
   h1D_target_minus_toweight__8->SetBinContent(94,3.43672e-05);
   h1D_target_minus_toweight__8->SetBinContent(95,-0.0001035085);
   h1D_target_minus_toweight__8->SetBinContent(96,6.911595e-05);
   h1D_target_minus_toweight__8->SetBinContent(97,-1.869904e-05);
   h1D_target_minus_toweight__8->SetBinContent(98,-7.157767e-06);
   h1D_target_minus_toweight__8->SetBinContent(99,1.298773e-05);
   h1D_target_minus_toweight__8->SetBinContent(100,-2.821619e-05);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("#eta (X)");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{0} #rightarrow K^{#minus} #pi^{+})#minusN(D^{0} #rightarrow K^{#minus} K^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",2.4,4.3);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   X_ETA_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(2.04375,-0.7363595,4.41875,0.6617411);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,2.4,4.3);
   h1D_asym__9->SetBinContent(1,-0.007817324);
   h1D_asym__9->SetBinContent(2,-0.01047066);
   h1D_asym__9->SetBinContent(3,-0.09537334);
   h1D_asym__9->SetBinContent(4,0.1175472);
   h1D_asym__9->SetBinContent(5,-0.2376506);
   h1D_asym__9->SetBinContent(6,-0.02068708);
   h1D_asym__9->SetBinContent(7,-0.006827721);
   h1D_asym__9->SetBinContent(8,-0.007162801);
   h1D_asym__9->SetBinContent(9,-0.01509243);
   h1D_asym__9->SetBinContent(10,-0.01242625);
   h1D_asym__9->SetBinContent(11,-0.004399915);
   h1D_asym__9->SetBinContent(12,-0.01171311);
   h1D_asym__9->SetBinContent(13,-0.01820035);
   h1D_asym__9->SetBinContent(14,-0.01576852);
   h1D_asym__9->SetBinContent(15,-0.01588147);
   h1D_asym__9->SetBinContent(16,-0.01791995);
   h1D_asym__9->SetBinContent(17,-0.02418821);
   h1D_asym__9->SetBinContent(18,-0.01398362);
   h1D_asym__9->SetBinContent(19,-0.02511689);
   h1D_asym__9->SetBinContent(20,-0.02451855);
   h1D_asym__9->SetBinContent(21,-0.01772905);
   h1D_asym__9->SetBinContent(22,-0.01783558);
   h1D_asym__9->SetBinContent(23,-0.01085881);
   h1D_asym__9->SetBinContent(24,-0.02261293);
   h1D_asym__9->SetBinContent(25,-0.01161389);
   h1D_asym__9->SetBinContent(26,-0.01839296);
   h1D_asym__9->SetBinContent(27,-0.02071811);
   h1D_asym__9->SetBinContent(28,-0.01846352);
   h1D_asym__9->SetBinContent(29,-0.02020721);
   h1D_asym__9->SetBinContent(30,-0.0187357);
   h1D_asym__9->SetBinContent(31,-0.01822738);
   h1D_asym__9->SetBinContent(32,-0.01759834);
   h1D_asym__9->SetBinContent(33,-0.009582818);
   h1D_asym__9->SetBinContent(34,-0.0193611);
   h1D_asym__9->SetBinContent(35,-0.02083076);
   h1D_asym__9->SetBinContent(36,-0.009181878);
   h1D_asym__9->SetBinContent(37,-0.0137193);
   h1D_asym__9->SetBinContent(38,-0.01980506);
   h1D_asym__9->SetBinContent(39,-0.01407126);
   h1D_asym__9->SetBinContent(40,-0.01304461);
   h1D_asym__9->SetBinContent(41,-0.02219381);
   h1D_asym__9->SetBinContent(42,-0.01712477);
   h1D_asym__9->SetBinContent(43,-0.01526256);
   h1D_asym__9->SetBinContent(44,-0.02507617);
   h1D_asym__9->SetBinContent(45,-0.02676158);
   h1D_asym__9->SetBinContent(46,-0.008485016);
   h1D_asym__9->SetBinContent(47,-0.01278477);
   h1D_asym__9->SetBinContent(48,-0.01788178);
   h1D_asym__9->SetBinContent(49,-0.01814705);
   h1D_asym__9->SetBinContent(50,-0.01795657);
   h1D_asym__9->SetBinContent(51,-0.01737411);
   h1D_asym__9->SetBinContent(52,-0.02117991);
   h1D_asym__9->SetBinContent(53,-0.01835508);
   h1D_asym__9->SetBinContent(54,-0.007361964);
   h1D_asym__9->SetBinContent(55,-0.01886926);
   h1D_asym__9->SetBinContent(56,-0.006373686);
   h1D_asym__9->SetBinContent(57,-0.01491192);
   h1D_asym__9->SetBinContent(58,-0.01550528);
   h1D_asym__9->SetBinContent(59,-0.02264739);
   h1D_asym__9->SetBinContent(60,-0.02270042);
   h1D_asym__9->SetBinContent(61,-0.009846945);
   h1D_asym__9->SetBinContent(62,-0.0119394);
   h1D_asym__9->SetBinContent(63,-0.01551274);
   h1D_asym__9->SetBinContent(64,-0.0172209);
   h1D_asym__9->SetBinContent(65,-0.005694649);
   h1D_asym__9->SetBinContent(66,-0.02131637);
   h1D_asym__9->SetBinContent(67,-0.02197628);
   h1D_asym__9->SetBinContent(68,-0.02205168);
   h1D_asym__9->SetBinContent(69,-0.01178946);
   h1D_asym__9->SetBinContent(70,-0.02461107);
   h1D_asym__9->SetBinContent(71,-0.008492623);
   h1D_asym__9->SetBinContent(72,-0.01648279);
   h1D_asym__9->SetBinContent(73,-0.0140897);
   h1D_asym__9->SetBinContent(74,-0.02805698);
   h1D_asym__9->SetBinContent(75,-0.01992975);
   h1D_asym__9->SetBinContent(76,-0.01297069);
   h1D_asym__9->SetBinContent(77,-0.0107081);
   h1D_asym__9->SetBinContent(78,-0.01222681);
   h1D_asym__9->SetBinContent(79,-0.02101041);
   h1D_asym__9->SetBinContent(80,-0.01406591);
   h1D_asym__9->SetBinContent(81,-0.01602493);
   h1D_asym__9->SetBinContent(82,-0.009821251);
   h1D_asym__9->SetBinContent(83,-0.003361206);
   h1D_asym__9->SetBinContent(84,0.0008433041);
   h1D_asym__9->SetBinContent(85,-0.009495228);
   h1D_asym__9->SetBinContent(86,-0.02525023);
   h1D_asym__9->SetBinContent(87,-0.02552882);
   h1D_asym__9->SetBinContent(88,-0.01650194);
   h1D_asym__9->SetBinContent(89,-0.01439553);
   h1D_asym__9->SetBinContent(90,-0.004762318);
   h1D_asym__9->SetBinContent(91,-0.01855908);
   h1D_asym__9->SetBinContent(92,-0.008453459);
   h1D_asym__9->SetBinContent(93,-0.01557956);
   h1D_asym__9->SetBinContent(94,-0.02457606);
   h1D_asym__9->SetBinContent(95,0.02868599);
   h1D_asym__9->SetBinContent(96,-0.05405772);
   h1D_asym__9->SetBinContent(97,-0.0686187);
   h1D_asym__9->SetBinContent(98,0.01821957);
   h1D_asym__9->SetBinContent(99,-0.01422252);
   h1D_asym__9->SetBinContent(100,0.01961483);
   h1D_asym__9->SetBinError(1,0.02930627);
   h1D_asym__9->SetBinError(2,0.02554083);
   h1D_asym__9->SetBinError(3,0.2977399);
   h1D_asym__9->SetBinError(4,0.3444652);
   h1D_asym__9->SetBinError(5,0.4416437);
   h1D_asym__9->SetBinError(6,0.01661747);
   h1D_asym__9->SetBinError(7,0.01653463);
   h1D_asym__9->SetBinError(8,0.01636283);
   h1D_asym__9->SetBinError(9,0.01514271);
   h1D_asym__9->SetBinError(10,0.01476729);
   h1D_asym__9->SetBinError(11,0.01470594);
   h1D_asym__9->SetBinError(12,0.01410306);
   h1D_asym__9->SetBinError(13,0.01387215);
   h1D_asym__9->SetBinError(14,0.01351831);
   h1D_asym__9->SetBinError(15,0.01318854);
   h1D_asym__9->SetBinError(16,0.01306776);
   h1D_asym__9->SetBinError(17,0.01270213);
   h1D_asym__9->SetBinError(18,0.01271089);
   h1D_asym__9->SetBinError(19,0.01230149);
   h1D_asym__9->SetBinError(20,0.01212414);
   h1D_asym__9->SetBinError(21,0.01210321);
   h1D_asym__9->SetBinError(22,0.01188848);
   h1D_asym__9->SetBinError(23,0.01193445);
   h1D_asym__9->SetBinError(24,0.01163372);
   h1D_asym__9->SetBinError(25,0.01156451);
   h1D_asym__9->SetBinError(26,0.0115172);
   h1D_asym__9->SetBinError(27,0.01130112);
   h1D_asym__9->SetBinError(28,0.01125598);
   h1D_asym__9->SetBinError(29,0.01105325);
   h1D_asym__9->SetBinError(30,0.01099101);
   h1D_asym__9->SetBinError(31,0.01101604);
   h1D_asym__9->SetBinError(32,0.0108564);
   h1D_asym__9->SetBinError(33,0.01103084);
   h1D_asym__9->SetBinError(34,0.0107821);
   h1D_asym__9->SetBinError(35,0.01068522);
   h1D_asym__9->SetBinError(36,0.01104396);
   h1D_asym__9->SetBinError(37,0.01094374);
   h1D_asym__9->SetBinError(38,0.01086791);
   h1D_asym__9->SetBinError(39,0.01097069);
   h1D_asym__9->SetBinError(40,0.01102891);
   h1D_asym__9->SetBinError(41,0.01123346);
   h1D_asym__9->SetBinError(42,0.01135394);
   h1D_asym__9->SetBinError(43,0.0115862);
   h1D_asym__9->SetBinError(44,0.0116482);
   h1D_asym__9->SetBinError(45,0.01165911);
   h1D_asym__9->SetBinError(46,0.01253093);
   h1D_asym__9->SetBinError(47,0.01251753);
   h1D_asym__9->SetBinError(48,0.01283321);
   h1D_asym__9->SetBinError(49,0.01293363);
   h1D_asym__9->SetBinError(50,0.01308734);
   h1D_asym__9->SetBinError(51,0.01369019);
   h1D_asym__9->SetBinError(52,0.01365182);
   h1D_asym__9->SetBinError(53,0.01399936);
   h1D_asym__9->SetBinError(54,0.01450967);
   h1D_asym__9->SetBinError(55,0.01429833);
   h1D_asym__9->SetBinError(56,0.01517696);
   h1D_asym__9->SetBinError(57,0.01510781);
   h1D_asym__9->SetBinError(58,0.01509692);
   h1D_asym__9->SetBinError(59,0.01506248);
   h1D_asym__9->SetBinError(60,0.01518402);
   h1D_asym__9->SetBinError(61,0.01617276);
   h1D_asym__9->SetBinError(62,0.01624681);
   h1D_asym__9->SetBinError(63,0.01634425);
   h1D_asym__9->SetBinError(64,0.01667433);
   h1D_asym__9->SetBinError(65,0.0171245);
   h1D_asym__9->SetBinError(66,0.01728651);
   h1D_asym__9->SetBinError(67,0.01743387);
   h1D_asym__9->SetBinError(68,0.01783833);
   h1D_asym__9->SetBinError(69,0.01867899);
   h1D_asym__9->SetBinError(70,0.01848275);
   h1D_asym__9->SetBinError(71,0.01975987);
   h1D_asym__9->SetBinError(72,0.02007197);
   h1D_asym__9->SetBinError(73,0.02065469);
   h1D_asym__9->SetBinError(74,0.02036876);
   h1D_asym__9->SetBinError(75,0.02095181);
   h1D_asym__9->SetBinError(76,0.02285386);
   h1D_asym__9->SetBinError(77,0.02346887);
   h1D_asym__9->SetBinError(78,0.02379725);
   h1D_asym__9->SetBinError(79,0.02421906);
   h1D_asym__9->SetBinError(80,0.02511285);
   h1D_asym__9->SetBinError(81,0.02648414);
   h1D_asym__9->SetBinError(82,0.02695302);
   h1D_asym__9->SetBinError(83,0.02897229);
   h1D_asym__9->SetBinError(84,0.02961843);
   h1D_asym__9->SetBinError(85,0.03045273);
   h1D_asym__9->SetBinError(86,0.03207776);
   h1D_asym__9->SetBinError(87,0.03286616);
   h1D_asym__9->SetBinError(88,0.03482395);
   h1D_asym__9->SetBinError(89,0.03717906);
   h1D_asym__9->SetBinError(90,0.0382622);
   h1D_asym__9->SetBinError(91,0.04072196);
   h1D_asym__9->SetBinError(92,0.04298821);
   h1D_asym__9->SetBinError(93,0.04633362);
   h1D_asym__9->SetBinError(94,0.04863727);
   h1D_asym__9->SetBinError(95,0.05608436);
   h1D_asym__9->SetBinError(96,0.05446704);
   h1D_asym__9->SetBinError(97,0.05628446);
   h1D_asym__9->SetBinError(98,0.07451937);
   h1D_asym__9->SetBinError(99,0.07587259);
   h1D_asym__9->SetBinError(100,0.08430763);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("#eta (X)");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",2.4,4.3);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","-0.016419355883",2.4,4.3);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   X_ETA_asym->cd();
   X_ETA_asym->Modified();
   X_ETA_asym->cd();
   X_ETA_asym->SetSelected(X_ETA_asym);
}
