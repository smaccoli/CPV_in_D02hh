void X_PHI_asym()
{
//=========Macro generated from canvas: X_PHI_asym/X_PHI_asym
//=========  (Mon Nov 18 20:49:37 2019) by ROOT version6.08/02
   TCanvas *X_PHI_asym = new TCanvas("X_PHI_asym", "X_PHI_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   X_PHI_asym->SetHighLightColor(2);
   X_PHI_asym->Range(0,0,1,1);
   X_PHI_asym->SetFillColor(0);
   X_PHI_asym->SetBorderMode(0);
   X_PHI_asym->SetBorderSize(10);
   X_PHI_asym->SetTickx(1);
   X_PHI_asym->SetTicky(1);
   X_PHI_asym->SetLeftMargin(0.18);
   X_PHI_asym->SetRightMargin(0.05);
   X_PHI_asym->SetTopMargin(0.07);
   X_PHI_asym->SetBottomMargin(0.16);
   X_PHI_asym->SetFrameFillStyle(0);
   X_PHI_asym->SetFrameLineStyle(0);
   X_PHI_asym->SetFrameLineColor(0);
   X_PHI_asym->SetFrameLineWidth(0);
   X_PHI_asym->SetFrameBorderMode(0);
   X_PHI_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(-4.3197,-0.001150689,3.5343,0.0006665027);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,-3.1416,3.1416);
   h1D_target_minus_toweight__8->SetBinContent(1,4.787184e-05);
   h1D_target_minus_toweight__8->SetBinContent(2,-0.0002810992);
   h1D_target_minus_toweight__8->SetBinContent(3,0.0001418116);
   h1D_target_minus_toweight__8->SetBinContent(4,2.500601e-06);
   h1D_target_minus_toweight__8->SetBinContent(5,7.946044e-05);
   h1D_target_minus_toweight__8->SetBinContent(6,-2.37124e-05);
   h1D_target_minus_toweight__8->SetBinContent(7,-0.0001425538);
   h1D_target_minus_toweight__8->SetBinContent(8,3.474299e-05);
   h1D_target_minus_toweight__8->SetBinContent(9,-0.000125695);
   h1D_target_minus_toweight__8->SetBinContent(10,0.0002580751);
   h1D_target_minus_toweight__8->SetBinContent(11,-0.0003636386);
   h1D_target_minus_toweight__8->SetBinContent(12,-0.0001896583);
   h1D_target_minus_toweight__8->SetBinContent(13,4.715472e-05);
   h1D_target_minus_toweight__8->SetBinContent(14,-3.060326e-05);
   h1D_target_minus_toweight__8->SetBinContent(15,0.0005014287);
   h1D_target_minus_toweight__8->SetBinContent(16,-0.0004563332);
   h1D_target_minus_toweight__8->SetBinContent(17,-0.0002488261);
   h1D_target_minus_toweight__8->SetBinContent(18,0.0001523551);
   h1D_target_minus_toweight__8->SetBinContent(19,0.0001783473);
   h1D_target_minus_toweight__8->SetBinContent(20,0.000367092);
   h1D_target_minus_toweight__8->SetBinContent(21,0.0001628734);
   h1D_target_minus_toweight__8->SetBinContent(22,-0.0001294231);
   h1D_target_minus_toweight__8->SetBinContent(23,0.000187451);
   h1D_target_minus_toweight__8->SetBinContent(24,1.691561e-05);
   h1D_target_minus_toweight__8->SetBinContent(25,-0.0002415502);
   h1D_target_minus_toweight__8->SetBinContent(26,-0.0001686118);
   h1D_target_minus_toweight__8->SetBinContent(27,-6.780913e-05);
   h1D_target_minus_toweight__8->SetBinContent(28,-8.277828e-05);
   h1D_target_minus_toweight__8->SetBinContent(29,0.0003693276);
   h1D_target_minus_toweight__8->SetBinContent(30,-5.743746e-05);
   h1D_target_minus_toweight__8->SetBinContent(31,5.372707e-05);
   h1D_target_minus_toweight__8->SetBinContent(32,0.0001168335);
   h1D_target_minus_toweight__8->SetBinContent(33,-6.758329e-05);
   h1D_target_minus_toweight__8->SetBinContent(34,-0.0002457276);
   h1D_target_minus_toweight__8->SetBinContent(35,0.0001388406);
   h1D_target_minus_toweight__8->SetBinContent(36,-0.000229883);
   h1D_target_minus_toweight__8->SetBinContent(37,-0.0003024656);
   h1D_target_minus_toweight__8->SetBinContent(38,0.00010812);
   h1D_target_minus_toweight__8->SetBinContent(39,0.0002085706);
   h1D_target_minus_toweight__8->SetBinContent(40,0.0001813266);
   h1D_target_minus_toweight__8->SetBinContent(41,-0.0004273867);
   h1D_target_minus_toweight__8->SetBinContent(42,-0.0001096996);
   h1D_target_minus_toweight__8->SetBinContent(43,-0.0002623145);
   h1D_target_minus_toweight__8->SetBinContent(44,0.0001880452);
   h1D_target_minus_toweight__8->SetBinContent(45,0.0006059296);
   h1D_target_minus_toweight__8->SetBinContent(46,-0.0001731031);
   h1D_target_minus_toweight__8->SetBinContent(47,6.814953e-05);
   h1D_target_minus_toweight__8->SetBinContent(48,0.0001073722);
   h1D_target_minus_toweight__8->SetBinContent(49,3.251713e-05);
   h1D_target_minus_toweight__8->SetBinContent(50,-5.107839e-05);
   h1D_target_minus_toweight__8->SetBinContent(51,-7.405877e-05);
   h1D_target_minus_toweight__8->SetBinContent(52,0.000131594);
   h1D_target_minus_toweight__8->SetBinContent(53,0.000110751);
   h1D_target_minus_toweight__8->SetBinContent(54,-0.0001971414);
   h1D_target_minus_toweight__8->SetBinContent(55,5.890429e-05);
   h1D_target_minus_toweight__8->SetBinContent(56,0.000342424);
   h1D_target_minus_toweight__8->SetBinContent(57,9.313039e-05);
   h1D_target_minus_toweight__8->SetBinContent(58,0.0002356926);
   h1D_target_minus_toweight__8->SetBinContent(59,-0.0001314031);
   h1D_target_minus_toweight__8->SetBinContent(60,-0.0005478431);
   h1D_target_minus_toweight__8->SetBinContent(61,0.0004537217);
   h1D_target_minus_toweight__8->SetBinContent(62,0.0004018033);
   h1D_target_minus_toweight__8->SetBinContent(63,-0.000307776);
   h1D_target_minus_toweight__8->SetBinContent(64,-0.0003463449);
   h1D_target_minus_toweight__8->SetBinContent(65,-0.0002044942);
   h1D_target_minus_toweight__8->SetBinContent(66,-6.149244e-05);
   h1D_target_minus_toweight__8->SetBinContent(67,1.974031e-05);
   h1D_target_minus_toweight__8->SetBinContent(68,-4.977919e-06);
   h1D_target_minus_toweight__8->SetBinContent(69,0.0001759408);
   h1D_target_minus_toweight__8->SetBinContent(70,-0.0001103478);
   h1D_target_minus_toweight__8->SetBinContent(71,-8.796807e-05);
   h1D_target_minus_toweight__8->SetBinContent(72,-0.0001025391);
   h1D_target_minus_toweight__8->SetBinContent(73,0.0002028649);
   h1D_target_minus_toweight__8->SetBinContent(74,-0.0001314646);
   h1D_target_minus_toweight__8->SetBinContent(75,0.0001525758);
   h1D_target_minus_toweight__8->SetBinContent(76,-0.0001500854);
   h1D_target_minus_toweight__8->SetBinContent(77,-4.706625e-05);
   h1D_target_minus_toweight__8->SetBinContent(78,-0.0001267465);
   h1D_target_minus_toweight__8->SetBinContent(79,0.0001837574);
   h1D_target_minus_toweight__8->SetBinContent(80,0.0001284415);
   h1D_target_minus_toweight__8->SetBinContent(81,0.0002058866);
   h1D_target_minus_toweight__8->SetBinContent(82,0.0001145005);
   h1D_target_minus_toweight__8->SetBinContent(83,0.0001268331);
   h1D_target_minus_toweight__8->SetBinContent(84,-0.0001000809);
   h1D_target_minus_toweight__8->SetBinContent(85,-0.0003361637);
   h1D_target_minus_toweight__8->SetBinContent(86,0.0003403947);
   h1D_target_minus_toweight__8->SetBinContent(87,0.0001432272);
   h1D_target_minus_toweight__8->SetBinContent(88,-2.818927e-05);
   h1D_target_minus_toweight__8->SetBinContent(89,-8.184556e-05);
   h1D_target_minus_toweight__8->SetBinContent(90,-0.0003616465);
   h1D_target_minus_toweight__8->SetBinContent(91,0.0002782652);
   h1D_target_minus_toweight__8->SetBinContent(92,-0.0003198916);
   h1D_target_minus_toweight__8->SetBinContent(93,8.924864e-05);
   h1D_target_minus_toweight__8->SetBinContent(94,0.0001231208);
   h1D_target_minus_toweight__8->SetBinContent(95,-0.000163951);
   h1D_target_minus_toweight__8->SetBinContent(96,1.761131e-05);
   h1D_target_minus_toweight__8->SetBinContent(97,-0.0001838775);
   h1D_target_minus_toweight__8->SetBinContent(98,5.365629e-05);
   h1D_target_minus_toweight__8->SetBinContent(99,0.0002561593);
   h1D_target_minus_toweight__8->SetBinContent(100,-0.0001107203);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("X_PHI");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{0} #rightarrow K^{#minus} #pi^{+})#minusN(D^{0} #rightarrow K^{#minus} K^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",-3.1416,3.1416);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   X_PHI_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(-4.3197,-0.1230072,3.5343,0.1497539);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,-3.1416,3.1416);
   h1D_asym__9->SetBinContent(1,-0.0121573);
   h1D_asym__9->SetBinContent(2,-0.02910051);
   h1D_asym__9->SetBinContent(3,-0.05614206);
   h1D_asym__9->SetBinContent(4,-0.03996515);
   h1D_asym__9->SetBinContent(5,-0.05983339);
   h1D_asym__9->SetBinContent(6,-0.08097748);
   h1D_asym__9->SetBinContent(7,-0.04776221);
   h1D_asym__9->SetBinContent(8,-0.03285945);
   h1D_asym__9->SetBinContent(9,-0.02945986);
   h1D_asym__9->SetBinContent(10,-0.03919785);
   h1D_asym__9->SetBinContent(11,-0.01761914);
   h1D_asym__9->SetBinContent(12,-0.03205641);
   h1D_asym__9->SetBinContent(13,-0.03455383);
   h1D_asym__9->SetBinContent(14,-0.03036123);
   h1D_asym__9->SetBinContent(15,-0.0388229);
   h1D_asym__9->SetBinContent(16,-0.007525828);
   h1D_asym__9->SetBinContent(17,-0.02722434);
   h1D_asym__9->SetBinContent(18,-0.01108041);
   h1D_asym__9->SetBinContent(19,-0.01924175);
   h1D_asym__9->SetBinContent(20,-0.03806078);
   h1D_asym__9->SetBinContent(21,-0.02054403);
   h1D_asym__9->SetBinContent(22,-0.01527811);
   h1D_asym__9->SetBinContent(23,-0.003185268);
   h1D_asym__9->SetBinContent(24,-0.02518549);
   h1D_asym__9->SetBinContent(25,0.0005554888);
   h1D_asym__9->SetBinContent(26,0.01494088);
   h1D_asym__9->SetBinContent(27,-0.004399996);
   h1D_asym__9->SetBinContent(28,-0.0005589836);
   h1D_asym__9->SetBinContent(29,-0.02150029);
   h1D_asym__9->SetBinContent(30,0.006971156);
   h1D_asym__9->SetBinContent(31,0.02399514);
   h1D_asym__9->SetBinContent(32,0.03613007);
   h1D_asym__9->SetBinContent(33,0.01883189);
   h1D_asym__9->SetBinContent(34,0.03232372);
   h1D_asym__9->SetBinContent(35,0.02655146);
   h1D_asym__9->SetBinContent(36,0.02982699);
   h1D_asym__9->SetBinContent(37,0.02878612);
   h1D_asym__9->SetBinContent(38,0.0189377);
   h1D_asym__9->SetBinContent(39,0.04742585);
   h1D_asym__9->SetBinContent(40,0.0262228);
   h1D_asym__9->SetBinContent(41,0.03009729);
   h1D_asym__9->SetBinContent(42,0.03085265);
   h1D_asym__9->SetBinContent(43,0.008476084);
   h1D_asym__9->SetBinContent(44,0.02779494);
   h1D_asym__9->SetBinContent(45,0.05455685);
   h1D_asym__9->SetBinContent(46,0.06057494);
   h1D_asym__9->SetBinContent(47,0.05087237);
   h1D_asym__9->SetBinContent(48,0.04080245);
   h1D_asym__9->SetBinContent(49,0.05759719);
   h1D_asym__9->SetBinContent(50,0.04994598);
   h1D_asym__9->SetBinContent(51,0.06283932);
   h1D_asym__9->SetBinContent(52,0.04708593);
   h1D_asym__9->SetBinContent(53,0.0631557);
   h1D_asym__9->SetBinContent(54,0.05977374);
   h1D_asym__9->SetBinContent(55,0.03908049);
   h1D_asym__9->SetBinContent(56,0.02646706);
   h1D_asym__9->SetBinContent(57,0.02887775);
   h1D_asym__9->SetBinContent(58,0.0303663);
   h1D_asym__9->SetBinContent(59,0.02821667);
   h1D_asym__9->SetBinContent(60,0.03321899);
   h1D_asym__9->SetBinContent(61,0.02157558);
   h1D_asym__9->SetBinContent(62,0.01852462);
   h1D_asym__9->SetBinContent(63,0.005880047);
   h1D_asym__9->SetBinContent(64,0.02887311);
   h1D_asym__9->SetBinContent(65,0.0150619);
   h1D_asym__9->SetBinContent(66,0.03025762);
   h1D_asym__9->SetBinContent(67,0.03362345);
   h1D_asym__9->SetBinContent(68,-0.001992796);
   h1D_asym__9->SetBinContent(69,-0.003679629);
   h1D_asym__9->SetBinContent(70,0.0193053);
   h1D_asym__9->SetBinContent(71,0.0114762);
   h1D_asym__9->SetBinContent(72,0.01011937);
   h1D_asym__9->SetBinContent(73,0.001125066);
   h1D_asym__9->SetBinContent(74,-0.001316243);
   h1D_asym__9->SetBinContent(75,0.008896453);
   h1D_asym__9->SetBinContent(76,0.03493049);
   h1D_asym__9->SetBinContent(77,-0.0006338996);
   h1D_asym__9->SetBinContent(78,0.01202963);
   h1D_asym__9->SetBinContent(79,0.03423626);
   h1D_asym__9->SetBinContent(80,-0.005923168);
   h1D_asym__9->SetBinContent(81,0.002930242);
   h1D_asym__9->SetBinContent(82,0.008541332);
   h1D_asym__9->SetBinContent(83,-0.0060347);
   h1D_asym__9->SetBinContent(84,-0.007190486);
   h1D_asym__9->SetBinContent(85,0.0001282389);
   h1D_asym__9->SetBinContent(86,0.01518695);
   h1D_asym__9->SetBinContent(87,0.007713342);
   h1D_asym__9->SetBinContent(88,-0.01288362);
   h1D_asym__9->SetBinContent(89,-0.01278111);
   h1D_asym__9->SetBinContent(90,-0.01897835);
   h1D_asym__9->SetBinContent(91,-0.004961997);
   h1D_asym__9->SetBinContent(92,-0.01806135);
   h1D_asym__9->SetBinContent(93,-0.0110959);
   h1D_asym__9->SetBinContent(94,-0.01274915);
   h1D_asym__9->SetBinContent(95,-0.01840758);
   h1D_asym__9->SetBinContent(96,-0.01062419);
   h1D_asym__9->SetBinContent(97,-0.04455744);
   h1D_asym__9->SetBinContent(98,-0.01930468);
   h1D_asym__9->SetBinContent(99,-0.04507801);
   h1D_asym__9->SetBinContent(100,-0.02580341);
   h1D_asym__9->SetBinError(1,0.04605655);
   h1D_asym__9->SetBinError(2,0.04363311);
   h1D_asym__9->SetBinError(3,0.0400776);
   h1D_asym__9->SetBinError(4,0.03922309);
   h1D_asym__9->SetBinError(5,0.03541812);
   h1D_asym__9->SetBinError(6,0.03089658);
   h1D_asym__9->SetBinError(7,0.03193444);
   h1D_asym__9->SetBinError(8,0.03300235);
   h1D_asym__9->SetBinError(9,0.0325262);
   h1D_asym__9->SetBinError(10,0.0321588);
   h1D_asym__9->SetBinError(11,0.03113794);
   h1D_asym__9->SetBinError(12,0.03087419);
   h1D_asym__9->SetBinError(13,0.03089521);
   h1D_asym__9->SetBinError(14,0.03132159);
   h1D_asym__9->SetBinError(15,0.03178615);
   h1D_asym__9->SetBinError(16,0.03466479);
   h1D_asym__9->SetBinError(17,0.03464134);
   h1D_asym__9->SetBinError(18,0.03646079);
   h1D_asym__9->SetBinError(19,0.03648588);
   h1D_asym__9->SetBinError(20,0.03641636);
   h1D_asym__9->SetBinError(21,0.04128738);
   h1D_asym__9->SetBinError(22,0.04295368);
   h1D_asym__9->SetBinError(23,0.04507498);
   h1D_asym__9->SetBinError(24,0.04563403);
   h1D_asym__9->SetBinError(25,0.04861655);
   h1D_asym__9->SetBinError(26,0.04967246);
   h1D_asym__9->SetBinError(27,0.04829464);
   h1D_asym__9->SetBinError(28,0.04712594);
   h1D_asym__9->SetBinError(29,0.04397584);
   h1D_asym__9->SetBinError(30,0.04516215);
   h1D_asym__9->SetBinError(31,0.04398522);
   h1D_asym__9->SetBinError(32,0.04385786);
   h1D_asym__9->SetBinError(33,0.04173892);
   h1D_asym__9->SetBinError(34,0.04124938);
   h1D_asym__9->SetBinError(35,0.04072551);
   h1D_asym__9->SetBinError(36,0.03816478);
   h1D_asym__9->SetBinError(37,0.03756114);
   h1D_asym__9->SetBinError(38,0.03645632);
   h1D_asym__9->SetBinError(39,0.03825817);
   h1D_asym__9->SetBinError(40,0.03620125);
   h1D_asym__9->SetBinError(41,0.03607838);
   h1D_asym__9->SetBinError(42,0.0363497);
   h1D_asym__9->SetBinError(43,0.03471699);
   h1D_asym__9->SetBinError(44,0.03584679);
   h1D_asym__9->SetBinError(45,0.03782714);
   h1D_asym__9->SetBinError(46,0.04010043);
   h1D_asym__9->SetBinError(47,0.04058451);
   h1D_asym__9->SetBinError(48,0.04341427);
   h1D_asym__9->SetBinError(49,0.046938);
   h1D_asym__9->SetBinError(50,0.04842308);
   h1D_asym__9->SetBinError(51,0.04794875);
   h1D_asym__9->SetBinError(52,0.04577519);
   h1D_asym__9->SetBinError(53,0.04338206);
   h1D_asym__9->SetBinError(54,0.04011637);
   h1D_asym__9->SetBinError(55,0.03697264);
   h1D_asym__9->SetBinError(56,0.03433874);
   h1D_asym__9->SetBinError(57,0.03404519);
   h1D_asym__9->SetBinError(58,0.03428195);
   h1D_asym__9->SetBinError(59,0.0342278);
   h1D_asym__9->SetBinError(60,0.03482565);
   h1D_asym__9->SetBinError(61,0.03427701);
   h1D_asym__9->SetBinError(62,0.0344059);
   h1D_asym__9->SetBinError(63,0.03391843);
   h1D_asym__9->SetBinError(64,0.0359368);
   h1D_asym__9->SetBinError(65,0.03505189);
   h1D_asym__9->SetBinError(66,0.03975948);
   h1D_asym__9->SetBinError(67,0.04126687);
   h1D_asym__9->SetBinError(68,0.03928934);
   h1D_asym__9->SetBinError(69,0.04038856);
   h1D_asym__9->SetBinError(70,0.0434456);
   h1D_asym__9->SetBinError(71,0.04601508);
   h1D_asym__9->SetBinError(72,0.04630765);
   h1D_asym__9->SetBinError(73,0.04764394);
   h1D_asym__9->SetBinError(74,0.04940378);
   h1D_asym__9->SetBinError(75,0.05207149);
   h1D_asym__9->SetBinError(76,0.05320996);
   h1D_asym__9->SetBinError(77,0.04931489);
   h1D_asym__9->SetBinError(78,0.04725459);
   h1D_asym__9->SetBinError(79,0.04739753);
   h1D_asym__9->SetBinError(80,0.04439975);
   h1D_asym__9->SetBinError(81,0.03829662);
   h1D_asym__9->SetBinError(82,0.03736392);
   h1D_asym__9->SetBinError(83,0.03648251);
   h1D_asym__9->SetBinError(84,0.03446748);
   h1D_asym__9->SetBinError(85,0.03486762);
   h1D_asym__9->SetBinError(86,0.03293299);
   h1D_asym__9->SetBinError(87,0.03173328);
   h1D_asym__9->SetBinError(88,0.03069709);
   h1D_asym__9->SetBinError(89,0.03069177);
   h1D_asym__9->SetBinError(90,0.03023106);
   h1D_asym__9->SetBinError(91,0.03262281);
   h1D_asym__9->SetBinError(92,0.03160537);
   h1D_asym__9->SetBinError(93,0.03226582);
   h1D_asym__9->SetBinError(94,0.03230675);
   h1D_asym__9->SetBinError(95,0.03268408);
   h1D_asym__9->SetBinError(96,0.03775373);
   h1D_asym__9->SetBinError(97,0.03751299);
   h1D_asym__9->SetBinError(98,0.04226237);
   h1D_asym__9->SetBinError(99,0.04264764);
   h1D_asym__9->SetBinError(100,0.04500808);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("X_PHI");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",-3.1416,3.1416);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","0.004984513910",-3.1416,3.1416);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   X_PHI_asym->cd();
   X_PHI_asym->Modified();
   X_PHI_asym->cd();
   X_PHI_asym->SetSelected(X_PHI_asym);
}
