void X_P_asym()
{
//=========Macro generated from canvas: X_P_asym/X_P_asym
//=========  (Mon Nov 18 20:49:37 2019) by ROOT version6.08/02
   TCanvas *X_P_asym = new TCanvas("X_P_asym", "X_P_asym",0,0,1000,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   X_P_asym->SetHighLightColor(2);
   X_P_asym->Range(0,0,1,1);
   X_P_asym->SetFillColor(0);
   X_P_asym->SetBorderMode(0);
   X_P_asym->SetBorderSize(10);
   X_P_asym->SetTickx(1);
   X_P_asym->SetTicky(1);
   X_P_asym->SetLeftMargin(0.18);
   X_P_asym->SetRightMargin(0.05);
   X_P_asym->SetTopMargin(0.07);
   X_P_asym->SetBottomMargin(0.16);
   X_P_asym->SetFrameFillStyle(0);
   X_P_asym->SetFrameLineStyle(0);
   X_P_asym->SetFrameLineColor(0);
   X_P_asym->SetFrameLineWidth(0);
   X_P_asym->SetFrameBorderMode(0);
   X_P_asym->SetFrameBorderSize(10);
  
// ------------>Primitives in pad: lowerPad
   TPad *lowerPad = new TPad("lowerPad", "lowerPad",0,0,1,0.54);
   lowerPad->Draw();
   lowerPad->cd();
   lowerPad->Range(-46875,-0.001557362,265625,0.001618108);
   lowerPad->SetFillColor(0);
   lowerPad->SetBorderMode(0);
   lowerPad->SetBorderSize(10);
   lowerPad->SetTickx(1);
   lowerPad->SetTicky(1);
   lowerPad->SetLeftMargin(0.15);
   lowerPad->SetRightMargin(0.05);
   lowerPad->SetTopMargin(0);
   lowerPad->SetBottomMargin(0.3);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   lowerPad->SetFrameFillStyle(0);
   lowerPad->SetFrameLineStyle(0);
   lowerPad->SetFrameLineColor(0);
   lowerPad->SetFrameLineWidth(0);
   lowerPad->SetFrameBorderMode(0);
   lowerPad->SetFrameBorderSize(10);
   
   TH1D *h1D_target_minus_toweight__8 = new TH1D("h1D_target_minus_toweight__8","h1D_target_minus_toweight",100,0,250000);
   h1D_target_minus_toweight__8->SetBinContent(13,-3.934652e-05);
   h1D_target_minus_toweight__8->SetBinContent(14,0.0005322583);
   h1D_target_minus_toweight__8->SetBinContent(15,0.001041733);
   h1D_target_minus_toweight__8->SetBinContent(16,0.001512259);
   h1D_target_minus_toweight__8->SetBinContent(17,0.001061022);
   h1D_target_minus_toweight__8->SetBinContent(18,4.537404e-06);
   h1D_target_minus_toweight__8->SetBinContent(19,0.0002930611);
   h1D_target_minus_toweight__8->SetBinContent(20,0.0003633574);
   h1D_target_minus_toweight__8->SetBinContent(21,0.0001808293);
   h1D_target_minus_toweight__8->SetBinContent(22,-0.0001260303);
   h1D_target_minus_toweight__8->SetBinContent(23,-0.0003166609);
   h1D_target_minus_toweight__8->SetBinContent(24,-0.0002537519);
   h1D_target_minus_toweight__8->SetBinContent(25,-0.0005039126);
   h1D_target_minus_toweight__8->SetBinContent(26,-0.0002770647);
   h1D_target_minus_toweight__8->SetBinContent(27,-0.0002401546);
   h1D_target_minus_toweight__8->SetBinContent(28,-0.0003166739);
   h1D_target_minus_toweight__8->SetBinContent(29,-0.0003250893);
   h1D_target_minus_toweight__8->SetBinContent(30,-6.88266e-05);
   h1D_target_minus_toweight__8->SetBinContent(31,-0.0003188513);
   h1D_target_minus_toweight__8->SetBinContent(32,-0.0002537016);
   h1D_target_minus_toweight__8->SetBinContent(33,-0.0003889911);
   h1D_target_minus_toweight__8->SetBinContent(34,-8.866657e-05);
   h1D_target_minus_toweight__8->SetBinContent(35,-0.0001940001);
   h1D_target_minus_toweight__8->SetBinContent(36,-0.0001313165);
   h1D_target_minus_toweight__8->SetBinContent(37,-0.0001457627);
   h1D_target_minus_toweight__8->SetBinContent(38,-0.0001304219);
   h1D_target_minus_toweight__8->SetBinContent(39,-9.445287e-05);
   h1D_target_minus_toweight__8->SetBinContent(40,-0.0001463569);
   h1D_target_minus_toweight__8->SetBinContent(41,-0.0001733939);
   h1D_target_minus_toweight__8->SetBinContent(42,-3.015064e-05);
   h1D_target_minus_toweight__8->SetBinContent(43,-9.157509e-05);
   h1D_target_minus_toweight__8->SetBinContent(44,-5.646539e-05);
   h1D_target_minus_toweight__8->SetBinContent(45,-3.158371e-05);
   h1D_target_minus_toweight__8->SetBinContent(46,-8.784607e-05);
   h1D_target_minus_toweight__8->SetBinContent(47,4.631304e-05);
   h1D_target_minus_toweight__8->SetBinContent(48,2.026872e-05);
   h1D_target_minus_toweight__8->SetBinContent(49,-4.79623e-05);
   h1D_target_minus_toweight__8->SetBinContent(50,-5.143997e-05);
   h1D_target_minus_toweight__8->SetBinContent(51,-2.228015e-05);
   h1D_target_minus_toweight__8->SetBinContent(52,-1.386268e-05);
   h1D_target_minus_toweight__8->SetBinContent(53,-1.696532e-05);
   h1D_target_minus_toweight__8->SetBinContent(54,-9.77749e-06);
   h1D_target_minus_toweight__8->SetBinContent(55,6.305549e-06);
   h1D_target_minus_toweight__8->SetBinContent(56,-3.426656e-06);
   h1D_target_minus_toweight__8->SetBinContent(57,-2.433668e-05);
   h1D_target_minus_toweight__8->SetBinContent(58,-5.691545e-07);
   h1D_target_minus_toweight__8->SetBinContent(59,-1.783487e-05);
   h1D_target_minus_toweight__8->SetBinContent(60,-1.385796e-05);
   h1D_target_minus_toweight__8->SetBinContent(61,-2.44267e-06);
   h1D_target_minus_toweight__8->SetBinContent(62,-5.985079e-06);
   h1D_target_minus_toweight__8->SetBinContent(63,-9.932242e-06);
   h1D_target_minus_toweight__8->SetBinContent(64,1.648827e-06);
   h1D_target_minus_toweight__8->SetBinContent(65,5.578768e-07);
   h1D_target_minus_toweight__8->SetBinContent(66,-8.764218e-07);
   h1D_target_minus_toweight__8->SetBinContent(67,-5.816633e-06);
   h1D_target_minus_toweight__8->SetBinContent(68,7.319726e-06);
   h1D_target_minus_toweight__8->SetBinContent(69,4.626516e-06);
   h1D_target_minus_toweight__8->SetBinContent(70,7.141733e-06);
   h1D_target_minus_toweight__8->SetBinContent(71,-1.610077e-05);
   h1D_target_minus_toweight__8->SetBinContent(72,-6.015818e-06);
   h1D_target_minus_toweight__8->SetBinContent(73,1.433464e-05);
   h1D_target_minus_toweight__8->SetBinContent(74,-1.2356e-06);
   h1D_target_minus_toweight__8->SetBinContent(75,3.546656e-06);
   h1D_target_minus_toweight__8->SetBinContent(76,-5.693446e-07);
   h1D_target_minus_toweight__8->SetBinContent(77,-1.943222e-07);
   h1D_target_minus_toweight__8->SetBinContent(78,-3.745796e-06);
   h1D_target_minus_toweight__8->SetBinContent(79,2.761599e-06);
   h1D_target_minus_toweight__8->SetBinContent(80,-7.791982e-09);
   h1D_target_minus_toweight__8->SetBinContent(81,8.663018e-07);
   h1D_target_minus_toweight__8->SetBinContent(82,-5.75932e-07);
   h1D_target_minus_toweight__8->SetBinContent(83,9.980002e-07);
   h1D_target_minus_toweight__8->SetBinContent(84,3.077131e-08);
   h1D_target_minus_toweight__8->SetBinContent(85,-5.908986e-07);
   h1D_target_minus_toweight__8->SetBinContent(86,9.300515e-07);
   h1D_target_minus_toweight__8->SetBinContent(87,8.9199e-07);
   h1D_target_minus_toweight__8->SetBinContent(89,-1.449074e-07);
   h1D_target_minus_toweight__8->SetEntries(100);
   h1D_target_minus_toweight__8->SetStats(0);
   h1D_target_minus_toweight__8->SetLineStyle(0);
   h1D_target_minus_toweight__8->SetLineWidth(2);
   h1D_target_minus_toweight__8->SetMarkerStyle(20);
   h1D_target_minus_toweight__8->SetMarkerSize(1.5);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitle("p (X) [MeV/c]");
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetXaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetXaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitle("N(D^{0} #rightarrow K^{#minus} #pi^{+})#minusN(D^{0} #rightarrow K^{#minus} K^{+})");
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelOffset(0.01);
   h1D_target_minus_toweight__8->GetYaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleOffset(0.95);
   h1D_target_minus_toweight__8->GetYaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelFont(132);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelOffset(0.008);
   h1D_target_minus_toweight__8->GetZaxis()->SetLabelSize(0.055);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleSize(0.0605);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleOffset(1.8);
   h1D_target_minus_toweight__8->GetZaxis()->SetTitleFont(132);
   h1D_target_minus_toweight__8->Draw("pe");
   
   TF1 *zero2 = new TF1("zero","0",0,250000);
   zero2->SetFillColor(19);
   zero2->SetFillStyle(0);
   zero2->SetMarkerStyle(25);
   zero2->SetLineWidth(2);
   zero2->SetLineStyle(2);
   zero2->GetXaxis()->SetLabelFont(132);
   zero2->GetXaxis()->SetLabelOffset(0.01);
   zero2->GetXaxis()->SetLabelSize(0.055);
   zero2->GetXaxis()->SetTitleSize(0.0605);
   zero2->GetXaxis()->SetTitleOffset(0.95);
   zero2->GetXaxis()->SetTitleFont(132);
   zero2->GetYaxis()->SetLabelFont(132);
   zero2->GetYaxis()->SetLabelOffset(0.01);
   zero2->GetYaxis()->SetLabelSize(0.055);
   zero2->GetYaxis()->SetTitleSize(0.0605);
   zero2->GetYaxis()->SetTitleOffset(0.95);
   zero2->GetYaxis()->SetTitleFont(132);
   zero2->Draw("lsame");
   lowerPad->Modified();
   X_P_asym->cd();
  
// ------------>Primitives in pad: upperPad
   TPad *upperPad = new TPad("upperPad", "upperPad",0,0.55,1,1);
   upperPad->Draw();
   upperPad->cd();
   upperPad->Range(-46875,-42.5539,265625,50.9498);
   upperPad->SetFillColor(0);
   upperPad->SetBorderMode(0);
   upperPad->SetBorderSize(10);
   upperPad->SetTickx(1);
   upperPad->SetTicky(1);
   upperPad->SetLeftMargin(0.15);
   upperPad->SetRightMargin(0.05);
   upperPad->SetBottomMargin(0);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   upperPad->SetFrameFillStyle(0);
   upperPad->SetFrameLineStyle(0);
   upperPad->SetFrameLineColor(0);
   upperPad->SetFrameLineWidth(0);
   upperPad->SetFrameBorderMode(0);
   upperPad->SetFrameBorderSize(10);
   
   TH1D *h1D_asym__9 = new TH1D("h1D_asym__9","h1D_asym",100,0,250000);
   h1D_asym__9->SetBinContent(13,0.02808969);
   h1D_asym__9->SetBinContent(14,0.009996695);
   h1D_asym__9->SetBinContent(15,0.008117571);
   h1D_asym__9->SetBinContent(16,0.01193064);
   h1D_asym__9->SetBinContent(17,-0.003059729);
   h1D_asym__9->SetBinContent(18,0.005752285);
   h1D_asym__9->SetBinContent(19,0.002747322);
   h1D_asym__9->SetBinContent(20,0.001200329);
   h1D_asym__9->SetBinContent(21,0.009497219);
   h1D_asym__9->SetBinContent(22,0.002481201);
   h1D_asym__9->SetBinContent(23,0.0028961);
   h1D_asym__9->SetBinContent(24,0.009496814);
   h1D_asym__9->SetBinContent(25,0.007460034);
   h1D_asym__9->SetBinContent(26,0.007630048);
   h1D_asym__9->SetBinContent(27,0.0009620363);
   h1D_asym__9->SetBinContent(28,-0.001804426);
   h1D_asym__9->SetBinContent(29,0.005804335);
   h1D_asym__9->SetBinContent(30,-0.004802456);
   h1D_asym__9->SetBinContent(31,0.01407761);
   h1D_asym__9->SetBinContent(32,-0.003578158);
   h1D_asym__9->SetBinContent(33,-0.005291705);
   h1D_asym__9->SetBinContent(34,0.002829644);
   h1D_asym__9->SetBinContent(35,-0.004028704);
   h1D_asym__9->SetBinContent(36,0.005871919);
   h1D_asym__9->SetBinContent(37,-0.002482777);
   h1D_asym__9->SetBinContent(38,-0.0007166852);
   h1D_asym__9->SetBinContent(39,0.01009932);
   h1D_asym__9->SetBinContent(40,-0.009268432);
   h1D_asym__9->SetBinContent(41,-0.004130063);
   h1D_asym__9->SetBinContent(42,-0.01254963);
   h1D_asym__9->SetBinContent(43,-0.02139803);
   h1D_asym__9->SetBinContent(44,-0.005667588);
   h1D_asym__9->SetBinContent(45,-0.01929618);
   h1D_asym__9->SetBinContent(46,0.001754044);
   h1D_asym__9->SetBinContent(47,-0.005770898);
   h1D_asym__9->SetBinContent(48,-0.003981308);
   h1D_asym__9->SetBinContent(49,-0.01603921);
   h1D_asym__9->SetBinContent(50,-0.003857164);
   h1D_asym__9->SetBinContent(51,-0.02264271);
   h1D_asym__9->SetBinContent(52,0.009719639);
   h1D_asym__9->SetBinContent(53,-0.02786527);
   h1D_asym__9->SetBinContent(54,-0.02236971);
   h1D_asym__9->SetBinContent(55,0.02714782);
   h1D_asym__9->SetBinContent(56,-0.05000236);
   h1D_asym__9->SetBinContent(57,-0.02017617);
   h1D_asym__9->SetBinContent(58,0.0203151);
   h1D_asym__9->SetBinContent(59,-0.001155389);
   h1D_asym__9->SetBinContent(60,-0.01749899);
   h1D_asym__9->SetBinContent(61,-0.03777561);
   h1D_asym__9->SetBinContent(62,-0.04377649);
   h1D_asym__9->SetBinContent(63,-0.0114977);
   h1D_asym__9->SetBinContent(64,0.007390247);
   h1D_asym__9->SetBinContent(65,-0.06298275);
   h1D_asym__9->SetBinContent(66,-0.06457304);
   h1D_asym__9->SetBinContent(67,0.03051611);
   h1D_asym__9->SetBinContent(68,-0.02055012);
   h1D_asym__9->SetBinContent(69,-0.1216871);
   h1D_asym__9->SetBinContent(70,0.1605484);
   h1D_asym__9->SetBinContent(71,-0.4546131);
   h1D_asym__9->SetBinContent(72,-0.234709);
   h1D_asym__9->SetBinContent(73,-0.1085576);
   h1D_asym__9->SetBinContent(74,0.2468894);
   h1D_asym__9->SetBinContent(75,0.2359294);
   h1D_asym__9->SetBinContent(76,0.122078);
   h1D_asym__9->SetBinContent(77,-0.04124672);
   h1D_asym__9->SetBinContent(78,0.01665102);
   h1D_asym__9->SetBinContent(79,-0.1711834);
   h1D_asym__9->SetBinContent(80,0.7482884);
   h1D_asym__9->SetBinContent(81,-1.253132);
   h1D_asym__9->SetBinContent(82,-0.6646188);
   h1D_asym__9->SetBinContent(83,-0.04380903);
   h1D_asym__9->SetBinContent(85,-0.5726458);
   h1D_asym__9->SetBinContent(89,1);
   h1D_asym__9->SetBinError(13,0.02306566);
   h1D_asym__9->SetBinError(14,0.01887313);
   h1D_asym__9->SetBinError(15,0.01711071);
   h1D_asym__9->SetBinError(16,0.01645347);
   h1D_asym__9->SetBinError(17,0.01561041);
   h1D_asym__9->SetBinError(18,0.01583986);
   h1D_asym__9->SetBinError(19,0.01607635);
   h1D_asym__9->SetBinError(20,0.0163797);
   h1D_asym__9->SetBinError(21,0.01710479);
   h1D_asym__9->SetBinError(22,0.01742771);
   h1D_asym__9->SetBinError(23,0.017751);
   h1D_asym__9->SetBinError(24,0.01845271);
   h1D_asym__9->SetBinError(25,0.01832529);
   h1D_asym__9->SetBinError(26,0.01857247);
   h1D_asym__9->SetBinError(27,0.01866582);
   h1D_asym__9->SetBinError(28,0.01888203);
   h1D_asym__9->SetBinError(29,0.01922214);
   h1D_asym__9->SetBinError(30,0.01923779);
   h1D_asym__9->SetBinError(31,0.0200782);
   h1D_asym__9->SetBinError(32,0.01979594);
   h1D_asym__9->SetBinError(33,0.02020901);
   h1D_asym__9->SetBinError(34,0.02118266);
   h1D_asym__9->SetBinError(35,0.02138265);
   h1D_asym__9->SetBinError(36,0.02246449);
   h1D_asym__9->SetBinError(37,0.02285654);
   h1D_asym__9->SetBinError(38,0.02369335);
   h1D_asym__9->SetBinError(39,0.02527698);
   h1D_asym__9->SetBinError(40,0.02554418);
   h1D_asym__9->SetBinError(41,0.02689773);
   h1D_asym__9->SetBinError(42,0.02766335);
   h1D_asym__9->SetBinError(43,0.02838152);
   h1D_asym__9->SetBinError(44,0.03073576);
   h1D_asym__9->SetBinError(45,0.03187826);
   h1D_asym__9->SetBinError(46,0.0345647);
   h1D_asym__9->SetBinError(47,0.03665016);
   h1D_asym__9->SetBinError(48,0.03850108);
   h1D_asym__9->SetBinError(49,0.03943183);
   h1D_asym__9->SetBinError(50,0.04240164);
   h1D_asym__9->SetBinError(51,0.04548843);
   h1D_asym__9->SetBinError(52,0.05011984);
   h1D_asym__9->SetBinError(53,0.05089929);
   h1D_asym__9->SetBinError(54,0.05447279);
   h1D_asym__9->SetBinError(55,0.06358732);
   h1D_asym__9->SetBinError(56,0.06079449);
   h1D_asym__9->SetBinError(57,0.06746354);
   h1D_asym__9->SetBinError(58,0.07854646);
   h1D_asym__9->SetBinError(59,0.0819522);
   h1D_asym__9->SetBinError(60,0.08774291);
   h1D_asym__9->SetBinError(61,0.09571233);
   h1D_asym__9->SetBinError(62,0.1033029);
   h1D_asym__9->SetBinError(63,0.1163922);
   h1D_asym__9->SetBinError(64,0.1244315);
   h1D_asym__9->SetBinError(65,0.1400973);
   h1D_asym__9->SetBinError(66,0.1453649);
   h1D_asym__9->SetBinError(67,0.1975958);
   h1D_asym__9->SetBinError(68,0.2066381);
   h1D_asym__9->SetBinError(69,0.1925944);
   h1D_asym__9->SetBinError(70,0.3368639);
   h1D_asym__9->SetBinError(71,0.1277204);
   h1D_asym__9->SetBinError(72,0.2278887);
   h1D_asym__9->SetBinError(73,0.4170443);
   h1D_asym__9->SetBinError(74,0.845575);
   h1D_asym__9->SetBinError(75,0.658442);
   h1D_asym__9->SetBinError(76,0.6712518);
   h1D_asym__9->SetBinError(77,0.7256042);
   h1D_asym__9->SetBinError(78,0.8846559);
   h1D_asym__9->SetBinError(79,0.7995645);
   h1D_asym__9->SetBinError(80,0.4858096);
   h1D_asym__9->SetBinError(81,2.48279);
   h1D_asym__9->SetBinError(82,2.045654);
   h1D_asym__9->SetBinError(83,3.663906);
   h1D_asym__9->SetBinError(85,38.16477);
   h1D_asym__9->SetEntries(100);
   h1D_asym__9->SetStats(0);
   h1D_asym__9->SetLineStyle(0);
   h1D_asym__9->SetLineWidth(2);
   h1D_asym__9->SetMarkerStyle(20);
   h1D_asym__9->SetMarkerSize(1.5);
   h1D_asym__9->GetXaxis()->SetTitle("p (X) [MeV/c]");
   h1D_asym__9->GetXaxis()->SetLabelFont(132);
   h1D_asym__9->GetXaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetXaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetXaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetXaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetXaxis()->SetTitleFont(132);
   h1D_asym__9->GetYaxis()->SetTitle("Asymmetry");
   h1D_asym__9->GetYaxis()->SetLabelFont(132);
   h1D_asym__9->GetYaxis()->SetLabelOffset(0.01);
   h1D_asym__9->GetYaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetYaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetYaxis()->SetTitleOffset(0.95);
   h1D_asym__9->GetYaxis()->SetTitleFont(132);
   h1D_asym__9->GetZaxis()->SetLabelFont(132);
   h1D_asym__9->GetZaxis()->SetLabelOffset(0.008);
   h1D_asym__9->GetZaxis()->SetLabelSize(0.055);
   h1D_asym__9->GetZaxis()->SetTitleSize(0.0605);
   h1D_asym__9->GetZaxis()->SetTitleOffset(1.8);
   h1D_asym__9->GetZaxis()->SetTitleFont(132);
   h1D_asym__9->Draw("p");
   
   TF1 *zero3 = new TF1("zero","0",0,250000);
   zero3->SetFillColor(19);
   zero3->SetFillStyle(0);
   zero3->SetMarkerStyle(25);
   zero3->SetLineWidth(2);
   zero3->SetLineStyle(2);
   zero3->GetXaxis()->SetLabelFont(132);
   zero3->GetXaxis()->SetLabelOffset(0.01);
   zero3->GetXaxis()->SetLabelSize(0.055);
   zero3->GetXaxis()->SetTitleSize(0.0605);
   zero3->GetXaxis()->SetTitleOffset(0.95);
   zero3->GetXaxis()->SetTitleFont(132);
   zero3->GetYaxis()->SetLabelFont(132);
   zero3->GetYaxis()->SetLabelOffset(0.01);
   zero3->GetYaxis()->SetLabelSize(0.055);
   zero3->GetYaxis()->SetTitleSize(0.0605);
   zero3->GetYaxis()->SetTitleOffset(0.95);
   zero3->GetYaxis()->SetTitleFont(132);
   zero3->Draw("lsame");
   
   TF1 *average4 = new TF1("average","0.005002760865",0,250000);
   average4->SetFillColor(19);
   average4->SetFillStyle(0);
   average4->SetMarkerStyle(20);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ff0000");
   average4->SetLineColor(ci);
   average4->SetLineWidth(2);
   average4->SetLineStyle(0);
   average4->GetXaxis()->SetLabelFont(132);
   average4->GetXaxis()->SetLabelOffset(0.01);
   average4->GetXaxis()->SetLabelSize(0.055);
   average4->GetXaxis()->SetTitleSize(0.0605);
   average4->GetXaxis()->SetTitleOffset(0.95);
   average4->GetXaxis()->SetTitleFont(132);
   average4->GetYaxis()->SetLabelFont(132);
   average4->GetYaxis()->SetLabelOffset(0.01);
   average4->GetYaxis()->SetLabelSize(0.055);
   average4->GetYaxis()->SetTitleSize(0.0605);
   average4->GetYaxis()->SetTitleOffset(0.95);
   average4->GetYaxis()->SetTitleFont(132);
   average4->Draw("lsame");
   upperPad->Modified();
   X_P_asym->cd();
   X_P_asym->Modified();
   X_P_asym->cd();
   X_P_asym->SetSelected(X_P_asym);
}
