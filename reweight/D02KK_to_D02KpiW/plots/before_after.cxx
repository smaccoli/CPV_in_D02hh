#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

string convertToString(const char* a, int size) 
{ 
  int i; 
  string s = ""; 
  for (i = 0; i <= size; i++) { 
    s = s + a[i]; 
  } 
  return s; 
} 

void before_after(TString year = "16", TString polarity = "Dw", string variable = "") {

  TString tmp = variable.c_str();
  tmp.ReplaceAll("__",",");
  variable = convertToString((const char*)tmp, sizeof(tmp));

  dcastyle();

  bool rew_X = 1;
  TString add_string = "";
  if (rew_X)
    add_string = "_X";

  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/D02KmKp_"+year+"_"+polarity+".root"); 
  toweight_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/D02KmKp_"+year+"_"+polarity+"_SPlot.root");
  toweight_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/D02KmKp_"+year+"_"+polarity+"_rew"+add_string+".root");

  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+".root"); 
  target_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+"_SPlot.root");
  target_tree->AddFriend("ntp","/home/LHCB/smaccoli/CPV_in_D02hh/data/D02Kmpip_"+year+"_"+polarity+"_rew.root");
  
  IDecay* toweight_decay = new IDecay(toweight_tree,"D02KmKp","D^{0} #rightarrow K^{#minus} K^{+}",{"N_S_sw"},toweight_tree->GetEntries(),0);

  IDecay* target_decay = new IDecay(target_tree,"D02Kmpip","D^{0} #rightarrow K^{#minus} #pi^{+}",{"N_S_sw","Iw"},target_tree->GetEntries(),0);

  if (variable == "") {
    toweight_decay->addTargetDistribution(target_decay,{"Dst_PT(100,2.28e3,9.5e3)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"Dst_ETA(100,2.4,4.3)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"Dst_PHI(100,-3.1416,3.1416)"},5);
    toweight_decay->addTargetDistribution(target_decay,{"sPi_PT(100,100,8e2)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"sPi_ETA(100,2.4,4.3)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"sPi_PHI(100,-3.1416,3.1416)"},5);

    toweight_decay->addTargetDistribution(target_decay,{"Dst_PX(100,-9.5e3,9.5e3)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"sPi_PX(100,-8e2,8e2)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"Dst_PY(100,-9.5e3,9.5e3)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"sPi_PY(100,-8e2,8e2)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"Dst_PZ(100,25e3,180e3)"},1);
    toweight_decay->addTargetDistribution(target_decay,{"sPi_PZ(100,2e3,19.5e3)"},1);
  }
  else {
    toweight_decay->addTargetDistribution(target_decay,{variable},5);
  }
  
  toweight_decay->setAsymVariable("Dst_ID");
  target_decay->setAsymVariable("Dst_ID");
  toweight_decay->calculateResidualAsymmetry = 1;

  toweight_decay->fillTargets();

  toweight_decay->use_Iw_max = 0;
  toweight_decay->Iw_max = 15;
  toweight_decay->Iw_max_value = 0;

  toweight_decay->setIWeight("Iw"); //from rew.root

  toweight_decay->plotDir = year+"_"+polarity+add_string+"/";
  toweight_decay->showVariables();
}

int main(int argc, char * argv[]) {
  before_after(argv[1],argv[2],argv[3]);
  return 0;
}
