#!/bin/bash
g++ -Wall -o _apply apply.cxx `root-config --cflags --glibs` `gsl-config --cflags --libs`

mode=apply
DIR=$PWD

#for year in $(echo 15 16 17 18); do
for year in $(echo 16); do
    #for year in $(echo 18); do
    #for year in $(echo 16 17 18); do
    for pol in $(echo Dw Up); do
	#for pol in $(echo Up); do
	echo "#!/bin/sh" > exec/$mode.$year.$pol.sh
	echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> exec/$mode.$year.$pol.sh 
	echo "cd $DIR" >> exec/$mode.$year.$pol.sh
	echo "hostname" >> exec/$mode.$year.$pol.sh
	echo "time ./_apply $year $pol" >> exec/$mode.$year.$pol.sh
	chmod +x exec/$mode.$year.$pol.sh
	python condor_make_template.py condor_template.sub $mode.$year.$pol
	condor_submit exec/condor_template.$mode.$year.$pol.sub -batch-name $mode.$year.$pol
	#break
    done
    #break
done
