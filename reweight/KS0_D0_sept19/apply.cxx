#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void apply() {

  myH * h_KS0_PTvsETA = new myH("weights/16_Dw_0.myH");
  myH * h_KS0_PHI = new myH("weights/16_Dw_1.myH");
  
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/sept19/Dp2KS0pipLL_16_Dw.root");
  
  double KS0_PT;
  toweight_tree->SetBranchAddress("KS0_PT",&KS0_PT);
  double KS0_ETA;
  toweight_tree->SetBranchAddress("KS0_ETA",&KS0_ETA);
  double KS0_PHI;
  toweight_tree->SetBranchAddress("KS0_PHI",&KS0_PHI);

  double Dplus_PT;
  toweight_tree->SetBranchAddress("Dplus_PT",&Dplus_PT);
  double Dplus_ETA;
  toweight_tree->SetBranchAddress("Dplus_ETA",&Dplus_ETA);
  double Dplus_PHI;
  toweight_tree->SetBranchAddress("Dplus_PHI",&Dplus_PHI);
  
  double hplus_PT;
  toweight_tree->SetBranchAddress("hplus_PT",&hplus_PT);
  double hplus_ETA;
  toweight_tree->SetBranchAddress("hplus_ETA",&hplus_ETA);
  double hplus_PHI;
  toweight_tree->SetBranchAddress("hplus_PHI",&hplus_PHI);

  double Dplus_M;
  toweight_tree->SetBranchAddress("Dplus_M",&Dplus_M);
  int Dplus_ID;
  toweight_tree->SetBranchAddress("Dplus_ID",&Dplus_ID);
  
  
  TFile *f = TFile::Open("rew.root","RECREATE");
  TTree * t = new TTree("ntp","ntp");
  double Iw;
  t->Branch("Iw",&Iw);
  t->Branch("KS0_PT",&KS0_PT);
  t->Branch("KS0_ETA",&KS0_ETA);
  t->Branch("KS0_PHI",&KS0_PHI);
  t->Branch("Dplus_PT",&Dplus_PT);
  t->Branch("Dplus_ETA",&Dplus_ETA);
  t->Branch("Dplus_PHI",&Dplus_PHI);
  t->Branch("hplus_PT",&hplus_PT);
  t->Branch("hplus_ETA",&hplus_ETA);
  t->Branch("hplus_PHI",&hplus_PHI);
  t->Branch("Dplus_M",&Dplus_M);
  t->Branch("Dplus_ID",&Dplus_ID);

  double HarmCut_post = 0;
  t->Branch("HarmCut_post",&HarmCut_post);

  TH1D * h_mass_plus = new TH1D("h_mass_plus", "h_mass_plus", 500, 1800, 1930);
  TH1D * h_mass_minus = new TH1D("h_mass_minus", "h_mass_minus", 500, 1800, 1930);
  vector <double> values;
  for(Long64_t i = 0; i < 
	//1e6;
	toweight_tree->GetEntries();
      i++){
    toweight_tree->GetEntry(i);

    Iw = h_KS0_PTvsETA->getBC(h_KS0_PTvsETA->find({KS0_PT,KS0_ETA}));
    Iw *=  h_KS0_PHI->getBC(h_KS0_PHI->find({KS0_PHI}));
    if (Iw > 15)
      Iw = 15;

    if (Iw > 0.05) //using Iw just for harm cut (means that D02Kpi has a weight smaller than 20)
      Iw = 1;
    else
      Iw = 0;

    if(Dplus_ID>0) {
      h_mass_plus->Fill(Dplus_M,Iw);
    }
    else {
      h_mass_minus->Fill(Dplus_M,Iw);
    }
    
    t->Fill();
  }
  
  f->Write();
  f->Close();
  
}
