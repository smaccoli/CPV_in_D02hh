#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

int main() {

  dcastyle();
  
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/sept19/Dp2KS0pipLL_16_Dw.root");
  toweight_tree->AddFriend("ntp","rew.root");

  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/sept19/D02Kmpip_16_Dw.root");

  target_tree->GetLeaf("D0_PT")->SetName("KS0_PT");
  target_tree->GetLeaf("D0_ETA")->SetName("KS0_ETA");
  target_tree->GetLeaf("D0_PHI")->SetName("KS0_PHI");
  target_tree->GetLeaf("D0_P")->SetName("KS0_P");
 
  IDecay* toweight_decay = new IDecay(toweight_tree,"Dp2KS0pip","D^{+} #rightarrow K_{S}^{0} h^{+}",{"Iw"},1e6,0);
  IDecay* target_decay = new IDecay(target_tree,"D02Kmpip","D^{0} #rightarrow K^{#minus} #pi^{+}",{},1e6,0);
  
  toweight_decay->addTargetDistribution(target_decay,{"KS0_PT(100,1.7e3,14e3)","KS0_ETA(100,2,4.4)"},5);//5
  toweight_decay->addTargetDistribution(target_decay,{"KS0_PHI(100,-3.1416,3.1416)"},15);
  
  toweight_decay->Iw_max = 15;
  toweight_decay->Iw_max_value = 15;
  toweight_decay->chi2_Ndof_max = 0.4;
  toweight_decay->ApplySmoothing = 0;
  toweight_decay->outFileName = "weights/16_Dw";
  toweight_decay->IWeight();
  
  return 0;
}
