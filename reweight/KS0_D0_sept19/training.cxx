#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TLeaf.h"
#include "TString.h"

#include "TMVA/MethodBase.h"
#include "TMVA/MethodCategory.h"

#if not defined(__CINT__) || defined(__MAKECINT__)
#include "TMVA/Factory.h"
#include "TMVA/DataLoader.h"
#include "TMVA/Tools.h"
#endif

#include<iostream>
using namespace std;
#include<vector>
#include<string>

//SetupProject ROOT 6.06.02

// Settings for the number of signal and background events (only used if !finalVersion). "0" means as many as possible.
TString NumberEventsSgn = "50000";
TString NumberEventsBkg = "50000";


void training(bool finalVersion=false, bool use_sWeights=true, bool compareDifferentMVAs=true) {
  
  TMVA::Tools::Instance();
  cout<<"*............................................................................*"<<endl<<endl;
  cout<<"*                      "<< TString("Kmpip")<<"-training"<<"                                     *"<<endl;
  cout<<"*............................................................................*"<<endl;
  // Output file                                                                   CHANGE ME
  TFile* outputFile = TFile::Open("test.root", "RECREATE");

  // Factory
  TString factoryOptions = "!V:!Silent:Color:Transformations=I;N:AnalysisType=Classification:DrawProgressBar";
  TMVA::Factory *factory = new TMVA::Factory("Kmpip", outputFile, factoryOptions);
  // DataLoader
  TMVA::DataLoader *dataloader=new TMVA::DataLoader("dataset");
  
  std::vector<std::string> var_name = {
    "PT","ETA","PHI"
    //,"PX","PY","PZ"
  };				

  TString particle;
  TString year = "16";
  TString polarity = "Dw";
  
  for(unsigned i=0;i<var_name.size();i++) {
    particle="KS0";
    dataloader->AddVariable(particle+"_"+var_name.at(i), particle+"_"+var_name.at(i), "", 'F');
  }
  
  // Data files and Ntuples
  TChain *chainS = new TChain("ntp");
  chainS->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/sept19/Dp2KS0pipLL_"+year+"_"+polarity+".root",1e6);
  chainS->AddFriend("ntp","rew.root");

  TChain *chainB = new TChain("ntp");
  //chainB->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/sept19/D02Kmpip_"+year+"_"+polarity+".root",6e5);
  chainB->Add("../D02Kpi_to_D2KpipiW_sept19/rew.root",1e6);
  //chainB->Add("../D2Kpipi_to_D2KSpi_sept19/rew.root",1e6);

  dataloader->AddSignalTree(chainS,1.);
  dataloader->AddBackgroundTree(chainB,1.);    
    
  //cuts
  TCut cutsS, cutsB;
   
  if (use_sWeights) {
    cutsS = "";
    cutsB = "";
  }
  else {
  
  }
  
  if (use_sWeights) {
    dataloader->SetSignalWeightExpression("Iw");
    //dataloader->SetBackgroundWeightExpression("Iw");
    dataloader->SetBackgroundWeightExpression("1.0");
  } 
  else {
    dataloader->SetSignalWeightExpression("1.0");
    dataloader->SetBackgroundWeightExpression("1.0");
  }         
  
  // Tell the factory how to use the training and testing events
  if (finalVersion) {
    dataloader->PrepareTrainingAndTestTree( cutsS, cutsB, "nTrain_Signal=0:nTrain_Background=0:nTest_Signal=3000:nTest_Background=3000:SplitMode=Random:NormMode=NumEvents:SplitSeed=500:!V" );
  }
  else {
    dataloader->PrepareTrainingAndTestTree( cutsS, cutsB, "nTrain_Signal=" + NumberEventsSgn + ":nTrain_Background=" + NumberEventsBkg + ":nTest_Signal=" + NumberEventsSgn + ":nTest_Background=" + NumberEventsBkg + ":SplitMode=Random:NormMode=NumEvents:SplitSeed=500:!V" );
  }

  // MVA methods: book BDT as default
  if (!compareDifferentMVAs) {
    factory->BookMethod(dataloader, TMVA::Types::kBDT, "BDT", "!H:!V:NTrees=800:MinNodeSize=2.5%:MaxDepth=2:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20:DoPreselection=False:NegWeightTreatment=PairNegWeightsGlobal" );
  }
  if (compareDifferentMVAs) {
    //1   
    factory->BookMethod(dataloader, TMVA::Types::kBDT, "BDT", "!H:!V:NTrees=800:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20:DoPreselection=False:NegWeightTreatment=PairNegWeightsGlobal" );
    // //    2
    //factory->BookMethod(dataloader, TMVA::Types::kBDT, "BDTG", "!H:!V:NTrees=800:MinNodeSize=2.5%:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.5:nCuts=20:MaxDepth=3:NegWeightTreatment=PairNegWeightsGlobal:SeparationType=GiniIndex" );

    //3
    //    factory->BookMethod(dataloader, TMVA::Types::kTMlpANN, "TMlpANN", "!H:!V:NCycles=200:HiddenLayers=N+1,N:LearningMethod=BFGS:ValidationFraction=0.3" );
    //4 (Pietro)
    //    factory->BookMethod(dataloader, TMVA::Types::kBDT, "BDTG","!H:!V:NTrees=100:BoostType=Grad:Shrinkage=0.10:BaggedSampleFraction=0.5:UseBaggedBoost=True:nCuts=256:MaxDepth=3:NegWeightTreatment=PairNegWeightsGlobal:DoBoostMonitor=True");
    
    }

      
  // Train and test MVAs
  factory->TrainAllMethods();
  factory->TestAllMethods();
    
  // Evaluate performances
  factory->EvaluateAllMethods();
  
  outputFile->Close();
  delete factory;
  delete dataloader;
}


