#include <RooStats/SPlot.h>
#include "TH2D.h"
#include "TROOT.h"
#include <RooDataHist.h>
#include "TTree.h"
#include "TFile.h"
#include "TChain.h"
#include <stdlib.h>
#include <map> 
#include <string>
#include <RooCategory.h>
#include <RooCBShape.h>
#include <RooAddPdf.h>
#include <RooExponential.h>
#include <RooGaussian.h>
#include <RooGenericPdf.h>
#include <RooProdPdf.h>
#include <RooProdPdf.h>
#include <RooPolynomial.h>
#include <RooChi2Var.h>
#include <RooMinuit.h>
#include "TCanvas.h"

 
using namespace std;
using namespace RooFit;
using namespace RooStats;

void fit(TString mode = "Kpipi", TString year="2016", TString pol="MagDown", bool sPlot = true) {

  //observable
  RooRealVar *Dplus_M = new RooRealVar("Dplus_M","Dplus_M", 1820, 1915);
  RooCategory *Dplus_ID = new RooCategory("Dplus_ID", "Dplus_ID");    
  Dplus_ID->defineType("Dplus", +411);
  Dplus_ID->defineType("Dminus",-411);

 

  TString path2 = "Dplus_M_Dp2Kmpippip_rew.root";
  //TString path2 = "Dplus_M_Dp2Kmpippip.root";
  //TString path2 = "Dplus_M_Dp2KS0pipLL.root";
  TFile * f =new TFile(path2);
  /*
  TH2D * h_mass_plus = (TH2D * ) f->Get("h_orig2_M_w_plus");
  TH2D * h_mass_minus = (TH2D * ) f->Get("h_orig2_M_w_minus");
  */
  
  TH2D * h_mass_plus = (TH2D * ) f->Get("h_Dplus");
  TH2D * h_mass_minus = (TH2D * ) f->Get("h_Dminus");

  h_mass_plus->Print();
  h_mass_minus->Print();
  
  RooDataHist * data_h = new RooDataHist("data_h","data_h",*Dplus_M,Index(*Dplus_ID),
					 Import("Dplus",*h_mass_plus),
					 Import("Dminus",*h_mass_minus));
  data_h->Print();


  RooArgSet * obs = new RooArgSet();                         
  obs->add(*Dplus_M);   
  obs->add(*Dplus_ID);   

  //signal 
  RooRealVar * mu = new RooRealVar("mu","mu", 1869, 1860, 1878);
  RooRealVar * sigma1 =  new RooRealVar("sigma1","sigma1", 7, 0, 40);
  RooRealVar * sigma2 =  new RooRealVar("sigma2","sigma2", 12, 0, 40);
  RooRealVar *  sigmaJ = new RooRealVar("sigmaJ","sigmaJ",10,0,30);
  RooRealVar *  deltaJ = new RooRealVar("deltaJ","deltaJ",0.1,-10,10);
  RooRealVar *  gammaJ = new RooRealVar("gammaJ","gammaJ",0.1,-10,10);

  RooGaussian * gauss1_plus = new RooGaussian("gauss1_plus", "gauss1_plus", *Dplus_M, *mu, *sigma1);
  RooGaussian * gauss2_plus = new RooGaussian("gauss2_plus", "gauss2_plus", *Dplus_M, *mu, *sigma2);
  RooAbsPdf* john_plus=new RooGenericPdf("john_plus","deltaJ/(sigmaJ*TMath::Sqrt(2*TMath::ASin(1.)))*TMath::Exp(-0.5*(gammaJ+deltaJ*TMath::ASinH((Dplus_M-mu)/sigmaJ))*(gammaJ+deltaJ*TMath::ASinH((Dplus_M-mu)/sigmaJ)))/TMath::Sqrt(1+(Dplus_M-mu)*(Dplus_M-mu)/(sigmaJ*sigmaJ))",RooArgSet(*Dplus_M,*mu,*sigmaJ,*gammaJ,*deltaJ));   

  RooGaussian * gauss1_minus = new RooGaussian("gauss1_minus", "gauss1_minus", *Dplus_M, *mu, *sigma1);
  RooGaussian * gauss2_minus = new RooGaussian("gauss2_minus", "gauss2_minus", *Dplus_M, *mu, *sigma2);
  RooAbsPdf* john_minus=new RooGenericPdf("john_minus","deltaJ/(sigmaJ*TMath::Sqrt(2*TMath::ASin(1.)))*TMath::Exp(-0.5*(gammaJ+deltaJ*TMath::ASinH((Dplus_M-mu)/sigmaJ))*(gammaJ+deltaJ*TMath::ASinH((Dplus_M-mu)/sigmaJ)))/TMath::Sqrt(1+(Dplus_M-mu)*(Dplus_M-mu)/(sigmaJ*sigmaJ))",RooArgSet(*Dplus_M,*mu,*sigmaJ,*gammaJ,*deltaJ));  

  RooRealVar * f1 = new RooRealVar("f1","f1",0.4,0,1);
  RooRealVar * f2 = new RooRealVar("f2","f2",0.4,0,1);
  RooAddPdf * sig_plus;
  
  if(mode=="KS0Pip")
    sig_plus = new RooAddPdf("sig_plus","sig_plus", RooArgList(*gauss1_plus, *john_plus),
			     RooArgList(*f1));
  else
    sig_plus = new RooAddPdf("sig_plus","sig_plus", RooArgList(*gauss1_plus, *gauss2_plus, *john_plus),
				       RooArgList(*f1,*f2));
  RooAddPdf * sig_minus; 
  if(mode=="KS0Pip")
    sig_minus = new RooAddPdf("sig_minus","sig_minus", RooArgList(*gauss1_minus, *john_minus),
			     RooArgList(*f1));
  else
    sig_minus = new RooAddPdf("sig_minus","sig_minus", RooArgList(*gauss1_minus, *gauss2_minus, *john_minus),RooArgList(*f1,*f2));


  //bkg
  RooRealVar * slp = new RooRealVar("slp","slp", -0.0001, -10, 10);
  RooExponential * bkg_plus = new RooExponential("bkg_plus", "bkg_plus", *Dplus_M, *slp);
  RooExponential * bkg_minus = new RooExponential("bkg_minus", "bkg_minus", *Dplus_M, *slp);
  

  //yields and asymmetries
  RooRealVar * Nsig = new RooRealVar("Nsig", "Nsig", 1.5e7, 0, 1e8);
  RooRealVar * Nbkg = new RooRealVar("Nbkg", "Nbkg", 2e7, 0, 1e8); 

  RooRealVar * Araw_sig= new RooRealVar("Araw_sig","Araw_sig",0,-1.,1.); 
  RooRealVar * Araw_bkg= new RooRealVar("Araw_bkg","Araw_bkg",0,-1.,1.); 

  RooFormulaVar * Nsig_plus= new RooFormulaVar("Nsig_plus","Nsig_plus","@0*(1+@1)/2.",RooArgSet(*Nsig,*Araw_sig)); 
  RooFormulaVar * Nsig_minus= new RooFormulaVar("Nsig_minus","Nsig_minus","@0*(1-@1)/2.",RooArgSet(*Nsig,*Araw_sig)); 
  RooFormulaVar * Nbkg_plus= new RooFormulaVar("Nbkg_plus","Nbkg_plus","@0*(1+@1)/2.",RooArgSet(*Nbkg,*Araw_bkg)); 
  RooFormulaVar * Nbkg_minus= new RooFormulaVar("Nbkg_minus","Nbkg_minus","@0*(1-@1)/2.",RooArgSet(*Nbkg,*Araw_bkg)); 

  RooGenericPdf * tag_plus= new RooGenericPdf("tag_plus","tag_plus","@0==411",RooArgSet(*Dplus_ID));
  RooGenericPdf * tag_minus= new RooGenericPdf("tag_minus","tag_minus","@0==-411",RooArgSet(*Dplus_ID));

  RooAddPdf * pdf_plus_tmp= new RooAddPdf("pdf_plus_tmp","pdf_plus_tmp",RooArgSet(*sig_plus,*bkg_plus),
				      RooArgSet(*Nsig_plus,*Nbkg_plus));
  RooAddPdf * pdf_minus_tmp= new RooAddPdf("pdf_minus_tmp","pdf_minus_tmp",RooArgSet(*sig_minus,*bkg_minus),
				      RooArgSet(*Nsig_minus,*Nbkg_minus));

  RooProdPdf * pdf_plus= new RooProdPdf("pdf_plus","pdf_plus",RooArgSet(*tag_plus,*pdf_plus_tmp));
 RooProdPdf * pdf_minus= new RooProdPdf("pdf_minus","pdf_minus",RooArgSet(*tag_minus,*pdf_minus_tmp));  


  //total PDF
  RooAddPdf * totPDF = new RooAddPdf("totPDF", "totPDF", RooArgSet(*pdf_plus, *pdf_minus));
  
  //params
  RooArgSet * params = totPDF->getParameters(*obs);
  
  if(sPlot)
    params->readFromFile("params_tmp.txt");
  else
    params->readFromFile("param/fit_"+mode+"_"+year+"_"+pol+".txt") ;
  //create RooDataHist

  //  RooDataHist * data_h = new RooDataHist("data_h","data_h",*Dplus_M,*Dplus_ID,histMap);
  data_h->Print();
  RooChi2Var * chi2 = new RooChi2Var("chi2","chi2",*totPDF,*data_h,NumCPU(1),Extended(kTRUE)) ;
  RooMinuit m1(*chi2) ; 
  m1.setVerbose(kTRUE);   
  m1.setPrintLevel(3);
  m1.setEps(1e-16);
  m1.setStrategy(2);  
  m1.migrad();
  m1.hesse(); 
  RooFitResult* result = m1.save();  
  result->Print("v");

  if(sPlot)
    params->writeToFile("params_tmp.txt") ;
  else
    params->writeToFile("param/fit_"+mode+"_"+year+"_"+pol+".txt") ;

  
  TCanvas * c = new TCanvas();
  c->cd();
  TPad*    upperPad_B = new TPad("upperPad_B", "upperPad_B",   .005, .2525, .995, .995);
  TPad*    lowerPad_B = new TPad("lowerPad_B", "lowerPad_B",   .005, .005,  .995, .2475);
  upperPad_B->Draw(); 
  lowerPad_B->Draw();
  upperPad_B->cd(); 
  RooPlot * plot_mass = Dplus_M->frame(); 
  data_h->plotOn(plot_mass);   
  totPDF->plotOn(plot_mass,Components("*bkg*"),LineColor(kBlack),LineStyle(kDashed)); 
  totPDF->plotOn(plot_mass,Components("*sig*"),LineColor(kRed));
  totPDF->plotOn(plot_mass,LineColor(kBlue));
  plot_mass->Draw();

  lowerPad_B->cd(); 
  RooHist * hpull = plot_mass->pullHist(); 
  hpull->SetFillColor(kBlack);
  RooPlot* pulls = Dplus_M->frame();
  pulls->addPlotable(hpull,"BX");
  pulls->Draw("B");

  /*
  if(sPlot) {
    cout << " --- applying sPlot...\n";  

    TChain * ntp = new TChain("ntp");          
    ntp->Add(filePath);  
    TString filePathOld = filePath;
    filePath.ReplaceAll(".root","_Splot.root");

    
    TFile * fSplot = TFile::Open(filePath, "recreate");
    //TTree * ntpOut= ntp->CopyTree("");
    TTree * ntpOut= ntp->CloneTree(-1,"fast");

    params->selectByName("*")->setAttribAll("Constant",kTRUE);
    params->selectByName("N*")->setAttribAll("Constant",kFALSE);
    m1.migrad();  
    m1.hesse();
    RooStats::SPlot *myPlot = new RooStats::SPlot("myPlot","myPlot",ntpOut,totPDF,RooArgSet(*Nsig,*Nbkg),obs); 
    fSplot->WriteTObject( ntpOut, "ntp", "WriteDelete" );
    fSplot->Close();    

    system("mv "+filePath+" "+filePathOld+"");
    cout << " ===> done!\n"; 
    
  }
  */
    
}
