#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void before_after() {
  dcastyle();

  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2Kmpippip.root");
  toweight_tree->AddFriend("ntp","../rew.root");
  TChain* target_tree = new TChain("ntp","ntp");
  target_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/Dp2KS0pipLL.root");
 
  IDecay* toweight_decay = new IDecay(toweight_tree,"Dp2Kmpippip","D^{+} #rightarrow K^{-} #pi^{+} h^{+}",{"Nsig_sw"},toweight_tree->GetEntries()
				      ,0);
  IDecay* target_decay = new IDecay(target_tree,"Dp2KS0pip","D^{+} #rightarrow K_{S}^{0} h^{+}",{"Nsig_sw"},target_tree->GetEntries()
				    ,0);

  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PT(80,3.5e3,14e3)","Dplus_ETA(80,2,4.5)"},10);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PHI(80,-3.1416,3.1416)"},10);
  toweight_decay->addTargetDistribution(target_decay,{"hplus_PT(80,1.5e3,8.5e3)","hplus_ETA(80,1.9,4.7)"},10);
  toweight_decay->addTargetDistribution(target_decay,{"hplus_PHI(80,-3.1416,3.1416)"},10);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PY(80,-14e3,14e3)","hplus_PY(80,-8.5e3,8.5e3)"},5);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PX(80,-14e3,14e3)","hplus_PX(80,-8.5e3,8.5e3)"},5);
  toweight_decay->addTargetDistribution(target_decay,{"Dplus_PZ(80,15e3,250e3)","hplus_PZ(80,5e3,150e3)"},5);
  
  toweight_decay->fillTargets();

  toweight_decay->setIWeight("Iw"); //from rew.root

  toweight_decay->showVariables();
}
