Araw_bkg =  0.0071300 +/- 0.0012709 L(-1 - 1) 
Araw_sig = -0.0116038 +/- 0.00017323 L(-1 - 1) 
Nbkg =  4696294 +/- 18142 L(0 - 1e+08) 
Nsig =  91980934 +/- 23454 L(0 - 1e+08) 
deltaJ =  3.2118 +/- 0.0093024 L(-5 - 5) 
f1 =  0.090040 +/- 0.0012694 L(0 - 1) 
f2 =  0.20791 +/- 0.0022454 L(0 - 1) 
gammaJ = -0.832606 +/- 0.012320 L(-5 - 5) 
mu =  1865.7 +/- 0.060661 L(1860 - 1878) 
sigma1 =  15.232 +/- 0.096088 L(0 - 40) 
sigma2 =  7.1834 +/- 0.034953 L(0 - 40) 
sigmaJ =  20.015 +/- 0.068757 L(0 - +INF) 
slp = -0.00611961 +/- 0.000052721 L(-10 - 10) 
