#include "/home/LHCB-T3/smaccoli/IWeight/IDecay.cxx"

void apply() {

  myH * h_Dp_PTvsETA = new myH("11_0.myH");
  myH * h_hp_PTvsETA = new myH("11_1.myH");
  
  TChain* toweight_tree = new TChain("ntp","ntp");
  toweight_tree->Add("/home/LHCB/smaccoli/CPV_in_D02hh/data/tmp_Dp2Kmpippip.root");
  
  double Dplus_PT;
  toweight_tree->SetBranchAddress("Dplus_PT",&Dplus_PT);
  double Dplus_ETA;
  toweight_tree->SetBranchAddress("Dplus_ETA",&Dplus_ETA);
  double Dplus_PHI;
  toweight_tree->SetBranchAddress("Dplus_PHI",&Dplus_PHI);
  double hplus_PT;
  toweight_tree->SetBranchAddress("hplus_PT",&hplus_PT);
  double hplus_ETA;
  toweight_tree->SetBranchAddress("hplus_ETA",&hplus_ETA);
  double hplus_PHI;
  toweight_tree->SetBranchAddress("hplus_PHI",&hplus_PHI);
 
  
  TFile *f = TFile::Open("rew.root","RECREATE");
  TTree * t = new TTree("ntp","ntp");
  double Iw;
  t->Branch("Iw",&Iw);

  vector <double> values;
  for(Long64_t i = 0; i < toweight_tree->GetEntries(); i++){
    toweight_tree->GetEntry(i);
    
    Iw = h_Dp_PTvsETA->getBC(h_Dp_PTvsETA->find({Dplus_PT,Dplus_ETA,Dplus_PHI}));
    Iw *= h_hp_PTvsETA->getBC(h_hp_PTvsETA->find({hplus_PT,hplus_ETA,hplus_PHI}));
    if (Iw > 20)
      Iw = 0;
    
    t->Fill();
  }
  
  f->Write();
  f->Close();
  
}
