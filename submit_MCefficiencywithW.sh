#!/bin/bash
isPGun=1

DIR=$PWD
mode=D2Kpipi

for year in $(echo 2016); do
    for pol in $(echo MagDown); do
#    for pol in $(echo MagUp); do
	inputLists=$(echo "/home/LHCB-T3/smaccoli/CPV_in_D02hh/lists/Dp2Kmpippip/list_${year}_${pol}_*.dat")

	if [ $isPGun -eq 1 ]
	then
	    inputLists=$(echo "/home/LHCB-T3/smaccoli/CPV_in_D02hh/lists/pGun/Dp2Kmpippip/list_${year}_${pol}_*.dat")
	fi

	for inputList in $(ls -I logs $inputLists); do
	    idList=$(echo $inputList | tr "/" " " | awk '{print $7}' | tr "_" " " | awk '{print $4}' | tr "." " " | awk '{print $1}')
	    if [ $isPGun -eq 1 ]
	    then
		idList=$(echo $inputList | tr "/" " " | awk '{print $8}' | tr "_" " " | awk '{print $4}' | tr "." " " | awk '{print $1}')
	    fi
	    echo $inputList
	    for inputFile in $(cat $inputList); do
		root -l -b -q MCefficiencywithW.cxx+\(\"$inputFile\"\) >> test_withW.txt
	    done
	    
	done
    done
done
