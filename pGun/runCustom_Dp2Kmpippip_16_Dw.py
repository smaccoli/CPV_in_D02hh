import sys, os

mainDir='/home/LHCB-T3/smaccoli/CPV_in_D02hh/pGun'            
evts=20000
#evts=100
year = '2016' #setted in options/*
polarity='MagDown'
polarity_short='Dw'
mode='Dp2Kmpippip'
#for job in range(0,500): 
for job in range(500,1000): 

    JOBID = job
    OUTPUTDIR='/storage/gpfs_data/local/lhcb/users/smaccoli/data/CPV_in_D02hh/pGun/' + mode + '_' + year + '_' + polarity_short + '/'
    if not os.path.exists(OUTPUTDIR):
        os.mkdir(OUTPUTDIR)
    OUTPUTDIR='/storage/gpfs_data/local/lhcb/users/smaccoli/data/CPV_in_D02hh/pGun/' + mode + '_' + year + '_' + polarity_short + '/' + str(JOBID) + '/'
    if not os.path.exists(OUTPUTDIR):
        os.mkdir(OUTPUTDIR)


    ERRDIR = OUTPUTDIR+'err/'
    if not os.path.exists(ERRDIR):
        os.mkdir(ERRDIR)

    OUTDIR = OUTPUTDIR+'out/'
    if not os.path.exists(OUTDIR):
        os.mkdir(OUTDIR)

    out = open('tmp.sh','w')
    out.write('#!/bin/sh\n')
    out.write('export CMTPROJECTPATH=$CMTPROJECTPATH:/home/LHCB/acarbone/cmtuser'+'\n')   
    out.write('cd '+str(OUTPUTDIR)+'\n')
    out.write('mainDir='+mainDir+'\n')
    out.write('JOBID='+str(JOBID)+'\n')
    out.write('export RUNNUMBER='+str(JOBID)+'\n')
    out.write('export EVTMAX='+str(evts)+'\n')
    out.write('export MAINDIR='+str(mainDir)+'\n')
    out.write('polarity='+str(polarity)+'\n')
    out.write('JOBNAME='+str(polarity)+'.'+str(mode)+'.'+str(JOBID)+'\n')
    out.write('cat '+str(mainDir)+'/options/runGauss-'+year+'-' + polarity_short + '.py > runGauss-'+year+'-'+str(JOBID)+'.py'+'\n')
    out.write('echo "GaussGen.RunNumber = '+str(JOBID)+'" >> runGauss-'+year+'-'+str(JOBID)+'.py'+'\n')

    out.write('cd '+str(OUTPUTDIR)+'\n')
    # out.write('rm  out/*'+'\n')
    # out.write('rm  err/*'+'\n')

    out.write('date'+'\n')
    out.write('lb-run Gauss/latest gaudirun.py '+OUTPUTDIR+'runGauss-'+year+'-'+str(JOBID)+'.py ${mainDir}/options/DataType-'+year+'-'+polarity_short+'.py ${mainDir}/options/pGun_'+mode+'.py'+' | gzip > out/log_Gauss.txt.gz \n')
    out.write('lb-run Boole/latest gaudirun.py ${mainDir}/options/runBoole.py ${mainDir}/options/DataType-'+year+'-'+polarity_short+'.py ${mainDir}/options/Gauss-Data.py'+' | gzip > out/log_Boole.txt.gz \n')
    out.write("lb-run Moore/latest gaudirun.py '$APPCONFIGOPTS/L0App/L0AppSimProduction.py' ${mainDir}/options/L0AppTCK-"+year+".py '$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py' ${mainDir}/options/DataType-"+year+".py ${mainDir}/options/runMoore.py ${mainDir}/options/Boole-Data.py"+' | gzip > out/log_Moore.txt.gz \n')
    out.write('lb-run --runtime Brunel/v50r4 RecDev/v19r7 gaudirun.py ${mainDir}/options/runBrunel.py ${mainDir}/options/DataType-'+year+'-'+polarity_short+'.py ${mainDir}/options/Moore-Data.py'+' | gzip > out/log_Brunel.txt.gz \n') #RecDev/v19r7
    out.write('lb-run DaVinci/latest gaudirun.py ${mainDir}/options/runDaVinci_Hlt1TwoTrackMVA.py ${mainDir}/options/DataType-'+year+'-'+polarity_short+'.py ${mainDir}/options/Brunel-Data.py'+' | gzip > out/log_DaVinci_Hlt1TwoTrackMVA.txt.gz \n') #/v41r4p4
    
    out.write('lb-run DaVinci/latest gaudirun.py ${mainDir}/options/runDaVinci_'+mode+'.py ${mainDir}/options/DataType-'+year+'-'+polarity_short+'.py ${mainDir}/options/Brunel-Data.py'+' | gzip > out/log_DaVinci_'+mode+'.txt.gz \n') #/v41r4p4
    out.write('root -l -b -q ${mainDir}/options/runRoot_Hlt1TrackMVA.C\(\\\"'+mode+'\\\"\) \n')
   
    out.write('lb-run DaVinci/latest gaudirun.py ${mainDir}/options/runDaVinci_'+mode+'_forADKpi.py ${mainDir}/options/DataType-'+year+'-'+polarity_short+'.py ${mainDir}/options/Brunel-Data.py'+' | gzip > out/log_DaVinci_'+mode+'_forADKpi.txt.gz \n') #/v41r4p4
    out.write('root -l -b -q ${mainDir}/options/runRoot_Hlt1TrackMVA.C\(\\\"'+mode+'\\\",\\\"_forADKpi\\\"\) \n')
   
    out.write('lb-run DaVinci/latest gaudirun.py ${mainDir}/options/runDaVinci_'+mode+'_ALL.py ${mainDir}/options/DataType-'+year+'-'+polarity_short+'.py ${mainDir}/options/Brunel-Data.py'+' | gzip > out/log_DaVinci_'+mode+'_ALL.txt.gz \n') #/v41r4p4
    out.write('root -l -b -q ${mainDir}/options/runRoot_Hlt1TrackMVA.C\(\\\"'+mode+'\\\",\\\"_ALL\\\"\) \n')
  
    out.write('rm  '+OUTPUTDIR+'runGauss-'+year+'-'+str(JOBID)+'.py Gauss*.root New* Boole* Moore* test_* Brunel*.root pGun_histos.root pGun_DT.root'+'\n')
    out.write('rm  Gauss*'+'\n')
    out.write('rm  Brunel*'+'\n')
    out.write('rm  out/*.gz'+'\n')
    out.write('date'+'\n')
    out.close()
    
    os.system('chmod 755 tmp.sh')
    os.system('qsub -q long -l nodes=1:ppn=3 -N '+mode+'.'+polarity+'.'+str(JOBID)+' -e '+ERRDIR+str(mode)+'_'+str(JOBID)+'.err -o '+OUTDIR+str(mode)+'_'+str(JOBID)+'.out tmp.sh \n')
    #os.system('qsub -l nodes=1:ppn=2 -N '+mode+'.'+polarity+'.'+str(JOBID)+' -e '+ERRDIR+str(mode)+'_'+str(JOBID)+'.err -o '+OUTDIR+str(mode)+'_'+str(JOBID)+'.out tmp.sh \n')
    os.system('sleep 0.003m')

   #lb-run .... | gzip > log.txt
