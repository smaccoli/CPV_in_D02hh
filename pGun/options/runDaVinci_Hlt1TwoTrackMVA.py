from Configurables import DaVinci, GaudiSequencer
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond, mrad

DaVinci().DataType = "2016"

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdAllNoPIDsKaons as kaons
from StandardParticles import StdAllNoPIDsPions as pions

#Hlt1TwoTrackMVA combinations

combineKst2pimpip = CombineParticles() 
combineKst2pimpip.DecayDescriptors = ['K*(892)0 -> pi- pi+', '[K*(892)0 -> pi+ pi+]cc']
combineKst2pimpip.CombinationCut = "(APT > 2000.0)"  
combineKst2pimpip.MotherCut = "DOCACHI2(1,2) < 10.0"
combineKst2pimpip.DaughtersCuts = {"pi+" : "((PT          > 600. * MeV) & (P           > 5000.    * MeV) &   (TRCHI2DOF   < 2.5) &        (TRGHOSTPROB < 0.2) & (BPVIPCHI2() > 4.))",
                                   "pi-" : "((PT          > 600. * MeV) & (P           > 5000.    * MeV) &   (TRCHI2DOF   < 2.5) &        (TRGHOSTPROB < 0.2) & (BPVIPCHI2() > 4.))",
                                   }
selectKst2pimpip = Selection( 'SelectKst2pimpip', Algorithm = combineKst2pimpip, RequiredSelections = [ kaons, pions ] )
sequenceKst2pimpip = SelectionSequence( 'SequenceKst2pimpip', TopSelection = selectKst2pimpip )
combineKst2pimpip = CombineParticles() 


tuples = [{"Name"   : "Kst2pimpip",
           "Inputs" : [sequenceKst2pimpip.outputLocation()],
           "Decay" : "K*(892)0 -> ^pi- ^pi+",
           "DecayBranches" : {
            "Kstar"   : "K*(892)0 -> pi- pi+",
            "piminus"   : "K*(892)0 -> ^pi- pi+",
            "piplus"   : "K*(892)0 -> pi- ^pi+",
            },
           },
          {"Name"   : "Kst2pippip",
           "Inputs" : [sequenceKst2pimpip.outputLocation()],
           "Decay" : "[K*(892)0 -> ^pi+ ^pi+]CC",
           "DecayBranches" : {
            "Kstar"   : "[K*(892)0 -> pi+ pi+]CC",
            "piplus1"   : "[K*(892)0 -> ^pi+ pi+]CC",
            "piplus2"   : "[K*(892)0 -> pi+ ^pi+]CC",
            },
           },
          ]


tupleSeq = GaudiSequencer("TupleSeq")
tupleSeq.ModeOR = True
tupleSeq.ShortCircuit = False
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from Configurables import DecayTreeTuple, TupleToolTrackInfo, TupleToolRecoStats,LoKi__Hybrid__Dict2Tuple, LoKi__Hybrid__DictOfFunctors
from Configurables import TupleToolMCTruth
from DecayTreeTuple.Configuration import *

MvaVars = {'chi2'   : 'VFASPF(VCHI2)',
           'fdchi2' : 'BPVVDCHI2',
            'sumpt'  : 'SUMTREE(PT, ISBASIC, 0.0)',
            'nlt16'  : 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'}

for tuple in tuples:
    tup = DecayTreeTuple(tuple["Name"])
    tup.Inputs = tuple["Inputs"]
    tup.Decay = tuple["Decay"]    
    tup.addBranches(tuple["DecayBranches"])
    
    # tup.InputPrimaryVertices = '/Event/Turbo/Primary'
    #Refit the PV
    tup.ReFitPVs = False
    
    tl = [ "TupleToolGeometry",
           "TupleToolKinematic",
           # "TupleToolPropertime",
           "TupleToolPrimaries",
           "TupleToolPid",
           "TupleToolEventInfo",
           # "TupleToolTrackInfo",
           # "TupleToolRecoStats",
           # "TupleToolTISTOS",
           # "TupleToolTrigger",
           "TupleToolMCBackgroundInfo",
           ]

    tup.ToolList += tl

    TriggerList = [
        'L0HadronDecision',
        'L0MuonDecision',
        # 'Hlt1TrackMVADecision',
        # 'Hlt1TwoTrackMVADecision',
        ]
    
    from Configurables import TupleToolTrigger
    tupleToolTrigger = tup.addTupleTool(TupleToolTrigger, name='TupleToolTrigger')
    tupleToolTrigger.VerboseL0 = True
    # tupleToolTrigger.VerboseHlt1 = True
    # tupleToolTrigger.VerboseHlt2 = False
    # tupleToolTrigger.FillHlt2 = False
    tupleToolTrigger.TriggerList = TriggerList
    
    from Configurables import TupleToolTISTOS
    tupleToolTISTOS = tup.addTupleTool(TupleToolTISTOS, name='TupleToolTISTOS')
    # tupleToolTISTOS.addTool(L0TriggerTisTos())
    # tupleToolTISTOS.addTool(TriggerTisTos())
    tupleToolTISTOS.Verbose = True         
    tupleToolTISTOS.VerboseL0 = True
    # tupleToolTISTOS.VerboseHlt1 = True
    # tupleToolTISTOS.VerboseHlt2 = False
    # tupleToolTISTOS.FillHlt2 = False
    tupleToolTISTOS.TriggerList = TriggerList

    tupleToolTrackInfo = tup.addTupleTool(TupleToolTrackInfo, name='TupleToolTrackInfo')
    tupleToolTrackInfo.Verbose = False
    
    # RecoStats to filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tupleToolRecoStats = tup.addTupleTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tupleToolRecoStats.Verbose=True

    # MC information
    tupleToolMCTruth = tup.addTupleTool(TupleToolMCTruth, name='TupleToolMCTruth')
    tupleToolMCTruth.ToolList =  [ "MCTupleToolHierarchy", "MCTupleToolKinematic",  ]
    
    #Hlt1TwoTrackMVA response
    from MVADictHelpers import *
    addMatrixnetclassifierTuple(tup.Kstar, '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx', MvaVars, Name='Hlt1TwoTrackMVAResponse_Classifier', Keep=True)
    
    ##LIST OF LOKI VARIABLES
    from Configurables import LoKi__Hybrid__TupleTool
    hybridTool = tup.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Hybrid')
    hybridTool.Preambulo +=  ["import math",
                              "Hlt1TrackMVAResponse = switch( ( TRCHI2DOF < %(TrChi2)s) & (TRGHOSTPROB < %(TrGP)s) & (((PT > %(MaxPT)s) & (BPVIPCHI2() > %(MinIPChi2)s)) | ( in_range( %(MinPT)s, PT, %(MaxPT)s) & (log(BPVIPCHI2()) > (%(Param1)s / ((PT / GeV - %(Param2)s) ** 2)  + (%(Param3)s / %(MaxPT)s) * (%(MaxPT)s   - PT) + math.log(%(MinIPChi2)s) ) ) ) )         ,1,0)" % {
            'TrChi2'      : 2.5,
            'TrGP'        : 0.2,
            'MinPT'       : 1000.  * MeV,
            'MaxPT'       : 25000.  * MeV,
            'MinIPChi2'   : 7.4,
            'Param1'      : 1.0,
            'Param2'      : 1.0,
            'Param3'      : 1.1
            # 'GEC'         : 'Loose'
            },
                              "Hlt1TwoTrackMVAResponse_goodVertex = switch( ( HASVERTEX & (VFASPF(VCHI2) < %(VxChi2)s)      &          (in_range(%(MinETA)s,  BPVETA,    %(MaxETA)s))  & (in_range(%(MinMCOR)s, BPVCORRM, %(MaxMCOR)s)) &     (BPVDIRA           > %(MinDirA)s) )                         ,1,0)" % {
            #'P'           :  5000. * MeV,
            #'PT'          :   600. * MeV,
            #'TrGP'        :  0.2,
            #'TrChi2'      :     2.5,
            #'IPChi2'      :     4.,
            'MinMCOR'     :  1000. * MeV,
            'MaxMCOR'     :   1e9  * MeV,
            'MinETA'      :     2.,
            'MaxETA'      :     5.,
            'MinDirA'     :     0.,
            'V0PT'        :  2000. * MeV,
            'VxChi2'      :    10.,
            #     #     # 'Threshold'   :     0.95,
            #     #     # 'MvaVars'     : {'chi2'   : 'VFASPF(VCHI2)',
            #     #     #                  'fdchi2' : 'BPVVDCHI2',
            #     #     #                  'sumpt'  : 'SUMTREE(PT, ISBASIC, 0.0)',
            #     #     #                  'nlt16'  : 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'},
            #     #     # 'Classifier'  : {'Type'   : 'MatrixNet',
            #     #     #                  'File'   : '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx'},
            #     #     # 'GEC'         : 'Loose'
            # ( VALUE('LoKi::Hybrid::DictValue/TwoTrackMVAMatrixNetTool') > 0.95 )
            },
                              ]
    hybridTool.Variables = {
        "ETA" : "ETA",
        "PHI" : "PHI",
        "M12" : "M12",
        "Hlt1TrackMVAResponse" : "Hlt1TrackMVAResponse",
        "Hlt1TwoTrackMVAResponse_goodVertex" : "Hlt1TwoTrackMVAResponse_goodVertex",
        }
     

    # event tuple
    from Configurables import LoKi__Hybrid__EvtTupleTool
    LoKi_EvtTuple = tup.addTupleTool(LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple"))
    LoKi_EvtTuple.VOID_Variables = { 
        "LoKi_nPVs"                : "CONTAINS('Rec/Vertex/Primary')",
        "LoKi_nSpdMult"            : "CONTAINS('Raw/Spd/Digits')",
        "LoKi_nVeloClusters"       : "CONTAINS('Raw/Velo/Clusters')",
        "LoKi_nVeloLiteClusters"   : "CONTAINS('Raw/Velo/LiteClusters')",
        "LoKi_nITClusters"         : "CONTAINS('Raw/IT/Clusters')",
        "LoKi_nTTClusters"         : "CONTAINS('Raw/TT/Clusters')",
        "LoKi_nOThits"             : "CONTAINS('Raw/OT/Times')"
        }   

         
    tupleSeq.Members += [tup]

    

from Configurables import PrintMCTree
pMC = PrintMCTree()
#pMC.ParticleNames = [ "D*(2010)+" , "D*(2010)-" , "D0"]
#pMC.OutputLevel = 2
userAlgos = [ sequenceKst2pimpip,
             tupleSeq]

DaVinci().HistogramFile = 'pGun_histos.root'
DaVinci().TupleFile = 'pGun_Hlt1TwoTrackMVA.root'
DaVinci().UserAlgorithms = userAlgos
#DaVinci().UserAlgorithms = [tupleSeq]
DaVinci().InputType = "DST"
#DaVinci().InputType = "sim"
DaVinci().Simulation = True
DaVinci().PrintFreq = 1000
DaVinci().Lumi = False
from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
