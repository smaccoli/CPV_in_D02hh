'''
import os
if 'MAINDIR' in os.environ:
    pGunDirectory = os.environ['MAINDIR']
if pGunDirectory[-1] != "/":
    pGunDirectory = pGunDirectory+"/"
'''
from Configurables import ParticleGun, MomentumRange, FlatNParticles,  MomentumSpectrum
from GaudiKernel import SystemOfUnits

pgun = ParticleGun()
pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.addTool( FlatNParticles , name = "FlatNParticles" )
pgun.DecayTool = "EvtGenDecay"
pgun.SignalPdgCode = 413 
from GaudiKernel.SystemOfUnits import *

pgun.ParticleGunTool = "MomentumSpectrum"
pgun.addTool( MomentumSpectrum , name = "MomentumSpectrum" )
pgun.MomentumSpectrum.PdgCodes = [ 413 , -413 ]
pgun.MomentumSpectrum.InputFile = "Dst2D0pi_D02Kpi_MomentumSpectrum_16.root" 
pgun.MomentumSpectrum.HistogramPath = "h_pteta"
pgun.MomentumSpectrum.BinningVariables = "pteta"

from Configurables import BeamSpotSmearVertex
pgun.addTool(BeamSpotSmearVertex, name="BeamSpotSmearVertex")
pgun.VertexSmearingTool = "BeamSpotSmearVertex"

pgun.EventType = 413421211


from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay, name = 'EvtGenDecay' )
ToolSvc().EvtGenDecay.UserDecayFile = "Dst2D0pi_D02Kpi.dec"

pgun.GenCutTool = "LoKi::GenCutTool/TightCut"
from Configurables import LoKi__GenCutTool
pgun.addTool ( LoKi__GenCutTool , name = 'TightCut' )
cuts = pgun.TightCut

cuts.Decay = '^[D*(2010)+ ==> ^(D0 ==> ^K- ^pi+) pi+]CC'
cuts.Preambulo += [
    "from LoKiCore.functions import in_range"  ,
    "from GaudiKernel.SystemOfUnits import GeV, MeV"  ,
    "piKP     = GCHILD(GP,('K+' == GABSID )) + GCHILD(GP,('pi-' == GABSID ))" ,
    "piKPT     = GCHILD(GPT,('K+' == GABSID )) + GCHILD(GPT,('pi-' == GABSID ))" ,
    'DV_minus_PV = pow( pow(GFAEVX(GVX,0)-GFAPVX(GVX,0),2) + pow(GFAEVX(GVY,0)-GFAPVX(GVY,0),2) + pow(GFAEVX(GVZ,0)-GFAPVX(GVZ,0),2) , 0.5)',
    'DV_minus_PV_XY = pow( pow(GFAEVX(GVX,0)-GFAPVX(GVX,0),2) + pow(GFAEVX(GVY,0)-GFAPVX(GVY,0),2) , 0.5)',
    'cosTheta_1 = ( ( (GFAEVX(GVX,0)-GFAPVX(GVX,0))*GCHILD(GPX,1) + (GFAEVX(GVY,0)-GFAPVX(GVY,0))*GCHILD(GPY,1) + (GFAEVX(GVZ,0)-GFAPVX(GVZ,0))*GCHILD(GPZ,1) ) / ( DV_minus_PV*GCHILD(GP,1) ) )',
    'sinTheta_1 = pow( 1 - pow( cosTheta_1, 2 ) , 0.5)',
    'IP_1 = DV_minus_PV * sinTheta_1',
    'cosTheta_2 = ( ( (GFAEVX(GVX,0)-GFAPVX(GVX,0))*GCHILD(GPX,2) + (GFAEVX(GVY,0)-GFAPVX(GVY,0))*GCHILD(GPY,2) + (GFAEVX(GVZ,0)-GFAPVX(GVZ,0))*GCHILD(GPZ,2) ) / ( DV_minus_PV*GCHILD(GP,2) ) )',
    'sinTheta_2 = pow( 1 - pow( cosTheta_2, 2 ) , 0.5)',
    'IP_2 = DV_minus_PV * sinTheta_2',
    'D0cuts      = ( GVEV & (GTIME > 0.1 * mm) & ( IP_1 > 0.02 *mm ) & ( IP_2 > 0.02 *mm ) & (DV_minus_PV_XY > 0.25 *mm) )', 
  ]
cuts.Cuts      =    {
    '[pi+]cc'   : " in_range (1 , GETA ,  5.5 ) & ( GPT > 750 * MeV ) & ( GP > 4950 * MeV )" ,
    '[K-]cc'   : " in_range (1 , GETA ,  5.5 ) & ( GPT > 750 * MeV ) & ( GP > 4950 * MeV )" ,
    '[D~0]cc'   : "in_range (1 , GETA ,  5.5 ) & ( piKP > 12000 * MeV ) & (piKPT > 1600 * MeV) & D0cuts",
    '[D*(2010)+]cc'   : "in_range (1 , GETA ,  5.5 ) & in_range( 0.010 , GCHILD(GTHETA,('pi+' == GABSID )) , 0.400 ) & (GCHILD(GPT,('pi+' == GABSID )) > 80 *MeV) & (GCHILD(GP,('pi+' == GABSID )) > 900 *MeV)" ,
    }
