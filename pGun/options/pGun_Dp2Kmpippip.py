import os
if 'MAINDIR' in os.environ:
    pGunDirectory = os.environ['MAINDIR']
if pGunDirectory[-1] != "/":
    pGunDirectory = pGunDirectory+"/"

from Configurables import ParticleGun, MomentumRange, FlatNParticles,  MomentumSpectrum
from GaudiKernel import SystemOfUnits

pgun = ParticleGun()
pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.addTool( FlatNParticles , name = "FlatNParticles" )
pgun.DecayTool = "EvtGenDecay"
pgun.SignalPdgCode = 411 #Dp
from GaudiKernel.SystemOfUnits import *

pgun.ParticleGunTool = "MomentumSpectrum"
pgun.addTool( MomentumSpectrum , name = "MomentumSpectrum" )
pgun.MomentumSpectrum.PdgCodes = [ 411 , -411 ]
pgun.MomentumSpectrum.InputFile = pGunDirectory+"options/Dp2Kmpippip_MomentumSpectrum_16.root" 
pgun.MomentumSpectrum.HistogramPath = "h_Dplus_pxpypz"
pgun.MomentumSpectrum.BinningVariables = "pxpypz"

from Configurables import BeamSpotSmearVertex
pgun.addTool(BeamSpotSmearVertex, name="BeamSpotSmearVertex")
pgun.VertexSmearingTool = "BeamSpotSmearVertex"

pgun.EventType = 411333211

pgun.GenCutTool = "LoKi::GenCutTool/TightCut"
from Configurables import LoKi__GenCutTool
pgun.addTool ( LoKi__GenCutTool , name = 'TightCut' )
cuts = pgun.TightCut

from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay, name = 'EvtGenDecay' )
ToolSvc().EvtGenDecay.UserDecayFile = pGunDirectory+"options/Dp2Kmpippip.dec"

cuts.Decay      = '^[D+ ==> ^K- ^pi+ ^pi+]CC'#D2Kpih
cuts.Preambulo += [
    'from GaudiKernel.SystemOfUnits import MeV ',
    'import math',
    'inAcc      = in_range (1 , GETA ,  5.5 ) & (GPT > 240 * MeV) & (GP > 990 * MeV)',
    'BeamPipeCut = (~((( abs(GPX) < 0.01605*GPZ + 497 *MeV) & ( abs(GPX) > - 0.01397*GPZ + 418 *MeV))&(abs(GPY/GPZ) < 0.02)))',
    'FiducialCuts = ( (~(abs(GPX) > 0.317*(GP - 2400.0 *MeV))) & BeamPipeCut )',
    'DV_minus_PV = pow( pow(GFAEVX(GVX,0)-GFAPVX(GVX,0),2) + pow(GFAEVX(GVY,0)-GFAPVX(GVY,0),2) + pow(GFAEVX(GVZ,0)-GFAPVX(GVZ,0),2) , 0.5)',
    'DV_minus_PV_XY = pow( pow(GFAEVX(GVX,0)-GFAPVX(GVX,0),2) + pow(GFAEVX(GVY,0)-GFAPVX(GVY,0),2) , 0.5)',
    'cosTheta_1 = ( ( (GFAEVX(GVX,0)-GFAPVX(GVX,0))*GCHILD(GPX,1) + (GFAEVX(GVY,0)-GFAPVX(GVY,0))*GCHILD(GPY,1) + (GFAEVX(GVZ,0)-GFAPVX(GVZ,0))*GCHILD(GPZ,1) ) / ( DV_minus_PV*GCHILD(GP,1) ) )',
    'sinTheta_1 = pow( 1 - pow( cosTheta_1, 2 ) , 0.5)',
    'IP_1 = DV_minus_PV * sinTheta_1',
    'IP_base_1 = DV_minus_PV * cosTheta_1 ',
    'Area_1 = IP_1*IP_base_1',
    'cosTheta_2 = ( ( (GFAEVX(GVX,0)-GFAPVX(GVX,0))*GCHILD(GPX,2) + (GFAEVX(GVY,0)-GFAPVX(GVY,0))*GCHILD(GPY,2) + (GFAEVX(GVZ,0)-GFAPVX(GVZ,0))*GCHILD(GPZ,2) ) / ( DV_minus_PV*GCHILD(GP,2) ) )',
    'sinTheta_2 = pow( 1 - pow( cosTheta_2, 2 ) , 0.5)',
    'IP_2 = DV_minus_PV * sinTheta_2',
    'IP_base_2 = DV_minus_PV * cosTheta_2 ',
    'Area_2 = IP_2*IP_base_2',
    'cosTheta_3 = ( ( (GFAEVX(GVX,0)-GFAPVX(GVX,0))*GCHILD(GPX,3) + (GFAEVX(GVY,0)-GFAPVX(GVY,0))*GCHILD(GPY,3) + (GFAEVX(GVZ,0)-GFAPVX(GVZ,0))*GCHILD(GPZ,3) ) / ( DV_minus_PV*GCHILD(GP,3) ) )',
    'sinTheta_3 = pow( 1 - pow( cosTheta_3, 2 ) , 0.5)',
    'IP_3 = DV_minus_PV * sinTheta_3',
    'IP_base_3 = DV_minus_PV * cosTheta_3 ',
    'Area_3 = IP_3*IP_base_3',
    'AreaCut = ( Area_1 * Area_2 * Area_3 > 0.0002 *mm *mm *mm *mm *mm *mm ) ',
    'Dcuts      = ( GVEV & (GTIME > 0.1 * mm) & ( IP_1 > 0.02 *mm ) & ( IP_2 > 0.02 *mm ) & ( IP_3 > 0.05 *mm ) & (DV_minus_PV_XY > 0.25 *mm) )', 
    'CombCuts  =  (GCHILD(GPT,1) + GCHILD(GPT,2) + GCHILD(GPT,3) > 2990 *MeV) & ( GCOUNT( (GPT > 990 * MeV), HepMC.descendants ) >= 1 ) & ( GCOUNT( (GPT > 390 * MeV), HepMC.descendants ) >= 2 )',
    'HarmonizationCuts = ( GCHILD(GPT,1) > 750 *MeV ) & ( GCHILD(GPT,2) > 750 *MeV ) & ( GCHILD(GP,1) > 4950 *MeV ) & ( GCHILD(GP,2) > 4950 *MeV )',
]
cuts.Cuts       = {
    '[pi+]cc'         : 'inAcc',# & FiducialCuts',
    '[K+]cc'          : 'inAcc',# & BeamPipeCut',
    '[D+]cc'        : 'Dcuts & HarmonizationCuts',# & # & AreaCut
    }

#you can add a GenLevCut on the hplus (which is the higher momentum pion)

#pgun.OutputLevel = 2

  
