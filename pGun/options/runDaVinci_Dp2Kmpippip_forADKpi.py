from Configurables import DaVinci, GaudiSequencer
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond, mrad

DaVinci().DataType = "2016"

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdAllNoPIDsKaons as kaons
from StandardParticles import StdAllNoPIDsPions as pions

combineDp2Kmpippip = CombineParticles()
combineDp2Kmpippip.DecayDescriptor = "[D+ -> K- pi+ pi+]cc" 

#Hlt2 Turbo line:
#Hlt2CharmHadDpToKmPipPip_ForKPiAsymTurbo

combineDp2Kmpippip.DaughtersCuts = { 'K-' : '(TRCHI2DOF < 3.0 )& (PT > 200.0)& (P > 1000.0)& (MIPCHI2DV(PRIMARY) > 25.0)' ,
                                     'pi+' : '(TRCHI2DOF < 3.0 )& (PT > 200.0)& (P > 1000.0)& (MIPCHI2DV(PRIMARY) > 25.0)'
                                    }


combineDp2Kmpippip.CombinationCut = '(in_range( 1709.0, AM, 2029.0 )) & ((APT1+APT2+APT3) > 1000.0 ) & (AHASCHILD(PT > 200.0)) & (ANUM(PT > 200.0) >= 2) & (AHASCHILD((MIPCHI2DV(PRIMARY)) > 100.0)) & (ANUM(MIPCHI2DV(PRIMARY) > 49.0) >= 2)'

combineDp2Kmpippip.MotherCut = '(VFASPF(VCHI2PDOF) < 10.0) & (BPVDIRA > lcldira ) & (BPVLTIME() > 0.0005 )'

combineDp2Kmpippip.Preambulo = [ "import math", 'lcldira = math.cos( 0.0141 )' ]


selectDp2Kmpippip = Selection( 'SelectDp2Kmpippip', Algorithm = combineDp2Kmpippip, RequiredSelections = [ kaons, pions ] )
sequenceDp2Kmpippip = SelectionSequence( 'SequenceDp2Kmpippip', TopSelection = selectDp2Kmpippip )

tuples = [{"Name"   : "Dp2Kmpippip",
           "Inputs" : [sequenceDp2Kmpippip.outputLocation()],
           "Decay"  : "[D+ -> ^K-  ^pi+ ^pi+]CC",
           "DecayBranches" : { 
            "Dplus"   : "[D+ -> K-  pi+ pi+]CC",
            "Kminus"   : "[D+ -> ^K-  pi+ pi+]CC",
            "piplus"  : "[D+ -> K-  ^pi+ pi+]CC",
            "hplus"  : "[D+ -> K-  pi+ ^pi+]CC",
            },
           }]

mcTuples = [{"Name"   : "Dp2Kmpippip",
             "Decay"  : "[D+ ==> ^K-  ^pi+ ^pi+]CC",
             "DecayBranches" : {
            "Dplus"   : "[D+ ==> K-  pi+ pi+]CC",
            "Kminus"   : "[D+ ==> ^K-  pi+ pi+]CC",
            "piplus"  : "[D+ ==> K-  ^pi+ pi+]CC",
            "hplus"  : "[D+ ==> K-  pi+ ^pi+]CC"
            },
             }]


tupleSeq = GaudiSequencer("TupleSeq")
tupleSeq.ModeOR = True
tupleSeq.ShortCircuit = False
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from Configurables import DecayTreeTuple, TupleToolTrackInfo, TupleToolRecoStats,LoKi__Hybrid__Dict2Tuple, LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import TupleToolMCTruth
from DecayTreeTuple.Configuration import *

LoKi_Dplus2PhiPi=LoKi__Hybrid__TupleTool("LoKi_Dplus2PhiPi")
LoKi_Dplus2PhiPi.Variables =  {
    'DOCACHI2_Kminus_piplus' : 'DOCACHI2(1,2)',
    'DOCA_Kminus_piplus' : 'DOCA(1,2)',
    'DOCACHI2_Kminus_hplus' : 'DOCACHI2(1,3)',
    'DOCA_Kminus_plus' : 'DOCA(1,3)',
    'DOCACHI2_piplus_hplus' : 'DOCACHI2(2,3)',
    'DOCA_piplus_hplus' : 'DOCA(2,3)',
    }

for tuple in tuples:
    tup = DecayTreeTuple(tuple["Name"])
    tup.Inputs = tuple["Inputs"]
    tup.Decay = tuple["Decay"]    
    tup.addBranches(tuple["DecayBranches"])
    
    # tup.InputPrimaryVertices = '/Event/Turbo/Primary'
    #Refit the PV
    tup.ReFitPVs = False
    
    tl = [ "TupleToolGeometry",
           "TupleToolKinematic",
           "TupleToolPropertime",
           "TupleToolPrimaries",
           "TupleToolPid",
           "TupleToolEventInfo",
           # "TupleToolTrackInfo",
           # "TupleToolRecoStats",
           # "TupleToolTISTOS",
           # "TupleToolTrigger",
           "TupleToolMCBackgroundInfo",
           ]

    tup.ToolList += tl

    TriggerList = [
        'L0HadronDecision',
        'L0MuonDecision',
        # 'Hlt1TrackMVADecision',
        # 'Hlt1TwoTrackMVADecision',
        ]
    
    from Configurables import TupleToolTrigger
    tupleToolTrigger = tup.addTupleTool(TupleToolTrigger, name='TupleToolTrigger')
    tupleToolTrigger.VerboseL0 = True
    # tupleToolTrigger.VerboseHlt1 = True
    # tupleToolTrigger.VerboseHlt2 = False
    # tupleToolTrigger.FillHlt2 = False
    tupleToolTrigger.TriggerList = TriggerList
    
    from Configurables import TupleToolTISTOS
    tupleToolTISTOS = tup.addTupleTool(TupleToolTISTOS, name='TupleToolTISTOS')
    # tupleToolTISTOS.addTool(L0TriggerTisTos())
    # tupleToolTISTOS.addTool(TriggerTisTos())
    tupleToolTISTOS.Verbose = True         
    tupleToolTISTOS.VerboseL0 = True
    # tupleToolTISTOS.VerboseHlt1 = True
    # tupleToolTISTOS.VerboseHlt2 = False
    # tupleToolTISTOS.FillHlt2 = False
    tupleToolTISTOS.TriggerList = TriggerList

    tupleToolTrackInfo = tup.addTupleTool(TupleToolTrackInfo, name='TupleToolTrackInfo')
    tupleToolTrackInfo.Verbose = False
    
    # RecoStats to filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tupleToolRecoStats = tup.addTupleTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tupleToolRecoStats.Verbose=True

    # MC information
    tupleToolMCTruth = tup.addTupleTool(TupleToolMCTruth, name='TupleToolMCTruth')
    tupleToolMCTruth.ToolList =  [ "MCTupleToolHierarchy", "MCTupleToolKinematic",  ]

    ##LIST OF LOKI VARIABLES
    from Configurables import LoKi__Hybrid__TupleTool
    hybridTool = tup.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Hybrid')
    hybridTool.Preambulo +=  ["import math",
                              "Hlt1TrackMVAResponse = switch( ( TRCHI2DOF < %(TrChi2)s) & (TRGHOSTPROB < %(TrGP)s) & (((PT > %(MaxPT)s) & (BPVIPCHI2() > %(MinIPChi2)s)) | ( in_range( %(MinPT)s, PT, %(MaxPT)s) & (log(BPVIPCHI2()) > (%(Param1)s / ((PT / GeV - %(Param2)s) ** 2)  + (%(Param3)s / %(MaxPT)s) * (%(MaxPT)s   - PT) + math.log(%(MinIPChi2)s) ) ) ) )         ,1,0)" % {
            'TrChi2'      : 2.5,
            'TrGP'        : 0.2,
            'MinPT'       : 1000.  * MeV,
            'MaxPT'       : 25000.  * MeV,
            'MinIPChi2'   : 7.4,
            'Param1'      : 1.0,
            'Param2'      : 1.0,
            'Param3'      : 1.1
            # 'GEC'         : 'Loose'
            },
                              ]
    hybridTool.Variables = {
        "ETA" : "ETA",
        "PHI" : "PHI",
        "M12" : "M12",
        "Hlt1TrackMVAResponse" : "Hlt1TrackMVAResponse",
        }
    tup.Dplus.addTool(LoKi_Dplus2PhiPi)
    tup.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2PhiPi"]
  
    # event tuple
    from Configurables import LoKi__Hybrid__EvtTupleTool
    LoKi_EvtTuple = tup.addTupleTool(LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple"))
    LoKi_EvtTuple.VOID_Variables = { 
        "LoKi_nPVs"                : "CONTAINS('Rec/Vertex/Primary')",
        "LoKi_nSpdMult"            : "CONTAINS('Raw/Spd/Digits')",
        "LoKi_nVeloClusters"       : "CONTAINS('Raw/Velo/Clusters')",
        "LoKi_nVeloLiteClusters"   : "CONTAINS('Raw/Velo/LiteClusters')",
        "LoKi_nITClusters"         : "CONTAINS('Raw/IT/Clusters')",
        "LoKi_nTTClusters"         : "CONTAINS('Raw/TT/Clusters')",
        "LoKi_nOThits"             : "CONTAINS('Raw/OT/Times')"
        }   

    
#DECAY TREE FITTER                                              
    tup.addTool(TupleToolDecay, name="Dplus")
#constrain only PV                                              
    tup.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
    tup.Dplus.DTFonlyPV.constrainToOriginVertex = True
    tup.Dplus.DTFonlyPV.Verbose = True
    tup.Dplus.DTFonlyPV.UpdateDaughters = True
#DTF simple (with only Momentum Scaling ?)                                
    tup.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMS')
    tup.Dplus.DTFonlyMS.Verbose = True
    tup.Dplus.DTFonlyMS.UpdateDaughters = True
#constrain Dp mass                                   
    tup.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyM')
    # tup.Dplus.DTFonlyM.constrainToOriginVertex = True
    tup.Dplus.DTFonlyM.Verbose = True
    tup.Dplus.DTFonlyM.daughtersToConstrain = ['D+']
    tup.Dplus.DTFonlyM.UpdateDaughters = True
    

    #Hlt1TwoTrackMVA requirement
    # from MVADictHelpers import *
    # addMatrixnetclassifierTuple(tup.Dplus, '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx', MvaVars, Name='MVAResponse', Keep=True)


    tupleSeq.Members += [tup]

    
for tuple in mcTuples:
    mctuple = MCDecayTreeTuple( "MC" + tuple["Name"] )
    mctuple.Decay = tuple["Decay"]
    mctuple.addBranches(tuple["DecayBranches"])
    mctuple.ToolList = [ "MCTupleToolKinematic", "MCTupleToolReconstructed", "MCTupleToolDecayType", "MCTupleToolHierarchy", "MCTupleToolPID", "LoKi::Hybrid::MCTupleTool/LoKi_Photos" ]
#    mctuple.OutputLevel = 1
    from Configurables import LoKi__Hybrid__MCTupleTool
    hybridTool = mctuple.addTupleTool('LoKi::Hybrid::MCTupleTool/LoKi_MCHybrid')
    hybridTool.Variables = {
        "ETA" : "MCETA",
        "PHI" : "MCPHI"
        }

    
    tupleSeq.Members += [mctuple]

from Configurables import PrintMCTree
pMC = PrintMCTree()
#pMC.ParticleNames = [ "D*(2010)+" , "D*(2010)-" , "D0"]
#pMC.OutputLevel = 1
#userAlgos = [sequenceDp2Kmpippip, sequenceDStPiToDp2Kmpippip, tupleSeq]
userAlgos = [sequenceDp2Kmpippip, tupleSeq]

DaVinci().HistogramFile = 'pGun_histos.root'
DaVinci().TupleFile = 'pGun_Dp2Kmpippip_forADKpi.root'
DaVinci().UserAlgorithms = userAlgos
DaVinci().InputType = "DST"
DaVinci().Simulation = True
DaVinci().PrintFreq = 1000
DaVinci().Lumi = False
from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

