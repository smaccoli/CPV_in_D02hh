from Configurables import DaVinci, GaudiSequencer
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond, mrad

DaVinci().DataType = "2016"

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdAllNoPIDsKaons as kaons
from StandardParticles import StdAllNoPIDsPions as pions
from StandardParticles import StdAllLooseMuons as muons

#Hlt2 Turbo line:

combineD02Kmpip = CombineParticles()
combineD02Kmpip.DecayDescriptor = "[D0 -> K- pi+]cc" 
combineD02Kmpip.DaughtersCuts = {"K-" : "ALL",
                                 "pi+" : "ALL",
                                 }
combineD02Kmpip.CombinationCut = "in_range(1615.0,  AM, 2115.0)"
combineD02Kmpip.MotherCut = "ALL"
selectD02Kmpip = Selection( 'SelectD02Kmpip', Algorithm = combineD02Kmpip, RequiredSelections = [ pions,kaons ] )
sequenceD02Kmpip = SelectionSequence( 'SequenceD02Kmpip', TopSelection = selectD02Kmpip )

combineDst2D0pi = CombineParticles()
combineDst2D0pi.DecayDescriptor = "[D*(2010)+ -> D0 pi+]cc" 
combineDst2D0pi.Preambulo = [ "import math" ]
combineDst2D0pi.DaughtersCuts = { "D0" : "ALL",
                                     "pi+" : "ALL",
                                     }
combineDst2D0pi.CombinationCut = "in_range(1760.0,  AM, 2260.0)"
combineDst2D0pi.MotherCut = "ALL"
selectDst2D0pi = Selection( 'SelectDst2D0pi', Algorithm = combineDst2D0pi, RequiredSelections = [ selectD02Kmpip, pions ] )
sequenceDst2D0pi = SelectionSequence( 'SequenceDst2D0pi', TopSelection = selectDst2D0pi )

tuples = [
    {"Name"   : "Dstp2D0pi",
     "Inputs" : [sequenceDst2D0pi.outputLocation()],
     "Decay" : "[D*(2010)+ -> ^(D0 -> ^K- ^pi+) ^pi+]CC",
     "DecayBranches" : {
            "Dst"   : "[D*(2010)+ -> (D0 -> K- pi+) pi+]CC",
            "D0"   : "[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC",
            "sPi"   : "[D*(2010)+ -> (D0 -> K- pi+) ^pi+]CC",
            "P2"   : "[D*(2010)+ -> (D0 -> ^K- pi+) pi+]CC",
            "P1"   : "[D*(2010)+ -> (D0 -> K- ^pi+) pi+]CC",
            },
     },
    ]

mcTuples = [
    {"Name"   : "Dstp2D0pi",
     "Decay" : "[D*(2010)+ ==> ^(D0 ==> ^K- ^pi+) ^pi+]CC",
     "DecayBranches" : {
            "Dst"   : "[D*(2010)+ ==> (D0 ==> K- pi+) pi+]CC",
            "D0"   : "[D*(2010)+ ==> ^(D0 ==> K- pi+) pi+]CC",
            "sPi"   : "[D*(2010)+ ==> (D0 ==> K- pi+) ^pi+]CC",
            "P2"   : "[D*(2010)+ ==> (D0 ==> ^K- pi+) pi+]CC",
            "P1"   : "[D*(2010)+ ==> (D0 ==> K- ^pi+) pi+]CC",
            },
     },
    ]


tupleSeq = GaudiSequencer("TupleSeq")
tupleSeq.ModeOR = True
tupleSeq.ShortCircuit = False
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from Configurables import DecayTreeTuple, TupleToolTrackInfo, TupleToolRecoStats,LoKi__Hybrid__Dict2Tuple, LoKi__Hybrid__DictOfFunctors
from Configurables import TupleToolMCTruth
from DecayTreeTuple.Configuration import *

MvaVars = {'chi2'   : 'VFASPF(VCHI2)',
           'fdchi2' : 'BPVVDCHI2',
            'sumpt'  : 'SUMTREE(PT, ISBASIC, 0.0)',
            'nlt16'  : 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'}

for tuple in tuples:
    tup = DecayTreeTuple(tuple["Name"])
    tup.Inputs = tuple["Inputs"]
    tup.Decay = tuple["Decay"]    
    tup.addBranches(tuple["DecayBranches"])
    
    # tup.InputPrimaryVertices = '/Event/Turbo/Primary'
    #Refit the PV
    tup.ReFitPVs = False
    
    tl = [ "TupleToolGeometry",
           "TupleToolKinematic",
           "TupleToolPropertime",
           "TupleToolPrimaries",
           "TupleToolPid",
           "TupleToolEventInfo",
           #"TupleToolTrackInfo",
           # "TupleToolRecoStats",
           # "TupleToolTISTOS",
           # "TupleToolTrigger",
           "TupleToolMCBackgroundInfo",
           ]

    tup.ToolList += tl

    TriggerList = [
        'L0HadronDecision',
        'L0MuonDecision',
        # 'Hlt1TrackMVADecision',
        # 'Hlt1TwoTrackMVADecision',
        ]
    
    from Configurables import TupleToolTrigger
    tupleToolTrigger = tup.addTupleTool(TupleToolTrigger, name='TupleToolTrigger')
    tupleToolTrigger.VerboseL0 = True
    # tupleToolTrigger.VerboseHlt1 = True
    # tupleToolTrigger.VerboseHlt2 = False
    # tupleToolTrigger.FillHlt2 = False
    tupleToolTrigger.TriggerList = TriggerList
    
    from Configurables import TupleToolTISTOS
    tupleToolTISTOS = tup.addTupleTool(TupleToolTISTOS, name='TupleToolTISTOS')
    # tupleToolTISTOS.addTool(L0TriggerTisTos())
    # tupleToolTISTOS.addTool(TriggerTisTos())
    tupleToolTISTOS.Verbose = True         
    tupleToolTISTOS.VerboseL0 = True
    # tupleToolTISTOS.VerboseHlt1 = True
    # tupleToolTISTOS.VerboseHlt2 = False
    # tupleToolTISTOS.FillHlt2 = False
    tupleToolTISTOS.TriggerList = TriggerList

    tupleToolTrackInfo = tup.addTupleTool(TupleToolTrackInfo, name='TupleToolTrackInfo')
    tupleToolTrackInfo.Verbose = True
    
    # RecoStats to filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tupleToolRecoStats = tup.addTupleTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tupleToolRecoStats.Verbose=True

    # MC information
    tupleToolMCTruth = tup.addTupleTool(TupleToolMCTruth, name='TupleToolMCTruth')
    tupleToolMCTruth.ToolList =  [ "MCTupleToolHierarchy", "MCTupleToolKinematic",  ]
     
    ##LIST OF LOKI VARIABLES
    from Configurables import LoKi__Hybrid__TupleTool
    hybridTool = tup.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Hybrid')
    hybridTool.Preambulo +=  ["import math",
                              "Hlt1TrackMVAResponse = switch( ( TRCHI2DOF < %(TrChi2)s) & (TRGHOSTPROB < %(TrGP)s) & (((PT > %(MaxPT)s) & (BPVIPCHI2() > %(MinIPChi2)s)) | ( in_range( %(MinPT)s, PT, %(MaxPT)s) & (log(BPVIPCHI2()) > (%(Param1)s / ((PT / GeV - %(Param2)s) ** 2)  + (%(Param3)s / %(MaxPT)s) * (%(MaxPT)s   - PT) + math.log(%(MinIPChi2)s) ) ) ) )         ,1,0)" % {
            'TrChi2'      : 2.5,
            'TrGP'        : 0.2,
            'MinPT'       : 1000.  * MeV,
            'MaxPT'       : 25000.  * MeV,
            'MinIPChi2'   : 7.4,
            'Param1'      : 1.0,
            'Param2'      : 1.0,
            'Param3'      : 1.1
            # 'GEC'         : 'Loose'
            },
                              
                              ]
    hybridTool.Variables = {
        #"ET" : "TransverseEnergy()",
        #"PE" : "PE",
        "ETA" : "ETA",
        "PHI" : "PHI",
        "Hlt1TrackMVAResponse" : "Hlt1TrackMVAResponse",
        }
     

    # event tuple
    from Configurables import LoKi__Hybrid__EvtTupleTool
    LoKi_EvtTuple = tup.addTupleTool(LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple"))
    LoKi_EvtTuple.VOID_Variables = { 
        "LoKi_nPVs"                : "CONTAINS('Rec/Vertex/Primary')",
        "LoKi_nSpdMult"            : "CONTAINS('Raw/Spd/Digits')",
        "LoKi_nVeloClusters"       : "CONTAINS('Raw/Velo/Clusters')",
        "LoKi_nVeloLiteClusters"   : "CONTAINS('Raw/Velo/LiteClusters')",
        "LoKi_nITClusters"         : "CONTAINS('Raw/IT/Clusters')",
        "LoKi_nTTClusters"         : "CONTAINS('Raw/TT/Clusters')",
        "LoKi_nOThits"             : "CONTAINS('Raw/OT/Times')"
        }   

    
# #DECAY TREE FITTER                                              
    tup.addTool(TupleToolDecay, name="Dst")
#DTF simple (with only Momentum Scaling)                                
    tup.Dst.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMS')
    tup.Dst.DTFonlyMS.Verbose = True
    tup.Dst.DTFonlyMS.UpdateDaughters = True
#constrain only PV                                              
    tup.Dst.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
    tup.Dst.DTFonlyPV.constrainToOriginVertex = True
    tup.Dst.DTFonlyPV.Verbose = True
    tup.Dst.DTFonlyPV.UpdateDaughters = True
#constrain D0 mass                                   
    tup.Dst.addTupleTool('TupleToolDecayTreeFitter/DTFonlyM')
    tup.Dst.DTFonlyM.Verbose = True
    tup.Dst.DTFonlyM.daughtersToConstrain = ['D0']
    tup.Dst.DTFonlyM.UpdateDaughters = True
#all the constraints                                   
    tup.Dst.addTupleTool('TupleToolDecayTreeFitter/DTF')
    tup.Dst.DTFonlyM.constrainToOriginVertex = True
    tup.Dst.DTFonlyM.Verbose = True
    tup.Dst.DTFonlyM.daughtersToConstrain = ['D0']
    tup.Dst.DTFonlyM.UpdateDaughters = True
     
     
    tupleSeq.Members += [tup]

    
for tuple in mcTuples:
    mctuple = MCDecayTreeTuple( "MC" + tuple["Name"] )
    mctuple.Decay = tuple["Decay"]
    mctuple.addBranches(tuple["DecayBranches"])
    mctuple.ToolList = [ "MCTupleToolKinematic", "MCTupleToolReconstructed", "MCTupleToolDecayType", "MCTupleToolHierarchy", "MCTupleToolPID", "LoKi::Hybrid::MCTupleTool/LoKi_Photos" ,"TupleToolEventInfo","MCTupleToolPrimaries"]
#    mctuple.OutputLevel = 1
    from Configurables import LoKi__Hybrid__MCTupleTool
    hybridTool = mctuple.addTupleTool('LoKi::Hybrid::MCTupleTool/LoKi_MCHybrid')
    hybridTool.Variables = {
        "ETA" : "MCETA",
        "PHI" : "MCPHI",
        # GeneratorLevelCuts (-> for checks)
        "THETA" : "MCTHETA",
        #"M12" : "MCM(1,2)",
        "PT" : "MCPT",
        }

    
    tupleSeq.Members += [mctuple]

from Configurables import PrintMCTree
pMC = PrintMCTree()
#pMC.ParticleNames = [ "D*(2010)+" , "D*(2010)-" , "D0"]
#pMC.OutputLevel = 2
userAlgos = [ sequenceD02Kmpip,sequenceDst2D0pi,
              tupleSeq]

DaVinci().HistogramFile = 'pGun_histos.root'
DaVinci().TupleFile = 'pGun_Dst2D0pi_D02Kpi_ALL.root'
DaVinci().UserAlgorithms = userAlgos
#DaVinci().UserAlgorithms = [tupleSeq]
DaVinci().InputType = "DST"
#DaVinci().InputType = "sim"
DaVinci().Simulation = True
DaVinci().PrintFreq = 1
DaVinci().Lumi = False
from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

