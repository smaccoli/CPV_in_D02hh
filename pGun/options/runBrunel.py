from Configurables import Brunel
from Configurables import GaudiSequencer
#Brunel().DDDBtag = "dddb-20150724"
#Brunel().CondDBtag = "sim-20161124-2-vc-md100"
Brunel().DataType = "2016"
#from Configurables import RootCnvSvc
#RootCnvSvc().GlobalCompression = "ZLIB:1"
from Configurables import Brunel
from Configurables import RecSysConf
RecSysConf().SpecialData = ["pGun"]
from Configurables import PGPrimaryVertex
pgPV = PGPrimaryVertex()
GaudiSequencer("RecoVertexSeq").Members = [ pgPV ]
Brunel().InputType = "DIGI" # implies also Brunel().Simulation = True
Brunel().WithMC    = True   # implies also Brunel().Simulation = True
Brunel().Simulation    = True
Brunel().SpecialData += ["pGun"]

Brunel().PrintFreq = 1000

from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False 
