from Configurables import DaVinci, GaudiSequencer
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond, mrad

DaVinci().DataType = "2016"

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdAllNoPIDsKaons as kaons
from StandardParticles import StdAllNoPIDsPions as pions

#Hlt2 Turbo line:

combineKS02pimpipLL = CombineParticles()
combineKS02pimpipLL.DecayDescriptor = "KS0 -> pi+ pi-" 
combineKS02pimpipLL.DaughtersCuts = {"pi+" : "(TRCHI2DOF<3.)& (MIPCHI2DV(PRIMARY)>36)",
                                     "pi-" : "(TRCHI2DOF<3.)& (MIPCHI2DV(PRIMARY)>36)",}
combineKS02pimpipLL.CombinationCut = "(ADAMASS('KS0')<50*MeV)"
combineKS02pimpipLL.MotherCut = "(ADMASS('KS0')<35*MeV) & (VFASPF(VCHI2PDOF)<30) & (BPVLTIME() > 2.0*ps)"

selectKS02pimpipLL = Selection( 'SelectKS02pimpipLL', Algorithm = combineKS02pimpipLL, RequiredSelections = [ pions ] )
sequenceKS02pimpipLL = SelectionSequence( 'SequenceKS02pimpipLL', TopSelection = selectKS02pimpipLL )

combineDp2KS0KpLL = CombineParticles()
combineDp2KS0KpLL.DecayDescriptor = "[D+ -> KS0 K+]cc" 
combineDp2KS0KpLL.Preambulo = [ "import math" ]
combineDp2KS0KpLL.DaughtersCuts = { "KS0" : "(BPVLTIME() > %(DecayTime_MIN)s) & (VFASPF(VZ) > %(VZ_MIN)s) & (VFASPF(VZ) < %(VZ_MAX)s)" % {
        'DecayTime_MIN': 0.5 * picosecond,
        'VZ_MAX': 500.0 * mm,
        'VZ_MIN': -100.0 * mm,
        },
                                     "K+" : " (PIDK > 5) & (P > 1000 *MeV) & (PT > %(Trk_ALL_PT_MIN)s) & (MIPCHI2DV(PRIMARY) > %(Trk_ALL_MIPCHI2DV_MIN)s) & (TRCHI2DOF < 3.0 )" % {
        'Trk_ALL_MIPCHI2DV_MIN'    :  36.0,
        'Trk_ALL_PT_MIN'           :  200.0 * MeV,
        },
                                     }
combineDp2KS0KpLL.CombinationCut = "(in_range( %(AM_MIN)s, AM, %(AM_MAX)s )) & ((APT1+APT2) > %(ASUMPT_MIN)s )" % {
    'AM_MIN'                   :  1679 * MeV,
    'AM_MAX'                   :  2159 * MeV, 
    'ASUMPT_MIN'               :  2000 * MeV,
    }

combineDp2KS0KpLL.MotherCut = "(VFASPF(VCHI2PDOF)<10) & (BPVDIRA > math.cos( %(acosBPVDIRA_MAX)s ) ) & (BPVLTIME() > %(BPVLTIME_MIN)s )  & (BPVVDCHI2 > %(BPVVDCHI2_MIN)s ) & in_range( 1689.0 , M , 2149.0) " % {
    'acosBPVDIRA_MAX'          :  17.3 * mrad,
    'BPVLTIME_MIN'             :  0.25 * picosecond,
    'BPVVDCHI2_MIN'            :  30.0,
    }

selectDp2KS0KpLL = Selection( 'SelectDp2KS0KpLL', Algorithm = combineDp2KS0KpLL, RequiredSelections = [ selectKS02pimpipLL, pions ] )
sequenceDp2KS0KpLL = SelectionSequence( 'SequenceDp2KS0KpLL', TopSelection = selectDp2KS0KpLL )

tuples = [{"Name"   : "Dp2KS0KpLL",
           "Inputs" : [sequenceDp2KS0KpLL.outputLocation()],
           "Decay" : "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^K+]CC",
           "DecayBranches" : {
            "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  K+]CC",
            "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  K+]CC",
            "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  K+]CC",
            "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  K+]CC",
            "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^K+]CC",
            },
           }]

mcTuples = [{"Name"   : "Dp2KS0KpLL",
           "Decay" : "[D+ ==> ^(KS0 ==> ^pi+ ^pi-) ^K+]CC",
           "DecayBranches" : {
            "Dplus"   : "[D+ ==> (KS0 ==> pi+ pi-)  K+]CC",
            "KS0"     : "[D+ ==> ^(KS0 ==>  pi+  pi-)  K+]CC",
            "piplus"  : "[D+ ==> ( KS0 ==> ^pi+  pi-)  K+]CC",
            "piminus" : "[D+ ==> ( KS0 ==>  pi+ ^pi-)  K+]CC",
            "hplus" : "[D+ ==> ( KS0 ==>  pi+  pi-) ^K+]CC"
            },
             }]


tupleSeq = GaudiSequencer("TupleSeq")
tupleSeq.ModeOR = True
tupleSeq.ShortCircuit = False
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from Configurables import DecayTreeTuple, TupleToolTrackInfo, TupleToolRecoStats,LoKi__Hybrid__Dict2Tuple, LoKi__Hybrid__DictOfFunctors
from Configurables import TupleToolMCTruth
from DecayTreeTuple.Configuration import *

MvaVars = {'chi2'   : 'VFASPF(VCHI2)',
           'fdchi2' : 'BPVVDCHI2',
            'sumpt'  : 'SUMTREE(PT, ISBASIC, 0.0)',
            'nlt16'  : 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'}

for tuple in tuples:
    tup = DecayTreeTuple(tuple["Name"])
    tup.Inputs = tuple["Inputs"]
    tup.Decay = tuple["Decay"]    
    tup.addBranches(tuple["DecayBranches"])
    
    # tup.InputPrimaryVertices = '/Event/Turbo/Primary'
    #Refit the PV
    tup.ReFitPVs = False
    
    tl = [ "TupleToolGeometry",
           "TupleToolKinematic",
           # "TupleToolPropertime",
           "TupleToolPrimaries",
           "TupleToolPid",
           "TupleToolEventInfo",
           # "TupleToolTrackInfo",
           # "TupleToolRecoStats",
           # "TupleToolTISTOS",
           # "TupleToolTrigger",
           "TupleToolMCBackgroundInfo",
           ]

    tup.ToolList += tl

    TriggerList = [
        'L0HadronDecision',
        'L0MuonDecision',
        # 'Hlt1TrackMVADecision',
        # 'Hlt1TwoTrackMVADecision',
        ]
    
    from Configurables import TupleToolTrigger
    tupleToolTrigger = tup.addTupleTool(TupleToolTrigger, name='TupleToolTrigger')
    tupleToolTrigger.VerboseL0 = True
    # tupleToolTrigger.VerboseHlt1 = True
    # tupleToolTrigger.VerboseHlt2 = False
    # tupleToolTrigger.FillHlt2 = False
    tupleToolTrigger.TriggerList = TriggerList
    
    from Configurables import TupleToolTISTOS
    tupleToolTISTOS = tup.addTupleTool(TupleToolTISTOS, name='TupleToolTISTOS')
    # tupleToolTISTOS.addTool(L0TriggerTisTos())
    # tupleToolTISTOS.addTool(TriggerTisTos())
    tupleToolTISTOS.Verbose = True         
    tupleToolTISTOS.VerboseL0 = True
    # tupleToolTISTOS.VerboseHlt1 = True
    # tupleToolTISTOS.VerboseHlt2 = False
    # tupleToolTISTOS.FillHlt2 = False
    tupleToolTISTOS.TriggerList = TriggerList

    tupleToolTrackInfo = tup.addTupleTool(TupleToolTrackInfo, name='TupleToolTrackInfo')
    tupleToolTrackInfo.Verbose = False
    
    # RecoStats to filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tupleToolRecoStats = tup.addTupleTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tupleToolRecoStats.Verbose=True

    # MC information
    tupleToolMCTruth = tup.addTupleTool(TupleToolMCTruth, name='TupleToolMCTruth')
    tupleToolMCTruth.ToolList =  [ "MCTupleToolHierarchy", "MCTupleToolKinematic",  ]
    
    #Hlt1TwoTrackMVA response (only KS0 daughters combination)
    from MVADictHelpers import *
    addMatrixnetclassifierTuple(tup.KS0, '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx', MvaVars, Name='Hlt1TwoTrackMVAResponse_Classifier', Keep=True)
    
    ##LIST OF LOKI VARIABLES
    from Configurables import LoKi__Hybrid__TupleTool
    hybridTool = tup.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Hybrid')
    hybridTool.Preambulo +=  ["import math",
                              "Hlt1TrackMVAResponse = switch( ( TRCHI2DOF < %(TrChi2)s) & (TRGHOSTPROB < %(TrGP)s) & (((PT > %(MaxPT)s) & (BPVIPCHI2() > %(MinIPChi2)s)) | ( in_range( %(MinPT)s, PT, %(MaxPT)s) & (log(BPVIPCHI2()) > (%(Param1)s / ((PT / GeV - %(Param2)s) ** 2)  + (%(Param3)s / %(MaxPT)s) * (%(MaxPT)s   - PT) + math.log(%(MinIPChi2)s) ) ) ) )         ,1,0)" % {
            'TrChi2'      : 2.5,
            'TrGP'        : 0.2,
            'MinPT'       : 1000.  * MeV,
            'MaxPT'       : 25000.  * MeV,
            'MinIPChi2'   : 7.4,
            'Param1'      : 1.0,
            'Param2'      : 1.0,
            'Param3'      : 1.1
            # 'GEC'         : 'Loose'
            },
                              "Hlt1TwoTrackMVAResponse_goodVertex = switch( ((PT          > %(PT)s * MeV) & (P           > %(P)s    * MeV) &   (TRCHI2DOF   < %(TrChi2)s) &        (TRGHOSTPROB < %(TrGP)s) & (BPVIPCHI2() > %(IPChi2)s)) & ( HASVERTEX & (VFASPF(VCHI2) < %(VxChi2)s)      &          (in_range(%(MinETA)s,  BPVETA,    %(MaxETA)s))  & (in_range(%(MinMCOR)s, BPVCORRM, %(MaxMCOR)s)) &     (BPVDIRA           > %(MinDirA)s) )                         ,1,0)" % {
            'P'           :  5000. * MeV,
            'PT'          :   600. * MeV,
            'TrGP'        :  0.2,
            'TrChi2'      :     2.5,
            'IPChi2'      :     4.,
            'MinMCOR'     :  1000. * MeV,
            'MaxMCOR'     :   1e9  * MeV,
            'MinETA'      :     2.,
            'MaxETA'      :     5.,
            'MinDirA'     :     0.,
            'V0PT'        :  2000. * MeV,
            'VxChi2'      :    10.,
            #     #     # 'Threshold'   :     0.95,
            #     #     # 'MvaVars'     : {'chi2'   : 'VFASPF(VCHI2)',
            #     #     #                  'fdchi2' : 'BPVVDCHI2',
            #     #     #                  'sumpt'  : 'SUMTREE(PT, ISBASIC, 0.0)',
            #     #     #                  'nlt16'  : 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'},
            #     #     # 'Classifier'  : {'Type'   : 'MatrixNet',
            #     #     #                  'File'   : '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx'},
            #     #     # 'GEC'         : 'Loose'
            # ( VALUE('LoKi::Hybrid::DictValue/TwoTrackMVAMatrixNetTool') > 0.95 )
            },
                              ]
    hybridTool.Variables = {
        #"ET" : "TransverseEnergy()",
        #"PE" : "PE",
        "ETA" : "ETA",
        "PHI" : "PHI",
        "M12" : "M12",
        "Hlt1TrackMVAResponse" : "Hlt1TrackMVAResponse",
        "Hlt1TwoTrackMVAResponse_goodVertex" : "Hlt1TwoTrackMVAResponse_goodVertex",
        }
     

    # event tuple
    from Configurables import LoKi__Hybrid__EvtTupleTool
    LoKi_EvtTuple = tup.addTupleTool(LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple"))
    LoKi_EvtTuple.VOID_Variables = { 
        "LoKi_nPVs"                : "CONTAINS('Rec/Vertex/Primary')",
        "LoKi_nSpdMult"            : "CONTAINS('Raw/Spd/Digits')",
        "LoKi_nVeloClusters"       : "CONTAINS('Raw/Velo/Clusters')",
        "LoKi_nVeloLiteClusters"   : "CONTAINS('Raw/Velo/LiteClusters')",
        "LoKi_nITClusters"         : "CONTAINS('Raw/IT/Clusters')",
        "LoKi_nTTClusters"         : "CONTAINS('Raw/TT/Clusters')",
        "LoKi_nOThits"             : "CONTAINS('Raw/OT/Times')"
        }   

    
#DECAY TREE FITTER                                              
    tup.addTool(TupleToolDecay, name="Dplus")
#constrain only PV                                              
    tup.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
    tup.Dplus.DTFonlyPV.constrainToOriginVertex = True
    tup.Dplus.DTFonlyPV.Verbose = True
    tup.Dplus.DTFonlyPV.UpdateDaughters = True
#DTF simple (with only Momentum Scaling ?)                                
    tup.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMS')
    tup.Dplus.DTFonlyMS.Verbose = True
    tup.Dplus.DTFonlyMS.UpdateDaughters = True
#constrain Dp mass                                   
    tup.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyM')
    # tup.Dplus.DTFonlyM.constrainToOriginVertex = True
    tup.Dplus.DTFonlyM.Verbose = True
    tup.Dplus.DTFonlyM.daughtersToConstrain = ['KS0']
    tup.Dplus.DTFonlyM.UpdateDaughters = True
     
    tupleSeq.Members += [tup]

    
for tuple in mcTuples:
    mctuple = MCDecayTreeTuple( "MC" + tuple["Name"] )
    mctuple.Decay = tuple["Decay"]
    mctuple.addBranches(tuple["DecayBranches"])
    mctuple.ToolList = [ "MCTupleToolKinematic", "MCTupleToolReconstructed", "MCTupleToolDecayType", "MCTupleToolHierarchy", "MCTupleToolPID", "LoKi::Hybrid::MCTupleTool/LoKi_Photos" ]
#    mctuple.OutputLevel = 1
    from Configurables import LoKi__Hybrid__MCTupleTool
    hybridTool = mctuple.addTupleTool('LoKi::Hybrid::MCTupleTool/LoKi_MCHybrid')
    hybridTool.Variables = {
        "ETA" : "MCETA",
        "PHI" : "MCPHI",
        # GeneratorLevelCuts (-> for checks)
        "THETA" : "MCTHETA",
        #"M12" : "MCM(1,2)",
        "PT" : "MCPT",
        }

    
    tupleSeq.Members += [mctuple]

from Configurables import PrintMCTree
pMC = PrintMCTree()
#pMC.ParticleNames = [ "D*(2010)+" , "D*(2010)-" , "D0"]
#pMC.OutputLevel = 2
userAlgos = [ sequenceKS02pimpipLL,sequenceDp2KS0KpLL,
             # sequenceHlt1TrackMVA,
             tupleSeq]

DaVinci().HistogramFile = 'pGun_histos.root'
DaVinci().TupleFile = 'pGun_Dp2KS0KpLL.root'
DaVinci().UserAlgorithms = userAlgos
#DaVinci().UserAlgorithms = [tupleSeq]
DaVinci().InputType = "DST"
#DaVinci().InputType = "sim"
DaVinci().Simulation = True
DaVinci().PrintFreq = 1
DaVinci().Lumi = False
from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

