import os
if 'MAINDIR' in os.environ:
    pGunDirectory = os.environ['MAINDIR']
if pGunDirectory[-1] != "/":
    pGunDirectory = pGunDirectory+"/"

from Configurables import ParticleGun, MomentumRange, FlatNParticles,  MomentumSpectrum
from GaudiKernel import SystemOfUnits

pgun = ParticleGun()
pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.addTool( FlatNParticles , name = "FlatNParticles" )
pgun.DecayTool = "EvtGenDecay"
pgun.SignalPdgCode = 411 #Dp
from GaudiKernel.SystemOfUnits import *

pgun.ParticleGunTool = "MomentumSpectrum"
pgun.addTool( MomentumSpectrum , name = "MomentumSpectrum" )
pgun.MomentumSpectrum.PdgCodes = [ 411 , -411 ]
pgun.MomentumSpectrum.InputFile = pGunDirectory+"options/Dp2KS0pipLL_MomentumSpectrum_16.root" 
pgun.MomentumSpectrum.HistogramPath = "h_Dplus_pxpypz"
pgun.MomentumSpectrum.BinningVariables = "pxpypz"

from Configurables import BeamSpotSmearVertex
pgun.addTool(BeamSpotSmearVertex, name="BeamSpotSmearVertex")
pgun.VertexSmearingTool = "BeamSpotSmearVertex"

pgun.EventType = 411310211

pgun.GenCutTool = "LoKi::GenCutTool/TightCut"
from Configurables import LoKi__GenCutTool
pgun.addTool ( LoKi__GenCutTool , name = 'TightCut' )
cuts = pgun.TightCut

from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay, name = 'EvtGenDecay' )
ToolSvc().EvtGenDecay.UserDecayFile = pGunDirectory+"options/Dp2KS0pip.dec"
'''
cuts.Decay      = '^[D+ -> ^(KS0 -> pi+ pi-) ^pi+]CC'
cuts.Preambulo += [
    'from GaudiKernel.SystemOfUnits import MeV ',
    'import math',
    'inAcc      = in_range (1.5 , GETA ,  5.1) & (GPT > 190 * MeV) & (GP > 990 * MeV)',
   # 'BeamPipeCut = (~((( abs(GPX) < 0.01605*GPZ + 497 *MeV) & ( abs(GPX) > - 0.01397*GPZ + 418 *MeV))&(abs(GPY/GPZ) < 0.02)))',
    #'FiducialCuts = ( (~(abs(GPX) > 0.317*(GP - 2400.0 *MeV))) & BeamPipeCut )',
    'DV_minus_PV = pow( pow(GFAEVX(GVX,0)-GFAPVX(GVX,0),2) + pow(GFAEVX(GVY,0)-GFAPVX(GVY,0),2) + pow(GFAEVX(GVZ,0)-GFAPVX(GVZ,0),2) , 0.5)',
    'DV_minus_PV_XY = pow( pow(GFAEVX(GVX,0)-GFAPVX(GVX,0),2) + pow(GFAEVX(GVY,0)-GFAPVX(GVY,0),2) , 0.5)',
    'cosTheta_2 = ( ( (GFAEVX(GVX,0)-GFAPVX(GVX,0))*GCHILD(GPX,2) + (GFAEVX(GVY,0)-GFAPVX(GVY,0))*GCHILD(GPY,2) + (GFAEVX(GVZ,0)-GFAPVX(GVZ,0))*GCHILD(GPZ,2) ) / ( DV_minus_PV*GCHILD(GP,2) ) )',
    'sinTheta_2 = pow( 1 - pow( cosTheta_2, 2 ) , 0.5)',
    'IP_2 = DV_minus_PV * sinTheta_2',
    'Dcuts      = ( GVEV & (GTIME > 0.1 * mm) & ( IP_2 > 0.1 *mm ) & (DV_minus_PV_XY > 0.2 *mm) )', 
   # 'CombCuts  =  (GCHILD(GPT,1) + GCHILD(GPT,2) > 1990 *MeV)',
    'HarmCuts  =  ( GCHILD(GP,1) > 29.950 *GeV ) & ( GCHILD(GPT,1) > 2 *GeV ) & ( GPT - GCHILD(GPT,2) > 2200 *MeV ) & ( GPT > 4.150 *GeV ) & ( GCHILD(GPT,2) > 1.5 *GeV ) & in_range( 2 , GCHILD(GETA,1) , 4.5 ) & in_range( 2 , GCHILD(GETA,2) , 4.5 ) & in_range( 2 , GETA, 4.5 )',
    'KS0ltime      = in_range (1.5 , GETA ,  5.1) & ( GFAEVX(GVZ,0) > -100.0 * mm ) & ( GFAEVX(GVZ,0) < 500.0 * mm ) & ( GFAEVX(abs(GVX),0) < 40.0 * mm ) & ( GFAEVX(abs(GVY),0) < 40.0 * mm )',
    ]
cuts.Cuts       = {
    '[pi+]cc'         : 'inAcc',
    #'[D+]cc'          : 'Dcuts & CombCuts',
    '[D+]cc'          : 'Dcuts & HarmCuts',
    'KS0'             : "KS0ltime & in_range( 1.5 , GCHILD(GETA,('pi+' == GABSID )) , 5.1 )",
    }
'''
cuts.Decay      = '^[D+ -> ^(KS0 -> pi+ pi-) ^pi+]CC'
cuts.Preambulo += [
    'from GaudiKernel.SystemOfUnits import MeV ',
    'import math',
    'DV_minus_PV = pow( pow(GFAEVX(GVX,0)-GFAPVX(GVX,0),2) + pow(GFAEVX(GVY,0)-GFAPVX(GVY,0),2) + pow(GFAEVX(GVZ,0)-GFAPVX(GVZ,0),2) , 0.5)',
    'DV_minus_PV_XY = pow( pow(GFAEVX(GVX,0)-GFAPVX(GVX,0),2) + pow(GFAEVX(GVY,0)-GFAPVX(GVY,0),2) , 0.5)',
    'cosTheta_2 = ( ( (GFAEVX(GVX,0)-GFAPVX(GVX,0))*GCHILD(GPX,2) + (GFAEVX(GVY,0)-GFAPVX(GVY,0))*GCHILD(GPY,2) + (GFAEVX(GVZ,0)-GFAPVX(GVZ,0))*GCHILD(GPZ,2) ) / ( DV_minus_PV*GCHILD(GP,2) ) )',
    'sinTheta_2 = pow( 1 - pow( cosTheta_2, 2 ) , 0.5)',
    'IP_2 = DV_minus_PV * sinTheta_2',
    'inAcc_Dp      = ( GVEV & (GTIME > 0.1 * mm) & ( IP_2 > 0.1 *mm ) & (DV_minus_PV_XY > 0.2 *mm) )', 
    'inAcc_KS0      = in_range( 1.5 , GCHILD(GETA,("pi+" == GABSID )) , 5.1 ) & ( GFAEVX(GVZ,0) > -100.0 * mm ) & ( GFAEVX(GVZ,0) < 500.0 * mm ) & ( GFAEVX(abs(GVX),0) < 40.0 * mm ) & ( GFAEVX(abs(GVY),0) < 40.0 * mm )',
    'HarmCuts_pip   = in_range (2 , GETA ,  4.5) & (GPT > 1.550 *GeV)',
    'HarmCuts_KS0  = in_range (2 , GETA ,  4.5) & (GP > 29.950 *GeV) & (GPT > 2 *GeV)',
    'HarmCuts_Dp  =  in_range (2 , GETA ,  4.5) & ( GPT > 4.150 *GeV ) & ( GPT - GCHILD(GPT,2) > 2200 *MeV )',
]
cuts.Cuts       = {
    '[pi+]cc'         : 'HarmCuts_pip',
    'KS0'             : 'inAcc_KS0 & HarmCuts_KS0',
    #    '[D+]cc'          : 'inAcc_Dp & HarmCuts_Dp',
    '[D+]cc'          : 'HarmCuts_Dp',
}

