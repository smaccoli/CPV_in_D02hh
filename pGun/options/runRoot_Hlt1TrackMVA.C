#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <Riostream.h>
#include <TLeaf.h>

using namespace std;

void runRoot_Hlt1TrackMVA(TString decay = "Dsp2KmKppip",TString addString = "",TString MotherName = "Dplus") {

  std::vector<std::string> DaughtersNames = {"piplus","Kminus"};
  
  if (decay.Contains("KmKppip"))
    DaughtersNames = {"piplus","Kplus","Kminus"};
  else if(decay.Contains("Kmpippip"))
    DaughtersNames = {"hplus","piplus","Kminus"};
  else if(decay.Contains("KS0pip"))
    DaughtersNames = {"hplus","piplus","piminus"};
  else if(decay.Contains("KS0Kp"))
    DaughtersNames = {"hplus","piplus","piminus"};
  
  cout << "Creating pGun_"+decay+addString+"_Hlt1TrackMVA.root ..." << endl;
  
  TString treename = "DecayTree";
  TChain * tree = new TChain(decay+"/"+treename);
  tree->Add("pGun_"+decay+addString+".root");
  Long64_t eventNumber;
  tree->SetBranchAddress("eventNumber",&eventNumber);

  TString TwoTrackMVA_pimpip = "Kst2pimpip";
  TChain * tree_TwoTrackMVA_pimpip = new TChain(TwoTrackMVA_pimpip+"/"+treename);
  tree_TwoTrackMVA_pimpip->Add("pGun_Hlt1TwoTrackMVA.root");
  Long64_t eventNumber_TwoTrackMVA_pimpip;
  tree_TwoTrackMVA_pimpip->SetBranchAddress("eventNumber",&eventNumber_TwoTrackMVA_pimpip);
  Double_t Kstar_Hlt1TwoTrackMVAResponse_goodVertex_TwoTrackMVA_pimpip;
  tree_TwoTrackMVA_pimpip->SetBranchAddress("Kstar_Hlt1TwoTrackMVAResponse_goodVertex",&Kstar_Hlt1TwoTrackMVAResponse_goodVertex_TwoTrackMVA_pimpip);
  Double_t Kstar_Hlt1TwoTrackMVAResponse_Classifier_TwoTrackMVA_pimpip;
  tree_TwoTrackMVA_pimpip->SetBranchAddress("Kstar_Hlt1TwoTrackMVAResponse_Classifier",&Kstar_Hlt1TwoTrackMVAResponse_Classifier_TwoTrackMVA_pimpip);

  TString TwoTrackMVA_pippip = "Kst2pippip";
  TChain * tree_TwoTrackMVA_pippip = new TChain(TwoTrackMVA_pippip+"/"+treename);
  tree_TwoTrackMVA_pippip->Add("pGun_Hlt1TwoTrackMVA.root");
  Long64_t eventNumber_TwoTrackMVA_pippip;
  tree_TwoTrackMVA_pippip->SetBranchAddress("eventNumber",&eventNumber_TwoTrackMVA_pippip);
  Double_t Kstar_Hlt1TwoTrackMVAResponse_goodVertex_TwoTrackMVA_pippip;
  tree_TwoTrackMVA_pippip->SetBranchAddress("Kstar_Hlt1TwoTrackMVAResponse_goodVertex",&Kstar_Hlt1TwoTrackMVAResponse_goodVertex_TwoTrackMVA_pippip);
  Double_t Kstar_Hlt1TwoTrackMVAResponse_Classifier_TwoTrackMVA_pippip;
  tree_TwoTrackMVA_pippip->SetBranchAddress("Kstar_Hlt1TwoTrackMVAResponse_Classifier",&Kstar_Hlt1TwoTrackMVAResponse_Classifier_TwoTrackMVA_pippip);
  
  TFile *NewFile = new TFile("pGun_"+decay+addString+"_Hlt1TrackMVA.root","RECREATE");//
  TTree *tmp_tree = new TTree(treename,treename);
  Bool_t Mother_Hlt1TwoTrackMVADecision_TOS;
  tmp_tree->Branch(MotherName+"_Hlt1TwoTrackMVADecision_TOS",&Mother_Hlt1TwoTrackMVADecision_TOS);
  Bool_t Mother_Hlt1TrackMVADecision_TOS;
  tmp_tree->Branch(MotherName+"_Hlt1TrackMVADecision_TOS",&Mother_Hlt1TrackMVADecision_TOS);
  TLeaf* leaf;
  TString DaughtersNames_Hlt1TrackMVAResponse;
  
  Long64_t nentries = tree->GetEntries();
  Long64_t nentries_TwoTrackMVA_pimpip = tree_TwoTrackMVA_pimpip->GetEntries();
  Long64_t nentries_TwoTrackMVA_pippip = tree_TwoTrackMVA_pippip->GetEntries();
  Long64_t j_old = 0;
  Long64_t k_old = 0;
  for (Long64_t i = 0; i < nentries; i++) {
    tree->GetEntry(i);
  
    //------ TrackMVADecision ---------
    Mother_Hlt1TrackMVADecision_TOS = 0;
    // cout << eventNumber << endl;
    for(std::string item : DaughtersNames) {
      DaughtersNames_Hlt1TrackMVAResponse = (TString)item.c_str();
      DaughtersNames_Hlt1TrackMVAResponse += "_Hlt1TrackMVAResponse";
      leaf = (TLeaf*) tree->GetLeaf((const char*)DaughtersNames_Hlt1TrackMVAResponse);
      leaf->GetBranch()->GetEntry(i);
      Mother_Hlt1TrackMVADecision_TOS = Mother_Hlt1TrackMVADecision_TOS || (leaf->GetValue() > 0);
      // cout << DaughtersNames_Hlt1TrackMVAResponse << '\t' << Mother_Hlt1TrackMVADecision_TOS << endl;
    }
    
    //------ TwoTrackMVADecision ---------
    Mother_Hlt1TwoTrackMVADecision_TOS = 0;
    // cout << eventNumber << endl;
    for (Long64_t j = j_old; j < nentries_TwoTrackMVA_pimpip; j++) {
      tree_TwoTrackMVA_pimpip->GetEntry(j);
      if (eventNumber == eventNumber_TwoTrackMVA_pimpip) {
	// cout << Kstar_Hlt1TwoTrackMVAResponse_goodVertex_TwoTrackMVA_pimpip << '\t' << Kstar_Hlt1TwoTrackMVAResponse_Classifier_TwoTrackMVA_pimpip << endl; 
	if(Kstar_Hlt1TwoTrackMVAResponse_goodVertex_TwoTrackMVA_pimpip > 0 && Kstar_Hlt1TwoTrackMVAResponse_Classifier_TwoTrackMVA_pimpip > 0.95) {
	  Mother_Hlt1TwoTrackMVADecision_TOS = 1;
	  // cout << eventNumber_TwoTrackMVA_pimpip << '\t' << "Mother_Hlt1TwoTrackMVADecision_TOS = 1" << endl;
	  j_old = j;
	  break;
	}
      }
    }
    if ( Mother_Hlt1TwoTrackMVADecision_TOS == 0 ) {
      for (Long64_t k = k_old; k < nentries_TwoTrackMVA_pippip; k++) {
	tree_TwoTrackMVA_pippip->GetEntry(k);
	if (eventNumber == eventNumber_TwoTrackMVA_pippip) {
	  // cout << Kstar_Hlt1TwoTrackMVAResponse_goodVertex_TwoTrackMVA_pippip << '\t' << Kstar_Hlt1TwoTrackMVAResponse_Classifier_TwoTrackMVA_pippip << endl; 
	  if(Kstar_Hlt1TwoTrackMVAResponse_goodVertex_TwoTrackMVA_pippip > 0 && Kstar_Hlt1TwoTrackMVAResponse_Classifier_TwoTrackMVA_pippip > 0.95) {
	    Mother_Hlt1TwoTrackMVADecision_TOS = 1;
	    // cout << eventNumber_TwoTrackMVA_pippip << '\t' << "Mother_Hlt1TwoTrackMVADecision_TOS = 1             pippip" << endl;
	    k_old = k;
	    break;
	  }
	}
      }    
    }
    tmp_tree->Fill(); 
  } 

  tmp_tree->Write();
  // tmp_tree->Print();
  // cout << NewFile->GetName() << " OK" <<endl;
  NewFile->Close();

}


