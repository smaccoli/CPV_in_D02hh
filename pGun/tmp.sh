#!/bin/sh
export CMTPROJECTPATH=$CMTPROJECTPATH:/home/LHCB/acarbone/cmtuser
cd /gpfs_data/local/lhcb/users/smaccoli/data/CPV_in_D02hh/pGun/Dp2KS0pipLL_2016_Up/4001/
mainDir=/home/LHCB-T3/smaccoli/CPV_in_D02hh/pGun
JOBID=4001
export RUNNUMBER=4001
export EVTMAX=1000
export MAINDIR=/home/LHCB-T3/smaccoli/CPV_in_D02hh/pGun
polarity=MagUp
JOBNAME=MagUp.Dp2KS0pipLL.4001
cat /home/LHCB-T3/smaccoli/CPV_in_D02hh/pGun/options/runGauss-2016-Up.py > runGauss-2016-4001.py
echo "GaussGen.RunNumber = 4001" >> runGauss-2016-4001.py
cd /gpfs_data/local/lhcb/users/smaccoli/data/CPV_in_D02hh/pGun/Dp2KS0pipLL_2016_Up/4001/
date
lb-run Gauss/latest gaudirun.py /gpfs_data/local/lhcb/users/smaccoli/data/CPV_in_D02hh/pGun/Dp2KS0pipLL_2016_Up/4001/runGauss-2016-4001.py ${mainDir}/options/DataType-2016-Up.py ${mainDir}/options/pGun_Dp2KS0pipLL.py | gzip > out/log_Gauss.txt.gz 
date
lb-run Boole/latest gaudirun.py ${mainDir}/options/runBoole.py ${mainDir}/options/DataType-2016-Up.py ${mainDir}/options/Gauss-Data.py | gzip > out/log_Boole.txt.gz 
lb-run Moore/latest gaudirun.py '$APPCONFIGOPTS/L0App/L0AppSimProduction.py' ${mainDir}/options/L0AppTCK-2016.py '$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py' ${mainDir}/options/DataType-2016.py ${mainDir}/options/runMoore.py ${mainDir}/options/Boole-Data.py | gzip > out/log_Moore.txt.gz 
lb-run --runtime Brunel/v50r4 RecDev/v19r7 gaudirun.py ${mainDir}/options/runBrunel.py ${mainDir}/options/DataType-2016-Up.py ${mainDir}/options/Moore-Data.py | gzip > out/log_Brunel.txt.gz 
lb-run DaVinci/latest gaudirun.py ${mainDir}/options/runDaVinci_Hlt1TwoTrackMVA.py ${mainDir}/options/DataType-2016-Up.py ${mainDir}/options/Brunel-Data.py | gzip > out/log_DaVinci_Hlt1TwoTrackMVA.txt.gz 
lb-run DaVinci/latest gaudirun.py ${mainDir}/options/runDaVinci_Dp2KS0pipLL.py ${mainDir}/options/DataType-2016-Up.py ${mainDir}/options/Brunel-Data.py | gzip > out/log_DaVinci_Dp2KS0pipLL.txt.gz 
root -l -b -q ${mainDir}/options/runRoot_Hlt1TrackMVA.C\(\"Dp2KS0pipLL\"\) 
lb-run DaVinci/latest gaudirun.py ${mainDir}/options/runDaVinci_Dp2KS0KpLL.py ${mainDir}/options/DataType-2016-Up.py ${mainDir}/options/Brunel-Data.py | gzip > out/log_DaVinci_Dp2KS0KpLL.txt.gz 
root -l -b -q ${mainDir}/options/runRoot_Hlt1TrackMVA.C\(\"Dp2KS0KpLL\"\) 
lb-run DaVinci/latest gaudirun.py ${mainDir}/options/runDaVinci_Dp2KS0pipLL_ALL.py ${mainDir}/options/DataType-2016-Up.py ${mainDir}/options/Brunel-Data.py | gzip > out/log_DaVinci_Dp2KS0pipLL_ALL.txt.gz 
root -l -b -q ${mainDir}/options/runRoot_Hlt1TrackMVA.C\(\"Dp2KS0pipLL\",\"_ALL\"\) 
rm  /gpfs_data/local/lhcb/users/smaccoli/data/CPV_in_D02hh/pGun/Dp2KS0pipLL_2016_Up/4001/runGauss-2016-4001.py Gauss*.root New* Boole* Moore* test_* Brunel*.root pGun_histos.root
rm  Gauss*
rm  Brunel*
rm  out/*.gz
date
