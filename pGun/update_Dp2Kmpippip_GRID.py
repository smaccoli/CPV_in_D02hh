#!/storage/gpfs_lhcb_t3_01/lhcb_soft/lcg/external/Python/2.6.5p2/x86_64-slc5-gcc43-opt/bin/python
import os, shelve, time

#INFOs
year='2016'
# polarity='MagDown'
# polarity_short='Dw'
polarity='MagUp'
polarity_short='Up'
mode='Dp2Kmpippip'
#WORKING DIRECTORIESr
workdir=os.getenv('PWD')+'/'
optdir=os.getenv('PWD')+'/options/'
#DATABASEdirectory
jobsdir='/storage/gpfs_data/local/lhcb/users/smaccoli/data/CPV_in_D02hh/pGun/' + mode + '_' + year + '_' + polarity_short + '/'
#DBname = jobsdir+mode+'.db'
#DBname = jobsdir+mode+'0_999_resub.db'
#addstring = '0000_0999'
#addstring = '1000_1999'
#addstring = '2000_2999'
#addstring = '3000_3999'
#addstring = '4000_4999'
#addstring = '5000_5999'
addstring = '6000_6999'
#addstring = '7000_7999'
# addstring = '8000_8999'
# addstring = '9000_9999'
DBname = jobsdir+mode+addstring+'.db'

#DIRAC objects instantiation
from DIRAC.Core.Base.Script import parseCommandLine
parseCommandLine()
from LHCbDIRAC.Interfaces.API.DiracLHCb import DiracLHCb
from LHCbDIRAC.Interfaces.API.LHCbJob import LHCbJob
from DIRAC.Interfaces.API.JobRepository import JobRepository


dirac = DiracLHCb()
jobs = shelve.open(DBname,'c',2)

result = dirac.getJobStatus(jobs.keys())
finalReport = {}
if result['OK']:
  newjobs = result['Value']
  for jID,jDict in jobs.iteritems():
    job = jobs[jID]
    if not newjobs.has_key(int(jID)): continue
    newjob = newjobs[int(jID)]
    if newjob.has_key('Status'): job['State'] = newjob['Status']
    if newjob.has_key('Site'): job['Site'] = newjob['Site']
    job['Time'] = time.ctime().replace(' ','_')
    if not finalReport.has_key(job['State']): finalReport[job['State']] = 1
    else: finalReport[job['State']] += 1

    jobs[jID] = job

jobs.close()
    
print finalReport

