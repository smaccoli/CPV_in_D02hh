#!/storage/gpfs_lhcb_t3_01/lhcb_soft/lcg/external/Python/2.6.5p2/x86_64-slc5-gcc43-opt/bin/python
import os, thread, threading, time, shelve, sys

#INFOs
year='2016'
#polarity='MagDown'
#polarity_short='Dw'
polarity='MagUp'
polarity_short='Up'
mode='Dst2D0pi_D02Kpi'
#WORKING DIRECTORIESr
workdir=os.getenv('PWD')+'/'
optdir=os.getenv('PWD')+'/options/'
#DATABASEdirectory
jobsdir='/storage/gpfs_data/local/lhcb/users/smaccoli/data/CPV_in_D02hh/pGun/' + mode + '_' + year + '_' + polarity_short + '/'
#DBname = jobsdir+mode+'.db'
#DBname = jobsdir+mode+'0_999_resub.db'
#addstring = '0000_0999'
#addstring = '1000_1999'
#addstring = '2000_2999'
#addstring = '3000_3999'
addstring = '4000_4999'

#addstring = '5000_5999'
#addstring = '6000_6999'#HarmonizationCuts
#addstring = '7000_7999'#HarmonizationCuts
#addstring = '8000_8999'#HarmonizationCuts
#addstring = '9000_9999'#HarmonizationCuts
DBname = jobsdir+mode+addstring+'.db'

# Function for job submission inside thread
def submitJobs(joblist = [], dirac = None, submittedJobs = [], flags = [], id = 0):
  for j in joblist:
#    result = dirac.submit(j['Job'])
    result = dirac.submitJob(j['Job'])
    if result['OK']:
      jobID = result['Value']
      jobDict = {}
      jobDict['State'] = 'Submitted'
      jobDict['Name']  = j['Name']
      jobDict['Retrieved'] = '0'
      jobDict['SubmitTime'] = time.ctime().replace(' ','_')
      #jobDict['RunNumber'] = arg
      submittedJobs[id][jobID] = jobDict
      print result,j['Name']
    else:
      print "Unable to submit",j['Name']
  #FLAG FOR THREAD TERMINATION
  flags[id] = 0

#DIRAC objects instantiation
from DIRAC.Core.Base.Script import parseCommandLine
parseCommandLine()
from LHCbDIRAC.Interfaces.API.DiracLHCb import DiracLHCb
from LHCbDIRAC.Interfaces.API.LHCbJob import LHCbJob
from DIRAC.Interfaces.API.JobRepository import JobRepository

dirac = DiracLHCb()

#Creation of Jobs directory
os.system('if [ ! -d '+jobsdir+' ]; then mkdir '+jobsdir+'; fi')

#JOBS Creation
jobList = []
njobs = 0

#for i in PreviouslyFailed:#range(0,1000):
for i in range(4000,5000):
  inputList = [optdir+'runGauss-'+year+'-'+polarity_short+'.py',optdir+'DataType-'+year+'-'+polarity_short+'.py',optdir+'pGun_'+mode+'_GRID.py',optdir+mode+'.dec',optdir+'runBoole.py',optdir+'Gauss-Data.py',optdir+'runMoore.py',optdir+'Boole-Data.py',optdir+'runBrunel.py',optdir+'Moore-Data.py',optdir+'Brunel-Data.py',optdir+'runDaVinci_Hlt1TwoTrackMVA.py',optdir+'runDaVinci_'+mode+'.py',optdir+'runDaVinci_'+mode+'_ALL.py']
 
  j1 = LHCbJob()
  j1.setName(mode+addstring+'.'+str(i))
  j1.setExecutable('toexec_'+mode+'_'+year+'_'+polarity_short+'_GRID.sh',
                   str(i),#RunNumber aka generation seed and output-directory
                   logFile='toy.log',
                   systemConfig='x86_64-slc6-gcc49-opt')
  j1.setInputSandbox(inputList)
  #j1.setOutputSandbox(['__postprocesslocations__','std.out','std.err','*.log','out/log_*'])
  j1.setOutputSandbox([''])
  j1.setOutputData(['*.root'])
  j1.setCPUTime(172800)
  j1.setBannedSites(['LCG.CERN.ch', 'LCG.CNAF.it', 'LCG.GRIDKA.de', 'LCG.IN2P3.fr', 'LCG.NIKHEF.nl', 'LCG.PIC.es', 'LCG.RAL.uk', 'LCG.SARA.nl',#Tier1
                     'DIRAC.UZH.ch', #sites which gives problems
                     'LCG.Bari.it',
                     'LCG.Beijing.cn',
                     'LCG.Bristol.uk',
                     'LCG.Manchester.uk',
                     'LCG.Pisa.it',
                     'LCG.UKI-LT2-Brunel.uk',
                     'LCG.RAL-HEP.uk'
                     ])
  jobList.append({ 'Job' : j1, 'Name' : mode+'.'+str(i) })
  

#SPLITTING JOBLIST BY THREAD
nThread = int(10)
if len(jobList) < nThread: nThread = len(jobList)
njobsPerList = len(jobList)/nThread #Number of jobs to submit for each thread
jobListPerThread = [jobList[x:x+njobsPerList] for x in xrange(0, len(jobList), njobsPerList)]

if nThread < len(jobListPerThread):
  nThread = nThread + 1

submittedJobs = [] # LIST FOR SUBMITTED JOBS DICTIONARIES
flags = [] #FLAG FOR THREAD TERMINATION
for i in range(nThread):

  flags.append(1) # flag for ending thread
  submittedJobs.append({})
  try:
    thread.start_new_thread(submitJobs, (jobListPerThread[i], dirac, submittedJobs, flags, i) )#thread submission
  except:
    print "Unable to start thread"


#WAITING FOR THREADS END
while(flags != nThread*[0]):
  print flags,nThread*[0],threading.activeCount()
  time.sleep(10)

repoDB = shelve.open(DBname,'c',2)

for i in range(nThread):
  for jobID,jobDict in submittedJobs[i].iteritems():
    repoDB[str(jobID)] = jobDict
    print jobID,repoDB[str(jobID)]

repoDB.close()

