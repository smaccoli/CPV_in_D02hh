#include <iostream>
#include <math.h>

using namespace std;

int main(int argc, char * argv[]) {
  
  double Nreco_plus = atoi(argv[1]);
  double Nreco_minus = atoi(argv[2]);
  double Ngen = atoi(argv[3]);
  double Nreco_plus_err = argc >= 5 ? atoi(argv[4]) : sqrt(Nreco_plus);
  double Nreco_minus_err = argc >= 6 ? atoi(argv[5]) : sqrt(Nreco_minus);
  
  cout << Nreco_plus << endl;
  cout << Nreco_minus << endl;
  cout << Ngen << endl;
  cout << Nreco_plus_err << endl;
  cout << Nreco_minus_err << endl;
  
  
  double efficiency_plus = 1.*Nreco_plus/Ngen;
  double efficiency_minus = 1.*Nreco_minus/Ngen;

  double efficiency_plus_err;
  double efficiency_minus_err;

  efficiency_plus_err = sqrt(
			     pow(1./Ngen,2) * Nreco_plus + 
			     pow(-1.*Nreco_plus/Ngen/Ngen,2) * Ngen
			     );
 
  efficiency_minus_err = sqrt(
			     pow(1./Ngen,2) * Nreco_minus + 
			     pow(-Nreco_minus/Ngen/Ngen,2) * Ngen
			     );
  
  
  cout << efficiency_plus << " pm " << efficiency_plus_err << endl;
  cout << efficiency_minus << " pm " << efficiency_minus_err << endl;


  double asymmetry = (efficiency_plus-efficiency_minus)/(efficiency_plus+efficiency_minus);

  double asymmetry_err = sqrt(
			      pow(2*Nreco_minus/(Nreco_plus+Nreco_minus)/(Nreco_plus+Nreco_minus),2) * pow(Nreco_plus_err,2) +
			      pow(2*Nreco_plus/(Nreco_plus+Nreco_minus)/(Nreco_plus+Nreco_minus),2) * pow(Nreco_minus_err,2)
			      );

  
  cout << asymmetry << " pm " << asymmetry_err << endl;

}

