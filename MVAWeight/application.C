#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TString.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TROOT.h"
#include "TInterpreter.h"
#include "TMath.h"
#include "Riostream.h"
//#include "weights/Dplus_TMlpANN.cxx"
//#include "weights/hplus_TMlpANN.cxx"

//#include "weights/Dplus_BDTG.class.C" 
#include "weights/Dplus2_BDT.class.C" 
//#include "weights/Dplus2_BDTG.class.C" 

using namespace std;

void application(/*TString year, TString polarity*/) {
  
  TString decay = "Dp2Kmpippip";
  //TString decay = "Dp2KS0pipLL";

  std::vector<std::string> inputNames_Dplus = {
    "hplus_PT", 
    "Dplus_PT", 
    "hplus_ETA",
    "Dplus_ETA",
    "hplus_PHI",
    "Dplus_PHI"/*,

    "hplus_PX", 
    "Dplus_PX", 
    "hplus_PY",
    "Dplus_PY",
    "hplus_PZ",
    "Dplus_PZ"
	       */
  };

 
  TChain * TreeOld = new TChain("ntp");
  //TreeOld->Add("../"+decay+"/"+decay+".root");
  //TreeOld->Add("/home/LHCB/fferrari/ACPKK/ADKpi/data/ntuples/2016/MagDown/tuple_Kpipi_2016_MagDown_withSelection_withFiducialCuts_withSecondariesCut_withMultCandRemoval.root");
  TreeOld->Add("../data/"+decay+".root");
  TreeOld->SetBranchStatus("*",0);
  TreeOld->SetBranchStatus("Dplus_PT",1);  
  TreeOld->SetBranchStatus("Dplus_ETA",1);  
  TreeOld->SetBranchStatus("Dplus_PHI",1);  
  TreeOld->SetBranchStatus("hplus_PT",1);  
  TreeOld->SetBranchStatus("hplus_ETA",1);  
  TreeOld->SetBranchStatus("hplus_PHI",1);  
  TreeOld->SetBranchStatus("*sw*",1);  
  TreeOld->SetBranchStatus("Dplus_PX",1);  
  TreeOld->SetBranchStatus("Dplus_PY",1);  
  TreeOld->SetBranchStatus("Dplus_PZ",1);  
  TreeOld->SetBranchStatus("hplus_PX",1);  
  TreeOld->SetBranchStatus("hplus_PY",1);  
  TreeOld->SetBranchStatus("hplus_PZ",1);  
 
  //TFile *file;   
  TFile *outfile;
  outfile = new TFile(decay+"_MVA.root","RECREATE");
  TTree *TreeNew = TreeOld->CloneTree(0);

  Double_t Dplus_MVA;
  TreeNew->Branch("Dplus_MVA",&Dplus_MVA,"Dplus_MVA/D");
  Bool_t Dplus_PT_3p5e3cut;
  TreeNew->Branch("Dplus_PT_3p5e3cut",&Dplus_PT_3p5e3cut);
  Bool_t Overlap;
  TreeNew->Branch("Overlap",&Overlap);
 

  //Dplus_TMlpANN *Dplus_TMlpANN_Reader = new Dplus_TMlpANN();
  std::vector<Double_t> inputVars_Dplus;   
  ReadBDT *BDTReader = new ReadBDT(inputNames_Dplus);
  //hplus_TMlpANN *hplus_TMlpANN_Reader = new hplus_TMlpANN();
  
  TLeaf* leaf;
   
  Long64_t nentries = TreeOld->GetEntries();
  for (Long64_t k=0; k < nentries; ++k) {
    TreeOld->GetEntry(k);

    inputVars_Dplus.clear();
    
    for(std::string item : inputNames_Dplus) {
      leaf = (TLeaf*) TreeOld->GetLeaf((const char*)item.c_str());
      leaf->GetBranch()->GetEntry(k);
      //cout << item.c_str() << '\t' << leaf->GetValue() << endl;
      inputVars_Dplus.push_back(leaf->GetValue());
      if((TString)item.c_str() == "Dplus_PT")
	Dplus_PT_3p5e3cut = leaf->GetValue() > 3.5e3;
    }
    //Dplus_MVA = Dplus_TMlpANN_Reader->Value(0, &inputVars_Dplus[0]);  
    Dplus_MVA = BDTReader->GetMvaValue(inputVars_Dplus);

    /*
    for(std::string item : inputNames_hplus) {
      leaf = (TLeaf*) TreeOld->GetLeaf((const char*)item.c_str());
      leaf->GetBranch()->GetEntry(k);
      //cout << item.c_str() << '\t' << leaf->GetValue() << endl;
      inputVars_hplus.push_back(leaf->GetValue());
    }
    //hplus_MVA = hplus_TMlpANN_Reader->Value(0, &inputVars_hplus[0]);  
    hplus_MVA = hplus_BDTReader->GetMvaValue(inputVars_hplus);
    */
    Overlap = Dplus_MVA > -0.3;
    TreeNew->Fill();
  }

  outfile = TreeNew->GetCurrentFile();
  outfile->cd();
  
  TreeNew->Print();
  TreeNew->Write();
  outfile->Close();
 
  delete outfile;


}

