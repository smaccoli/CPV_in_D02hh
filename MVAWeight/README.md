To add variables weight, weight_err and weight_bkg to KK samples:

   export CMTCONFIG=x86_64-slc6-gcc49-opt

    lb-run LHCb/v41r1 python reweightKK_PP.py 15 | tee log_15
    lb-run LHCb/v41r1 python reweightKK_PP.py 16 | tee log_16
