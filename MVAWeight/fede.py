import os, sys
import root_numpy
import pandas
from hep_ml.reweight import BinsReweighter, GBReweighter
from hep_ml import reweight
import numpy as np
from rootpy.io import root_open
from ROOT import *

year = sys.argv[1]

### Remove previous weights

#serena
# f_up = TFile('/home/LHCB/smaccoli/data/Dsp2KmKppip_selectedANDsweighted_'+year+'.root',"update")
# ntp_up=f_up.Get("ntp")
# b_up = ntp_up.GetBranch("weight")
# if b_up != None:
#     print 'HERE '
#     ntp_up.GetListOfBranches().Remove(b_up)
#     f_up.WriteTObject(ntp_up)

# b_up_err = ntp_up.GetBranch("weight_err")
# if b_up_err != None:
#     print 'HERE '
#     ntp_up_err.GetListOfBranches().Remove(b_up_err)
#     f_up_err.WriteTObject(ntp_up_err)

# b_up_bkg = ntp_up.GetBranch("weight_bkg")
# if b_up_bkg != None:
#     print 'HERE '
#     ntp_up_bkg.GetListOfBranches().Remove(b_up_bkg)
#     f_up_bkg.WriteTObject(ntp_up_bkg)
# f_up.Close()


# f_down = TFile('../ntuples/DStarKmKppip_Tuple_MagDown_20'+year+'_sPlot.root',"update")
# ntp_down=f_down.Get("ntp")
# b_down = ntp_down.GetBranch("weight")
# if b_down != None:
#     print 'HERE '
#     ntp_down.GetListOfBranches().Remove(b_down)
#     f_down.WriteTObject(ntp) 


# b_down_err = ntp_down.GetBranch("weight_err")
# if b_down_err != None:
#     print 'HERE '
#     ntp_down_err.GetListOfBranches().Remove(b_down_err)
#     f_down_err.WriteTObject(ntp_down_err)


# b_down_bkg = ntp_down.GetBranch("weight_bkg")
# if b_down_bkg != None:
#     print 'HERE '
#     ntp_down_bkg.GetListOfBranches().Remove(b_down_bkg)
#     f_down_bkg.WriteTObject(ntp_down_bkg)
# f_down.Close()




###READ THE DATASETS (N.B. DATASETS SHOULD HAVE THE SAME NUMBER OF ENTRIES AND THE SAME VARIABLE NAMES)
print 'Reading datasets...'

###VARIABLES TO REWEIGHT
columns_KmKppip = ['Dplus_PT','Dplus_ETA','hplus_PT','hplus_ETA'
#,'Dplus_PHI','hplus_PHI'
]
columns_KS0pipLL = ['Dplus_PT','Dplus_ETA','hplus_PT','hplus_ETA'
#,'Dplus_PHI','hplus_PHI'
]

input_KmKppip_tot =    root_numpy.root2array('Dp2Kmpippip.root', treename='ntp', branches=columns_KmKppip)
input_KmKppip_up =    root_numpy.root2array('Dp2Kmpippip.root', treename='ntp', branches=columns_KmKppip
#, start=0, stop=800000
)
input_KS0pipLL_up =    root_numpy.root2array('Dp2KS0pipLL.root', treename='ntp', branches=columns_KS0pipLL
#, start=0, stop=100000
)

input_KmKppip_up_splot =    root_numpy.root2array('Dp2Kmpippip.root', treename='ntp', branches=['Nsig_sw']
#, start=0, stop=800000
) 
input_KS0pipLL_up_splot =    root_numpy.root2array('Dp2KS0pipLL.root', treename='ntp', branches=['Nsig_sw']
#, start=0, stop=100000
)

input_KmKppip_tot    = pandas.DataFrame(input_KmKppip_tot)
input_KmKppip_up    = pandas.DataFrame(input_KmKppip_up)
input_KS0pipLL_up    = pandas.DataFrame(input_KS0pipLL_up)

input_KmKppip_up_splot = input_KmKppip_up_splot['Nsig_sw'] 
input_KS0pipLL_up_splot = input_KS0pipLL_up_splot['Nsig_sw']

print "Number of entries",str(len(input_KmKppip_up))+" "+str(len(input_KS0pipLL_up))+" "+str(len(input_KmKppip_up_splot))+" "+str(len(input_KS0pipLL_up_splot)) 
#sys.exit()

###########################
### REWEIGHT FUNCTION. FOR TESTING PURPOSES, CHOOSE 'n_estimators' VERY LOW (10 IS OK). n_estimators IS THE NUMBER OF TREES THAT WILL BE TRAINED TO PERFORM THE REWEIGHTING
print 'Reweighting...'

reweighter = reweight.BinsReweighter([50,30,50,30], n_neighs=4)
#reweighter = reweight.GBReweighter(n_estimators=200, learning_rate=0.05, min_samples_leaf=500, max_depth=3) 
#reweighter = reweight.GBReweighter(n_estimators=500, learning_rate=0.001, min_samples_leaf=100, max_depth=6) 
#reweighter = reweight.FoldingReweighter(reweighter, n_folds=3) 
reweighter_err = reweighter
#reweight.BinsReweighter([100,20,100,20,20,20], n_neighs=20)
reweighter.fit(input_KmKppip_up,input_KS0pipLL_up,input_KmKppip_up_splot,input_KS0pipLL_up_splot)
gb_weights_up = reweighter.predict_weights(input_KmKppip_up)
new_column = np.array(gb_weights_up , dtype=[('weight', 'f8')])
sumWeights=0
for sw in new_column['weight']:
    sumWeights +=sw 
normFactor=sumWeights/len(input_KmKppip_up);
new_column['weight']=new_column['weight']/normFactor
root_numpy.array2root(new_column, 'Dp2Kmpippip_rew.root', 'ntp')
'''
reweighter_err.fit_err(input_KmKppip_up,input_KS0pipLL_up,input_KmKppip_up_splot,input_KS0pipLL_up_splot)
gb_weights_up_err = reweighter_err.predict_weights(input_KmKppip_up)
new_column_err = np.array(gb_weights_up_err , dtype=[('weight_err', 'f8')])
sumWeights=0
new_column_err['weight_err']=new_column_err['weight_err']/normFactor
root_numpy.array2root(new_column_err, 'Dp2Kmpippip_rew.root', 'ntp')
'''
