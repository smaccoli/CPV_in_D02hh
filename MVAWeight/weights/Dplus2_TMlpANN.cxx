#include "weights/Dplus2_TMlpANN.h"
#include <cmath>

double Dplus2_TMlpANN::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5) {
   input0 = (in0 - 10.7177)/2306.85;
   input1 = (in1 - 30.6587)/5122.97;
   input2 = (in2 - 22.1368)/2141.12;
   input3 = (in3 - 49.1002)/4729.91;
   input4 = (in4 - 31529.9)/22073.4;
   input5 = (in5 - 72900.6)/39439.2;
   switch(index) {
     case 0:
         return neuron0xaa144330();
     default:
         return 0.;
   }
}

double Dplus2_TMlpANN::Value(int index, double* input) {
   input0 = (input[0] - 10.7177)/2306.85;
   input1 = (input[1] - 30.6587)/5122.97;
   input2 = (input[2] - 22.1368)/2141.12;
   input3 = (input[3] - 49.1002)/4729.91;
   input4 = (input[4] - 31529.9)/22073.4;
   input5 = (input[5] - 72900.6)/39439.2;
   switch(index) {
     case 0:
         return neuron0xaa144330();
     default:
         return 0.;
   }
}

double Dplus2_TMlpANN::neuron0xdcca8eb0() {
   return input0;
}

double Dplus2_TMlpANN::neuron0x90d635b0() {
   return input1;
}

double Dplus2_TMlpANN::neuron0xb3f46e30() {
   return input2;
}

double Dplus2_TMlpANN::neuron0x662cfe70() {
   return input3;
}

double Dplus2_TMlpANN::neuron0x645fec70() {
   return input4;
}

double Dplus2_TMlpANN::neuron0xadb822e0() {
   return input5;
}

double Dplus2_TMlpANN::input0x16e23980() {
   double input = 0.72868;
   input += synapse0x78fbe570();
   input += synapse0xadb82470();
   input += synapse0x6e77fe60();
   input += synapse0x16e23b10();
   input += synapse0x99a8c230();
   input += synapse0x99a8c270();
   return input;
}

double Dplus2_TMlpANN::neuron0x16e23980() {
   double input = input0x16e23980();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0x5f3d20f0() {
   double input = 3.8087;
   input += synapse0x99a8c2b0();
   input += synapse0x5f3d2280();
   input += synapse0xce9fb2b0();
   input += synapse0xce9fb2f0();
   input += synapse0xce9fb330();
   input += synapse0x4fe3dd30();
   return input;
}

double Dplus2_TMlpANN::neuron0x5f3d20f0() {
   double input = input0x5f3d20f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0xc8765630() {
   double input = -0.435745;
   input += synapse0x4fe3dd70();
   input += synapse0x4fe3ddb0();
   input += synapse0x57793670();
   input += synapse0x577936b0();
   input += synapse0x577936f0();
   input += synapse0x4d93d340();
   return input;
}

double Dplus2_TMlpANN::neuron0xc8765630() {
   double input = input0xc8765630();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0xf0fa2770() {
   double input = 1.41329;
   input += synapse0x86a986f0();
   input += synapse0x86a98730();
   input += synapse0x86a98770();
   input += synapse0x10935c70();
   input += synapse0x10935cb0();
   input += synapse0x10935cf0();
   return input;
}

double Dplus2_TMlpANN::neuron0xf0fa2770() {
   double input = input0xf0fa2770();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0xb23ba1f0() {
   double input = -1.06711;
   input += synapse0xd645870();
   input += synapse0xd6458b0();
   input += synapse0xd6458f0();
   input += synapse0xa63881b0();
   input += synapse0xa63881f0();
   input += synapse0xa6388230();
   return input;
}

double Dplus2_TMlpANN::neuron0xb23ba1f0() {
   double input = input0xb23ba1f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0xff6fb030() {
   double input = -0.390737;
   input += synapse0x933ca1f0();
   input += synapse0x933ca230();
   input += synapse0x933ca270();
   input += synapse0xe1ed36b0();
   input += synapse0xe1ed36f0();
   input += synapse0xe1ed3730();
   return input;
}

double Dplus2_TMlpANN::neuron0xff6fb030() {
   double input = input0xff6fb030();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0x98ffd470() {
   double input = 0.726797;
   input += synapse0xd258b0b0();
   input += synapse0xd258b0f0();
   input += synapse0xd258b130();
   input += synapse0x2bf12eb0();
   input += synapse0x2bf12ef0();
   input += synapse0x2bf12f30();
   return input;
}

double Dplus2_TMlpANN::neuron0x98ffd470() {
   double input = input0x98ffd470();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0xfe5aeaf0() {
   double input = -0.968821;
   input += synapse0x3e61d8b0();
   input += synapse0x3e61d8f0();
   input += synapse0x3e61d930();
   input += synapse0x1b9fdc70();
   input += synapse0x1b9fdcb0();
   input += synapse0x1b9fdcf0();
   input += synapse0x48e324f0();
   return input;
}

double Dplus2_TMlpANN::neuron0xfe5aeaf0() {
   double input = input0xfe5aeaf0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0x3edb7e30() {
   double input = 0.0280641;
   input += synapse0x48e32530();
   input += synapse0x48e32570();
   input += synapse0xd4706e30();
   input += synapse0xd4706e70();
   input += synapse0xd4706eb0();
   input += synapse0x7182b4b0();
   input += synapse0x7182b4f0();
   return input;
}

double Dplus2_TMlpANN::neuron0x3edb7e30() {
   double input = input0x3edb7e30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0x317b4570() {
   double input = -0.124014;
   input += synapse0x7182b530();
   input += synapse0x8dd583f0();
   input += synapse0x8dd58430();
   input += synapse0x8dd58470();
   input += synapse0x6b3ebb30();
   input += synapse0x6b3ebb70();
   input += synapse0x6b3ebbb0();
   return input;
}

double Dplus2_TMlpANN::neuron0x317b4570() {
   double input = input0x317b4570();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0xdd215af0() {
   double input = 0.412582;
   input += synapse0xf2a3a7f0();
   input += synapse0xf2a3a830();
   input += synapse0x6315e760();
   input += synapse0xd9bc9ba0();
   input += synapse0x4fb83bd0();
   input += synapse0x53915030();
   input += synapse0x53915070();
   return input;
}

double Dplus2_TMlpANN::neuron0xdd215af0() {
   double input = input0xdd215af0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0x55b7140() {
   double input = 0.240964;
   input += synapse0x539150b0();
   input += synapse0x31bd5170();
   input += synapse0x31bd51b0();
   input += synapse0x31bd51f0();
   input += synapse0x7913c3f0();
   input += synapse0x7913c430();
   input += synapse0x7913c470();
   return input;
}

double Dplus2_TMlpANN::neuron0x55b7140() {
   double input = input0x55b7140();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0xaeadfdb0() {
   double input = 0.985924;
   input += synapse0x8fa46bb0();
   input += synapse0x8fa46bf0();
   input += synapse0x8fa46c30();
   input += synapse0x9aed9670();
   input += synapse0x9aed96b0();
   input += synapse0x9aed96f0();
   input += synapse0xff8850b0();
   return input;
}

double Dplus2_TMlpANN::neuron0xaeadfdb0() {
   double input = input0xaeadfdb0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double Dplus2_TMlpANN::input0xaa144330() {
   double input = 0.162559;
   input += synapse0xff8850f0();
   input += synapse0xff885130();
   input += synapse0x207fbdb0();
   input += synapse0x207fbdf0();
   input += synapse0x207fbe30();
   input += synapse0x3aaf8bb0();
   return input;
}

double Dplus2_TMlpANN::neuron0xaa144330() {
   double input = input0xaa144330();
   return (input * 1)+0;
}

double Dplus2_TMlpANN::synapse0x78fbe570() {
   return (neuron0xdcca8eb0()*-1.75087);
}

double Dplus2_TMlpANN::synapse0xadb82470() {
   return (neuron0x90d635b0()*4.82642);
}

double Dplus2_TMlpANN::synapse0x6e77fe60() {
   return (neuron0xb3f46e30()*0.0231698);
}

double Dplus2_TMlpANN::synapse0x16e23b10() {
   return (neuron0x662cfe70()*-0.199573);
}

double Dplus2_TMlpANN::synapse0x99a8c230() {
   return (neuron0x645fec70()*0.544797);
}

double Dplus2_TMlpANN::synapse0x99a8c270() {
   return (neuron0xadb822e0()*-0.414502);
}

double Dplus2_TMlpANN::synapse0x99a8c2b0() {
   return (neuron0xdcca8eb0()*0.319491);
}

double Dplus2_TMlpANN::synapse0x5f3d2280() {
   return (neuron0x90d635b0()*-0.112337);
}

double Dplus2_TMlpANN::synapse0xce9fb2b0() {
   return (neuron0xb3f46e30()*-0.114425);
}

double Dplus2_TMlpANN::synapse0xce9fb2f0() {
   return (neuron0x662cfe70()*0.0183587);
}

double Dplus2_TMlpANN::synapse0xce9fb330() {
   return (neuron0x645fec70()*-2.31138);
}

double Dplus2_TMlpANN::synapse0x4fe3dd30() {
   return (neuron0xadb822e0()*4.26016);
}

double Dplus2_TMlpANN::synapse0x4fe3dd70() {
   return (neuron0xdcca8eb0()*0.585493);
}

double Dplus2_TMlpANN::synapse0x4fe3ddb0() {
   return (neuron0x90d635b0()*0.427415);
}

double Dplus2_TMlpANN::synapse0x57793670() {
   return (neuron0xb3f46e30()*-0.471344);
}

double Dplus2_TMlpANN::synapse0x577936b0() {
   return (neuron0x662cfe70()*1.71511);
}

double Dplus2_TMlpANN::synapse0x577936f0() {
   return (neuron0x645fec70()*3.75888);
}

double Dplus2_TMlpANN::synapse0x4d93d340() {
   return (neuron0xadb822e0()*-2.64555);
}

double Dplus2_TMlpANN::synapse0x86a986f0() {
   return (neuron0xdcca8eb0()*2.63671);
}

double Dplus2_TMlpANN::synapse0x86a98730() {
   return (neuron0x90d635b0()*-5.63355);
}

double Dplus2_TMlpANN::synapse0x86a98770() {
   return (neuron0xb3f46e30()*0.424994);
}

double Dplus2_TMlpANN::synapse0x10935c70() {
   return (neuron0x662cfe70()*-1.88238);
}

double Dplus2_TMlpANN::synapse0x10935cb0() {
   return (neuron0x645fec70()*-0.630743);
}

double Dplus2_TMlpANN::synapse0x10935cf0() {
   return (neuron0xadb822e0()*0.4063);
}

double Dplus2_TMlpANN::synapse0xd645870() {
   return (neuron0xdcca8eb0()*0.173016);
}

double Dplus2_TMlpANN::synapse0xd6458b0() {
   return (neuron0x90d635b0()*0.816511);
}

double Dplus2_TMlpANN::synapse0xd6458f0() {
   return (neuron0xb3f46e30()*1.26889);
}

double Dplus2_TMlpANN::synapse0xa63881b0() {
   return (neuron0x662cfe70()*-3.9865);
}

double Dplus2_TMlpANN::synapse0xa63881f0() {
   return (neuron0x645fec70()*-1.48454);
}

double Dplus2_TMlpANN::synapse0xa6388230() {
   return (neuron0xadb822e0()*1.70955);
}

double Dplus2_TMlpANN::synapse0x933ca1f0() {
   return (neuron0xdcca8eb0()*0.0511602);
}

double Dplus2_TMlpANN::synapse0x933ca230() {
   return (neuron0x90d635b0()*1.34586);
}

double Dplus2_TMlpANN::synapse0x933ca270() {
   return (neuron0xb3f46e30()*-1.78778);
}

double Dplus2_TMlpANN::synapse0xe1ed36b0() {
   return (neuron0x662cfe70()*3.80788);
}

double Dplus2_TMlpANN::synapse0xe1ed36f0() {
   return (neuron0x645fec70()*-2.83616);
}

double Dplus2_TMlpANN::synapse0xe1ed3730() {
   return (neuron0xadb822e0()*2.56111);
}

double Dplus2_TMlpANN::synapse0xd258b0b0() {
   return (neuron0xdcca8eb0()*-1.43214);
}

double Dplus2_TMlpANN::synapse0xd258b0f0() {
   return (neuron0x90d635b0()*2.13);
}

double Dplus2_TMlpANN::synapse0xd258b130() {
   return (neuron0xb3f46e30()*0.716301);
}

double Dplus2_TMlpANN::synapse0x2bf12eb0() {
   return (neuron0x662cfe70()*-1.4118);
}

double Dplus2_TMlpANN::synapse0x2bf12ef0() {
   return (neuron0x645fec70()*0.11811);
}

double Dplus2_TMlpANN::synapse0x2bf12f30() {
   return (neuron0xadb822e0()*-0.141596);
}

double Dplus2_TMlpANN::synapse0x3e61d8b0() {
   return (neuron0x16e23980()*-0.402516);
}

double Dplus2_TMlpANN::synapse0x3e61d8f0() {
   return (neuron0x5f3d20f0()*0.903379);
}

double Dplus2_TMlpANN::synapse0x3e61d930() {
   return (neuron0xc8765630()*-0.295687);
}

double Dplus2_TMlpANN::synapse0x1b9fdc70() {
   return (neuron0xf0fa2770()*2.21626);
}

double Dplus2_TMlpANN::synapse0x1b9fdcb0() {
   return (neuron0xb23ba1f0()*-1.18929);
}

double Dplus2_TMlpANN::synapse0x1b9fdcf0() {
   return (neuron0xff6fb030()*1.07286);
}

double Dplus2_TMlpANN::synapse0x48e324f0() {
   return (neuron0x98ffd470()*-1.76729);
}

double Dplus2_TMlpANN::synapse0x48e32530() {
   return (neuron0x16e23980()*0.0798843);
}

double Dplus2_TMlpANN::synapse0x48e32570() {
   return (neuron0x5f3d20f0()*-0.243258);
}

double Dplus2_TMlpANN::synapse0xd4706e30() {
   return (neuron0xc8765630()*0.0602115);
}

double Dplus2_TMlpANN::synapse0xd4706e70() {
   return (neuron0xf0fa2770()*0.997195);
}

double Dplus2_TMlpANN::synapse0xd4706eb0() {
   return (neuron0xb23ba1f0()*1.11043);
}

double Dplus2_TMlpANN::synapse0x7182b4b0() {
   return (neuron0xff6fb030()*-0.216714);
}

double Dplus2_TMlpANN::synapse0x7182b4f0() {
   return (neuron0x98ffd470()*0.140414);
}

double Dplus2_TMlpANN::synapse0x7182b530() {
   return (neuron0x16e23980()*-1.35558);
}

double Dplus2_TMlpANN::synapse0x8dd583f0() {
   return (neuron0x5f3d20f0()*0.707409);
}

double Dplus2_TMlpANN::synapse0x8dd58430() {
   return (neuron0xc8765630()*0.945179);
}

double Dplus2_TMlpANN::synapse0x8dd58470() {
   return (neuron0xf0fa2770()*-2.18323);
}

double Dplus2_TMlpANN::synapse0x6b3ebb30() {
   return (neuron0xb23ba1f0()*-0.27766);
}

double Dplus2_TMlpANN::synapse0x6b3ebb70() {
   return (neuron0xff6fb030()*0.0812645);
}

double Dplus2_TMlpANN::synapse0x6b3ebbb0() {
   return (neuron0x98ffd470()*-1.92479);
}

double Dplus2_TMlpANN::synapse0xf2a3a7f0() {
   return (neuron0x16e23980()*-2.15458);
}

double Dplus2_TMlpANN::synapse0xf2a3a830() {
   return (neuron0x5f3d20f0()*4.35224);
}

double Dplus2_TMlpANN::synapse0x6315e760() {
   return (neuron0xc8765630()*-1.88713);
}

double Dplus2_TMlpANN::synapse0xd9bc9ba0() {
   return (neuron0xf0fa2770()*-1.35347);
}

double Dplus2_TMlpANN::synapse0x4fb83bd0() {
   return (neuron0xb23ba1f0()*2.45886);
}

double Dplus2_TMlpANN::synapse0x53915030() {
   return (neuron0xff6fb030()*2.94902);
}

double Dplus2_TMlpANN::synapse0x53915070() {
   return (neuron0x98ffd470()*-0.473773);
}

double Dplus2_TMlpANN::synapse0x539150b0() {
   return (neuron0x16e23980()*0.610001);
}

double Dplus2_TMlpANN::synapse0x31bd5170() {
   return (neuron0x5f3d20f0()*2.20569);
}

double Dplus2_TMlpANN::synapse0x31bd51b0() {
   return (neuron0xc8765630()*0.253976);
}

double Dplus2_TMlpANN::synapse0x31bd51f0() {
   return (neuron0xf0fa2770()*-2.57883);
}

double Dplus2_TMlpANN::synapse0x7913c3f0() {
   return (neuron0xb23ba1f0()*-1.14669);
}

double Dplus2_TMlpANN::synapse0x7913c430() {
   return (neuron0xff6fb030()*2.48578);
}

double Dplus2_TMlpANN::synapse0x7913c470() {
   return (neuron0x98ffd470()*-0.449805);
}

double Dplus2_TMlpANN::synapse0x8fa46bb0() {
   return (neuron0x16e23980()*-2.04542);
}

double Dplus2_TMlpANN::synapse0x8fa46bf0() {
   return (neuron0x5f3d20f0()*0.00846665);
}

double Dplus2_TMlpANN::synapse0x8fa46c30() {
   return (neuron0xc8765630()*0.84063);
}

double Dplus2_TMlpANN::synapse0x9aed9670() {
   return (neuron0xf0fa2770()*0.139441);
}

double Dplus2_TMlpANN::synapse0x9aed96b0() {
   return (neuron0xb23ba1f0()*-1.02791);
}

double Dplus2_TMlpANN::synapse0x9aed96f0() {
   return (neuron0xff6fb030()*0.962711);
}

double Dplus2_TMlpANN::synapse0xff8850b0() {
   return (neuron0x98ffd470()*-2.17911);
}

double Dplus2_TMlpANN::synapse0xff8850f0() {
   return (neuron0xfe5aeaf0()*-1.95302);
}

double Dplus2_TMlpANN::synapse0xff885130() {
   return (neuron0x3edb7e30()*-0.728442);
}

double Dplus2_TMlpANN::synapse0x207fbdb0() {
   return (neuron0x317b4570()*-1.34653);
}

double Dplus2_TMlpANN::synapse0x207fbdf0() {
   return (neuron0xdd215af0()*2.12252);
}

double Dplus2_TMlpANN::synapse0x207fbe30() {
   return (neuron0x55b7140()*-0.65655);
}

double Dplus2_TMlpANN::synapse0x3aaf8bb0() {
   return (neuron0xaeadfdb0()*2.10857);
}

