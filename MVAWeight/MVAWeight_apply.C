#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TString.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TH1.h"
#include "TH2.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
//#include"/home/LHCB/smaccoli/dcastyle.C"
#include "TROOT.h"
#include "TInterpreter.h"
#include "Riostream.h"

using namespace std;

void MVAWeight_apply(TString year = "16", TString polarity = "Dw") {
 
 
  TH1::SetDefaultSumw2();

  TString MVA_string = "Dplus_MVA";
  
  TString target_inFileName = "Dp2KS0pipLL_MVA.root";
  TChain *target_tree = new TChain("ntp");
  target_tree->Add(target_inFileName);
  target_tree->Draw(Form("%s>>h_target(500,-1,1)",(const char*)MVA_string),"Nsig_sw");
  TH1D* h_target = (TH1D*)gDirectory->Get("h_target");
  
  TString toweight_inFileName = "Dp2Kmpippip_MVA.root";
  TChain *toweight_tree = new TChain("ntp");
  toweight_tree->Add(toweight_inFileName);
  toweight_tree->Draw(Form("%s>>h_toweight(500,-1,1)",(const char*)MVA_string),"Nsig_sw");
  TH1D* h_toweight = (TH1D*)gDirectory->Get("h_toweight");
  
  TH1D* h_weight = (TH1D*)h_target->Clone();
  h_weight->Divide(h_toweight);
  
  toweight_tree->SetBranchStatus("*",0);
  toweight_tree->SetBranchStatus("Dplus*T*",1);
  toweight_tree->SetBranchStatus("Dplus*PHI*",1);
  toweight_tree->SetBranchStatus("hplus*T*",1);
  toweight_tree->SetBranchStatus("hplus*PHI*",1);
  toweight_tree->SetBranchStatus("Dplus*P*",1);
  toweight_tree->SetBranchStatus("hplus*P*",1);
  toweight_tree->SetBranchStatus("*sw*",1);
  toweight_tree->SetBranchStatus(MVA_string,1);
  
  Double_t MVA;
  toweight_tree->SetBranchAddress(MVA_string,&MVA);

 
  TString outFileName_B = "Dp2Kmpippip_rew2Dp2KS0pipLL.root";
 
  TFile *NewFile = new TFile(outFileName_B,"RECREATE");
  TTree *new_tree = toweight_tree->CloneTree(0);//new TTree("ntp","ntp");
  Double_t weight_MVA = 0;
  new_tree->Branch("weight_MVA",&weight_MVA);
  
  Long64_t nentries = toweight_tree->GetEntries();
  for (Long64_t i = 0; i < nentries; i++) {
    toweight_tree->GetEntry(i);
    weight_MVA = h_weight->GetBinContent(h_weight->FindBin(MVA));
    
    new_tree->Fill();
  }

  new_tree->Write();
  
  new_tree->Print();
  
  cout << outFileName_B << " OK" <<endl;

  NewFile->Close();
  
   

}

