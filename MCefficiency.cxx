#include <iostream>      
#include <stdio.h>       
#include <stdlib.h>      
#include <TString.h>         
#include <TFile.h>            
#include <TChain.h>      
#include <TTree.h>  
#include <TLorentzVector.h>   
#include "/home/LHCB/smaccoli/Tools.h"

void MCefficiency(TString inputFile = "/home/LHCB-T3/smaccoli/CPV_in_D02hh/data/pGun/Dp2Kmpippip_2016_Dw/1635/pGun_Dp2Kmpippip.root") {
  
  TFile * f;
  inputFile.ReplaceAll(".root","_ALL.root");
  f = TFile::Open(inputFile);
  if(!f) cout << "FILE " << inputFile << " DOES NOT EXIST!!" << endl;
  TTree * ntp;
  ntp = (TTree*)f->Get("MCDp2Kmpippip/MCDecayTree");
  ntp->SetBranchStatus("*ID",1);
  ntp->SetBranchStatus("*Reconstructed",1);
  
  int Dplus_ID; ntp->SetBranchAddress("Dplus_ID", &Dplus_ID);
  int Kminus_Reconstructed; ntp->SetBranchAddress("Kminus_Reconstructed", &Kminus_Reconstructed);
  int piplus_Reconstructed; ntp->SetBranchAddress("piplus_Reconstructed", &piplus_Reconstructed);

  double Dplus_TRUEP;// ntp->SetBranchAddress("Dplus_TRUEP", &Dplus_TRUEP);
  double Dplus_TRUEPT; ntp->SetBranchAddress("Dplus_TRUEPT", &Dplus_TRUEPT);
  double Dplus_ETA; ntp->SetBranchAddress("Dplus_ETA", &Dplus_ETA);
  double Dplus_PHI; ntp->SetBranchAddress("Dplus_PHI", &Dplus_PHI);
  double Dplus_TRUEP_X; ntp->SetBranchAddress("Dplus_TRUEP_X", &Dplus_TRUEP_X);
  double Dplus_TRUEP_Y; ntp->SetBranchAddress("Dplus_TRUEP_Y", &Dplus_TRUEP_Y);
  double Dplus_TRUEP_Z; ntp->SetBranchAddress("Dplus_TRUEP_Z", &Dplus_TRUEP_Z);
  double hplus_TRUEP;// ntp->SetBranchAddress("hplus_TRUEP", &hplus_TRUEP);
  double hplus_TRUEPT; ntp->SetBranchAddress("hplus_TRUEPT", &hplus_TRUEPT);
  double hplus_ETA; ntp->SetBranchAddress("hplus_ETA", &hplus_ETA);
  double hplus_PHI; ntp->SetBranchAddress("hplus_PHI", &hplus_PHI);
  double hplus_TRUEP_X; ntp->SetBranchAddress("hplus_TRUEP_X", &hplus_TRUEP_X);
  double hplus_TRUEP_Y; ntp->SetBranchAddress("hplus_TRUEP_Y", &hplus_TRUEP_Y);
  double hplus_TRUEP_Z; ntp->SetBranchAddress("hplus_TRUEP_Z", &hplus_TRUEP_Z);
  double Kminus_TRUEP;// ntp->SetBranchAddress("Kminus_TRUEP", &Kminus_TRUEP);
  double Kminus_TRUEPT; ntp->SetBranchAddress("Kminus_TRUEPT", &Kminus_TRUEPT);
  double Kminus_ETA; ntp->SetBranchAddress("Kminus_ETA", &Kminus_ETA);
  double Kminus_PHI; ntp->SetBranchAddress("Kminus_PHI", &Kminus_PHI);
  double Kminus_TRUEP_X; ntp->SetBranchAddress("Kminus_TRUEP_X", &Kminus_TRUEP_X);
  double Kminus_TRUEP_Y; ntp->SetBranchAddress("Kminus_TRUEP_Y", &Kminus_TRUEP_Y);
  double Kminus_TRUEP_Z; ntp->SetBranchAddress("Kminus_TRUEP_Z", &Kminus_TRUEP_Z);
  double piplus_TRUEP;// ntp->SetBranchAddress("piplus_TRUEP", &piplus_TRUEP);
  double piplus_TRUEPT; ntp->SetBranchAddress("piplus_TRUEPT", &piplus_TRUEPT);
  double piplus_ETA; ntp->SetBranchAddress("piplus_ETA", &piplus_ETA);
  double piplus_PHI; ntp->SetBranchAddress("piplus_PHI", &piplus_PHI);
  double piplus_TRUEP_X; ntp->SetBranchAddress("piplus_TRUEP_X", &piplus_TRUEP_X);
  double piplus_TRUEP_Y; ntp->SetBranchAddress("piplus_TRUEP_Y", &piplus_TRUEP_Y);
  double piplus_TRUEP_Z; ntp->SetBranchAddress("piplus_TRUEP_Z", &piplus_TRUEP_Z);

  double Kmpip_TRUEPT;// ntpOut->Branch("X_PT",&Kmpip_TRUEPT);//
  double Kmpip_ETA;// ntpOut->Branch("X_ETA",&Kmpip_ETA);//
  double Kmpip_PHI;// ntpOut->Branch("X_PHI",&Kmpip_PHI);//
  double Kmpip_TRUEP;// ntpOut->Branch("X_P",&Kmpip_TRUEP);//  
  double Kmpip_TRUEM;// ntpOut->Branch("X_M",&Kmpip_TRUEM);//  
  double Kmpip_TRUEpipi_M;// ntpOut->Branch("X_pipi_M",&Kmpip_TRUEpipi_M);//  
  

  bool HarmonizationCut_1D = true;
  bool HarmonizationCut_1D_X = true;
  bool HarmonizationCut_2D_hD = true;
  bool HarmonizationCut_2D_Kpi = true;
  bool HarmonizationCut = true;

  //Useful variables
  double Kminus_E, piplus_E;
  TLorentzVector Kminus, piplus, Kmpip;

  int Nreco_plus = 0;
  int Nreco_minus = 0;
  int Ngen = 0;
 
  for(int i = 0; i < ntp->GetEntries(); i++) {
    ntp->GetEntry(i);
    

    Kminus_E = sqrt(Kminus_TRUEP_X*Kminus_TRUEP_X+Kminus_TRUEP_Y*Kminus_TRUEP_Y+Kminus_TRUEP_Z*Kminus_TRUEP_Z+PdgMass::mKplus_PDG*PdgMass::mKplus_PDG);
    Kminus.SetPxPyPzE(Kminus_TRUEP_X,Kminus_TRUEP_Y,Kminus_TRUEP_Z,Kminus_E);
    piplus_E = sqrt(piplus_TRUEP_X*piplus_TRUEP_X+piplus_TRUEP_Y*piplus_TRUEP_Y+piplus_TRUEP_Z*piplus_TRUEP_Z+PdgMass::mPiplus_PDG*PdgMass::mPiplus_PDG);
    piplus.SetPxPyPzE(piplus_TRUEP_X,piplus_TRUEP_Y,piplus_TRUEP_Z,piplus_E);
    Kmpip = Kminus + piplus;
    
    Kmpip_TRUEPT = Kmpip.Pt();
    Kmpip_ETA = Kmpip.Eta();
    Kmpip_PHI = Kmpip.Phi();
    Kmpip_TRUEP = Kmpip.P();
    Kmpip_TRUEM = Kmpip.M();


    Dplus_TRUEP = sqrt(pow(Dplus_TRUEP_X,2)+pow(Dplus_TRUEP_Y,2)+pow(Dplus_TRUEP_Z,2));
    hplus_TRUEP = sqrt(pow(hplus_TRUEP_X,2)+pow(hplus_TRUEP_Y,2)+pow(hplus_TRUEP_Z,2));
    Kminus_TRUEP = sqrt(pow(Kminus_TRUEP_X,2)+pow(Kminus_TRUEP_Y,2)+pow(Kminus_TRUEP_Z,2));
    piplus_TRUEP = sqrt(pow(piplus_TRUEP_X,2)+pow(piplus_TRUEP_Y,2)+pow(piplus_TRUEP_Z,2));

    HarmonizationCut_1D = Dplus_TRUEP>14e3&&Dplus_TRUEP<250e3&&Dplus_TRUEPT>3.5e3&&Dplus_TRUEPT<14e3&&Dplus_ETA>2&&Dplus_ETA<4.5 && Kminus_TRUEP>5e3&&Kminus_TRUEP<90e3&&Kminus_TRUEPT>800&&Kminus_TRUEPT<6e3&&Kminus_ETA>1.9&&Kminus_ETA<4.7 && piplus_TRUEP>5e3&&piplus_TRUEP<90e3&&piplus_TRUEPT>800&&piplus_TRUEPT<5e3&&piplus_ETA>1.9&&piplus_ETA<4.7 && hplus_TRUEP>5e3&&hplus_TRUEP<150e3&&hplus_TRUEPT>1.5e3&&hplus_TRUEPT<8.5e3&&hplus_ETA>1.9&&hplus_ETA<4.7;
    HarmonizationCut_1D_X = Kmpip_TRUEPT > 2.2e3 && Kmpip_TRUEP > 25e3;
    HarmonizationCut_2D_hD = hplus_TRUEPT < (Dplus_TRUEPT-3.4e3)*6.5/7+1.2e3/*similar to Dplus_TRUEPT - 2.2e3 but better*/ && hplus_TRUEPT > (Dplus_TRUEPT-7.5e3);
    HarmonizationCut_2D_hD = HarmonizationCut_2D_hD && (
							(hplus_PHI > Dplus_PHI-2.96/6.51 && hplus_PHI < Dplus_PHI+2.96/6.51)
							||
							hplus_PHI > Dplus_PHI+2.96/0.51
							||
							hplus_PHI < Dplus_PHI-2.96/0.51
							);
    HarmonizationCut_2D_hD = HarmonizationCut_2D_hD && hplus_ETA < Dplus_ETA + 0.35 && hplus_ETA > Dplus_ETA - 0.45 && hplus_ETA > 2*Dplus_ETA - 4.4;
    HarmonizationCut_2D_Kpi = (Kminus_TRUEPT > -piplus_TRUEPT + 2.5e3) && (Kminus_TRUEPT + piplus_TRUEPT < 7.5e3);
    HarmonizationCut = HarmonizationCut_1D && HarmonizationCut_1D_X && HarmonizationCut_2D_hD && HarmonizationCut_2D_Kpi; 
    if(!HarmonizationCut) continue;
 
    Ngen++;

    if (Kminus_Reconstructed == 1 && piplus_Reconstructed == 1) {
      if (Dplus_ID > 0)
	Nreco_plus++;
      else
	Nreco_minus++;
    }
    
  }

  cout << "Yields:" << '\t' << Nreco_plus << '\t' << Nreco_minus << '\t' << Ngen << endl;
}
